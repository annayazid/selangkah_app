// import 'dart:io';
// import 'package:flutter/foundation.dart' show kIsWeb;
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:android_id/android_id.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Services/navigation_service.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';
import 'package:http/http.dart' as http;

class GlobalVariables {
  static String? privacy;
  static String? termsofuse;
  static String? language;
  static bool? retakeEkyc;
  static String? sessionId;
}

class GlobalFunction {
  static Future<void> getDeviceIdentifier() async {
    final deviceInfoPlugin = DeviceInfoPlugin();
    final deviceInfo = await deviceInfoPlugin.deviceInfo;
    final allInfo = deviceInfo.data;
    log(allInfo.toString());

    // String deviceIdentifier = "unknown";
    // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

    // if (Platform.isAndroid) {
    //   AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    //   deviceIdentifier = androidInfo.id;
    // } else if (Platform.isIOS) {
    //   IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    //   deviceIdentifier = iosInfo.identifierForVendor!;
    // } else if (kIsWeb) {
    //   // The web doesnt have a device UID, so use a combination fingerprint as an example
    //   WebBrowserInfo webInfo = await deviceInfo.webBrowserInfo;
    //   deviceIdentifier = webInfo.vendor! +
    //       webInfo.userAgent! +
    //       webInfo.hardwareConcurrency.toString();
    // } else if (Platform.isLinux) {
    //   LinuxDeviceInfo linuxInfo = await deviceInfo.linuxInfo;
    //   deviceIdentifier = linuxInfo.machineId!;
    // }
    // return deviceIdentifier;
  }

  static Future<bool> displayDisclosure({
    Widget? icon,
    String? title,
    String? description,
    String? key,
  }) async {
    bool proceed = false;

    if (Platform.isAndroid) {
      String? condition = await SecureStorage().readSecureData(key!);

      print('condition is $condition');

      if (condition == null || condition == 'false') {
        await showDialog(
          context:
              NavigationService.navigatorKey.currentState!.overlay!.context,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              content: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    icon!,
                    SizedBox(height: 10),
                    Text(
                      title!,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(height: 20),
                    Text(
                      description!,
                      textAlign: TextAlign.center,
                    ),
                    // SizedBox(height: 20),
                    // Text(
                    //   description,
                    //   textAlign: TextAlign.center,
                    // ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        TextButton(
                          child: Text(
                            "deny".tr(),
                            style: TextStyle(color: Colors.blue),
                          ),
                          onPressed: () {
                            proceed = false;
                            SecureStorage().writeSecureData(key, 'false');
                            Navigator.of(context).pop();
                            // showDenyDialog(icon, key);
                          },
                        ),
                        TextButton(
                          child: Text(
                            "agree".tr(),
                            style: TextStyle(color: Colors.blue),
                          ),
                          onPressed: () {
                            proceed = true;
                            SecureStorage().writeSecureData(key, 'true');
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        );
      } else if (condition == 'true') {
        proceed = true;
      } else {
        //toast

        // await Flushbar(
        //   backgroundColor: Colors.white,
        //   // message: LoremText,
        //   // title: 'Permission needed to continue',
        //   titleText: Text(
        //     'Permission needed to continue',
        //     style: TextStyle(
        //       color: Colors.black,
        //       fontWeight: FontWeight.bold,
        //       fontSize: 15,
        //     ),
        //   ),
        //   messageText: Text(
        //     'To continue this action, you need to enable the permission in app settings.',
        //     style: TextStyle(color: Colors.black),
        //   ),
        //   mainButton: FlatButton(
        //     onPressed: () {
        //       SecureStorage().writeSecureData(key, 'true');
        //       proceed = true;
        //       Navigator.of(navigatorKey.currentState.overlay.context).pop();
        //     },
        //     child: Text(
        //       'App Settings',
        //       style: TextStyle(color: Colors.blue),
        //     ),
        //   ),
        //   duration: Duration(seconds: 7),
        // ).show(navigatorKey.currentState.overlay.context);
      }
    } else {
      proceed = true;
    }

    return proceed;
  }

  static Future<void> screenJourney(String screenId) async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    String sessionId = GlobalVariables.sessionId!;

    final Map<String, String> map = {
      "id_selangkah_user": id,
      "screen_id": screenId,
      "session_id": sessionId,
      "token": TOKEN,
    };
    await http.post(
      Uri.parse('$API_URL/app_screen_journey'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    // print('screen id $screenId');
    // print(response.body);
  }

  static Future<void> startSession() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      "id_selangkah_user": id,
      "token": TOKEN,
    };

    final response = await http.post(
      Uri.parse('$API_URL/set_start_session'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print('calling start session');
    print(response.body);

    var decoded = json.decode(response.body);
    String sessionId = decoded['Session'];
    GlobalVariables.sessionId = sessionId;
    print('session id is ${GlobalVariables.sessionId}');
  }

  static Future<String> getDeviceId() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor!; // unique ID on iOS
    } else {
      const _androidIdPlugin = AndroidId();

      String? androidId = await _androidIdPlugin.getId();
      return androidId ?? '';
    }
  }
}
