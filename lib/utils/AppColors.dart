import 'package:flutter/material.dart';

// Light Theme Colors

const appColorPrimary = Color(0xFFCF152D);
const app2ndColor = Color(0xFFfcb712);
const appColorPrimaryDark = Color(0xFF0A79DF);
const appColorAccent = Color(0xFF03DAC5);
const appTextColorPrimary = Color(0xFF212121);
const iconColorPrimary = Color(0xFFFFFFFF);
const appTextColorSecondary = Color(0xFF5A5C5E);
const iconColorSecondary = Color(0xFFA8ABAD);
const appLayout_background = Color(0xFFf8f8f8);
const appWhite = Color(0xFFFFFFFF);
const appLight_purple = Color(0xFFdee1ff);
const appLight_orange = Color(0xFFffddd5);
const appLight_parrot_green = Color(0xFFb4ef93);
const appIconTintCherry = Color(0xFFffddd5);
const appIconTint_sky_blue = Color(0xFF73d8d4);
const appIconTint_mustard_yellow = Color(0xFFffc980);
const appIconTintDark_purple = Color(0xFF8998ff);
const appTxtTintDark_purple = Color(0xFF515BBE);
const appIconTintDarkCherry = Color(0xFFf2866c);
const appIconTintDark_sky_blue = Color(0xFF73d8d4);
const appDark_parrot_green = Color(0xFF5BC136);
const appDarkRed = Color(0xFFF06263);
const appLightRed = Color(0xFFF89B9D);
const appCat1 = Color(0xFF8998FE);
const appCat2 = Color(0xFFFF9781);
const appCat3 = Color(0xFF73D7D3);
const appCat4 = Color(0xFF87CEFA);
const appCat5 = appColorPrimary;
const appShadowColor = Color(0x95E9EBF0);
const appColorPrimaryLight = Color(0xFFF9FAFF);
const appSecondaryBackgroundColor = Color(0xFF131d25);
const appDividerColor = Color(0xFFDADADA);
// const appTextBackground = Color(0xFFF5F4F4);

const appTextBackground = Colors.white;
const appMySJBackground = Color(0xFF677485);
const appMySJPrimaryColor = Color(0xFF3983FE);
const appMySJSecondaryColor = Color(0xFF3099FF);
const appMySJTertiaryColor = Color(0xFF008202);
const appSelangkahBackground = Color(0xFFFDDA7F);

// Dark Theme Colors
const appBackgroundColorDark = Color(0xFF131d25);
const cardBackgroundBlackDark = Color(0xFF1D2939);
const color_primary_black = Color(0xFF131d25);
const appColorPrimaryDarkLight = Color(0xFFF9FAFF);
const iconColorPrimaryDark = Color(0xFF212121);
const iconColorSecondaryDark = Color(0xFFA8ABAD);
const appShadowColorDark = Color(0x1A3E3942);
const colorBlack = Color(0xFF000000);

const appTextColorGray = Color(0xFF757575);
const appTextColorGrayBack = Color(0xFFf4f4f4);
const appTextColorGrayhint = Color(0xFFbdbdbd);
const appNotClickButton = Color(0x80ce0e2d);
const appNotClickText = Color(0x50FFFFFF);
const appLineColor = Color(0xFFf5f5f5);
const numberKeyBoardBack = Color(0xFFfff4d8);
const numberKeyBoardForget = Color(0xFF1d0909);
const appColorPrimaryKiple = Color(0xFFce0e2d);
const appNotificationBack = Color(0xffFFFEF2);
const colorMoneyAdd = Color(0xff00c717);
