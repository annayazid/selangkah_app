import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFCE0E2D);
const kPrimaryLightColor = Color(0xFFfa877f);
const backgroundColor = Color(0xFFfff2e5);
const kBackgroundColor = Color(0xffF0F0f0);
