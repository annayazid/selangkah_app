import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/ResetPassword/reset_pass.dart';

part 'reset_pass_state.dart';

class ResetPassCubit extends Cubit<ResetPassState> {
  ResetPassCubit() : super(ResetPassInitial());
  Future<void> resetPass(String phoneNum, String pass) async {
    emit(ResetPassLoading());

    var connectivityResult = await (Connectivity().checkConnectivity());
    // I am connected to a mobile network.
    // I am connected to a wifi network.
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      bool resetpass = await ResetAccRepositories.resetCount(phoneNum, pass);

      if (resetpass) {
        print('sini');
        await AppSplashRepositories.getUserProfile();
        print('sana');
        emit(ResetPassLoaded());
      } else {
        Fluttertoast.showToast(msg: 'passwordfail'.tr());
        emit(ResetPassFailed());
      }
    } else {
      Fluttertoast.showToast(msg: "nointernet".tr());
      emit(ResetPassNoInternet());
    }
  }
}
