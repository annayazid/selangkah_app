part of 'reset_pass_cubit.dart';

@immutable
abstract class ResetPassState {
  const ResetPassState();
}

class ResetPassInitial extends ResetPassState {
  const ResetPassInitial();
}

class ResetPassLoading extends ResetPassState {
  const ResetPassLoading();
}

class ResetPassLoaded extends ResetPassState {
  const ResetPassLoaded();
}

class ResetPassNoInternet extends ResetPassState {
  const ResetPassNoInternet();
}

class ResetPassFailed extends ResetPassState {
  const ResetPassFailed();
}
