import 'dart:convert';

import 'package:selangkah_new/utils/endpoint.dart';
import 'package:http/http.dart' as http;
import 'package:selangkah_new/utils/secure_storage.dart';

import '../../AppSplashScreen/app_splash.dart';

class ResetAccRepositories {
  static Future<bool> resetCount(String phoneNum, String password) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'phonenumber': phoneNum,
      'password': password,
    };

    final response = await http.post(
      Uri.parse('$API_URL/forgot_password_app'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );
    var data = json.decode(response.body);

    if (data['Data'] != false) {
      UserProfile? userProfile = UserProfile(
        id: data['Data'][0],
        selId: data['Data'][1],
        name: data['Data'][2],
        token: data['Data'][3] != null ? data['Data'][3] : '',
        idNo: data['Data'][4] != null ? data['Data'][4] : '',
        language: data['Data'][5] != null ? data['Data'][5] : '',
        citizen: data['Data'][6] != null ? data['Data'][6] : '',
        gender: data['Data'][7] != null ? data['Data'][7] : '',
        dob: data['Data'][8] != null ? data['Data'][8] : '',
        address1: data['Data'][9] != null ? data['Data'][9] : '',
        address2: data['Data'][10] != null ? data['Data'][10] : '',
        postcode: data['Data'][11] != null ? data['Data'][11] : '',
        city: data['Data'][12] != null ? data['Data'][12] : '',
        state: data['Data'][13] != null ? data['Data'][13] : '',
        country: data['Data'][14] != null ? data['Data'][14] : '',
        email: data['Data'][15] != null ? data['Data'][15] : '',
        race: data['Data'][16] != null ? data['Data'][16] : '',
        idType: data['Data'][17] != null ? data['Data'][17] : '',
      );

      SecureStorage secureStorage = SecureStorage();

      if (userProfile.id != null)
        secureStorage.writeSecureData('userId', userProfile.id!);
      if (userProfile.selId != null)
        secureStorage.writeSecureData('userSelId', userProfile.selId!);
      if (userProfile.name != null)
        secureStorage.writeSecureData('userName', userProfile.name!);
      if (userProfile.token != null)
        secureStorage.writeSecureData('userToken', userProfile.token!);
      if (userProfile.idNo != null)
        secureStorage.writeSecureData('userIdNo', userProfile.idNo!);
      if (userProfile.language != null)
        secureStorage.writeSecureData("language", userProfile.language!);
      if (userProfile.citizen != null)
        secureStorage.writeSecureData("citizen", userProfile.citizen!);
      if (userProfile.gender != null)
        secureStorage.writeSecureData('gender', userProfile.gender!);
      if (userProfile.dob != null)
        secureStorage.writeSecureData("dateOfBirth", userProfile.dob!);
      if (userProfile.address1 != null)
        secureStorage.writeSecureData("address1", userProfile.address1!);
      if (userProfile.address2 != null)
        secureStorage.writeSecureData("address2", userProfile.address2!);
      if (userProfile.postcode != null)
        secureStorage.writeSecureData("postcode", userProfile.postcode!);
      if (userProfile.city != null)
        secureStorage.writeSecureData("city", userProfile.city!);
      if (userProfile.state != null)
        secureStorage.writeSecureData("state", userProfile.state!);
      if (userProfile.country != null)
        secureStorage.writeSecureData("country", userProfile.country!);
      if (userProfile.email != null)
        secureStorage.writeSecureData("email", userProfile.email!);
      if (userProfile.race != null)
        secureStorage.writeSecureData("race", userProfile.race!);
      if (userProfile.idType != null)
        secureStorage.writeSecureData("userIdType", userProfile.idType!);

      return true;
    } else {
      return false;
    }
  }
}
