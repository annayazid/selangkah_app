import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/ForgotPassword/forgot_pass.dart';
import 'package:selangkah_new/Screens/ResetPassword/reset_pass.dart';
import 'package:selangkah_new/Screens/SelangkahAppBarScaffold/scaffold.dart';
import 'package:selangkah_new/Screens/SignIn/sign_in.dart';
import 'package:selangkah_new/utils/constants.dart';

class ResetPasswordPage extends StatefulWidget {
  final String phoneNum;

  const ResetPasswordPage({super.key, required this.phoneNum});

  @override
  State<ResetPasswordPage> createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  final TextEditingController _password = TextEditingController();
  final TextEditingController _passwordrepeat = TextEditingController();

  bool passwordVisibleOff = true;
  bool passwordRepeatVisibleOff = true;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top -
        MediaQuery.of(context).padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return TopBarScaffold(
        title: 'Forgot Password',
        content: SingleChildScrollView(
          child: Center(
            child: Container(
              width: width * 0.9,
              padding: EdgeInsets.symmetric(vertical: 25),
              height: height,
              child: Form(
                key: _formKey,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'forgotPassword'.tr().toString(),
                      style:
                          TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 15),
                    Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        // width: width * 0.8,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(width: 1, color: Colors.grey),
                        ),
                        child: TextFormField(
                          controller: _password,
                          keyboardType: TextInputType.visiblePassword,
                          obscureText: passwordVisibleOff,
                          validator: validatePassword,
                          cursorColor: kPrimaryColor,
                          decoration: InputDecoration(
                            errorMaxLines: 4,
                            icon: Icon(
                              Icons.lock,
                              color: kPrimaryColor,
                            ),
                            hintText: 'new_password'.tr(),
                            border: InputBorder.none,
                            suffixIcon: IconButton(
                              icon: Icon(
                                // Based on passwordVisible state choose the icon
                                passwordVisibleOff
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: Theme.of(context).primaryColorDark,
                              ),
                              onPressed: () {
                                // Update the state i.e. toogle the state of passwordVisible variable
                                setState(() {
                                  passwordVisibleOff = !passwordVisibleOff;
                                });
                              },
                            ),
                          ),
                        )),
                    SizedBox(height: 15),
                    Container(
                        // margin: EdgeInsets.symmetric(vertical: 10),
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        // width: width * 0.8,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(width: 1, color: Colors.grey),
                        ),
                        child: TextFormField(
                          controller: _passwordrepeat,
                          keyboardType: TextInputType.visiblePassword,
                          obscureText: passwordRepeatVisibleOff,
                          validator: validatePasswordRepeat,
                          cursorColor: kPrimaryColor,
                          decoration: InputDecoration(
                            errorMaxLines: 4,
                            icon: Icon(
                              Icons.lock,
                              color: kPrimaryColor,
                            ),
                            hintText: 'retype_new_password'.tr(),
                            border: InputBorder.none,
                            suffixIcon: IconButton(
                              icon: Icon(
                                // Based on passwordVisible state choose the icon
                                passwordRepeatVisibleOff
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: Theme.of(context).primaryColorDark,
                              ),
                              onPressed: () {
                                // Update the state i.e. toogle the state of passwordVisible variable
                                setState(() {
                                  passwordRepeatVisibleOff =
                                      !passwordRepeatVisibleOff;
                                });
                              },
                            ),
                          ),
                        )),
                    Spacer(),
                    Text(
                      'invalid_password_format'.tr(),
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.red),
                    ),
                    SizedBox(height: 20),
                    Container(
                      width: width * 0.9,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          backgroundColor: kPrimaryColor,
                        ),
                        onPressed: () {
                          final isValid = _formKey.currentState!.validate();
                          if (!isValid) {
                            print(_password);
                            print(_passwordrepeat);
                            return;
                          } else {
                            _formKey.currentState!.save();

                            if (_password.text == _passwordrepeat.text) {
                              context.read<ResetPassCubit>().resetPass(
                                    widget.phoneNum,
                                    _password.text,
                                  );
                            } else {
                              Fluttertoast.showToast(
                                  msg: 'passwordunmatched'.tr());
                            }
                          }
                        },
                        child: BlocConsumer<ResetPassCubit, ResetPassState>(
                          listener: (context, state) {
                            print(state.toString());
                            if (state is ResetPassLoaded) {
                              // Navigator.of(context).push(
                              //   MaterialPageRoute(
                              //     builder: (context) => HomePageScreen(),
                              //   ),
                              // );

                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => MultiBlocProvider(
                                      providers: [
                                        BlocProvider(
                                          create: (context) =>
                                              PrivacyTermCubit(),
                                        ),
                                        BlocProvider(
                                          create: (context) => CheckUserCubit(),
                                        ),
                                      ],
                                      child: SignInScreen(),
                                    ),
                                  ),
                                  ModalRoute.withName("/Home"));

                              // Navigator.of(context).pushReplacement(
                              //   MaterialPageRoute(
                              //     builder: (context) => MultiBlocProvider(
                              //       providers: [
                              //         BlocProvider(
                              //           create: (context) => PrivacyTermCubit(),
                              //         ),
                              //         BlocProvider(
                              //           create: (context) => CheckUserCubit(),
                              //         ),
                              //       ],
                              //       child: SignInScreen(),
                              //     ),
                              //   ),
                              // );

                              Fluttertoast.showToast(
                                msg: 'succes_reset_pass_login'.tr(),
                                toastLength: Toast.LENGTH_LONG,
                                gravity: ToastGravity.BOTTOM,
                              );
                            }
                          },
                          builder: (context, state) {
                            if (state is ForgotAccLoading) {
                              return Padding(
                                padding: EdgeInsets.all(18.0),
                                child: SpinKitFadingCircle(
                                  color: Colors.white,
                                  size: 18,
                                ),
                              );
                            } else {
                              return Container(
                                child: Center(
                                  child: Padding(
                                    padding: EdgeInsets.all(18.0),
                                    child: Text(
                                      'next_forgot_password'.tr(),
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16),
                                      // textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              );
                            }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  String? validatePassword(String? value) {
    //regex for the password matches (8 length alphanumeric with number)
    // RegExp reg = RegExp(r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$');
    RegExp reg = RegExp(r'^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$');

    if (value!.length == 0) {
      return 'passwordrequired'.tr();
    }

    if (!reg.hasMatch(value)) {
      return 'invalid_password_format'.tr();
    }
    return null;
  }

  String? validatePasswordRepeat(String? value) {
    //regex for the password matches (8 length alphanumeric with number)
    // RegExp reg = RegExp(r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$');
    RegExp reg = RegExp(r'^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$');

    if (value!.length == 0) {
      return 'passwordrequired'.tr();
    }

    if (!reg.hasMatch(value)) {
      return 'invalid_password_format'.tr();
    }
    return null;
  }
}
