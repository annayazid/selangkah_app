// To parse this JSON data, do
//
//     final faqCategory = faqCategoryFromJson(jsonString);

import 'dart:convert';

FaqCategory faqCategoryFromJson(String str) =>
    FaqCategory.fromJson(json.decode(str));

String faqCategoryToJson(FaqCategory data) => json.encode(data.toJson());

class FaqCategory {
  FaqCategory({
    this.code,
    this.data,
  });

  int? code;
  List<FaqCategoryData>? data;

  factory FaqCategory.fromJson(Map<String, dynamic> json) => FaqCategory(
        code: json["Code"],
        data: List<FaqCategoryData>.from(
            json["Data"].map((x) => FaqCategoryData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class FaqCategoryData {
  FaqCategoryData({
    this.id,
    this.categoryName,
    this.categoryImage,
  });

  String? id;
  String? categoryName;
  String? categoryImage;

  factory FaqCategoryData.fromJson(Map<String, dynamic> json) =>
      FaqCategoryData(
        id: json["id"],
        categoryName: json["category_name"],
        categoryImage: json["category_image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "category_name": categoryName,
        "category_image": categoryImage,
      };
}
