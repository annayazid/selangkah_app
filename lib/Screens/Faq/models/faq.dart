// To parse this JSON data, do
//
//     final faq = faqFromJson(jsonString);

import 'dart:convert';

Faq faqFromJson(String str) => Faq.fromJson(json.decode(str));

String faqToJson(Faq data) => json.encode(data.toJson());

class Faq {
  Faq({
    this.code,
    this.data,
  });

  int? code;
  List<Datum>? data;

  factory Faq.fromJson(Map<String, dynamic> json) => Faq(
        code: json["Code"],
        data: List<Datum>.from(json["Data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.date,
    this.faqName,
    this.faqAnswer,
    this.faqCategory,
    this.url,
    this.image,
    this.homeImage,
    this.displayOrder,
  });

  String? date;
  String? faqName;
  String? faqAnswer;
  String? faqCategory;
  String? url;
  String? image;
  String? homeImage;
  String? displayOrder;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        date: json["date"],
        faqName: json["faq_name"],
        faqAnswer: json["faq_answer"],
        faqCategory: json["faq_category"],
        url: json["url"],
        image: json["image"],
        homeImage: json["home_image"],
        displayOrder: json["display_order"],
      );

  Map<String, dynamic> toJson() => {
        "date": date,
        "faq_name": faqName,
        "faq_answer": faqAnswer,
        "faq_category": faqCategory,
        "url": url,
        "image": image,
        "home_image": homeImage,
        "display_order": displayOrder,
      };
}
