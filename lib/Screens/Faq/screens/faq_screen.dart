import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:expandable/expandable.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:selangkah_new/Screens/Faq/faq.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/WebView/webview.dart';
import 'package:selangkah_new/utils/AppColors.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

import 'package:url_launcher/url_launcher.dart';

class FaqScreen extends StatefulWidget {
  @override
  _FaqScreenState createState() => _FaqScreenState();
}

class _FaqScreenState extends State<FaqScreen> {
  @override
  void initState() {
    getFaq();
    super.initState();
  }

  void getFaq() async {
    final SecureStorage secureStorage = SecureStorage();

    String language = await secureStorage.readSecureData('language');

    context.read<GetFaqCubit>().getFaq(language);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/images/selangkah_logo.png",
          width: size.width * 0.45,
        ),
        //automaticallyImplyLeading: true,
        leading: InkWell(
          // splashColor: kPrimaryColor,
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_ios_new,
            color: kPrimaryColor,
          ),
        ),
      ),
      body: BlocConsumer<GetFaqCubit, GetFaqState>(
        listener: (context, state) {},
        builder: (context, state) {
          if (state is GetFaqLoading) {
            return FaqLoadingWidget();
          } else if (state is GetFaqLoaded) {
            return FaqLoadedWidget(
              faq: state.faq,
              faqCategory: state.faqCategory,
              config: state.config,
            );
          } else {
            return FaqLoadingWidget();
          }
        },
      ),
    );
  }
}

class FaqLoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SpinKitFadingCircle(
      color: kPrimaryColor,
      size: 35,
    );
  }
}

class FaqLoadedWidget extends StatefulWidget {
  final Faq faq;
  final FaqCategory faqCategory;
  final Config config;

  const FaqLoadedWidget({
    Key? key,
    required this.faq,
    required this.faqCategory,
    required this.config,
  }) : super(key: key);

  @override
  _FaqLoadedWidgetState createState() => _FaqLoadedWidgetState();
}

class _FaqLoadedWidgetState extends State<FaqLoadedWidget> {
  Faq? faq;
  FaqCategory? faqCategory;
  Config? config;

  String? categoryName;
  String? categoryId;

  @override
  void initState() {
    faq = widget.faq;
    faqCategory = widget.faqCategory;
    config = widget.config;
    categoryName = faqCategory!.data![0].categoryName!;
    categoryId = faqCategory!.data![0].id!;
    super.initState();
  }

  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // return Container(
    //   height: size.height,
    //   padding: EdgeInsets.all(10),
    //   child: SingleChildScrollView(
    //     child: Column(
    //       children: [
    //         SizedBox(height: 10),
    //         Text(
    //           'Frequently asked question',
    //           style: TextStyle(
    //             fontSize: 25,
    //             fontWeight: FontWeight.bold,
    //           ),
    //         ),
    //         SizedBox(height: 30),
    //         ListView.builder(
    //           physics: NeverScrollableScrollPhysics(),
    //           shrinkWrap: true,
    //           scrollDirection: Axis.vertical,
    //           itemBuilder: (context, index) {
    //             return ExpandablePanel(
    //               theme: ExpandableThemeData(
    //                 headerAlignment: ExpandablePanelHeaderAlignment.center,
    //               ),
    //               header: Text(discover.data[index].discoverName),
    //               expanded: Text(
    //                 'Answer here',
    //                 softWrap: true,
    //               ),
    //             );
    //           },
    //           itemCount: discover.data.length,
    //         ),
    //         SizedBox(height: 50),
    //         Container(
    //           width: size.width,
    //           child: RaisedButton(
    //             onPressed: () {
    //               launch('https://t.me/Selangkah_my');
    //               //
    //             },
    //             child: Text('Contact us thru telegram'),
    //           ),
    //         )
    //       ],
    //     ),
    //   ),
    // );
    return AnimatedSwitcher(
      duration: Duration(milliseconds: 500),
      child: Container(
        height: size.height,
        padding: EdgeInsets.all(10),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 10),
              Text(
                'faq'.tr(),
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 15),
              // Row(
              //   children: [
              //     Expanded(
              //       child: GestureDetector(
              //         onTap: () {
              //           setState(() {
              //             index = 0;
              //             category = 'Screening';
              //           });
              //         },
              //         child: Container(
              //           padding: EdgeInsets.all(10),
              //           decoration: BoxDecoration(
              //               borderRadius: BorderRadius.circular(20),
              //               color:
              //                   index != 0 ? Colors.grey.shade300 : app2ndColor),
              //           child: Column(
              //             children: [
              //               IconButton(
              //                 onPressed: () {
              //                   setState(() {
              //                     index = 0;
              //                     category = 'Screening';
              //                   });
              //                 },
              //                 icon: Icon(FontAwesomeIcons.tools,
              //                     color: kPrimaryColor),
              //               ),
              //               Text('Screening'),
              //             ],
              //           ),
              //         ),
              //       ),
              //     ),
              //     SizedBox(width: 10),
              //     Expanded(
              //       child: Container(
              //         padding: EdgeInsets.all(10),
              //         decoration: BoxDecoration(
              //             borderRadius: BorderRadius.circular(20),
              //             color: index != 1 ? Colors.grey.shade300 : app2ndColor),
              //         child: Column(
              //           children: [
              //             IconButton(
              //               onPressed: () {
              //                 setState(() {
              //                   index = 1;
              //                   category = 'CAC';
              //                 });
              //               },
              //               icon: Icon(FontAwesomeIcons.newspaper,
              //                   color: kPrimaryColor),
              //             ),
              //             Text('CAC'),
              //           ],
              //         ),
              //       ),
              //     ),
              //     SizedBox(width: 10),
              //     Expanded(
              //       child: Container(
              //         padding: EdgeInsets.all(10),
              //         decoration: BoxDecoration(
              //             borderRadius: BorderRadius.circular(20),
              //             color: index != 2 ? Colors.grey.shade300 : app2ndColor),
              //         child: Column(
              //           children: [
              //             IconButton(
              //               onPressed: () {
              //                 setState(() {
              //                   index = 2;
              //                   category = 'Vaccine';
              //                 });
              //               },
              //               icon: Icon(FontAwesomeIcons.syringe,
              //                   color: kPrimaryColor),
              //             ),
              //             Text('Vaccine'),
              //           ],
              //         ),
              //       ),
              //     ),
              //   ],
              // ),
              // Row(
              //   children: [],
              // ),
              GridView.builder(
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                ),
                itemCount: faqCategory!.data!.length,
                itemBuilder: (BuildContext ctx, index) {
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        selectedIndex = index;
                        categoryName = faqCategory!.data![index].categoryName;
                        categoryId = faqCategory!.data![index].id;
                      });
                    },
                    child: Container(
                      height: 50,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: () {
                          if (selectedIndex == index) {
                            return app2ndColor;
                          } else {
                            return Colors.grey.shade300;
                          }
                        }(),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            child: AspectRatio(
                              aspectRatio: 1 / 1,
                              child: CachedNetworkImage(
                                imageUrl:
                                    faqCategory!.data![index].categoryImage!,
                              ),
                            ),
                          ),
                          SizedBox(height: 10),
                          Text(
                            faqCategory!.data![index].categoryName!,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
              SizedBox(height: 35),
              QuestionList(
                faq: faq!,
                category: categoryName!,
                categoryId: categoryId!,
              ),
              SizedBox(height: 50),
              Text(
                'still_need_help'.tr(),
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(
                width: size.width * 0.80,
                child: ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    backgroundColor: Color(0xFF2CA5E0),
                  ),
                  onPressed: () {
                    // launch(config.data[0].telegramLink);

                    launchUrl(
                      Uri.parse(config!.data![0].telegramLink!),
                      mode: LaunchMode.externalApplication,
                    );
                  },
                  label: Text(
                    'contactus'.tr(),
                    style: TextStyle(color: Colors.white),
                  ),
                  icon: Icon(FontAwesomeIcons.telegram, color: Colors.white),
                ),
              ),
              Container(
                width: size.width * 0.80,
                child: ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    backgroundColor: Colors.amber,
                  ),
                  onPressed: () {
                    launchUrl(
                      Uri.parse('email_cs'.tr()),
                      mode: LaunchMode.externalApplication,
                    );
                  },
                  label: Text(
                    'contact_us_email'.tr(),
                    style: TextStyle(color: Colors.white),
                  ),
                  icon: Icon(
                    Icons.email,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class QuestionList extends StatelessWidget {
  final Faq faq;
  final String category;
  final String categoryId;

  const QuestionList(
      {Key? key,
      required this.faq,
      required this.category,
      required this.categoryId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      itemBuilder: (context, index) {
        return Column(
          children: [
            () {
              if (faq.data![index].faqCategory == categoryId) {
                return Container(
                  padding: EdgeInsets.all(5),
                  child: Column(
                    children: [
                      ExpandablePanel(
                        collapsed: Container(),
                        theme: ExpandableThemeData(
                          headerAlignment:
                              ExpandablePanelHeaderAlignment.center,
                        ),
                        header: Text(
                          faq.data![index].faqName!,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        expanded: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              faq.data![index].faqAnswer!,
                              softWrap: true,
                            ),
                            SizedBox(height: 10),
                            () {
                              if (faq.data![index].url == "" ||
                                  faq.data![index].url == "null" ||
                                  faq.data![index].url == null) {
                                return Container();
                              } else {
                                return Linkify(
                                  text: '${faq.data![index].url}',
                                  options: LinkifyOptions(humanize: false),
                                  onOpen: (link) {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => SelangkahWebview(
                                          appBarTitle:
                                              faq.data![index].faqName!,
                                          url: faq.data![index].url!,
                                        ),
                                        settings:
                                            RouteSettings(name: 'FAQ webpage'),
                                      ),
                                    );
                                  },
                                );
                              }
                            }()
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              } else {
                return Container();
              }
            }(),
          ],
        );
      },
      itemCount: faq.data!.length,
    );
  }
}
