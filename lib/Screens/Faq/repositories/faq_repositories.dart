import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/Faq/faq.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/utils/endpoint.dart';

class FaqRepositories {
  static Future<Faq> getFaq(String language) async {
    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
    };

    // print('calling get faq');
    final response = await http.post(
      Uri.parse('$API_URL/cust_faq'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    Faq faq = faqFromJson(response.body);

    return faq;
  }

  static Future<FaqCategory> getFaqCategory(String language) async {
    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
    };

    // print('calling get faq category');

    final response = await http.post(
      Uri.parse('$API_URL/cust_faq_category'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    FaqCategory faqCategory = faqCategoryFromJson(response.body);

    return faqCategory;
  }

  static Future<Config> getConfig() async {
    final Map<String, String> map = {
      'dataset': 'get_app_config',
      'token': TOKEN,
    };

    // print('calling get config');
    final response = await http.post(
      Uri.parse('$API_URL/get_app_config'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    Config config = configFromJson(response.body);

    return config;
  }
}
