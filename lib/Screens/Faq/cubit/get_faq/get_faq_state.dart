part of 'get_faq_cubit.dart';

@immutable
abstract class GetFaqState {
  const GetFaqState();
}

class GetFaqInitial extends GetFaqState {
  const GetFaqInitial();
}

class GetFaqLoading extends GetFaqState {
  const GetFaqLoading();
}

class GetFaqLoaded extends GetFaqState {
  final Faq faq;
  final FaqCategory faqCategory;
  final Config config;

  GetFaqLoaded(this.faq, this.faqCategory, this.config);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetFaqLoaded &&
        other.faq == faq &&
        other.faqCategory == faqCategory &&
        other.config == config;
  }

  @override
  int get hashCode => faq.hashCode ^ faqCategory.hashCode ^ config.hashCode;
}
