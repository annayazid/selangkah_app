import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Faq/faq.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/utils/global.dart';

part 'get_faq_state.dart';

class GetFaqCubit extends Cubit<GetFaqState> {
  GetFaqCubit() : super(GetFaqInitial());

  Future<void> getFaq(String language) async {
    emit(GetFaqLoading());

    await GlobalFunction.screenJourney('25');

    FaqCategory faqCategory = await FaqRepositories.getFaqCategory(language);
    Faq faq = await FaqRepositories.getFaq(language);
    Config config = await FaqRepositories.getConfig();

    emit(GetFaqLoaded(faq, faqCategory, config));
  }
}
