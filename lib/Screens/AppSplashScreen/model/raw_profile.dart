// To parse this JSON data, do
//
//     final rawProfile = rawProfileFromJson(jsonString);

import 'dart:convert';

RawProfile? rawProfileFromJson(String str) =>
    RawProfile.fromJson(json.decode(str));

String rawProfileToJson(RawProfile? data) => json.encode(data!.toJson());

class RawProfile {
  RawProfile({
    this.code,
    this.data,
    this.deviceId,
  });

  int? code;
  List<String?>? data;
  DeviceId? deviceId;

  factory RawProfile.fromJson(Map<String, dynamic> json) => RawProfile(
      code: json["Code"],
      data: json["Data"] == null
          ? []
          : json["Data"] == null
              ? []
              : List<String?>.from(json["Data"]!.map((x) => x)),
      deviceId: DeviceId.fromJson(json["device_id"]));

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : data == null
                ? []
                : List<dynamic>.from(data!.map((x) => x)),
        "device_id": deviceId,
      };
}

class DeviceId {
  DeviceId({
    this.deviceId,
  });

  String? deviceId;

  factory DeviceId.fromJson(Map<String, dynamic> json) => DeviceId(
        deviceId: json["device_id"],
      );

  Map<String, dynamic> toJson() => {
        "device_id": deviceId,
      };
}
