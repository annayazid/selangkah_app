class UserProfile {
  final String? id;
  final String? selId;
  final String? name;
  final String? token;
  final String? idNo;
  final String? language;
  final String? citizen;
  final String? gender;
  final String? dob;
  final String? address1;
  final String? address2;
  final String? postcode;
  final String? city;
  final String? state;
  final String? country;
  final String? email;
  final String? race;
  final String? idType;
  final String? profilePic;
  final String? phoneNo;
  final String? ekycStatus;

  UserProfile({
    this.id,
    this.selId,
    this.name,
    this.token,
    this.idNo,
    this.language,
    this.citizen,
    this.gender,
    this.dob,
    this.address1,
    this.address2,
    this.postcode,
    this.city,
    this.state,
    this.country,
    this.email,
    this.race,
    this.idType,
    this.profilePic,
    this.phoneNo,
    this.ekycStatus,
  });

  UserProfile copyWith({
    String? id,
    String? selId,
    String? name,
    String? token,
    String? idNo,
    String? language,
    String? citizen,
    String? gender,
    String? dob,
    String? address1,
    String? address2,
    String? postcode,
    String? city,
    String? state,
    String? country,
    String? email,
    String? race,
    String? idType,
    String? profilePic,
    String? phoneNo,
    String? ekycStatus,
  }) {
    return UserProfile(
      id: id ?? this.id,
      selId: selId ?? this.selId,
      name: name ?? this.name,
      token: token ?? this.token,
      idNo: idNo ?? this.idNo,
      language: language ?? this.language,
      citizen: citizen ?? this.citizen,
      gender: gender ?? this.gender,
      dob: dob ?? this.dob,
      address1: address1 ?? this.address1,
      address2: address2 ?? this.address2,
      postcode: postcode ?? this.postcode,
      city: city ?? this.city,
      state: state ?? this.state,
      country: country ?? this.country,
      email: email ?? this.email,
      race: race ?? this.race,
      idType: idType ?? this.idType,
      profilePic: profilePic ?? this.profilePic,
      phoneNo: phoneNo ?? this.phoneNo,
      ekycStatus: ekycStatus ?? this.ekycStatus,
    );
  }
}
