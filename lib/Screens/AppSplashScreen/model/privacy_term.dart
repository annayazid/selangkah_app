// To parse this JSON data, do
//
//     final privacyTerm = privacyTermFromJson(jsonString);

import 'dart:convert';

PrivacyTerm privacyTermFromJson(String str) =>
    PrivacyTerm.fromJson(json.decode(str));

String privacyTermToJson(PrivacyTerm data) => json.encode(data.toJson());

class PrivacyTerm {
  int? code;
  List<Datum>? data;

  PrivacyTerm({
    this.code,
    this.data,
  });

  factory PrivacyTerm.fromJson(Map<String, dynamic> json) => PrivacyTerm(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<Datum>.from(json["Data"]!.map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  String? termUse;
  String? privacyPolicy;
  String? privacyPolicyBm;

  Datum({
    this.termUse,
    this.privacyPolicy,
    this.privacyPolicyBm,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        termUse: json["term_use"],
        privacyPolicy: json["privacy_policy"],
        privacyPolicyBm: json["privacy_policy_bm"],
      );

  Map<String, dynamic> toJson() => {
        "term_use": termUse,
        "privacy_policy": privacyPolicy,
        "privacy_policy_bm": privacyPolicyBm,
      };
}
