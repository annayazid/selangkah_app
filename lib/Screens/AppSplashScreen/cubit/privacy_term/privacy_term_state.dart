part of 'privacy_term_cubit.dart';

@immutable
abstract class PrivacyTermState {
  const PrivacyTermState();
}

class PrivacyTermInitial extends PrivacyTermState {
  const PrivacyTermInitial();
}

class PrivacyTermLoading extends PrivacyTermState {
  const PrivacyTermLoading();
}

class PrivacyTermLoaded extends PrivacyTermState {
  const PrivacyTermLoaded();
}
