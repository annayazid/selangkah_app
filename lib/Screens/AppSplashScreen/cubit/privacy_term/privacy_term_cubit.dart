import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';

import 'dart:io' show File, HttpClient;

import 'package:selangkah_new/utils/global.dart';

part 'privacy_term_state.dart';

class PrivacyTermCubit extends Cubit<PrivacyTermState> {
  PrivacyTermCubit() : super(PrivacyTermInitial());

  // Future<void> getPrivacyTerm();

  Future<void> getPrivacyTerm(String language) async {
    emit(PrivacyTermLoading());

    PrivacyTerm privacyTerm = await AppSplashRepositories.getPrivacyTerm();

    print('language term is $language');

    GlobalVariables.termsofuse = privacyTerm.data![0].termUse;
    GlobalVariables.privacy = language == 'EN'
        ? privacyTerm.data![0].privacyPolicy
        : privacyTerm.data![0].privacyPolicyBm;

    print(GlobalVariables.privacy);

    createFileOfPdfUrl(privacyTerm.data![0].termUse!).then((f) {
      GlobalVariables.termsofuse = f.path;
    });

    createFileOfPdfUrl(language == 'EN'
            ? privacyTerm.data![0].privacyPolicy!
            : privacyTerm.data![0].privacyPolicyBm!)
        .then((f) {
      GlobalVariables.privacy = f.path;
    });

    // emit(PrivacyTermLoaded());
  }

  Future<File> createFileOfPdfUrl(String url) async {
    Completer<File> completer = Completer();
    //print("Start download file from internet!");
    try {
      final fileurl = url;
      final filename = fileurl.substring(fileurl.lastIndexOf("/") + 1);
      var request = await HttpClient().getUrl(Uri.parse(fileurl));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      var dir = await getApplicationDocumentsDirectory();
      //print("Download files");
      //print("${dir.path}/$filename");
      File file = File("${dir.path}/$filename");

      await file.writeAsBytes(bytes, flush: true);
      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }

    return completer.future;
  }
}
