part of 'user_profile_cubit.dart';

@immutable
abstract class UserProfileState {
  const UserProfileState();
}

class UserProfileInitial extends UserProfileState {
  const UserProfileInitial();
}

class UserFirstTime extends UserProfileState {
  const UserFirstTime();
}

class UserSignedIn extends UserProfileState {
  final UserProfile? userProfile;

  UserSignedIn(this.userProfile);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UserSignedIn && other.userProfile == userProfile;
  }

  @override
  int get hashCode => userProfile.hashCode;
}
