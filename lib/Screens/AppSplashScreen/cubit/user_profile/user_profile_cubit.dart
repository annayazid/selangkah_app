import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/utils/secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'user_profile_state.dart';

class UserProfileCubit extends Cubit<UserProfileState> {
  UserProfileCubit() : super(UserProfileInitial());

  Future<void> getUserProfile() async {
    getAccessTokenFromDevice();
    SecureStorage secureStorage = SecureStorage();
    String? id = await secureStorage.readSecureData("userId");
    String? phoneNo = await secureStorage.readSecureData("userPhoneNo");

    //cache data akan kosong kalau user logout
    //cache data akan kosong bila uninstall kt android. If uninstall kat ios, cache data still ada
    final prefs = await SharedPreferences.getInstance();
    print('id is $id');
    print('phoneNo is $phoneNo');
    if (id != null && phoneNo != null) {
      //sharedpreference akan hilang kat ios if user uninstall

      print('prefs userFirstTime');
      print(prefs.getBool('userFirst'));

      //cache ada data untuk iOS tapi user first time, so checking first time dalam if cache data
      if (prefs.getBool('userFirst') ?? true) {
        print('asuk 1');
        SecureStorage secureStorage = SecureStorage();
        await secureStorage.deleteAllSecureData();
        prefs.setBool('userFirst', false);
        // if (Platform.localeName.split('_')[0] == 'ms') {
        //   context.setLocale(Locale('ms', 'MY'));
        // } else {
        //   context.setLocale(Locale('en', 'US'));
        // }
        emit(UserFirstTime());
      } else {
        print('asuk 2');

        await prefs.setBool('userFirst', false);
        // _loadUser(id);

        UserProfile? userProfile = await AppSplashRepositories.getUserProfile();

        await AppSplashRepositories.updateUserToken();

        emit(UserSignedIn(userProfile));
      }
    } else {
      print('asuk 3');
      prefs.setBool('userFirst', false);
      emit(UserFirstTime());
    }
  }

  static Future<String?> getAccessTokenFromDevice() async {
    const _storage = FlutterSecureStorage();
    try {
      final accessToken = await _storage.read(key: 'accessToken');
      print('access token is');
      print(accessToken);
      return accessToken;
    } catch (e) {
      // Workaround for https://github.com/mogol/flutter_secure_storage/issues/43
      await _storage.deleteAll();

      return null;
    }
  }
}
