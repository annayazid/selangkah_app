import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
// import 'package:selangkah_new/Screens/SignIn/sign_in.dart';
import 'package:selangkah_new/Screens/WalkthroughPage/walkthrough.dart';
import 'package:selangkah_new/utils/global.dart';

class AppSplashScreen extends StatefulWidget {
  const AppSplashScreen({Key? key}) : super(key: key);

  @override
  State<AppSplashScreen> createState() => _AppSplashScreenState();
}

class _AppSplashScreenState extends State<AppSplashScreen> {
  @override
  void initState() {
    context.read<UserProfileCubit>().getUserProfile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<UserProfileCubit, UserProfileState>(
        listener: (context, state) async {
          print(state.toString());
          if (state is UserFirstTime) {
            await Future.delayed(Duration(seconds: 1));
            WalkthroughPage().launch(context, isNewTask: true);
          }

          if (state is UserSignedIn) {
            // SignInScreen().launch(context, isNewTask: true);
            // Navigator.of(context).pushReplacement(
            //   MaterialPageRoute(
            //     builder: (context) => MultiBlocProvider(
            //       providers: [
            //         BlocProvider(
            //           create: (context) => PrivacyTermCubit(),
            //         ),
            //         BlocProvider(
            //           create: (context) => CheckUserCubit(),
            //         ),
            //       ],
            //       child: SignInScreen(),
            //     ),
            //   ),
            // );

            String language =
                EasyLocalization.of(context)!.locale.languageCode == 'en'
                    ? 'EN'
                    : 'MY';

            GlobalVariables.language = language;

            context.read<PrivacyTermCubit>().getPrivacyTerm(language);

            BlocProvider(
              create: (context) => GetHomepageDataCubit(),
              child: HomePageScreen(),
            ).launch(
              context,
              isNewTask: true,
            );
          }
        },
        builder: (context, state) {
          return Container(
            alignment: Alignment.center,
            child: Image.asset(
              'assets/images/AppSplash/selangkah_icon.png',
              width: 50,
              fit: BoxFit.fitHeight,
            ),
          );
        },
      ),
    );
  }
}
