import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:http/http.dart' as http;
import 'package:selangkah_new/utils/secure_storage.dart';

class AppSplashRepositories {
  static Future<PrivacyTerm> getPrivacyTerm() async {
    final Map<String, String> map = {
      'token': TOKEN,
    };

    final response = await http.post(
      Uri.parse('$API_URL/get_privacy_term'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    PrivacyTerm privacyTerm = privacyTermFromJson(response.body);
    return privacyTerm;
  }

  static Future<void> updateUserToken() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    String? token = await FirebaseMessaging.instance.getToken();
    print('token is $token');
    // final fcmToken = await FirebaseMessaging.instance.getToken();

    final Map<String, String> map = {
      'token': TOKEN,
      'fcm': token!,
      'id': id,
    };

    final response = await http.post(
      Uri.parse('$API_URL/update_user_fcm'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }

  static Future<UserProfile?> getUserProfile() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');
    print(id);

    final Map<String, String> map = {
      'token': TOKEN,
      'id': id,
      // 'id': '82986',
    };

    final response = await http.post(
      Uri.parse('$API_URL/check_user_profile'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    // log(response.body);
    print(response.body
        .substring(response.body.length - 50, response.body.length));

    RawProfile? rawProfile = rawProfileFromJson(response.body);

    UserProfile? userProfile = UserProfile(
      id: rawProfile!.data![0],
      selId: rawProfile.data![1],
      name: rawProfile.data![2],
      token: rawProfile.data![3] != null ? rawProfile.data![3] : '',
      idNo: rawProfile.data![4] != null ? rawProfile.data![4] : '',
      language: rawProfile.data![5] != null ? rawProfile.data![5] : '',
      citizen: rawProfile.data![6] != null ? rawProfile.data![6] : '',
      gender: rawProfile.data![7] != null ? rawProfile.data![7] : '',
      dob: rawProfile.data![8] != null ? rawProfile.data![8] : '',
      address1: rawProfile.data![9] != null ? rawProfile.data![9] : '',
      address2: rawProfile.data![10] != null ? rawProfile.data![10] : '',
      postcode: rawProfile.data![11] != null ? rawProfile.data![11] : '',
      city: rawProfile.data![12] != null ? rawProfile.data![12] : '',
      state: rawProfile.data![13] != null ? rawProfile.data![13] : '',
      country: rawProfile.data![14] != null ? rawProfile.data![14] : '',
      email: rawProfile.data![15] != null ? rawProfile.data![15] : '',
      race: rawProfile.data![16] != null ? rawProfile.data![16] : '',
      idType: rawProfile.data![17] != null ? rawProfile.data![17] : '',
      profilePic: rawProfile.data![18] != null ? rawProfile.data![18] : '',
      phoneNo: rawProfile.data![19] != null ? rawProfile.data![19] : '',
      ekycStatus: rawProfile.data![20],
    );

    if (userProfile.id != null)
      secureStorage.writeSecureData('userId', userProfile.id!);
    if (userProfile.selId != null)
      secureStorage.writeSecureData('userSelId', userProfile.selId!);
    if (userProfile.name != null)
      secureStorage.writeSecureData('userName', userProfile.name!);
    if (userProfile.token != null)
      secureStorage.writeSecureData('userToken', userProfile.token!);
    if (userProfile.idNo != null)
      secureStorage.writeSecureData('userIdNo', userProfile.idNo!);
    if (userProfile.language != null)
      secureStorage.writeSecureData("language", userProfile.language!);
    if (userProfile.citizen != null)
      secureStorage.writeSecureData("citizen", userProfile.citizen!);
    if (userProfile.gender != null)
      secureStorage.writeSecureData('gender', userProfile.gender!);
    if (userProfile.dob != null)
      secureStorage.writeSecureData("dateOfBirth", userProfile.dob!);
    if (userProfile.address1 != null)
      secureStorage.writeSecureData("address1", userProfile.address1!);
    if (userProfile.address2 != null)
      secureStorage.writeSecureData("address2", userProfile.address2!);
    if (userProfile.postcode != null)
      secureStorage.writeSecureData("postcode", userProfile.postcode!);
    if (userProfile.city != null)
      secureStorage.writeSecureData("city", userProfile.city!);
    if (userProfile.state != null)
      secureStorage.writeSecureData("state", userProfile.state!);
    if (userProfile.country != null)
      secureStorage.writeSecureData("country", userProfile.country!);
    if (userProfile.email != null)
      secureStorage.writeSecureData("email", userProfile.email!);
    if (userProfile.race != null)
      secureStorage.writeSecureData("race", userProfile.race!);
    if (userProfile.idType != null)
      secureStorage.writeSecureData("userIdType", userProfile.idType!);
    if (userProfile.profilePic != null)
      secureStorage.writeSecureData("profilePicture", userProfile.profilePic!);
    if (userProfile.phoneNo != null)
      secureStorage.writeSecureData("userPhoneNo", userProfile.phoneNo!);
    if (userProfile.ekycStatus != null)
      secureStorage.writeSecureData("ekycStatus", userProfile.ekycStatus!);

    return userProfile;
  }
}
