import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/VerifyOTP/verifyOTP.dart';
import 'package:selangkah_new/utils/constants.dart';

class ChangePassword extends StatefulWidget {
  final bool hardReset;
  final String phone;

  const ChangePassword(
      {super.key, required this.hardReset, required this.phone});

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  final _formKey = GlobalKey<FormState>();
  // bool _validate = false;
  bool passwordVisibleOff = true;
  bool cpasswordVisibleOff = true;
  String? password, cpassword;

  String? validatePassword(String? value) {
    //regex for the password matches (8 length alphanumeric with number)
    RegExp reg = RegExp(r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$');

    if (value!.length == 0) {
      return 'passwordrequired'.tr();
    }

    if (!reg.hasMatch(value)) {
      return 'invalid_password_format'.tr();
    }
    return null;
  }

  String? validateCPassword(String? value) {
    //regex for the password matches (8 length alphanumeric with number)
    RegExp reg = RegExp(r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$');

    if (value!.length == 0) {
      return 'passwordrequired'.tr();
    }

    if (!reg.hasMatch(value)) {
      return 'invalid_password_format'.tr();
    }
    return null;
  }

  _sendToServer() {
    if (_formKey.currentState!.validate()) {
      // No any error in validation
      _formKey.currentState!.save();

      if (password == cpassword) {
        // print("Phone Number ${widget.phone}");
        checkConnectivity();
        // print('success');
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('passwordunmatched'.tr().toString()),
        ));
      }
    } else {
      // validation error
      // setState(() {
      //   _validate = true;
      // });
    }
  }

  checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());

    setState(() {
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        // print("Internet is available");
        getPost();
      } else {
        // print("Internet is not available");
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('nointernet'.tr().toString()),
        ));
      }
    });
  }

  Future<void> getPost() async {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => BlocProvider(
          create: (context) => OtpVerifyCubit(),
          child: VerifyOTP(
            fromScreen: 'change',
            phone: widget.phone,
            password: password,
            username: '',
            hardReset: widget.hardReset,
          ),
        ),
      ),
    );
    // final Map<String, String> map = {
    //   "dataset": "get_app_config",
    //   "token": TOKEN,
    // };

    // final response = await http.post(
    //   API_URL,
    //   headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    //   body: map,
    // );

    // Config config = configFromJson(response.body);

    // if (config.data[0].disableOtp == '1') {
    //   final Map<String, String> map = {
    //     'dataset': 'forgot_password_app',
    //     'phonenumber': widget.phone,
    //     'password': password,
    //     'token': TOKEN,
    //   };

    //   print('calling post FORGOT_PASSWORD');

    //   await http.post(
    //     API_URL,
    //     headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    //     body: map,
    //   );

    //   Navigator.of(context).pop();
    //   if (widget.hardReset) {
    //     Fluttertoast.showToast(
    //       msg: 'succes_reset_pass_login'.tr(),
    //       toastLength: Toast.LENGTH_SHORT,
    //       gravity: ToastGravity.BOTTOM,
    //     );
    //   } else {
    //     Fluttertoast.showToast(
    //       msg: 'success'.tr(),
    //       toastLength: Toast.LENGTH_SHORT,
    //       gravity: ToastGravity.CENTER,
    //     );
    //   }
    // } else if (config.data[0].disableOtp == '0') {
    //   Navigator.of(context).pushReplacement(
    //     MaterialPageRoute(
    //       builder: (context) => BlocProvider(
    //         create: (context) => OtpBloc(),
    //         child: VerifyOTPNewScreen(
    //           fromScreen: 'change',
    //           phone: widget.phone,
    //           password: password,
    //           username: '',
    //         ),
    //       ),
    //     ),
    //   );
    // }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        centerTitle: true,
        // automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          'change_password'.tr(),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 15,
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: 'Roboto',
          ),
        ),
      ),
      body: Center(
        child: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              width: width * 0.9,
              padding: EdgeInsets.symmetric(vertical: 25),
              height: height,
              child: Stack(
                children: <Widget>[
                  Form(
                    key: _formKey,
                    child: formUI(size),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget formUI(Size size) {
    double width = MediaQuery.of(context).size.width;
    return Column(
      // mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'resetpassword'.tr().toString(),
          style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 15,
        ),
        Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            // width: width * 0.8,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(width: 1, color: Colors.grey),
            ),
            child: TextFormField(
              keyboardType: TextInputType.visiblePassword,
              obscureText: passwordVisibleOff,
              onSaved: (String? val) {
                password = val;
                // print(password);
              },
              validator: validatePassword,
              onChanged: (value) {},
              cursorColor: kPrimaryColor,
              decoration: InputDecoration(
                errorMaxLines: 4,
                icon: Icon(
                  Icons.lock,
                  color: kPrimaryColor,
                ),
                hintText: 'new_password'.tr(),
                border: InputBorder.none,
                suffixIcon: IconButton(
                  icon: Icon(
                    // Based on passwordVisible state choose the icon
                    passwordVisibleOff
                        ? Icons.visibility_off
                        : Icons.visibility,
                    color: Theme.of(context).primaryColorDark,
                  ),
                  onPressed: () {
                    // Update the state i.e. toogle the state of passwordVisible variable
                    setState(() {
                      passwordVisibleOff = !passwordVisibleOff;
                    });
                  },
                ),
              ),
            )),
        SizedBox(
          height: 20,
        ),
        Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            // width: width * 0.8,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(width: 1, color: Colors.grey),
            ),
            child: TextFormField(
              keyboardType: TextInputType.visiblePassword,
              obscureText: cpasswordVisibleOff,
              onSaved: (String? val) {
                cpassword = val;
                // print(cpassword);
              },
              validator: validateCPassword,
              onChanged: (value) {},
              cursorColor: kPrimaryColor,
              decoration: InputDecoration(
                errorMaxLines: 4,
                icon: Icon(
                  Icons.lock,
                  color: kPrimaryColor,
                ),
                hintText: 'retype_new_password'.tr(),
                border: InputBorder.none,
                suffixIcon: IconButton(
                  icon: Icon(
                    // Based on passwordVisible state choose the icon
                    cpasswordVisibleOff
                        ? Icons.visibility_off
                        : Icons.visibility,
                    color: Theme.of(context).primaryColorDark,
                  ),
                  onPressed: () {
                    // Update the state i.e. toogle the state of passwordVisible variable
                    setState(() {
                      cpasswordVisibleOff = !cpasswordVisibleOff;
                    });
                  },
                ),
              ),
            )),
        Spacer(),
        Text(
          'invalid_password_format'.tr(),
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.red),
        ),
        SizedBox(height: 20),
        Container(
          width: width * 0.9,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              backgroundColor: kPrimaryColor,
            ),
            onPressed: () {
              FocusScope.of(context).requestFocus(FocusNode());
              _sendToServer();
            },
            child: Padding(
              padding: EdgeInsets.all(18.0),
              child: Text(
                'submit'.tr(),
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          ),
        ),

        // Container(
        //   width: width * 0.9,
        //   // width: size.width,
        //   child: Column(
        //     crossAxisAlignment: CrossAxisAlignment.end,
        //     children: <Widget>[
        //       Container(
        //         width: size.width / 3,
        //         child: ClipRRect(
        //           borderRadius: BorderRadius.circular(10),
        //           child: TextButton(
        //             style: TextButton.styleFrom(
        //               backgroundColor: kPrimaryColor,
        //               padding: EdgeInsets.symmetric(
        //                 horizontal: 30,
        //               ),
        //               shape: RoundedRectangleBorder(
        //                   borderRadius: BorderRadius.circular(5.0)),
        //             ),
        //             onPressed: () {
        //               FocusScope.of(context).requestFocus(FocusNode());
        //               _sendToServer();
        //             },
        //             child: Text(
        //               'submit'.tr().toString(),
        //               style: TextStyle(color: Colors.white, fontSize: 14),
        //             ),
        //           ),
        //         ),
        //       ),
        //     ],
        //   ),
        // )
      ],
    );
  }
}
