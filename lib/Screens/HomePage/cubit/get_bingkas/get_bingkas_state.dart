part of 'get_bingkas_cubit.dart';

@immutable
abstract class GetBingkasState {
  const GetBingkasState();
}

class GetBingkasInitial extends GetBingkasState {
  const GetBingkasInitial();
}

class GetBingkasLoading extends GetBingkasState {
  const GetBingkasLoading();
}

class GetBingkasNotActivate extends GetBingkasState {
  const GetBingkasNotActivate();
}

class GetBingkasSemak extends GetBingkasState {
  const GetBingkasSemak();
}

class GetBingkasWavpay extends GetBingkasState {
  final WavpayBalance? wavpayBalance;
  final String selId;

  GetBingkasWavpay(this.wavpayBalance, this.selId);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetBingkasWavpay &&
        other.wavpayBalance == wavpayBalance &&
        other.selId == selId;
  }

  @override
  int get hashCode => wavpayBalance.hashCode ^ selId.hashCode;
}
