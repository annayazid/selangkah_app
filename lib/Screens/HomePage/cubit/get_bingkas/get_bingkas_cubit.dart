import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Bingkas/bingkas.dart';
import 'package:selangkah_new/Screens/WavPay/wavpay.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

part 'get_bingkas_state.dart';

class GetBingkasCubit extends Cubit<GetBingkasState> {
  GetBingkasCubit() : super(GetBingkasInitial());

  Future<void> getBingkas() async {
    String token = await BingkasRepositories.getToken();

    String status = await BingkasRepositories.getStatus(token);

    //get bingkas status
    String bingkasStatus = await BingkasRepositories.getWalletStatus(token);

    if (status == '5' || status == '7') {
      if (bingkasStatus == '-1') {
        emit(GetBingkasNotActivate());
      } else if (bingkasStatus == '0') {
        emit(GetBingkasSemak());
      } else if (bingkasStatus == '1') {
        //getwavpay
        WavpayBalance? wavpayBalance = await WavPayRepositories.getBalance();

        String selId = await SecureStorage().readSecureData('userSelId');

        emit(GetBingkasWavpay(wavpayBalance, selId));
      }
    } else if (status == '1') {
      emit(GetBingkasSemak());
    } else if (status == '1') {
      emit(GetBingkasNotActivate());
    } else {
      emit(GetBingkasNotActivate());
    }
  }
}
