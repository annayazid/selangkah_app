import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/Selidik/selidik.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

part 'get_homepage_data_state.dart';

class GetHomepageDataCubit extends Cubit<GetHomepageDataState> {
  GetHomepageDataCubit() : super(GetHomepageDataInitial());

  Future<void> getHomepageData() async {
    emit(GetHomepageDataLoading());

    //process here weee

    //set journey
    await GlobalFunction.startSession();
    GlobalFunction.screenJourney('4');

    //get config
    Config config = await HomepageRepositories.getConfig();

    //get announcement & discover
    Announcement announcement = await HomepageRepositories.getAnnouncement();
    Discover discover = await HomepageRepositories.getDiscover();

    //get package info & latest version
    bool isLatestVersion = true;
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    if (config.data![0].disableAppUpdateAlert == '0') {
      Version version = await HomepageRepositories.getVersion();

      if (Platform.isAndroid) {
        if (packageInfo.version != version.data![0].androidVersion) {
          isLatestVersion = false;
        }
      } else {
        if (packageInfo.version != version.data![0].iosVersion) {
          isLatestVersion = false;
        }
      }
    }

    BubbleList bubbleList = await AsasRepositories.getAsasBubble();

    bool gotAsas = bubbleList.data!.any((element) {
      return element.bubbleName == 'ASAS';
    });

    Question question = await SelidikRepositories.getQuestion();
    BookAppointment appointment = await LhrRepositories.bookAppointment();

    bool isGotAppointment = false;

    if (appointment.data!.isNotEmpty) {
      isGotAppointment = true;
    }

    print('appointment is');
    print(isGotAppointment);

    SplashData splashData =
        await HomepageRepositories.getPopUp(GlobalVariables.language!);

    String id = await SecureStorage().readSecureData('userId');

    SplashInfo splashInfo =
        await HomepageRepositories.getSplashInfo(GlobalVariables.language!);

    HomepageData homepageData = HomepageData(
      config: config,
      announcement: announcement,
      discover: discover,
      version: '${packageInfo.version} (${packageInfo.buildNumber})',
      question: question,
      isLatestVersion: isLatestVersion,
      showAsas: gotAsas,
      isGotAppointment: isGotAppointment,
      splashData: splashData,
      userId: id,
      splashInfo: splashInfo,
    );

    // await Future.delayed(Duration(seconds: 1));

    emit(GetHomepageDataLoaded(homepageData));
  }
}
