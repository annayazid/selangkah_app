part of 'get_homepage_data_cubit.dart';

@immutable
abstract class GetHomepageDataState {
  const GetHomepageDataState();
}

class GetHomepageDataInitial extends GetHomepageDataState {
  const GetHomepageDataInitial();
}

class GetHomepageDataLoading extends GetHomepageDataState {
  const GetHomepageDataLoading();
}

class GetHomepageDataLoaded extends GetHomepageDataState {
  final HomepageData homepageData;

  GetHomepageDataLoaded(this.homepageData);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetHomepageDataLoaded && other.homepageData == homepageData;
  }

  @override
  int get hashCode => homepageData.hashCode;
}
