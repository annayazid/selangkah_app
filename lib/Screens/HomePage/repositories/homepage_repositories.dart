import 'dart:convert';
import 'dart:developer';

import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:http/http.dart' as http;
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class HomepageRepositories {
  static Future<Config> getConfig() async {
    final Map<String, String> map = {
      'token': TOKEN,
    };

    log('calling config');
    final response = await http.post(
      Uri.parse('$API_URL/get_app_config'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    log(response.body);

    Config config = configFromJson(response.body);
    return config;
  }

  static Future<Announcement> getAnnouncement() async {
    String language = await SecureStorage().readSecureData('language');

    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
    };

    print(map);

    log('calling post get annoucement');

    final response = await http.post(
      Uri.parse('$API_URL/app_get_announcement'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    log(response.body);

    Announcement announcement = announcementFromJson(response.body);
    return announcement;
  }

  static Future<Discover> getDiscover() async {
    String language = await SecureStorage().readSecureData('language');

    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
    };

    log('calling post get discover');

    final response = await http.post(
      Uri.parse('$API_URL/app_get_discover'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    log(response.body);

    Discover discover = discoverFromJson(response.body);
    return discover;
  }

  static Future<Version> getVersion() async {
    final Map<String, String> map = {
      'token': TOKEN,
    };

    log('calling get app ver');
    final response = await http.post(
      Uri.parse('$API_URL/get_app_ver'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    log(response.body);

    Version version = versionFromJson(response.body);
    return version;
  }

  static Future<Device> getDeviceId(String id) async {
    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_URL/get_device_id'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    Device device = deviceFromJson(response.body);

    return device;
  }

  static Future<void> submitVisitAnnouncement(String id) async {
    Map<String, String> map = {
      'ann_id': id,
      'token': TOKEN,
    };

    print(map);
    print('sending annoucement visit');

    final response = await http.post(
      Uri.parse('$API_URL/announcement_visit'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }

  static Future<void> submitVisitDiscover(String id) async {
    Map<String, String> map = {
      'dis_id': id,
      'token': TOKEN,
    };

    print(map);

    print('sending discover visit');

    final response = await http.post(
      Uri.parse('$API_URL/discover_visit'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }

  static Future<void> dontShowAgain({required String splashId}) async {
    // String id = '99971';

    String sessionId = GlobalVariables.sessionId!;

    final Map<String, String> map = {
      'session_id': sessionId,
      'splash_id': splashId,
      'token': TOKEN,
    };

    //print('calling get config');
    final response = await http.post(
      Uri.parse('$API_URL/splash_enquiry'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }

  static Future<SplashData> getPopUp(String language) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'language': language,
      'token': TOKEN,
    };

    print('calling splash');
    final response = await http.post(
      Uri.parse('$API_URL/splash_screen'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    SplashData splashData = splashDataFromJson(response.body);
    return splashData;

    // if (response.body.contains('[]')) {
    //   return SliderSelangkah(code: null, data: null);
    // } else {
    //   SliderSelangkah slider = sliderSelangkahFromJson(response.body);
    //   return slider;
    // }
  }

  static Future<String> getLinkedCards() async {
    String id = await SecureStorage().readSecureData('userId');

    final response = await http.get(
      Uri.parse('https://selangkah.kiplepay.com/sm/api/v1.0/card/card-manager'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'selangkah_id': id,
      },
    );

    // print(response.body);

    var data = json.decode(response.body);

    String url = data['data']['url'];
    return url;
  }

  static Future<SplashInfo> getSplashInfo(String language) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'language': language,
      'token': TOKEN,
      'session_id': GlobalVariables.sessionId!,
    };

    print(map);

    print('calling splash');
    final response = await http.post(
      Uri.parse('$API_URL/get_splash'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    SplashInfo splashInfo = splashInfoFromJson(response.body);
    // SplashInfo splashInfo = splashInfoFromJson(
    //     '{"Code":200,"Data":[{"id_splash_screen":"1","id_splash_screen_dtl":"1","text":"","image":"https://app.selangkah.my/manage2/faq_upload/AlrajhiEng[1].png","link":"https://app.selangkah.my/al-rajhi/pages/interest_form.php?uid=","button_name":"Check Now"},{"id_splash_screen":"1","id_splash_screen_dtl":"2","text":"","image":"https://app.selangkah.my/manage2/faq_upload/flower.jpg","link":"https://app.selangkah.my/al-rajhi/pages/interest_form.php?uid=","button_name":""}]}');

    return splashInfo;
  }

  static Future<void> submitSplash(String splashId, String splashDtlId) async {
    String id = await SecureStorage().readSecureData('userId');

    Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'id_splash_screen': splashId,
      'id_splash_screen_dtl': splashId,
      'view': '1',
      'session_id': GlobalVariables.sessionId!,
    };

    print(map);
    print('sending splash visit');

    final response = await http.post(
      Uri.parse('$API_URL/set_respond_splash'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }
}
