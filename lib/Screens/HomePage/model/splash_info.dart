// To parse this JSON data, do
//
//     final splashInfo = splashInfoFromJson(jsonString);

import 'dart:convert';

SplashInfo splashInfoFromJson(String str) =>
    SplashInfo.fromJson(json.decode(str));

String splashInfoToJson(SplashInfo data) => json.encode(data.toJson());

class SplashInfo {
  int? code;
  List<SplashInfoData>? data;

  SplashInfo({
    this.code,
    this.data,
  });

  factory SplashInfo.fromJson(Map<String, dynamic> json) => SplashInfo(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<SplashInfoData>.from(
                json["Data"]!.map((x) => SplashInfoData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class SplashInfoData {
  String? title;
  String? redirect;
  String? idSplashScreen;
  String? idSplashScreenDtl;
  String? text;
  String? image;
  String? link;
  String? buttonName;

  SplashInfoData({
    this.title,
    this.redirect,
    this.idSplashScreen,
    this.idSplashScreenDtl,
    this.text,
    this.image,
    this.link,
    this.buttonName,
  });

  factory SplashInfoData.fromJson(Map<String, dynamic> json) => SplashInfoData(
        title: json["title"],
        redirect: json["redirect"],
        idSplashScreen: json["id_splash_screen"],
        idSplashScreenDtl: json["id_splash_screen_dtl"],
        text: json["text"],
        image: json["image"],
        link: json["link"],
        buttonName: json["button_name"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "redirect": redirect,
        "id_splash_screen": idSplashScreen,
        "id_splash_screen_dtl": idSplashScreenDtl,
        "text": text,
        "image": image,
        "link": link,
        "button_name": buttonName,
      };
}
