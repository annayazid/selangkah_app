import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/Selidik/selidik.dart';

class HomepageData {
  final Config config;
  final Announcement announcement;
  final Discover discover;
  final String version;
  final bool isLatestVersion;
  final Question question;
  final bool showAsas;
  final bool isGotAppointment;
  final SplashData splashData;
  final String userId;
  final SplashInfo splashInfo;

  HomepageData({
    required this.config,
    required this.announcement,
    required this.discover,
    required this.version,
    required this.isLatestVersion,
    required this.question,
    required this.showAsas,
    required this.isGotAppointment,
    required this.splashData,
    required this.userId,
    required this.splashInfo,
  });
}
