// To parse this JSON data, do
//
//     final splashData = splashDataFromJson(jsonString);

import 'dart:convert';

SplashData splashDataFromJson(String str) =>
    SplashData.fromJson(json.decode(str));

String splashDataToJson(SplashData data) => json.encode(data.toJson());

class SplashData {
  int? code;
  List<Datum>? data;

  SplashData({
    this.code,
    this.data,
  });

  factory SplashData.fromJson(Map<String, dynamic> json) => SplashData(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<Datum>.from(json["Data"]!.map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  String? id;
  String? date;
  String? faqName;
  String? docContent;
  String? url;
  String? image;
  String? homeImage;
  String? displayOrder;

  Datum({
    this.id,
    this.date,
    this.faqName,
    this.docContent,
    this.url,
    this.image,
    this.homeImage,
    this.displayOrder,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        date: json["date"],
        faqName: json["faq_name"],
        docContent: json["doc_content"],
        url: json["url"],
        image: json["image"],
        homeImage: json["home_image"],
        displayOrder: json["display_order"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "date": date,
        "faq_name": faqName,
        "doc_content": docContent,
        "url": url,
        "image": image,
        "home_image": homeImage,
        "display_order": displayOrder,
      };
}
