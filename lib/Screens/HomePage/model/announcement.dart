// To parse this JSON data, do
//
//     final announcement = announcementFromJson(jsonString);

import 'dart:convert';

Announcement announcementFromJson(String str) =>
    Announcement.fromJson(json.decode(str));

String announcementToJson(Announcement data) => json.encode(data.toJson());

class Announcement {
  Announcement({
    this.code,
    this.data,
  });

  int? code;
  List<AnnouncementData>? data;

  factory Announcement.fromJson(Map<String, dynamic> json) => Announcement(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<AnnouncementData>.from(
                json["Data"]!.map((x) => AnnouncementData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class AnnouncementData {
  AnnouncementData({
    this.id,
    this.date,
    this.faqName,
    this.docContent,
    this.url,
    this.image,
    this.homeImage,
    this.displayOrder,
    this.autoDirect,
    this.webView,
  });

  String? id;
  String? date;
  String? faqName;
  String? docContent;
  String? url;
  String? image;
  String? homeImage;
  String? displayOrder;
  String? autoDirect;
  String? webView;

  factory AnnouncementData.fromJson(Map<String, dynamic> json) =>
      AnnouncementData(
        id: json["id"],
        date: json["date"],
        faqName: json["faq_name"],
        docContent: json["doc_content"],
        url: json["url"],
        image: json["image"],
        homeImage: json["home_image"],
        displayOrder: json["display_order"],
        autoDirect: json["auto_direct"],
        webView: json["web_view"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "date": date,
        "faq_name": faqName,
        "doc_content": docContent,
        "url": url,
        "image": image,
        "home_image": homeImage,
        "display_order": displayOrder,
        "auto_direct": autoDirect,
        "web_view": webView,
      };
}
