// To parse this JSON data, do
//
//     final discover = discoverFromJson(jsonString);

import 'dart:convert';

Discover discoverFromJson(String str) => Discover.fromJson(json.decode(str));

String discoverToJson(Discover data) => json.encode(data.toJson());

class Discover {
  Discover({
    this.code,
    this.data,
  });

  int? code;
  List<DiscoverData>? data;

  factory Discover.fromJson(Map<String, dynamic> json) => Discover(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<DiscoverData>.from(
                json["Data"]!.map((x) => DiscoverData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class DiscoverData {
  DiscoverData({
    this.id,
    this.date,
    this.discoverName,
    this.docContent,
    this.url,
    this.image,
    this.homeImage,
    this.displayOrder,
    this.remedi,
    this.remediRedirect,
  });

  String? id;
  String? date;
  String? discoverName;
  String? docContent;
  String? url;
  String? image;
  String? homeImage;
  String? displayOrder;
  dynamic remedi;
  String? remediRedirect;

  factory DiscoverData.fromJson(Map<String, dynamic> json) => DiscoverData(
        id: json["id"],
        date: json["date"],
        discoverName: json["discover_name"],
        docContent: json["doc_content"],
        url: json["url"],
        image: json["image"],
        homeImage: json["home_image"],
        displayOrder: json["display_order"],
        remedi: json["remedi"],
        remediRedirect: json["remedi_redirect"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "date": date,
        "discover_name": discoverName,
        "doc_content": docContent,
        "url": url,
        "image": image,
        "home_image": homeImage,
        "display_order": displayOrder,
        "remedi": remedi,
        "remedi_redirect": remediRedirect,
      };
}
