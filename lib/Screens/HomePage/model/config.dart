// To parse this JSON data, do
//
//     final config = configFromJson(jsonString);

import 'dart:convert';

Config configFromJson(String str) => Config.fromJson(json.decode(str));

String configToJson(Config data) => json.encode(data.toJson());

class Config {
  Config({
    this.code,
    this.data,
  });

  int? code;
  List<ConfigData>? data;

  factory Config.fromJson(Map<String, dynamic> json) => Config(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<ConfigData>.from(
                json["Data"]!.map((x) => ConfigData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class ConfigData {
  ConfigData({
    this.welcomeBanner,
    this.announcementBanner,
    this.discoverBanner,
    this.disableOtp,
    this.telegramLink,
    this.disableVax,
    this.redirectVax,
    this.announcementTotal,
    this.disableAppUpdateAlert,
    this.disableEwallet,
    this.disableSplash,
    this.disableSelidik,
    this.disableSaring,
    this.disableWavpay,
    this.disableZakat,
  });

  String? welcomeBanner;
  String? announcementBanner;
  String? discoverBanner;
  String? disableOtp;
  String? telegramLink;
  String? disableVax;
  String? redirectVax;
  String? announcementTotal;
  String? disableAppUpdateAlert;
  String? disableEwallet;
  String? disableSplash;
  String? disableSelidik;
  String? disableSaring;
  String? disableWavpay;
  String? disableZakat;

  factory ConfigData.fromJson(Map<String, dynamic> json) => ConfigData(
        welcomeBanner: json["welcome_banner"],
        announcementBanner: json["announcement_banner"],
        discoverBanner: json["discover_banner"],
        disableOtp: json["disable_otp"],
        telegramLink: json["telegram_link"],
        disableVax: json["disable_vax"],
        redirectVax: json["redirect_vax"],
        announcementTotal: json["announcement_total"],
        disableAppUpdateAlert: json["disable_app_update_alert"],
        disableEwallet: json["disable_ewallet"],
        disableSplash: json["disable_splash"],
        disableSelidik: json["disable_selidik"],
        disableSaring: json["disable_saring"],
        disableWavpay: json["disable_wavpay"],
        disableZakat: json["disable_zakat"],
      );

  Map<String, dynamic> toJson() => {
        "welcome_banner": welcomeBanner,
        "announcement_banner": announcementBanner,
        "discover_banner": discoverBanner,
        "disable_otp": disableOtp,
        "telegram_link": telegramLink,
        "disable_vax": disableVax,
        "redirect_vax": redirectVax,
        "announcement_total": announcementTotal,
        "disable_app_update_alert": disableAppUpdateAlert,
        "disable_ewallet": disableEwallet,
        "disable_splash": disableSplash,
        "disable_selidik": disableSelidik,
        "disable_saring": disableSaring,
        "disable_wavpay": disableWavpay,
        "disable_zakat": disableZakat,
      };
}
