// To parse this JSON data, do
//
//     final device = deviceFromJson(jsonString);

import 'dart:convert';

Device deviceFromJson(String str) => Device.fromJson(json.decode(str));

class Device {
  Device({
    this.code,
    this.deviceId,
  });

  int? code;
  DeviceId? deviceId;

  factory Device.fromJson(Map<String, dynamic> json) => Device(
        code: json["Code"],
        deviceId: DeviceId.fromJson(json["device_id"]),
      );
}

class DeviceId {
  DeviceId({
    this.deviceId,
  });

  dynamic deviceId;

  factory DeviceId.fromJson(Map<String, dynamic> json) => DeviceId(
        deviceId: json["device_id"],
      );

  Map<String, dynamic> toJson() => {
        "device_id": deviceId,
      };
}
