// To parse this JSON data, do
//
//     final version = versionFromJson(jsonString);

import 'dart:convert';

Version versionFromJson(String str) => Version.fromJson(json.decode(str));

String versionToJson(Version data) => json.encode(data.toJson());

class Version {
  Version({
    this.code,
    this.data,
  });

  int? code;
  List<VersionData>? data;

  factory Version.fromJson(Map<String, dynamic> json) => Version(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<VersionData>.from(
                json["Data"]!.map((x) => VersionData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class VersionData {
  VersionData({
    this.androidVersion,
    this.iosVersion,
  });

  String? androidVersion;
  String? iosVersion;

  factory VersionData.fromJson(Map<String, dynamic> json) => VersionData(
        androidVersion: json["android_version"],
        iosVersion: json["ios_version"],
      );

  Map<String, dynamic> toJson() => {
        "android_version": androidVersion,
        "ios_version": iosVersion,
      };
}
