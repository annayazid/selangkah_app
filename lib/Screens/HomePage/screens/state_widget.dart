import 'package:flutter/material.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/WebView/webview.dart';
import 'package:selangkah_new/utils/global.dart';

class StateWidget extends StatelessWidget {
  const StateWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: EdgeInsets.all(0),
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        childAspectRatio: 6.5 / 6,
        crossAxisCount: 4,
        crossAxisSpacing: 2,
        mainAxisSpacing: 0,
      ),
      children: [
        GridChild(
          image: 'assets/images/Homepage/icon_state_selangor.png',
          title: 'Selangor',
          function: () {
            GlobalFunction.screenJourney('68');
            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) =>
                    SelangorProgram(appBarTitle: 'Selangor State Programs'),
              ),
            );
          },
        ),
        GridChild(
          image: 'assets/images/Homepage/icon_state_perak.png',
          title: 'Perak',
          function: () {
            GlobalFunction.screenJourney('66');
            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) => SelangkahWebview(
                  appBarTitle: 'Perak',
                  url: 'https://www.perakprihatin.com/',
                ),
              ),
            );
          },
        ),
        GridChild(
          image: 'assets/images/Homepage/icon_state_terengganu.png',
          title: 'Terengganu',
          function: () {
            GlobalFunction.screenJourney('67');
            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) => SelangkahWebview(
                  appBarTitle: 'Terengganu',
                  url: 'https://www.kadsejahtera.com/',
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
