import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/WebView/webview.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class AnnouncementInfo extends StatelessWidget {
  final AnnouncementData announcementData;

  const AnnouncementInfo({super.key, required this.announcementData});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.arrow_back,
            color: kPrimaryColor,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/images/Scaffold/selangkah_logo.png",
          width: width * 0.45,
        ),
      ),
      body: Container(
        height: height,
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: width * 0.05, vertical: height * 0.03),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(15),
                            topRight: Radius.circular(15),
                          ),
                          child: Image(
                            image: AssetImage(
                                'assets/images/Homepage/Bg_Small.png'),
                          ),
                        ),
                        Center(
                          child: Container(
                            margin: EdgeInsets.only(top: height * 0.07),
                            child: Text(
                              'notification'.tr().toString(),
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                              textScaleFactor: 2,
                            ),
                          ),
                        )
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
                      // height: size.height * 0.6,
                      width: width * 0.9,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(15),
                            bottomRight: Radius.circular(15),
                          )),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: height * 0.01),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                announcementData.faqName!,
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 10),
                          Container(
                            margin: EdgeInsets.only(top: height * 0.01),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                announcementData.date!,
                                style: TextStyle(
                                  fontSize: 17,
                                  color: kPrimaryColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          announcementData.image == ''
                              ? Container()
                              : Container(
                                  height: height / 3,
                                  margin: EdgeInsets.only(top: 15),
                                  child: CachedNetworkImage(
                                    imageUrl: announcementData.image!,
                                    fit: BoxFit.contain,
                                    errorWidget: (context, url, error) {
                                      return Image.asset(
                                        'assets/images/Homepage/SELANGKAH_App_Banners-07.png',
                                      );
                                    },
                                  ),
                                ),
                          Container(
                            margin: EdgeInsets.only(top: 15),
                            child: Text(
                              announcementData.docContent!,
                              // maxLines: ,
                              style: TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ),
                          SizedBox(height: 50)
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            () {
              if (announcementData.url == '' ||
                  announcementData.url == 'null') {
                return Container();
              } else {
                return Positioned.fill(
                  bottom: height * 0.02,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      height: height * 0.06,
                      // width: width - 40,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: kPrimaryColor),
                        onPressed: () async {
                          String newUrl = announcementData.url!;

                          if (announcementData.url!.contains('uid=')) {
                            String uid =
                                await SecureStorage().readSecureData('userId');

                            newUrl = announcementData.url!
                                .replaceAll('uid=', 'uid=$uid');
                          }
                          print('new url is $newUrl');
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => SelangkahWebview(
                                appBarTitle: announcementData.faqName!,
                                url: newUrl,
                              ),
                              settings:
                                  RouteSettings(name: 'Notification web page'),
                            ),
                          );
                        },
                        child: Center(
                          child: Text(
                            'view_more_info'.tr().toString(),
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              }
            }()
          ],
        ),
      ),
    );
  }
}
