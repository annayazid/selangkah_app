import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/Bingkas/bingkas.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/SelVax/selvax.dart';
import 'package:selangkah_new/Screens/WebView/webview.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart' as ekyc;
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';

class SelangorProgram extends StatefulWidget {
  final String appBarTitle;

  const SelangorProgram({super.key, required this.appBarTitle});

  @override
  State<SelangorProgram> createState() => _SelangorProgramState();
}

class _SelangorProgramState extends State<SelangorProgram> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/images/Scaffold/selangkah_logo.png",
          width: width * 0.45,
        ),
      ),
      body: Column(
        children: [
          AppBar(
            centerTitle: true,
            automaticallyImplyLeading: true,
            backgroundColor: kPrimaryColor,
            title: Text(
              widget.appBarTitle,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 15.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontFamily: "Roboto",
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(7.5),
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.5, 0.5),
                  blurRadius: 2,
                ),
              ],
            ),
            child: GridView(
              padding: EdgeInsets.all(0),
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 6.5 / 7,
                crossAxisCount: 4,
                crossAxisSpacing: 2,
                mainAxisSpacing: 0,
              ),
              children: [
                GridChild(
                  image: 'assets/images/Homepage/icon_selvax.png',
                  title: 'SelVax',
                  function: () {
                    GlobalFunction.screenJourney('58');
                    Navigator.of(context, rootNavigator: true).push(
                      MaterialPageRoute(
                        builder: (context) => BlocProvider(
                          create: (context) => CheckProfileCubit(),
                          child: SelvaxMain(),
                        ),
                      ),
                    );
                  },
                ),
                GridChild(
                  image: 'assets/images/Homepage/icon_saring.png',
                  title: 'Selangor\nSaring',
                  function: () {
                    GlobalFunction.screenJourney('50');
                    Navigator.of(context, rootNavigator: true).push(
                      MaterialPageRoute(
                        // builder: (context) => BlocProvider(
                        //   create: (context) => UserTokenCubit(),
                        //   child: SaringPage(),
                        builder: (context) => MultiBlocProvider(
                          providers: [
                            BlocProvider(
                              create: (context) => ekyc.CheckProfileCubit(),
                            )
                          ],
                          child: ekyc.EKYCScreen(),
                        ),
                      ),
                    );
                  },
                ),
                GridChild(
                  image: 'assets/images/Homepage/icon_selcan.png',
                  title: 'Kanser\nSelangor',
                  function: () {
                    GlobalFunction.screenJourney('69');
                    Navigator.of(context, rootNavigator: true).push(
                      MaterialPageRoute(
                        builder: (context) => SelangkahWebview(
                          appBarTitle: 'Kanser Selangor',
                          url: 'https://selangorsaring.selangkah.my/',
                        ),
                      ),
                    );
                  },
                ),
                GridChild(
                  image: 'assets/images/Homepage/icon_skim-rawatan-jantung.png',
                  title: 'Skim Rawatan\nJantung',
                  function: () {
                    GlobalFunction.screenJourney('70');
                    Navigator.of(context, rootNavigator: true).push(
                      MaterialPageRoute(
                        builder: (context) => SelangkahWebview(
                          appBarTitle: 'Skim Rawatan Jantung',
                          url: 'https://iltizamselangorsihat.com/cvs.html',
                        ),
                      ),
                    );
                  },
                ),
                GridChild(
                  image: 'assets/images/Homepage/icon_isspa.png',
                  title: 'ISS',
                  function: () {
                    GlobalFunction.screenJourney('71');
                    Navigator.of(context, rootNavigator: true).push(
                      MaterialPageRoute(
                        builder: (context) => SelangkahWebview(
                          appBarTitle: 'ISS',
                          url: 'https://iltizamselangorsihat.com/',
                        ),
                      ),
                    );
                  },
                ),
                GridChild(
                  image: 'assets/images/Homepage/icon_isspa.png',
                  title: 'ISSPA',
                  function: () {
                    GlobalFunction.screenJourney('72');
                    Navigator.of(context, rootNavigator: true).push(
                      MaterialPageRoute(
                        builder: (context) => SelangkahWebview(
                          appBarTitle: 'ISSPA',
                          url: 'https://iltizamselangorsihat.com/pspa.html',
                        ),
                      ),
                    );
                  },
                ),
                GridChild(
                  image: 'assets/images/Homepage/icon_tibi.png',
                  title: 'Rawatan Tibi\nSelangor',
                  function: () {
                    GlobalFunction.screenJourney('73');
                    Navigator.of(context, rootNavigator: true).push(
                      MaterialPageRoute(
                        builder: (context) => SelangkahWebview(
                          appBarTitle: 'Rawatan Tibi Selangor',
                          url:
                              'https://www.selangorprihatin.com/insentifrawatantibi',
                        ),
                      ),
                    );
                  },
                ),
                GridChild(
                  image: 'assets/images/Homepage/icon_mental-sehat.png',
                  title: 'Mental SEHAT',
                  function: () {
                    Navigator.of(context, rootNavigator: true).push(
                      MaterialPageRoute(
                        builder: (context) => MultiBlocProvider(
                          providers: [
                            BlocProvider(
                              create: (context) => GetHomepageCubit(),
                            ),
                            BlocProvider(
                              create: (context) => CommentCubit(),
                            ),
                          ],
                          child: SehatHomepage(),
                        ),
                      ),
                    );
                  },
                ),
                GridChild(
                  image: 'assets/images/Homepage/icon_bingkas.png',
                  title: 'BINGKAS',
                  function: () {
                    GlobalFunction.screenJourney('54');
                    // Navigator.of(context, rootNavigator: true).push(
                    //   MaterialPageRoute(
                    //     builder: (context) => SelangkahWebview(
                    //       appBarTitle: 'Bingkas Selangor',
                    //       url: 'https://www.bingkasselangor.com/',
                    //     ),
                    //   ),
                    // );

                    Navigator.of(context, rootNavigator: true).push(
                      MaterialPageRoute(
                        builder: (context) => MultiBlocProvider(
                          providers: [
                            BlocProvider(
                              create: (context) => ProfileCubit(),
                            ),
                            BlocProvider(
                              create: (context) => BingkasStatusCubit(),
                            ),
                          ],
                          child: BingkasScreen(),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
