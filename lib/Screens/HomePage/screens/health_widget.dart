import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';
import 'package:selangkah_new/Screens/Jovian/jovian.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/MapClinic/map_clinic.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/Screens/WebView/webview.dart';
import 'package:selangkah_new/Screens/WebviewIos/webview_ios.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class HealthWidget extends StatelessWidget {
  final bool showAsas;

  const HealthWidget({super.key, required this.showAsas});

  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: EdgeInsets.all(0),
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        childAspectRatio: 5.5 / 6,
        crossAxisCount: 4,
        crossAxisSpacing: 2,
        mainAxisSpacing: 0,
      ),
      children: [
        GridChild(
          image: 'assets/images/Homepage/pharmacy.png',
          title: 'pharmacy'.tr(),
          function: () {
            GlobalFunction.screenJourney('60');

            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) => BlocProvider(
                  create: (context) => CheckUserCubit(),
                  child: JovianScreen(),
                ),
              ),
            );
          },
        ),
        GridChild(
          image: 'assets/images/Homepage/telemedicine.png',
          title: 'telemedicine'.tr(),
          function: () async {
            GlobalFunction.screenJourney('61');
            String id = await SecureStorage().readSecureData('userId');
            String phoneNo =
                await SecureStorage().readSecureData('userPhoneNo');
            String email = await SecureStorage().readSecureData('email');

            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) => WebviewIOS(
                  appBarTitle: 'telemedicine'.tr(),
                  url:
                      'https://selcare.com/auth/selangkah-sso?selangkahID=$id&phoneNumber=$phoneNo&email=$email&redirectAfter=telemedicine',
                ),
              ),
            );
          },
        ),
        GridChild(
          image: 'assets/images/Homepage/home_nursing_physio.png',
          title: 'home_nursing_physio'.tr(),
          function: () async {
            GlobalFunction.screenJourney('62');
            String id = await SecureStorage().readSecureData('userId');
            String phoneNo =
                await SecureStorage().readSecureData('userPhoneNo');
            String email = await SecureStorage().readSecureData('email');

            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) => WebviewIOS(
                  appBarTitle: 'home_nursing_physio_title'.tr(),
                  url:
                      'https://selcare.com/auth/selangkah-sso?selangkahID=$id&phoneNumber=$phoneNo&email=$email&redirectAfter=home_nursing',
                ),
              ),
            );
          },
        ),
        GridChild(
          image: 'assets/images/Homepage/dental_clinic_appointment.png',
          title: 'clinic_dental_appointment'.tr(),
          function: () async {
            GlobalFunction.screenJourney('63');
            String id = await SecureStorage().readSecureData('userId');
            String phoneNo =
                await SecureStorage().readSecureData('userPhoneNo');
            String email = await SecureStorage().readSecureData('email');

            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) => WebviewIOS(
                  appBarTitle: 'clinic_dental_appointment_title'.tr(),
                  url:
                      'https://selcare.com/auth/selangkah-sso?selangkahID=$id&phoneNumber=$phoneNo&email=$email&redirectAfter=clinic_appointment',
                ),
              ),
            );
          },
        ),
        GridChild(
          image: 'assets/images/Homepage/clinic_locator.png',
          title: 'clinic_locator'.tr(),
          function: () async {
            GlobalFunction.screenJourney('64');
            // Navigator.of(context, rootNavigator: true).push(
            //   MaterialPageRoute(
            //     builder: (context) => ScreenTest(),
            //   ),
            // );

            GlobalFunction.displayDisclosure(
              icon: Icon(
                Icons.location_on_outlined,
                color: Colors.blue,
                size: 30,
              ),
              title: 'locationMapTitle'.tr(),
              description: 'locationMap'.tr(),
              key: 'locationMap',
            ).then((value) async {
              if (value) {
                //function here
                if (await Permission.location.request().isGranted) {
                  if (value) {
                    // GeolocationStatus geolocationStatus =
                    //     await Geolocator()
                    //         .checkGeolocationPermissionStatus();

                    // print('geolocationStatus $geolocationStatus');

                    //prompt user to OPEN LOCATION here

                    if (await Geolocator.isLocationServiceEnabled()) {
                      Navigator.of(context, rootNavigator: true).push(
                        MaterialPageRoute(
                          builder: (context) => MapClinicScreen(),
                        ),
                      );
                      print('granted');
                    } else {
                      AppSettings.openLocationSettings().then((value) async {
                        // await Future.delayed(Duration(seconds: 1));
                        if (await Geolocator.isLocationServiceEnabled()) {
                          Navigator.of(context, rootNavigator: true).push(
                            MaterialPageRoute(
                              builder: (context) => MapClinicScreen(),
                            ),
                          );
                        }
                      });

                      print('not granted');
                    }
                  }
                } else {
                  Flushbar(
                    backgroundColor: Colors.white,
                    // message: LoremText,
                    // title: 'Permission needed to continue',
                    titleText: Text(
                      'permission_continue'.tr(),
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                    messageText: Text(
                      'continue_permission'.tr(),
                      style: TextStyle(color: Colors.black),
                    ),
                    mainButton: TextButton(
                      onPressed: AppSettings.openAppSettings,
                      child: Text(
                        'App Settings',
                        style: TextStyle(color: Colors.blue),
                      ),
                    ),
                    duration: Duration(seconds: 7),
                  ).show(context);
                }
              }
            });
          },
        ),
        GridChild(
          image: 'assets/images/Homepage/medial_erecord.png',
          title: 'emedical_record'.tr(),
          function: () async {
            GlobalFunction.screenJourney('49');
            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => SliderCardCubit(),
                    ),
                    BlocProvider(
                      create: (context) => ProfilePageCubit(),
                    ),
                    BlocProvider(
                      create: (context) => DropdownMeasurementCubit(),
                    ),
                    BlocProvider(
                      create: (context) => HealthRecordCubit(),
                    ),
                    BlocProvider(
                      create: (context) => BackgroundIllnessCubit(),
                    ),
                    BlocProvider(
                      create: (context) => AppointmentCubit(),
                    ),
                    BlocProvider(
                      create: (context) => TestResultCubit(),
                    ),
                    BlocProvider(
                      create: (context) => MedicalCertificatesCubit(),
                    ),
                    BlocProvider(
                      create: (context) => PrescriptionsCubit(),
                    ),
                    BlocProvider(
                      create: (context) => VaccineCertificatesCubit(),
                    ),
                    BlocProvider(
                      create: (context) => ArchivedRecordCubit(),
                    ),
                    BlocProvider(
                      create: (context) => InvoiceCubit(),
                    ),
                    BlocProvider(
                      create: (context) => ReferralLetterCubit(),
                    ),
                  ],
                  child: ProfilePage(
                    page: 'HAYAT',
                  ),
                ),
              ),
            );
          },
        ),
        GridChild(
          image: 'assets/images/Homepage/selcare_corporate_tpa.png',
          title: 'selcare_corporate_tpa'.tr(),
          function: () async {
            GlobalFunction.screenJourney('65');
            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) => SelangkahWebview(
                  appBarTitle: 'selcare_corporate_tpa_title'.tr(),
                  url: 'https://secure.selcare.my/site/login',
                ),
              ),
            );
          },
        ),
        if (showAsas)
          GridChild(
            image: 'assets/images/Homepage/asas.png',
            title: 'ASAS',
            function: () {
              GlobalFunction.screenJourney('51');
              Navigator.of(context, rootNavigator: true).push(
                MaterialPageRoute(
                  builder: (context) => BlocProvider(
                    create: (context) => GetProfileCubit(),
                    child: AsasScreen(),
                  ),
                ),
              );
            },
          ),
      ],
    );
  }
}

class GridChild extends StatelessWidget {
  final String image;
  final String title;
  final Function() function;

  const GridChild(
      {super.key,
      required this.function,
      required this.image,
      required this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.red,
      child: GestureDetector(
        onTap: function,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              image,
              height: 50,
            ),
            SizedBox(height: 10),
            Text(
              title,
              style: TextStyle(fontSize: 12),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
