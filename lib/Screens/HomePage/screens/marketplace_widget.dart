import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/WebView/webview.dart';
import 'package:selangkah_new/Screens/Zakat/zakat.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';
import 'package:url_launcher/url_launcher.dart';

class MarketplaceWidget extends StatelessWidget {
  const MarketplaceWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: EdgeInsets.all(0),
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        childAspectRatio: 5.5 / 6,
        crossAxisCount: 4,
        crossAxisSpacing: 2,
        mainAxisSpacing: 0,
      ),
      children: [
        GridChild(
          image: 'assets/images/Homepage/selgate_foundation.png',
          title: 'selgate_foundation'.tr(),
          function: () async {
            GlobalFunction.screenJourney('59');

            // print('redirect biz');

            String id = await SecureStorage().readSecureData('userId');

            if (Platform.isAndroid) {
              Navigator.of(context, rootNavigator: true).push(
                MaterialPageRoute(
                  builder: (context) => SelangkahWebview(
                    appBarTitle: 'selgate_foundation_title'.tr(),
                    url:
                        'https://app.selangkah.my/selgate-foundation/pages/home.php?uid=$id',
                  ),
                ),
              );
            } else {
              launchUrl(
                Uri.parse('https://biz.selangkah.my/'),
                mode: LaunchMode.externalApplication,
              );
            }
          },
        ),
        GridChild(
          image: 'assets/images/Homepage/zakat.png',
          title: 'Zakat',
          function: () {
            GlobalFunction.screenJourney('56');
            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) => BlocProvider(
                  create: (context) => GetZakatListCubit(),
                  child: ZakatScreen(),
                ),
              ),
            );
          },
        ),
        GridChild(
          image: 'assets/images/Homepage/selangkahbiz.png',
          title: 'Selangkah\nBiz',
          function: () {
            // print('redirect biz');

            GlobalFunction.screenJourney('53');
            launchUrl(
              Uri.parse('https://biz.selangkah.my/'),
              mode: LaunchMode.externalApplication,
            );
          },
        ),
        GridChild(
          image: 'assets/images/Homepage/adworks.png',
          title: 'Adwork',
          function: () {
            // print('redirect adwork');
            GlobalFunction.screenJourney('52');
            launchUrl(
              Uri.parse('https://www.adwork.io/'),
              mode: LaunchMode.externalApplication,
            );
          },
        ),
      ],
    );
  }
}
