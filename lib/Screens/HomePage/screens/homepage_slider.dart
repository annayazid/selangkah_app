import 'dart:io';

import 'package:barcode_widget/barcode_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/Bingkas/bingkas.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/WavPay/wavpay.dart';
import 'package:selangkah_new/Screens/WebView/webview.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_banner.dart';

import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';
import 'package:url_launcher/url_launcher.dart';

class HomepageSlider extends StatefulWidget {
  final HomepageData homepageData;

  const HomepageSlider({super.key, required this.homepageData});

  @override
  State<HomepageSlider> createState() => _HomepageSliderState();
}

class _HomepageSliderState extends State<HomepageSlider> {
  int carouselIndex = 0;

  List<String> dot = [];

  @override
  void initState() {
    dot.add('wallet');
    dot.add('bingkas');
    context.read<GetBingkasCubit>().getBingkas();
    widget.homepageData.announcement.data!.forEach((element) {
      dot.add(element.faqName!);
    });
    super.initState();
  }

  Widget getBingkasCard(
    double width,
    double height,
    WavpayBalance? wavpayBalance,
    String selId,
  ) {
    if (wavpayBalance == null) {
      return Stack(
        children: [
          Image.asset('assets/images/bingkas/wavpay_not_activated.png'),
          Positioned(
            bottom: 10,
            left: 10,
            child: Text(
              'AKTIFKAN SEKARANG',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 17,
                color: Colors.white,
              ),
            ),
          )
        ],
      );
    } else {
      if (wavpayBalance.totalBalance != null) {
        return Stack(
          children: [
            Image.asset('assets/images/bingkas/wavpay_balance.png'),
            Positioned(
              bottom: 25,
              child: Container(
                padding: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'BAKI AKAUN',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 10,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      'RM ${wavpayBalance.balance![0].balance}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              right: 20,
              bottom: 25,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                ),
                padding: EdgeInsets.all(5),
                child: Column(
                  children: [
                    BarcodeWidget(
                      barcode: Barcode.code128(),
                      data: selId,
                      width: width * 0.4,
                      height: height * 0.03,
                      style: TextStyle(fontSize: 9),
                      drawText: false,
                    ),
                    SizedBox(height: 2),
                    Text(
                      selId,
                      style: TextStyle(fontSize: 9),
                    ),
                  ],
                ),
              ),
            ),
          ],
        );
      } else {
        return Stack(
          children: [
            Image.asset('assets/images/bingkas/wavpay_not_activated.png'),
            Positioned(
              bottom: 20,
              left: 20,
              child: Text(
                'Sistem kami sedang \ndalam penyelenggaraan.\nSila semak semula sebentar lagi.',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                  color: Colors.white,
                ),
              ),
            )
          ],
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      children: [
        CarouselSlider(
          items: [
            //ewallet
            GestureDetector(
              onTap: () {
                //EWALLET MODULE STARTING POINT HERE
              },
              child: WalletHomeBanner(),
            ),

            //bingkas
            ...[
              BlocBuilder<GetBingkasCubit, GetBingkasState>(
                builder: (context, state) {
                  print('state is ${state.toString()}');

                  if (state is GetBingkasNotActivate) {
                    // GlobalFunction.screenJourney('54');
                    return GestureDetector(
                      onTap: () {
                        GlobalFunction.screenJourney('54');
                        Navigator.of(context, rootNavigator: true).push(
                          MaterialPageRoute(
                            builder: (context) => MultiBlocProvider(
                              providers: [
                                BlocProvider(
                                  create: (context) => ProfileCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => BingkasStatusCubit(),
                                ),
                              ],
                              child: BingkasScreen(),
                            ),
                          ),
                        );
                      },
                      child: Stack(
                        children: [
                          Image.asset(
                            'assets/images/bingkas/wavpay_balance.png',
                          ),
                          Positioned(
                            bottom: 10,
                            left: 10,
                            child: Text(
                              'DAFTAR SEKARANG',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 17,
                                color: Colors.white,
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  } else if (state is GetBingkasSemak) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.of(context, rootNavigator: true).push(
                          MaterialPageRoute(
                            builder: (context) => MultiBlocProvider(
                              providers: [
                                BlocProvider(
                                  create: (context) => ProfileCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => BingkasStatusCubit(),
                                ),
                              ],
                              child: BingkasScreen(),
                            ),
                          ),
                        );
                      },
                      child: Stack(
                        children: [
                          Image.asset(
                            'assets/images/bingkas/wavpay_balance.png',
                          ),
                          Positioned(
                            bottom: 10,
                            left: 10,
                            child: Text(
                              'SEMAK PENDAFTARAN',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 17,
                                color: Colors.white,
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  } else if (state is GetBingkasWavpay) {
                    return GestureDetector(
                      onTap: () async {
                        // await LaunchApp.openApp(
                        //   androidPackageName: 'net.wavpay.wallet',
                        //   // iosUrlScheme: 'pulsesecure://',
                        //   // appStoreLink: 'itms-apps://itunes.apple.com/us/app/pulse-secure/id945832041',
                        //   // openStore: false
                        // );

                        if (state.wavpayBalance == null) {
                          Navigator.of(context, rootNavigator: true).push(
                            MaterialPageRoute(
                              builder: (context) => BlocProvider(
                                create: (context) => RegisterWalletCubit(),
                                child: WavpayScreen(
                                  registered: false,
                                ),
                              ),
                            ),
                          );
                        } else {
                          if (state.wavpayBalance!.totalBalance != null) {
                            Navigator.of(context, rootNavigator: true).push(
                              MaterialPageRoute(
                                builder: (context) => BlocProvider(
                                  create: (context) => RegisterWalletCubit(),
                                  child: WavpayScreen(
                                    balance:
                                        'RM ${state.wavpayBalance!.balance![0].balance}',
                                    registered: true,
                                  ),
                                ),
                              ),
                            );
                          } else {}
                        }
                      },
                      child: getBingkasCard(
                        width,
                        height,
                        state.wavpayBalance,
                        state.selId,
                      ),
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            ],
            ...widget.homepageData.announcement.data!.map((e) {
              if (e.homeImage == '0') {
                return GestureDetector(
                  onTap: () {
                    HomepageRepositories.submitVisitAnnouncement(e.id!);
                    Navigator.of(context, rootNavigator: true).push(
                      MaterialPageRoute(
                        builder: (context) => AnnouncementInfo(
                          announcementData: e,
                        ),
                      ),
                    );
                  },
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      CachedNetworkImage(
                        imageUrl: widget
                            .homepageData.config.data![0].announcementBanner!,
                        // fit: BoxFit.fitWidth,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 35, left: 20, right: 20, bottom: 35),
                        child: Column(
                          // mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              e.faqName!,
                              textAlign: TextAlign.center,
                              style: boldTextStyle(
                                  size: 14, color: Color(0xFFFFDC89)
                                  // app2ndColor
                                  ),
                            ),
                            SizedBox(height: 7),
                            Expanded(
                              child: Text(
                                e.docContent!,
                                textAlign: TextAlign.center,
                                style: secondaryTextStyle(
                                  size: 12,
                                  color: Colors.white,
                                ),
                                maxLines: 5,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              } else {
                return GestureDetector(
                  onTap: () async {
                    HomepageRepositories.submitVisitAnnouncement(e.id!);
                    if (e.autoDirect == '0' && e.webView == '0') {
                      Navigator.of(context, rootNavigator: true).push(
                        MaterialPageRoute(
                          builder: (context) => AnnouncementInfo(
                            announcementData: e,
                          ),
                        ),
                      );
                    } else if (e.autoDirect == '0' && e.webView == '1') {
                      String id =
                          await SecureStorage().readSecureData('userId');
                      String language =
                          await SecureStorage().readSecureData('language');

                      Navigator.of(context, rootNavigator: true).push(
                        MaterialPageRoute(
                          builder: (context) => SelangkahWebview(
                            appBarTitle: e.faqName!,
                            url: '${e.url}$id&language=$language',
                          ),
                        ),
                      );
                    } else if (e.autoDirect == '1') {
                      String id =
                          await SecureStorage().readSecureData('userId');
                      String language =
                          await SecureStorage().readSecureData('language');

                      String url = e.url!;

                      if (url.contains('uid=')) {
                        url = '${e.url}$id&language=$language';
                      }

                      if (Platform.isIOS) {
                        launchUrl(
                          Uri.parse(url),
                          mode: LaunchMode.externalApplication,
                        );
                      } else {
                        Navigator.of(context, rootNavigator: true).push(
                          MaterialPageRoute(
                            builder: (context) => SelangkahWebview(
                              appBarTitle: e.faqName!,
                              url: url,
                            ),
                          ),
                        );
                      }
                    }
                  },
                  child: CachedNetworkImage(
                    imageUrl: e.image!,
                    errorWidget: (context, url, error) {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.error_outline,
                            color: Colors.red,
                          ),
                          Text('Image Link Error'),
                        ],
                      );
                    },
                  ),
                );
              }
            }).toList()
          ],
          options: CarouselOptions(
            viewportFraction: 0.9,
            aspectRatio: 2.0,
            initialPage: 0,
            enableInfiniteScroll: true,
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 5),
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
            scrollDirection: Axis.horizontal,
            onPageChanged: (index, reason) {
              setState(() {
                carouselIndex = index;
              });
            },
          ),
        ),

        //dot indicator

        //dot
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: dot.map((slider) {
            int index = dot.indexOf(slider);
            return Container(
              width: 8.0,
              height: 8.0,
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: carouselIndex == index
                    ? kPrimaryColor
                    : Color.fromARGB(255, 248, 226, 226),
                // ? Colors.red
                // : Colors.blue,
              ),
            );
          }).toList(),
        ),
      ],
    );
  }
}
