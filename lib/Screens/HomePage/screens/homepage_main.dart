import 'dart:async';

import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent-tab-view.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart' as bubble;
import 'package:selangkah_new/Screens/Faq/faq.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/Inbox/inbox.dart';
import 'package:selangkah_new/Screens/Jovian/jovian.dart' as jovian;
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/Screens/SignIn/sign_in.dart';
import 'package:selangkah_new/Screens/WebView/webview.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_qr.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_screen.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class HomePageScreen extends StatefulWidget {
  const HomePageScreen({Key? key}) : super(key: key);

  @override
  State<HomePageScreen> createState() => _MyAppState();
}

PersistentTabController? persistentTabController =
    PersistentTabController(initialIndex: 2);

class _MyAppState extends State<HomePageScreen> {
  Timer? timer;

  @override
  void initState() {
    timer = Timer.periodic(Duration(seconds: 60), (Timer t) => checkLogin());
    super.initState();
  }

  void clearCacheLogout() async {
    SecureStorage secureStorage = SecureStorage();
    await secureStorage.deleteAllSecureData();
    MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => PrivacyTermCubit(),
        ),
        BlocProvider(
          create: (context) => CheckUserCubit(),
        ),
      ],
      child: SignInScreen(),
    ).launch(context, isNewTask: true);
  }

  void checkLogin() async {
    String userId = await SecureStorage().readSecureData('userId');

    if (userId != '1793367') {
      String currentDeviceId = await GlobalFunction.getDeviceId();
      Device deviceFromDB = await HomepageRepositories.getDeviceId(userId);

      print('currentDeviceId $currentDeviceId');
      print('deviceFromDB ${deviceFromDB.deviceId!.deviceId}');

      if (currentDeviceId != deviceFromDB.deviceId!.deviceId) {
        Alert(
          style: AlertStyle(),
          closeFunction: () => clearCacheLogout(),
          onWillPopActive: true,
          context: context,
          type: AlertType.error,
          title: 'logged_in_device'.tr(),
          desc: 'login_again'.tr(),
          buttons: [
            DialogButton(
              child: Text(
                "Okay",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () => clearCacheLogout(),
              color: kPrimaryColor,
            )
          ],
        ).show();

        timer!.cancel();
      }
    } else {
      timer!.cancel();
    }
  }

  double? x, y;

  @override
  void dispose() {
    if (timer != null) {
      timer!.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    if (x == null && y == null) {
      setState(() {
        x = MediaQuery.of(context).size.width * 0.8;
        y = MediaQuery.of(context).size.height * 0.75 -
            AppBar().preferredSize.height -
            MediaQuery.of(context).padding.top;
      });
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/images/Scaffold/selangkah_logo.png",
          width: width * 0.45,
        ),
        leading: IconButton(
            icon: Icon(
              Icons.person_outline_rounded,
              color: Colors.grey,
              size: 30,
            ),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                        create: (context) => ProfilePageCubit(),
                      ),
                      BlocProvider(
                        create: (context) => SliderCardCubit(),
                      ),
                      BlocProvider(
                        create: (context) => DropdownMeasurementCubit(),
                      ),
                      BlocProvider(
                        create: (context) => HealthRecordCubit(),
                      ),
                      BlocProvider(
                        create: (context) => BackgroundIllnessCubit(),
                      ),
                      BlocProvider(
                        create: (context) => AppointmentCubit(),
                      ),
                      BlocProvider(
                        create: (context) => ReferralLetterCubit(),
                      ),
                      BlocProvider(
                        create: (context) => TestResultCubit(),
                      ),
                      BlocProvider(
                        create: (context) => MedicalCertificatesCubit(),
                      ),
                      BlocProvider(
                        create: (context) => PrescriptionsCubit(),
                      ),
                      BlocProvider(
                        create: (context) => VaccineCertificatesCubit(),
                      ),
                      BlocProvider(
                        create: (context) => InvoiceCubit(),
                      ),
                      BlocProvider(
                        create: (context) => ArchivedRecordCubit(),
                      ),
                    ],
                    child: ProfilePage(page: 'SelangkahID'),
                  ),
                ),
              );
            }),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context, rootNavigator: true).push(
                MaterialPageRoute(
                  builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                        create: (context) => GetInboxCubit(),
                      ),
                      BlocProvider(
                        create: (context) => DeleteInboxCubit(),
                      ),
                    ],
                    child: InboxMain(),
                  ),
                ),
              );
            },
            icon: Icon(
              Icons.mail_outline_rounded,
              color: Colors.grey,
              size: 30,
            ),
          ),
          // SizedBox(width: 5),
        ],
      ),
      body: DoubleBackToCloseApp(
        snackBar: SnackBar(
          content: Text('tap_again_exit'.tr()),
        ),
        child: Stack(
          children: [
            Column(
              children: [
                Expanded(
                  child: PersistentTabView(
                    context,
                    controller: persistentTabController,
                    screens: _buildScreens(),
                    items: _navBarsItems(),
                    confineInSafeArea: false,
                    backgroundColor: Colors.white,
                    // Default is Colors.white.
                    handleAndroidBackButtonPress: false,
                    // Default is true.
                    resizeToAvoidBottomInset: true,
                    // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
                    stateManagement: true,
                    // Default is true.
                    hideNavigationBarWhenKeyboardShows: true,
                    // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
                    decoration: NavBarDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      colorBehindNavBar: Colors.white,
                    ),
                    popAllScreensOnTapOfSelectedTab: true,
                    popActionScreens: PopActionScreensType.all,
                    itemAnimationProperties: ItemAnimationProperties(
                      // Navigation Bar's items animation properties.
                      duration: Duration(milliseconds: 200),
                      curve: Curves.ease,
                    ),
                    screenTransitionAnimation: ScreenTransitionAnimation(
                      // Screens transition animation on change of selected tab.
                      animateTabTransition: true,
                      curve: Curves.ease,
                      duration: Duration(milliseconds: 200),
                    ),
                    navBarStyle: NavBarStyle
                        .style3, // Choose the nav bar style with this property.
                  ),
                ),
                SizedBox(height: 10),
              ],
            ),
            Positioned(
              left: x,
              top: y,
              child: Draggable(
                feedback: TelegramButton(),
                child: TelegramButton(),
                childWhenDragging: Container(),
                onDragEnd: (details) {
                  setState(() {
                    x = details.offset.dx;
                    y = details.offset.dy -
                        AppBar().preferredSize.height -
                        MediaQuery.of(context).padding.top;
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        onPressed: (p0) async {
          GlobalFunction.screenJourney('61');
          // Navigator.of(context, rootNavigator: true).push(
          //   MaterialPageRoute(
          //     builder: (context) => MultiBlocProvider(
          //       providers: [
          //         BlocProvider(
          //           create: (context) => SliderCardCubit(),
          //         ),
          //         BlocProvider(
          //           create: (context) => ProfilePageCubit(),
          //         ),
          //         BlocProvider(
          //           create: (context) => DropdownMeasurementCubit(),
          //         ),
          //       ],
          //       child: ProfilePage(
          //         page: 'HAYAT',
          //       ),
          //     ),
          //   ),
          // );

          String id = await SecureStorage().readSecureData('userId');
          String phoneNo = await SecureStorage().readSecureData('userPhoneNo');
          String email = await SecureStorage().readSecureData('email');

          Navigator.of(context, rootNavigator: true).push(
            MaterialPageRoute(
              builder: (context) => SelangkahWebview(
                appBarTitle: 'telemedicine'.tr(),
                url:
                    'https://selcare.com/auth/selangkah-sso?selangkahID=$id&phoneNumber=$phoneNo&email=$email&redirectAfter=telemedicine',
              ),
            ),
          );
        },
        icon: Padding(
          padding: EdgeInsets.only(bottom: 2.5),
          child: ImageIcon(
            AssetImage('assets/images/Homepage/telemedicine_bottombar.png'),
            size: 20,
          ),
        ),
        title: ("telemedicine".tr()),
        activeColorPrimary: kPrimaryColor,
        inactiveColorPrimary: Colors.grey,
        textStyle: TextStyle(fontSize: 12),
      ),
      PersistentBottomNavBarItem(
        onPressed: (p0) async {
          GlobalFunction.screenJourney('49');
          Navigator.of(context, rootNavigator: true).push(
            MaterialPageRoute(
              builder: (context) => MultiBlocProvider(
                providers: [
                  BlocProvider(
                    create: (context) => SliderCardCubit(),
                  ),
                  BlocProvider(
                    create: (context) => ProfilePageCubit(),
                  ),
                  BlocProvider(
                    create: (context) => DropdownMeasurementCubit(),
                  ),
                  BlocProvider(
                    create: (context) => HealthRecordCubit(),
                  ),
                  BlocProvider(
                    create: (context) => BackgroundIllnessCubit(),
                  ),
                  BlocProvider(
                    create: (context) => AppointmentCubit(),
                  ),
                  BlocProvider(
                    create: (context) => ReferralLetterCubit(),
                  ),
                  BlocProvider(
                    create: (context) => TestResultCubit(),
                  ),
                  BlocProvider(
                    create: (context) => MedicalCertificatesCubit(),
                  ),
                  BlocProvider(
                    create: (context) => PrescriptionsCubit(),
                  ),
                  BlocProvider(
                    create: (context) => VaccineCertificatesCubit(),
                  ),
                  BlocProvider(
                    create: (context) => InvoiceCubit(),
                  ),
                  BlocProvider(
                    create: (context) => ArchivedRecordCubit(),
                  ),
                ],
                child: ProfilePage(
                  page: 'HAYAT',
                ),
              ),
            ),
          );
        },
        icon: Padding(
          padding: EdgeInsets.only(bottom: 2.5),
          child: ImageIcon(
            AssetImage('assets/images/Homepage/emr_bottombar.png'),
            size: 30,
          ),
        ),
        title: 'EMR',
        activeColorPrimary: kPrimaryColor,
        inactiveColorPrimary: Colors.grey,
        textStyle: TextStyle(fontSize: 12),
      ),
      PersistentBottomNavBarItem(
        icon: Padding(
          padding: EdgeInsets.only(bottom: 2.5),
          child: ImageIcon(
            AssetImage('assets/images/Homepage/home.png'),
            size: 40,
          ),
        ),
        title: 'home_menubtn'.tr(),
        activeColorPrimary: kPrimaryColor,
        inactiveColorPrimary: Colors.grey,
        textStyle: TextStyle(fontSize: 12),
      ),
      PersistentBottomNavBarItem(
        icon: Padding(
          padding: EdgeInsets.only(bottom: 2.5),
          child: ImageIcon(
            AssetImage('assets/images/Homepage/pay_bottombar.png'),
            size: 30,
          ),
        ),
        title: 'scan_qr_pay_button'.tr(),
        activeColorPrimary: kPrimaryColor,
        inactiveColorPrimary: Colors.grey,
        textStyle: TextStyle(fontSize: 12),
      ),
      PersistentBottomNavBarItem(
        onPressed: (p0) {
          GlobalFunction.screenJourney('60');
          Navigator.of(context, rootNavigator: true).push(
            MaterialPageRoute(
              builder: (context) => BlocProvider(
                create: (context) => jovian.CheckUserCubit(),
                child: jovian.JovianScreen(),
              ),
            ),
          );
        },
        icon: Padding(
          padding: EdgeInsets.only(bottom: 2.5),
          child: ImageIcon(
            AssetImage('assets/images/Homepage/pharmacy_bottombar.png'),
            size: 30,
          ),
        ),
        title: 'pharmacy'.tr(),
        activeColorPrimary: kPrimaryColor,
        inactiveColorPrimary: Colors.grey,
        textStyle: TextStyle(fontSize: 12),
      ),
    ];
  }

  List<Widget> _buildScreens() {
    return [
      Container(),
      Container(),
      HomeWidget(),
      WalletQRWidget(),
      Container(),
    ];
  }
}

class TelegramButton extends StatelessWidget {
  const TelegramButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      heroTag: 'btn_whatsapp',
      backgroundColor: Color(0xFF2CA5E0),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).push(
          MaterialPageRoute(
            builder: (context) => BlocProvider(
              create: (context) => GetFaqCubit(),
              child: FaqScreen(),
            ),
          ),
        );
      },
      child: FaIcon(
        FontAwesomeIcons.headset,
        color: Colors.white,
      ),
    );
  }
}
