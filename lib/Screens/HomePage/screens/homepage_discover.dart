import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/WebView/webview.dart';

class HomepageDiscover extends StatelessWidget {
  final Discover discover;

  const HomepageDiscover({super.key, required this.discover});

  @override
  Widget build(BuildContext context) {
    double height =
        MediaQuery.of(context).size.height - AppBar().preferredSize.height;
    double width = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Container(
        height: height * 0.23,
        child: ListView.builder(
          padding: EdgeInsets.all(2),
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemCount: discover.data!.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                HomepageRepositories.submitVisitDiscover(
                    discover.data![index].id!);
                Navigator.of(context, rootNavigator: true).push(
                  MaterialPageRoute(
                    builder: (context) => SelangkahWebview(
                      appBarTitle: discover.data![index].discoverName!,
                      url: discover.data![index].url!,
                    ),
                  ),
                );
              },
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 5),
                width: width * 0.55,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0.5, 0.5),
                      blurRadius: 1,
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Expanded(
                      flex: 2,
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                        ),
                        child: CachedNetworkImage(
                          imageUrl: discover.data![index].image!,
                          // height: width,
                          // width: size.width,
                          // height: ((size.width * 0.4) / 10) * 6.5,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          discover.data![index].discoverName!,
                          style: TextStyle(fontWeight: FontWeight.bold),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.start,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
