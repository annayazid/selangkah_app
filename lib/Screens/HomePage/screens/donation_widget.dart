import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/Zakat/zakat.dart';
import 'package:selangkah_new/utils/global.dart';

class DonationWidget extends StatelessWidget {
  const DonationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: EdgeInsets.all(0),
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        childAspectRatio: 5.5 / 6,
        crossAxisCount: 4,
        crossAxisSpacing: 2,
        mainAxisSpacing: 0,
      ),
      children: [
        GridChild(
          image: 'assets/images/Homepage/zakat.png',
          title: 'Zakat',
          function: () {
            GlobalFunction.screenJourney('56');
            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) => BlocProvider(
                  create: (context) => GetZakatListCubit(),
                  child: ZakatScreen(),
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
