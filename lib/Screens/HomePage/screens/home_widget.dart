import 'dart:io';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:expandable_page_view/expandable_page_view.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/Screens/WebView/webview.dart';
import 'package:selangkah_new/main.dart';
import 'package:selangkah_new/utils/secure_storage.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/PDFInAppScreen/pdf_in_app_screen.dart';
import 'package:selangkah_new/Screens/Selidik/selidik.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_menu.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';

class HomeWidget extends StatefulWidget {
  HomeWidget({Key? key}) : super(key: key);

  @override
  State<HomeWidget> createState() => _HomePageScreenState();
}

class _HomePageScreenState extends State<HomeWidget> {
  @override
  void initState() {
    context.read<GetHomepageDataCubit>().getHomepageData();
    init();
    super.initState();
  }

  void init() async {
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    // print('${initialMessage!.data.toString()}');

    if (initialMessage != null) {
      redirectHandler(initialMessage);
    }
  }

  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    double height =
        MediaQuery.of(context).size.height - AppBar().preferredSize.height;

    return RefreshIndicator(
      onRefresh: () async {
        context.read<GetHomepageDataCubit>().getHomepageData();
      },
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //carousel
            BlocConsumer<GetHomepageDataCubit, GetHomepageDataState>(
              listener: (context, state) async {
                if (state is GetHomepageDataLoaded) {
                  void splashSaring() {
                    if (state.homepageData.isGotAppointment) {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            contentPadding: EdgeInsets.zero,
                            backgroundColor: Colors.transparent,
                            content: Container(
                              width: double.infinity,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Image.asset(
                                    'assets/images/Saring/saring_semak_susul.png',
                                    fit: BoxFit.fill,
                                  ),
                                  SizedBox(height: 10),
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor: kPrimaryColor,
                                    ),
                                    onPressed: () {
                                      // Navigator.of(context).pop();
                                      Navigator.of(context, rootNavigator: true)
                                          .pushReplacement(
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              MultiBlocProvider(
                                            providers: [
                                              BlocProvider(
                                                create: (context) =>
                                                    SliderCardCubit(),
                                              ),
                                              BlocProvider(
                                                create: (context) =>
                                                    ProfilePageCubit(),
                                              ),
                                              BlocProvider(
                                                create: (context) =>
                                                    DropdownMeasurementCubit(),
                                              ),
                                              BlocProvider(
                                                create: (context) =>
                                                    HealthRecordCubit(),
                                              ),
                                              BlocProvider(
                                                create: (context) =>
                                                    BackgroundIllnessCubit(),
                                              ),
                                              BlocProvider(
                                                create: (context) =>
                                                    AppointmentCubit(),
                                              ),
                                              BlocProvider(
                                                create: (context) =>
                                                    ReferralLetterCubit(),
                                              ),
                                              BlocProvider(
                                                create: (context) =>
                                                    TestResultCubit(),
                                              ),
                                              BlocProvider(
                                                create: (context) =>
                                                    MedicalCertificatesCubit(),
                                              ),
                                              BlocProvider(
                                                create: (context) =>
                                                    PrescriptionsCubit(),
                                              ),
                                              BlocProvider(
                                                create: (context) =>
                                                    VaccineCertificatesCubit(),
                                              ),
                                              BlocProvider(
                                                create: (context) =>
                                                    InvoiceCubit(),
                                              ),
                                              BlocProvider(
                                                create: (context) =>
                                                    ArchivedRecordCubit(),
                                              ),
                                            ],
                                            child: ProfilePage(
                                              page: 'HAYAT',
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                    child: Text('check_result'.tr()),
                                  ),
                                  SizedBox(height: 10),
                                ],
                              ),
                            ),
                            insetPadding: const EdgeInsets.all(10),
                          );
                        },
                      );
                    }
                  }

                  // print('redirect from noti is $redirectFromNoti');
                  // if (false) {
                  //   Navigator.of(context, rootNavigator: true).push(
                  //     MaterialPageRoute(
                  //       builder: (context) => MultiBlocProvider(
                  //         providers: [
                  //           BlocProvider(
                  //             create: (context) => GetInboxCubit(),
                  //           ),
                  //           BlocProvider(
                  //             create: (context) => DeleteInboxCubit(),
                  //           ),
                  //         ],
                  //         child: InboxMain(),
                  //       ),
                  //     ),
                  //   );
                  // }

                  if (!state.homepageData.isLatestVersion) {
                    Alert(
                      context: context,
                      image: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset("assets/images/selangkah_logo.png"),
                      ),
                      title: 'selangkah_needs_update'.tr(),
                      desc: 'new_update_store'.tr(),
                      buttons: [
                        DialogButton(
                          child: Text(
                            'update'.tr(),
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                          onPressed: () {
                            //launch store

                            Navigator.of(context, rootNavigator: true).pop();
                            if (Platform.isAndroid) {
                              launchUrl(
                                Uri.parse(
                                    'https://play.google.com/store/apps/details?id=sel.main.selangkah'),
                                mode: LaunchMode.externalApplication,
                              );
                            } else {
                              launchUrl(
                                Uri.parse(
                                    'https://apps.apple.com/my/app/selangkah/id1552552227'),
                                mode: LaunchMode.externalApplication,
                              );
                              // launch(
                              //     'https://apps.apple.com/my/app/selangkah/id1552552227');
                            }
                          },
                          color: kPrimaryColor,
                          radius: BorderRadius.circular(20),
                        ),
                      ],
                    ).show();
                  }

                  if (state.homepageData.config.data![0].disableSplash == '0' &&
                      state.homepageData.splashData.data!.isNotEmpty) {
                    await Alert(
                      style: AlertStyle(
                        animationType: AnimationType.grow,
                        buttonAreaPadding: EdgeInsets.zero,
                      ),
                      context: context,
                      title: state.homepageData.splashData.data![0].faqName,
                      content: SplashAnnouncement(
                        popUp: state.homepageData.splashData,
                      ),
                      buttons: [],
                    ).show();
                  }

                  if (state.homepageData.userId != '1793367') {
                    if (state.homepageData.config.data![0].disableSelidik ==
                            '0' &&
                        state.homepageData.question.data!.isNotEmpty) {
                      Navigator.of(context, rootNavigator: true).push(
                        MaterialPageRoute(
                          builder: (context) => MultiBlocProvider(
                            providers: [
                              BlocProvider(
                                create: (context) => SelidikCubit(),
                              ),
                              BlocProvider(
                                create: (context) => SendAnswerCubit(),
                              ),
                            ],
                            child: SelidikScreen(),
                          ),
                        ),
                      );
                    }
                  }

                  if (state.homepageData.splashInfo.data!.isNotEmpty) {
                    await showDialog(
                      context: context,
                      builder: (context) {
                        return SplashInfoWidget(
                          homepageData: state.homepageData,
                        );
                      },
                    );
                  } else if (state.homepageData.isGotAppointment) {
                    //uncomment ni balik nanti
                    splashSaring();
                  }
                }
              },
              builder: (context, state) {
                if (state is GetHomepageDataLoaded) {
                  //real carousel
                  return AnimatedSwitcher(
                    duration: Duration(milliseconds: 500),
                    child: BlocProvider(
                      create: (context) => GetBingkasCubit(),
                      child: HomepageSlider(
                        homepageData: state.homepageData,
                      ),
                    ),
                  );
                } else {
                  //shimmer carausel
                  return AnimatedSwitcher(
                    duration: Duration(milliseconds: 500),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade300,
                      highlightColor: Colors.white,
                      period: Duration(seconds: 2),
                      child: CarouselSlider(
                        items: [
                          Container(color: Colors.red),
                        ],
                        options: CarouselOptions(
                          viewportFraction: 0.9,
                          aspectRatio: 2.0,
                          initialPage: 0,
                          enableInfiniteScroll: true,
                          autoPlay: true,
                          autoPlayInterval: Duration(seconds: 5),
                          autoPlayAnimationDuration:
                              Duration(milliseconds: 800),
                          autoPlayCurve: Curves.fastOutSlowIn,
                          scrollDirection: Axis.horizontal,
                        ),
                      ),
                    ),
                  );
                }
              },
            ),

            SizedBox(height: 10),

            //ewallet home menu
            BlocBuilder<GetHomepageDataCubit, GetHomepageDataState>(
              builder: (context, state) {
                if (state is GetHomepageDataLoaded) {
                  return WalletHomeMenu();
                } else {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade300,
                      highlightColor: Colors.white,
                      period: Duration(seconds: 2),
                      child: Container(
                        width: double.infinity,
                        height: height * 0.09,
                        decoration: BoxDecoration(color: Colors.white),
                      ),
                    ),
                  );
                }
              },
            ),

            SizedBox(height: 10),

            //health
            BlocBuilder<GetHomepageDataCubit, GetHomepageDataState>(
              builder: (context, state) {
                if (state is GetHomepageDataLoaded) {
                  return Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      'health_hometitle'.tr(),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade300,
                      highlightColor: Colors.white,
                      period: Duration(seconds: 2),
                      child: Container(
                        color: Colors.white,
                        child: Text(
                          'Health',
                          style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  );
                }
              },
            ),
            SizedBox(height: 10),

            //banner saring
            BlocBuilder<GetHomepageDataCubit, GetHomepageDataState>(
              builder: (context, state) {
                if (state is GetHomepageDataLoaded) {
                  return GestureDetector(
                    onTap: () {
                      if ('redirect_saring'.tr() == '1') {
                        GlobalFunction.screenJourney('50');
                        Navigator.of(context, rootNavigator: true).push(
                          MaterialPageRoute(
                            // builder: (context) => BlocProvider(
                            //   create: (context) => UserTokenCubit(),
                            //   child: SaringPage(),
                            builder: (context) => MultiBlocProvider(
                              providers: [
                                BlocProvider(
                                  create: (context) => CheckProfileCubit(),
                                )
                              ],
                              child: EKYCScreen(),
                            ),
                          ),
                        );
                      } else {
                        Navigator.of(context, rootNavigator: true).push(
                          MaterialPageRoute(
                            builder: (context) => MultiBlocProvider(
                              providers: [
                                BlocProvider(
                                  create: (context) => SliderCardCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => ProfilePageCubit(),
                                ),
                                BlocProvider(
                                  create: (context) =>
                                      DropdownMeasurementCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => HealthRecordCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => BackgroundIllnessCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => AppointmentCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => TestResultCubit(),
                                ),
                                BlocProvider(
                                  create: (context) =>
                                      MedicalCertificatesCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => PrescriptionsCubit(),
                                ),
                                BlocProvider(
                                  create: (context) =>
                                      VaccineCertificatesCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => ArchivedRecordCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => InvoiceCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => ReferralLetterCubit(),
                                ),
                              ],
                              child: ProfilePage(
                                page: 'HAYAT',
                              ),
                            ),
                          ),
                        );
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 15),
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0.0, 1.0), //(x,y)
                            blurRadius: 4.0,
                          ),
                        ],
                        borderRadius: BorderRadius.circular(12),
                        color: Colors.white,
                      ),
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(12),
                            child: Image.asset(
                              'redirect_saring'.tr() == '1'
                                  ? 'assets/images/Saring/saring-banner-bg3.png'
                                  : 'assets/images/Homepage/saring_semak_susul.png',
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          // Container(
                          //   height: 100,
                          //   width: double.infinity,
                          //   decoration: BoxDecoration(
                          //     // color: Colors.grey.withOpacity(0.2),
                          //     borderRadius: BorderRadius.circular(12),
                          //   ),
                          // ),
                          // Container(
                          Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: 15,
                              vertical: 20,
                            ),
                            child: Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                if ('redirect_saring'.tr() == '0') Spacer(),
                                if ('redirect_saring'.tr() == '1')
                                  Expanded(
                                    flex: 2,
                                    child: Container(),
                                  ),
                                'redirect_saring'.tr() == '1'
                                    ? Expanded(
                                        child: AnimatedTextKit(
                                          isRepeatingAnimation: true,
                                          repeatForever: true,
                                          animatedTexts: [
                                            FadeAnimatedText(
                                              'banner_saring_desc'.tr(),
                                              textStyle: TextStyle(
                                                color: Color(0xFF610C0E),
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold,
                                              ),
                                              textAlign: TextAlign.center,
                                              duration: Duration(seconds: 7),
                                            ),
                                            FadeAnimatedText(
                                              'register_saring'.tr(),
                                              textStyle: TextStyle(
                                                color: Color(0xFF610C0E),
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold,
                                              ),
                                              textAlign: TextAlign.center,
                                              duration: Duration(seconds: 5),
                                            ),
                                          ],
                                          // onTap: () {
                                          //   print("Tap Event");
                                          // },
                                        ),
                                      )
                                    : Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.all(5),
                                          child: AnimatedTextKit(
                                            isRepeatingAnimation: true,
                                            repeatForever: true,
                                            animatedTexts: [
                                              FadeAnimatedText(
                                                'banner_saring_desc_semak_susul'
                                                    .tr(),
                                                textStyle: TextStyle(
                                                  color: Color(0xFF610C0E),
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                                textAlign: TextAlign.center,
                                                duration: Duration(seconds: 7),
                                              ),
                                              FadeAnimatedText(
                                                'register_saring_semak_susul'
                                                    .tr(),
                                                textStyle: TextStyle(
                                                  color: Color(0xFF610C0E),
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                                textAlign: TextAlign.center,
                                                duration: Duration(seconds: 5),
                                              ),
                                            ],
                                            // onTap: () {
                                            //   print("Tap Event");
                                            // },
                                          ),
                                        ),
                                      ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade300,
                      highlightColor: Colors.white,
                      period: Duration(seconds: 2),
                      child: Container(
                        width: double.infinity,
                        height: 50,
                        decoration: BoxDecoration(color: Colors.white),
                      ),
                    ),
                  );
                }
              },
            ),

            SizedBox(height: 10),

            //health content
            BlocBuilder<GetHomepageDataCubit, GetHomepageDataState>(
              builder: (context, state) {
                if (state is GetHomepageDataLoaded) {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Container(
                      padding: EdgeInsets.all(7.5),
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0.5, 0.5),
                            blurRadius: 2,
                          ),
                        ],
                      ),
                      child:
                          HealthWidget(showAsas: state.homepageData.showAsas),
                    ),
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade300,
                      highlightColor: Colors.white,
                      period: Duration(seconds: 2),
                      child: Container(
                        width: double.infinity,
                        height: height * 0.18,
                        decoration: BoxDecoration(color: Colors.white),
                      ),
                    ),
                  );
                }
              },
            ),

            SizedBox(height: 10),

            //state programs
            BlocBuilder<GetHomepageDataCubit, GetHomepageDataState>(
              builder: (context, state) {
                if (state is GetHomepageDataLoaded) {
                  return Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      'state_programs'.tr(),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade300,
                      highlightColor: Colors.white,
                      period: Duration(seconds: 2),
                      child: Container(
                        color: Colors.white,
                        child: Text(
                          'state_programs',
                          style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  );
                }
              },
            ),

            SizedBox(height: 10),

            BlocBuilder<GetHomepageDataCubit, GetHomepageDataState>(
              builder: (context, state) {
                if (state is GetHomepageDataLoaded) {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Container(
                      padding: EdgeInsets.all(7.5),
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0.5, 0.5),
                            blurRadius: 2,
                          ),
                        ],
                      ),
                      child: StateWidget(),
                    ),
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade300,
                      highlightColor: Colors.white,
                      period: Duration(seconds: 2),
                      child: Container(
                        width: double.infinity,
                        height: height * 0.09,
                        decoration: BoxDecoration(color: Colors.white),
                      ),
                    ),
                  );
                }
              },
            ),

            SizedBox(height: 10),

            //marketplace
            BlocBuilder<GetHomepageDataCubit, GetHomepageDataState>(
              builder: (context, state) {
                if (state is GetHomepageDataLoaded) {
                  return Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      'welfare_other_services'.tr(),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade300,
                      highlightColor: Colors.white,
                      period: Duration(seconds: 2),
                      child: Container(
                        color: Colors.white,
                        child: Text(
                          'Market Place',
                          style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  );
                }
              },
            ),

            SizedBox(height: 10),

            //marketplace content
            BlocBuilder<GetHomepageDataCubit, GetHomepageDataState>(
              builder: (context, state) {
                if (state is GetHomepageDataLoaded) {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Container(
                      padding: EdgeInsets.all(7.5),
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0.5, 0.5),
                            blurRadius: 2,
                          ),
                        ],
                      ),
                      child: MarketplaceWidget(),
                    ),
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade300,
                      highlightColor: Colors.white,
                      period: Duration(seconds: 2),
                      child: Container(
                        width: double.infinity,
                        height: height * 0.09,
                        decoration: BoxDecoration(color: Colors.white),
                      ),
                    ),
                  );
                }
              },
            ),

            SizedBox(height: 10),

            //discover
            BlocBuilder<GetHomepageDataCubit, GetHomepageDataState>(
              builder: (context, state) {
                if (state is GetHomepageDataLoaded) {
                  return Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      'discover'.tr(),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade300,
                      highlightColor: Colors.white,
                      period: Duration(seconds: 2),
                      child: Container(
                        color: Colors.white,
                        child: Text(
                          'discover'.tr(),
                          style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  );
                }
              },
            ),

            SizedBox(height: 10),

            //discover
            BlocBuilder<GetHomepageDataCubit, GetHomepageDataState>(
              builder: (context, state) {
                if (state is GetHomepageDataLoaded) {
                  return HomepageDiscover(
                    discover: state.homepageData.discover,
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade300,
                      highlightColor: Colors.white,
                      period: Duration(seconds: 2),
                      child: Container(
                        height: height * 0.15,
                        width: double.infinity,
                        decoration: BoxDecoration(color: Colors.white),
                      ),
                    ),
                  );
                }
              },
            ),
            SizedBox(height: 10),

            //version
            BlocBuilder<GetHomepageDataCubit, GetHomepageDataState>(
              builder: (context, state) {
                if (state is GetHomepageDataLoaded) {
                  return Center(
                    child: Text(
                      'Version : ${state.homepageData.version}',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade300,
                      highlightColor: Colors.white,
                      period: Duration(seconds: 2),
                      child: Container(
                        height: height * 0.05,
                        width: double.infinity,
                        decoration: BoxDecoration(color: Colors.white),
                      ),
                    ),
                  );
                }
              },
            ),
            SizedBox(height: 15),

            //privacy
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.of(context, rootNavigator: true)
                        .push(MaterialPageRoute(
                      builder: (context) => PDFInAppPageScreen(
                        // url: GlobalVariables.privacy,
                        url: GlobalVariables.privacy,
                        title: 'privacy'.tr(),
                      ),
                    ));
                  },
                  child: Text(
                    'privacy'.tr(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.red,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //     builder: (context) => PDFInAppPageScreen(
                    //       url: GlobalVariables.termsofuse,
                    //       title: 'termsofuse'.tr(),
                    //     ),
                    //   ),
                    // );
                    Navigator.of(context, rootNavigator: true)
                        .push(MaterialPageRoute(
                      builder: (context) => PDFInAppPageScreen(
                        url: GlobalVariables.termsofuse,
                        title: 'termsofuse'.tr(),
                      ),
                    ));
                  },
                  child: Text(
                    'termsofuse'.tr(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.red,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}

class SplashAnnouncement extends StatefulWidget {
  const SplashAnnouncement({
    Key? key,
    required this.popUp,
  }) : super(key: key);

  final SplashData popUp;

  @override
  _SplashAnnouncementState createState() => _SplashAnnouncementState();
}

class _SplashAnnouncementState extends State<SplashAnnouncement> {
  bool dontShowAgain = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 10),
        CachedNetworkImage(
          imageUrl: widget.popUp.data![0].image!,
          errorWidget: (context, url, error) {
            return Image.asset('assets/images/walkthrough_1.png');
          },
        ),
        SizedBox(height: 10),
        Text(
          widget.popUp.data![0].docContent!,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.normal,
          ),
        ),
        SizedBox(height: 15),
        GestureDetector(
          onTap: () {
            setState(() {
              dontShowAgain = !dontShowAgain;
            });
          },
          child: Row(
            children: [
              SizedBox(width: 10),
              SizedBox(
                height: 15,
                width: 15,
                child: Checkbox(
                  activeColor: kPrimaryColor,
                  value: dontShowAgain,
                  onChanged: (value) {
                    dontShowAgain = value!;
                    setState(() {
                      dontShowAgain = value;
                    });
                  },
                ),
              ),
              SizedBox(width: 10),
              Text(
                'dont_show_again'.tr(),
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 15),
          width: double.infinity,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(backgroundColor: kPrimaryColor),
            onPressed: () {
              print(dontShowAgain);

              if (dontShowAgain) {
                //call api
                HomepageRepositories.dontShowAgain(
                    splashId: widget.popUp.data![0].id!);
              } else {}
              Navigator.of(context).pop();
            },
            child: Text(
              "Okay",
              style: TextStyle(
                color: Colors.white,
                fontSize: 15,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class SplashInfoWidget extends StatefulWidget {
  final HomepageData homepageData;

  const SplashInfoWidget({super.key, required this.homepageData});

  @override
  State<SplashInfoWidget> createState() => _SplashInfoWidgetState();
}

class _SplashInfoWidgetState extends State<SplashInfoWidget> {
  //splash thing
  PageController pageController = PageController();
  List<String> dot = [];
  int splashIndex = 0;

  @override
  void initState() {
    widget.homepageData.splashInfo.data!.forEach((element) {
      dot.add(element.title!);
    });

    if (widget.homepageData.isGotAppointment) {
      dot.add('saring');
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AlertDialog(
        contentPadding: EdgeInsets.zero,
        insetPadding: const EdgeInsets.all(10),
        backgroundColor: Colors.transparent,
        content: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ExpandablePageView(
                controller: pageController,
                // itemCount: widget.homepageData.splashInfo.data!.length,
                onPageChanged: (value) {
                  setState(() {
                    splashIndex = value;
                  });
                },

                children: [
                  ...widget.homepageData.splashInfo.data!.map((e) {
                    return Container(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CachedNetworkImage(
                            imageUrl: e.image!,
                            fit: BoxFit.fill,
                          ),
                          SizedBox(height: 10),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: kPrimaryColor,
                            ),
                            onPressed: () async {
                              String id = await SecureStorage()
                                  .readSecureData('userId');
                              String language = await SecureStorage()
                                  .readSecureData('language');

                              String url = e.link!;
                              if (e.link!.contains('uid=')) {
                                url = '${e.link!}$id&language=$language';
                              }

                              HomepageRepositories.submitSplash(
                                e.idSplashScreen!,
                                e.idSplashScreenDtl!,
                              );

                              Navigator.of(context, rootNavigator: true)
                                  .pushReplacement(
                                MaterialPageRoute(
                                  builder: (context) => SelangkahWebview(
                                    appBarTitle: e.title!,
                                    url: url,
                                  ),
                                ),
                              );
                            },
                            child: Text(e.buttonName!),
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
                    );
                  }).toList(),
                  if (widget.homepageData.isGotAppointment)
                    Container(
                      width: double.infinity,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Image.asset(
                            'assets/images/Saring/saring_semak_susul.png',
                            fit: BoxFit.fill,
                          ),
                          SizedBox(height: 10),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: kPrimaryColor,
                            ),
                            onPressed: () {
                              // Navigator.of(context).pop();
                              Navigator.of(context, rootNavigator: true)
                                  .pushReplacement(
                                MaterialPageRoute(
                                  builder: (context) => MultiBlocProvider(
                                    providers: [
                                      BlocProvider(
                                        create: (context) => SliderCardCubit(),
                                      ),
                                      BlocProvider(
                                        create: (context) => ProfilePageCubit(),
                                      ),
                                      BlocProvider(
                                        create: (context) =>
                                            DropdownMeasurementCubit(),
                                      ),
                                      BlocProvider(
                                        create: (context) =>
                                            HealthRecordCubit(),
                                      ),
                                      BlocProvider(
                                        create: (context) =>
                                            BackgroundIllnessCubit(),
                                      ),
                                      BlocProvider(
                                        create: (context) => AppointmentCubit(),
                                      ),
                                      BlocProvider(
                                        create: (context) =>
                                            ReferralLetterCubit(),
                                      ),
                                      BlocProvider(
                                        create: (context) => TestResultCubit(),
                                      ),
                                      BlocProvider(
                                        create: (context) =>
                                            MedicalCertificatesCubit(),
                                      ),
                                      BlocProvider(
                                        create: (context) =>
                                            PrescriptionsCubit(),
                                      ),
                                      BlocProvider(
                                        create: (context) =>
                                            VaccineCertificatesCubit(),
                                      ),
                                      BlocProvider(
                                        create: (context) => InvoiceCubit(),
                                      ),
                                      BlocProvider(
                                        create: (context) =>
                                            ArchivedRecordCubit(),
                                      ),
                                    ],
                                    child: ProfilePage(
                                      page: 'HAYAT',
                                    ),
                                  ),
                                ),
                              );
                            },
                            child: Text('check_result'.tr()),
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
                    ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: dot.map((slider) {
                  int index = dot.indexOf(slider);
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: splashIndex == index
                          ? kPrimaryColor
                          : Color.fromARGB(255, 248, 226, 226),
                    ),
                  );
                }).toList(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
