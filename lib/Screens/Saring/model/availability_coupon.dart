// To parse this JSON data, do
//
//     final availabilityCoupon = availabilityCouponFromJson(jsonString);

import 'dart:convert';

AvailabilityCoupon availabilityCouponFromJson(String str) =>
    AvailabilityCoupon.fromJson(json.decode(str));

class AvailabilityCoupon {
  AvailabilityCoupon({
    this.code,
    this.data,
  });

  int? code;
  List<AvailabilityCouponData>? data;

  factory AvailabilityCoupon.fromJson(Map<String, dynamic> json) =>
      AvailabilityCoupon(
        code: json["code"],
        data: json["data"] == null
            ? []
            : List<AvailabilityCouponData>.from(
                json["data"].map((x) => AvailabilityCouponData.fromJson(x))),
      );
}

class AvailabilityCouponData {
  AvailabilityCouponData({
    this.id,
    this.siteName,
    this.date,
    this.timeStart,
    this.timeEnd,
    this.addressLine1,
    this.addressLine2,
    this.couponSelectedAr,
    this.allCan,
  });

  int? id;
  String? siteName;
  DateTime? date;
  String? timeStart;
  String? timeEnd;
  String? addressLine1;
  String? addressLine2;
  List<CouponSelectedAr>? couponSelectedAr;
  bool? allCan;

  factory AvailabilityCouponData.fromJson(Map<String, dynamic> json) =>
      AvailabilityCouponData(
        id: json["id"],
        siteName: json["site_name"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        timeStart: json["time_start"],
        timeEnd: json["time_end"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        couponSelectedAr: json["coupon_selected_ar"] == null
            ? []
            : List<CouponSelectedAr>.from(json["coupon_selected_ar"]
                .map((x) => CouponSelectedAr.fromJson(x))),
        allCan: json["all_can"],
      );
}

class CouponSelectedAr {
  CouponSelectedAr({
    this.idCoupon,
    this.couponName,
    this.couponAvailablity,
  });

  String? idCoupon;
  String? couponName;
  String? couponAvailablity;

  factory CouponSelectedAr.fromJson(Map<String, dynamic> json) =>
      CouponSelectedAr(
        idCoupon: json["id_coupon"],
        couponName: json["coupon_name"],
        couponAvailablity: json["coupon_availablity"],
      );

  Map<String, dynamic> toJson() => {
        "id_coupon": idCoupon,
        "coupon_name": couponName,
        "coupon_availablity": couponAvailablity,
      };
}
