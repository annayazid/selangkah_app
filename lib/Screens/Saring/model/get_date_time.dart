// To parse this JSON data, do
//
//     final getDateTime = getDateTimeFromJson(jsonString);

import 'dart:convert';

GetDateTime getDateTimeFromJson(String str) =>
    GetDateTime.fromJson(json.decode(str));

class GetDateTime {
  GetDateTime({
    this.code,
    this.data,
  });

  int? code;
  List<GetDateTimeData>? data;

  factory GetDateTime.fromJson(Map<String, dynamic> json) => GetDateTime(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<GetDateTimeData>.from(
                json["Data"].map((x) => GetDateTimeData.fromJson(x))),
      );
}

class GetDateTimeData {
  GetDateTimeData({
    this.slotDate,
    this.timeStart,
    this.timeEnd,
  });

  DateTime? slotDate;
  String? timeStart;
  String? timeEnd;

  factory GetDateTimeData.fromJson(Map<String, dynamic> json) =>
      GetDateTimeData(
        slotDate: json["slot_date"] == null
            ? null
            : DateTime.parse(json["slot_date"]),
        timeStart: json["time_start"] == null ? null : json["time_start"],
        timeEnd: json["time_end"] == null ? null : json["time_end"],
      );
}
