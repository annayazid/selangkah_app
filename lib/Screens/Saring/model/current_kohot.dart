// To parse this JSON data, do
//
//     final currentKohot = currentKohotFromJson(jsonString);

import 'dart:convert';

CurrentKohot currentKohotFromJson(String str) =>
    CurrentKohot.fromJson(json.decode(str));

String currentKohotToJson(CurrentKohot data) => json.encode(data.toJson());

class CurrentKohot {
  CurrentKohot({
    this.code,
    this.data,
  });

  int? code;
  Data? data;

  factory CurrentKohot.fromJson(Map<String, dynamic> json) => CurrentKohot(
        code: json["code"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "data": data?.toJson(),
      };
}

class Data {
  Data({
    this.id,
    this.idProgram,
    this.year,
    this.series,
    this.active,
    this.createdBy,
    this.createdTs,
  });

  int? id;
  int? idProgram;
  int? year;
  int? series;
  int? active;
  int? createdBy;
  DateTime? createdTs;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        idProgram: json["id_program"],
        year: json["year"],
        series: json["series"],
        active: json["active"],
        createdBy: json["created_by"],
        createdTs: json["created_ts"] == null
            ? null
            : DateTime.parse(json["created_ts"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_program": idProgram,
        "year": year,
        "series": series,
        "active": active,
        "created_by": createdBy,
        "created_ts": createdTs?.toIso8601String(),
      };
}
