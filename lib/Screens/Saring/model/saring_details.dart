// To parse this JSON data, do
//
//     final saringDetails = saringDetailsFromJson(jsonString);

import 'dart:convert';

SaringDetails saringDetailsFromJson(String str) =>
    SaringDetails.fromJson(json.decode(str));

String saringDetailsToJson(SaringDetails data) => json.encode(data.toJson());

class SaringDetails {
  SaringDetails({
    this.code,
    this.data,
  });

  int? code;
  SaringDetailsData? data;

  factory SaringDetails.fromJson(Map<String, dynamic> json) => SaringDetails(
        code: json["code"],
        data: json["data"] == null
            ? null
            : SaringDetailsData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "data": data?.toJson(),
      };
}

class SaringDetailsData {
  SaringDetailsData({
    this.name,
    this.userNo,
    this.idProgramApplicant,
    this.idBingkasDun,
    this.barcodeDir,
    this.eventSite,
    this.eventDate,
    this.eventAddress1,
    this.eventAddress2,
    this.eventTimeStart,
    this.eventTimeEnd,
    this.ekycVerified,
  });

  String? name;
  String? userNo;
  int? idProgramApplicant;
  int? idBingkasDun;
  String? barcodeDir;
  String? eventSite;
  DateTime? eventDate;
  String? eventAddress1;
  String? eventAddress2;
  String? eventTimeStart;
  String? eventTimeEnd;
  bool? ekycVerified;

  factory SaringDetailsData.fromJson(Map<String, dynamic> json) =>
      SaringDetailsData(
        name: json["name"],
        userNo: json["user_no"],
        idProgramApplicant: json["id_program_applicant"],
        idBingkasDun: json["id_bingkas_dun"],
        barcodeDir: json["barcode_dir"],
        eventSite: json["event_site"],
        eventDate: json["event_date"] == null
            ? null
            : DateTime.parse(json["event_date"]),
        eventAddress1: json["event_address1"],
        eventAddress2: json["event_address2"],
        eventTimeStart: json["event_time_start"],
        eventTimeEnd: json["event_time_end"],
        ekycVerified: json["ekyc_verified"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "user_no": userNo,
        "id_program_applicant": idProgramApplicant,
        "id_bingkas_dun": idBingkasDun,
        "barcode_dir": barcodeDir,
        "event_site": eventSite,
        "event_date":
            "${eventDate!.year.toString().padLeft(4, '0')}-${eventDate!.month.toString().padLeft(2, '0')}-${eventDate!.day.toString().padLeft(2, '0')}",
        "event_address1": eventAddress1,
        "event_address2": eventAddress2,
        "event_time_start": eventTimeStart,
        "event_time_end": eventTimeEnd,
        "ekyc_verified": ekycVerified,
      };
}
