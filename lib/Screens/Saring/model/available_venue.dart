// To parse this JSON data, do
//
//     final availableVenue = availableVenueFromJson(jsonString);

import 'dart:convert';

AvailableVenue availableVenueFromJson(String str) =>
    AvailableVenue.fromJson(json.decode(str));

String availableVenueToJson(AvailableVenue data) => json.encode(data.toJson());

class AvailableVenue {
  AvailableVenue({
    this.code,
    this.data,
  });

  int? code;
  List<AvailabelVenuData>? data;

  factory AvailableVenue.fromJson(Map<String, dynamic> json) => AvailableVenue(
        code: json["code"],
        data: json["data"] == null
            ? []
            : List<AvailabelVenuData>.from(
                json["data"].map((x) => AvailabelVenuData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class AvailabelVenuData {
  AvailabelVenuData({
    this.id,
    this.idProgram,
    this.idKohot,
    this.idAdun,
    this.siteName,
    this.date,
    this.timeStart,
    this.timeEnd,
    this.allocation,
    this.megaFlag,
    this.addressLine1,
    this.addressLine2,
    this.createdBy,
    this.createdTs,
  });

  int? id;
  int? idProgram;
  int? idKohot;
  String? idAdun;
  String? siteName;
  DateTime? date;
  String? timeStart;
  String? timeEnd;
  String? allocation;
  int? megaFlag;
  String? addressLine1;
  String? addressLine2;
  int? createdBy;
  DateTime? createdTs;

  factory AvailabelVenuData.fromJson(Map<String, dynamic> json) =>
      AvailabelVenuData(
        id: json["id"],
        idProgram: json["id_program"],
        idKohot: json["id_kohot"],
        idAdun: json["id_adun"],
        siteName: json["site_name"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        timeStart: json["time_start"],
        timeEnd: json["time_end"],
        allocation: json["allocation"],
        megaFlag: json["mega_flag"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        createdBy: json["created_by"],
        createdTs: json["created_ts"] == null
            ? null
            : DateTime.parse(json["created_ts"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_program": idProgram,
        "id_kohot": idKohot,
        "id_adun": idAdun,
        "site_name": siteName,
        "date":
            "${date!.year.toString().padLeft(4, '0')}-${date!.month.toString().padLeft(2, '0')}-${date!.day.toString().padLeft(2, '0')}",
        "time_start": timeStart,
        "time_end": timeEnd,
        "allocation": allocation,
        "mega_flag": megaFlag,
        "address_line1": addressLine1,
        "address_line2": addressLine2,
        "created_by": createdBy,
        "created_ts": createdTs?.toIso8601String(),
      };
}
