// To parse this JSON data, do
//
//     final venueDate = venueDateFromJson(jsonString);

import 'dart:convert';

VenueDate venueDateFromJson(String str) => VenueDate.fromJson(json.decode(str));

String venueDateToJson(VenueDate data) => json.encode(data.toJson());

class VenueDate {
  VenueDate({
    this.code,
    this.data,
  });

  int? code;
  List<DateTime>? data;

  factory VenueDate.fromJson(Map<String, dynamic> json) => VenueDate(
        code: json["code"],
        data: json["data"] == null
            ? []
            : List<DateTime>.from(json["data"].map((x) => DateTime.parse(x))),
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) =>
                "${x.year.toString().padLeft(4, '0')}-${x.month.toString().padLeft(2, '0')}-${x.day.toString().padLeft(2, '0')}")),
      };
}
