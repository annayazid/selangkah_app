// To parse this JSON data, do
//
//     final getProvider = getProviderFromJson(jsonString);

import 'dart:convert';

GetProvider getProviderFromJson(String str) =>
    GetProvider.fromJson(json.decode(str));

class GetProvider {
  GetProvider({
    this.code,
    this.data,
  });

  int? code;
  List<ProviderData>? data;

  factory GetProvider.fromJson(Map<String, dynamic> json) => GetProvider(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<ProviderData>.from(
                json["Data"].map((x) => ProviderData.fromJson(x))),
      );
}

class ProviderData {
  ProviderData({
    this.idProvider,
    this.ppvName,
    this.idProgramSetting,
  });

  String? idProvider;
  String? ppvName;
  String? idProgramSetting;

  factory ProviderData.fromJson(Map<String, dynamic> json) => ProviderData(
        idProvider: json["id_provider"] == null ? null : json["id_provider"],
        ppvName: json["ppv_name"] == null ? null : json["ppv_name"],
        idProgramSetting: json["id_program_setting"] == null
            ? null
            : json["id_program_setting"],
      );
}
