// To parse this JSON data, do
//
//     final couponStatus = couponStatusFromJson(jsonString);

import 'dart:convert';

CouponStatus couponStatusFromJson(String str) =>
    CouponStatus.fromJson(json.decode(str));

String couponStatusToJson(CouponStatus data) => json.encode(data.toJson());

class CouponStatus {
  CouponStatus({
    this.code,
    this.data,
  });

  int? code;
  List<CouponData>? data;

  factory CouponStatus.fromJson(Map<String, dynamic> json) => CouponStatus(
        code: json["code"],
        data: json["data"] == null
            ? []
            : List<CouponData>.from(
                json["data"].map((x) => CouponData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class CouponData {
  CouponData({
    this.id,
    this.paramName,
    this.status,
    this.barcodeDir,
    this.expiryOffer,
    this.redFlag,
    this.title,
    this.content,
    this.image,
    this.idProgramSetting,
    this.finalFlag,
    this.special,
    this.plus,
  });

  int? id;
  String? paramName;
  int? status;
  String? barcodeDir;
  DateTime? expiryOffer;
  int? redFlag;
  String? title;
  String? content;
  String? image;
  int? idProgramSetting;
  int? finalFlag;
  String? special;
  bool? plus;

  factory CouponData.fromJson(Map<String, dynamic> json) => CouponData(
        id: json["id"],
        paramName: json["param_name"],
        status: json["status"],
        barcodeDir: json["barcode_dir"],
        expiryOffer: json["expiry_offer"] == null
            ? null
            : DateTime.parse(json["expiry_offer"]),
        redFlag: json["red_flag"],
        title: json["title"],
        content: json["content"],
        image: json["image"],
        idProgramSetting: json["id_program_setting"],
        finalFlag: json["final_flag"],
        special: json["special"],
        plus: json["plus"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "param_name": paramName,
        "status": status,
        "barcode_dir": barcodeDir,
        "expiry_offer":
            "${expiryOffer!.year.toString().padLeft(4, '0')}-${expiryOffer!.month.toString().padLeft(2, '0')}-${expiryOffer!.day.toString().padLeft(2, '0')}",
        "red_flag": redFlag,
        "title": title,
        "content": content,
        "image": image,
        "id_program_setting": idProgramSetting,
        "final_flag": finalFlag,
        "special": special,
        "plus": plus,
      };
}
