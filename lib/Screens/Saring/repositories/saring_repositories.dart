import 'dart:convert';
import 'dart:developer';

import 'package:date_format/date_format.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/Saring/saring.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class SaringRepositories {
  static Future<String> getToken() async {
    Map map = {
      "email": EMAIL,
      "password": PASSWORD,
    };

    String body = json.encode(map);

    print('calling post get token');

    final response = await http.post(
      Uri.parse(AUTH),
      headers: {'Content-Type': 'application/json'},
      body: body,
    );

    var data = json.decode(response.body);

    String token = data['data'];
    print(token);

    return token;
  }

  static Future<CouponStatus> getCoupon() async {
    String id = await SecureStorage().readSecureData('userId');

    String token = await getToken();
    String? kohot = await getKohot();

    print('kohit is $kohot');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'id_kohot': kohot!,
    };

    print(map);
    print('calling applicant coupon status');
    final response = await http.post(
      Uri.parse('$SARINGV2/applicant_coupon_status'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer $token',
      },
      body: map,
    );

    log(response.body);

    try {
      CouponStatus coupon = couponStatusFromJson(response.body);
      // CouponStatus coupon = couponFromJson(
      //     '{"code":200,"data":[{"id":2,"param_name":"Physical Assessment (Saringan Fizikal) ","status":1,"barcode_dir":"https://app.selangkah.my/saring/images/SSR230722108821.png","expiry_offer":"2022-12-12","red_flag":0,"title":"PEMERIKSAAN FIZIKAL","content":"Pemeriksaan yang dilakukan di tapak adalah","image":"https://app.selangkah.my/saring/images/Physicalpic.png","id_program_setting":2,"final_flag":0},{"id":3,"param_name":"Blood Test (Ujian Darah)","status":1,"barcode_dir":"https://app.selangkah.my/saring/images/SSR230722108822.png","expiry_offer":"2022-12-12","red_flag":0,"title":"PENGAMBILAN SAMPEL DARAH","content":"Nota penting : Sampel darah peserta akan diambil di tapak","image":"https://app.selangkah.my/saring/images/Picture2.png","id_program_setting":3,"final_flag":0},{"id":10,"param_name":"Eye Screening (Saringan Mata)","status":1,"barcode_dir":"https://app.selangkah.my/saring/images/SSR230722108823.png","expiry_offer":"2022-12-12","red_flag":0,"title":"PEMERIKSAAN MATA","content":"Pemeriksaan yang dilakukan di tapak adalah :","image":"https://app.selangkah.my/saring/images/Picture4.png","id_program_setting":10,"final_flag":0}]}');
      return coupon;
    } catch (e) {
      print('error ${e.toString()}');
      CouponStatus coupon = CouponStatus(code: 200, data: []);
      return coupon;
    }
  }

  static Future<AvailableVenue> getAvailableVenue() async {
    String id = await SecureStorage().readSecureData('userId');

    String token = await getToken();
    String? kohot = await getKohot();

    print('kohot is $kohot');

    if (kohot == null) {
      AvailableVenue availableVenue = AvailableVenue(code: 200, data: []);
      return availableVenue;
    } else {
      final Map<String, String> map = {
        'token': TOKEN,
        'id_selangkah_user': id,
        'id_kohot': kohot,
      };

      print(map);
      print('calling avaialble venue');
      final response = await http.post(
        Uri.parse('$SARINGV2/get_available_venue'),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Bearer $token',
        },
        body: map,
      );

      log(response.body);

      try {
        AvailableVenue availableVenue = availableVenueFromJson(response.body);
        return availableVenue;
      } catch (e) {
        print('error ${e.toString()}');
        AvailableVenue availableVenue = AvailableVenue(code: 200, data: []);
        return availableVenue;
      }
    }
  }

  static Future<String?> getKohot() async {
    String token = await getToken();

    final response = await http.post(
      Uri.parse('$SARINGV2/current_kohot'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer $token',
      },
      // body: map,
    );

    log(response.body);

    try {
      CurrentKohot currentKohot = currentKohotFromJson(response.body);
      return '${currentKohot.data!.id}';
    } catch (e) {
      print('error ${e.toString()}');
      return null;
    }
  }

  static Future<VenueDate> getVenueDate(String venueId) async {
    String token = await getToken();

    final Map<String, String> map = {'id_saring_event': venueId};

    print(map);
    print('calling venue date');
    final response = await http.post(
      Uri.parse('$SARINGV2/get_venue_date'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer $token',
      },
      body: map,
    );

    log(response.body);

    try {
      VenueDate venueDate = venueDateFromJson(response.body);
      return venueDate;
    } catch (e) {
      print('error ${e.toString()}');
      VenueDate venueDate = VenueDate(code: 200, data: []);
      return venueDate;
    }
  }

  static Future<void> sendCoupon(
    DateTime selectedDate,
    List<String> selectedCouponId,
    String selectedVenueId,
  ) async {
    String id = await SecureStorage().readSecureData('userId');
    String token = await getToken();
    String? kohot = await getKohot();
    String selectedCouponIdCombined = selectedCouponId.join(',');
    // print(s);

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'id_saring_event': selectedVenueId,
      'date': formatDate(selectedDate, [yyyy, '-', mm, '-', dd]),
      'accept': '1',
      'list_coupon': selectedCouponIdCombined,
      'id_kohot': kohot!,
    };

    print(map);
    print('calling sendCoupon');
    final response = await http.post(
      Uri.parse('$SARINGV2/accept_offer_saring'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer $token',
      },
      body: map,
    );

    log(response.body);
  }

  static Future<SaringDetails?> getDetails() async {
    String id = await SecureStorage().readSecureData('userId');
    String token = await getToken();
    String? kohot = await getKohot();

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'id_kohot': kohot!,
    };

    print(map);
    print('calling getDetails');
    final response = await http.post(
      Uri.parse('$SARINGV2/get_saring_details_app'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer $token',
      },
      body: map,
    );

    log(response.body);

    try {
      SaringDetails saringDetails = saringDetailsFromJson(response.body);
      return saringDetails;
    } catch (e) {
      print('error ${e.toString()}');
      return null;
    }
  }

  static Future<void> updateStatusCoupon() async {
    String id = await SecureStorage().readSecureData('userId');
    String token = await getToken();
    String? kohot = await getKohot();

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'accept': '1',
      'id_kohot': kohot!,
    };

    print(map);
    print('calling getDetails');
    final response = await http.post(
      Uri.parse('$SARINGV2/accept_offer_saring'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer $token',
      },
      body: map,
    );

    log(response.body);
  }

  static Future<bool> getStatusBypass() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    print('calling api bypass');

    final response = await http.post(
      Uri.parse('$SARING_DATA/check_ekyc_bypass'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    var data = json.decode(response.body);

    String status = data['Data'];

    if (status == '1') {
      return true;
    } else {
      return false;
    }
  }

  static Future<CouponStatus> getCouponLast() async {
    String id = await SecureStorage().readSecureData('userId');

    String token = await getToken();
    String? kohot = await getKohot();

    int lastKohot = int.parse(kohot!) - 1;
    // int lastKohot = int.parse(kohot) ;

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'id_kohot': '$lastKohot',
    };

    print(map);
    print('calling applicant coupon status last');
    final response = await http.post(
      Uri.parse('$SARINGV2/applicant_coupon_status'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer $token',
      },
      body: map,
    );

    log(response.body);

    try {
      CouponStatus coupon = couponStatusFromJson(response.body);
      // CouponStatus coupon = couponFromJson(
      //     '{"code":200,"data":[{"id":2,"param_name":"Physical Assessment (Saringan Fizikal) ","status":1,"barcode_dir":"https://app.selangkah.my/saring/images/SSR230722108821.png","expiry_offer":"2022-12-12","red_flag":0,"title":"PEMERIKSAAN FIZIKAL","content":"Pemeriksaan yang dilakukan di tapak adalah","image":"https://app.selangkah.my/saring/images/Physicalpic.png","id_program_setting":2,"final_flag":0},{"id":3,"param_name":"Blood Test (Ujian Darah)","status":1,"barcode_dir":"https://app.selangkah.my/saring/images/SSR230722108822.png","expiry_offer":"2022-12-12","red_flag":0,"title":"PENGAMBILAN SAMPEL DARAH","content":"Nota penting : Sampel darah peserta akan diambil di tapak","image":"https://app.selangkah.my/saring/images/Picture2.png","id_program_setting":3,"final_flag":0},{"id":10,"param_name":"Eye Screening (Saringan Mata)","status":1,"barcode_dir":"https://app.selangkah.my/saring/images/SSR230722108823.png","expiry_offer":"2022-12-12","red_flag":0,"title":"PEMERIKSAAN MATA","content":"Pemeriksaan yang dilakukan di tapak adalah :","image":"https://app.selangkah.my/saring/images/Picture4.png","id_program_setting":10,"final_flag":0}]}');
      return coupon;
    } catch (e) {
      print('error ${e.toString()}');
      CouponStatus coupon = CouponStatus(code: 200, data: []);
      return coupon;
    }
  }

  static Future<AvailabilityCoupon> getAvailabilityCoupon(
      String listCouponId) async {
    String id = await SecureStorage().readSecureData('userId');

    String token = await getToken();
    String? kohot = await getKohot();

    print('kohot is $kohot');

    if (kohot == null) {
      AvailabilityCoupon availabilityCoupon =
          AvailabilityCoupon(code: 200, data: []);
      return availabilityCoupon;
    } else {
      final Map<String, String> map = {
        'token': TOKEN,
        'id_selangkah_user': id,
        'id_kohot': kohot,
        'list_coupon': listCouponId,
      };

      print(map);
      print('calling avaialble venue new');
      final response = await http.post(
        Uri.parse('$SARINGV2/get_venue_new'),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Bearer $token',
        },
        body: map,
      );

      log(response.body);

      try {
        AvailabilityCoupon availabilityCoupon =
            availabilityCouponFromJson(response.body);
        print('return data betul');
        return availabilityCoupon;
      } catch (e) {
        print('error ${e.toString()}');
        AvailabilityCoupon availabilityCoupon =
            AvailabilityCoupon(code: 200, data: []);
        return availabilityCoupon;
      }
    }
  }

  static Future<GetProvider> getProvider(String id) async {
    String selId = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'test_id': id,
      'id_selangkah_user': selId,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$SARING_DATA/get_provider'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: map,
    );

    log(response.body);

    try {
      GetProvider provider = getProviderFromJson(response.body);
      // GetProvider provider = getProviderFromJson(
      //     '{"Code":200,"Data":[{"id_provider":"101","ppv_name":"Danial","id_program_setting":"15"},{"id_provider":"101","ppv_name":"Ma","id_program_setting":"15"},{"id_provider":"101","ppv_name":"Arum","id_program_setting":"15"}]}');
      return provider;
    } catch (e) {
      GetProvider provider = GetProvider(code: 200, data: []);
      return provider;
    }
  }

  static Future<void> createAppointment(
      String idProvider, String testId, String? slotDate) async {
    String id = await SecureStorage().readSecureData('userId');
    Map<String, String> map;
    if (slotDate != null) {
      map = {
        'token': TOKEN,
        'id_selangkah_user': id,
        'test_id': testId,
        'id_provider': idProvider,
        'slot_date': slotDate,
      };
    } else {
      map = {
        'token': TOKEN,
        'id_selangkah_user': id,
        'test_id': testId,
        'id_provider': idProvider,
      };
    }
    print(map);

    final response = await http.post(
      Uri.parse('$SARING_DATA/create_appointment'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: map,
    );

    log(response.body);

    if (response.body.contains('true')) {
      Fluttertoast.showToast(msg: 'appointment_created'.tr());
    } else {
      Fluttertoast.showToast(
          msg: 'No appointment created yet. Try again later');
    }
  }

  static Future<void> changeAppointment(
    String idProvider,
    String testId,
    String? slotDate,
    String idProgramAppointment,
  ) async {
    String id = await SecureStorage().readSecureData('userId');
    Map<String, String> map;
    if (slotDate != null) {
      map = {
        'token': TOKEN,
        'id_selangkah_user': id,
        'test_id': testId,
        'id_provider': idProvider,
        'slot_date': slotDate,
        'id_program_appointment': idProgramAppointment,
      };
    } else {
      map = {
        'token': TOKEN,
        'id_selangkah_user': id,
        'test_id': testId,
        'id_provider': idProvider,
        'id_program_appointment': idProgramAppointment,
      };
    }

    print(map);

    final response = await http.post(
      Uri.parse('$SARING_DATA/change_appointment'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: map,
    );

    print(response.body);
  }

  static Future<GetDateTime> getDateTime(String idProvider) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'id_provider': idProvider,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$SARING_DATA/get_slot_date'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: map,
    );

    log(response.body);

    try {
      GetDateTime dateTime = getDateTimeFromJson(response.body);
      return dateTime;
    } catch (e) {
      GetDateTime dateTime = GetDateTime(code: 200, data: []);
      return dateTime;
    }
  }
}
