import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';

part 'get_coupon_state.dart';

class GetCouponCubit extends Cubit<GetCouponState> {
  GetCouponCubit() : super(GetCouponInitial());

  Future<void> getCoupon() async {
    emit(GetCouponLoading());

    //get coupon
    CouponStatus couponStatus = await SaringRepositories.getCoupon();

    //1 - tak accept lagi
    //10 - belum redeem
    //11 - rejected
    //12 - dah redeem

    if (couponStatus.data!.any((element) =>
        element.status == 10 || element.status == 11 || element.status == 12)) {
      emit(GetCouponBarcode());
      // print('redirect to page barcode');
    } else {
      emit(GetCouponLoaded(couponStatus));
    }
  }
}
