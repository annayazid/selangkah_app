part of 'get_coupon_cubit.dart';

@immutable
abstract class GetCouponState {
  const GetCouponState();
}

class GetCouponInitial extends GetCouponState {
  const GetCouponInitial();
}

class GetCouponLoading extends GetCouponState {
  const GetCouponLoading();
}

class GetCouponLoaded extends GetCouponState {
  final CouponStatus couponStatus;

  GetCouponLoaded(this.couponStatus);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetCouponLoaded && other.couponStatus == couponStatus;
  }

  @override
  int get hashCode => couponStatus.hashCode;
}

class GetCouponBarcode extends GetCouponState {
  const GetCouponBarcode();
}
