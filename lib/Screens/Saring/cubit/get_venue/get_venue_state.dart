part of 'get_venue_cubit.dart';

@immutable
abstract class GetVenueState {
  const GetVenueState();
}

class GetVenueInitial extends GetVenueState {
  const GetVenueInitial();
}

class GetVenueLoading extends GetVenueState {
  const GetVenueLoading();
}

class GetVenueLoaded extends GetVenueState {
  final AvailableVenue availableVenue;

  GetVenueLoaded(this.availableVenue);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetVenueLoaded && other.availableVenue == availableVenue;
  }

  @override
  int get hashCode => availableVenue.hashCode;
}
