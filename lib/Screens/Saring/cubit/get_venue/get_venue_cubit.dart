import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';

part 'get_venue_state.dart';

class GetVenueCubit extends Cubit<GetVenueState> {
  GetVenueCubit() : super(GetVenueInitial());

  Future<void> getAvailableVenue() async {
    emit(GetVenueLoading());

    AvailableVenue availableVenue =
        await SaringRepositories.getAvailableVenue();

    emit(GetVenueLoaded(availableVenue));
  }
}
