import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';

part 'update_status_coupon_state.dart';

class UpdateStatusCouponCubit extends Cubit<UpdateStatusCouponState> {
  UpdateStatusCouponCubit() : super(UpdateStatusCouponInitial());

  Future<void> updateStatusCoupon() async {
    emit(UpdateStatusCouponLoading());

    //process
    // await Future.delayed(Duration(seconds: 2));

    await SaringRepositories.updateStatusCoupon();

    emit(UpdateStatusCouponLoaded());
  }
}
