part of 'update_status_coupon_cubit.dart';

@immutable
abstract class UpdateStatusCouponState {
  const UpdateStatusCouponState();
}

class UpdateStatusCouponInitial extends UpdateStatusCouponState {
  const UpdateStatusCouponInitial();
}

class UpdateStatusCouponLoading extends UpdateStatusCouponState {
  const UpdateStatusCouponLoading();
}

class UpdateStatusCouponLoaded extends UpdateStatusCouponState {
  const UpdateStatusCouponLoaded();
}
