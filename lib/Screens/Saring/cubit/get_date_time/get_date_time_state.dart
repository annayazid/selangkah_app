part of 'get_date_time_cubit.dart';

@immutable
abstract class GetDateTimeState {
  const GetDateTimeState();
}

class GetDateTimeInitial extends GetDateTimeState {
  const GetDateTimeInitial();
}

class GetDateTimeLoading extends GetDateTimeState {
  const GetDateTimeLoading();
}

class GetDateTimeLoaded extends GetDateTimeState {
  final GetDateTime dateTime;

  GetDateTimeLoaded(this.dateTime);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetDateTimeLoaded && other.dateTime == dateTime;
  }

  @override
  int get hashCode => dateTime.hashCode;
}
