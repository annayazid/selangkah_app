import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';

part 'get_date_time_state.dart';

class GetDateTimeCubit extends Cubit<GetDateTimeState> {
  GetDateTimeCubit() : super(GetDateTimeInitial());
  Future<void> resetDateTime() async {
    emit(GetDateTimeInitial());
  }

  Future<void> getDateTime(String idProvider) async {
    emit(GetDateTimeLoading());

    //process
    GetDateTime dateTime = await SaringRepositories.getDateTime(idProvider);

    emit(GetDateTimeLoaded(dateTime));
  }
}
