import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

part 'get_saring_details_state.dart';

class GetSaringDetailsCubit extends Cubit<GetSaringDetailsState> {
  GetSaringDetailsCubit() : super(GetSaringDetailsInitial());

  Future<void> getDetails() async {
    emit(GetSaringDetailsLoading());

    SaringDetails? saringDetails = await SaringRepositories.getDetails();

    CouponStatus couponStatus = await SaringRepositories.getCoupon();

    couponStatus.data!.removeWhere((element) =>
        element.status != 10 &&
        element.status != 11 &&
        element.status != 12 &&
        element.status != 3);

    String icNo = await SecureStorage().readSecureData('userIdNo');

    emit(GetSaringDetailsLoaded(saringDetails!, couponStatus, icNo));
  }
}
