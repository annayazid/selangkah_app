part of 'get_saring_details_cubit.dart';

@immutable
abstract class GetSaringDetailsState {
  const GetSaringDetailsState();
}

class GetSaringDetailsInitial extends GetSaringDetailsState {
  const GetSaringDetailsInitial();
}

class GetSaringDetailsLoading extends GetSaringDetailsState {
  const GetSaringDetailsLoading();
}

class GetSaringDetailsLoaded extends GetSaringDetailsState {
  final SaringDetails saringDetails;
  final CouponStatus couponStatus;
  final String icNo;

  GetSaringDetailsLoaded(this.saringDetails, this.couponStatus, this.icNo);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetSaringDetailsLoaded &&
        other.saringDetails == saringDetails &&
        other.couponStatus == couponStatus &&
        other.icNo == icNo;
  }

  @override
  int get hashCode =>
      saringDetails.hashCode ^ couponStatus.hashCode ^ icNo.hashCode;
}
