part of 'create_appointment_cubit.dart';

@immutable
abstract class CreateAppointmentState {
  const CreateAppointmentState();
}

class CreateAppointmentInitial extends CreateAppointmentState {
  const CreateAppointmentInitial();
}

class CreateAppointmentLoading extends CreateAppointmentState {
  const CreateAppointmentLoading();
}

class CreateAppointmentLoaded extends CreateAppointmentState {
  const CreateAppointmentLoaded();
}

class CreateAppointmentLoadedWithPopUp extends CreateAppointmentState {
  const CreateAppointmentLoadedWithPopUp();
}
