import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';

part 'create_appointment_state.dart';

class CreateAppointmentCubit extends Cubit<CreateAppointmentState> {
  CreateAppointmentCubit() : super(CreateAppointmentInitial());
  Future<void> createAppointment(
      String idProvider, String testId, String? slotDate) async {
    emit(CreateAppointmentLoading());

    //process
    print('Status Accept API $idProvider, test id $testId, Date $slotDate');

    await SaringRepositories.createAppointment(idProvider, testId, slotDate);

    if (slotDate != null) {
      emit(CreateAppointmentLoaded());
    } else {
      emit(CreateAppointmentLoadedWithPopUp());
    }
  }

  Future<void> changeAppointment(String idProvider, String testId,
      String? slotDate, String idProgramAppointment) async {
    emit(CreateAppointmentLoading());

    //process
    // print('Status Accept API $idProvider, test id $testId, Date $slotDate');

    await SaringRepositories.changeAppointment(
      idProvider,
      testId,
      slotDate,
      idProgramAppointment,
    );

    if (slotDate != null) {
      emit(CreateAppointmentLoaded());
    } else {
      emit(CreateAppointmentLoadedWithPopUp());
    }
  }
}
