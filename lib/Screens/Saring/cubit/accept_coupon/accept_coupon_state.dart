part of 'accept_coupon_cubit.dart';

@immutable
abstract class AcceptCouponState {
  const AcceptCouponState();
}

class AcceptCouponInitial extends AcceptCouponState {
  const AcceptCouponInitial();
}

class AcceptCouponLoading extends AcceptCouponState {
  const AcceptCouponLoading();
}

class AcceptCouponLoaded extends AcceptCouponState {
  const AcceptCouponLoaded();
}
