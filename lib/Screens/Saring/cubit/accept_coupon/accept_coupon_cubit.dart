import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';

part 'accept_coupon_state.dart';

class AcceptCouponCubit extends Cubit<AcceptCouponState> {
  AcceptCouponCubit() : super(AcceptCouponInitial());

  Future<void> sendCoupon({
    required DateTime selectedDate,
    required List<String> selectedCouponId,
    required String selectedVenueId,
  }) async {
    emit(AcceptCouponLoading());

    await SaringRepositories.sendCoupon(
        selectedDate, selectedCouponId, selectedVenueId);

    emit(AcceptCouponLoaded());
  }
}
