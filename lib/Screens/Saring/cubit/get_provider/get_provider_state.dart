part of 'get_provider_cubit.dart';

@immutable
abstract class GetProviderState {
  const GetProviderState();
}

class GetProviderInitial extends GetProviderState {
  const GetProviderInitial();
}

class GetProviderNoData extends GetProviderState {
  const GetProviderNoData();
}

class GetProviderLoading extends GetProviderState {
  const GetProviderLoading();
}

class GetProviderLoaded extends GetProviderState {
  final GetProvider provider;

  GetProviderLoaded(this.provider);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetProviderLoaded && other.provider == provider;
  }

  @override
  int get hashCode => provider.hashCode;
}
