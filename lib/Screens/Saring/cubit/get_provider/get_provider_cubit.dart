import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';

part 'get_provider_state.dart';

class GetProviderCubit extends Cubit<GetProviderState> {
  GetProviderCubit() : super(GetProviderInitial());
  Future<void> getProvider(String id) async {
    emit(GetProviderLoading());

    //process
    GetProvider provider = await SaringRepositories.getProvider(id);

    // for (var i = 0; i < provider.data.length; i++) {
    //   providerList.add(provider.data[i].ppvName);
    // }

    emit(GetProviderLoaded(provider));
  }
}
