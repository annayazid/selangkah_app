part of 'get_availability_coupon_cubit.dart';

@immutable
abstract class GetAvailabilityCouponState {
  const GetAvailabilityCouponState();
}

class GetAvailabilityCouponInitial extends GetAvailabilityCouponState {
  const GetAvailabilityCouponInitial();
}

class GetAvailabilityCouponLoading extends GetAvailabilityCouponState {
  const GetAvailabilityCouponLoading();
}

class GetAvailabilityCouponLoaded extends GetAvailabilityCouponState {
  final AvailabilityCoupon availabilityCoupon;

  GetAvailabilityCouponLoaded(this.availabilityCoupon);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetAvailabilityCouponLoaded &&
        other.availabilityCoupon == availabilityCoupon;
  }

  @override
  int get hashCode => availabilityCoupon.hashCode;
}
