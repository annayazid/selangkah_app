import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';

part 'get_availability_coupon_state.dart';

class GetAvailabilityCouponCubit extends Cubit<GetAvailabilityCouponState> {
  GetAvailabilityCouponCubit() : super(GetAvailabilityCouponInitial());

  Future<void> getAvailabilityCoupon(List<String> couponId) async {
    emit(GetAvailabilityCouponLoading());

    String combinedCouponId = couponId.join(',');

    AvailabilityCoupon availabilityCoupon =
        await SaringRepositories.getAvailabilityCoupon(combinedCouponId);

    emit(GetAvailabilityCouponLoaded(availabilityCoupon));
  }
}
