import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';

part 'get_venue_date_state.dart';

class GetVenueDateCubit extends Cubit<GetVenueDateState> {
  GetVenueDateCubit() : super(GetVenueDateInitial());

  Future<void> getVenueDate(String venueId, List<String> couponId) async {
    emit(GetVenueDateLoading());

    String combinedCouponId = couponId.join(',');

    AvailabilityCoupon availabilityCoupon =
        await SaringRepositories.getAvailabilityCoupon(combinedCouponId);

    VenueDate venueDate = await SaringRepositories.getVenueDate(venueId);

    emit(GetVenueDateLoaded(venueDate, availabilityCoupon));
  }
}
