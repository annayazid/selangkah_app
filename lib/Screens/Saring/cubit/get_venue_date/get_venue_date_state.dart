part of 'get_venue_date_cubit.dart';

@immutable
abstract class GetVenueDateState {
  const GetVenueDateState();
}

class GetVenueDateInitial extends GetVenueDateState {
  const GetVenueDateInitial();
}

class GetVenueDateLoading extends GetVenueDateState {
  const GetVenueDateLoading();
}

class GetVenueDateLoaded extends GetVenueDateState {
  final VenueDate venueDate;
  final AvailabilityCoupon availabilityCoupon;

  GetVenueDateLoaded(this.venueDate, this.availabilityCoupon);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetVenueDateLoaded &&
        other.venueDate == venueDate &&
        other.availabilityCoupon == availabilityCoupon;
  }

  @override
  int get hashCode => venueDate.hashCode ^ availabilityCoupon.hashCode;
}
