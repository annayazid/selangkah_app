import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';
import 'package:selangkah_new/utils/constants.dart';

class SaringVenue extends StatefulWidget {
  final List<String> selectVoucherId;
  final CouponStatus couponStatus;

  const SaringVenue(
      {Key? key, required this.selectVoucherId, required this.couponStatus})
      : super(key: key);

  @override
  _SaringVenueState createState() => _SaringVenueState();
}

class _SaringVenueState extends State<SaringVenue> {
  @override
  void initState() {
    context.read<GetVenueCubit>().getAvailableVenue();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SaringScaffold(
      appBarTitle: 'Selangor Saring',
      scrollableBanner: false,
      content: BlocBuilder<GetVenueCubit, GetVenueState>(
        builder: (context, state) {
          if (state is GetVenueLoaded) {
            return AvailableVenueLoadedWidget(
              couponStatus: widget.couponStatus,
              selectedCouponId: widget.selectVoucherId,
              availableVenue: state.availableVenue,
            );
          } else {
            return Center(
              child: SpinKitFadingCircle(
                color: kPrimaryColor,
                size: 20,
              ),
            );
          }
        },
      ),
    );
  }
}

class AvailableVenueLoadedWidget extends StatefulWidget {
  final AvailableVenue availableVenue;
  final CouponStatus couponStatus;
  final List<String> selectedCouponId;

  const AvailableVenueLoadedWidget(
      {Key? key,
      required this.availableVenue,
      required this.couponStatus,
      required this.selectedCouponId})
      : super(key: key);

  @override
  _AvailableVenueLoadedWidgetState createState() =>
      _AvailableVenueLoadedWidgetState();
}

class _AvailableVenueLoadedWidgetState
    extends State<AvailableVenueLoadedWidget> {
  String? choosenVenueId;
  int? choosenVenueIndex;
  DateTime? chosenDate;
  int? chosenDateIndex;

  AvailabilityCouponData? availabilityCouponData;

  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Scrollbar(
            controller: scrollController,
            thumbVisibility: true,
            thickness: 2.5,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.3),
                          blurRadius: 1,
                          spreadRadius: 1,
                          offset: Offset(0, 0),
                        ),
                      ],
                    ),
                    child: Column(
                      children: [
                        //yello warning
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Colors.yellow,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Text(
                              'program_session_depend'.tr(),
                              style: TextStyle(
                                color: Colors.red,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),

                        //venue place
                        SizedBox(height: 10),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                'sitename'.tr(),
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  height: 2,
                                ),
                              ),
                            ),

                            //if got venue
                            if (widget.availableVenue.data!.isNotEmpty)
                              Expanded(
                                flex: 2,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: InputDecorator(
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.symmetric(
                                        horizontal: 15,
                                      ),
                                      errorStyle: TextStyle(
                                          color: Colors.redAccent,
                                          fontSize: 16.0),
                                      border: InputBorder.none,
                                    ),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton<String>(
                                        isExpanded: true,
                                        icon: Icon(
                                          Icons.keyboard_arrow_down,
                                          size: 30,
                                          color: Color(0xFFF36B21),
                                        ),
                                        hint: Text(
                                          'chooseLocation'.tr(),
                                          style: TextStyle(
                                            fontSize: 14,
                                          ),
                                        ),
                                        value: choosenVenueId,
                                        onChanged: (String? value) {
                                          //set state venue id and index
                                          setState(() {
                                            choosenVenueId = value;

                                            choosenVenueIndex = widget
                                                .availableVenue.data!
                                                .indexWhere((element) =>
                                                    element.id ==
                                                    int.parse(value!));
                                            chosenDate = null;
                                            availabilityCouponData = null;
                                          });

                                          //calling context time

                                          context
                                              .read<GetVenueDateCubit>()
                                              .getVenueDate(value!,
                                                  widget.selectedCouponId);
                                        },
                                        items: widget.availableVenue.data!
                                            .map((e) {
                                          return DropdownMenuItem<String>(
                                            value: '${e.id}',
                                            child: Text(
                                              '${e.siteName}',
                                              style: TextStyle(
                                                fontSize: 14,
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 4,
                                            ),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  ),
                                ),
                              ),

                            //if no venue
                            if (widget.availableVenue.data!.isEmpty)
                              Expanded(
                                flex: 2,
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                  child: Text(
                                    'no_program_session'.tr(),
                                    style: TextStyle(
                                      color: Colors.red,
                                      height: 2,
                                    ),
                                  ),
                                ),
                              ),
                          ],
                        ),

                        //venue address
                        if (choosenVenueId != null &&
                            choosenVenueIndex != null) ...[
                          SizedBox(height: 10),
                          Row(
                            children: [
                              Expanded(
                                child: Text(
                                  'location'.tr(),
                                  style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    height: 2,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Text(
                                    '${widget.availableVenue.data![choosenVenueIndex!].addressLine1}, ${widget.availableVenue.data![choosenVenueIndex!].addressLine2}'),
                              ),
                            ],
                          ),
                        ],

                        //venue time
                        BlocConsumer<GetVenueDateCubit, GetVenueDateState>(
                          listener: (context, state) {
                            // if (state is GetVenueDateLoaded) {
                            //   setState(() {
                            //     availabilityCoupon = state.availabilityCoupon;
                            //   });
                            // }
                          },
                          builder: (context, state) {
                            if (state is GetVenueDateInitial) {
                              return Container();
                            } else if (state is GetVenueDateLoaded) {
                              return Column(
                                children: [
                                  SizedBox(height: 10),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          'date'.tr(),
                                          style: TextStyle(
                                            fontWeight: FontWeight.w700,
                                            height: 2,
                                          ),
                                        ),
                                      ),
                                      if (state.venueDate.data!.isNotEmpty)
                                        Expanded(
                                          flex: 2,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              border: Border.all(),
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            child: InputDecorator(
                                              decoration: InputDecoration(
                                                contentPadding:
                                                    EdgeInsets.symmetric(
                                                  horizontal: 15,
                                                ),
                                                errorStyle: TextStyle(
                                                    color: Colors.redAccent,
                                                    fontSize: 16.0),
                                                border: InputBorder.none,
                                              ),
                                              child:
                                                  DropdownButtonHideUnderline(
                                                child: DropdownButtonFormField<
                                                    DateTime>(
                                                  // isDense: false,
                                                  isExpanded: true,
                                                  icon: Icon(
                                                    Icons.keyboard_arrow_down,
                                                    size: 30,
                                                    color: Color(0xFFF36B21),
                                                  ),
                                                  hint: Text(
                                                    'selectdate'.tr(),
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                  value: chosenDate,
                                                  onChanged: (DateTime? value) {
                                                    setState(
                                                      () {
                                                        chosenDate = value;

                                                        chosenDateIndex = state
                                                            .venueDate.data!
                                                            .indexWhere(
                                                                (element) =>
                                                                    element ==
                                                                    chosenDate);

                                                        //here

                                                        availabilityCouponData = state
                                                            .availabilityCoupon
                                                            .data!
                                                            .singleWhere((element) =>
                                                                element.id
                                                                        .toString() ==
                                                                    choosenVenueId &&
                                                                element.date ==
                                                                    value);
                                                      },
                                                    );
                                                    print(chosenDate);
                                                    print(chosenDateIndex);
                                                  },

                                                  items: state.venueDate.data!
                                                      .map((e) {
                                                    return DropdownMenuItem<
                                                        DateTime>(
                                                      value: e,
                                                      child: Text(
                                                        formatDate(e, [
                                                          dd,
                                                          ' ',
                                                          MM,
                                                          ' ',
                                                          yyyy
                                                        ]),
                                                        style: TextStyle(
                                                          fontSize: 14,
                                                        ),
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 3,
                                                      ),
                                                    );
                                                  }).toList(),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      if (state.venueDate.data!.isEmpty)
                                        Expanded(
                                          flex: 2,
                                          child: Text(
                                            'noavailabledatetime'.tr(),
                                            style: TextStyle(
                                              color: Colors.red,
                                            ),
                                          ),
                                        ),
                                    ],
                                  ),
                                ],
                              );
                            } else {
                              return Column(
                                children: [
                                  SizedBox(height: 10),
                                  SpinKitFadingCircle(
                                    color: kPrimaryColor,
                                    size: 20,
                                  ),
                                ],
                              );
                            }
                          },
                        ),

                        //voucher list
                      ],
                    ),
                  ),

                  //list voucher
                  if (availabilityCouponData != null)
                    VoucherAcceptedList(
                      couponStatus: widget.couponStatus,
                      selectedCouponId: widget.selectedCouponId,
                      availabilityCouponData: availabilityCouponData!,
                    ),

                  //button confirm
                ],
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          width: double.infinity,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: chosenDate != null && choosenVenueId != null
                  ? kPrimaryColor
                  : Colors.grey,
            ),
            onPressed: () {
              if (chosenDate != null && choosenVenueId != null) {
                context.read<AcceptCouponCubit>().sendCoupon(
                      selectedCouponId: widget.selectedCouponId,
                      selectedDate: chosenDate!,
                      selectedVenueId: choosenVenueId!,
                    );
              }
            },
            child: BlocConsumer<AcceptCouponCubit, AcceptCouponState>(
              listener: (context, state) {
                if (state is AcceptCouponLoaded) {
                  // print('redirect here');
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) => MultiBlocProvider(
                        providers: [
                          BlocProvider(
                            create: (context) => GetSaringDetailsCubit(),
                          ),
                          BlocProvider(
                            create: (context) => UpdateStatusCouponCubit(),
                          ),
                        ],
                        child: SaringBarcode(),
                      ),
                    ),
                  );
                }
              },
              builder: (context, state) {
                if (state is AcceptCouponLoading) {
                  return SpinKitFadingCircle(
                    color: Colors.white,
                    size: 14,
                  );
                } else {
                  return Text('swabook_confirm'.tr());
                }
              },
            ),
          ),
        ),
        SizedBox(height: 10),
      ],
    );
  }
}

class VoucherAcceptedList extends StatefulWidget {
  final CouponStatus couponStatus;
  final List<String> selectedCouponId;
  final AvailabilityCouponData availabilityCouponData;

  const VoucherAcceptedList(
      {Key? key,
      required this.couponStatus,
      required this.selectedCouponId,
      required this.availabilityCouponData})
      : super(key: key);

  @override
  _VoucherAcceptedListState createState() => _VoucherAcceptedListState();
}

class _VoucherAcceptedListState extends State<VoucherAcceptedList> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int noAvailableCoupons = 0;
    for (var i = 0;
        i < widget.availabilityCouponData.couponSelectedAr!.length;
        i++) {
      if (widget.selectedCouponId.contains(
              widget.availabilityCouponData.couponSelectedAr![i].idCoupon) &&
          widget.availabilityCouponData.couponSelectedAr![i]
                  .couponAvailablity ==
              'available') {
        noAvailableCoupons++;
      }
    }
    return Container(
      width: double.infinity,
      margin: EdgeInsets.fromLTRB(10, 0, 10, 5),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            blurRadius: 1,
            spreadRadius: 1,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (widget.selectedCouponId.length - noAvailableCoupons > 0)
            Row(
              children: [
                Expanded(
                  flex: 3,
                  child: Text(
                    '${widget.selectedCouponId.length - noAvailableCoupons} ${'selected_coupon'.tr()}',
                    style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                // SizedBox(width: 10),

                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      print(
                          'length is ${widget.availabilityCouponData.couponSelectedAr!.length}');
                      showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            content: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  'coupon_quota_full'.tr(),
                                  style: TextStyle(fontSize: 14),
                                  // textAlign: TextAlign.,
                                ),
                                // SizedBox(height: 5),
                                // Text(
                                //   'coupon_list_unavailable'.tr(),
                                //   style: TextStyle(fontSize: 14),
                                // ),
                                // SizedBox(height: 10),
                                // ListView.builder(
                                //   itemCount: widget.availabilityCouponData
                                //       .couponSelectedAr.length,
                                //   shrinkWrap: true,
                                //   itemBuilder: (context, index) {
                                //     print(widget
                                //         .availabilityCouponData
                                //         .couponSelectedAr[index]
                                //         .couponAvailablity);
                                //     if (widget
                                //             .availabilityCouponData
                                //             .couponSelectedAr[index]
                                //             .couponAvailablity !=
                                //         'available') {
                                //       return Text(
                                //         '- ${widget.availabilityCouponData.couponSelectedAr}',
                                //         style: TextStyle(
                                //           fontSize: 14,
                                //           fontWeight: FontWeight.bold,
                                //         ),
                                //       );
                                //     } else {
                                //       return Container();
                                //     }
                                //   },
                                // ),
                                SizedBox(height: 10),
                                Text(
                                  'consider_change_location'.tr(),
                                  style: TextStyle(fontSize: 14),
                                ),
                              ],
                            ),
                          );
                        },
                      );
                    },
                    child: Icon(
                      FontAwesomeIcons.circleInfo,
                      size: 20,
                    ),
                  ),
                ),
                SizedBox(width: 10),
                // Tooltip(
                //   waitDuration: Duration(microseconds: 5),
                //   message: 'Text',
                //   child: Text(
                //     'Flutter',
                //     style: TextStyle(
                //       color: Colors.grey,
                //     ), //TextStyle
                //   ), //Text
                // ),
              ],
            ),
          if (widget.selectedCouponId.length == noAvailableCoupons)
            Text(
              'All selected screenings available',
              style: TextStyle(
                color: Colors.grey,
                fontWeight: FontWeight.bold,
              ),
            ),
          SizedBox(height: 15),
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: widget.selectedCouponId.length,
            itemBuilder: (BuildContext context, int index) {
              CouponData couponData = widget.couponStatus.data!.firstWhere(
                  (element) =>
                      element.id.toString() == widget.selectedCouponId[index]);
              return Column(
                children: [
                  SizedBox(height: 5),
                  Row(
                    children: [
                      if (widget.availabilityCouponData.couponSelectedAr!.any(
                          (element) =>
                              element.idCoupon ==
                                  widget.selectedCouponId[index] &&
                              element.couponAvailablity == 'available'))
                        Icon(
                          FontAwesomeIcons.check,
                          color: Colors.green,
                          size: 14,
                        ),
                      if (widget.availabilityCouponData.couponSelectedAr!.any(
                          (element) =>
                              element.idCoupon ==
                                  widget.selectedCouponId[index] &&
                              element.couponAvailablity != 'available'))
                        Icon(
                          FontAwesomeIcons.xmark,
                          color: Colors.red,
                          size: 14,
                        ),
                      SizedBox(width: 20),
                      Expanded(
                        child: Text(
                          couponData.paramName!,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Image.asset(
                        'assets/images/Saring/kupon.png',
                        width: 20,
                        height: 20,
                      ),
                      SizedBox(width: 10),
                    ],
                  ),
                  SizedBox(height: 5),
                  Divider(),
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}
