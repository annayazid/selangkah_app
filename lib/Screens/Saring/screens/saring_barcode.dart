import 'package:barcode_widget/barcode_widget.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';
import 'package:selangkah_new/utils/AppConstant.dart';
import 'package:selangkah_new/utils/constants.dart';

class SaringBarcode extends StatefulWidget {
  const SaringBarcode({Key? key}) : super(key: key);

  @override
  _SaringBarcodeState createState() => _SaringBarcodeState();
}

class _SaringBarcodeState extends State<SaringBarcode> {
  @override
  void initState() {
    context.read<GetSaringDetailsCubit>().getDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        // resizeToAvoidBottomInset: false,
        appBar: AppBar(
          centerTitle: true,
          // automaticallyImplyLeading: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          title: Text(
            'Selangor Saring',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 15,
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: 'Roboto',
            ),
          ),
        ),
        body: Column(
          children: [
            Container(
              height: height - AppBar().preferredSize.height,
              width: width,
              child: RefreshIndicator(
                color: kPrimaryColor,
                onRefresh: () async {
                  context.read<GetSaringDetailsCubit>().getDetails();
                },
                child: SingleChildScrollView(
                  child: Column(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: double.infinity,
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Image.asset(
                              'assets/images/Saring/selangor-saring-banner-bg.png',
                              // fit: BoxFit.fitHeight,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Center(
                                  child: Container(
                                    width: width * 0.75,
                                    child: Image.asset(
                                      'assets/images/Saring/selangor_saring_logo.png',
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      BlocConsumer<GetSaringDetailsCubit,
                          GetSaringDetailsState>(
                        listener: (context, state) {},
                        builder: (context, state) {
                          if (state is GetSaringDetailsLoaded) {
                            return GetSaringDetailsLoadedWidget(
                              saringDetails: state.saringDetails,
                              couponStatus: state.couponStatus,
                              icNo: state.icNo,
                            );
                          } else {
                            return Column(
                              children: [
                                SizedBox(height: 40),
                                Center(
                                  child: SpinKitFadingCircle(
                                    color: kPrimaryColor,
                                    size: 20,
                                  ),
                                ),
                              ],
                            );
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class GetSaringDetailsLoadedWidget extends StatefulWidget {
  final SaringDetails saringDetails;
  final CouponStatus couponStatus;
  final String icNo;

  const GetSaringDetailsLoadedWidget(
      {Key? key,
      required this.saringDetails,
      required this.couponStatus,
      required this.icNo})
      : super(key: key);

  @override
  State<GetSaringDetailsLoadedWidget> createState() =>
      _GetSaringDetailsLoadedWidgetState();
}

class _GetSaringDetailsLoadedWidgetState
    extends State<GetSaringDetailsLoadedWidget> with TickerProviderStateMixin {
  int selectedIndex = 0;

  late TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Column(
      children: [
        SizedBox(height: 10),
        Container(
          width: width * 0.95,
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                offset: Offset(0, 0),
                blurRadius: 5,
                // spreadRadius: 3,
              ),
            ],
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.yellow,
                ),
                child: RichText(
                  text: TextSpan(
                    children: transformWord('screenshot'.tr()),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(0xFFFF0000),
                    ),
                  ),
                  textAlign: TextAlign.center,
                  // textScaleFactor: 1.5,
                ),
              ),
              // SizedBox(height: 20),
              SizedBox(height: 20),

              TabBar(
                indicatorColor: kPrimaryColor,
                indicatorWeight: 2,
                labelColor: Colors.black,
                controller: _tabController,
                tabs: [
                  Padding(
                    padding: const EdgeInsets.all(5),
                    child: Text('IC No'),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5),
                    child: Text('Selangkah ID'),
                  ),
                ],
              ),

              SizedBox(height: 15),

              Container(
                height: 75,
                child: TabBarView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _tabController,
                  children: [
                    Column(
                      children: [
                        BarcodeWidget(
                          barcode: Barcode.code128(),
                          data: widget.icNo,
                          width: width * 0.7,
                          height: 50,
                          style: TextStyle(fontSize: 9),
                          drawText: false,
                        ),
                        SizedBox(height: 5),
                        Text(
                          widget.icNo,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        BarcodeWidget(
                          barcode: Barcode.code128(),
                          data: widget.saringDetails.data!.userNo!,
                          width: width * 0.7,
                          height: 50,
                          style: TextStyle(fontSize: 9),
                          drawText: false,
                        ),
                        SizedBox(height: 5),
                        Text(
                          widget.saringDetails.data!.userNo!,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),

              SizedBox(height: 10),
              Text(
                'scan_label'.tr(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
              SizedBox(height: 10),
              Text(
                'scan_purpose'.tr(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.grey,
                ),
              ),
              SizedBox(height: 20),

              if (!widget.saringDetails.data!.ekycVerified!)
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 10,
                  ),
                  decoration: BoxDecoration(
                    color: Color(0xFFFFEAEA),
                    border: Border.all(
                      color: Color(0xFFFE0000),
                    ),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'account_unverified'.tr(),
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFFFF3434),
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 5),
                      Text(
                        'bring_ic_event'.tr(),
                        style: TextStyle(
                          color: Color(0xFFFF3434),
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),

              SizedBox(height: 20),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 2,
                    child: Text(
                      'name'.tr(),
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  SizedBox(width: 5),
                  Expanded(
                    flex: 2,
                    child: Text(
                      widget.saringDetails.data!.name!,
                    ),
                  ),
                ],
              ),
              if (widget.saringDetails.data!.eventSite != 'null' &&
                  widget.saringDetails.data!.eventDate.toString() !=
                      'null') ...[
                SizedBox(height: 20),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Text(
                        'sitename'.tr(),
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                      flex: 2,
                      child: Text(
                        widget.saringDetails.data!.eventSite!,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Text(
                        'address'.tr(),
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                      flex: 2,
                      child: Text(
                        // 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ut sapien dolor. Nunc tristique rutrum tempus.',
                        '${widget.saringDetails.data!.eventAddress1}, ${widget.saringDetails.data!.eventAddress2}',
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Text(
                        'date'.tr(),
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                      flex: 2,
                      child: Text(
                        formatDate(widget.saringDetails.data!.eventDate!,
                            [dd, ' ', MM, ' ', yyyy]),
                        // 'Dummy',
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Text(
                        'time'.tr(),
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                      flex: 2,
                      child: Text(
                          // formatDate(
                          //       widget.saringDetails.data.eventTimeStart,
                          //       [hh, ':', mm])
                          '${widget.saringDetails.data!.eventTimeStart!.substring(0, 5)} - ${widget.saringDetails.data!.eventTimeEnd!.substring(0, 5)}'),
                      // ),
                    ),
                  ],
                ),
              ],
              // SizedBox(height: 30),

              SizedBox(height: 20),
              if (!widget.couponStatus.data!.any(
                  (element) => element.status == 11 || element.status == 12))
                Container(
                  width: width * 0.7,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      backgroundColor: kPrimaryColor,
                    ),
                    onPressed: () {
                      // Navigator.of(context).push(
                      //   MaterialPageRoute(
                      //     builder: (context) => CouponSplit(),
                      //   ),
                      // );

                      context
                          .read<UpdateStatusCouponCubit>()
                          .updateStatusCoupon();
                    },
                    child: BlocConsumer<UpdateStatusCouponCubit,
                        UpdateStatusCouponState>(
                      listener: (context, state) {
                        if (state is UpdateStatusCouponLoaded) {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              builder: (context) => BlocProvider(
                                create: (context) => GetCouponCubit(),
                                child: SaringCoupon(),
                              ),
                            ),
                          );
                        }
                      },
                      builder: (context, state) {
                        if (state is UpdateStatusCouponLoading) {
                          return SpinKitFadingCircle(
                            color: Colors.white,
                            size: 13,
                          );
                        } else {
                          return Text(
                            'change_session'.tr(),
                            style: TextStyle(fontSize: 13),
                            textAlign: TextAlign.center,
                          );
                        }
                      },
                    ),
                  ),
                ),
            ],
          ),
        ),
        SizedBox(height: 10),
        Container(
          width: width * 0.95,
          padding: EdgeInsets.fromLTRB(35, 25, 35, 25),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                offset: Offset(0, 0),
                blurRadius: 5,
                // spreadRadius: 3,
              ),
            ],
            borderRadius: BorderRadius.circular(10),
          ),
          child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: widget.couponStatus.data!.length,
            itemBuilder: (context, index) {
              if (!widget.couponStatus.data![index].plus!) {
                return CouponListWidget(
                  couponStatus: widget.couponStatus,
                  index: index,
                );
              } else {
                return Container();
              }
            },
          ),
        ),
        SizedBox(height: 10),
        if (widget.couponStatus.data!.any((element) => element.plus!))
          Container(
            width: width * 0.95,
            padding: EdgeInsets.fromLTRB(35, 25, 35, 25),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  offset: Offset(0, 0),
                  blurRadius: 5,
                  // spreadRadius: 3,
                ),
              ],
              borderRadius: BorderRadius.circular(10),
            ),
            child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: widget.couponStatus.data!.length,
              itemBuilder: (context, index) {
                if (widget.couponStatus.data![index].plus!) {
                  return CouponListWidget(
                    couponStatus: widget.couponStatus,
                    index: index,
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),
        SizedBox(height: 10),
      ],
    );
  }
}

class CouponListWidget extends StatelessWidget {
  final CouponStatus couponStatus;
  final int index;

  const CouponListWidget(
      {Key? key, required this.couponStatus, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: height * 0.09,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 4,
                child: Text(
                  couponStatus.data![index].paramName!,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              Expanded(
                  flex: 4,
                  child: () {
                    if (couponStatus.data![index].status == 11) {
                      return Text(
                        'rejected'.tr(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: Colors.red,
                        ),
                      );
                    } else if (couponStatus.data![index].status == 12) {
                      return Text(
                        'redeemed'.tr(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: Colors.green,
                        ),
                      );
                    } else if (couponStatus.data![index].status == 3) {
                      return Text(
                        'quota_full'.tr().toUpperCase(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: Colors.grey,
                        ),
                      );
                    } else {
                      if (couponStatus.data![index].expiryOffer!
                          .isAfter(DateTime.now())) {
                        return Text(
                          'unredeemed'.tr().toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: Colors.grey,
                          ),
                        );
                      } else {
                        return Text(
                          'expired'.tr().toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: Colors.red,
                          ),
                        );
                      }
                    }
                  }())
            ],
          ),
        ),
        Container(
          child: Row(
            children: [
              if (couponStatus.data![index].image != null ||
                  couponStatus.data![index].title != null ||
                  couponStatus.data![index].content != null) ...[
                Expanded(
                  flex: 3,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: kPrimaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                              content: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    couponStatus.data![index].image != null
                                        ? Image.network(
                                            couponStatus.data![index].image!,
                                            height: 100,
                                          )
                                        : Container(),
                                    SizedBox(height: 10),
                                    couponStatus.data![index].title != null
                                        ? Text(
                                            couponStatus.data![index].title!
                                                .toUpperCase(),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w700,
                                              fontSize: 14,
                                              color: Color(0xff505660),
                                            ),
                                          )
                                        : Container(),
                                    SizedBox(height: 10),
                                    couponStatus.data![index].content != null
                                        ? Text(
                                            couponStatus.data![index].content!,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 13,
                                              color: Color(0xff505660),
                                            ),
                                          )
                                        : Container()
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                        child: Text(
                          'details'.tr(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ] else
                Expanded(
                  flex: 3,
                  child: Container(),
                ),
              couponStatus.data![index].redFlag == 1
                  ? Expanded(
                      flex: 4,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Fluttertoast.showToast(
                                msg: 'undergo_consultation'.tr(),
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                              );
                            },
                            child: Row(
                              children: [
                                Icon(
                                  FontAwesomeIcons.circleExclamation,
                                  size: 15,
                                  color: Colors.red,
                                ),
                                SizedBox(width: 5),
                                Text(
                                  'need_attention'.tr(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.italic,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  : Expanded(
                      flex: 4,
                      child: Container(),
                    )
            ],
          ),
        ),
        SizedBox(height: 15),
        Divider(
          color: Colors.grey,
          // height: 3,
          thickness: 1,
        ),
      ],
    );
  }
}
