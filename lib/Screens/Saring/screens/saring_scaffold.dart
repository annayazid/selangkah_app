import 'package:flutter/material.dart';
import 'package:selangkah_new/utils/constants.dart';

class SaringScaffold extends StatelessWidget {
  final String appBarTitle;
  final Widget content;
  final bool scrollableBanner;

  const SaringScaffold(
      {Key? key,
      required this.appBarTitle,
      required this.content,
      required this.scrollableBanner})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        // resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: kPrimaryColor,
          centerTitle: true,
          // automaticallyImplyLeading: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          title: Text(
            appBarTitle,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 15,
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: 'Roboto',
            ),
          ),
        ),
        body: Column(
          children: [
            if (!scrollableBanner)
              Container(
                height: height - AppBar().preferredSize.height,
                width: width,
                child: Column(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: double.infinity,
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Image.asset(
                            'assets/images/Saring/selangor-saring-banner-bg.png',
                            // fit: BoxFit.fitHeight,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Center(
                                child: Container(
                                  width: width * 0.75,
                                  child: Image.asset(
                                    'assets/images/Saring/selangor_saring_logo.png',
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Expanded(child: content),
                  ],
                ),
              ),
            if (scrollableBanner)
              Container(
                height: height - AppBar().preferredSize.height,
                width: width,
                child: RefreshIndicator(
                  color: kPrimaryColor,
                  onRefresh: () async {},
                  child: SingleChildScrollView(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: double.infinity,
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Image.asset(
                                'assets/images/selangor_saring/selangor-saring-banner-bg.png',
                                // fit: BoxFit.fitHeight,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Center(
                                    child: Container(
                                      width: width * 0.75,
                                      child: Image.asset(
                                        'assets/images/selangor_saring/selangor_saring_logo.png',
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        content,
                      ],
                    ),
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }
}
