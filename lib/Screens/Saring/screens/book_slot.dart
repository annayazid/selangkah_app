import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';
import 'package:selangkah_new/utils/constants.dart';

class BookSlot extends StatefulWidget {
  final int showDate;
  final bool isCreateAppointment;
  final String id;
  final String service;
  final String idProgramAppointment;

  const BookSlot({
    Key? key,
    required this.showDate,
    required this.isCreateAppointment,
    required this.id,
    required this.service,
    required this.idProgramAppointment,
  }) : super(key: key);

  // final List<String> provider;
  // final List<DateTime> dateTime;

  @override
  _BookSlotState createState() => _BookSlotState();
}

class _BookSlotState extends State<BookSlot> {
  void initState() {
    print('showdate is ${widget.showDate}');

    context.read<GetProviderCubit>().getProvider(widget.id);
    super.initState();
  }

  String? idChoosen;
  int? indexChosenDate;
  DateTime? chosenDate;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    return SaringScaffold(
      scrollableBanner: false,
      appBarTitle: 'bookslot'.tr(),
      content: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 10),
            Container(
              width: width * 0.95,
              padding: EdgeInsets.all(25),
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    offset: Offset(0, 0),
                    blurRadius: 5,
                    // spreadRadius: 3,
                  ),
                ],
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.service.toUpperCase(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 10),
                  Divider(
                    color: Colors.black,
                    height: 1,
                  ),
                  SizedBox(height: 20),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Text(
                          'location'.tr(),
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            height: 2,
                          ),
                        ),
                      ),
                      BlocBuilder<GetProviderCubit, GetProviderState>(
                        builder: (context, state) {
                          if (state is GetProviderLoaded) {
                            if (state.provider.data!.isNotEmpty) {
                              return Expanded(
                                flex: 2,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: InputDecorator(
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.symmetric(
                                        horizontal: 15,
                                      ),
                                      errorStyle: TextStyle(
                                          color: Colors.redAccent,
                                          fontSize: 16.0),
                                      border: InputBorder.none,
                                    ),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton<String>(
                                        isExpanded: true,
                                        icon: Icon(
                                          Icons.keyboard_arrow_down,
                                          size: 30,
                                          color: Color(0xFFF36B21),
                                        ),
                                        hint: Text(
                                          'chooseLocation'.tr(),
                                          style: TextStyle(
                                            fontSize: 14,
                                          ),
                                        ),
                                        value: idChoosen,
                                        onChanged: (String? value) {
                                          setState(() {
                                            idChoosen = value;

                                            context
                                                .read<GetDateTimeCubit>()
                                                .resetDateTime();

                                            context
                                                .read<GetDateTimeCubit>()
                                                .getDateTime(idChoosen!);
                                          });
                                          print(idChoosen);
                                        },
                                        items: state.provider.data!.map((e) {
                                          return DropdownMenuItem<String>(
                                            value: e.idProvider,
                                            child: Text(
                                              e.ppvName!,
                                              style: TextStyle(
                                                fontSize: 14,
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 4,
                                            ),
                                          );
                                        }).toList(),

                                        // state.provider.data
                                        //     .map((String value) {
                                        //   return DropdownMenuItem<String>(
                                        //     value: value,
                                        //     child: Text(
                                        //       value,
                                        //       style: TextStyle(
                                        //         fontSize: 14,
                                        //       ),
                                        //       overflow: TextOverflow.ellipsis,
                                        //       maxLines: 3,
                                        //     ),
                                        //   );
                                        // }).toList(),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            } else {
                              return Expanded(
                                flex: 2,
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                  child: Text(
                                    'no_clinic_available'.tr(),
                                    style: TextStyle(
                                      color: Colors.red,
                                      height: 2,
                                    ),
                                  ),
                                ),
                              );
                            }
                          } else {
                            return Container();
                          }
                        },
                      ),
                    ],
                  ),
                  if (widget.showDate == 1)
                    BlocBuilder<GetDateTimeCubit, GetDateTimeState>(
                      builder: (context, state) {
                        if (state is GetDateTimeLoaded) {
                          if (state.dateTime.data!.isNotEmpty) {
                            return Column(
                              children: [
                                SizedBox(height: 25),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Text(
                                        'date'.tr(),
                                        style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                          height: 2,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        child: InputDecorator(
                                          decoration: InputDecoration(
                                            contentPadding:
                                                EdgeInsets.symmetric(
                                              horizontal: 15,
                                            ),
                                            errorStyle: TextStyle(
                                                color: Colors.redAccent,
                                                fontSize: 16.0),
                                            border: InputBorder.none,
                                          ),
                                          child: DropdownButtonHideUnderline(
                                            child: DropdownButtonFormField<
                                                DateTime>(
                                              // isDense: false,
                                              isExpanded: true,
                                              decoration: InputDecoration(),
                                              icon: Icon(
                                                Icons.keyboard_arrow_down,
                                                size: 30,
                                                color: Color(0xFFF36B21),
                                              ),
                                              hint: Text(
                                                'selectdate'.tr(),
                                                style: TextStyle(
                                                  fontSize: 14,
                                                ),
                                              ),
                                              value: chosenDate,
                                              onChanged: (DateTime? value) {
                                                setState(
                                                  () {
                                                    chosenDate = value;

                                                    indexChosenDate = state
                                                        .dateTime.data!
                                                        .indexWhere((element) =>
                                                            element.slotDate ==
                                                            chosenDate);
                                                  },
                                                );
                                                print(chosenDate);
                                                print(indexChosenDate);
                                              },

                                              items:
                                                  state.dateTime.data!.map((e) {
                                                return DropdownMenuItem<
                                                    DateTime>(
                                                  value: e.slotDate,
                                                  child: Text(
                                                    formatDate(e.slotDate!, [
                                                      dd,
                                                      ' ',
                                                      MM,
                                                      ' ',
                                                      yyyy
                                                    ]),
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                    ),
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 3,
                                                  ),
                                                );
                                              }).toList(),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                if (indexChosenDate != null)
                                  Column(
                                    children: [
                                      SizedBox(height: 25),
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            flex: 1,
                                            child: Text(
                                              'time'.tr(),
                                              style: TextStyle(
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: Container(
                                              margin: EdgeInsets.fromLTRB(
                                                  10, 0, 5, 0),
                                              child: Text(
                                                '${state.dateTime.data![indexChosenDate!].timeStart} - ${state.dateTime.data![indexChosenDate!].timeEnd}',
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                              ],
                            );
                          } else {
                            return Column(
                              children: [
                                SizedBox(height: 25),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Text(
                                        'date'.tr(),
                                        style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        margin:
                                            EdgeInsets.fromLTRB(10, 0, 5, 0),
                                        child: Text(
                                          'noavailabledatetime'.tr(),
                                          style: TextStyle(
                                            color: Colors.red,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            );
                          }
                        } else {
                          return Container();
                          // Center(
                          //   child: SpinKitFadingCircle(
                          //     color: kPrimaryColor,
                          //     size: 30,
                          //   ),
                          // );
                        }
                      },
                    ),
                ],
              ),
            ),
            SizedBox(height: 15),
            Container(
              width: width * 0.95,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: () {
                      if (idChoosen != null) {
                        if (widget.showDate == 1 && chosenDate != null) {
                          return kPrimaryColor;
                        } else if (widget.showDate == 0) {
                          return kPrimaryColor;
                        } else {
                          return Colors.grey;
                        }
                      } else {
                        return Colors.grey;
                      }
                    }(),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onPressed: () {
                    // print(idChoosen);
                    // print(widget.showDate);
                    // print(indexChosenDate);

                    if (widget.isCreateAppointment) {
                      print(widget.isCreateAppointment);
                      context.read<CreateAppointmentCubit>().createAppointment(
                            idChoosen!,
                            widget.id,
                            chosenDate != null
                                ? formatDate(
                                    chosenDate!, [yyyy, '-', mm, '-', dd])
                                : null,
                          );
                    } else {
                      print(widget.isCreateAppointment);
                      context.read<CreateAppointmentCubit>().changeAppointment(
                            idChoosen!,
                            widget.id,
                            chosenDate != null
                                ? formatDate(
                                    chosenDate!, [yyyy, '-', mm, '-', dd])
                                : null,
                            widget.idProgramAppointment,
                          );
                    }
                  },
                  child: BlocConsumer<CreateAppointmentCubit,
                      CreateAppointmentState>(
                    listener: (context, state) {
                      if (state is CreateAppointmentLoaded) {
                        Navigator.of(context).pop();
                      } else if (state is CreateAppointmentLoadedWithPopUp) {
                        Navigator.of(context).pop(true);
                      }
                    },
                    builder: (context, state) {
                      if (state is CreateAppointmentLoading) {
                        return SpinKitFadingCircle(
                          color: Colors.white,
                          size: 18,
                        );
                      } else {
                        return Text(
                          'swabook_confirm'.tr(),
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                            fontSize: 18,
                          ),
                        );
                      }
                    },
                  )),
            ),
            SizedBox(
              height: 15,
            ),
          ],
        ),
      ),
    );
  }
}
