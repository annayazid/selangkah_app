import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';
import 'package:selangkah_new/utils/constants.dart';

class SaringCoupon extends StatefulWidget {
  const SaringCoupon({Key? key}) : super(key: key);

  @override
  _SaringCouponState createState() => _SaringCouponState();
}

class _SaringCouponState extends State<SaringCoupon> {
  @override
  void initState() {
    context.read<GetCouponCubit>().getCoupon();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SaringScaffold(
      appBarTitle: 'Selangor Saring',
      scrollableBanner: false,
      content: BlocConsumer<GetCouponCubit, GetCouponState>(
        listener: (context, state) {
          if (state is GetCouponBarcode) {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => GetSaringDetailsCubit(),
                    ),
                    BlocProvider(
                      create: (context) => UpdateStatusCouponCubit(),
                    ),
                  ],
                  child: SaringBarcode(),
                ),
              ),
            );
          }
        },
        builder: (context, state) {
          if (state is GetCouponLoaded) {
            return GetCouponLoadedWidget(
              couponStatus: state.couponStatus,
            );
          } else {
            return Center(
              child: SpinKitFadingCircle(
                color: kPrimaryColor,
                size: 30,
              ),
            );
          }
        },
      ),
    );
  }
}

class GetCouponLoadedWidget extends StatefulWidget {
  final CouponStatus couponStatus;

  const GetCouponLoadedWidget({Key? key, required this.couponStatus})
      : super(key: key);

  @override
  _GetCouponLoadedWidgetState createState() => _GetCouponLoadedWidgetState();
}

class _GetCouponLoadedWidgetState extends State<GetCouponLoadedWidget> {
  List<bool> statusCouponList = [];
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    print(widget.couponStatus.data!.length);
    widget.couponStatus.data!.forEach((element) {
      statusCouponList.add(true);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    if (widget.couponStatus.data!.isEmpty) {
      return Column(
        children: [
          SizedBox(height: 75),
          Image.asset(
            'assets/images/Saring/no_voucher.png',
            width: width * 0.3,
            height: width * 0.3,
          ),
          SizedBox(height: 10),
          Padding(
            padding: EdgeInsets.all(15),
            child: Text(
              'no_voucher'.tr(),
              style: TextStyle(
                color: Color(0xFF505660),
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      );
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Text(
            'available_coupons'.tr(),
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        SizedBox(height: 7.5),
        Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Text(
            '${'please_claim_by'.tr()} ${formatDate(
              widget.couponStatus.data![0].expiryOffer!,
              [dd, ' ', MM, ' ', yyyy],
            )}',
            style: TextStyle(
              color: Colors.red,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        // SizedBox(height: 7.5),
        // Padding(
        //   padding: const EdgeInsets.symmetric(horizontal: 20),
        //   child: Text(
        //     'Please choose the programs you are interested in from the list below by marking your selections.',
        //     style: TextStyle(
        //       fontWeight: FontWeight.bold,
        //     ),
        //   ),
        // ),
        SizedBox(height: 10),
        Expanded(
          flex: 10,
          child: Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Scrollbar(
              controller: scrollController,
              thumbVisibility: true,
              thickness: 3,
              child: ListView.builder(
                controller: scrollController,
                // shrinkWrap: true,
                itemCount: widget.couponStatus.data!.length,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        // mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Checkbox(
                            value: statusCouponList[index],
                            onChanged: (value) {
                              setState(() {
                                statusCouponList[index] = value!;
                              });
                            },
                          ),
                          SizedBox(width: 30),
                          Expanded(
                            child: Text(
                              widget.couponStatus.data![index].paramName!,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          // Spacer(),
                          SizedBox(width: 10),
                          Image.asset(
                            'assets/images/Saring/kupon.png',
                            width: 20,
                            height: 20,
                          ),
                          SizedBox(width: 30),
                        ],
                      ),
                      Divider(),
                    ],
                  );
                },
              ),
            ),
          ),
        ),
        Spacer(),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Center(
            child: Container(
              width: double.infinity,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: statusCouponList.any((element) => element)
                      ? kPrimaryColor
                      : Colors.grey,
                ),
                onPressed: () {
                  //check if at least one tick
                  if (statusCouponList.any((element) => element)) {
                    List<String> selectedId = [];

                    for (var i = 0; i < statusCouponList.length; i++) {
                      if (statusCouponList[i]) {
                        selectedId.add(widget.couponStatus.data!
                            .elementAt(i)
                            .id
                            .toString());
                      }
                    }

                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) => MultiBlocProvider(
                          providers: [
                            BlocProvider(
                              create: (context) => GetVenueCubit(),
                            ),
                            BlocProvider(
                              create: (context) => GetVenueDateCubit(),
                            ),
                            BlocProvider(
                              create: (context) => AcceptCouponCubit(),
                            ),
                          ],
                          child: SaringVenue(
                            selectVoucherId: selectedId,
                            couponStatus: widget.couponStatus,
                          ),
                        ),
                      ),
                    );
                  }
                },
                child: Text('accept'.tr()),
              ),
            ),
          ),
        ),
        SizedBox(height: 10),
      ],
    );
  }
}
