import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class SaringMenu extends StatelessWidget {
  final bool showRegistration;

  const SaringMenu({Key? key, required this.showRegistration})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    void redirectRegistration() async {
      String token = await SaringRepositories.getToken();

      String id = await SecureStorage().readSecureData('userId');
      String selId = await SecureStorage().readSecureData('userSelId');

      String? kohot = await SaringRepositories.getKohot();

      Map<String, String> queryParams = {
        'user_token': token,
        'id_selangkah_user': id,
        'selangkah_id': selId,
        'is_app': '1',
        'bearer_token': token,
        'id_kohot': kohot!,
      };

      String queryString = Uri(queryParameters: queryParams).query;

      String url = '$URL_SARING/check-credential-app?' + queryString;

      print(url);

      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => SaringWebNew(
            appBarTitle: 'Selangor Saring',
            url: url,
          ),
        ),
      );
    }

    double width = MediaQuery.of(context).size.width;
    return SaringScaffold(
      appBarTitle: 'Selangor Saring',
      scrollableBanner: false,
      content: Column(
        children: [
          if (showRegistration) ...[
            SizedBox(
              height: 10,
            ),
            _saringButton(
              width,
              'register'.tr(),
              'assets/images/Saring/daftar_saring.png',
              redirectRegistration,
            ),
          ],
          SizedBox(height: 10),
          _saringButton(
            width,
            'screening_voucher'.tr(),
            'assets/images/Saring/saring_kupon.png',
            () async {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => BlocProvider(
                    create: (context) => GetCouponCubit(),
                    child: SaringCoupon(),
                  ),
                ),
              );
            },
          ),
          SizedBox(height: 10),
          _saringButton(
            width,
            'result'.tr(),
            'assets/images/Saring/saring_result.png',
            () async {
              Navigator.of(context, rootNavigator: true).push(
                MaterialPageRoute(
                  builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                        create: (context) => SliderCardCubit(),
                      ),
                      BlocProvider(
                        create: (context) => ProfilePageCubit(),
                      ),
                      BlocProvider(
                        create: (context) => DropdownMeasurementCubit(),
                      ),
                      BlocProvider(
                        create: (context) => HealthRecordCubit(),
                      ),
                      BlocProvider(
                        create: (context) => BackgroundIllnessCubit(),
                      ),
                      BlocProvider(
                        create: (context) => AppointmentCubit(),
                      ),
                      BlocProvider(
                        create: (context) => TestResultCubit(),
                      ),
                      BlocProvider(
                        create: (context) => MedicalCertificatesCubit(),
                      ),
                      BlocProvider(
                        create: (context) => PrescriptionsCubit(),
                      ),
                      BlocProvider(
                        create: (context) => VaccineCertificatesCubit(),
                      ),
                      BlocProvider(
                        create: (context) => ArchivedRecordCubit(),
                      ),
                      BlocProvider(
                        create: (context) => InvoiceCubit(),
                      ),
                      BlocProvider(
                        create: (context) => ReferralLetterCubit(),
                      ),
                    ],
                    child: ProfilePage(
                      page: 'HAYAT',
                    ),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  Container _saringButton(width, title, image, function) {
    return Container(
      width: width * 0.95,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
        onPressed: function,
        child: Column(
          children: [
            SizedBox(height: 20),
            Image.asset(
              image,
              width: 40,
              height: 40,
            ),
            SizedBox(height: 15),
            Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 15,
                color: Color(0xff505660),
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
