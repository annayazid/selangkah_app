import 'dart:convert';
import 'dart:io';

import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/endpoint.dart';

class SaringWebNew extends StatefulWidget {
  final String appBarTitle;
  final String url;

  const SaringWebNew({super.key, required this.appBarTitle, required this.url});

  @override
  State<SaringWebNew> createState() => _SaringWebNewState();
}

class _SaringWebNewState extends State<SaringWebNew> {
  late InAppWebViewController webView;

  bool isLoaded = false;

  _createFileFromBase64(
      String base64content, String fileName, String yourExtension) async {
    var bytes = base64Decode(base64content.replaceAll('\n', ''));
    final output = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    final file = File("${output!.path}/$fileName.$yourExtension");
    await file.writeAsBytes(bytes.buffer.asUint8List());
    print("${output.path}/$fileName.$yourExtension");
    await OpenFilex.open("${output.path}/$fileName.$yourExtension");
    webView.goBack();

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: true,
        backgroundColor: kPrimaryColor,
        title: Text(
          widget.appBarTitle,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 15.0,
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: "Roboto",
          ),
        ),
      ),
      body: Column(
        children: [
          Container(
            height: height,
            width: width,
            child: Stack(
              children: [
                InAppWebView(
                  initialUrlRequest: URLRequest(
                    url: Uri.parse(widget.url),
                  ),
                  androidOnPermissionRequest:
                      (controller, origin, resources) async {
                    return PermissionRequestResponse(
                      resources: resources,
                      action: PermissionRequestResponseAction.GRANT,
                    );
                  },
                  initialOptions: InAppWebViewGroupOptions(
                    crossPlatform: InAppWebViewOptions(
                      useShouldOverrideUrlLoading: true,
                      mediaPlaybackRequiresUserGesture: false,
                      useOnDownloadStart: true,
                    ),
                    android: AndroidInAppWebViewOptions(
                      hardwareAcceleration: true,
                    ),
                    ios: IOSInAppWebViewOptions(
                      allowsInlineMediaPlayback: true,
                    ),
                  ),
                  onWebViewCreated: (InAppWebViewController controller) {
                    webView = controller;
                    print("onWebViewCreated");

                    controller.addJavaScriptHandler(
                      handlerName: 'blobToBase64Handler',
                      callback: (data) async {
                        if (data.isNotEmpty) {
                          final String receivedFileInBase64 = data[0];
                          final String receivedMimeType = data[1];

                          print('receivedMimeType is $receivedMimeType');

                          // NOTE: create a method that will handle your extensions
                          List<String> splittedExtension =
                              receivedMimeType.split('/');
                          final String yourExtension =
                              splittedExtension.last; // 'pdf'

                          DateTime now = DateTime.now();

                          String dateNow =
                              formatDate(now, [dd, mm, yyyy, HH, nn]);

                          _createFileFromBase64(receivedFileInBase64,
                              'YS_receipt_$dateNow', yourExtension);
                        }
                      },
                    );
                  },
                  onConsoleMessage: (controller, consoleMessage) {
                    // print(consoleMessage);
                  },
                  onLoadStop: (controller, url) async {
                    setState(() {
                      isLoaded = true;
                    });

                    if (url.toString() == '$URL_SARING/redirect-to-app') {
                      await Future.delayed(Duration(seconds: 1));
                      // Navigator.of(context).pop();

                      //redirect ekyc here
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (context) => MultiBlocProvider(
                            providers: [
                              BlocProvider(
                                create: (context) => CheckProfileCubit(),
                              )
                            ],
                            child: EKYCScreen(),
                          ),
                        ),
                      );
                    }
                  },
                  onDownloadStartRequest:
                      (controller, downloadStartRequest) async {
                    print('masuk download request');
                    var jsContent =
                        await rootBundle.loadString("assets/js/base64.js");
                    await controller.evaluateJavascript(
                        source: jsContent.replaceAll("blobUrlPlaceholder",
                            downloadStartRequest.url.toString()));
                  },
                ),
                if (!isLoaded)
                  SpinKitRing(
                    color: kPrimaryColor,
                    size: 30,
                    lineWidth: 5,
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
