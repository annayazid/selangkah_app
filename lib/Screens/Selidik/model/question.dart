// To parse this JSON data, do
//
//     final getQuestion = getQuestionFromJson(jsonString);

import 'dart:convert';

Question getQuestionFromJson(String str) => Question.fromJson(json.decode(str));

String getQuestionToJson(Question data) => json.encode(data.toJson());

class Question {
  Question({
    this.code,
    this.data,
  });

  int? code;
  List<Datum>? data;

  factory Question.fromJson(Map<String, dynamic> json) => Question(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<Datum>.from(json["Data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.question,
    this.questionType,
    this.answer,
  });

  String? id;
  String? question;
  String? questionType;
  List<String>? answer;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        question: json["question"] == null ? null : json["question"],
        questionType:
            json["question_type"] == null ? null : json["question_type"],
        answer: json["answer"] == null
            ? null
            : List<String>.from(json["answer"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "question": question == null ? null : question,
        "question_type": questionType == null ? null : questionType,
        "answer":
            answer == null ? null : List<dynamic>.from(answer!.map((x) => x)),
      };
}
