import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:selangkah_new/Screens/Selidik/selidik.dart';
import 'package:selangkah_new/utils/constants.dart';

class SelidikScreen extends StatefulWidget {
  @override
  _SelidikScreenState createState() => _SelidikScreenState();
}

class _SelidikScreenState extends State<SelidikScreen> {
  void initState() {
    context.read<SelidikCubit>().getQuestion();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SelidikScaffold(
      content: BlocBuilder<SelidikCubit, SelidikState>(
        builder: (context, state) {
          if (state is SelidikLoaded) {
            print('Question has ${state.question.data!.length}');
            return SelidikLoadedWidget(question: state.question);
          } else {
            return SpinKitDualRing(color: kPrimaryColor, size: 3);
          }
        },
      ),
    );
  }
}

class SelidikLoadedWidget extends StatefulWidget {
  final Question question;

  const SelidikLoadedWidget({Key? key, required this.question})
      : super(key: key);

  @override
  _SelidikLoadedWidgetState createState() => _SelidikLoadedWidgetState();
}

class _SelidikLoadedWidgetState extends State<SelidikLoadedWidget> {
  //hold question and answer
  int radioValue = 0;
  int sliderValue = 1;
  int currentQuestionIndex = 0;

  sliderScalling(child, answer) {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerRight,
          child: UnconstrainedBox(
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 10,
              ),
              // width: width * 0.4,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(13),
              ),
              child: Text(
                answer.toString(),
                style: TextStyle(
                  color: Colors.black,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        child,
        Row(
          children: [
            Text(
              '1',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            Spacer(),
            Text(
              '5',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;

    if (widget.question.data!.isNotEmpty) {
      return SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(
                  bottom: height * 0.02,
                  left: height * 0.02,
                  right: height * 0.02),
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                children: [
                  SizedBox(height: 10),
                  Text(
                    widget.question.data![currentQuestionIndex].question!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 10),

                  //if yes no or multiple
                  if (widget.question.data![currentQuestionIndex]
                              .questionType ==
                          'Multiple Choices' ||
                      widget.question.data![currentQuestionIndex]
                              .questionType ==
                          'Yes / No')
                    Column(
                      children: [
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: widget.question.data![currentQuestionIndex]
                              .answer!.length,
                          itemBuilder: (context, index) {
                            return Row(
                              children: [
                                Radio(
                                  groupValue: radioValue,
                                  activeColor: Colors.black,
                                  value: index,
                                  onChanged: (int? value) {
                                    setState(() {
                                      radioValue = value!;
                                    });
                                  },
                                ),
                                Expanded(
                                  child: Text(
                                    widget.question.data![currentQuestionIndex]
                                        .answer![index],
                                    // textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),
                              ],
                            );
                          },
                        ),
                      ],
                    ),
                  if (widget.question.data![0].questionType == 'Scaling')
                    Column(
                      children: [
                        sliderScalling(
                          SliderTheme(
                            data: SliderTheme.of(context).copyWith(
                              activeTrackColor: Colors.black.withOpacity(0.5),
                              inactiveTrackColor: Colors.black.withOpacity(0.5),
                              trackShape: RoundedRectSliderTrackShape(),
                              trackHeight: 8,
                              thumbShape: RoundSliderThumbShape(
                                  enabledThumbRadius: 12.0),
                              thumbColor: Colors.black,
                              overlayColor: Colors.black.withAlpha(32),
                              overlayShape:
                                  RoundSliderOverlayShape(overlayRadius: 28.0),
                              // tickMarkShape: RoundSliderTickMarkShape(),
                              activeTickMarkColor: Colors.black,
                              inactiveTickMarkColor: Colors.black,
                              // valueIndicatorShape: PaddleSliderValueIndicatorShape(),
                              // valueIndicatorColor: Colors.white,
                              // valueIndicatorTextStyle: TextStyle(
                              //   color: Colors.white,
                              // ),
                            ),
                            child: Slider(
                              value: sliderValue.toDouble(),
                              min: 1.0,
                              max: 5.0,
                              onChanged: (value) {
                                setState(() {
                                  sliderValue = value.round();
                                });
                              },
                            ),
                          ),
                          sliderValue,
                        ),
                      ],
                    ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: height * 0.02),
              width: width * 0.3,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Color(0xFFD27F00)),
                onPressed: () {
                  String answer;

                  if (widget
                          .question.data![currentQuestionIndex].questionType ==
                      'Scaling') {
                    answer = sliderValue.toString();
                  } else {
                    answer = widget.question.data![currentQuestionIndex]
                        .answer![radioValue];
                  }

                  context.read<SendAnswerCubit>().sendAnswer(
                        question:
                            widget.question.data![currentQuestionIndex].id!,
                        answer: answer,
                      );

                  if (currentQuestionIndex < widget.question.data!.length - 1) {
                    setState(() {
                      radioValue = 0;
                      sliderValue = 0;
                      currentQuestionIndex++;
                    });
                  } else {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (builder) => ThankYou(),
                      ),
                    );
                  }
                },
                child: BlocConsumer<SendAnswerCubit, SendAnswerState>(
                  listener: (context, state) {},
                  builder: (context, state) {
                    if (state is SendAnswerLoading) {
                      return SpinKitFadingCircle(
                        color: Colors.white,
                        size: 14,
                      );
                    } else {
                      return Text('submit'.tr());
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return Text('no_question_moment'.tr());
    }
  }
}
