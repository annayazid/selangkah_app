import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Selidik/selidik.dart';

class ThankYou extends StatelessWidget {
  const ThankYou({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    // Size size = MediaQuery.of(context).size;
    return SelidikScaffold(
      content: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.only(
              bottom: height * 0.02, left: height * 0.02, right: height * 0.02),
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            children: [
              SizedBox(height: 10),
              Text(
                'thank_you_feedback'.tr(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
                maxLines: 5,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(height: 10),
              Container(
                width: width * 0.3,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Color(0xFFD27F00)),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text('Okay')),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
