import 'package:flutter/material.dart';
import 'package:selangkah_new/utils/constants.dart';

class SelidikScaffold extends StatelessWidget {
  final Widget content;

  const SelidikScaffold({Key? key, required this.content}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/images/selangkah_logo.png",
          width: size.width * 0.45,
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: kPrimaryColor,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: height * 0.225,
              // width: double.infinity,
              child: Stack(
                // fit: StackFit.expand,
                children: [
                  Image.asset(
                    'assets/images/Selidik/selidik-banner-bg.png',
                    fit: BoxFit.fitWidth,
                    width: double.infinity,
                  ),
                  Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Text(
                            'Selangkah Selidik',
                            style: TextStyle(
                              color: Color(0xffA56300),
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            content,
          ],
        ),
      ),
    );
  }
}
