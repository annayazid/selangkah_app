part of 'send_answer_cubit.dart';

@immutable
abstract class SendAnswerState {
  const SendAnswerState();
}

class IncrementQuestion extends SendAnswerState {
  const IncrementQuestion();
}

class SendAnswerInitial extends SendAnswerState {
  const SendAnswerInitial();
}

class SendAnswerLoading extends SendAnswerState {
  const SendAnswerLoading();
}

class SendAnswerSent extends SendAnswerState {
  const SendAnswerSent();
}
