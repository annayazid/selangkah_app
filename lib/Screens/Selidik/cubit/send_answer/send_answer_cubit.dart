import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Selidik/repositories/repositories.dart';

part 'send_answer_state.dart';

class SendAnswerCubit extends Cubit<SendAnswerState> {
  SendAnswerCubit() : super(SendAnswerInitial());

  Future<void> sendAnswer(
      {required String question, required String answer}) async {
    emit(SendAnswerLoading());

    //process
    await SelidikRepositories.sendAnswer(
      question: question,
      answer: answer,
    );

    emit(SendAnswerSent());
  }
}
