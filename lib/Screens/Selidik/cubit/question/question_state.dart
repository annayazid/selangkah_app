part of 'question_cubit.dart';

@immutable
abstract class SelidikState {
  const SelidikState();
}

class SelidikInitial extends SelidikState {
  const SelidikInitial();
}

class SelidikLoading extends SelidikState {
  const SelidikLoading();
}

class SelidikLoaded extends SelidikState {
  final Question question;

  SelidikLoaded(this.question);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SelidikLoaded && other.question == question;
  }

  @override
  int get hashCode => question.hashCode;
}
