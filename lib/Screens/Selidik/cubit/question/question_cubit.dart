import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Selidik/selidik.dart';

part 'question_state.dart';

class SelidikCubit extends Cubit<SelidikState> {
  SelidikCubit() : super(SelidikInitial());

  Future<void> getQuestion() async {
    emit(SelidikLoading());

    Question getquestion = await SelidikRepositories.getQuestion();

    emit(SelidikLoaded(getquestion));
  }
}
