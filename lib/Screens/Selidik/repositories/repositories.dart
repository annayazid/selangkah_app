import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/Selidik/selidik.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class SelidikRepositories {
  static Future<Question> getQuestion() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'session_id': GlobalVariables.sessionId!,
      'language': GlobalVariables.language!,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_URL/get_selidik_quest'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    log(response.body);

    Question getquestion = getQuestionFromJson(response.body);

    return getquestion;
  }

  static Future<void> getAnswer(
      int questID, String answer, String reason) async {
    String id = await SecureStorage().readSecureData('userId');

    String questionID = '$questID';

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'questionID': questionID,
      'answer': answer,
      'reason': reason,
    };

    await http.post(
      Uri.parse('$API_URL/selidik_ans'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );
  }

  static Future<void> sendAnswer(
      {required String question, required String answer}) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'id_selidik_question': question,
      'answer': answer,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_URL/selidik_ans'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );
    print(response.body);
  }
}
