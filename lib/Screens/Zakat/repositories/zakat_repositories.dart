import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/Zakat/zakat.dart';
import 'package:selangkah_new/utils/endpoint.dart';

class ZakatDonateRepositories {
  static Future<ZakatList> getZakatList() async {
    final Map<String, String> map = {
      'token': TOKEN,
    };

    //call api
    final response = await http.post(
      Uri.parse('$API_ZAKAT/list_zakat_available'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
    //convert result api to class

    ZakatList zakatList = zakatListFromJson(response.body);

    return zakatList;
  }
}
