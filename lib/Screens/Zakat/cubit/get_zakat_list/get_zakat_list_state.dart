part of 'get_zakat_list_cubit.dart';

@immutable
abstract class GetZakatListState {
  const GetZakatListState();
}

class GetZakatListInitial extends GetZakatListState {
  const GetZakatListInitial();
}

class GetZakatListLoading extends GetZakatListState {
  const GetZakatListLoading();
}

class GetZakatListLoaded extends GetZakatListState {
  final ZakatList zakatList;

  GetZakatListLoaded(this.zakatList);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetZakatListLoaded && other.zakatList == zakatList;
  }

  @override
  int get hashCode => zakatList.hashCode;
}
