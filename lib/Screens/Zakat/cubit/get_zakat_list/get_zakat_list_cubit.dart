import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Zakat/zakat.dart';

part 'get_zakat_list_state.dart';

class GetZakatListCubit extends Cubit<GetZakatListState> {
  GetZakatListCubit() : super(GetZakatListInitial());

  Future<void> getZakatList() async {
    emit(GetZakatListLoading());

    ZakatList zakatList = await ZakatDonateRepositories.getZakatList();

    emit(GetZakatListLoaded(zakatList));
  }
}
