import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/Zakat/zakat.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/secure_storage.dart';
import 'package:url_launcher/url_launcher.dart';

class ZakatScreen extends StatefulWidget {
  ZakatScreen({Key? key}) : super(key: key);

  @override
  State<ZakatScreen> createState() => _ZakatScreenState();
}

class _ZakatScreenState extends State<ZakatScreen> {
  @override
  void initState() {
    super.initState();
    context.read<GetZakatListCubit>().getZakatList();
  }

  @override
  Widget build(BuildContext context) {
    // double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    return BlocBuilder<GetZakatListCubit, GetZakatListState>(
      builder: (context, state) {
        if (state is GetZakatListLoaded) {
          return ZakatScaffold(
            appBarTitle: 'Zakat',
            content: Container(
              margin: EdgeInsets.all(15),
              child: GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 15,
                  mainAxisSpacing: 15,
                  childAspectRatio: 1.2,
                ),
                itemCount: state.zakatList.data!.length,
                itemBuilder: (BuildContext context, int index) {
                  return ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.white,
                      padding: EdgeInsets.all(20),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    onPressed: () async {
                      SecureStorage secureStorage = SecureStorage();
                      String id = await secureStorage.readSecureData('userId');
                      if (Platform.isIOS) {
                        launchUrl(
                          Uri.parse(
                              '${state.zakatList.data![index].linkWeb}$id'),
                          mode: LaunchMode.externalApplication,
                        );
                      } else {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => ZakatWeb(
                              title: state.zakatList.data![index].programName!,
                              url: '${state.zakatList.data![index].linkWeb}$id',
                            ),
                          ),
                        );
                      }
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Image.network(state.zakatList.data![index].picDir!),
                        // SizedBox(height: 5),
                        Text(
                          state.zakatList.data![index].programName!,
                          style: TextStyle(
                            color: Color(0xFF676767),
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          );
        } else {
          return SpinKitFadingCircle(
            color: kPrimaryColor,
            size: 25,
          );
        }
      },
    );
  }
}
