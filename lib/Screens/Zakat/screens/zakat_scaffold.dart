import 'package:flutter/material.dart';
import 'package:selangkah_new/utils/constants.dart';

class ZakatScaffold extends StatelessWidget {
  final String appBarTitle;
  final Widget content;

  const ZakatScaffold(
      {Key? key, required this.appBarTitle, required this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        // resizeToAvoidBottomInset: false,
        appBar: AppBar(
          centerTitle: true,
          // automaticallyImplyLeading: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          title: Text(
            appBarTitle,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 15,
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: 'Roboto',
            ),
          ),
        ),
        body: SingleChildScrollView(
            child: Container(
          height: height - AppBar().preferredSize.height,
          width: width,
          child: content,
        )),
      ),
    );
  }
}
