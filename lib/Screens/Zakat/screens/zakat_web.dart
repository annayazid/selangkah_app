import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ZakatWeb extends StatefulWidget {
  final String url;
  final String title;

  ZakatWeb({Key? key, required this.title, required this.url})
      : super(key: key);

  @override
  _ZakatWebState createState() => _ZakatWebState();
}

class _ZakatWebState extends State<ZakatWeb> {
  @override
  void initState() {
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  WebViewController? webViewCon;

  final Completer<WebViewController> _controllerCompleter =
      Completer<WebViewController>();

  bool redirected = false;

  @override
  Widget build(BuildContext context) {
    // double width = MediaQuery.of(context).size.width;

    print('url is ${widget.url}');

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Color(0xFFD1D1D1),
        centerTitle: true,
        title: Text(
          widget.title,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15.0,
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: "Roboto"),
        ),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                child: WebView(
                  javascriptMode: JavascriptMode.unrestricted,
                  initialUrl: widget.url,
                  onWebViewCreated: (WebViewController controller) {
                    _controllerCompleter.future
                        .then((value) => webViewCon = value);
                    _controllerCompleter.complete(controller);
                  },
                  onPageFinished: (String url) async {},
                  navigationDelegate: (NavigationRequest request) async {
                    if (request.url
                        .contains('https://ezo.zakatselangor.com.my/')) {
                      // launch();
                      launchUrl(
                        Uri.parse(request.url),
                        mode: LaunchMode.externalApplication,
                      );

                      // Navigator.of(context).push(
                      //   MaterialPageRoute(
                      //     builder: (context) => ZakatWeb(
                      //       title: widget.title,
                      //       url: request.url,
                      //     ),
                      //   ),
                      // );
                      return NavigationDecision.prevent;
                    }

                    if (request.url.contains('mailto:aduan@zakat.selangor')) {
                      launchUrl(
                        Uri.parse(request.url),
                        mode: LaunchMode.externalApplication,
                      );
                      return NavigationDecision.prevent;
                    }
                    return NavigationDecision.navigate;
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
