// To parse this JSON data, do
//
//     final zakatList = zakatListFromJson(jsonString);

import 'dart:convert';

ZakatList zakatListFromJson(String str) => ZakatList.fromJson(json.decode(str));

String zakatListToJson(ZakatList data) => json.encode(data.toJson());

class ZakatList {
  ZakatList({
    this.code,
    this.data,
  });

  int? code;
  List<Datum>? data;

  factory ZakatList.fromJson(Map<String, dynamic> json) => ZakatList(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<Datum>.from(json["Data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.programName,
    this.createdBy,
    this.createdTs,
    this.picDir,
    this.linkWeb,
  });

  String? id;
  String? programName;
  String? createdBy;
  DateTime? createdTs;
  String? picDir;
  String? linkWeb;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        programName: json["program_name"] == null ? null : json["program_name"],
        createdBy: json["created_by"] == null ? null : json["created_by"],
        createdTs: json["created_ts"] == null
            ? null
            : DateTime.parse(json["created_ts"]),
        picDir: json["pic_dir"] == null ? null : json["pic_dir"],
        linkWeb: json["link_web"] == null ? null : json["link_web"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "program_name": programName == null ? null : programName,
        "created_by": createdBy == null ? null : createdBy,
        "created_ts": createdTs == null ? null : createdTs?.toIso8601String(),
        "pic_dir": picDir == null ? null : picDir,
        "link_web": linkWeb == null ? null : linkWeb,
      };
}
