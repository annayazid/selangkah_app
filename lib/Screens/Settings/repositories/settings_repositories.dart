import 'package:nb_utils/nb_utils.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:http/http.dart' as http;
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class SettingsRepositories {
  static Future<void> getLanguage(String id, String language) async {
    final Map<String, String> map = {
      'id': id,
      'language': language,
      'token': TOKEN,
    };

    //print('calling post UPDATE_LANGUAGE');

    final response = await http.post(
      Uri.parse('$API_URL/update_user_language'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    final result = response.body;

    if (result.contains('200')) {
      toast('updateSuccess'.tr().toString());
      SecureStorage secureStorage = SecureStorage();
      secureStorage.writeSecureData('language', language);
    } else {
      toast('updateFailed'.tr().toString());
      //print(result);
    }
  }

  static Future<void> deleteAccount() async {
    String id = await SecureStorage().readSecureData('userId');
    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
    };

    final response = await http.post(
      Uri.parse('$API_URL/del_account'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }
}
