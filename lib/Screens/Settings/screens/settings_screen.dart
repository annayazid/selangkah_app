import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/ChangePassword/change_password.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/PDFInAppScreen/pdf_in_app_screen.dart';
import 'package:selangkah_new/Screens/Settings/delete_account/delete_account_cubit.dart';
import 'package:selangkah_new/Screens/Settings/settings.dart';
import 'package:selangkah_new/Screens/SignIn/sign_in.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool cameraOnLoad = false;
  String? language;
  Config? config;

  String? appName;
  String? packageName;
  String? version;
  String? buildNumber;

  void clearCacheLogout() async {
    SecureStorage secureStorage = SecureStorage();
    await secureStorage.deleteAllSecureData();
    MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => PrivacyTermCubit(),
        ),
        BlocProvider(
          create: (context) => CheckUserCubit(),
        ),
      ],
      child: SignInScreen(),
    ).launch(context, isNewTask: true);
  }

  void _getProductVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    setState(() {
      appName = packageInfo.appName;
      packageName = packageInfo.packageName;
      version = packageInfo.version;
      buildNumber = packageInfo.buildNumber;
    });
  }

  void setNewLanguageDropDown(String language) async {
    // if (language == 'Bahasa Malaysia') {
    //   language = 'MY';
    //   context
    //       .read<ProfileBloc>()
    //       .add(UpdateLanguage(id: id, language: language));
    //   await Future.delayed(const Duration(milliseconds: 500), () {
    //     EasyLocalization.of(context).locale = Locale('ms', 'MY');
    //   });
    // } else if (language == 'English') {
    //   language = 'EN';
    //   context
    //       .read<ProfileBloc>()
    //       .add(UpdateLanguage(id: id, language: language));
    //   await Future.delayed(const Duration(milliseconds: 500), () {
    //     EasyLocalization.of(context).locale = Locale('en', 'US');
    //   });
    // }

    Alert(
      style: AlertStyle(),
      onWillPopActive: false,
      context: context,
      type: AlertType.info,
      title: 'change_language'.tr(),
      desc: 'restart_languages'.tr(),
      buttons: [
        DialogButton(
          child: Text(
            'continue'.tr(),
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
            ),
          ),
          onPressed: () async {
            Navigator.of(context).pop();
            SecureStorage secureStorage = SecureStorage();
            String id = await secureStorage.readSecureData('userId');

            if (language == 'Bahasa Malaysia') {
              context.setLocale(Locale('ms', 'MY'));
              language = 'MY';
              await SettingsRepositories.getLanguage(id, language);
            } else {
              context.setLocale(Locale('en', 'US'));
              language = 'EN';
              await SettingsRepositories.getLanguage(id, language);
            }

            MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (context) => UserProfileCubit(),
                ),
                BlocProvider(
                  create: (context) => PrivacyTermCubit(),
                ),
              ],
              child: AppSplashScreen(),
            ).launch(context, isNewTask: true);
          },
          color: Colors.green,
          radius: BorderRadius.circular(10),
        ),
        DialogButton(
          child: Text(
            'cancel'.tr(),
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
          color: Colors.red,
          radius: BorderRadius.circular(10),
        ),
      ],
    ).show();

    // SecureStorage secureStorage = SecureStorage();
    // String id = await secureStorage.readSecureData('guestId');

    // if (language == 'Bahasa Malaysia') {
    //   EasyLocalization.of(context).locale = Locale('ms', 'MY');
    //   language = 'MY';
    //   await SettingsRepositories.getLanguage(id, language);
    // } else {
    //   EasyLocalization.of(context).locale = Locale('en', 'US');
    //   language = 'EN';
    //   await SettingsRepositories.getLanguage(id, language);
    // }
  }

  // switchLanguage(int language) {
  //   LanguageProvider languageProvider = LanguageProvider(context);
  //   languageProvider.sendLanguageData(language, showDialog: false);
  // }

  @override
  void initState() {
    init();
    _getProductVersion();
    super.initState();
  }

  void changePassword() async {
    SecureStorage secureStorage = SecureStorage();
    String phone = await secureStorage.readSecureData('userPhoneNo');

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => ChangePassword(
          hardReset: false,
          phone: phone,
        ),
      ),
    );
  }

  void init() async {
    await GlobalFunction.screenJourney('36');

    // SecureStorage secureStorage = SecureStorage();
    // String? camera = await secureStorage.readSecureData('guestCamera');

    //print(camera);

    // setState(() {
    //   if (camera == null) {
    //     cameraOnLoad = false;
    //   } else if (camera == '0') {
    //     cameraOnLoad = false;
    //   } else if (camera == '1') {
    //     cameraOnLoad = true;
    //   }
    // });

    final Map<String, String> map = {
      'token': TOKEN,
    };

    //print('calling get config');
    final response = await http.post(
      Uri.parse('$API_URL/get_app_config'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    config = configFromJson(response.body);
  }

  Future<File> createFileOfPdfUrl(String url) async {
    Completer<File> completer = Completer();
    //print("Start download file from internet!");
    try {
      final fileurl = url;
      final filename = fileurl.substring(fileurl.lastIndexOf("/") + 1);
      var request = await HttpClient().getUrl(Uri.parse(fileurl));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      var dir = await getApplicationDocumentsDirectory();
      //print("Download files");
      //print("${dir.path}/$filename");
      File file = File("${dir.path}/$filename");

      await file.writeAsBytes(bytes, flush: true);
      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }

    return completer.future;
  }

  Future<void> deleteAccount() async {
    String id = await SecureStorage().readSecureData('userId');
    String phone = await SecureStorage().readSecureData('userPhoneNo');
    bool? isDelete;

    await showDialog(
      context: context,
      builder: (context) {
        return PasswordDelete();
      },
    ).then((value) {
      if (value ?? false) {
        isDelete = value;
      } else {
        isDelete = false;
      }
    });

    print('isDelete is $isDelete');

    if (isDelete!) {
      showDialog(
        context: context,
        builder: (context) {
          return PasswordPopup(
            id: id,
            phone: phone,
          );
        },
      ).then((value) {
        print('value is $value');
        if (value ?? false) {
          print('masuk sini');
          context.read<DeleteAccountCubit>().deleteAccount();
        } else {
          print('masuk sana');
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    language = EasyLocalization.of(context)!.locale.languageCode;
    if (language == 'ms') {
      setState(() {
        language = 'Bahasa Malaysia';
      });
    } else {
      setState(() {
        language = 'English';
      });
    }
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/images/selangkah_logo.png",
          width: size.width * 0.45,
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: Colors.grey,
              size: 30,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              height: size.height * 0.3,
              width: size.width,
              child: Image.asset(
                'assets/images/ProfilePage/background_profile_page.png',
                fit: BoxFit.fitWidth,
              ),
              // Image.asset(
              //   'assets/images/Bg_Large.png',
              //   fit: BoxFit.fitWidth,
              // ),
            ),
            Container(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'settings'.tr(),
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  SizedBox(height: 40),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.0, 1.0), //(x,y)
                          blurRadius: 4.0,
                        ),
                      ],
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Container(
                        //   padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                        //   child: Row(
                        //     children: [
                        //       Icon(
                        //         Icons.camera_alt,
                        //         color: kPrimaryColor,
                        //         size: 20,
                        //       ),
                        //       SizedBox(width: 15),
                        //       Container(
                        //         width: size.width * 0.4,
                        //         child: Text(
                        //           'camera_on_load'.tr(),
                        //           style: TextStyle(
                        //             fontWeight: FontWeight.bold,
                        //             fontSize: 14,
                        //           ),
                        //         ),
                        //       ),
                        //       Expanded(
                        //         child: Align(
                        //           alignment: Alignment.centerRight,
                        //           child: Switch(
                        //             activeColor: kPrimaryColor,
                        //             inactiveThumbColor: kPrimaryColor,
                        //             value: cameraOnLoad,
                        //             onChanged: (value) async {
                        //               String value2;

                        //               if (value) {
                        //                 value2 = '1';
                        //               } else {
                        //                 value2 = '0';
                        //               }

                        //               SecureStorage secureStorage =
                        //                   SecureStorage();
                        //               secureStorage.writeSecureData(
                        //                   'guestCamera', value2);
                        //               setState(() {
                        //                 cameraOnLoad = value;
                        //               });
                        //             },
                        //           ),
                        //         ),
                        //       ),
                        //     ],
                        //   ),
                        // ),
                        // Divider(
                        //   color: Colors.grey[300],
                        //   thickness: 2,
                        //   height: 0,
                        // ),
                        Container(
                          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          child: Row(
                            children: [
                              Icon(
                                FontAwesomeIcons.language,
                                color: kPrimaryColor,
                                size: 20,
                              ),
                              SizedBox(width: 15),
                              Text(
                                'language'.tr(),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                ),
                              ),
                              Expanded(
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: DropdownButton(
                                    value: language,
                                    items: <String>[
                                      'English',
                                      'Bahasa Malaysia'
                                    ].map<DropdownMenuItem<String>>(
                                      (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(
                                            value,
                                            style: TextStyle(
                                              fontSize: 14,
                                            ),
                                          ),
                                        );
                                      },
                                    ).toList(),
                                    onChanged: (value) =>
                                        setNewLanguageDropDown(
                                      value!,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 2,
                          height: 0,
                        ),
                        Material(
                          child: InkWell(
                            onTap: () => changePassword(),
                            child: Container(
                              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                              child: Row(
                                children: [
                                  Icon(
                                    FontAwesomeIcons.key,
                                    color: kPrimaryColor,
                                    size: 20,
                                  ),
                                  SizedBox(width: 15),
                                  Text(
                                    'change_password'.tr(),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                    ),
                                  ),
                                  Expanded(
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Icon(
                                        Icons.arrow_right_rounded,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 2,
                          height: 0,
                        ),
                        Material(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                          child: InkWell(
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20),
                            ),
                            onTap: () {
                              clearCacheLogout();
                            },
                            child: Container(
                              padding: EdgeInsets.fromLTRB(20, 10, 20, 15),
                              child: Row(
                                children: [
                                  Icon(
                                    FontAwesomeIcons.doorOpen,
                                    color: kPrimaryColor,
                                    size: 17,
                                  ),
                                  SizedBox(width: 15),
                                  Text(
                                    'logout'.tr(),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                    ),
                                  ),
                                  Expanded(
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Icon(
                                        Icons.arrow_right_rounded,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 50),
                  Center(
                    child: Text(
                      'need_help'.tr(),
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 10),

                  Center(
                    child: Container(
                      width: size.width * 0.80,
                      child: ElevatedButton.icon(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          backgroundColor: Colors.amber,
                        ),
                        onPressed: () {
                          // launch();
                          launchUrl(
                            Uri.parse('email_cs'.tr()),
                            mode: LaunchMode.externalApplication,
                          );
                        },
                        label: Text(
                          'contact_us_email'.tr(),
                          style: TextStyle(color: Colors.white),
                        ),
                        icon: Icon(
                          Icons.email,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),

                  Center(
                    child: Container(
                      width: size.width * 0.80,
                      child: ElevatedButton.icon(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          backgroundColor: Color(0xFF2CA5E0),
                        ),
                        onPressed: () {
                          launchUrl(
                            Uri.parse(config!.data![0].telegramLink!),
                            mode: LaunchMode.externalApplication,
                          );
                        },
                        label: Text(
                          'contactus'.tr(),
                          style: TextStyle(color: Colors.white),
                        ),
                        icon: Icon(FontAwesomeIcons.telegram,
                            color: Colors.white),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  Center(
                    child: Container(
                      width: size.width * 0.80,
                      child:
                          BlocConsumer<DeleteAccountCubit, DeleteAccountState>(
                        listener: (context, state) {
                          if (state is DeleteAccountDeleted) {
                            Fluttertoast.showToast(
                              msg: 'accountDeleted'.tr(),
                              toastLength: Toast.LENGTH_LONG,
                              gravity: ToastGravity.BOTTOM,
                            );
                            clearCacheLogout();

                            //alert
                          }
                        },
                        builder: (context, state) {
                          if (state is DeleteAccountLoading) {
                            return ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                backgroundColor: Color(0xFFFF0000),
                              ),
                              onPressed: () {},
                              child: SpinKitFadingCircle(
                                color: Colors.white,
                                size: 20,
                              ),
                            );
                          } else {
                            return ElevatedButton.icon(
                              style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                backgroundColor: Color(0xFFFF0000),
                              ),
                              onPressed: () async {
                                await deleteAccount();
                              },
                              label: Text(
                                'Delete Selangkah Account'.tr(),
                                style: TextStyle(color: Colors.white),
                              ),
                              icon: Icon(FontAwesomeIcons.circleMinus,
                                  color: Colors.white),
                            );
                          }
                        },
                      ),
                    ),
                  ),

                  //delete
                  // Center(
                  //   child: Container(
                  //     width: size.width * 0.80,
                  //     child: RaisedButton.icon(
                  //       shape: RoundedRectangleBorder(
                  //           borderRadius: BorderRadius.circular(10)),
                  //       color: Colors.red,
                  //       onPressed: () {
                  //         // launch(config.data[0].telegramLink);
                  //         deleteAccount();
                  //       },
                  //       label: Text(
                  //         'Delete SELANGKAH account',
                  //         style: TextStyle(color: Colors.white),
                  //       ),
                  //       icon: Icon(
                  //         FontAwesomeIcons.trash,
                  //         color: Colors.white,
                  //         size: 16,
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  SizedBox(height: 30),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      GestureDetector(
                        onTap: () {
                          // launch(GlobalVariables.privacy);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PDFInAppPageScreen(
                                url: GlobalVariables.privacy,
                                title: 'privacy'.tr().toString(),
                              ),
                            ),
                          );
                        },
                        child: Text(
                          'privacy'.tr(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: kPrimaryColor,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          // launch(GlobalVariables.termsofuse);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PDFInAppPageScreen(
                                url: GlobalVariables.termsofuse,
                                title: 'termsofuse'.tr(),
                              ),
                            ),
                          );
                        },
                        child: Text(
                          'termsofuse'.tr(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: kPrimaryColor,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 80),

                  Center(
                    child: Text(
                      "Version: $version ($buildNumber)",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
