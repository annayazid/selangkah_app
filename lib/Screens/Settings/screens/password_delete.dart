import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/global.dart';

class PasswordDelete extends StatefulWidget {
  const PasswordDelete({
    super.key,
  });
  @override
  _PasswordDeleteState createState() => _PasswordDeleteState();
}

class _PasswordDeleteState extends State<PasswordDelete> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      scrollable: true,
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          // mainAxisSize:
          //     MainAxisSize.min,
          children: [
            Icon(
              FontAwesomeIcons.circleExclamation,
              color: Colors.red[900],
              size: 50,
            ),
            SizedBox(height: 20),
            Text(
              'AlertDelete'.tr(),
              style: TextStyle(
                color: Color(0xFF505660),
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                    child: Text(
                      'no'.tr(),
                      style: TextStyle(
                        color: Color(0xFF505660),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 15),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: Colors.red[900],
                    ),
                    onPressed: () {
                      Navigator.of(context).pop(true);
                    },
                    child: Text(
                      'yes'.tr(),
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );

    // return AlertDialog(
    //   actions: [
    //     ElevatedButton(
    //       style: ElevatedButton.styleFrom(
    //         shape:
    //             RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    //         backgroundColor: kPrimaryColor,
    //       ),
    //       onPressed: () => checkPassword(),
    //       child: Text('Delete'),
    //     ),
    //   ],
    //   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    //   title: Text(
    //     'To confirm the deletion of your account, enter your password.',
    //     style: TextStyle(),
    //     textAlign: TextAlign.center,
    //   ),
    //   content: TextField(
    //     controller: passwordController,
    //     decoration: InputDecoration(
    //       // hintText: "enter_your_password".tr(),
    //       suffixIcon: IconButton(
    //         onPressed: () {
    //           setState(() {
    //             show = !show;
    //           });
    //         },
    //         icon: Icon(
    //           show ? FontAwesomeIcons.solidEye : FontAwesomeIcons.solidEyeSlash,
    //           size: 17.5,
    //         ),
    //       ),
    //     ),
    //     obscureText: show ? false : true,
    //   ),
    // );
  }
}

class PasswordPopup extends StatefulWidget {
  final String? id;
  final String? phone;

  const PasswordPopup({super.key, this.id, this.phone});

  @override
  State<PasswordPopup> createState() => _PasswordPopupState();
}

class _PasswordPopupState extends State<PasswordPopup> {
  final passwordController = TextEditingController();
  bool? isDelete;
  bool show = false;

  Future<void> checkPassword() async {
    //get device id

    String deviceId = await GlobalFunction.getDeviceId();

    final Map<String, String?> map = {
      'phonenumber': widget.phone,
      'pwd': passwordController.text,
      'device_id': deviceId,
      'token': TOKEN,
    };

    //print('calling post LOGIN');

    final response = await http.post(
      Uri.parse('$API_URL/check_user_login'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    var data = json.decode(response.body);

    if (data['Data'] != false) {
      Fluttertoast.showToast(
        msg: 'success'.tr(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
      Navigator.of(context).pop(true);
      print('object');
    } else {
      //
      Fluttertoast.showToast(
        msg: 'signinerror'.tr(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
      Navigator.of(context).pop(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      scrollable: true,
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          // mainAxisSize:
          //     MainAxisSize.min,
          children: [
            Center(
              child: Text(
                'AlertDeleteDesc'.tr(),
                style: TextStyle(
                  color: Color(0xFF505660),
                  fontWeight: FontWeight.bold,
                  // color: Color(0xFF898DA1),
                  // fontStyle: FontStyle.italic,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 15),
            TextField(
              controller: passwordController,
              decoration: InputDecoration(
                hintText: "password".tr(),
                suffixIcon: IconButton(
                  onPressed: () {
                    setState(() {
                      show = !show;
                    });
                  },
                  icon: Icon(
                    show
                        ? FontAwesomeIcons.solidEye
                        : FontAwesomeIcons.solidEyeSlash,
                    size: 17.5,
                  ),
                ),
              ),
              obscureText: show ? false : true,
            ),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                    child: Text(
                      'cancel'.tr(),
                      style: TextStyle(
                        color: Color(0xFF505660),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 15),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: Colors.red[900],
                    ),
                    onPressed: () async {
                      checkPassword();
                    },
                    child: Text(
                      'delete'.tr(),
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
