part of 'delete_account_cubit.dart';

@immutable
abstract class DeleteAccountState {
  const DeleteAccountState();
}

class DeleteAccountInitial extends DeleteAccountState {
  const DeleteAccountInitial();
}

class DeleteAccountLoading extends DeleteAccountState {
  const DeleteAccountLoading();
}

class DeleteAccountDeleted extends DeleteAccountState {
  const DeleteAccountDeleted();
}
