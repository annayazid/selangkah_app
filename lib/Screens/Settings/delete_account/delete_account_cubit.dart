import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Settings/settings.dart';

part 'delete_account_state.dart';

class DeleteAccountCubit extends Cubit<DeleteAccountState> {
  DeleteAccountCubit() : super(DeleteAccountInitial());

  Future<void> deleteAccount() async {
    emit(DeleteAccountLoading());

    await Future.delayed(Duration(seconds: 2));

    //process
    await SettingsRepositories.deleteAccount();

    emit(DeleteAccountDeleted());
  }
}
