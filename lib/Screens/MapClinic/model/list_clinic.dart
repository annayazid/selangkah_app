// To parse this JSON data, do
//
//     final listClinic = listClinicFromJson(jsonString);

import 'dart:convert';

ListClinic listClinicFromJson(String str) =>
    ListClinic.fromJson(json.decode(str));

String listClinicToJson(ListClinic data) => json.encode(data.toJson());

class ListClinic {
  ListClinic({
    this.code,
    this.data,
  });

  int? code;
  List<ListClinicData>? data;

  factory ListClinic.fromJson(Map<String, dynamic> json) => ListClinic(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<ListClinicData>.from(
                json["Data"].map((x) => ListClinicData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class ListClinicData {
  ListClinicData({
    this.idPpv,
    this.ppvName,
    this.ppvAddressLine1,
    this.ppvAddressLine2,
    this.ppvPostcode,
    this.ppvCategory,
    this.ppvCity,
    this.ppvState,
    this.lat,
    this.lng,
    this.ppvProgram,
    this.services,
    this.operatingHour,
  });

  String? idPpv;
  String? ppvName;
  String? ppvAddressLine1;
  String? ppvAddressLine2;
  String? ppvPostcode;
  String? ppvCategory;
  String? ppvCity;
  String? ppvState;
  String? lat;
  String? lng;
  List<String>? ppvProgram;
  List<String>? services;
  List<OperatingHour>? operatingHour;

  factory ListClinicData.fromJson(Map<String, dynamic> json) => ListClinicData(
        idPpv: json["id_ppv"] == null ? null : json["id_ppv"],
        ppvName: json["ppv_name"] == null ? null : json["ppv_name"],
        ppvAddressLine1: json["ppv_address_line1"] == null
            ? null
            : json["ppv_address_line1"],
        ppvAddressLine2: json["ppv_address_line2"] == null
            ? null
            : json["ppv_address_line2"],
        ppvPostcode: json["ppv_postcode"] == null ? null : json["ppv_postcode"],
        ppvCategory: json["ppv_category"] == null ? null : json["ppv_category"],
        ppvCity: json["ppv_city"] == null ? null : json["ppv_city"],
        ppvState: json["ppv_state"] == null ? null : json["ppv_state"],
        lat: json["lat"] == null ? null : json["lat"],
        lng: json["lng"] == null ? null : json["lng"],
        ppvProgram: json["ppv_program"] == null
            ? null
            : List<String>.from(json["ppv_program"].map((x) => x)),
        services: json["services"] == null
            ? null
            : List<String>.from(json["services"].map((x) => x)),
        operatingHour: json["operating_hour"] == null
            ? null
            : List<OperatingHour>.from(
                json["operating_hour"].map((x) => OperatingHour.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id_ppv": idPpv == null ? null : idPpv,
        "ppv_name": ppvName == null ? null : ppvName,
        "ppv_address_line1": ppvAddressLine1 == null ? null : ppvAddressLine1,
        "ppv_address_line2": ppvAddressLine2 == null ? null : ppvAddressLine2,
        "ppv_postcode": ppvPostcode == null ? null : ppvPostcode,
        "ppv_category": ppvCategory == null ? null : ppvCategory,
        "ppv_city": ppvCity == null ? null : ppvCity,
        "ppv_state": ppvState == null ? null : ppvState,
        "lat": lat == null ? null : lat,
        "lng": lng == null ? null : lng,
        "ppv_program": ppvProgram == null
            ? null
            : List<dynamic>.from(ppvProgram!.map((x) => x)),
        "services": services == null
            ? null
            : List<dynamic>.from(services!.map((x) => x)),
        "operating_hour": operatingHour == null
            ? null
            : List<dynamic>.from(operatingHour!.map((x) => x.toJson())),
      };
}

class OperatingHour {
  OperatingHour({
    this.bizhourName,
    this.startTime,
    this.endTime,
  });

  String? bizhourName;
  String? startTime;
  String? endTime;

  factory OperatingHour.fromJson(Map<String, dynamic> json) => OperatingHour(
        bizhourName: json["bizhour_name"] == null ? null : json["bizhour_name"],
        startTime: json["start_time"] == null ? null : json["start_time"],
        endTime: json["end_time"] == null ? null : json["end_time"],
      );

  Map<String, dynamic> toJson() => {
        "bizhour_name": bizhourName == null ? null : bizhourName,
        "start_time": startTime == null ? null : startTime,
        "end_time": endTime == null ? null : endTime,
      };
}
