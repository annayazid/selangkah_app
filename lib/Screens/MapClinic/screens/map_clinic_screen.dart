import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:maps_launcher/maps_launcher.dart';

import 'package:http/http.dart' as http;

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/MapClinic/map_clinic.dart';
import 'package:selangkah_new/Wallet/Services/navigation_service.dart';
import 'dart:ui' as ui;

import 'package:selangkah_new/utils/endpoint.dart';

class MapClinicScreen extends StatefulWidget {
  const MapClinicScreen({Key? key}) : super(key: key);

  @override
  _MapClinicScreenState createState() => _MapClinicScreenState();
}

class _MapClinicScreenState extends State<MapClinicScreen> {
  GoogleMapController? mapController; //contrller for Google map
  Set<Marker> markers = Set(); //markers for google map
  LatLng currentPostion = LatLng(3.137155985970252, 101.69131524860859);

  @override
  void initState() {
    init();

    //you can add more markers here
    super.initState();
  }

  Future<Uint8List> getBytesFromAsset(String path) async {
    double pixelRatio = MediaQuery.of(context).devicePixelRatio;
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: pixelRatio.round() * 20);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

  void init() async {
    final Map<String, String> map = {
      'token': TOKEN,
    };

    print('calling get config');
    final response = await http.post(
      Uri.parse('$API_URL/list_clinic'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    ListClinic listClinic = listClinicFromJson(response.body);

    // // print(response.body);

    var position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    setState(() {
      currentPostion = LatLng(position.latitude, position.longitude);
    });

    Set<Marker> markersRepo = Set();

    listClinic.data!.forEach((element) async {
      if (element.ppvCategory == 'Selcare-Clinic') {
        markersRepo.add(
          Marker(
            onTap: () async {
              await showModalBottomSheet(
                isScrollControlled: true,
                context: NavigationService
                    .navigatorKey.currentState!.overlay!.context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  ),
                ),
                backgroundColor: Colors.white,
                builder: (_) => BottomDetails(listClinicData: element),
              );
            },
            //add marker on google map
            markerId: MarkerId(
                LatLng(double.parse(element.lat!), double.parse(element.lng!))
                    .toString()),
            position: LatLng(double.parse(element.lat!),
                double.parse(element.lng!)), //position of marker
            icon: BitmapDescriptor.fromBytes(await getBytesFromAsset(
                'assets/images/clinic_locator/marker_red.png')),
          ),
        );
      } else if (element.ppvCategory == 'Panel Clinic') {
        markersRepo.add(
          Marker(
            onTap: () async {
              await showModalBottomSheet(
                isScrollControlled: true,
                context: context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  ),
                ),
                backgroundColor: Colors.white,
                builder: (_) => BottomDetails(listClinicData: element),
              );
            },
            //add marker on google map
            markerId: MarkerId(
                LatLng(double.parse(element.lat!), double.parse(element.lng!))
                    .toString()),
            position: LatLng(double.parse(element.lat!),
                double.parse(element.lng!)), //position of marker

            icon: BitmapDescriptor.fromBytes(await getBytesFromAsset(
                'assets/images/clinic_locator/marker_green.png')),
          ),
        );
      } else if (element.ppvCategory == 'Panel Hospital') {
        markersRepo.add(
          Marker(
            onTap: () async {
              await showModalBottomSheet(
                isScrollControlled: true,
                context: context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  ),
                ),
                backgroundColor: Colors.white,
                builder: (_) => BottomDetails(listClinicData: element),
              );
            },
            //add marker on google map
            markerId: MarkerId(
                LatLng(double.parse(element.lat!), double.parse(element.lng!))
                    .toString()),
            position: LatLng(double.parse(element.lat!),
                double.parse(element.lng!)), //position of marker

            icon: BitmapDescriptor.fromBytes(await getBytesFromAsset(
                'assets/images/clinic_locator/marker_blue.png')),
          ),
        );
      }
    });

    await Future.delayed(Duration(seconds: 1));

    setState(() {
      print('masuk setstate');
      markers = markersRepo;

      print(markers.length);
    });
  }

  //location to show in map
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top -
        AppBar().preferredSize.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/images/selangkah_logo.png",
          width: width * 0.40,
        ),
      ),
      body: Column(
        children: [
          AppBar(
            title: Text(
              'clinic_locator_title'.tr(),
              // style: TextStyle(color: Colors.black),
            ),
          ),
          Container(
            height: height,
            child: Stack(
              children: [
                GoogleMap(
                  //Map widget from google_maps_flutter package
                  zoomGesturesEnabled: true, //enable Zoom in, out on map
                  initialCameraPosition: CameraPosition(
                    //innital position in map
                    target: currentPostion, //initial position
                    zoom: 11.5, //initial zoom level
                  ),
                  markers: markers, //markers to show on map
                  onTap: (argument) {
                    print(argument.latitude);
                    print(argument.longitude);
                  },
                  mapType: MapType.normal, //map type
                  onMapCreated: (controller) {
                    //method called when map is created
                    setState(() {
                      mapController = controller;
                    });
                  },
                  myLocationEnabled: true,
                  myLocationButtonEnabled: false,
                ),
                Positioned(
                  bottom: height * 0.2,
                  right: 8,
                  child: FloatingActionButton(
                    backgroundColor: Colors.white,
                    mini: true,
                    child: Icon(
                      Icons.my_location,
                      color: Colors.blue,
                    ),
                    onPressed: () {
                      mapController!
                          .animateCamera(CameraUpdate.newCameraPosition(
                        CameraPosition(target: currentPostion, zoom: 12),
                        //17 is new zoom level
                      ));
                    },
                  ),
                ),
                Positioned(
                  bottom: height * 0.075,
                  left: 8,
                  child: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.black),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Image.asset(
                              'assets/images/clinic_locator/marker_red.png',
                              width: 14,
                            ),
                            SizedBox(width: 5),
                            Text('selcare_clinic'.tr()),
                          ],
                        ),
                        SizedBox(height: 5),
                        Row(
                          children: [
                            Image.asset(
                              'assets/images/clinic_locator/marker_green.png',
                              width: 14,
                            ),
                            SizedBox(width: 5),
                            Text('panel_clinic'.tr()),
                          ],
                        ),
                        SizedBox(height: 5),
                        Row(
                          children: [
                            Image.asset(
                              'assets/images/clinic_locator/marker_blue.png',
                              width: 14,
                            ),
                            SizedBox(width: 5),
                            Text('panel_hospital'.tr()),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class BottomDetails extends StatefulWidget {
  final ListClinicData listClinicData;

  const BottomDetails({Key? key, required this.listClinicData})
      : super(key: key);

  @override
  _BottomDetailsState createState() => _BottomDetailsState();
}

class _BottomDetailsState extends State<BottomDetails> {
  var controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top -
        AppBar().preferredSize.height;
    // double width = MediaQuery.of(context).size.width;
    return Container(
      padding: EdgeInsets.all(20),
      height: widget.listClinicData.operatingHour!.isEmpty
          ? height * 0.4
          : height * 0.5,
      child: Scrollbar(
        controller: controller,
        thumbVisibility: true,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: double.infinity,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Color(0xFFFBBC04),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onPressed: () {
                    MapsLauncher.launchQuery(widget.listClinicData.ppvName!);
                  },
                  child: Text(
                    'direction'.tr(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
              Text(
                widget.listClinicData.ppvName!.toUpperCase(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                ),
              ),
              SizedBox(height: 5),
              Text(
                '${widget.listClinicData.ppvAddressLine1}, ${widget.listClinicData.ppvAddressLine2}, ${widget.listClinicData.ppvPostcode} ${widget.listClinicData.ppvCity}, ${widget.listClinicData.ppvState},',
                style: TextStyle(
                  fontSize: 14,
                  height: 1.5,
                ),
              ),
              SizedBox(height: 10),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (widget.listClinicData.services!.isNotEmpty) ...[
                          Text(
                            'services_provided'.tr(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                            ),
                          ),

                          SizedBox(height: 5),
                          //here list of services

                          ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: widget.listClinicData.services!.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      '• ${widget.listClinicData.services![index]}'),
                                  SizedBox(height: 5),
                                ],
                              );
                            },
                          ),
                        ],

                        // Text(
                        //   'Operating Hour :',
                        //   style: TextStyle(
                        //     fontWeight: FontWeight.bold,
                        //     fontSize: 15,
                        //   ),
                        // ),
                        // SizedBox(height: 5),
                        // Text('• Monday (9am - 4pm)'),
                        // Text('• Tuesday (9am - 4pm)'),
                        // Text('• Wednesday (9am - 4pm)'),
                      ],
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (widget.listClinicData.services!.isNotEmpty) ...[
                          Text(
                            'program'.tr(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                            ),
                          ),

                          SizedBox(height: 5),
                          //here list of services

                          ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: widget.listClinicData.ppvProgram!.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      '• ${widget.listClinicData.ppvProgram![index]}'),
                                  SizedBox(height: 5),
                                ],
                              );
                            },
                          ),
                        ],
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              if (widget.listClinicData.operatingHour!.isNotEmpty) ...[
                Text(
                  'operating_hour'.tr(),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                  ),
                ),

                SizedBox(height: 5),
                //here list of services

                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: widget.listClinicData.operatingHour!.length,
                  itemBuilder: (BuildContext context, int index) {
                    String timeStart = widget
                        .listClinicData.operatingHour![index].startTime!
                        .substring(0, 5);

                    String timeEnd = widget
                        .listClinicData.operatingHour![index].endTime!
                        .substring(0, 5);

                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '• ${widget.listClinicData.operatingHour![index].bizhourName} ($timeStart - $timeEnd)',
                        ),
                        SizedBox(height: 5),
                      ],
                    );
                  },
                ),
              ],
            ],
          ),
        ),
      ),
    );
  }
}
