import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class AsasRepositories {
  static Future<AsasVideo> getVideo(String language) async {
    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
    };

    print('calling get video');

    final response = await http.post(
      Uri.parse('$API_ASAS/get_vid_url'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    AsasVideo asasVideo = asasVideoFromJson(response.body);

    // AsasVideo asasVideo = asasVideoFromJson(
    //     '{"Code":200,"Data":[{"title":"Recepi masakan","video":[{"id":"32","video_name":"LALARUDIN","display_no":"1","url":"https://www.youtube.com/watch?v=DXUAyRRkI6k"},{"id":"33","video_name":"Telur Dadar","display_no":"2","url":"https://www.youtube.com/watch?v=DXUAyRRkI6k"}]},{"title":"Resepi minuman","video":[{"id":"32","video_name":"LALARUDIN","display_no":"1","url":"https://www.youtube.com/watch?v=DXUAyRRkI6k"},{"id":"33","video_name":"Telur Dadar","display_no":"2","url":"https://www.youtube.com/watch?v=DXUAyRRkI6k"}]}]}');

    return asasVideo;
  }

  static Future<Kids> getKids() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
    };

    print('calling get kids');
    print(map);

    final response = await http.post(
      Uri.parse('$API_ASAS/display_dependent_member'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    log(response.body);

    // try {
    //   Kids kidslist = kidsFromJson(response.body);
    //   return kidslist;
    // } catch (e) {
    //   print(e.toString());
    //   return Kids();
    // }

    Kids kidslist = kidsFromJson(response.body);
    // Kids kidslist = kidsFromJson(
    //     '{"Code":200,"Data":[{"title":"KOHOT 1 SESI 1","date":"2022-01-05","id_sid":"1371444","ic_no":"11655524896","acc_name":"kakakak","dob":"2022-04-22","gender":"M"}]}');
    return kidslist;
  }

  static Future<RawProfile> getProfile() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id': id,
      'token': TOKEN,
    };

    //print('calling post get profile');

    final response = await http.post(
      Uri.parse('$API_URL/check_user_profile'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    // print(response.body);

    RawProfile? profile = rawProfileFromJson(response.body);

    return profile!;
  }

  static Future<AsasAppointment> getAppointment() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    //print('calling post get profile');

    final response = await http.post(
      Uri.parse('$API_ASAS/get_appointment'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    AsasAppointment asasAppointment = asasAppointmentFromJson(response.body);

    // AsasAppointment asasAppointment = asasAppointmentFromJson(
    //     '{"Code":200,"Status":"Success","Data":[{"date":"2022-05-24 00:00:00","site_name":"KOHOT 1 ASAS","time_start":"08:00:00","time_end":"16:00:00"}]}');

    return asasAppointment;
  }

  static Future<int> getStatus() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    print(map);

    //print('calling post get profile');

    final response = await http.post(
      Uri.parse('$API_ASAS/get_status'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    var data = json.decode(response.body);

    int status = data['Data'];
    return status;
  }

  static Future<DependentSignature> getDependentSignature() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    //print('calling post get profile');

    final response = await http.post(
      Uri.parse('$API_ASAS/get_dependent_signature'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print('a');
    print(response.body);

    DependentSignature dependentSignature =
        dependentSignatureFromJson(response.body);

    // DependentSignature dependentSignature = dependentSignatureFromJson(
    //     '{"Code":200,"Data":[{"ic_no":"11023455845","acc_name":"Amira Osman"},{"ic_no":"110234558465","acc_name":"jane"}]}');

    return dependentSignature;
  }

  static Future<void> sendSignature(String img) async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'data': img,
      'token': TOKEN,
    };

    //print('calling post get profile');

    final response = await http.post(
      Uri.parse('$API_ASAS/update_signature'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    // DependentSignature dependentSignature =
    //     dependentSignatureFromJson(response.body);
  }

  static Future<BubbleList> getAsasBubble() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      // 'id_selangkah_user': '2',
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    //print('calling post get profile');

    final response = await http.post(
      Uri.parse('$API_URL/list_my_bubble'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    BubbleList bubbleList = bubbleListFromJson(response.body);

    return bubbleList;
  }

  static Future<void> createAsasBubble() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    Map<String, String> map = {
      'id_sid': id,
      'id_bubble_class': '4',
      'bubble_name': 'ASAS',
      'token': TOKEN,
    };

    //print('calling post get profile');

    var response = await http.post(
      Uri.parse('$API_URL/create_bubble'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    var data = json.decode(response.body);
    String bubbleId = data['Data'];

    //add admin
    String name = await secureStorage.readSecureData('userName');

    map = {
      'token': TOKEN,
      'id_bubble_hdr': '$bubbleId',
      'id_bubble_personal_rel': '0',
      'id_sid': id,
      'is_admin': '1',
      'is_dependent': '1',
      'nick_name': name,
      'create_sid': '0',
    };

    response = await http.post(
      Uri.parse('$API_URL/create_bubble_member'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }

  static Future<BubbleMember> getBubbleMemberList(String bubbleId) async {
    Map<String, String> map = {
      'id_bubble_hdr': bubbleId,
      'token': TOKEN,
    };

    print(map);

    //print('calling post get profile');

    var response = await http.post(
      Uri.parse('$API_URL/list_my_bubble_members'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    log(response.body);

    BubbleMember bubbleMember = bubbleMemberFromJson(response.body);
    return bubbleMember;
  }

  static Future<AsasCorak> getCorak() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      // 'id_selangkah_user': '2',
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    //print('calling post get profile');

    final response = await http.post(
      Uri.parse('$API_ASAS/get_user_records'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    // AsasCorak asasCorak = asasCorakFromJson(response.body);

    AsasCorak asasCorak = asasCorakFromJson(
        '{"Code":200,"Data":[{"acc_name":"Amira Osman","trend":[{"name":"weight","x_axis":"visit","data_x":["1","2","3"],"y_axis":"weight (kg)","data_y":["30","32","33"]},{"name":"height","x_axis":"visit","data_x":["1","2","3"],"y_axis":"height (cm)","data_y":["70","72","73"]}]}]}');
    return asasCorak;
  }

  static Future<AsasCertificate> getCertificate(String id) async {
    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    print('calling post get cert');

    final response = await http.post(
      Uri.parse('$API_ASAS/display_cert_user'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    log(response.body);

    AsasCertificate asasCertificate = asasCertificateFromJson(response.body);
    return asasCertificate;

    // AsasCertificate asasCertificate = asasCertificateFromJson(response.body);
    // // AsasCertificate asasCertificate = asasCertificateFromJson(
    // //     '{"Code":200,"Data":{"title":"KOHOT 1 ASAS","date":"2022-05-23","id_sid":"1371478","ic_no":"912321321312","acc_name":"meow123","dob":"2022-05-23","gender":"M"}}');

    // return asasCertificate;
  }
}
