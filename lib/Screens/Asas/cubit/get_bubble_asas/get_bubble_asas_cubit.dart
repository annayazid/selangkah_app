import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';

part 'get_bubble_asas_state.dart';

class GetBubbleAsasCubit extends Cubit<GetBubbleAsasState> {
  GetBubbleAsasCubit() : super(GetBubbleAsasInitial());

  void getBubbleAsas() async {
    emit(GetBubbleAsasLoading());
    // try {
    //   await Future.delayed(Duration(seconds: 1));

    //   BubbleList bubbleList = await AsasRepositories.getAsasBubble();

    //   if (bubbleList.data.isNotEmpty) {
    //     bool gotAsas = bubbleList.data.any((element) {
    //       return element.bubbleName == 'ASAS';
    //     });

    //     if (gotAsas) {
    //       print('got asas hehe');
    //     } else {
    //       emit(GetBubbleAsasNoAsas());
    //     }
    //   } else {
    //     emit(GetBubbleAsasNoAsas());
    //   }
    // } catch (e) {
    //   emit(GetBubbleAsasError());
    // }

    try {
      await Future.delayed(Duration(seconds: 1));

      BubbleList bubbleList = await AsasRepositories.getAsasBubble();

      if (bubbleList.data!.isNotEmpty) {
        bool gotAsas = bubbleList.data!.any((element) {
          return element.bubbleName == 'ASAS';
        });

        if (gotAsas) {
          int index = bubbleList.data!
              .indexWhere((element) => element.bubbleName == 'ASAS');

          //get bubble member list

          BubbleMember bubbleMember =
              await AsasRepositories.getBubbleMemberList(
                  bubbleList.data![index].id!);

          emit(GetBubbleAsasLoaded(bubbleMember));
        } else {
          emit(GetBubbleAsasNoAsas());
        }
      } else {
        emit(GetBubbleAsasNoAsas());
      }
    } catch (e) {
      emit(GetBubbleAsasError());
    }
  }

  void createAsasBubble() async {
    emit(GetBubbleAsasLoading());

    await Future.delayed(Duration(seconds: 1));

    await AsasRepositories.createAsasBubble();

    getBubbleAsas();

    //create asas here
  }
}
