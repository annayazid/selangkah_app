part of 'get_bubble_asas_cubit.dart';

@immutable
abstract class GetBubbleAsasState {
  const GetBubbleAsasState();
}

class GetBubbleAsasInitial extends GetBubbleAsasState {
  const GetBubbleAsasInitial();
}

class GetBubbleAsasLoading extends GetBubbleAsasState {
  const GetBubbleAsasLoading();
}

class GetBubbleAsasLoaded extends GetBubbleAsasState {
  final BubbleMember bubbleMember;

  GetBubbleAsasLoaded(this.bubbleMember);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetBubbleAsasLoaded && other.bubbleMember == bubbleMember;
  }

  @override
  int get hashCode => bubbleMember.hashCode;
}

class GetBubbleAsasNoAsas extends GetBubbleAsasState {
  const GetBubbleAsasNoAsas();
}

class GetBubbleAsasError extends GetBubbleAsasState {
  const GetBubbleAsasError();
}
