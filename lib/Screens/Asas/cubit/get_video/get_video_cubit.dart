import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';

part 'get_video_state.dart';

class GetVideoCubit extends Cubit<GetVideoState> {
  GetVideoCubit() : super(GetVideoInitial());

  Future<void> getVideo(String language) async {
    emit(GetVideoLoading());

    try {
      AsasVideo asasVideo = await AsasRepositories.getVideo(language);
      if (asasVideo.data!.isNotEmpty) {
        emit(GetVideoLoaded(asasVideo));
      } else {
        emit(GetVideoEmpty());
      }
    } catch (e) {
      print(e.toString());
      emit(GetVideoEmpty());
    }
  }
}
