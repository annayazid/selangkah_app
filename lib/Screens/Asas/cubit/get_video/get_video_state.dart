part of 'get_video_cubit.dart';

@immutable
abstract class GetVideoState {
  const GetVideoState();
}

class GetVideoInitial extends GetVideoState {
  const GetVideoInitial();
}

class GetVideoLoading extends GetVideoState {
  const GetVideoLoading();
}

class GetVideoEmpty extends GetVideoState {
  const GetVideoEmpty();
}

class GetVideoLoaded extends GetVideoState {
  final AsasVideo asasVideo;

  GetVideoLoaded(this.asasVideo);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetVideoLoaded && other.asasVideo == asasVideo;
  }

  @override
  int get hashCode => asasVideo.hashCode;
}
