part of 'get_certificate_cubit.dart';

@immutable
abstract class GetCertificateState {
  const GetCertificateState();
}

class GetCertificateInitial extends GetCertificateState {
  const GetCertificateInitial();
}

class GetCertificateLoading extends GetCertificateState {
  const GetCertificateLoading();
}

class GetCertificateLoaded extends GetCertificateState {
  final AsasCertificate certificate;

  GetCertificateLoaded(this.certificate);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetCertificateLoaded && other.certificate == certificate;
  }

  @override
  int get hashCode => certificate.hashCode;
}

class GetCertificateError extends GetCertificateState {
  const GetCertificateError();
}

class GetCertificateEmpty extends GetCertificateState {
  const GetCertificateEmpty();
}
