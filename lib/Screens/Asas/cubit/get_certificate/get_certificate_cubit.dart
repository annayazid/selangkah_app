import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';

part 'get_certificate_state.dart';

class GetCertificateCubit extends Cubit<GetCertificateState> {
  GetCertificateCubit() : super(GetCertificateInitial());

  void getCertificate(String id) async {
    emit(GetCertificateLoading());

    await Future.delayed(Duration(seconds: 1));

    try {
      AsasCertificate asasCertificate =
          await AsasRepositories.getCertificate(id);

      if (asasCertificate.data!.accName == null ||
          asasCertificate.data!.date == null ||
          asasCertificate.data!.dob == null ||
          asasCertificate.data!.gender == null ||
          asasCertificate.data!.icNo == null ||
          asasCertificate.data!.idSid == null ||
          asasCertificate.data!.title == null) {
        emit(GetCertificateError());
      }

      emit(GetCertificateLoaded(asasCertificate));
    } catch (e) {
      emit(GetCertificateError());
    }
  }
}
