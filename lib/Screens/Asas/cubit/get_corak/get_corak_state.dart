part of 'get_corak_cubit.dart';

@immutable
abstract class GetCorakState {
  const GetCorakState();
}

class GetCorakInitial extends GetCorakState {
  const GetCorakInitial();
}

class GetCorakLoading extends GetCorakState {
  const GetCorakLoading();
}

class GetCorakLoaded extends GetCorakState {
  final AsasCorak asasCorak;
  final int selectedIndex;

  GetCorakLoaded(this.asasCorak, this.selectedIndex);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetCorakLoaded &&
        other.asasCorak == asasCorak &&
        other.selectedIndex == selectedIndex;
  }

  @override
  int get hashCode => asasCorak.hashCode ^ selectedIndex.hashCode;
}

class GetCorakEmpty extends GetCorakState {
  const GetCorakEmpty();
}

class GetCorakError extends GetCorakState {
  const GetCorakError();
}
