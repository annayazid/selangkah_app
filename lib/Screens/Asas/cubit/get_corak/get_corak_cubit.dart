import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';

part 'get_corak_state.dart';

class GetCorakCubit extends Cubit<GetCorakState> {
  GetCorakCubit() : super(GetCorakInitial());

  void getTrend(int selectedIndex) async {
    emit(GetCorakLoading());

    await Future.delayed(Duration(seconds: 1));

    //get data from api
    try {
      AsasCorak asasCorak = await AsasRepositories.getCorak();
      if (asasCorak.data!.isNotEmpty) {
        // List<String> name = asasCorak.data.map((e) => e.accName).toList();

        emit(GetCorakLoaded(asasCorak, selectedIndex));
      } else {
        emit(GetCorakEmpty());
      }
    } catch (e) {
      print(e.toString());
      emit(GetCorakError());
    }
  }
}
