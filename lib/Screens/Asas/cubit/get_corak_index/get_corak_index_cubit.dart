import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';

part 'get_corak_index_state.dart';

class GetCorakIndexCubit extends Cubit<GetCorakIndexState> {
  GetCorakIndexCubit() : super(GetCorakIndexInitial());

  void getTrendByIndex(AsasCorak asasCorak, int index) {
    //get weight
    List<AsasData> asasDataWeight = [];
    if (asasCorak.data![index].trend!
        .any((element) => element.name == 'weight')) {
      int indexWeight = asasCorak.data![index].trend!
          .indexWhere((element) => element.name == 'weight');

      for (var i = 0;
          i < asasCorak.data![index].trend![indexWeight].dataX!.length;
          i++) {
        asasDataWeight.add(AsasData(
          x: asasCorak.data![index].trend![indexWeight].dataX![i],
          y: double.parse(asasCorak.data![index].trend![indexWeight].dataY![i]),
        ));
      }

      print('length is ${asasDataWeight.length}');

      emit(GetCorakIndexLoaded(asasCorak, asasDataWeight));

      //get height
      List<AsasData> asasDataHeight = [];
      if (asasCorak.data![index].trend!
          .any((element) => element.name == 'height')) {
        int indexHeight = asasCorak.data![index].trend!
            .indexWhere((element) => element.name == 'height');

        for (var i = 0;
            i < asasCorak.data![index].trend![indexHeight].dataX!.length;
            i++) {
          asasDataHeight.add(AsasData(
            x: asasCorak.data![index].trend![indexHeight].dataX![i],
            y: double.parse(
                asasCorak.data![index].trend![indexHeight].dataY![i]),
          ));
        }

        print('length is ${asasDataHeight.length}');

        emit(GetCorakIndexLoaded(asasCorak, asasDataHeight));
      }
    }
  }
}
