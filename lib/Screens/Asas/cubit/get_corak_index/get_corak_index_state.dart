part of 'get_corak_index_cubit.dart';

@immutable
abstract class GetCorakIndexState {
  const GetCorakIndexState();
}

class GetCorakIndexInitial extends GetCorakIndexState {
  const GetCorakIndexInitial();
}

class GetCorakIndexLoading extends GetCorakIndexState {
  const GetCorakIndexLoading();
}

class GetCorakIndexLoaded extends GetCorakIndexState {
  final AsasCorak asasCorak;
  final List<AsasData> asasDataWeight;

  GetCorakIndexLoaded(this.asasCorak, this.asasDataWeight);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetCorakIndexLoaded &&
        other.asasCorak == asasCorak &&
        listEquals(other.asasDataWeight, asasDataWeight);
  }

  @override
  int get hashCode => asasCorak.hashCode ^ asasDataWeight.hashCode;
}
