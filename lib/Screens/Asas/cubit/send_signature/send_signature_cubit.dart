import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';

part 'send_signature_state.dart';

class SendSignatureCubit extends Cubit<SendSignatureState> {
  SendSignatureCubit() : super(SendSignatureInitial());

  Future<void> sendSignature(ui.Image image) async {
    emit(SendSignatureLoading());

    ByteData? byeData = await image.toByteData(format: ui.ImageByteFormat.png);

    Uint8List uint8list = byeData!.buffer.asUint8List();
    String base64String = base64Encode(uint8list);

    // log(base64String);

    //process
    await Future.delayed(Duration(seconds: 1));

    await AsasRepositories.sendSignature(base64String);

    emit(SendSignatureSent());
  }
}
