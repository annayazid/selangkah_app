part of 'send_signature_cubit.dart';

@immutable
abstract class SendSignatureState {
  const SendSignatureState();
}

class SendSignatureInitial extends SendSignatureState {
  const SendSignatureInitial();
}

class SendSignatureLoading extends SendSignatureState {
  const SendSignatureLoading();
}

class SendSignatureSent extends SendSignatureState {
  const SendSignatureSent();
}
