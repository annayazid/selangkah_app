import 'dart:convert';
import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';

part 'get_profile_state.dart';

class GetProfileCubit extends Cubit<GetProfileState> {
  GetProfileCubit() : super(GetProfileInitial());

  Future<void> getProfile() async {
    emit(GetProfileLoading());

    //get profile
    RawProfile profile = await AsasRepositories.getProfile();

    Uint8List? imgbytes;

    if (profile.data![18] != null) {
      imgbytes = base64.decode(profile.data![18]!);
    } else {
      imgbytes = null;
    }
    emit(
      GetProfileLoaded(
        profile.data![1]!,
        profile.data![2]!,
        imgbytes,
      ),
    );
  }
}
