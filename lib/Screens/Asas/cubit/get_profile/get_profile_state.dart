part of 'get_profile_cubit.dart';

@immutable
abstract class GetProfileState {
  const GetProfileState();
}

class GetProfileInitial extends GetProfileState {
  const GetProfileInitial();
}

class GetProfileLoading extends GetProfileState {
  const GetProfileLoading();
}

class GetProfileLoaded extends GetProfileState {
  final String id;
  final String name;
  final Uint8List? imgData;

  GetProfileLoaded(this.id, this.name, this.imgData);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetProfileLoaded &&
        other.id == id &&
        other.name == name &&
        other.imgData == imgData;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ imgData.hashCode;
}
