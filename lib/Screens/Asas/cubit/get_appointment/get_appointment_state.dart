part of 'get_appointment_cubit.dart';

@immutable
abstract class GetAppointmentState {
  const GetAppointmentState();
}

class GetAppointmentInitial extends GetAppointmentState {
  const GetAppointmentInitial();
}

class GetAppointmentLoading extends GetAppointmentState {
  const GetAppointmentLoading();
}

class GetAppointmentLoaded extends GetAppointmentState {
  final AsasAppointment asasAppointment;

  GetAppointmentLoaded(this.asasAppointment);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetAppointmentLoaded &&
        other.asasAppointment == asasAppointment;
  }

  @override
  int get hashCode => asasAppointment.hashCode;
}

class GetAppointmentSignature extends GetAppointmentState {
  final DependentSignature dependentSignature;
  final String name;
  final String ic;

  GetAppointmentSignature(this.dependentSignature, this.name, this.ic);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetAppointmentSignature &&
        other.dependentSignature == dependentSignature &&
        other.name == name &&
        other.ic == ic;
  }

  @override
  int get hashCode => dependentSignature.hashCode ^ name.hashCode ^ ic.hashCode;
}

class GetAppointmentError extends GetAppointmentState {
  const GetAppointmentError();
}
