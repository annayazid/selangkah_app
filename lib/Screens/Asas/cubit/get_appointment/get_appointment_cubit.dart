import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';

part 'get_appointment_state.dart';

class GetAppointmentCubit extends Cubit<GetAppointmentState> {
  GetAppointmentCubit() : super(GetAppointmentInitial());

  Future<void> getAppointment() async {
    emit(GetAppointmentLoading());

    //process
    await Future.delayed(Duration(seconds: 1));

    // emit(GetAppointmentLoaded());

    int status = await AsasRepositories.getStatus();

    print(status);

    if (status == 2) {
      //if sign (status != 1)
      try {
        RawProfile profile = await AsasRepositories.getProfile();
        DependentSignature dependentSignature =
            await AsasRepositories.getDependentSignature();

        emit(GetAppointmentSignature(
            dependentSignature, profile.data![2]!, profile.data![4]!));
      } catch (e) {
        print(e.toString());
        emit(GetAppointmentError());
      }
    } else {
      //if not sign (status = 1)
      try {
        AsasAppointment asasAppointment =
            await AsasRepositories.getAppointment();
        emit(GetAppointmentLoaded(asasAppointment));
      } catch (e) {
        print(e.toString());

        emit(GetAppointmentError());
      }
    }
  }
}
