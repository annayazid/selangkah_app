part of 'get_kids_cubit.dart';

@immutable
abstract class GetKidsState {
  const GetKidsState();
}

class GetKidsInitial extends GetKidsState {
  const GetKidsInitial();
}

class GetKidsLoading extends GetKidsState {
  const GetKidsLoading();
}

class GetKidsNoData extends GetKidsState {
  const GetKidsNoData();
}

class GetKidsLoaded extends GetKidsState {
  final Kids kidsList;

  GetKidsLoaded(this.kidsList);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetKidsLoaded && other.kidsList == kidsList;
  }

  @override
  int get hashCode => kidsList.hashCode;
}
