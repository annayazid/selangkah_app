import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';

part 'get_kids_state.dart';

class GetKidsCubit extends Cubit<GetKidsState> {
  GetKidsCubit() : super(GetKidsInitial());

  Future<void> getKids() async {
    emit(GetKidsLoading());

    try {
      Kids kidsList = await AsasRepositories.getKids();
      if (kidsList.data!.length > 0) {
        emit(GetKidsLoaded(kidsList));
      } else {
        emit(GetKidsNoData());
      }
    } catch (e) {
      emit(GetKidsNoData());
    }
  }
}
