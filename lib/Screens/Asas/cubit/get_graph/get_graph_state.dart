part of 'get_graph_cubit.dart';

@immutable
abstract class GetGraphState {
  const GetGraphState();
}

class GetGraphInitial extends GetGraphState {
  const GetGraphInitial();
}

class GetGraphLoading extends GetGraphState {
  const GetGraphLoading();
}

class GetGraphLoaded extends GetGraphState {
  final String url;

  GetGraphLoaded(this.url);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetGraphLoaded && other.url == url;
  }

  @override
  int get hashCode => url.hashCode;
}
