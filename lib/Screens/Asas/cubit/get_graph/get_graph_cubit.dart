import 'package:bloc/bloc.dart';
import 'package:date_format/date_format.dart';
import 'package:encrypt/encrypt.dart';
import 'package:meta/meta.dart';

part 'get_graph_state.dart';

class GetGraphCubit extends Cubit<GetGraphState> {
  GetGraphCubit() : super(GetGraphInitial());

  Future<void> getGraph(String id) async {
    emit(GetGraphInitial());

    //get graph

    //get date
    DateTime now = DateTime.now();
    String date = formatDate(now, [yyyy, mm, dd]);

    print(date);

    // String id = await SecureStorage().readSecureData('guestId');

    final plainText = id;
    // final key = Key.fromUtf8('my32lengthsupersecretnooneknows1');
    // final iv = IV.fromLength(16);

    final b64key = Key.fromUtf8('${date}ujG1LvaqIEyWpthc0QSkiWCW');
    final fernet = Fernet(b64key);
    final encrypter = Encrypter(fernet);

    final encrypted = encrypter.encrypt(plainText);
    final decrypted = encrypter.decrypt(encrypted);

    print(decrypted); // Lorem ipsum dolor sit amet, consectetur adipiscing elit
    print(encrypted.base64); // random cipher text
    print(fernet.extractTimestamp(encrypted.bytes)); // unix timestamp

    String url = 'https://app.selangkah.my/cgc/?id=${encrypted.base64}';

    await Future.delayed(Duration(seconds: 1));

    emit(GetGraphLoaded(url));
  }
}
