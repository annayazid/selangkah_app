// To parse this JSON data, do
//
//     final kids = kidsFromJson(jsonString);

import 'dart:convert';

Kids kidsFromJson(String str) => Kids.fromJson(json.decode(str));

String kidsToJson(Kids data) => json.encode(data.toJson());

class Kids {
  Kids({
    this.code,
    this.data,
  });

  int? code;
  List<KidsData>? data;

  factory Kids.fromJson(Map<String, dynamic> json) => Kids(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<KidsData>.from(
                json["Data"].map((x) => KidsData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class KidsData {
  KidsData({
    this.title,
    this.date,
    this.idSid,
    this.icNo,
    this.accName,
    this.dob,
    this.gender,
  });

  String? title;
  DateTime? date;
  String? idSid;
  String? icNo;
  String? accName;
  DateTime? dob;
  String? gender;

  factory KidsData.fromJson(Map<String, dynamic> json) => KidsData(
        title: json["title"] == null ? null : json["title"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        idSid: json["id_sid"] == null ? null : json["id_sid"],
        icNo: json["ic_no"] == null ? null : json["ic_no"],
        accName: json["acc_name"] == null ? null : json["acc_name"],
        dob: json["dob"] == null ? null : DateTime.parse(json["dob"]),
        gender: json["gender"] == null ? null : json["gender"],
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "date": date == null
            ? null
            : "${date!.year.toString().padLeft(4, '0')}-${date!.month.toString().padLeft(2, '0')}-${date!.day.toString().padLeft(2, '0')}",
        "id_sid": idSid == null ? null : idSid,
        "ic_no": icNo == null ? null : icNo,
        "acc_name": accName == null ? null : accName,
        "dob": dob == null
            ? null
            : "${dob!.year.toString().padLeft(4, '0')}-${dob!.month.toString().padLeft(2, '0')}-${dob!.day.toString().padLeft(2, '0')}",
        "gender": gender == null ? null : gender,
      };
}
