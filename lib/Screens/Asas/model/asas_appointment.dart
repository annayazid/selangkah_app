// To parse this JSON data, do
//
//     final asasAppointment = asasAppointmentFromJson(jsonString);

import 'dart:convert';

AsasAppointment asasAppointmentFromJson(String str) =>
    AsasAppointment.fromJson(json.decode(str));

String asasAppointmentToJson(AsasAppointment data) =>
    json.encode(data.toJson());

class AsasAppointment {
  AsasAppointment({
    this.code,
    this.status,
    this.data,
  });

  int? code;
  String? status;
  List<AsasAppointmentData>? data;

  factory AsasAppointment.fromJson(Map<String, dynamic> json) =>
      AsasAppointment(
        code: json["Code"] == null ? null : json["Code"],
        status: json["Status"] == null ? null : json["Status"],
        data: json["Data"] == null
            ? null
            : List<AsasAppointmentData>.from(
                json["Data"].map((x) => AsasAppointmentData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Status": status == null ? null : status,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class AsasAppointmentData {
  AsasAppointmentData({
    this.date,
    this.siteName,
    this.timeStart,
    this.timeEnd,
  });

  String? date;
  String? siteName;
  String? timeStart;
  String? timeEnd;

  factory AsasAppointmentData.fromJson(Map<String, dynamic> json) =>
      AsasAppointmentData(
        date: json["date"] == null ? null : json["date"],
        siteName: json["site_name"] == null ? null : json["site_name"],
        timeStart: json["time_start"] == null ? null : json["time_start"],
        timeEnd: json["time_end"] == null ? null : json["time_end"],
      );

  Map<String, dynamic> toJson() => {
        "date": date == null ? null : date,
        "site_name": siteName == null ? null : siteName,
        "time_start": timeStart == null ? null : timeStart,
        "time_end": timeEnd == null ? null : timeEnd,
      };
}
