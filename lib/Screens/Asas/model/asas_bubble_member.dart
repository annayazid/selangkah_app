// To parse this JSON data, do
//
//     final bubbleMember = bubbleMemberFromJson(jsonString);

import 'dart:convert';

BubbleMember bubbleMemberFromJson(String str) =>
    BubbleMember.fromJson(json.decode(str));

String bubbleMemberToJson(BubbleMember data) => json.encode(data.toJson());

class BubbleMember {
  BubbleMember({
    this.code,
    this.data,
  });

  final int? code;
  final List<BubbleMemberData>? data;

  factory BubbleMember.fromJson(Map<String, dynamic> json) => BubbleMember(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<BubbleMemberData>.from(
                json["Data"].map((x) => BubbleMemberData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class BubbleMemberData {
  BubbleMemberData({
    this.id,
    this.nickName,
    this.isAdmin,
    this.idSid,
    this.idBubblePersonalRel,
    this.isDependent,
    this.profilePic,
    this.userNo,
    this.phonenumber,
  });

  final String? id;
  final String? nickName;
  final String? isAdmin;
  final String? idSid;
  final String? idBubblePersonalRel;
  final String? isDependent;
  final String? profilePic;
  final String? userNo;
  final String? phonenumber;

  factory BubbleMemberData.fromJson(Map<String, dynamic> json) =>
      BubbleMemberData(
        id: json["id"] == null ? null : json["id"],
        nickName: json["nick_name"] == null ? null : json["nick_name"],
        isAdmin: json["is_admin"] == null ? null : json["is_admin"],
        idSid: json["id_sid"] == null ? null : json["id_sid"],
        idBubblePersonalRel: json["id_bubble_personal_rel"] == null
            ? null
            : json["id_bubble_personal_rel"],
        isDependent: json["is_dependent"] == null ? null : json["is_dependent"],
        profilePic: json["profile_pic"] == null ? null : json["profile_pic"],
        userNo: json["user_no"] == null ? null : json["user_no"],
        phonenumber: json["phonenumber"] == null ? null : json["phonenumber"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "nick_name": nickName == null ? null : nickName,
        "is_admin": isAdmin == null ? null : isAdmin,
        "id_sid": idSid == null ? null : idSid,
        "id_bubble_personal_rel":
            idBubblePersonalRel == null ? null : idBubblePersonalRel,
        "is_dependent": isDependent == null ? null : isDependent,
        "profile_pic": profilePic == null ? null : profilePic,
        "user_no": userNo == null ? null : userNo,
        "phonenumber": phonenumber == null ? null : phonenumber,
      };
}
