// To parse this JSON data, do
//
//     final asasCorak = asasCorakFromJson(jsonString);

import 'dart:convert';

AsasCorak asasCorakFromJson(String str) => AsasCorak.fromJson(json.decode(str));

String asasCorakToJson(AsasCorak data) => json.encode(data.toJson());

class AsasCorak {
  AsasCorak({
    this.code,
    this.data,
  });

  final int? code;
  final List<AsasCorakData>? data;

  factory AsasCorak.fromJson(Map<String, dynamic> json) => AsasCorak(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<AsasCorakData>.from(
                json["Data"].map((x) => AsasCorakData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class AsasCorakData {
  AsasCorakData({
    this.accName,
    this.trend,
  });

  final String? accName;
  final List<Trend>? trend;

  factory AsasCorakData.fromJson(Map<String, dynamic> json) => AsasCorakData(
        accName: json["acc_name"] == null ? null : json["acc_name"],
        trend: json["trend"] == null
            ? null
            : List<Trend>.from(json["trend"].map((x) => Trend.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "acc_name": accName == null ? null : accName,
        "trend": trend == null
            ? null
            : List<dynamic>.from(trend!.map((x) => x.toJson())),
      };
}

class Trend {
  Trend({
    this.name,
    this.xAxis,
    this.dataX,
    this.yAxis,
    this.dataY,
  });

  final String? name;
  final String? xAxis;
  final List<String>? dataX;
  final String? yAxis;
  final List<String>? dataY;

  factory Trend.fromJson(Map<String, dynamic> json) => Trend(
        name: json["name"] == null ? null : json["name"],
        xAxis: json["x_axis"] == null ? null : json["x_axis"],
        dataX: json["data_x"] == null
            ? null
            : List<String>.from(json["data_x"].map((x) => x)),
        yAxis: json["y_axis"] == null ? null : json["y_axis"],
        dataY: json["data_y"] == null
            ? null
            : List<String>.from(json["data_y"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "x_axis": xAxis == null ? null : xAxis,
        "data_x":
            dataX == null ? null : List<dynamic>.from(dataX!.map((x) => x)),
        "y_axis": yAxis == null ? null : yAxis,
        "data_y":
            dataY == null ? null : List<dynamic>.from(dataY!.map((x) => x)),
      };
}
