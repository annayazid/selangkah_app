// To parse this JSON data, do
//
//     final dependentSignature = dependentSignatureFromJson(jsonString);

import 'dart:convert';

DependentSignature dependentSignatureFromJson(String str) =>
    DependentSignature.fromJson(json.decode(str));

String dependentSignatureToJson(DependentSignature data) =>
    json.encode(data.toJson());

class DependentSignature {
  DependentSignature({
    this.code,
    this.data,
  });

  final int? code;
  final List<DependentSignatureData>? data;

  factory DependentSignature.fromJson(Map<String, dynamic> json) =>
      DependentSignature(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<DependentSignatureData>.from(
                json["Data"].map((x) => DependentSignatureData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class DependentSignatureData {
  DependentSignatureData({
    this.icNo,
    this.accName,
  });

  final String? icNo;
  final String? accName;

  factory DependentSignatureData.fromJson(Map<String, dynamic> json) =>
      DependentSignatureData(
        icNo: json["ic_no"] == null ? null : json["ic_no"],
        accName: json["acc_name"] == null ? null : json["acc_name"],
      );

  Map<String, dynamic> toJson() => {
        "ic_no": icNo == null ? null : icNo,
        "acc_name": accName == null ? null : accName,
      };
}
