// To parse this JSON data, do
//
//     final asasVideo = asasVideoFromJson(jsonString);

import 'dart:convert';

AsasVideo asasVideoFromJson(String str) => AsasVideo.fromJson(json.decode(str));

String asasVideoToJson(AsasVideo data) => json.encode(data.toJson());

class AsasVideo {
  AsasVideo({
    this.code,
    this.data,
  });

  int? code;
  List<Datum>? data;

  factory AsasVideo.fromJson(Map<String, dynamic> json) => AsasVideo(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<Datum>.from(json["Data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.title,
    this.video,
  });

  String? title;
  List<Video>? video;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        title: json["title"] == null ? null : json["title"],
        video: json["video"] == null
            ? null
            : List<Video>.from(json["video"].map((x) => Video.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "video": video == null
            ? null
            : List<dynamic>.from(video!.map((x) => x.toJson())),
      };
}

class Video {
  Video({
    this.id,
    this.videoName,
    this.displayNo,
    this.url,
  });

  String? id;
  String? videoName;
  String? displayNo;
  String? url;

  factory Video.fromJson(Map<String, dynamic> json) => Video(
        id: json["id"] == null ? null : json["id"],
        videoName: json["video_name"] == null ? null : json["video_name"],
        displayNo: json["display_no"] == null ? null : json["display_no"],
        url: json["url"] == null ? null : json["url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "video_name": videoName == null ? null : videoName,
        "display_no": displayNo == null ? null : displayNo,
        "url": url == null ? null : url,
      };
}
