// To parse this JSON data, do
//
//     final bubbleList = bubbleListFromJson(jsonString);

import 'dart:convert';

BubbleList bubbleListFromJson(String str) =>
    BubbleList.fromJson(json.decode(str));

String bubbleListToJson(BubbleList data) => json.encode(data.toJson());

class BubbleList {
  BubbleList({
    this.code,
    this.data,
  });

  final int? code;
  final List<BubbleListData>? data;

  factory BubbleList.fromJson(Map<String, dynamic> json) => BubbleList(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<BubbleListData>.from(
                json["Data"].map((x) => BubbleListData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

BubbleListData bubbleListDataFromJson(String str) =>
    BubbleListData.fromJson(json.decode(str));

String bubbleListDataToJson(BubbleListData data) => json.encode(data.toJson());

class BubbleListData {
  BubbleListData({
    this.id,
    this.bubbleName,
    this.idBubbleClass,
    this.isAdmin,
    this.className,
  });

  final String? id;
  final String? bubbleName;
  final String? idBubbleClass;
  final String? isAdmin;
  final String? className;

  factory BubbleListData.fromJson(Map<String, dynamic> json) => BubbleListData(
        id: json["id"] == null ? null : json["id"],
        bubbleName: json["bubble_name"] == null ? null : json["bubble_name"],
        idBubbleClass:
            json["id_bubble_class"] == null ? null : json["id_bubble_class"],
        isAdmin: json["is_admin"] == null ? null : json["is_admin"],
        className: json["class_name"] == null ? null : json["class_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "bubble_name": bubbleName == null ? null : bubbleName,
        "id_bubble_class": idBubbleClass == null ? null : idBubbleClass,
        "is_admin": isAdmin == null ? null : isAdmin,
        "class_name": className == null ? null : className,
      };
}
