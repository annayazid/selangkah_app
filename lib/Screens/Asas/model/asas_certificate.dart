// To parse this JSON data, do
//
//     final asasCertificate = asasCertificateFromJson(jsonString);

import 'dart:convert';

AsasCertificate asasCertificateFromJson(String str) =>
    AsasCertificate.fromJson(json.decode(str));

String asasCertificateToJson(AsasCertificate data) =>
    json.encode(data.toJson());

class AsasCertificate {
  AsasCertificate({
    this.code,
    this.data,
  });

  final int? code;
  final AsasCertificateData? data;

  factory AsasCertificate.fromJson(Map<String, dynamic> json) =>
      AsasCertificate(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : AsasCertificateData.fromJson(json["Data"]),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null ? null : data!.toJson(),
      };
}

class AsasCertificateData {
  AsasCertificateData({
    this.title,
    this.date,
    this.idSid,
    this.icNo,
    this.accName,
    this.dob,
    this.gender,
  });

  final String? title;
  final DateTime? date;
  final String? idSid;
  final String? icNo;
  final String? accName;
  final DateTime? dob;
  final String? gender;

  factory AsasCertificateData.fromJson(Map<String, dynamic> json) =>
      AsasCertificateData(
        title: json["title"] == null ? null : json["title"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        idSid: json["id_sid"] == null ? null : json["id_sid"],
        icNo: json["ic_no"] == null ? null : json["ic_no"],
        accName: json["acc_name"] == null ? null : json["acc_name"],
        dob: json["dob"] == null ? null : DateTime.parse(json["dob"]),
        gender: json["gender"] == null ? null : json["gender"],
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "date": date == null
            ? null
            : "${date!.year.toString().padLeft(4, '0')}-${date!.month.toString().padLeft(2, '0')}-${date!.day.toString().padLeft(2, '0')}",
        "id_sid": idSid == null ? null : idSid,
        "ic_no": icNo == null ? null : icNo,
        "acc_name": accName == null ? null : accName,
        "dob": dob == null
            ? null
            : "${dob!.year.toString().padLeft(4, '0')}-${dob!.month.toString().padLeft(2, '0')}-${dob!.day.toString().padLeft(2, '0')}",
        "gender": gender == null ? null : gender,
      };
}
