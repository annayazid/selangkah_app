import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';
import 'package:selangkah_new/utils/constants.dart';

class KidsCertificate extends StatelessWidget {
  final String id;

  const KidsCertificate({Key? key, required this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    context.read<GetCertificateCubit>().getCertificate(id);

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return AsasScaffold(
      appBarTitle: 'Senarai Anak',
      content: BlocConsumer<GetCertificateCubit, GetCertificateState>(
        listener: (context, state) {
          if (state is GetCertificateError) {
            Navigator.of(context).pop();
            Fluttertoast.showToast(
              msg: 'Something wrong, please try again later',
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0,
            );
          } else if (state is GetCertificateEmpty) {
            Navigator.of(context).pop();
            Fluttertoast.showToast(
              msg: 'This person does not have certificate yet.',
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0,
            );
          }
        },
        builder: (context, state) {
          if (state is GetCertificateLoaded) {
            return SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    // height: 600,
                    margin: EdgeInsets.all(15),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Color(0xFFE9DDAD),
                      borderRadius: BorderRadius.circular(60),
                    ),
                    child: Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: Color(0xFFE9DDAD),
                        borderRadius: BorderRadius.circular(60),
                        border: Border.all(
                          width: 5,
                          color: Color(0xFFDECE91),
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                child: Column(
                                  children: [
                                    Image.asset(
                                      'assets/images/Asas/jata.png',
                                      height: height * 0.08,
                                    ),
                                    SizedBox(height: 10),
                                    Text(
                                      'KERAJAAN NEGERI SELANGOR',
                                      style: TextStyle(
                                        fontSize: 7,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Text(
                                  'SIJIL PENERIMAAN PEK ASAS BAGI ${state.certificate.data!.title}',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    // fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    height: 2,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10),
                          Divider(
                            color: Color(0xFFDECE91),
                            // height: 3,
                            thickness: 5,
                          ),
                          Container(
                            padding: EdgeInsets.all(30),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Dengan ini telah disahkan bahawa',
                                  style: TextStyle(
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.bold,
                                    color: Color(0xFFB0692F),
                                    fontSize: 12,
                                  ),
                                ),
                                SizedBox(height: 15),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Text(
                                        'Nama',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Text(
                                        ':',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Text(
                                        state.certificate.data!.accName!,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 15),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Text(
                                        'Tarikh Lahir',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Text(
                                        ':',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Text(
                                        formatDate(state.certificate.data!.dob!,
                                            [dd, '/', mm, '/', yyyy]),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 15),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Text(
                                        'Jantina',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Text(
                                        ':',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Text(
                                        state.certificate.data!.gender == 'M'
                                            ? 'Lelaki'
                                            : 'Perempuan',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 15),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Text(
                                        'Warganegara',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Text(
                                        ':',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Text(
                                        'Malaysia',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 15),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Text(
                                        'No IC',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Text(
                                        ':',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Text(
                                        state.certificate.data!.icNo!,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 15),
                                Text(
                                  'telah menerima pek ASAS pada ${formatDate(state.certificate.data!.date!, [
                                        dd,
                                        '/',
                                        mm,
                                        '/',
                                        yyyy
                                      ])} (tarikh terima pek ASAS)',
                                  maxLines: 2,
                                  style: TextStyle(
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.bold,
                                    color: Color(0xFFB0692F),
                                    fontSize: 12,
                                    height: 2,
                                  ),
                                ),
                                SizedBox(height: 30),
                                Row(
                                  // mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: Column(
                                        children: [
                                          Container(
                                            color: Colors.yellow,
                                            child: Text(
                                              'KERJASAMA BERSAMA',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Color(0xFFDF8C53),
                                                fontSize: 10,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            'INTERNATIONAL ISLAMIC UNIVERSITY MALAYSIA (IIUM)',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold,
                                              color: Color(0xFFDF8C53),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Expanded(
                                      child: Column(
                                        children: [
                                          Container(
                                            color: Colors.yellow,
                                            child: Text(
                                              'PENGANJUR',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Color(0xFFDF8C53),
                                                fontSize: 10,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            'ANAK SELANGOR ANAK SIHAT (ASAS)',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Color(0xFFDF8C53),
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: 15),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        height: 60,
                                        child: Image.asset(
                                          'assets/images/Asas/uia_logo.png',
                                          width: width * 0.40,
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Expanded(
                                      child: Container(
                                        height: 60,
                                        child: Image.asset(
                                          'assets/images/Asas/asas_logo_2.png',
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else {
            return Center(
              child: SpinKitFadingCircle(color: kPrimaryColor, size: 25),
            );
          }
        },
      ),
    );
  }
}
