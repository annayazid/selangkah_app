// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:intl/intl.dart';
// import 'package:selangkah/Screens/Asas/asas.dart';
// import 'package:selangkah/util/constants.dart';

// import 'package:syncfusion_flutter_charts/charts.dart';
// import 'package:easy_localization/easy_localization.dart';
// import 'package:fluttertoast/fluttertoast.dart';

// class AsasTrend extends StatefulWidget {
//   const AsasTrend({Key key}) : super(key: key);

//   @override
//   _AsasTrendState createState() => _AsasTrendState();
// }

// class _AsasTrendState extends State<AsasTrend> {
//   @override
//   void initState() {
//     context.read<GetCorakCubit>().getTrend(0);
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return AsasScaffold(
//       appBarTitle: 'Asas',
//       content: BlocConsumer<GetCorakCubit, GetCorakState>(
//         listener: (context, state) {
//           if (state is GetCorakError) {
//             Navigator.of(context).pop();
//             Fluttertoast.showToast(
//               msg: 'Something wrong, please try again later',
//               toastLength: Toast.LENGTH_SHORT,
//               gravity: ToastGravity.BOTTOM,
//               timeInSecForIosWeb: 1,
//               backgroundColor: Colors.red,
//               textColor: Colors.white,
//               fontSize: 16.0,
//             );
//           }
//         },
//         builder: (context, state) {
//           print(state.toString());
//           if (state is GetCorakLoaded) {
//             return CorakLoadedWidget(
//               asasCorak: state.asasCorak,
//               selectedIndex: state.selectedIndex,
//             );
//           } else if (state is GetCorakEmpty) {
//             return Center(
//               child: Text(
//                 'no_depend_asas'.tr(),
//                 style: TextStyle(
//                   fontWeight: FontWeight.bold,
//                 ),
//               ),
//             );
//           } else {
//             return SpinKitFadingCircle(
//               color: kPrimaryColor,
//               size: 25,
//             );
//           }
//         },
//       ),
//     );
//   }
// }

// // class CorakLoadedWidget extends StatefulWidget {
// //   final AsasCorak asasCorak;

// //   const CorakLoadedWidget({Key key, this.asasCorak}) : super(key: key);

// //   @override
// //   _CorakLoadedWidgetState createState() => _CorakLoadedWidgetState();
// // }

// // class _CorakLoadedWidgetState extends State<CorakLoadedWidget> {
// //   List<String> _anak;

// //   String _currentSelected;

// //   // List<AsasData> asasData = [];
// //   TooltipBehavior _tooltipBehaviorWeight;
// //   TooltipBehavior _tooltipBehaviorHeight;
// //   TooltipBehavior _tooltipBehavioBMI;

// //   @override
// //   void initState() {
// //     context.read<GetCorakIndexCubit>().getTrendByIndex(widget.asasCorak, 0);
// //     _anak = widget.asasCorak.data.map((e) => e.accName).toList();
// //     _currentSelected = _anak[0];

// //     _tooltipBehaviorWeight = TooltipBehavior(enable: true);
// //     _tooltipBehaviorHeight = TooltipBehavior(enable: true);
// //     _tooltipBehavioBMI = TooltipBehavior(enable: true);
// //     super.initState();
// //   }

// //   @override
// //   Widget build(BuildContext context) {
// //     double width = MediaQuery.of(context).size.width;

// //     return BlocBuilder<GetCorakIndexCubit, GetCorakIndexState>(
// //       builder: (context, state) {
// //         if (state is GetCorakIndexLoaded) {
// //           return SingleChildScrollView(
// //             padding: EdgeInsets.all(20),
// //             child: Column(
// //               children: [
// //                 Row(
// //                   mainAxisAlignment: MainAxisAlignment.spaceAround,
// //                   children: [
// //                     Text(
// //                       'Anak',
// //                       style: TextStyle(),
// //                     ),
// //                     Container(
// //                       width: width * 0.6,
// //                       child: InputDecorator(
// //                         decoration: InputDecoration(
// //                           contentPadding: EdgeInsets.symmetric(
// //                             horizontal: 15,
// //                           ),
// //                           border: OutlineInputBorder(
// //                             borderRadius:
// //                                 BorderRadius.all(Radius.circular(10.0)),
// //                           ),
// //                         ),
// //                         child: DropdownButtonHideUnderline(
// //                           child: DropdownButton<String>(
// //                             isExpanded: true,
// //                             icon: Icon(
// //                               Icons.keyboard_arrow_down,
// //                               size: 30,
// //                               color: Color(0xFFF36B21),
// //                             ),
// //                             value: _currentSelected,
// //                             onChanged: (String value) {
// //                               setState(
// //                                 () {
// //                                   _currentSelected = value;
// //                                 },
// //                               );
// //                             },
// //                             items: _anak
// //                                 .map<DropdownMenuItem<String>>((String value) {
// //                               return DropdownMenuItem<String>(
// //                                 value: value,
// //                                 child: Text(
// //                                   value,
// //                                   style: TextStyle(
// //                                     fontSize: 14,
// //                                   ),
// //                                   overflow: TextOverflow.ellipsis,
// //                                   // maxLines: 2,
// //                                 ),
// //                               );
// //                             }).toList(),
// //                           ),
// //                         ),
// //                       ),
// //                     ),
// //                   ],
// //                 ),
// //                 SizedBox(height: 20),

// //                 //weight
// //                 if (state.asasDataWeight.isNotEmpty)
// //                   SfCartesianChart(
// //                     title: ChartTitle(text: 'Weight Trend'),
// //                     legend: Legend(
// //                       isVisible: true,
// //                       position: LegendPosition.bottom,
// //                     ),
// //                     tooltipBehavior: _tooltipBehaviorWeight,
// //                     series: <ChartSeries>[
// //                       LineSeries<AsasData, String>(
// //                         name: 'Weight',
// //                         dataSource: state.asasDataWeight,
// //                         xValueMapper: (AsasData asasX, _) => asasX.x,
// //                         yValueMapper: (AsasData asasY, _) => asasY.y,
// //                       ),
// //                     ],
// //                     primaryXAxis: CategoryAxis(
// //                       title: AxisTitle(
// //                           text: widget.asasCorak.data[0].trend[0].xAxis
// //                               .toUpperCase()),
// //                     ),
// //                     primaryYAxis: NumericAxis(
// //                       title: AxisTitle(
// //                           text: widget.asasCorak.data[0].trend[0].yAxis
// //                               .toUpperCase()),
// //                     ),
// //                     //   // enableAxisAnimation: true,
// //                   ),
// //               ],
// //             ),
// //           );
// //         } else {
// //           return SpinKitFadingCircle(
// //             color: kPrimaryColor,
// //             size: 25,
// //           );
// //         }
// //       },
// //     );
// //   }
// // }

// class CorakLoadedWidget extends StatefulWidget {
//   final AsasCorak asasCorak;
//   final int selectedIndex;

//   const CorakLoadedWidget({Key key, this.asasCorak, this.selectedIndex})
//       : super(key: key);

//   @override
//   _CorakLoadedWidgetState createState() => _CorakLoadedWidgetState();
// }

// class _CorakLoadedWidgetState extends State<CorakLoadedWidget> {
//   List<String> _anak;

//   int _selectedIndex = 0;

//   List<AsasData> asasData = [];
//   // TooltipBehavior _tooltipBehaviorWeight;
//   // TooltipBehavior _tooltipBehaviorHeight;
//   // TooltipBehavior _tooltipBehavioBMI;

//   List<TooltipBehavior> _tooltipBehaviors = [];

//   @override
//   void initState() {
//     _anak = widget.asasCorak.data.map((e) => e.accName).toList();
//     _selectedIndex = widget.selectedIndex;

//     for (var i = 0;
//         i < widget.asasCorak.data[widget.selectedIndex].trend.length;
//         i++) {
//       _tooltipBehaviors.add(TooltipBehavior(enable: true));
//     }

//     // _tooltipBehaviors = widget.asasCorak.data;
//     // _tooltipBehaviorWeight = TooltipBehavior(enable: true);
//     // _tooltipBehaviorHeight = TooltipBehavior(enable: true);
//     // _tooltipBehavioBMI = TooltipBehavior(enable: true);
//     super.initState();
//   }

//   List<AsasData> getData(int indexTrend) {
//     List<AsasData> data = [];
//     for (int i = 0;
//         i <
//             widget
//                 .asasCorak.data[_selectedIndex].trend[indexTrend].dataX.length;
//         i++) {
//       data.add(AsasData(
//         x: widget.asasCorak.data[_selectedIndex].trend[indexTrend].dataX[i],
//         y: double.parse(
//             widget.asasCorak.data[_selectedIndex].trend[indexTrend].dataY[i]),
//       ));
//     }
//     return data;
//   }

//   @override
//   Widget build(BuildContext context) {
//     double width = MediaQuery.of(context).size.width;

//     return SingleChildScrollView(
//       padding: EdgeInsets.all(20),
//       child: Column(
//         children: [
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceAround,
//             children: [
//               Text(
//                 'kid'.tr(),
//                 style: TextStyle(),
//               ),
//               Container(
//                 width: width * 0.6,
//                 child: InputDecorator(
//                   decoration: InputDecoration(
//                     contentPadding: EdgeInsets.symmetric(
//                       horizontal: 15,
//                     ),
//                     border: OutlineInputBorder(
//                       borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                     ),
//                   ),
//                   child: DropdownButtonHideUnderline(
//                     child: DropdownButton<String>(
//                       isExpanded: true,
//                       icon: Icon(
//                         Icons.keyboard_arrow_down,
//                         size: 30,
//                         color: Color(0xFFF36B21),
//                       ),
//                       value: _anak[_selectedIndex],
//                       onChanged: (String value) {
//                         setState(
//                           () {
//                             _selectedIndex = _anak.indexOf(value);
//                           },
//                         );
//                         context.read<GetCorakCubit>().getTrend(_selectedIndex);
//                       },
//                       items:
//                           _anak.map<DropdownMenuItem<String>>((String value) {
//                         return DropdownMenuItem<String>(
//                           value: value,
//                           child: Text(
//                             value,
//                             style: TextStyle(
//                               fontSize: 14,
//                             ),
//                             overflow: TextOverflow.ellipsis,
//                             // maxLines: 2,
//                           ),
//                         );
//                       }).toList(),
//                     ),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//           SizedBox(height: 20),

//           //weight
//           ListView.builder(
//             physics: NeverScrollableScrollPhysics(),
//             shrinkWrap: true,
//             itemCount: widget.asasCorak.data[_selectedIndex].trend.length,
//             itemBuilder: (BuildContext context, int trendIndex) {
//               return Column(
//                 children: [
//                   SfCartesianChart(
//                     title: ChartTitle(
//                         text: toBeginningOfSentenceCase(
//                             '${widget.asasCorak.data[_selectedIndex].trend[trendIndex].name} Trend')),
//                     legend: Legend(
//                       isVisible: false,
//                       position: LegendPosition.bottom,
//                     ),
//                     tooltipBehavior: _tooltipBehaviors[trendIndex],
//                     series: <ChartSeries>[
//                       // ColumnSeries<AsasData, String>(
//                       //   name: widget.asasCorak.data[_selectedIndex]
//                       //       .trend[trendIndex].name
//                       //       .toUpperCase(),
//                       //   dataSource: getData(trendIndex),
//                       //   xValueMapper: (AsasData asasX, _) => asasX.x,
//                       //   yValueMapper: (AsasData asasY, _) => asasY.y,
//                       // ),
//                       LineSeries<AsasData, String>(
//                         name: widget.asasCorak.data[_selectedIndex]
//                             .trend[trendIndex].name
//                             .toUpperCase(),
//                         dataSource: getData(trendIndex),
//                         // pointColorMapper: (AsasData asasX, _) => Colors.black,
//                         xValueMapper: (AsasData asasX, _) => asasX.x,
//                         yValueMapper: (AsasData asasY, _) => asasY.y,
//                       ),
//                     ],
//                     primaryXAxis: CategoryAxis(
//                       title: AxisTitle(
//                         text: toBeginningOfSentenceCase(widget.asasCorak
//                             .data[_selectedIndex].trend[trendIndex].xAxis),
//                       ),
//                     ),
//                     primaryYAxis: NumericAxis(
//                       title: AxisTitle(
//                           text: toBeginningOfSentenceCase(widget.asasCorak
//                               .data[_selectedIndex].trend[trendIndex].yAxis
//                               .toUpperCase())),
//                     ),
//                     //   // enableAxisAnimation: true,
//                   ),
//                   SizedBox(height: 20),
//                 ],
//               );
//             },
//           ),

//           // //height
//           // SfCartesianChart(
//           //   title: ChartTitle(text: 'Height Trend'),
//           //   legend: Legend(
//           //     isVisible: true,
//           //     position: LegendPosition.bottom,
//           //   ),
//           //   tooltipBehavior: _tooltipBehaviorHeight,
//           //   series: <ChartSeries>[
//           //     LineSeries<AsasWeight, String>(
//           //       color: Colors.orange,
//           //       name: 'Height',
//           //       dataSource: asasWeight,
//           //       xValueMapper: (AsasWeight asasWeight, _) => asasWeight.month,
//           //       yValueMapper: (AsasWeight asasWeight, _) => asasWeight.weight,
//           //     ),
//           //   ],
//           //   primaryXAxis: CategoryAxis(
//           //     title: AxisTitle(text: 'Month'),
//           //   ),
//           //   primaryYAxis: NumericAxis(
//           //     title: AxisTitle(text: 'Height (CM)'),
//           //     visibleMaximum: 200,
//           //   ),
//           //   // enableAxisAnimation: true,
//           // ),
//           // SizedBox(height: 20),
//           // SfCartesianChart(
//           //   title: ChartTitle(text: 'BMI Trend'),
//           //   legend: Legend(
//           //     isVisible: true,
//           //     position: LegendPosition.bottom,
//           //   ),
//           //   tooltipBehavior: _tooltipBehavioBMI,
//           //   series: <ChartSeries>[
//           //     LineSeries<AsasWeight, String>(
//           //       color: Colors.purple,
//           //       name: 'BMI',
//           //       dataSource: asasWeight,
//           //       xValueMapper: (AsasWeight asasWeight, _) => asasWeight.month,
//           //       yValueMapper: (AsasWeight asasWeight, _) => asasWeight.weight,
//           //     ),
//           //   ],
//           //   primaryXAxis: CategoryAxis(
//           //     title: AxisTitle(text: 'Month'),
//           //   ),
//           //   primaryYAxis: NumericAxis(
//           //     title: AxisTitle(text: 'BMI'),
//           //     visibleMaximum: 60,
//           //   ),
//           //   // enableAxisAnimation: true,
//           // ),
//         ],
//       ),
//     );
//   }
// }
