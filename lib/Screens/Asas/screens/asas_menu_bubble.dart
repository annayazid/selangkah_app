import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';

class BubbleMenu extends StatelessWidget {
  const BubbleMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return AsasScaffold(
        appBarTitle: 'swabook_button2'.tr(),
        content: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: double.infinity,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Image.asset(
                      'assets/images/Asas/asas-banner-bg.png',
                      // fit: BoxFit.fitHeight,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: Container(
                            width: width * 0.75,
                            child: Image.asset(
                              'assets/images/Asas/asas_logo.png',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                child: Row(
                  children: [
                    _deliveryButton(width, 'Asas Bubble',
                        'assets/images/Asas/asas_bubble1.png', () {}),
                    Spacer(),
                    _deliveryButton(width, 'Sijil Anak',
                        'assets/images/Asas/asas_anak2.png', () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (builder) => BlocProvider(
                            create: (context) => GetKidsCubit(),
                            child: KidsList(),
                          ),
                        ),
                      );
                    }),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  InkWell _deliveryButton(width, title, img, function) {
    return InkWell(
      onTap: function,
      child: Container(
        height: width * 0.42,
        width: width * 0.42,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              offset: Offset(0, 0),
              blurRadius: 5,
              spreadRadius: 3,
            ),
          ],
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(img),
            SizedBox(
              height: 15,
            ),
            Text(
              title,
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w700,
                color: Color(0xFF505660),
              ),
            )
          ],
        ),
      ),
    );
  }
}
