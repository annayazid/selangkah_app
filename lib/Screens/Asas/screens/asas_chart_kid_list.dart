import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';
import 'package:selangkah_new/utils/constants.dart';

class ChartKidList extends StatefulWidget {
  const ChartKidList({Key? key}) : super(key: key);

  @override
  _ChartKidListState createState() => _ChartKidListState();
}

class _ChartKidListState extends State<ChartKidList> {
  void initState() {
    context.read<GetKidsCubit>().getKids();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    return AsasScaffold(
      appBarTitle: 'Asas',
      content: BlocBuilder<GetKidsCubit, GetKidsState>(
        builder: (context, state) {
          if (state is GetKidsLoaded) {
            return ChartKidListLoaded(kidsList: state.kidsList);
          } else if (state is GetKidsNoData) {
            return GetKidsNoDataWidget();
          } else {
            return Padding(
              padding: EdgeInsets.only(top: height * 0.175),
              child: SpinKitFadingCircle(
                color: Color(0xFFD39400),
                size: 25,
              ),
            );
          }
        },
      ),
    );
  }
}

class ChartKidListLoaded extends StatefulWidget {
  final Kids kidsList;

  const ChartKidListLoaded({Key? key, required this.kidsList})
      : super(key: key);

  @override
  _ChartKidListLoadedState createState() => _ChartKidListLoadedState();
}

class _ChartKidListLoadedState extends State<ChartKidListLoaded> {
  List<String> selIdList = [];

  @override
  void initState() {
    for (var i = 0; i < widget.kidsList.data!.length; i++) {
      // String selId = '24';
      String selId = widget.kidsList.data![i].idSid!;
      int left = 11 - selId.length;
      int count0 = left - 2;
      String zero = '';

      for (var i = 0; i < count0; i++) {
        zero = zero + '0';
      }

      if (zero != '') {
        zero = zero.replaceRange(0, 1, '1');
      }

      selId = 'SL$zero$selId';

      selIdList.add(selId);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    return SingleChildScrollView(
      child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: double.infinity,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Image.asset(
                      'assets/images/Asas/asas-banner-bg.png',
                      // fit: BoxFit.fitHeight,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: Container(
                            width: width * 0.75,
                            child: Image.asset(
                              'assets/images/Asas/asas_logo.png',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 15),
              Container(
                width: width * 0.95,
                padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      offset: Offset(0, 0),
                      blurRadius: 5,
                      // spreadRadius: 3,
                    ),
                  ],
                  borderRadius: BorderRadius.circular(10),
                ),
                child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: widget.kidsList.data!.length,
                  itemBuilder: (context, index) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            if (widget.kidsList.data![index].gender == 'M')
                              Icon(
                                FontAwesomeIcons.person,
                                color: Color(0xFFCF152D),
                                size: 30,
                              ),
                            if (widget.kidsList.data![index].gender == 'F')
                              Icon(
                                FontAwesomeIcons.personDress,
                                color: Color(0xFFCF152D),
                                size: 30,
                              ),
                            SizedBox(width: 10),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    widget.kidsList.data![index].accName!
                                        .toUpperCase(),
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Text(
                                    selIdList[index],
                                    style: TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            // if (kidsList.data[index].title != null &&
                            //     kidsList.data[index].date != null)
                            SizedBox(width: 10),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: kPrimaryColor),
                              onPressed: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => BlocProvider(
                                      create: (context) => GetGraphCubit(),
                                      child: AsasChart(
                                        selId:
                                            widget.kidsList.data![index].idSid!,
                                      ),
                                    ),
                                  ),
                                );
                              },
                              child: Text('check_graph'.tr()),
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.grey,
                          // height: 3,
                          thickness: 1,
                        ),
                      ],
                    );
                  },
                ),
              ),
              SizedBox(height: 15),
            ],
          )
        ],
      ),
    );
  }
}
