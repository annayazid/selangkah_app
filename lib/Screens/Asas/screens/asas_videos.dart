import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';
import 'package:url_launcher/url_launcher.dart';

class VideosList extends StatelessWidget {
  const VideosList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String language =
        EasyLocalization.of(context)!.locale.languageCode == 'en' ? 'EN' : 'MY';
    double height = MediaQuery.of(context).size.height;

    context.read<GetVideoCubit>().getVideo(language);

    return AsasScaffold(
      appBarTitle: 'Asas',
      content: BlocBuilder<GetVideoCubit, GetVideoState>(
        builder: (context, state) {
          if (state is GetVideoLoaded) {
            return VideosListLoadedWidget(
              asasVideo: state.asasVideo,
            );
          } else if (state is GetVideoEmpty) {
            return Center(
              child: Text(
                'no_vid'.tr(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            );
          } else {
            return Padding(
              padding: EdgeInsets.only(top: height * 0.175),
              child: SpinKitFadingCircle(
                color: Color(0xFFCF152D),
                size: 25,
              ),
            );
          }
        },
      ),
    );
  }
}

class VideosListLoadedWidget extends StatefulWidget {
  final AsasVideo asasVideo;

  const VideosListLoadedWidget({Key? key, required this.asasVideo})
      : super(key: key);

  @override
  _VideosListLoadedWidgetState createState() => _VideosListLoadedWidgetState();
}

class _VideosListLoadedWidgetState extends State<VideosListLoadedWidget> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    return Stack(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Image.asset(
                    'assets/images/Asas/asas-banner-bg.png',
                    // fit: BoxFit.fitHeight,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: Container(
                          width: width * 0.75,
                          child: Image.asset(
                            'assets/images/Asas/asas_logo.png',
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 10),
            Container(
              height: 5,
              width: double.infinity,
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: widget.asasVideo.data!.length,
                itemBuilder: (BuildContext context, int index) {
                  // return Text('Test');
                  if (_currentIndex == index) {
                    return Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      width: 1 / widget.asasVideo.data!.length * width,
                      height: 5,
                      child: Container(
                        // color: Color(0xFFCF152D),

                        color: Color(0xFFCF152D),
                      ),
                    );
                  } else {
                    return Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      width: 1 / widget.asasVideo.data!.length * width,
                      height: 5,
                      child: Container(
                        // color: Color(0xFFFFFBA7),
                        color: Colors.white,
                        // color: Color(0xFFE5A0A9),
                      ),
                    );
                  }
                },
              ),
            ),
            SizedBox(height: 15),
            Column(
              children: [
                Container(
                  width: width * 0.95,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(
                      width: 3,
                      color: Color(0xFFCF152D),
                    ),
                  ),
                  child: Center(
                    child: Text(
                      widget.asasVideo.data![_currentIndex].title!
                          .toUpperCase(),
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        color: Color(0xFFCF152D),
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 15),
                Container(
                  width: width * 0.95,
                  padding: EdgeInsets.fromLTRB(25, 25, 25, 10),
                  decoration: BoxDecoration(
                    color: Color(0xFFCF152D),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.3),
                        offset: Offset(0, 0),
                        blurRadius: 5,
                        // spreadRadius: 3,
                      ),
                    ],
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount:
                        widget.asasVideo.data![_currentIndex].video!.length,
                    itemBuilder: (context, index) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            // height: height * 0.05,
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    '${widget.asasVideo.data![_currentIndex].video![index].displayNo}.    ${widget.asasVideo.data![_currentIndex].video![index].videoName}',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                // Spacer(),
                                SizedBox(width: 20),
                                GestureDetector(
                                  onTap: () {
                                    launchUrl(
                                      Uri.parse(widget
                                          .asasVideo
                                          .data![_currentIndex]
                                          .video![index]
                                          .url!),
                                      mode: LaunchMode.externalApplication,
                                    );
                                  },
                                  child: Container(
                                    width: 100,
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(20),
                                    ),
                                    child: Center(
                                      child: Text(
                                        'watch'.tr().toUpperCase(),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Color(0xFFCF152D),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 15),
                        ],
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: width * 0.85,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Visibility(
                        visible: (_currentIndex == 0) ? false : true,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Color(0xFFCF152D),
                            shape: BoxShape.circle,
                          ),
                          child: IconButton(
                            icon: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              setState(() {
                                _currentIndex--;
                              });
                            },
                          ),
                        ),
                      ),
                      Visibility(
                        visible:
                            _currentIndex == widget.asasVideo.data!.length - 1
                                ? false
                                : true,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Color(0xFFCF152D),
                            shape: BoxShape.circle,
                          ),
                          child: IconButton(
                            icon: Icon(
                              Icons.arrow_forward,
                              color: Colors.white,
                            ),
                            onPressed: () async {
                              if (_currentIndex < 3) {
                                setState(() {
                                  _currentIndex++;
                                });
                              }
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        )
      ],
    );
  }
}
