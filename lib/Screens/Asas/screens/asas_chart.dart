import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';
import 'package:selangkah_new/Screens/WebView/webview.dart';
import 'package:selangkah_new/utils/constants.dart';

class AsasChart extends StatefulWidget {
  final String selId;

  const AsasChart({Key? key, required this.selId}) : super(key: key);

  @override
  _AsasChartState createState() => _AsasChartState();
}

class _AsasChartState extends State<AsasChart> {
  @override
  void initState() {
    context.read<GetGraphCubit>().getGraph(widget.selId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AsasScaffold(
      appBarTitle: 'Asas',
      content: BlocConsumer<GetGraphCubit, GetGraphState>(
        listener: (context, state) {
          print('listener ${state.toString()}');

          if (state is GetGraphLoaded) {
            // print('here');
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => SelangkahWebview(
                  appBarTitle: 'ASAS',
                  url: state.url,
                ),
              ),
            );
          }
        },
        builder: (context, state) {
          return SpinKitFadingCircle(
            color: kPrimaryColor,
            size: 30,
          );
        },
      ),
    );
  }
}
