import 'package:flutter/material.dart';
import 'package:selangkah_new/utils/constants.dart';

class AsasScaffold extends StatelessWidget {
  final String appBarTitle;
  final Widget content;

  const AsasScaffold(
      {Key? key, required this.appBarTitle, required this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        // resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: kPrimaryColor,
          centerTitle: true,
          // automaticallyImplyLeading: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          title: Text(
            appBarTitle,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 15,
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: 'Roboto',
            ),
          ),
        ),
        body: Container(
          height: height - AppBar().preferredSize.height,
          width: width,
          child: content,
        ),
      ),
    );
  }
}
