import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';
import 'package:selangkah_new/utils/constants.dart';

class AsasScreen extends StatelessWidget {
  const AsasScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    context.read<GetProfileCubit>().getProfile();

    return AsasScaffold(
      appBarTitle: 'Asas',
      content: BlocBuilder<GetProfileCubit, GetProfileState>(
        builder: (context, state) {
          print(state.toString());
          if (state is GetProfileLoaded) {
            return AsasLoadedWidget(
              id: state.id,
              name: state.name,
              imgData: state.imgData,
            );
          } else {
            return Center(
              child: SpinKitFadingCircle(
                color: kPrimaryColor,
                size: 30,
              ),
            );
          }
        },
      ),
    );
  }
}

class AsasLoadedWidget extends StatelessWidget {
  final String id;
  final String name;
  final Uint8List? imgData;

  const AsasLoadedWidget(
      {Key? key, required this.id, required this.name, required this.imgData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: double.infinity,
            child: Stack(
              alignment: Alignment.center,
              children: [
                Image.asset(
                  'assets/images/Asas/asas-banner-bg.png',
                  // fit: BoxFit.fitHeight,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Container(
                        width: width * 0.75,
                        child: Image.asset(
                          'assets/images/Asas/asas_logo.png',
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(15),
            height: 475,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              border: Border.all(
                width: 3,
                color: Color(0xFFCF152D),
              ),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  offset: Offset(0, 0),
                  blurRadius: 5,
                  // spreadRadius: 3,
                ),
              ],
              borderRadius: BorderRadius.circular(20),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  children: [
                    CircleAvatar(
                      backgroundImage: () {
                        if (imgData == null) {
                          return AssetImage(
                              'assets/images/Asas/male_avatar.png');
                        } else {
                          return MemoryImage(imgData!);
                        }
                      }() as ImageProvider,
                      backgroundColor: Colors.blueGrey,
                      radius: height * 0.05,
                    ),
                    SizedBox(
                      width: width * 0.05,
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            id,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            name,
                            textAlign: TextAlign.center,
                          ),
                          // SizedBox(
                          //   height: 10,
                          // ),
                          // Container(
                          //   padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                          //   decoration: BoxDecoration(
                          //     borderRadius: BorderRadius.circular(10),
                          //     color: kPrimaryColor,
                          //   ),
                          //   child: Text(
                          //     'View ASAS Certificate',
                          //     textAlign: TextAlign.center,
                          //     style: TextStyle(
                          //       color: Colors.white,
                          //       fontWeight: FontWeight.bold,
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 15),
                Row(
                  children: [
                    AsasButton(
                      height: height,
                      width: width,
                      function: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => MultiBlocProvider(
                              providers: [
                                BlocProvider(
                                  create: (context) => GetAppointmentCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => SendSignatureCubit(),
                                ),
                              ],
                              child: AppointmentScreen(),
                            ),
                          ),
                        );
                      },
                      title: 'basket'.tr(),
                      img: 'assets/images/Asas/asas_bakul.png',
                      color: Color(0xFF61F1CF),
                    ),
                    Spacer(),
                    AsasButton(
                      height: height,
                      width: width,
                      function: () {
                        // Navigator.of(context).push(
                        //   MaterialPageRoute(
                        //     builder: (context) => BubbleMenu(),
                        //   ),
                        // );
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (builder) => BlocProvider(
                              create: (context) => GetKidsCubit(),
                              child: KidsList(),
                            ),
                          ),
                        );
                      },
                      title: 'kids'.tr(),
                      img: 'assets/images/Asas/asas_keluarga.png',
                      color: Color(0xFFFF7EEA),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Row(
                  children: [
                    AsasButton(
                      height: height,
                      width: width,
                      function: () {
                        // Navigator.of(context).push(
                        //   MaterialPageRoute(
                        //     builder: (context) => MultiBlocProvider(
                        //       providers: [
                        //         BlocProvider(
                        //           create: (context) => GetCorakCubit(),
                        //         ),
                        //         BlocProvider(
                        //           create: (context) => GetCorakIndexCubit(),
                        //         ),
                        //       ],
                        //       child: AsasTrend(),
                        //     ),
                        //   ),
                        // );
                        // Navigator.of(context).push(
                        //   MaterialPageRoute(
                        //     builder: (builder) => BlocProvider(
                        //       create: (context) => GetKidsCubit(),
                        //       child: KidsList(),
                        //     ),
                        //   ),
                        // );
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => BlocProvider(
                              create: (context) => GetKidsCubit(),
                              child: ChartKidList(),
                            ),
                          ),
                        );
                      },
                      title: 'trend'.tr(),
                      img: 'assets/images/Asas/asas_trend.png',
                      color: Color(0xFFFFC47E),
                    ),
                    Spacer(),
                    AsasButton(
                      height: height,
                      width: width,
                      function: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => BlocProvider(
                              create: (context) => GetVideoCubit(),
                              child: VideosList(),
                            ),
                          ),
                        );
                      },
                      title: 'video'.tr(),
                      img: 'assets/images/Asas/asas_video.png',
                      color: Color(0xFF7E83FF),
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(height: 15),
        ],
      ),
    );
  }
}

class AsasButton extends StatelessWidget {
  final double height;
  final double width;
  final Function function;
  final String title;
  final String img;
  final Color color;

  const AsasButton(
      {Key? key,
      required this.height,
      required this.width,
      required this.function,
      required this.title,
      required this.img,
      required this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        function();
      },
      child: Container(
        padding: EdgeInsets.all(10),
        height: height * 0.15,
        width: width * 0.37,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          children: [
            SizedBox(height: 7),
            Expanded(
              flex: 3,
              child: Align(
                alignment: Alignment.center,
                child: Image.asset(
                  img,
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Text(
                  title,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
