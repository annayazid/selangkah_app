import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/Asas/asas.dart';
import 'package:selangkah_new/utils/constants.dart';

class AsasBubble extends StatelessWidget {
  const AsasBubble({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    context.read<GetBubbleAsasCubit>().getBubbleAsas();
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    return AsasScaffold(
      appBarTitle: 'Asas',
      content: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Image.asset(
                    'assets/images/Asas/asas-banner-bg.png',
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: Container(
                          width: width * 0.75,
                          child: Image.asset(
                            'assets/images/Asas/asas_logo.png',
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            BlocConsumer<GetBubbleAsasCubit, GetBubbleAsasState>(
              listener: (context, state) {
                if (state is GetBubbleAsasError) {
                  Fluttertoast.showToast(
                    msg: 'There\'s something wrong, please try again later',
                    toastLength: Toast.LENGTH_LONG,
                  );
                  Navigator.of(context).pop();
                }
              },
              builder: (context, state) {
                print(state.toString());
                if (state is GetBubbleAsasNoAsas) {
                  return NoAsas();
                } else if (state is GetBubbleAsasLoaded) {
                  return GetBubbleAsasLoadedWidget(
                    bubbleMember: state.bubbleMember,
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.only(top: 50),
                    child: SpinKitFadingCircle(
                      color: kPrimaryColor,
                      size: 25,
                    ),
                  );
                }
              },
            )
          ],
        ),
      ),
    );
  }
}

class GetBubbleAsasLoadedWidget extends StatefulWidget {
  final BubbleMember bubbleMember;

  const GetBubbleAsasLoadedWidget({Key? key, required this.bubbleMember})
      : super(key: key);

  @override
  _GetBubbleAsasLoadedWidgetState createState() =>
      _GetBubbleAsasLoadedWidgetState();
}

class _GetBubbleAsasLoadedWidgetState extends State<GetBubbleAsasLoadedWidget> {
  List<Uint8List?> profilePic = [];

  @override
  void initState() {
    getProfilePic();
    super.initState();
  }

  void getProfilePic() {
    profilePic.clear();
    setState(() {
      for (var i = 0; i < widget.bubbleMember.data!.length; i++) {
        if (widget.bubbleMember.data![i].profilePic != null) {
          profilePic
              .add(base64.decode(widget.bubbleMember.data![i].profilePic!));
        } else {
          profilePic.add(null);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          width: double.infinity,
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                offset: Offset(0, 0),
                blurRadius: 5,
              ),
            ],
          ),
          child: Row(
            children: [
              Image.asset(
                'assets/images/Asas/Add_Member.png',
                width: width * 0.10,
              ),
              SizedBox(width: 20),
              Text('Add Member', style: TextStyle(fontSize: 17)),
            ],
          ),
        ),
        ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: widget.bubbleMember.data!.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.only(top: 0, bottom: 15, left: 15, right: 15),
              width: double.infinity,
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    offset: Offset(0, 0),
                    blurRadius: 5,
                  ),
                ],
              ),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CircleAvatar(
                    backgroundImage: () {
                      if (profilePic[index] == null)
                        return AssetImage(
                          'assets/images/Asas/male_avatar.png',
                        );
                      else {
                        return MemoryImage(profilePic[index]!);
                      }
                    }() as ImageProvider,
                    backgroundColor: Colors.blueGrey,
                    radius: 25,
                  ),
                  SizedBox(width: 15),
                  Container(
                    width: width * 0.45,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.bubbleMember.data![index].nickName!
                              .toUpperCase(),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 5),
                        Text(
                          widget.bubbleMember.data![index].userNo!,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: widget.bubbleMember.data![index].isAdmin == '0'
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Icon(
                                Icons.edit,
                                color: Colors.blue,
                              ),
                              SizedBox(width: 5),
                              Icon(
                                Icons.delete,
                                color: Colors.red,
                              ),
                            ],
                          )
                        : Container(),
                  ),
                ],
              ),
            );
          },
        ),
      ],
    );
  }
}

class NoAsas extends StatelessWidget {
  const NoAsas({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      margin: EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            offset: Offset(0, 0),
            blurRadius: 5,
            // spreadRadius: 3,
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 25),
          Text(
            'You have no ASAS bubble yet. Create one to use ASAS function',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          ),
          SizedBox(height: 25),
          Text(
            'Bubble Name',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 10),
          Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                color: Colors.black,
              ),
            ),
            child: Text('ASAS'),
          ),
          SizedBox(height: 25),
          Text(
            'Bubble Class',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 10),
          Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                color: Colors.black,
              ),
            ),
            child: Text('ASAS'),
          ),

          SizedBox(height: 25),

          //create a submit button
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(backgroundColor: kPrimaryColor),
              onPressed: () {
                context.read<GetBubbleAsasCubit>().createAsasBubble();
              },
              child: Text('Create'),
            ),
          ),
        ],
      ),
    );
  }
}
