import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:easy_localization/easy_localization.dart';
import 'dart:ui' as ui;

import 'package:selangkah_new/Screens/Asas/asas.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:syncfusion_flutter_signaturepad/signaturepad.dart';

class AppointmentScreen extends StatelessWidget {
  const AppointmentScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    context.read<GetAppointmentCubit>().getAppointment();
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;

    return AsasScaffold(
      appBarTitle: 'Asas',
      content: SingleChildScrollView(
        child: Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: double.infinity,
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Image.asset(
                        'assets/images/Asas/asas-banner-bg.png',
                        // fit: BoxFit.fitHeight,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: Container(
                              width: width * 0.75,
                              child: Image.asset(
                                'assets/images/Asas/asas_logo.png',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 15),
                BlocConsumer<GetAppointmentCubit, GetAppointmentState>(
                  listener: (context, state) {
                    if (state is GetAppointmentError) {
                      Navigator.of(context).pop();
                      Fluttertoast.showToast(
                        msg: 'Something wrong, please try again later',
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 16.0,
                      );
                    }
                  },
                  builder: (context, state) {
                    if (state is GetAppointmentSignature) {
                      return SignatureWidget(
                        ic: state.ic,
                        dependentSignature: state.dependentSignature,
                        name: state.name,
                      );
                    } else if (state is GetAppointmentLoaded) {
                      return AppointmentWidget(
                          asasAppointment: state.asasAppointment);
                    } else if (state is GetAppointmentError) {
                      return Center(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 50),
                          child: Text(
                            'no_appointment'.tr(),
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      );
                    } else {
                      return Column(
                        children: [
                          SizedBox(height: 50),
                          SpinKitFadingCircle(
                            size: 25,
                            color: kPrimaryColor,
                          ),
                        ],
                      );
                    }
                  },
                ),
                SizedBox(height: 15),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class AppointmentWidget extends StatelessWidget {
  final AsasAppointment asasAppointment;

  const AppointmentWidget({Key? key, required this.asasAppointment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    return Container(
      width: width * 0.95,
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            offset: Offset(0, 0),
            blurRadius: 5,
            // spreadRadius: 3,
          ),
        ],
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Icon(
            FontAwesomeIcons.fileLines,
            color: Color(0xFFD39400),
            size: 75,
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'next_coll'.tr(),
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: Text(
                  'location'.tr(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Text(
                  ':',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Text(
                  asasAppointment.data![0].siteName!,
                  textAlign: TextAlign.left,
                  style: TextStyle(),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: Text(
                  'date'.tr(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Text(
                  ':',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Text(
                  // formatDate(
                  //   asasAppointment.data[0].date,
                  //   [dd, ' ', MM, ' ', yyyy],
                  // ),
                  asasAppointment.data![0].date!,
                  textAlign: TextAlign.left,
                  style: TextStyle(),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: Text(
                  'time'.tr(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Text(
                  ':',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Text(
                  '${asasAppointment.data![0].timeStart} - ${asasAppointment.data![0].timeEnd}',
                  textAlign: TextAlign.left,
                  style: TextStyle(),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class SignatureWidget extends StatefulWidget {
  final DependentSignature dependentSignature;
  final String name;
  final String ic;

  const SignatureWidget(
      {Key? key,
      required this.dependentSignature,
      required this.name,
      required this.ic})
      : super(key: key);

  @override
  _SignatureWidgetState createState() => _SignatureWidgetState();
}

class _SignatureWidgetState extends State<SignatureWidget> {
  GlobalKey<SfSignaturePadState> _signaturePadKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      children: [
        Container(
          width: width * 0.95,
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                offset: Offset(0, 0),
                blurRadius: 5,
                // spreadRadius: 3,
              ),
            ],
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            children: [
              Text(
                'accept_form'.tr().toUpperCase(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              SizedBox(height: 10),
              Divider(
                color: Colors.grey,
                // height: 3,
                thickness: 1,
              ),
              SizedBox(height: 10),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  style: TextStyle(
                    height: 2,
                    color: Colors.black,
                    fontSize: 14,
                  ),
                  children: [
                    TextSpan(text: 'Saya '),
                    TextSpan(
                      text: '${widget.name}, ',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    TextSpan(text: 'No Kad Pengenalan '),
                    TextSpan(
                      text: '${widget.ic}, ',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    TextSpan(text: 'mengakui menerima pek ASAS pada '),
                    TextSpan(
                      text:
                          formatDate(DateTime.now(), [dd, '/', mm, '/', yyyy]),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    TextSpan(text: ' bagi penerima bernama :'),
                  ],
                ),
              ),
              SizedBox(height: 15),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  style: TextStyle(
                    height: 2,
                    color: Colors.black,
                    fontSize: 14,
                    fontStyle: FontStyle.italic,
                  ),
                  children: [
                    TextSpan(text: 'I '),
                    TextSpan(
                      text: '${widget.name} , ',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    TextSpan(text: 'Identification No '),
                    TextSpan(
                      text: '${widget.ic}, ',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    TextSpan(
                        text:
                            ' hereby confirm that I have collected an ASAS pack on '),
                    TextSpan(
                      text:
                          formatDate(DateTime.now(), [dd, '/', mm, '/', yyyy]),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    TextSpan(text: ' on behalf of the recipient named:'),
                  ],
                ),
              ),
              SizedBox(height: 30),
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: widget.dependentSignature.data!.length,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: [
                      Container(
                        height: height * 0.05,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text(
                              '${widget.dependentSignature.data![index].accName}',
                              style: TextStyle(
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Spacer(),
                            Text(
                              '${widget.dependentSignature.data![index].icNo}',
                            ),
                            SizedBox(width: 20),
                          ],
                        ),
                      ),
                      Divider(
                        color: Colors.grey,
                        // height: 3,
                        thickness: 1,
                      ),
                    ],
                  );
                },
              ),
              SizedBox(height: 30),
              Text(
                'sign'.tr(),
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                ),
              ),
              SizedBox(height: 15),
              Container(
                height: 270,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(
                    color: Colors.grey,
                  ),
                ),
                child: Stack(
                  children: [
                    SfSignaturePad(
                      key: _signaturePadKey,
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: TextButton(
                        onPressed: () async {
                          _signaturePadKey.currentState!.clear();
                        },
                        child: Text(
                          'clear'.tr(),
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 15),
              Text(
                'sign_in_box'.tr(),
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 10,
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 15),
        Container(
          width: width * 0.9,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Color(0xFFCF152D),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            onPressed: () async {
              ui.Image image = await _signaturePadKey.currentState!.toImage();

              context.read<SendSignatureCubit>().sendSignature(image);
            },
            child: BlocConsumer<SendSignatureCubit, SendSignatureState>(
              listener: (context, state) {
                if (state is SendSignatureSent) {
                  context.read<GetAppointmentCubit>().getAppointment();
                }
              },
              builder: (context, state) {
                if (state is SendSignatureLoading) {
                  return SpinKitFadingCircle(
                    color: Colors.white,
                    size: 14,
                  );
                } else {
                  return Text(
                    'submit'.tr(),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  );
                }
              },
            ),
          ),
        ),
      ],
    );
  }
}
