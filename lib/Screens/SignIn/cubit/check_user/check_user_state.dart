part of 'check_user_cubit.dart';

@immutable
abstract class CheckUserState {
  const CheckUserState();
}

class CheckUserInitial extends CheckUserState {
  const CheckUserInitial();
}

class CheckUserLoading extends CheckUserState {
  const CheckUserLoading();
}

class CheckUserNotRegister extends CheckUserState {
  const CheckUserNotRegister();
}

class CheckUserFailedLogin extends CheckUserState {
  const CheckUserFailedLogin();
}

class CheckUserLoaded extends CheckUserState {
  final UserProfile? userProfile;

  CheckUserLoaded(this.userProfile);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CheckUserLoaded && other.userProfile == userProfile;
  }

  @override
  int get hashCode => userProfile.hashCode;
}
