import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/SignIn/sign_in.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

part 'check_user_state.dart';

class CheckUserCubit extends Cubit<CheckUserState> {
  CheckUserCubit() : super(CheckUserInitial());

  Future<void> checkUserLogin(String phoneNum, String password) async {
    emit(CheckUserLoading());

    bool userReg = await SignInRepositories.checkUserReg(phoneNum);

    print(userReg);

    if (userReg) {
      // bool userLogin =
      //     await SignInRepositories.checkUserLogin(phoneNum, password);

      String id = await SignInRepositories.checkUserLogin(phoneNum, password);

      SecureStorage secureStorage = new SecureStorage();
      secureStorage.writeSecureData('userId', id);

      print('id is $id');

      if (id != '') {
        UserProfile? userProfile = await AppSplashRepositories.getUserProfile();
        await AppSplashRepositories.updateUserToken();
        emit(CheckUserLoaded(userProfile));
        // print(userProfile);
      } else {
        Fluttertoast.showToast(msg: "signinerror".tr());
        emit(CheckUserFailedLogin());
      }
    } else {
      Fluttertoast.showToast(msg: "not_register".tr());
      emit(CheckUserNotRegister());
    }
  }
}
