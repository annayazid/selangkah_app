import 'dart:convert';

import 'package:selangkah_new/utils/endpoint.dart';
import 'package:http/http.dart' as http;
import 'package:selangkah_new/utils/global.dart';

class SignInRepositories {
  static Future<bool> checkUserReg(String phoneNum) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'phonenumber': phoneNum,
    };

    final response = await http.post(
      Uri.parse('$API_URL/check_user_reg'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    var data = jsonDecode(response.body);

    if (data['Data'] != false) {
      return true;
    } else {
      return false;
      // Fluttertoast.showToast(msg: "not_register".tr());
    }
  }

  static Future<String> checkUserLogin(String phoneNum, String password) async {
    String deviceId = await GlobalFunction.getDeviceId();
    final Map<String, String> map = {
      'token': TOKEN,
      'phonenumber': phoneNum,
      'pwd': password,
      'device_id': deviceId,
    };

    final response = await http.post(
      Uri.parse('$API_URL/check_user_login'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    var data = jsonDecode(response.body);

    if (data['Data'] != false) {
      String id = data['Data'][0];
      return id;
    } else {
      return '';
    }
  }
}
