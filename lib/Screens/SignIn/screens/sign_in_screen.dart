import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nb_utils/nb_utils.dart';

import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/ForgotPassword/forgot_pass.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/PDFInAppScreen/pdf_in_app_screen.dart';
import 'package:selangkah_new/Screens/SignIn/sign_in.dart';
import 'package:selangkah_new/Screens/SignUp/sign_up.dart';
import 'package:selangkah_new/Wallet/Services/navigation_service.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({super.key});

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  bool passwordVisibleOff = true;
  final TextEditingController _phoneNumController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    // GlobalFunction.screenJourney('1');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    context.read<PrivacyTermCubit>().getPrivacyTerm(
        EasyLocalization.of(context)!.locale.languageCode == 'en'
            ? 'EN'
            : 'MY');

    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: height,
            child: Column(
              children: [
                //top banner
                Container(
                  height: (MediaQuery.of(context).size.height) / 3.5,
                  child: Stack(
                    children: <Widget>[
                      Image.asset(
                        'assets/images/SignIn/signin_background.png',
                        fit: BoxFit.fill,
                        width: width,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 16),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'welcomeback'.tr(),
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontSize: 40,
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),

                //icon selangkah top right
                Container(
                  alignment: Alignment.topRight,
                  margin: EdgeInsets.only(right: 45),
                  transform: Matrix4.translationValues(0.0, -40.0, 0.0),
                  child: Image.asset('assets/images/SignIn/signin_icon.png',
                      height: 70, width: 70),
                ),

                //login form
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                  child: TextFormField(
                    controller: _phoneNumController,
                    style: TextStyle(fontSize: 18),
                    obscureText: false,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(26, 18, 4, 18),
                      prefixText: "+6 ",
                      hintText: 'phonenumber'.tr(),
                      filled: true,
                      fillColor: Color(0xFFF5F4F4),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide:
                            BorderSide(color: Color(0xFFF5F4F4), width: 0.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide:
                            BorderSide(color: Color(0xFFF5F4F4), width: 0.0),
                      ),
                    ),
                    keyboardType: TextInputType.phone,
                  ),
                ),
                SizedBox(height: 15),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                  child: TextFormField(
                    controller: _passwordController,
                    style: TextStyle(fontSize: 18),
                    obscureText: passwordVisibleOff,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(50, 18, 0, 18),
                      // prefixText: "   ",
                      hintText: 'password'.tr(),
                      filled: true,
                      fillColor: Color(0xFFF5F4F4),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide:
                            BorderSide(color: Color(0xFFF5F4F4), width: 0.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40),
                        borderSide:
                            BorderSide(color: Color(0xFFF5F4F4), width: 0.0),
                      ),
                      suffixIcon: IconButton(
                        icon: Icon(
                          // Based on passwordVisible state choose the icon
                          passwordVisibleOff
                              ? Icons.visibility_off
                              : Icons.visibility,
                          color: Theme.of(context).primaryColorDark,
                        ),
                        onPressed: () {
                          // Update the state i.e. toogle the state of passwordVisible variable
                          setState(() {
                            passwordVisibleOff = !passwordVisibleOff;
                          });
                        },
                      ),
                    ),
                    keyboardType: TextInputType.visiblePassword,
                  ),
                ),
                SizedBox(height: 25),

                //signin button
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      backgroundColor: Color(0xFFffc980),
                    ),
                    onPressed: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      print(_phoneNumController.text);
                      if (_phoneNumController.text == '' ||
                          _passwordController.text == '') {
                        Fluttertoast.showToast(
                            msg:
                                "Please insert your phone number and password");
                      } else {
                        context.read<CheckUserCubit>().checkUserLogin(
                            _phoneNumController.text, _passwordController.text);
                      }
                      // GlobalFunction.getDeviceIdentifier();
                    },
                    child: BlocConsumer<CheckUserCubit, CheckUserState>(
                      listener: (context, state) async {
                        if (state is CheckUserLoaded) {
                          await showDialog(
                            barrierDismissible: false,
                            context: NavigationService
                                .navigatorKey.currentState!.overlay!.context,
                            builder: (context) {
                              return DisclosurePopUp();
                            },
                          );
                          String language = EasyLocalization.of(context)!
                                      .locale
                                      .languageCode ==
                                  'en'
                              ? 'EN'
                              : 'MY';

                          GlobalVariables.language = language;
                          // Fluttertoast.showToast(msg: "User Logged In");
                          BlocProvider(
                            create: (context) => GetHomepageDataCubit(),
                            child: HomePageScreen(),
                          ).launch(
                            context,
                            isNewTask: true,
                          );
                          // Navigator.of(context).push(
                          //   MaterialPageRoute(
                          //     builder: (context) => HomePageScreen(),
                          //   ),
                          // );
                        }
                      },
                      builder: (context, state) {
                        print(state.toString());
                        if (state is CheckUserLoading) {
                          return Padding(
                            padding: EdgeInsets.all(18.0),
                            child: SpinKitFadingCircle(
                              color: Colors.white,
                              size: 18,
                            ),
                          );
                        } else {
                          return Container(
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.all(18.0),
                                child: Text(
                                  'signin'.tr(),
                                  style: TextStyle(fontSize: 18),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          );
                        }
                      },
                    ),
                  ),
                ),
                SizedBox(height: 25),

                //forgot pass text
                GestureDetector(
                  onTap: () async {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => BlocProvider(
                          create: (context) => ForgotAccCubit(),
                          child: ForgotPasswordPage(),
                        ),
                      ),
                    );
                  },
                  child: Text(
                    'forgotPassword'.tr() + '?',
                    style: TextStyle(
                        // fontSize: 16,
                        ),
                  ),
                ),
                SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'dontHaveAccount'.tr(),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 4),
                      child: GestureDetector(
                          child: Text('signUp'.tr(),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  decoration: TextDecoration.underline,
                                  color: Color(0xFFfc4a1a))),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return BlocProvider(
                                    create: (context) => UserRegCubit(),
                                    child: SignUpPage(),
                                  );
                                },
                                // settings: RouteSettings(name: 'Signup'),
                              ),
                            );
                          }),
                    ),
                  ],
                ),
                Spacer(),
                //privacy statement/general term of use
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.only(left: 16, right: 16),
                  height: 75,
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: '${'accepttnc_signin'.tr()} ',
                          style: TextStyle(color: Colors.grey),
                        ),
                        TextSpan(
                          text: '${'privacy'.tr()} ',
                          recognizer: TapGestureRecognizer()
                            ..onTap = () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => PDFInAppPageScreen(
                                      // url: GlobalVariables.privacy,
                                      url: GlobalVariables.privacy,
                                      title: 'privacy'.tr(),
                                    ),
                                  ),
                                ),
                          // launch(GlobalVariables.privacy),
                          style: TextStyle(
                            color: Color(0xFFfc4a1a),
                            decoration: TextDecoration.underline,
                          ),
                        ),
                        TextSpan(
                          text: '${'and'.tr()} ',
                          style: TextStyle(color: Colors.grey),
                        ),
                        TextSpan(
                          text: '${'termsofuse'.tr()}.',
                          recognizer: TapGestureRecognizer()
                            ..onTap = () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => PDFInAppPageScreen(
                                      url: GlobalVariables.termsofuse,
                                      title: 'termsofuse'.tr(),
                                    ),
                                  ),
                                ),
                          // launch(GlobalVariables.termsofuse),
                          style: TextStyle(
                            color: Color(0xFFfc4a1a),
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class DisclosurePopUp extends StatefulWidget {
  const DisclosurePopUp({
    Key? key,
  }) : super(key: key);

  @override
  _DisclosurePopUpState createState() => _DisclosurePopUpState();
}

class _DisclosurePopUpState extends State<DisclosurePopUp> {
  var controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return AlertDialog(
      backgroundColor: Colors.grey[200],
      content: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'note_user_quarantine'.tr(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
            SizedBox(height: 5),
            Text.rich(
              TextSpan(
                children: [
                  TextSpan(
                    text: '${'agree_consent'.tr()} ',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: 'privacy'.tr(),
                    style: TextStyle(
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                      fontWeight: FontWeight.bold,
                    ),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () => Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PDFInAppPageScreen(
                                url: GlobalVariables.privacy,
                                title: 'privacy'.tr(),
                              ),
                            ),
                          ),
                    // launch(GlobalVariables.privacy),
                  ),
                  TextSpan(
                    text: ' ${'to'.tr()}: ',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10),
            Container(
              height: height * 0.5,
              child: Scrollbar(
                controller: controller,
                thumbVisibility: true,
                child: SingleChildScrollView(
                  controller: controller,
                  child: Column(
                    children: [
                      Text('consent_1'.tr()),
                      SizedBox(height: 5),
                      Text('consent_2'.tr()),
                      SizedBox(height: 5),
                      Text('consent_3'.tr()),
                      SizedBox(height: 5),
                      Text('consent_5'.tr()),
                      SizedBox(height: 5),
                      Text('consent_6'.tr()),
                      SizedBox(height: 5),
                      Text('consent_7'.tr()),
                      SizedBox(height: 5),
                      Text('consent_8'.tr()),
                      SizedBox(height: 5),
                      Text('consent_9'.tr()),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TextButton(
                  child: Text(
                    "deny".tr(),
                    style: TextStyle(color: Colors.blue),
                  ),
                  onPressed: () async {
                    SecureStorage secureStorage = SecureStorage();
                    await secureStorage.deleteAllSecureData();
                    MultiBlocProvider(
                      providers: [
                        BlocProvider(
                          create: (context) => PrivacyTermCubit(),
                        ),
                        BlocProvider(
                          create: (context) => CheckUserCubit(),
                        ),
                      ],
                      child: SignInScreen(),
                    ).launch(context, isNewTask: true);
                    Fluttertoast.showToast(
                      msg: 'user_deny_message'.tr(),
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                    );
                  },
                ),
                TextButton(
                  child: Text(
                    "agree".tr(),
                    style: TextStyle(color: Colors.blue),
                  ),
                  onPressed: () async {
                    await SecureStorage()
                        .writeSecureData('privacyMain', 'true');
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
