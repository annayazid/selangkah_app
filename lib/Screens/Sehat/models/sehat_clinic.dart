// To parse this JSON data, do
//
//     final sehatClinic = sehatClinicFromJson(jsonString);

import 'dart:convert';

SehatClinic sehatClinicFromJson(String str) =>
    SehatClinic.fromJson(json.decode(str));

String sehatClinicToJson(SehatClinic data) => json.encode(data.toJson());

class SehatClinic {
  SehatClinic({
    this.code,
    this.data,
  });

  int? code;
  List<SehatClinicData>? data;

  factory SehatClinic.fromJson(Map<String, dynamic> json) => SehatClinic(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<SehatClinicData>.from(
                json["Data"].map((x) => SehatClinicData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class SehatClinicData {
  SehatClinicData({
    this.idProvider,
    this.ppvName,
    this.idProgramSetting,
  });

  String? idProvider;
  String? ppvName;
  String? idProgramSetting;

  factory SehatClinicData.fromJson(Map<String, dynamic> json) =>
      SehatClinicData(
        idProvider: json["id_provider"] == null ? null : json["id_provider"],
        ppvName: json["ppv_name"] == null ? null : json["ppv_name"],
        idProgramSetting: json["id_program_setting"] == null
            ? null
            : json["id_program_setting"],
      );

  Map<String, dynamic> toJson() => {
        "id_provider": idProvider == null ? null : idProvider,
        "ppv_name": ppvName == null ? null : ppvName,
        "id_program_setting":
            idProgramSetting == null ? null : idProgramSetting,
      };
}
