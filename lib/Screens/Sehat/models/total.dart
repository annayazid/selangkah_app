// To parse this JSON data, do
//
//     final total = totalFromJson(jsonString);

import 'dart:convert';

Total totalFromJson(String str) => Total.fromJson(json.decode(str));

String totalToJson(Total data) => json.encode(data.toJson());

class Total {
  Total({
    this.code,
    this.data,
  });

  int? code;
  int? data;

  factory Total.fromJson(Map<String, dynamic> json) => Total(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null ? null : json["Data"],
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null ? null : data,
      };
}
