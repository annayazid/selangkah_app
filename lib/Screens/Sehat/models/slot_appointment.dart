// To parse this JSON data, do
//
//     final slotAppointment = slotAppointmentFromJson(jsonString);

import 'dart:convert';

SlotAppointment slotAppointmentFromJson(String str) =>
    SlotAppointment.fromJson(json.decode(str));

String slotAppointmentToJson(SlotAppointment data) =>
    json.encode(data.toJson());

class SlotAppointment {
  SlotAppointment({
    this.code,
    this.data,
    this.slot,
  });

  int? code;
  bool? data;
  String? slot;

  factory SlotAppointment.fromJson(Map<String, dynamic> json) =>
      SlotAppointment(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null ? null : json["Data"],
        slot: json["Slot"] == null ? null : json["Slot"],
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data,
        "Slot": slot,
      };
}
