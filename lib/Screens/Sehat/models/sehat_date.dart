// To parse this JSON data, do
//
//     final sehatDate = sehatDateFromJson(jsonString);

import 'dart:convert';

SehatDate sehatDateFromJson(String str) => SehatDate.fromJson(json.decode(str));

String sehatDateToJson(SehatDate data) => json.encode(data.toJson());

class SehatDate {
  SehatDate({
    this.code,
    this.data,
  });

  int? code;
  List<SehatDateData>? data;

  factory SehatDate.fromJson(Map<String, dynamic> json) => SehatDate(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<SehatDateData>.from(
                json["Data"].map((x) => SehatDateData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class SehatDateData {
  SehatDateData({
    this.slotDate,
    this.timeStart,
    this.timeEnd,
  });

  DateTime? slotDate;
  String? timeStart;
  String? timeEnd;

  factory SehatDateData.fromJson(Map<String, dynamic> json) => SehatDateData(
        slotDate: json["slot_date"] == null
            ? null
            : DateTime.parse(json["slot_date"]),
        timeStart: json["time_start"] == null ? null : json["time_start"],
        timeEnd: json["time_end"] == null ? null : json["time_end"],
      );

  Map<String, dynamic> toJson() => {
        "slot_date": slotDate == null
            ? null
            : "${slotDate!.year.toString().padLeft(4, '0')}-${slotDate!.month.toString().padLeft(2, '0')}-${slotDate!.day.toString().padLeft(2, '0')}",
        "time_start": timeStart == null ? null : timeStart,
        "time_end": timeEnd == null ? null : timeEnd,
      };
}
