// To parse this JSON data, do
//
//     final sehatRedirect = sehatRedirectFromJson(jsonString);

import 'dart:convert';

SehatRedirect sehatRedirectFromJson(String str) =>
    SehatRedirect.fromJson(json.decode(str));

String sehatRedirectToJson(SehatRedirect data) => json.encode(data.toJson());

class SehatRedirect {
  SehatRedirect({
    this.program,
    this.idModule,
    this.moduleName,
    this.idPathway,
    this.idActivity,
    this.activityName,
  });

  String? program;
  String? idModule;
  String? moduleName;
  String? idPathway;
  String? idActivity;
  String? activityName;

  factory SehatRedirect.fromJson(Map<String, dynamic> json) => SehatRedirect(
        program: json["program"] == null ? null : json["program"],
        idModule: json["id_module"] == null ? null : json["id_module"],
        moduleName: json["module_name"] == null ? null : json["module_name"],
        idPathway: json["id_pathway"] == null ? null : json["id_pathway"],
        idActivity: json["id_activity"] == null ? null : json["id_activity"],
        activityName:
            json["activity_name"] == null ? null : json["activity_name"],
      );

  Map<String, dynamic> toJson() => {
        "program": program == null ? null : program,
        "id_module": idModule == null ? null : idModule,
        "module_name": moduleName == null ? null : moduleName,
        "id_pathway": idPathway == null ? null : idPathway,
        "id_activity": idActivity == null ? null : idActivity,
        "activity_name": activityName == null ? null : activityName,
      };
}
