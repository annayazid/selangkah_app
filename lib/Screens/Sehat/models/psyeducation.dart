class PsyEducations {
  String? id;
  String? name;
  String? displayNo;
  String? url;
  String? status;

  PsyEducations({this.id, this.displayNo, this.name, this.url, this.status});

  factory PsyEducations.fromJson(Map<String, dynamic> json) => PsyEducations(
        id: json['id'] == null ? null : json['id'],
        name: json['video_name'] == null ? null : json['video_name'],
        displayNo: json['display_no'] == null ? null : json['display_no'],
        url: json['url'] == null ? null : json['url'],
        status: json['status'] == null ? "0" : json['status'],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "video_name": name == null ? null : name,
        "display_no": displayNo == null ? null : displayNo,
        "url": url == null ? null : url,
        "status": status == null ? null : status,
      };
}
