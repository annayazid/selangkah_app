import 'package:flutter/material.dart';

class ScorePanel {
  String? id;
  String? title;
  List<ScoreTile>? score;
  ScorePanel({
    this.id,
    this.title,
    this.score,
  });
}

class ScoreTile {
  String? name;
  String? score;
  Color? color;
  String? level;
  ScoreTile({
    this.name,
    this.score,
    this.color,
    this.level,
  });
}
