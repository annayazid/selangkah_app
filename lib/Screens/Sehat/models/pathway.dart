// To parse this JSON data, do
//
//     final pathway = pathwayFromJson(jsonString);

import 'dart:convert';

Pathway pathwayFromJson(String str) => Pathway.fromJson(json.decode(str));

String pathwayToJson(Pathway data) => json.encode(data.toJson());

class Pathway {
  Pathway({
    this.code,
    this.data,
  });

  int? code;
  List<PathwayData>? data;

  factory Pathway.fromJson(Map<String, dynamic> json) => Pathway(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<PathwayData>.from(
                json["Data"].map((x) => PathwayData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class PathwayData {
  PathwayData({
    this.id,
    this.pathwayName,
    this.displayNo,
  });

  String? id;
  String? pathwayName;
  String? displayNo;

  factory PathwayData.fromJson(Map<String, dynamic> json) => PathwayData(
        id: json["id"] == null ? null : json["id"],
        pathwayName: json["pathway_name"] == null ? null : json["pathway_name"],
        displayNo: json["display_no"] == null ? null : json["display_no"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "pathway_name": pathwayName == null ? null : pathwayName,
        "display_no": displayNo == null ? null : displayNo,
      };
}
