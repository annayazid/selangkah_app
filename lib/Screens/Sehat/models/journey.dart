// To parse this JSON data, do
//
//     final journey = journeyFromJson(jsonString);

import 'dart:convert';

Journey journeyFromJson(String str) => Journey.fromJson(json.decode(str));

String journeyToJson(Journey data) => json.encode(data.toJson());

class Journey {
  Journey({
    this.code,
    this.data,
  });

  int? code;
  List<JourneyData>? data;

  factory Journey.fromJson(Map<String, dynamic> json) => Journey(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<JourneyData>.from(
                json["Data"].map((x) => JourneyData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class JourneyData {
  JourneyData({
    this.idMentalQuestionCat,
    this.status,
    this.score,
  });

  String? idMentalQuestionCat;
  String? status;
  String? score;

  factory JourneyData.fromJson(Map<String, dynamic> json) => JourneyData(
        idMentalQuestionCat: json["id_mental_question_cat"] == null
            ? null
            : json["id_mental_question_cat"],
        status: json["status"] == null ? null : json["status"],
        score: json["score"] == null ? null : json["score"],
      );

  Map<String, dynamic> toJson() => {
        "id_mental_question_cat":
            idMentalQuestionCat == null ? null : idMentalQuestionCat,
        "status": status == null ? null : status,
        "score": score == null ? null : score,
      };
}
