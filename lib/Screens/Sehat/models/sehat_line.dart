// To parse this JSON data, do
//
//     final sehatLine = sehatLineFromJson(jsonString);

import 'dart:convert';

SehatLine sehatLineFromJson(String str) => SehatLine.fromJson(json.decode(str));

String sehatLineToJson(SehatLine data) => json.encode(data.toJson());

class SehatLine {
  SehatLine({
    this.code,
    this.data,
  });

  int? code;
  SehatLineData? data;

  factory SehatLine.fromJson(Map<String, dynamic> json) => SehatLine(
        code: json["Code"] == null ? null : json["Code"],
        data:
            json["Data"] == null ? null : SehatLineData.fromJson(json["Data"]),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null ? null : data!.toJson(),
      };
}

class SehatLineData {
  SehatLineData({
    this.tel1,
    this.tel2,
  });

  String? tel1;
  String? tel2;

  factory SehatLineData.fromJson(Map<String, dynamic> json) => SehatLineData(
        tel1: json["tel_1"] == null ? null : json["tel_1"],
        tel2: json["tel_2"] == null ? null : json["tel_2"],
      );

  Map<String, dynamic> toJson() => {
        "tel_1": tel1 == null ? null : tel1,
        "tel_2": tel2 == null ? null : tel2,
      };
}
