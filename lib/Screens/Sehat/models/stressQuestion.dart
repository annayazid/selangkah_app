class StressQuestion {
  String? id;
  String? questionText;
  String? displayNo;

  StressQuestion({this.id, this.questionText, this.displayNo});

  factory StressQuestion.fromJson(Map<String, dynamic> json) => StressQuestion(
        id: json['id'] == null ? null : json['id'],
        questionText:
            json['question_text'] == null ? null : json['question_text'],
        displayNo: json['display_no'] == null ? null : json['display_no'],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "question_text": questionText == null ? null : questionText,
        "display_no": displayNo == null ? null : displayNo,
      };
}
