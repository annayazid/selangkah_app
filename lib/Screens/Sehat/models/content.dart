// To parse this JSON data, do
//
//     final content = contentFromJson(jsonString);

import 'dart:convert';

Content contentFromJson(String str) => Content.fromJson(json.decode(str));

String contentToJson(Content data) => json.encode(data.toJson());

class Content {
  Content({
    this.code,
    this.data,
  });

  int? code;
  List<ContentData>? data;

  factory Content.fromJson(Map<String, dynamic> json) => Content(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<ContentData>.from(
                json["Data"].map((x) => ContentData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class ContentData {
  ContentData({
    this.page,
    this.card,
  });

  int? page;
  List<CardContent>? card;

  factory ContentData.fromJson(Map<String, dynamic> json) => ContentData(
        page: json["page"] == null ? null : json["page"],
        card: json["card"] == null
            ? null
            : List<CardContent>.from(
                json["card"].map((x) => CardContent.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "page": page == null ? null : page,
        "card": card == null
            ? null
            : List<dynamic>.from(card!.map((x) => x.toJson())),
      };
}

class CardContent {
  CardContent({
    this.idMentalContent,
    this.contentType,
    this.data,
  });

  String? idMentalContent;
  String? contentType;
  CardData? data;

  factory CardContent.fromJson(Map<String, dynamic> json) => CardContent(
        idMentalContent: json["id_mental_content"] == null
            ? null
            : json["id_mental_content"],
        contentType: json["content_type"] == null ? null : json["content_type"],
        data: json["data"] == null ? null : CardData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "id_mental_content": idMentalContent == null ? null : idMentalContent,
        "content_type": contentType == null ? null : contentType,
        "data": data == null ? null : data!.toJson(),
      };
}

class CardData {
  CardData({
    this.id,
    this.questionText,
    this.displayNo,
    this.answerType,
    this.choice,
    this.url,
    this.articleText,
    this.image,
    this.indicator,
  });

  String? id;
  String? questionText;
  String? displayNo;
  String? answerType;
  List<Choice>? choice;
  String? url;
  String? articleText;
  String? image;
  dynamic indicator;

  factory CardData.fromJson(Map<String, dynamic> json) => CardData(
        id: json["id"] == null ? null : json["id"],
        questionText:
            json["question_text"] == null ? null : json["question_text"],
        displayNo: json["display_no"] == null ? null : json["display_no"],
        answerType: json["answer_type"] == null ? null : json["answer_type"],
        choice: json["choice"] == null
            ? null
            : List<Choice>.from(json["choice"].map((x) => Choice.fromJson(x))),
        url: json["url"] == null ? null : json["url"],
        articleText: json["article_text"] == null ? null : json["article_text"],
        image: json["image"] == null ? null : json["image"],
        indicator: json["indicator"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "question_text": questionText == null ? null : questionText,
        "display_no": displayNo == null ? null : displayNo,
        "answer_type": answerType == null ? null : answerType,
        "choice": choice == null
            ? null
            : List<dynamic>.from(choice!.map((x) => x.toJson())),
        "url": url == null ? null : url,
        "article_text": articleText == null ? null : articleText,
        "image": image == null ? null : image,
        "indicator": indicator,
      };
}

class Choice {
  Choice({
    this.answerId,
    this.answerValue,
    this.correctFlag,
  });

  String? answerId;
  String? answerValue;
  String? correctFlag;

  factory Choice.fromJson(Map<String, dynamic> json) => Choice(
        answerId: json["answer_id"] == null ? null : json["answer_id"],
        answerValue: json["answer_value"] == null ? null : json["answer_value"],
        correctFlag: json["correct_flag"] == null ? null : json["correct_flag"],
      );

  Map<String, dynamic> toJson() => {
        "answer_id": answerId == null ? null : answerId,
        "answer_value": answerValue == null ? null : answerValue,
        "correct_flag": correctFlag == null ? null : correctFlag,
      };
}
