// To parse this JSON data, do
//
//     final category = categoryFromJson(jsonString);

import 'dart:convert';

Category categoryFromJson(String str) => Category.fromJson(json.decode(str));

String categoryToJson(Category data) => json.encode(data.toJson());

class Category {
  Category({
    this.code,
    this.data,
  });

  int? code;
  List<CategoryData>? data;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<CategoryData>.from(
                json["Data"].map((x) => CategoryData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class CategoryData {
  CategoryData({
    this.id,
    this.categoryName,
    this.displayNo,
    this.description,
    this.haveChild,
  });

  String? id;
  String? categoryName;
  String? displayNo;
  String? description;
  int? haveChild;

  factory CategoryData.fromJson(Map<String, dynamic> json) => CategoryData(
        id: json["id"] == null ? null : json["id"],
        categoryName:
            json["category_name"] == null ? null : json["category_name"],
        displayNo: json["display_no"] == null ? null : json["display_no"],
        description: json["description"] == null ? '' : json["description"],
        haveChild: json["have_child"] == null ? null : json["have_child"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "category_name": categoryName == null ? null : categoryName,
        "display_no": displayNo == null ? null : displayNo,
        "description": description == null ? null : description,
        "have_child": haveChild == null ? null : haveChild,
      };
}
