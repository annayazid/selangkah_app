// To parse this JSON data, do
//
//     final activityComplete = activityCompleteFromJson(jsonString);

import 'dart:convert';

ActivityComplete activityCompleteFromJson(String str) =>
    ActivityComplete.fromJson(json.decode(str));

String activityCompleteToJson(ActivityComplete data) =>
    json.encode(data.toJson());

class ActivityComplete {
  ActivityComplete({
    this.code,
    this.data,
  });

  int? code;
  List<Datum>? data;

  factory ActivityComplete.fromJson(Map<String, dynamic> json) =>
      ActivityComplete(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<Datum>.from(json["Data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.idMentalActivity,
    this.status,
    this.completedTs,
  });

  String? idMentalActivity;
  String? status;
  String? completedTs;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        idMentalActivity: json["id_mental_activity"] == null
            ? null
            : json["id_mental_activity"],
        status: json["status"] == null ? null : json["status"],
        completedTs: json["completed_ts"] == null ? null : json["completed_ts"],
      );

  Map<String, dynamic> toJson() => {
        "id_mental_activity":
            idMentalActivity == null ? null : idMentalActivity,
        "status": status == null ? null : status,
        "completed_ts": completedTs == null ? null : completedTs,
      };
}
