// To parse this JSON data, do
//
//     final appointment = appointmentFromJson(jsonString);

import 'dart:convert';

Appointment appointmentFromJson(String str) =>
    Appointment.fromJson(json.decode(str));

String appointmentToJson(Appointment data) => json.encode(data.toJson());

class Appointment {
  Appointment({
    this.code,
    this.data,
  });

  int? code;
  AppointmentData? data;

  factory Appointment.fromJson(Map<String, dynamic> json) => Appointment(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : AppointmentData.fromJson(json["Data"]),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null ? null : data!.toJson(),
      };
}

class AppointmentData {
  AppointmentData({
    this.idProgramAppointment,
    this.idProvider,
    this.testId,
    this.testName,
    this.location,
    this.timeStart,
    this.timeEnd,
    this.address,
    this.date,
    this.referral,
  });

  String? idProgramAppointment;
  String? idProvider;
  String? testId;
  String? testName;
  String? location;
  String? timeStart;
  String? timeEnd;
  String? address;
  DateTime? date;
  String? referral;

  factory AppointmentData.fromJson(Map<String, dynamic> json) =>
      AppointmentData(
        idProgramAppointment: json["id_program_appointment"] == null
            ? null
            : json["id_program_appointment"],
        idProvider: json["id_provider"] == null ? null : json["id_provider"],
        testId: json["test_id"] == null ? null : json["test_id"],
        testName: json["test_name"] == null ? null : json["test_name"],
        location: json["location"] == null ? null : json["location"],
        timeStart: json["time_start"] == null ? null : json["time_start"],
        timeEnd: json["time_end"] == null ? null : json["time_end"],
        address: json["address"] == null ? null : json["address"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        referral: json["referral"] == null ? null : json["referral"],
      );

  Map<String, dynamic> toJson() => {
        "id_program_appointment":
            idProgramAppointment == null ? null : idProgramAppointment,
        "id_provider": idProvider == null ? null : idProvider,
        "test_id": testId == null ? null : testId,
        "test_name": testName == null ? null : testName,
        "location": location == null ? null : location,
        "time_start": timeStart == null ? null : timeStart,
        "time_end": timeEnd == null ? null : timeEnd,
        "address": address == null ? null : address,
        "date": date == null
            ? null
            : "${date!.year.toString().padLeft(4, '0')}-${date!.month.toString().padLeft(2, '0')}-${date!.day.toString().padLeft(2, '0')}",
        "referral": referral == null ? null : referral,
      };
}
