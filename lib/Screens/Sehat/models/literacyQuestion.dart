class LiteracyQuestion {
  String? id;
  String? questionText;
  String? displayNo;
  String? answerType;

  LiteracyQuestion(
      {this.id, this.questionText, this.displayNo, this.answerType});

  factory LiteracyQuestion.fromJson(Map<String, dynamic> json) =>
      LiteracyQuestion(
        id: json['id'] == null ? null : json['id'],
        questionText:
            json['question_text'] == null ? null : json['question_text'],
        displayNo: json['display_no'] == null ? null : json['display_no'],
        answerType: json['answer_type'] == null ? null : json['answer_type'],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "question_text": questionText == null ? null : questionText,
        "display_no": displayNo == null ? null : displayNo,
        "answer_type": answerType == null ? null : answerType,
      };
}
