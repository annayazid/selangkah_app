// To parse this JSON data, do
//
//     final score = scoreFromJson(jsonString);

import 'dart:convert';

Score scoreFromJson(String str) => Score.fromJson(json.decode(str));

String scoreToJson(Score data) => json.encode(data.toJson());

class Score {
  Score({
    this.code,
    this.data,
    this.warning,
  });

  int? code;
  List<ScoreData>? data;
  int? warning;

  factory Score.fromJson(Map<String, dynamic> json) => Score(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<ScoreData>.from(
                json["Data"].map((x) => ScoreData.fromJson(x))),
        warning: json["Warning"] == null ? null : json["Warning"],
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
        "Warning": warning == null ? null : warning,
      };
}

class ScoreData {
  ScoreData({
    this.idMentalQuestionCat,
    this.title,
    this.child,
  });

  String? idMentalQuestionCat;
  String? title;
  List<Child>? child;

  factory ScoreData.fromJson(Map<String, dynamic> json) => ScoreData(
        idMentalQuestionCat: json["id_mental_question_cat"] == null
            ? null
            : json["id_mental_question_cat"],
        title: json["title"] == null ? null : json["title"],
        child: json["child"] == null
            ? null
            : List<Child>.from(json["child"].map((x) => Child.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id_mental_question_cat":
            idMentalQuestionCat == null ? null : idMentalQuestionCat,
        "title": title == null ? null : title,
        "child": child == null
            ? null
            : List<dynamic>.from(child!.map((x) => x.toJson())),
      };
}

class Child {
  Child({
    this.name,
    this.score,
    this.color,
    this.level,
  });

  String? name;
  String? score;
  String? color;
  String? level;

  factory Child.fromJson(Map<String, dynamic> json) => Child(
        name: json["name"] == null ? null : json["name"],
        score: json["score"] == null ? null : json["score"],
        color: json["color"] == null ? null : json["color"],
        level: json["level"] == null ? null : json["level"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "score": score == null ? null : score,
        "color": color == null ? null : color,
        "level": level == null ? null : level,
      };
}
