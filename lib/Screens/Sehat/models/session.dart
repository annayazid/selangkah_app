// To parse this JSON data, do
//
//     final session = sessionFromJson(jsonString);

import 'dart:convert';

Session sessionFromJson(String str) => Session.fromJson(json.decode(str));

String sessionToJson(Session data) => json.encode(data.toJson());

class Session {
  Session({
    this.code,
    this.session,
  });

  int? code;
  SessionClass? session;

  factory Session.fromJson(Map<String, dynamic> json) => Session(
        code: json["Code"] == null ? null : json["Code"],
        session: json["Session"] == null
            ? null
            : SessionClass.fromJson(json["Session"]),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Session": session == null ? null : session!.toJson(),
      };
}

class SessionClass {
  SessionClass({
    this.id,
  });

  String? id;

  factory SessionClass.fromJson(Map<String, dynamic> json) => SessionClass(
        id: json["id"] == null ? null : json["id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
      };
}
