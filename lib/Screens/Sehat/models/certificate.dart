// To parse this JSON data, do
//
//     final certificate = certificateFromJson(jsonString);

import 'dart:convert';

Certificate certificateFromJson(String str) =>
    Certificate.fromJson(json.decode(str));

String certificateToJson(Certificate data) => json.encode(data.toJson());

class Certificate {
  Certificate({
    this.code,
    this.data,
  });

  int? code;
  CertificateData? data;

  factory Certificate.fromJson(Map<String, dynamic> json) => Certificate(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : CertificateData.fromJson(json["Data"]),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null ? null : data!.toJson(),
      };
}

class CertificateData {
  CertificateData({
    this.accName,
    this.address1,
    this.address2,
    this.postcode,
    this.city,
    this.state,
    this.ppvName,
    this.appDate,
    this.canChange,
  });

  String? accName;
  String? address1;
  String? address2;
  String? postcode;
  String? city;
  String? state;
  String? ppvName;
  DateTime? appDate;
  int? canChange;

  factory CertificateData.fromJson(Map<String, dynamic> json) =>
      CertificateData(
        accName: json["acc_name"] == null ? null : json["acc_name"],
        address1: json["address1"] == null ? null : json["address1"],
        address2: json["address2"] == null ? null : json["address2"],
        postcode: json["postcode"] == null ? null : json["postcode"],
        city: json["city"] == null ? null : json["city"],
        state: json["state"] == null ? null : json["state"],
        ppvName: json["ppv_name"] == null ? null : json["ppv_name"],
        appDate:
            json["app_date"] == null ? null : DateTime.parse(json["app_date"]),
        canChange: json["can_change"] == null ? null : json["can_change"],
      );

  Map<String, dynamic> toJson() => {
        "acc_name": accName == null ? null : accName,
        "address1": address1 == null ? null : address1,
        "address2": address2 == null ? null : address2,
        "postcode": postcode == null ? null : postcode,
        "city": city == null ? null : city,
        "state": state == null ? null : state,
        "ppv_name": ppvName == null ? null : ppvName,
        "app_date": appDate == null
            ? null
            : "${appDate!.year.toString().padLeft(4, '0')}-${appDate!.month.toString().padLeft(2, '0')}-${appDate!.day.toString().padLeft(2, '0')}",
        "can_change": canChange == null ? null : canChange,
      };
}
