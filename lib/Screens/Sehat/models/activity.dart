// To parse this JSON data, do
//
//     final activity = activityFromJson(jsonString);

import 'dart:convert';

Activity activityFromJson(String str) => Activity.fromJson(json.decode(str));

String activityToJson(Activity data) => json.encode(data.toJson());

class Activity {
  Activity({
    this.code,
    this.data,
  });

  int? code;
  List<ActivityData>? data;

  factory Activity.fromJson(Map<String, dynamic> json) => Activity(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<ActivityData>.from(
                json["Data"].map((x) => ActivityData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class ActivityData {
  ActivityData({
    this.id,
    this.activityName,
    this.displayNo,
    this.day,
  });

  String? id;
  String? activityName;
  String? displayNo;
  String? day;

  factory ActivityData.fromJson(Map<String, dynamic> json) => ActivityData(
        id: json["id"] == null ? null : json["id"],
        activityName:
            json["activity_name"] == null ? null : json["activity_name"],
        displayNo: json["display_no"] == null ? null : json["display_no"],
        day: json["day"] == null ? null : json["day"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "activity_name": activityName == null ? null : activityName,
        "display_no": displayNo == null ? null : displayNo,
        "day": day == null ? null : day,
      };
}
