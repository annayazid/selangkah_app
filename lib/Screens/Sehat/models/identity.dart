// To parse this JSON data, do
//
//     final identity = identityFromJson(jsonString);

import 'dart:convert';

Identity identityFromJson(String str) => Identity.fromJson(json.decode(str));

String identityToJson(Identity data) => json.encode(data.toJson());

class Identity {
  Identity({
    this.code,
    this.data,
  });

  int? code;
  Data? data;

  factory Identity.fromJson(Map<String, dynamic> json) => Identity(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null ? null : Data.fromJson(json["Data"]),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null ? null : data!.toJson(),
      };
}

class Data {
  Data({
    this.accName,
    this.phonenumber,
    this.dob,
    this.gender,
    this.religion,
    this.district,
    this.monthlyIncome,
    this.education,
    this.maritalStatus,
    this.householdQuantity,
    this.workStatus,
  });

  String? accName;
  String? phonenumber;
  DateTime? dob;
  String? gender;
  String? religion;
  String? district;
  String? monthlyIncome;
  String? education;
  String? maritalStatus;
  String? householdQuantity;
  String? workStatus;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        accName: json["acc_name"] == null ? null : json["acc_name"],
        phonenumber: json["phonenumber"] == null ? null : json["phonenumber"],
        dob: json["dob"] == null ? null : DateTime.parse(json["dob"]),
        gender: json["gender"] == null ? null : json["gender"],
        religion: json["religion"] == null ? null : json["religion"],
        district: json["district"] == null ? null : json["district"],
        monthlyIncome:
            json["monthly_income"] == null ? null : json["monthly_income"],
        education: json["education"] == null ? null : json["education"],
        maritalStatus:
            json["marital_status"] == null ? null : json["marital_status"],
        householdQuantity: json["household_quantity"] == null
            ? null
            : json["household_quantity"],
        workStatus: json["work_status"] == null ? null : json["work_status"],
      );

  Map<String, dynamic> toJson() => {
        "acc_name": accName == null ? null : accName,
        "phonenumber": phonenumber == null ? null : phonenumber,
        "dob": dob == null
            ? null
            : "${dob!.year.toString().padLeft(4, '0')}-${dob!.month.toString().padLeft(2, '0')}-${dob!.day.toString().padLeft(2, '0')}",
        "gender": gender == null ? null : gender,
        "religion": religion == null ? null : religion,
        "district": district == null ? null : district,
        "monthly_income": monthlyIncome == null ? null : monthlyIncome,
        "education": education == null ? null : education,
        "marital_status": maritalStatus == null ? null : maritalStatus,
        "household_quantity":
            householdQuantity == null ? null : householdQuantity,
        "work_status": workStatus == null ? null : workStatus,
      };
}
