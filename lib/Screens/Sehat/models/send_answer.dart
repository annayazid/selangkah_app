class SendAnswer {
  String idQuestion;
  String answer;
  String idMentalContent;
  String contentType;
  bool canNext;
  SendAnswer(
    this.idQuestion,
    this.answer,
    this.idMentalContent,
    this.contentType,
    this.canNext,
  );
}
