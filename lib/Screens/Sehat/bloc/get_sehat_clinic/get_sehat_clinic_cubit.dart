import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';

part 'get_sehat_clinic_state.dart';

class GetSehatClinicCubit extends Cubit<GetSehatClinicState> {
  GetSehatClinicCubit() : super(GetSehatClinicInitial());

  Future<void> getClinic() async {
    emit(GetSehatClinicLoading());

    //process

    SehatClinic sehatClinic = await SehatRepositories.getClinic();

    emit(GetSehatClinicLoaded(sehatClinic));
  }
}
