part of 'get_sehat_clinic_cubit.dart';

@immutable
abstract class GetSehatClinicState {
  const GetSehatClinicState();
}

class GetSehatClinicInitial extends GetSehatClinicState {
  const GetSehatClinicInitial();
}

class GetSehatClinicLoading extends GetSehatClinicState {
  const GetSehatClinicLoading();
}

class GetSehatClinicLoaded extends GetSehatClinicState {
  final SehatClinic sehatClinic;

  GetSehatClinicLoaded(this.sehatClinic);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetSehatClinicLoaded && other.sehatClinic == sehatClinic;
  }

  @override
  int get hashCode => sehatClinic.hashCode;
}
