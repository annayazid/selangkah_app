import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

part 'certificate_state.dart';

class CertificateCubit extends Cubit<CertificateState> {
  CertificateCubit() : super(CertificateInitial());

  Future<void> getCertificateDetail() async {
    emit(CertificateLoading());

    SecureStorage secureStorage = SecureStorage();
    String? identification = await secureStorage.readSecureData('userIdNo');

    print('ic is $identification');

    if (identification == null) {
      identification = '';
    }

    Certificate certificate = await SehatRepositories.getCertificateDetails();
    Appointment appointment = await SehatRepositories.getAppointment();

    if (certificate.data != null) {
      Score score = await SehatRepositories.getApptScore('MY');

      List<ScorePanel> scorePanel = [];

      if (score.data!.isNotEmpty) {
        for (var i = 0; i < score.data!.length; i++) {
          if (score.data![i].idMentalQuestionCat == '12' ||
              score.data![i].idMentalQuestionCat == '13' ||
              score.data![i].idMentalQuestionCat == '14' ||
              score.data![i].idMentalQuestionCat == '15') {
            if (score.data![i].child!.isNotEmpty) {
              List<ScoreTile> scoreTile = [];

              for (var j = 0; j < score.data![i].child!.length; j++) {
                scoreTile.add(ScoreTile(
                  name: score.data![i].child![j].name,
                  score: score.data![i].child![j].score,
                  level: score.data![i].child![j].level,
                ));
              }

              scorePanel.add(ScorePanel(
                id: score.data![i].idMentalQuestionCat,
                title: score.data![i].title,
                score: scoreTile,
              ));
            }
          }
        }
        scorePanel.sort((a, b) => a.id!.compareTo(b.id!));
      }
      emit(CertificateLoaded(
          certificate, appointment, identification, scorePanel));
    } else {
      emit(RegistrationLoaded(appointment));
    }
  }
}
