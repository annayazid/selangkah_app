part of 'certificate_cubit.dart';

@immutable
abstract class CertificateState {
  const CertificateState();
}

class CertificateInitial extends CertificateState {
  const CertificateInitial();
}

class CertificateLoading extends CertificateState {
  const CertificateLoading();
}

class RegistrationLoaded extends CertificateState {
  final Appointment appointment;

  RegistrationLoaded(this.appointment);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RegistrationLoaded && other.appointment == appointment;
  }

  @override
  int get hashCode => appointment.hashCode;
}

class CertificateLoaded extends CertificateState {
  final Certificate certificate;
  final Appointment appointment;
  final String identification;
  final List<ScorePanel> scorePanel;

  CertificateLoaded(
      this.certificate, this.appointment, this.identification, this.scorePanel);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CertificateLoaded && other.certificate == certificate;
  }

  @override
  int get hashCode => certificate.hashCode;
}
