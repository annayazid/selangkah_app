part of 'get_homepage_cubit.dart';

@immutable
abstract class GetHomepageState {
  const GetHomepageState();
}

class GetHomepageInitial extends GetHomepageState {
  const GetHomepageInitial();
}

class GetHomepageLoading extends GetHomepageState {
  const GetHomepageLoading();
}

class GetHomepagePopUpStress extends GetHomepageState {
  final int totalStress;

  GetHomepagePopUpStress(this.totalStress);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetHomepagePopUpStress && other.totalStress == totalStress;
  }

  @override
  int get hashCode => totalStress.hashCode;
}

class GetHomepagePopUpMental extends GetHomepageState {
  const GetHomepagePopUpMental();
}

class GetHomepageLoaded extends GetHomepageState {
  final Journey? journey;
  final Uint8List? imgData;
  final String? name;
  final String? status;
  final Category? category;
  final Score? score;
  final List<ScorePanel?>? scorePanel;
  final String? notiText;
  final String? literacyScore;
  final SlotAppointment? slotAppointment;
  final bool? gotCertificate;

  GetHomepageLoaded({
    this.journey,
    this.imgData,
    this.name,
    this.status,
    this.category,
    this.score,
    this.scorePanel,
    this.notiText,
    this.literacyScore,
    this.slotAppointment,
    this.gotCertificate,
  });

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetHomepageLoaded &&
        other.journey == journey &&
        other.imgData == imgData &&
        other.name == name &&
        other.status == status &&
        other.category == category &&
        other.score == score &&
        foundation.listEquals(other.scorePanel, scorePanel) &&
        other.notiText == notiText &&
        other.literacyScore == literacyScore &&
        other.slotAppointment == slotAppointment &&
        other.gotCertificate == gotCertificate;
  }

  @override
  int get hashCode {
    return journey.hashCode ^
        imgData.hashCode ^
        name.hashCode ^
        status.hashCode ^
        category.hashCode ^
        score.hashCode ^
        scorePanel.hashCode ^
        notiText.hashCode ^
        literacyScore.hashCode ^
        slotAppointment.hashCode ^
        gotCertificate.hashCode;
  }
}
