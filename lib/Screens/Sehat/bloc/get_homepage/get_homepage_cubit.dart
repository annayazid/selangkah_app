import 'dart:convert';
import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter/material.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';

import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/utils/global.dart';

part 'get_homepage_state.dart';

class GetHomepageCubit extends Cubit<GetHomepageState> {
  GetHomepageCubit() : super(GetHomepageInitial());

  Future<void> getHomepage() async {
    emit(GetHomepageLoading());

    //get session
    //if 0 = set session
    //if != 0, get journey

    //process session
    Session session = await SehatRepositories.getSession();

    int sessionConverted = int.parse(session.session!.id!);

    if (sessionConverted == 0) {
      //update current session
      await SehatRepositories.setSession();
      session = await SehatRepositories.getSession();
      sessionConverted = int.parse(session.session!.id!);
      SehatGlobalVar.currentSession = sessionConverted;
    } else {
      SehatGlobalVar.currentSession = sessionConverted;
    }

    print('session is $sessionConverted');
    //kalau status 6 = "1"

    Journey journey = await SehatRepositories.getJourney(sessionConverted);

    SehatGlobalVar.journeyGlobal = journey;
    if (SehatGlobalVar.journeyFirst) {
      SehatGlobalVar.journeyFirst = false;
    }

    //get profile
    RawProfile? profile = await SehatRepositories.getProfile();

    Uint8List? imgbytes;

    if (profile!.data![18] != null) {
      imgbytes = base64.decode(profile.data![18]!);
    } else {
      imgbytes = null;
    }

    //setting status

    String status = '';

    if (!journey.data!.any((element) => element.idMentalQuestionCat == '1')) {
      status = 'pending_confirmation'.tr();
    } else if ((!journey.data!
                .any((element) => element.idMentalQuestionCat == '12') &&
            !journey.data!
                .any((element) => element.idMentalQuestionCat == '13') &&
            !journey.data!
                .any((element) => element.idMentalQuestionCat == '14') &&
            !journey.data!
                .any((element) => element.idMentalQuestionCat == '15')) ||
        !journey.data!.any((element) => element.idMentalQuestionCat == '3')) {
      status = 'pending_screening'.tr().toUpperCase();
    } else if (!journey.data!
        .any((element) => element.idMentalQuestionCat == '7')) {
      status = 'mental_health_module'.tr().toUpperCase();
    } else if (!journey.data!
        .any((element) => element.idMentalQuestionCat == '4')) {
      status = 'watching_video'.tr();
    } else if (!journey.data!
        .any((element) => element.idMentalQuestionCat == '5')) {
      status = 'hotline'.tr().toUpperCase();
    } else if (!journey.data!
        .any((element) => element.idMentalQuestionCat == '6')) {
      status = 'pending_survey'.tr().toUpperCase();
    } else {
      status = 'completed'.tr().toUpperCase();
    }

    // if (journey.data[0].status1 == "0") {
    //   status = 'pending_confirmation'.tr();
    // } else if (journey.data[0].status2 == "0") {
    //   status = 'pending_screening'.tr().toUpperCase();
    // } else if (journey.data[0].status3 == "0") {
    //   status = 'pending_screening'.tr().toUpperCase();
    // } else if (journey.data[0].status4 == "0") {
    //   status = 'watching_video_whatsdoc'.tr().toUpperCase();
    // } else if (journey.data[0].status5 == "0") {
    //   status = 'watching_video_whatsdoc'.tr().toUpperCase();
    // } else if (journey.data[0].status6 == "0") {
    //   status = 'pending_survey'.tr().toUpperCase();
    // } else {
    //   status = 'completed'.tr().toUpperCase();
    // }

    //get total stress
    // int totalStress = await SehatRepositories.getTotal('2');
    // Color stressColor;
    // String stressLevel;

    // //print(totalStress);

    // if (totalStress >= 0 && totalStress <= 13) {
    //   stressColor = Colors.green;
    //   stressLevel = 'low'.tr();
    // } else if (totalStress >= 14 && totalStress <= 26) {
    //   stressColor = Colors.orange;
    //   stressLevel = 'medium'.tr();
    // } else if (totalStress >= 26 && totalStress <= 40) {
    //   stressColor = Colors.red;
    //   stressLevel = 'high'.tr();
    // } else {
    //   stressColor = Colors.white;
    //   stressLevel = '';
    // }

    // if (journey.data[0].status2 == "1" &&
    //     journey.data[0].status3 == "1" &&
    //     !SehatGlobalVar.dismissedPopUpStress) {
    //   if (journey.data[0].status5 == "1" &&
    //       journey.data[0].status6 == "0" &&
    //       !SehatGlobalVar.dismissedPopUpMental) {
    //     SehatGlobalVar.dismissedPopUpStress = true;
    //     yield HomePagePopUpMental();
    //   }
    //   if (!SehatGlobalVar.dismissedPopUpStress) {
    //     yield HomepagePopUpStress(totalStress);
    //   }
    // }

    // int totalMental = 1000;

    print('session is ${SehatGlobalVar.currentSession}');

    Category category = await SehatRepositories.getCategory(profile.data![5]!);

    category.data!.sort((a, b) => a.displayNo!.compareTo(b.displayNo!));

    Score score = await SehatRepositories.getScore(GlobalVariables.language!);

    List<ScorePanel> scorePanel = [];

    if (score.data!.isNotEmpty) {
      for (var i = 0; i < score.data!.length; i++) {
        if (score.data![i].idMentalQuestionCat == '12' ||
            score.data![i].idMentalQuestionCat == '13' ||
            score.data![i].idMentalQuestionCat == '14' ||
            score.data![i].idMentalQuestionCat == '15') {
          if (score.data![i].child!.isNotEmpty) {
            List<ScoreTile> scoreTile = [];

            for (var j = 0; j < score.data![i].child!.length; j++) {
              scoreTile.add(ScoreTile(
                name: score.data![i].child![j].name,
                color: Color(int.parse(
                    '0xFF${score.data![i].child![j].color!.substring(1, score.data![i].child![j].color!.length)}')),
                level: score.data![i].child![j].level,
              ));
            }

            scorePanel.add(ScorePanel(
              id: score.data![i].idMentalQuestionCat,
              title: score.data![i].title,
              score: scoreTile,
            ));
          }
        }
      }
      scorePanel.sort((a, b) => a.id!.compareTo(b.id!));
    }

    String notiText = await SehatRepositories.getNotiText('${score.warning}');
    SlotAppointment slotAppointment = await SehatRepositories.getWarnStatus();
    Certificate certificate = await SehatRepositories.getCertificateDetails();

    bool gotCertificate = certificate.data != null ? true : false;

    //get literacy score

    String? literacyScore;

    for (var i = 0; i < journey.data!.length; i++) {
      print(journey.data![i].idMentalQuestionCat);
      if (journey.data![i].idMentalQuestionCat == '6') {
        literacyScore = journey.data![i].score!;
      }
    }

    emit(
      GetHomepageLoaded(
        journey: journey,
        name: profile.data![2]!,
        imgData: imgbytes,
        status: status,
        category: category,
        score: score,
        scorePanel: scorePanel,
        notiText: notiText,
        literacyScore: literacyScore,
        slotAppointment: slotAppointment,
        gotCertificate: gotCertificate,
      ),
    );
  }

  Future<void> updateSession() async {
    emit(GetHomepageLoading());

    await SehatRepositories.setSession();

    getHomepage();
  }
}
