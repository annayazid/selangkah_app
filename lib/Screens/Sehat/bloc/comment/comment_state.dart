part of 'comment_cubit.dart';

@immutable
abstract class CommentState {
  const CommentState();
}

class CommentInitial extends CommentState {
  const CommentInitial();
}

class CommentLoading extends CommentState {
  const CommentLoading();
}

class CommentSent extends CommentState {
  const CommentSent();
}
