import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';

part 'comment_state.dart';

class CommentCubit extends Cubit<CommentState> {
  CommentCubit() : super(CommentInitial());

  Future<void> sendFeedback(double rating, String comment) async {
    emit(CommentLoading());

    //process

    // await Future.delayed(Duration(seconds: 2));

    comment = comment.replaceAll('\'', '');
    comment = comment.replaceAll('"', '');
    comment = comment.replaceAll('`', '');

    await SehatRepositories.sendFeedback(rating, comment);

    emit(CommentSent());
  }
}
