part of 'get_content_cubit.dart';

@immutable
abstract class GetContentState {
  const GetContentState();
}

class GetContentInitial extends GetContentState {
  const GetContentInitial();
}

class GetContentLoading extends GetContentState {
  const GetContentLoading();
}

class GetContentLoaded extends GetContentState {
  final Content content;

  GetContentLoaded(this.content);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetContentLoaded && other.content == content;
  }

  @override
  int get hashCode => content.hashCode;
}
