import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';

part 'get_content_state.dart';

class GetContentCubit extends Cubit<GetContentState> {
  GetContentCubit() : super(GetContentInitial());

  Future<void> getContent(
    String idCategory,
    String language,
    bool isQuestionnaire,
  ) async {
    emit(GetContentLoading());

    //process
    // Category category = await SehatRepositories.getCategoryModule(idCategory, language);

    //process
    Content content = await SehatRepositories.getContent(
      idCategory,
      language,
      isQuestionnaire,
    );

    emit(GetContentLoaded(content));
  }
}
