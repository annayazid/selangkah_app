import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';

part 'get_whatsdoc_state.dart';

class GetWhatsdocCubit extends Cubit<GetWhatsdocState> {
  GetWhatsdocCubit() : super(GetWhatsdocInitial());

  Future<void> getWhatsdoc() async {
    emit(GetWhatsdocLoading());

    //process

    await SehatRepositories.setAnswerCat5();

    SehatLine sehatLine = await SehatRepositories.getSehatLine();

    emit(GetWhatsdocLoaded(sehatLine: sehatLine));
  }
}
