part of 'get_whatsdoc_cubit.dart';

@immutable
abstract class GetWhatsdocState {
  const GetWhatsdocState();
}

class GetWhatsdocInitial extends GetWhatsdocState {
  const GetWhatsdocInitial();
}

class GetWhatsdocLoading extends GetWhatsdocState {
  const GetWhatsdocLoading();
}

class GetWhatsdocLoaded extends GetWhatsdocState {
  final SehatLine sehatLine;
  GetWhatsdocLoaded({
    required this.sehatLine,
  });

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetWhatsdocLoaded && other.sehatLine == sehatLine;
  }

  @override
  int get hashCode => sehatLine.hashCode;
}
