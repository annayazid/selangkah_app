import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';

part 'get_pathway_state.dart';

class GetPathwayCubit extends Cubit<GetPathwayState> {
  GetPathwayCubit() : super(GetPathwayInitial());

  Future<void> getPathway(String idModule, String language) async {
    emit(GetPathwayLoading());

    //process
    Pathway pathway = await SehatRepositories.getPathway(idModule, language);

    ActivityComplete activityComplete =
        await SehatRepositories.getActivityComplete();

    //get activity user done
    List<Activity> activityList = [];

    List<bool> pathwayCanAccess = [];
    pathwayCanAccess.add(true);

    if (pathway.data!.isNotEmpty) {
      for (var i = 0; i < pathway.data!.length; i++) {
        Activity activity =
            await SehatRepositories.getActivity(pathway.data![i].id!, language);
        activityList.add(activity);
      }
    }

    // pathwayCanAccess.add(activity.data.every((activityElement) =>
    //     activityComplete.data.any((activityCompleteElement) =>
    //         activityCompleteElement.idMentalActivity == activityElement.id)));

    //check if previous activty is completed all or not
    for (var i = 1; i < pathway.data!.length; i++) {
      pathwayCanAccess.add(activityList[i - 1].data!.every((activityElement) =>
          activityComplete.data!.any((activityCompleteElement) =>
              activityCompleteElement.idMentalActivity == activityElement.id)));

      // pathwayCanAccess.add(true);
    }

    // for (var i = 0; i < pathwayCanAccess.length; i++) {
    //   print('$i ${pathwayCanAccess[i]}');
    // }

    // for (var i = 0; i < activityList.length; i++) {
    //   print(activityList[i].data.length);
    // }

    // print('leng activity is ${activityList.length}');

    bool isReceiveNotification = await SehatRepositories.getNotification();

    emit(GetPathwayLoaded(pathway, activityList, activityComplete,
        pathwayCanAccess, isReceiveNotification));
  }
}
