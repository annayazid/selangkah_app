part of 'get_pathway_cubit.dart';

@immutable
abstract class GetPathwayState {
  const GetPathwayState();
}

class GetPathwayInitial extends GetPathwayState {
  const GetPathwayInitial();
}

class GetPathwayLoading extends GetPathwayState {
  const GetPathwayLoading();
}

class GetPathwayLoaded extends GetPathwayState {
  final Pathway pathway;
  final List<Activity> activityList;
  final ActivityComplete activityComplete;
  final List<bool> pathwayCanAccess;
  final bool isReceiveNotification;

  GetPathwayLoaded(this.pathway, this.activityList, this.activityComplete,
      this.pathwayCanAccess, this.isReceiveNotification);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetPathwayLoaded &&
        other.pathway == pathway &&
        listEquals(other.activityList, activityList) &&
        other.activityComplete == activityComplete &&
        listEquals(other.pathwayCanAccess, pathwayCanAccess) &&
        other.isReceiveNotification == isReceiveNotification;
  }

  @override
  int get hashCode {
    return pathway.hashCode ^
        activityList.hashCode ^
        activityComplete.hashCode ^
        pathwayCanAccess.hashCode ^
        isReceiveNotification.hashCode;
  }
}
