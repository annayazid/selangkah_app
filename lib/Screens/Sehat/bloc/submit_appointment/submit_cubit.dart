import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';

part 'submit_state.dart';

class SubmitCubit extends Cubit<SubmitState> {
  SubmitCubit() : super(SubmitInitial());

  Future<void> changeAppointment(
      String idProvider, String idTest, String idProgram, String date) async {
    emit(SubmitLoading());

    await SehatRepositories.changeAppointment(
        idProvider, idTest, idProgram, date);

    emit(SubmitLoaded());
  }

  Future<void> setAppointment(
      String idProvider, String date, String language) async {
    emit(SubmitLoading());

    await SehatRepositories.setAppointment(idProvider, date, language);

    emit(SubmitLoaded());
  }
}
