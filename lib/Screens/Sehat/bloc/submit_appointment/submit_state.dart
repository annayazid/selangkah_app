part of 'submit_cubit.dart';

@immutable
abstract class SubmitState {
  const SubmitState();
}

class SubmitInitial extends SubmitState {
  const SubmitInitial();
}

class SubmitLoading extends SubmitState {
  const SubmitLoading();
}

class SubmitLoaded extends SubmitState {
  const SubmitLoaded();
}
