import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';

part 'get_sehat_date_state.dart';

class GetSehatDateCubit extends Cubit<GetSehatDateState> {
  GetSehatDateCubit() : super(GetSehatDateInitial());

  Future<void> getDateTime(String idProvider) async {
    emit(GetSehatDateLoading());

    //
    SehatDate sehatDate = await SehatRepositories.getDate(idProvider);

    emit(GetSehatDateLoaded(sehatDate));
  }
  // if (state is GetSehatResetDate()){
  //   yield GetSehatDateInitial();
  // }
}
