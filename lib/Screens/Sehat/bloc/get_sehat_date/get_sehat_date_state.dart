part of 'get_sehat_date_cubit.dart';

@immutable
abstract class GetSehatDateState {
  const GetSehatDateState();
}

class GetSehatDateInitial extends GetSehatDateState {
  const GetSehatDateInitial();
}

// class GetSehatResetDate extends GetSehatDateState {
//   const GetSehatResetDate();
// }

class GetSehatDateLoaded extends GetSehatDateState {
  final SehatDate sehatDate;

  GetSehatDateLoaded(this.sehatDate);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetSehatDateLoaded && other.sehatDate == sehatDate;
  }

  @override
  int get hashCode => sehatDate.hashCode;
}

class GetSehatDateLoading extends GetSehatDateState {
  const GetSehatDateLoading();
}
