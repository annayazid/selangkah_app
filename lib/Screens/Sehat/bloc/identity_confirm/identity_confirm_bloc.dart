// import 'dart:async';

// import 'package:bloc/bloc.dart';
// import 'package:meta/meta.dart';
// import 'package:easy_localization/easy_localization.dart';
// import 'package:selangkah_new/Screens/Sehat/sehat.dart';

// part 'identity_confirm_event.dart';
// part 'identity_confirm_state.dart';

// class IdentityConfirmBloc
//     extends Bloc<IdentityConfirmEvent, IdentityConfirmState> {
//   IdentityConfirmBloc() : super(IdentityConfirmInitial());

//   Stream<IdentityConfirmState> mapEventToState(
//     IdentityConfirmEvent event,
//   ) async* {
//     if (event is SendIdentity) {
//       yield IdentityConfirmLoading();

//       //set gender
//       if (event.gender == 'male'.tr()) {
//         event.gender = 'M';
//       } else {
//         event.gender = 'F';
//       }

//       //set religion
//       if (event.religion == 'muslim'.tr()) {
//         event.religion = 'muslim';
//       } else if (event.religion == 'christian'.tr()) {
//         event.religion = 'christian';
//       } else if (event.religion == 'buddha'.tr()) {
//         event.religion = 'buddha';
//       } else if (event.religion == 'hindu'.tr()) {
//         event.religion = 'hindu';
//       } else {
//         event.religion = 'others';
//       }

//       //set income
//       if (event.income == 'no_income'.tr()) {
//         event.income = 'no_income';
//       }

//       //set education
//       if (event.education == 'no_education'.tr()) {
//         event.education = 'no_education';
//       } else if (event.education == 'primary'.tr()) {
//         event.education = 'primary';
//       } else if (event.education == 'secondary'.tr()) {
//         event.education = 'secondary';
//       } else if (event.education == 'college'.tr()) {
//         event.education = 'college';
//       } else {
//         event.education = 'university';
//       }

//       //set district if non selangor
//       if (event.district == 'NON SELANGOR' ||
//           event.district == 'LUAR SELANGOR') {
//         event.district = 'NON SELANGOR';
//       }

//       //set work status
//       if (event.workStatus == 'working_fulltime'.tr()) {
//         event.workStatus = 'working_fulltime';
//       } else if (event.workStatus == 'working_parttime'.tr()) {
//         event.workStatus = 'working_parttime';
//       } else {
//         event.workStatus = 'waiting_for_job';
//       }

//       //set marital status
//       if (event.maritalStatus == 'single'.tr()) {
//         event.maritalStatus = 'single';
//       } else if (event.maritalStatus == 'married'.tr()) {
//         event.maritalStatus = 'married';
//       } else if (event.maritalStatus == 'divorced'.tr()) {
//         event.maritalStatus = 'divorced';
//       } else if (event.maritalStatus == 'widow'.tr()) {
//         event.maritalStatus = 'widow';
//       } else if (event.maritalStatus == 'widower'.tr()) {
//         event.maritalStatus = 'widower';
//       } else if (event.maritalStatus == 'separated'.tr()) {
//         event.maritalStatus = 'separated';
//       } else {
//         event.maritalStatus = 'other';
//       }

//       //process
//       await SehatRepositories.setIdentity(
//         event.age!,
//         event.gender!,
//         event.religion!,
//         event.district!,
//         event.income!,
//         event.education!,
//         event.workStatus!,
//         event.maritalStatus!,
//         event.houseHold!,
//         event.dob!,
//       );

//       yield IdentityConfirmFinish();
//     }
//   }
// }
