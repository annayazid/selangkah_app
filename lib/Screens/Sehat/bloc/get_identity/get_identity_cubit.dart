import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';

part 'get_identity_state.dart';

class GetIdentityCubit extends Cubit<GetIdentityState> {
  GetIdentityCubit() : super(GetIdentityInitial());

  Future<void> getIdentity() async {
    emit(GetIdentityLoading());

    //process here
    Identity identity = await SehatRepositories.getIdentity();

    //set gender
    if (identity.data!.gender != null) {
      identity.data!.gender = identity.data!.gender!.toUpperCase() == 'M'
          ? 'male'.tr()
          : 'female'.tr();
    }

    //set age
    // identity.data.age = identity.data.age == '0' ? '' : identity.data.age;

    //set religion
    if (identity.data!.religion != null) {
      if (identity.data!.religion == 'muslim') {
        identity.data!.religion = 'muslim'.tr();
      } else if (identity.data!.religion == 'christian') {
        identity.data!.religion = 'christian'.tr();
      } else if (identity.data!.religion == 'buddha') {
        identity.data!.religion = 'buddha'.tr();
      } else if (identity.data!.religion == 'hindu') {
        identity.data!.religion = 'hindu'.tr();
      } else {
        identity.data!.religion = 'others'.tr();
      }
    }

    //set income
    if (identity.data!.monthlyIncome != null) {
      if (identity.data!.monthlyIncome == 'no_income') {
        identity.data!.monthlyIncome = 'no_income'.tr();
      }
    }

    //set education
    if (identity.data!.education != null) {
      if (identity.data!.education == 'no_education') {
        identity.data!.education = 'no_education'.tr();
      } else if (identity.data!.education == 'primary') {
        identity.data!.education = 'primary'.tr();
      } else if (identity.data!.education == 'secondary') {
        identity.data!.education = 'secondary'.tr();
      } else if (identity.data!.education == 'college') {
        identity.data!.education = 'college'.tr();
      } else if (identity.data!.education == 'university') {
        identity.data!.education = 'university'.tr();
      } else {
        identity.data!.education = 'please_choose'.tr();
      }
    }

    //set work status
    if (identity.data!.workStatus == 'working_fulltime') {
      identity.data!.workStatus = 'working_fulltime'.tr();
    } else if (identity.data!.workStatus == 'working_parttime') {
      identity.data!.workStatus = 'working_parttime'.tr();
    } else {
      identity.data!.workStatus = 'please_choose'.tr();
    }

    //set marital status
    if (identity.data!.maritalStatus == 'single') {
      identity.data!.maritalStatus = 'single'.tr();
    } else if (identity.data!.maritalStatus == 'married') {
      identity.data!.maritalStatus = 'married'.tr();
    } else if (identity.data!.maritalStatus == 'divorced') {
      identity.data!.maritalStatus = 'divorced'.tr();
    } else if (identity.data!.maritalStatus == 'widow') {
      identity.data!.maritalStatus = 'widow'.tr();
    } else if (identity.data!.maritalStatus == 'widower') {
      identity.data!.maritalStatus = 'widower'.tr();
    } else if (identity.data!.maritalStatus == 'separated') {
      identity.data!.maritalStatus = 'separated'.tr();
    } else {
      identity.data!.maritalStatus = 'please_choose'.tr();
    }

    print('workStatus is ${identity.data!.workStatus}');
    print('householdQuantity is ${identity.data!.householdQuantity}');
    print('maritalStatus is ${identity.data!.maritalStatus}');

    emit(GetIdentityLoaded(
      name: identity.data!.accName,
      phoneNumber: identity.data!.phonenumber,
      // age: identity.data.age,
      gender: identity.data!.gender,
      religion: identity.data!.religion,
      district: identity.data!.district,
      income: identity.data!.monthlyIncome,
      education: identity.data!.education,
      workStatus: identity.data!.workStatus,
      houseHold: identity.data!.householdQuantity,
      maritalStatus: identity.data!.maritalStatus,
      dob: identity.data!.dob,
    ));
  }
}
