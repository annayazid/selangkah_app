part of 'get_identity_cubit.dart';

@immutable
abstract class GetIdentityState {
  const GetIdentityState();
}

class GetIdentityInitial extends GetIdentityState {
  const GetIdentityInitial();
}

class GetIdentityLoading extends GetIdentityState {
  const GetIdentityLoading();
}

class GetIdentityLoaded extends GetIdentityState {
  final String? name;
  final String? phoneNumber;
  final String? age;
  final String? gender;
  final String? religion;
  final String? district;
  final String? income;
  final String? education;
  final String? workStatus;
  final String? maritalStatus;
  final String? houseHold;
  final DateTime? dob;

  GetIdentityLoaded(
      {this.name,
      this.phoneNumber,
      this.age,
      this.gender,
      this.religion,
      this.district,
      this.income,
      this.education,
      this.workStatus,
      this.maritalStatus,
      this.houseHold,
      this.dob});

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetIdentityLoaded &&
        other.name == name &&
        other.phoneNumber == phoneNumber &&
        other.age == age &&
        other.gender == gender &&
        other.religion == religion &&
        other.district == district &&
        other.income == income &&
        other.education == education &&
        other.workStatus == workStatus &&
        other.maritalStatus == maritalStatus &&
        other.houseHold == houseHold &&
        other.dob == dob;
  }

  @override
  int get hashCode {
    return name.hashCode ^
        phoneNumber.hashCode ^
        age.hashCode ^
        gender.hashCode ^
        religion.hashCode ^
        district.hashCode ^
        income.hashCode ^
        education.hashCode ^
        workStatus.hashCode ^
        maritalStatus.hashCode ^
        houseHold.hashCode ^
        dob.hashCode;
  }
}
