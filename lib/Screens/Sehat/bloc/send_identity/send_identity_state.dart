part of 'send_identity_cubit.dart';

@immutable
abstract class SendIdentityState {
  const SendIdentityState();
}

class SendIdentityInitial extends SendIdentityState {
  const SendIdentityInitial();
}

class SendIdentityLoading extends SendIdentityState {
  const SendIdentityLoading();
}

class SendIdentityLoaded extends SendIdentityState {
  const SendIdentityLoaded();
}
