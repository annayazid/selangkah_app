import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';

part 'send_identity_state.dart';

class SendIdentityCubit extends Cubit<SendIdentityState> {
  SendIdentityCubit() : super(SendIdentityInitial());

  Future<void> sendIdentity({
    String? age,
    String? gender,
    String? religion,
    String? district,
    String? income,
    String? education,
    String? workStatus,
    String? maritalStatus,
    String? houseHold,
    DateTime? dob,
  }) async {
    emit(SendIdentityLoading());

    //set gender
    if (gender == 'male'.tr()) {
      gender = 'M';
    } else {
      gender = 'F';
    }

    //set religion
    if (religion == 'muslim'.tr()) {
      religion = 'muslim';
    } else if (religion == 'christian'.tr()) {
      religion = 'christian';
    } else if (religion == 'buddha'.tr()) {
      religion = 'buddha';
    } else if (religion == 'hindu'.tr()) {
      religion = 'hindu';
    } else {
      religion = 'others';
    }

    //set income
    if (income == 'no_income'.tr()) {
      income = 'no_income';
    }

    //set education
    if (education == 'no_education'.tr()) {
      education = 'no_education';
    } else if (education == 'primary'.tr()) {
      education = 'primary';
    } else if (education == 'secondary'.tr()) {
      education = 'secondary';
    } else if (education == 'college'.tr()) {
      education = 'college';
    } else {
      education = 'university';
    }

    //set district if non selangor
    if (district == 'NON SELANGOR' || district == 'LUAR SELANGOR') {
      district = 'NON SELANGOR';
    }

    //set work status
    if (workStatus == 'working_fulltime'.tr()) {
      workStatus = 'working_fulltime';
    } else if (workStatus == 'working_parttime'.tr()) {
      workStatus = 'working_parttime';
    } else {
      workStatus = 'waiting_for_job';
    }

    //set marital status
    if (maritalStatus == 'single'.tr()) {
      maritalStatus = 'single';
    } else if (maritalStatus == 'married'.tr()) {
      maritalStatus = 'married';
    } else if (maritalStatus == 'divorced'.tr()) {
      maritalStatus = 'divorced';
    } else if (maritalStatus == 'widow'.tr()) {
      maritalStatus = 'widow';
    } else if (maritalStatus == 'widower'.tr()) {
      maritalStatus = 'widower';
    } else if (maritalStatus == 'separated'.tr()) {
      maritalStatus = 'separated';
    } else {
      maritalStatus = 'other';
    }

    //process
    await SehatRepositories.setIdentity(
      age!,
      gender,
      religion,
      district!,
      income!,
      education,
      workStatus,
      maritalStatus,
      houseHold!,
      dob!,
    );

    emit(SendIdentityLoaded());
  }
}
