part of 'get_module_cubit.dart';

@immutable
abstract class GetModuleState {
  const GetModuleState();
}

class GetModuleInitial extends GetModuleState {
  const GetModuleInitial();
}

class GetModuleLoading extends GetModuleState {
  const GetModuleLoading();
}

class GetModuleLoaded extends GetModuleState {
  final Category category;
  final Journey journey;

  GetModuleLoaded(this.category, this.journey);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetModuleLoaded &&
        other.category == category &&
        other.journey == journey;
  }

  @override
  int get hashCode => category.hashCode ^ journey.hashCode;
}
