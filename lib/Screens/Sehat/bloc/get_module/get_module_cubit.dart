import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';

part 'get_module_state.dart';

class GetModuleCubit extends Cubit<GetModuleState> {
  GetModuleCubit() : super(GetModuleInitial());

  Future<void> getModule(String idCategory, String language) async {
    emit(GetModuleLoading());

    //process
    Category category =
        await SehatRepositories.getCategoryModule(idCategory, language);

    int session = SehatGlobalVar.currentSession!;

    Journey journey = await SehatRepositories.getJourney(session);

    emit(GetModuleLoaded(category, journey));
  }
}
