import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:url_launcher/url_launcher.dart';

class WhatsDoc extends StatefulWidget {
  const WhatsDoc({Key? key}) : super(key: key);

  @override
  _WhatsDocState createState() => _WhatsDocState();
}

class _WhatsDocState extends State<WhatsDoc> {
  @override
  void initState() {
    context.read<GetWhatsdocCubit>().getWhatsdoc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;

    return SehatScaffold(
      appBarTitle: 'talian_sehat'.tr(),
      child: Container(
        height: height - AppBar().preferredSize.height,
        width: width,
        child: BlocConsumer<GetWhatsdocCubit, GetWhatsdocState>(
          listener: (context, state) {
            if (state is GetWhatsdocLoaded) {
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                    backgroundColor: Colors.white,
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          FontAwesomeIcons.circleExclamation,
                          size: 60,
                          color: themeColor,
                        ),
                        SizedBox(height: 20),
                        Text(
                          'text1_whatsdoc'.tr(),
                          style: TextStyle(
                            fontSize: 14,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 10),
                        Text(
                          'text2_whatsdoc'.tr(),
                          style: TextStyle(
                            fontSize: 14,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 10),
                        Text(
                          'text3_whatsdoc'.tr(),
                          style: TextStyle(
                            fontSize: 14,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 10),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: themeColor,
                            padding: EdgeInsets.symmetric(
                              horizontal: 30,
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            'OK',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              );
            }
          },
          builder: (context, state) {
            if (state is GetWhatsdocLoaded) {
              return WhatsdocLoadedWidget(
                sehatLine: state.sehatLine,
              );
            } else {
              return SpinKitFadingCircle(
                color: themeColor,
                size: 30,
              );
            }
          },
        ),
      ),
    );
  }
}

class WhatsdocLoadedWidget extends StatelessWidget {
  final SehatLine sehatLine;

  const WhatsdocLoadedWidget({Key? key, required this.sehatLine})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 30),

          Container(
            height: height * 0.3,
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: Stack(
              children: [
                Container(
                  color: Colors.black,
                  child: Image.asset(
                    'assets/images/sehat/doctor3.jpg',
                    fit: BoxFit.fitHeight,
                  ),
                ),
                Positioned(
                  right: width * 0.01,
                  child: Container(
                    width: width * 0.45,
                    child: Column(
                      children: [
                        SizedBox(height: 20),
                        Text(
                          '${'feeling_stress'.tr()}?',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 10),
                        Text(
                          'talk_counselor'.tr(),
                          style: TextStyle(
                            // fontWeight: FontWeight.bold,
                            fontSize: 17,
                            color: Colors.blue,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),

                // BackdropFilter(
                //   filter: ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
                //   child: Container(
                //     // the size where the blurring starts
                //     height: height * 0.4,
                //     color: Colors.transparent,
                //   ),
                // ),
              ],
            ),
          ),

          //
          // SizedBox(height: 20),

          // SizedBox(
          //   width: width * 0.8,
          //   child: ElevatedButton(
          //     style: ElevatedButton.styleFrom(
          //       primary: Color(0xFF2A3A91),
          //       padding: EdgeInsets.symmetric(vertical: 10),
          //     ),
          //     onPressed: () {},
          //     child: Row(
          //       mainAxisAlignment: MainAxisAlignment.center,
          //       children: [
          //         Icon(Icons.phone),
          //         SizedBox(width: 10),
          //         Text(
          //           'call_now_line_1'.tr(),
          //           style: TextStyle(
          //             fontSize: 17,
          //           ),
          //         ),
          //       ],
          //     ),
          //   ),
          // ),

          SizedBox(height: 10),

          PhoneButton(
            line: '1',
            phone: sehatLine.data!.tel1!,
          ),

          SizedBox(height: 10),

          PhoneButton(
            line: '2',
            phone: sehatLine.data!.tel2!,
          ),

          SizedBox(height: 10),

          Container(
            margin: EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              children: [
                Icon(
                  FontAwesomeIcons.calendarDays,
                ),
                SizedBox(width: 20),
                Expanded(
                  child: Text(
                    'operating_hours_whatsdoc'.tr(),
                  ),
                ),
              ],
            ),
          ),

          SizedBox(height: 10),

          // Container(
          //   margin: EdgeInsets.symmetric(horizontal: 30),
          //   child: Divider(
          //     color: Colors.grey,
          //   ),
          // ),

          // SizedBox(height: 10),

          // Container(
          //   margin: EdgeInsets.symmetric(horizontal: 30),
          //   child: Row(
          //     children: [
          //       Expanded(
          //         child: Text(
          //           'join_sehat_free'.tr(),
          //           style: TextStyle(
          //             fontWeight: FontWeight.bold,
          //             fontSize: 20,
          //           ),
          //         ),
          //       ),
          //       SizedBox(width: 30),
          //       Image.asset(
          //         'assets/images/sehat/whatsdocLogo.png',
          //       ),
          //     ],
          //   ),
          // ),

          // SizedBox(height: 10),

          // Container(
          //   width: double.infinity,
          //   margin: EdgeInsets.symmetric(horizontal: 30),
          //   padding: EdgeInsets.symmetric(
          //     horizontal: 15,
          //     vertical: 10,
          //   ),
          //   decoration: BoxDecoration(
          //     color: HexColor('#E2F4FF'),
          //     border: Border.all(
          //       color: HexColor('#2FABE2'),
          //     ),
          //     borderRadius: BorderRadius.circular(10),
          //   ),
          //   child: Row(
          //     children: [
          //       Expanded(
          //         child: Column(
          //           crossAxisAlignment: CrossAxisAlignment.start,
          //           children: [
          //             Text('organization_code'.tr()),
          //             SizedBox(
          //               height: 5,
          //             ),
          //             Text(
          //               'sehat'.toUpperCase(),
          //               style: TextStyle(
          //                 color: HexColor('#2FABE2'),
          //                 fontSize: width * 0.04,
          //                 fontWeight: FontWeight.bold,
          //               ),
          //             )
          //           ],
          //         ),
          //       ),
          //       IconButton(
          //         icon: Icon(
          //           Icons.copy,
          //           color: HexColor('#2FABE2'),
          //         ),
          //         onPressed: () {
          //           Clipboard.setData(
          //                   new ClipboardData(text: 'sehat'.toUpperCase()))
          //               .then((_) {
          //             Scaffold.of(context).showSnackBar(
          //               SnackBar(
          //                 content: Text('code_copied'.tr()),
          //               ),
          //             );
          //           });
          //         },
          //       )
          //     ],
          //   ),
          // ),

          // SizedBox(height: 20),

          // Container(
          //   width: double.infinity,
          //   margin: EdgeInsets.symmetric(horizontal: 30),
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceAround,
          //     children: [
          //       InkWell(
          //         onTap: () {
          //           launch(
          //               'https://play.google.com/store/apps/details?id=com.whatsdoc.app');
          //         },
          //         child: Image(
          //           width: width * 0.35,
          //           image: AssetImage(
          //             'assets/images/sehat/playstorebutton.png',
          //           ),
          //         ),
          //       ),
          //       SizedBox(width: 10),
          //       InkWell(
          //         onTap: () {
          //           launch(
          //               'https://apps.apple.com/sg/app/whatsdoc-healthcare-for-all/id1529733122');
          //         },
          //         child: Image(
          //           width: width * 0.35,
          //           image: AssetImage(
          //             'assets/images/sehat/appstorebutton.png',
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          // Container(
          //   padding: EdgeInsets.all(10),
          //   child: RichText(
          //     text: TextSpan(
          //       children: [
          //         TextSpan(
          //           text: 'user_guide_sehat'.tr(),
          //           recognizer: TapGestureRecognizer()
          //             ..onTap = () {
          //               launch(
          //                   'https://app.selangkah.my/cache/WDMY_User_Guide_for_SEHAT_(User)_190821.pdf');
          //             },
          //           style: TextStyle(
          //             color: Color(0xFF2A3A8F),
          //             decoration: TextDecoration.underline,
          //           ),
          //         ),
          //       ],
          //     ),
          //   ),
          // ),
        ],
      ),
    );

    // return Stack(
    //   children: [
    //     BgWhatsdoc(),
    //     Container(
    //       height: height - AppBar().preferredSize.height,
    //       width: width,
    //       child: Column(
    //         children: [
    //           Container(
    //             margin: EdgeInsets.only(
    //               right: 28,
    //               top: 28,
    //               left: 28,
    //             ),
    //             alignment: Alignment.centerRight,
    //             child: Row(
    //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //               children: [
    //                 Column(
    //                   children: [
    //                     GestureDetector(
    //                       onTap: () {
    //                         Alert(
    //                           title: 'Disclaimer',
    //                           context: context,
    //                           desc: 'akan kene cas ye boss',
    //                           buttons: [
    //                             DialogButton(
    //                               onPressed: () {
    //                                 launch('tel:+60177565622');
    //                               },
    //                               child: Text(
    //                                 'Okay',
    //                                 style: TextStyle(
    //                                   color: Colors.white,
    //                                   fontWeight: FontWeight.bold,
    //                                   fontSize: 17,
    //                                 ),
    //                               ),
    //                             ),
    //                           ],
    //                         ).show();
    //                       },
    //                       child: Row(
    //                         children: [
    //                           Icon(Icons.phone),
    //                           SizedBox(width: 10),
    //                           Text(
    //                             '+60177565622',
    //                             style: TextStyle(
    //                               color: Colors.blue,
    //                               decoration: TextDecoration.underline,
    //                             ),
    //                           ),
    //                         ],
    //                       ),
    //                     ),
    //                     SizedBox(height: 10),
    //                     GestureDetector(
    //                       onTap: () {
    //                         Alert(
    //                           title: 'Disclaimer',
    //                           context: context,
    //                           desc: 'akan kene cas ye boss',
    //                           buttons: [
    //                             DialogButton(
    //                               onPressed: () {
    //                                 launch('tel:+60163247412');
    //                               },
    //                               child: Text(
    //                                 'Okay',
    //                                 style: TextStyle(
    //                                   color: Colors.white,
    //                                   fontWeight: FontWeight.bold,
    //                                   fontSize: 17,
    //                                 ),
    //                               ),
    //                             ),
    //                           ],
    //                         ).show();
    //                       },
    //                       child: Row(
    //                         children: [
    //                           Icon(Icons.phone),
    //                           SizedBox(width: 10),
    //                           Text(
    //                             '+60163247412',
    //                             style: TextStyle(
    //                               color: Colors.blue,
    //                               decoration: TextDecoration.underline,
    //                             ),
    //                           ),
    //                         ],
    //                       ),
    //                     ),
    //                   ],
    //                 ),
    //                 Image(
    //                   image: AssetImage(
    //                     'assets/images/sehat/whatsdocLogo.png',
    //                   ),
    //                 ),
    //               ],
    //             ),
    //           ),
    //           SizedBox(
    //             height: 25,
    //           ),
    //           Container(
    //             alignment: Alignment.center,
    //             width: width * 0.8,
    //             child: AutoSizeText(
    //               'Get Answers. Get Treatment. Get Well.',
    //               style: TextStyle(
    //                 color: HexColor('#56A8DC'),
    //                 fontSize: width * 0.05,
    //                 fontWeight: FontWeight.bold,
    //               ),
    //               maxLines: 1,
    //             ),
    //           ),
    //           SizedBox(
    //             height: 20,
    //           ),
    //           Spacer(),
    //           Container(
    //             // width: width * 0.7,
    //             child: Row(
    //               mainAxisAlignment: MainAxisAlignment.center,
    //               children: [
    //                 InkWell(
    //                   onTap: () {
    //                     launch(
    //                         'https://play.google.com/store/apps/details?id=com.whatsdoc.app');
    //                   },
    //                   child: Image(
    //                     image: AssetImage(
    //                       'assets/images/sehat/playstorebutton.png',
    //                     ),
    //                   ),
    //                 ),
    //                 SizedBox(width: 10),
    //                 InkWell(
    //                   onTap: () {
    //                     launch(
    //                         'https://apps.apple.com/sg/app/whatsdoc-healthcare-for-all/id1529733122');
    //                   },
    //                   child: Image(
    //                     image: AssetImage(
    //                       'assets/images/sehat/appstorebutton.png',
    //                     ),
    //                   ),
    //                 ),
    //               ],
    //             ),
    //           ),
    //           SizedBox(
    //             height: 20,
    //           ),
    //           Container(
    //               width: width * 0.7,
    //               padding: EdgeInsets.symmetric(
    //                 horizontal: 15,
    //                 vertical: 10,
    //               ),
    //               decoration: BoxDecoration(
    //                 color: HexColor('#E2F4FF'),
    //                 border: Border.all(
    //                   color: HexColor('#2FABE2'),
    //                 ),
    //                 borderRadius: BorderRadius.circular(10),
    //               ),
    //               child: Row(
    //                 children: [
    //                   Expanded(
    //                     child: Column(
    //                       crossAxisAlignment: CrossAxisAlignment.start,
    //                       children: [
    //                         Text('Organization Code'),
    //                         SizedBox(
    //                           height: 5,
    //                         ),
    //                         Text(
    //                           'sehat'.toUpperCase(),
    //                           style: TextStyle(
    //                             color: HexColor('#2FABE2'),
    //                             fontSize: width * 0.04,
    //                             fontWeight: FontWeight.bold,
    //                           ),
    //                         )
    //                       ],
    //                     ),
    //                   ),
    //                   IconButton(
    //                     icon: Icon(
    //                       Icons.copy,
    //                       color: HexColor('#2FABE2'),
    //                     ),
    //                     onPressed: () {
    //                       Clipboard.setData(new ClipboardData(
    //                               text: 'sehat'.toUpperCase()))
    //                           .then((_) {
    //                         Scaffold.of(context).showSnackBar(
    //                           SnackBar(
    //                             content: Text('code_copied'.tr()),
    //                           ),
    //                         );
    //                       });
    //                     },
    //                   )
    //                 ],
    //               )),
    //           Container(
    //             padding: EdgeInsets.all(10),
    //             child: RichText(
    //               text: TextSpan(
    //                 children: [
    //                   TextSpan(
    //                     text: 'user_guide_sehat'.tr(),
    //                     recognizer: TapGestureRecognizer()
    //                       ..onTap = () {
    //                         launch(
    //                             'https://app.selangkah.my/cache/WDMY_User_Guide_for_SEHAT_(User)_190821.pdf');
    //                       },
    //                     style: TextStyle(
    //                       color: Color(0xFFfc4a1a),
    //                       decoration: TextDecoration.underline,
    //                     ),
    //                   ),
    //                 ],
    //               ),
    //             ),
    //           ),
    //           SizedBox(
    //             height: (height - AppBar().preferredSize.height) * 0.03,
    //           ),
    //         ],
    //       ),
    //     )
    //   ],
    // );
  }
}

class PhoneButton extends StatelessWidget {
  final String phone;
  final String line;

  const PhoneButton({Key? key, required this.phone, required this.line})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 30),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: Color(0xFF2A3A91),
          padding: EdgeInsets.symmetric(vertical: 10),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
        onPressed: () {
          Alert(
            title: 'disclaimer'.tr(),
            context: context,
            desc: 'disclaimer_description'.tr(),
            buttons: [
              DialogButton(
                onPressed: () {
                  if (line == '1') {
                    GlobalFunction.screenJourney('47');
                  } else {
                    GlobalFunction.screenJourney('48');
                  }
                  // launch('tel:$phone');
                  launchUrl(
                    Uri.parse('tel:$phone'),
                    mode: LaunchMode.externalApplication,
                  );
                },
                child: Text(
                  'Okay',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                  ),
                ),
              ),
            ],
          ).show();
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.phone),
            SizedBox(width: 10),
            Text(
              'call_now_line'.tr() + ' $line',
              style: TextStyle(
                fontSize: 17,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BgWhatsdoc extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Container(
      height: height - AppBar().preferredSize.height,
      width: width,
      child: Stack(
        children: [
          Image(
            width: width,
            fit: BoxFit.fitWidth,
            image: AssetImage(
              'assets/images/sehat/Group.png',
            ),
          ),
          Positioned(
            bottom: (height - AppBar().preferredSize.height) * 0.2,
            child: Image(
              width: width,
              image: AssetImage(
                'assets/images/sehat/bgImageWhatsdoc.png',
              ),
              fit: BoxFit.fitWidth,
            ),
          ),
          Positioned(
            bottom: -((height - AppBar().preferredSize.height) * 0.07),
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                Image(
                  width: width,
                  image: AssetImage(
                    'assets/images/sehat/bottomShape.png',
                  ),
                  fit: BoxFit.fitWidth,
                ),
                Positioned(
                  top: -(height * 0.145),
                  // bottom: (height - AppBar().preferredSize.height) * 0.4,
                  right: 20,
                  child: Image(
                    height: 150,
                    image: AssetImage(
                      'assets/images/sehat/Screen.png',
                    ),
                    fit: BoxFit.fitHeight,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
