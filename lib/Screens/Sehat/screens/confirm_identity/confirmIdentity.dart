import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:flutter/gestures.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Sehat/screens/components/components.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';
import 'package:selangkah_new/utils/constants.dart';

import 'package:url_launcher/url_launcher.dart';

class ConfirmIdentity extends StatefulWidget {
  final String language;

  const ConfirmIdentity({Key? key, required this.language}) : super(key: key);

  @override
  _ConfirmIdentityState createState() => _ConfirmIdentityState();
}

class _ConfirmIdentityState extends State<ConfirmIdentity> {
  @override
  void initState() {
    context.read<GetIdentityCubit>().getIdentity();
    context.read<GetContentCubit>().getContent('1', widget.language, true);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
        TextEditingController().clear();
      },
      child: SehatScaffold(
        appBarTitle: 'confirm_your_identity'.tr(),
        child: Container(
          height: height - AppBar().preferredSize.height,
          width: width,
          child: BlocConsumer<GetIdentityCubit, GetIdentityState>(
            listener: (context, state) {},
            builder: (context, state) {
              if (state is GetIdentityLoaded) {
                return IdentityLoadedWidget(
                  name: state.name,
                  phoneNumber: state.phoneNumber,
                  age: state.age,
                  gender: state.gender,
                  religion: state.religion,
                  district: state.district,
                  income: state.income,
                  education: state.education,
                  workStatus: state.workStatus,
                  maritalStatus: state.maritalStatus,
                  houseHold: state.houseHold,
                  dob: state.dob,
                );

                //loading
              } else {
                return SpinKitFadingCircle(
                  color: kPrimaryColor,
                  size: 30,
                );
              }
            },
          ),
        ),
      ),
    );
  }
}

class IdentityLoadedWidget extends StatefulWidget {
  final String? name;
  final String? phoneNumber;
  final String? age;
  final String? gender;
  final String? religion;
  final String? district;
  final String? income;
  final String? education;
  final String? workStatus;
  final String? maritalStatus;
  final String? houseHold;
  final DateTime? dob;

  const IdentityLoadedWidget({
    Key? key,
    this.name,
    this.phoneNumber,
    this.age,
    this.gender,
    this.religion,
    this.district,
    this.income,
    this.education,
    this.workStatus,
    this.maritalStatus,
    this.houseHold,
    this.dob,
  }) : super(key: key);

  @override
  _IdentityLoadedWidgetState createState() => _IdentityLoadedWidgetState();
}

class _IdentityLoadedWidgetState extends State<IdentityLoadedWidget> {
  bool isChecked = false;
  bool showDialog = false;

  List<String> _workStatusList = [
    'please_choose'.tr(),
    'working_fulltime'.tr(),
    'working_parttime'.tr(),
    'waiting_for_job'.tr(),
  ];

  List<String> _maritalStatusList = [
    'please_choose'.tr(),
    'single'.tr(),
    'married'.tr(),
    'divorced'.tr(),
    'widow'.tr(),
    'widower'.tr(),
    'separated'.tr(),
  ];

  List<String> _gender = [
    'please_choose'.tr(),
    'male'.tr(),
    'female'.tr(),
  ];
  List<String> _religion = [
    'please_choose'.tr(),
    'muslim'.tr(),
    'christian'.tr(),
    'buddha'.tr(),
    'hindu'.tr(),
    'others'.tr(),
  ];

  List<String> _district = [
    'please_choose'.tr(),
    'GOMBAK',
    'HULU LANGAT',
    'HULU SELANGOR',
    'KLANG',
    'KUALA LANGAT',
    'KUALA SELANGOR',
    'PETALING',
    'SABAK BERNAM',
    'SELAYANG',
    'SEPANG',
    'non_selangor'.tr()
  ];

  List<String> _monthlyIncome = [
    'please_choose'.tr(),
    'no_income'.tr(),
    '< 1000',
    '1001-5000',
    '5001-10000',
    '> 10000',
  ];
  List<String> _education = [
    'please_choose'.tr(),
    'no_education'.tr(),
    'primary'.tr(),
    'secondary'.tr(),
    'college'.tr(),
    'university'.tr(),
  ];

  //var
  TextEditingController name = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController age = TextEditingController();
  TextEditingController houseHold = TextEditingController();

  String? gender,
      religion,
      district,
      income,
      education,
      workStatus,
      maritalStatus = '';
  DateTime? dob;

  @override
  void initState() {
    setData();
    setDistrict();
    super.initState();
  }

  // IF DISTRICT NON SELANGOR
  void setDistrict() {
    if (district == 'NON SELANGOR' || district == 'LUAR SELANGOR') {
      if (district != _district.last) {
        if (district == 'NON SELANGOR') {
          setState(() {
            district = 'LUAR SELANGOR';
            _district.last = 'LUAR SELANGOR';
          });
        } else {
          setState(() {
            district = 'NON SELANGOR';
            _district.last = 'NON SELANGOR';
          });
        }
      }
    }
  }

  void setData() async {
    name.text = widget.name ?? '';
    phone.text = widget.phoneNumber ?? '';
    age.text = widget.age ?? '';
    houseHold.text = widget.houseHold ?? '';

    gender = widget.gender ?? 'please_choose'.tr();
    religion = widget.religion ?? 'please_choose'.tr();
    district = widget.district ?? 'please_choose'.tr();
    income = widget.income ?? 'please_choose'.tr();
    education = widget.education ?? 'please_choose'.tr();

    workStatus = widget.workStatus ?? 'please_choose'.tr();
    maritalStatus = widget.maritalStatus ?? 'please_choose'.tr();

    dob = widget.dob;

    //check list
    //check gender list
    if (!_gender.contains(gender)) {
      gender = 'please_choose'.tr();
    }

    //check religion list
    if (!_religion.contains(religion)) {
      religion = 'please_choose'.tr();
    }

    //check district list
    if (!_district.contains(district)) {
      district = 'please_choose'.tr();
    }

    //check income list
    if (!_monthlyIncome.contains(income)) {
      income = 'please_choose'.tr();
    }

    //check education list
    if (!_education.contains(education)) {
      education = 'please_choose'.tr();
    }

    //check work list
    if (!_workStatusList.contains(workStatus)) {
      workStatus = 'please_choose'.tr();
    }

    //check marital list
    if (!_maritalStatusList.contains(maritalStatus)) {
      maritalStatus = 'please_choose'.tr();
    }
  }

  final _formKey = GlobalKey<FormState>();

  String? checkValidator(String? value) {
    if (value == 'please_choose'.tr() || value == '' || value == null) {
      return 'incomplete_field'.tr();
    } else {
      return null;
    }
  }

  int answer1 = 0;
  int answer2 = 0;

  setContentAnswer(int answer1New, int answer2New) {
    setState(() {
      answer1 = answer1New;
      answer2 = answer2New;
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Stack(
      children: [
        SingleChildScrollView(
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: Container(
                  width: double.infinity,
                  margin: EdgeInsets.all(20),
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(
                      color: Color(0xFF438C8B),
                      width: 3,
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 3,
                        offset: Offset(0, 5), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Column(
                    children: [
                      Text(
                        'confirm_your_identity'.tr(),
                        style: TextStyle(
                          fontSize: width * 0.05,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      ListIdentity(
                        text: 'name'.tr() + ':',
                        child: TextField(
                          readOnly: true,
                          controller: name,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            isDense: true,
                          ),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 13,
                          ),
                          onChanged: (value) {
                            name.text = value;
                          },
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      ListIdentity(
                        text: 'phonenumber'.tr() + ':',
                        child: TextField(
                          readOnly: true,
                          controller: phone,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            isDense: true,
                          ),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 13,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: [
                          customLeading(width, 'dob'.tr()),
                          SizedBox(width: 10),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                horizontal: 10,
                                vertical: 5,
                              ),
                              decoration: BoxDecoration(
                                color: themeColor.withOpacity(0.2),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Row(
                                children: [
                                  if (dob != null)
                                    Text(
                                      formatDate(
                                        dob!,
                                        [dd, '-', mm, '-', yyyy],
                                      ),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  Spacer(),
                                  GestureDetector(
                                    onTap: () async {
                                      DateTime? picked = await showDatePicker(
                                        context: context,
                                        initialDate: DateTime.now(),
                                        firstDate: DateTime(1920),
                                        lastDate: DateTime.now(),
                                      );

                                      if (picked != null && picked != dob)
                                        setState(() {
                                          dob = picked;
                                        });
                                    },
                                    child: Icon(
                                      FontAwesomeIcons.calendarDay,
                                      color: themeColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          // Expanded(
                          //   child: Container(
                          //     padding: EdgeInsets.symmetric(
                          //       horizontal: 10,
                          //       vertical: 5,
                          //     ),
                          //     decoration: BoxDecoration(
                          //       color: themeColor.withOpacity(0.2),
                          //       borderRadius: BorderRadius.circular(5),
                          //     ),
                          //     child: TextFormField(
                          //       controller: age,
                          //       decoration: InputDecoration(
                          //         border: InputBorder.none,
                          //         isDense: true,
                          //       ),
                          //       style: TextStyle(
                          //         fontWeight: FontWeight.bold,
                          //         fontSize: 13,
                          //       ),
                          //       validator: checkValidator,
                          //       keyboardType: TextInputType.number,
                          //     ),
                          //   ),
                          // )
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        children: [
                          customLeading(width, 'gender'.tr()),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Stack(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 10,
                                    // vertical: 3,
                                  ),
                                  decoration: BoxDecoration(
                                    color: themeColor.withOpacity(0.2),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: DropdownButtonFormField<String>(
                                    validator: checkValidator,
                                    value: gender,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 13,
                                    ),
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(0),
                                      hintText: 'please_choose'.tr(),
                                      hintStyle: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 13,
                                      ),
                                      border: InputBorder.none,
                                    ),
                                    icon: Icon(
                                      Icons.arrow_drop_down_circle_rounded,
                                      color: themeColor,
                                    ),
                                    isDense: true,
                                    items: _gender
                                        .map((label) => DropdownMenuItem(
                                              child: Text(label),
                                              value: label,
                                            ))
                                        .toList(),
                                    onChanged: (value) {
                                      setState(
                                        () => gender = value,
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        children: [
                          customLeading(width, 'religion'.tr()),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Stack(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 10,
                                    // vertical: 3,
                                  ),
                                  decoration: BoxDecoration(
                                    color: themeColor.withOpacity(0.2),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: DropdownButtonFormField<String>(
                                    validator: checkValidator,
                                    value: religion,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 13,
                                    ),
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(0),
                                      hintText: 'please_choose'.tr(),
                                      hintStyle: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 13,
                                      ),
                                      border: InputBorder.none,
                                    ),
                                    icon: Icon(
                                      Icons.arrow_drop_down_circle_rounded,
                                      color: themeColor,
                                    ),
                                    isDense: true,
                                    items: _religion
                                        .map((label) => DropdownMenuItem(
                                              child: Text(label),
                                              value: label,
                                            ))
                                        .toList(),
                                    onChanged: (value) {
                                      setState(() => religion = value);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        children: [
                          customLeading(width, 'district'.tr()),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Stack(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 10,
                                    // vertical: 3,
                                  ),
                                  decoration: BoxDecoration(
                                    color: themeColor.withOpacity(0.2),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: DropdownButtonFormField<String>(
                                    validator: checkValidator,
                                    value: district,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 13,
                                    ),
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(0),
                                      hintText: 'please_choose'.tr(),
                                      hintStyle: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 13,
                                      ),
                                      border: InputBorder.none,
                                    ),
                                    icon: Icon(
                                      Icons.arrow_drop_down_circle_rounded,
                                      color: themeColor,
                                    ),
                                    isDense: true,
                                    items: _district
                                        .map((label) => DropdownMenuItem(
                                              child: Text(label),
                                              value: label,
                                            ))
                                        .toList(),
                                    onChanged: (value) {
                                      setState(() => district = value);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        children: [
                          customLeading(width, 'monthly_income'.tr()),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Stack(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 10,
                                    // vertical: 3,
                                  ),
                                  decoration: BoxDecoration(
                                    color: themeColor.withOpacity(0.2),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: DropdownButtonFormField<String>(
                                    validator: checkValidator,
                                    value: income,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 13,
                                    ),
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(0),
                                      hintText: 'please_choose'.tr(),
                                      hintStyle: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 13,
                                      ),
                                      border: InputBorder.none,
                                    ),
                                    icon: Icon(
                                      Icons.arrow_drop_down_circle_rounded,
                                      color: themeColor,
                                    ),
                                    isDense: true,
                                    items: _monthlyIncome
                                        .map((label) => DropdownMenuItem(
                                              child: Text(label),
                                              value: label,
                                            ))
                                        .toList(),
                                    onChanged: (value) {
                                      setState(() => income = value);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        children: [
                          customLeading(width, 'education'.tr()),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Stack(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 10,
                                    // vertical: 3,
                                  ),
                                  decoration: BoxDecoration(
                                    color: themeColor.withOpacity(0.2),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: DropdownButtonFormField<String>(
                                    validator: checkValidator,
                                    value: education,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 13,
                                    ),
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(0),
                                      hintText: 'please_choose'.tr(),
                                      hintStyle: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 13,
                                      ),
                                      border: InputBorder.none,
                                    ),
                                    icon: Icon(
                                      Icons.arrow_drop_down_circle_rounded,
                                      color: themeColor,
                                    ),
                                    isDense: true,
                                    items: _education
                                        .map((label) => DropdownMenuItem(
                                              child: Text(label),
                                              value: label,
                                            ))
                                        .toList(),
                                    onChanged: (value) {
                                      setState(() => education = value);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),

                      //new field
                      SizedBox(height: 15),

                      Row(
                        children: [
                          customLeading(width, 'work_status'.tr()),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Stack(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 10,
                                    // vertical: 3,
                                  ),
                                  decoration: BoxDecoration(
                                    color: themeColor.withOpacity(0.2),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: DropdownButtonFormField<String>(
                                    validator: checkValidator,
                                    value: workStatus,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 13,
                                    ),
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(0),
                                      hintText: 'please_choose'.tr(),
                                      hintStyle: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 13,
                                      ),
                                      border: InputBorder.none,
                                    ),
                                    icon: Icon(
                                      Icons.arrow_drop_down_circle_rounded,
                                      color: themeColor,
                                    ),
                                    isDense: true,
                                    items: _workStatusList
                                        .map((label) => DropdownMenuItem(
                                              child: Text(label),
                                              value: label,
                                            ))
                                        .toList(),
                                    onChanged: (value) {
                                      setState(() => workStatus = value);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),

                      SizedBox(height: 15),

                      //marital
                      Row(
                        children: [
                          customLeading(width, 'marital_status'.tr()),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Stack(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 10,
                                    // vertical: 3,
                                  ),
                                  decoration: BoxDecoration(
                                    color: themeColor.withOpacity(0.2),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: DropdownButtonFormField<String>(
                                    validator: checkValidator,
                                    value: maritalStatus,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 13,
                                    ),
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(0),
                                      hintText: 'please_choose'.tr(),
                                      hintStyle: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 13,
                                      ),
                                      border: InputBorder.none,
                                    ),
                                    icon: Icon(
                                      Icons.arrow_drop_down_circle_rounded,
                                      color: themeColor,
                                    ),
                                    isDense: true,
                                    items: _maritalStatusList
                                        .map((label) => DropdownMenuItem(
                                              child: Text(label),
                                              value: label,
                                            ))
                                        .toList(),
                                    onChanged: (value) {
                                      setState(() => maritalStatus = value);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),

                      SizedBox(height: 20),
                      Text(
                        'household_text'.tr(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 10),

                      Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 5,
                        ),
                        decoration: BoxDecoration(
                          color: themeColor.withOpacity(0.2),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: TextFormField(
                          controller: houseHold,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            isDense: true,
                          ),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 13,
                          ),
                          validator: checkValidator,
                          keyboardType: TextInputType.number,
                        ),
                      ),

                      SizedBox(height: 15),

                      //content
                      BlocBuilder<GetContentCubit, GetContentState>(
                        builder: (context, state) {
                          if (state is GetContentLoading) {
                            return Container();
                          } else if (state is GetContentLoaded) {
                            return GetContentIdentity(
                              content: state.content,
                              updateParent: setContentAnswer,
                            );
                          } else {
                            return SpinKitFadingCircle(
                              color: kPrimaryColor,
                              size: 15,
                            );
                          }
                        },
                      ),

                      SizedBox(height: 25),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Checkbox(
                            activeColor: themeColor,
                            checkColor: Colors.white,
                            value: isChecked,
                            onChanged: (bool? value) {
                              setState(() {
                                isChecked = value!;
                              });
                            },
                          ),
                          SizedBox(height: 10),
                          Container(
                            width: width * 0.6,
                            child: RichText(
                              text: TextSpan(
                                text: 'sehat_pdpa_text'.tr(),
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                                children: [
                                  TextSpan(
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        // launch(
                                        //     'https://app.selangkah.my/cache/PDPA_SEHAT.pdf');
                                        launchUrl(
                                          Uri.parse(
                                              'https://app.selangkah.my/cache/PDPA_SEHAT.pdf'),
                                          mode: LaunchMode.externalApplication,
                                        );
                                      },
                                    text: 'PDPA',
                                    style: TextStyle(
                                      color: themeColor,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  TextSpan(
                                    text: ')',
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 20),
                    ],
                  ),
                ),
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(
                    horizontal: 25,
                  ),
                  backgroundColor: isChecked ? themeColor : Colors.grey,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
                onPressed: () {
                  // print(answer1);
                  // print(answer2);

                  if (isChecked) {
                    if (_formKey.currentState!.validate()) {
                      // No any error in validation
                      context.read<SendIdentityCubit>().sendIdentity(
                            age: age.text,
                            gender: gender,
                            religion: religion,
                            district: district,
                            income: income,
                            education: education,
                            workStatus: workStatus,
                            maritalStatus: maritalStatus,
                            houseHold: houseHold.text,
                            dob: dob,
                          );

                      //set answer

                      //question 1
                      SehatRepositories.setAnswer(
                        answer: '$answer1',
                        contentType: '1',
                        idMentalContent: '107',
                        idQuestion: '93',
                        idCategory: '',
                      );

                      //question 2
                      SehatRepositories.setAnswer(
                        answer: '$answer2',
                        contentType: '1',
                        idMentalContent: '108',
                        idQuestion: '94',
                        idCategory: '',
                      );
                    }
                  }
                },
                child: Container(
                  width: width * 0.25,
                  height: 15,
                  child: Center(
                    child: BlocConsumer<SendIdentityCubit, SendIdentityState>(
                      listener: (context, state) {
                        if (state is SendIdentityLoaded) {
                          setState(() {
                            showDialog = true;
                          });
                        }
                      },
                      builder: (context, state) {
                        if (state is SendIdentityInitial) {
                          return Text(
                            'confirm_otp'.tr(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 13,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          );
                        } else {
                          return SpinKitFadingCircle(
                            color: Colors.white,
                            size: 15,
                          );
                        }
                      },
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
        Visibility(
          visible: showDialog,
          child: buildSuccessDialog(
            height,
            width,
            '',
            'your_identity_confirmed'.tr().toUpperCase(),
            '',
            '',
            () {
              Navigator.of(context).pop();
            },
            'confirmed'.tr().toUpperCase(),
          ),
        ),
      ],
    );
  }
}

class GetContentIdentity extends StatefulWidget {
  final Content content;
  final Function(int answer1, int answer2) updateParent;

  const GetContentIdentity(
      {Key? key, required this.content, required this.updateParent})
      : super(key: key);

  @override
  _GetContentIdentityState createState() => _GetContentIdentityState();
}

class _GetContentIdentityState extends State<GetContentIdentity> {
  int answer1 = 0;
  int answer2 = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.content.data![0].card![0].data!.questionText!,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 13,
          ),
          textAlign: TextAlign.start,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Radio(
              activeColor: themeColor,
              value: 1,
              groupValue: answer1,
              onChanged: (val) {
                setState(() {
                  answer1 = val!;
                  // print('hooo $val');
                });
                widget.updateParent(answer1, answer2);
              },
            ),
            Text(
                widget.content.data![0].card![0].data!.choice![0].answerValue!),
            Radio(
              activeColor: themeColor,
              value: 0,
              groupValue: answer1,
              onChanged: (val) {
                setState(() {
                  answer1 = val!;
                  // print('hooo $val');
                });
                widget.updateParent(answer1, answer2);
              },
            ),
            Text(
                widget.content.data![0].card![0].data!.choice![1].answerValue!),
          ],
        ),

        SizedBox(height: 10),

        //question 2
        Text(
          widget.content.data![0].card![1].data!.questionText!,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 13,
          ),
          textAlign: TextAlign.start,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Radio(
              activeColor: themeColor,
              value: 1,
              groupValue: answer2,
              onChanged: (val) {
                setState(() {
                  answer2 = val!;
                  // print('hooo $val');
                });
                widget.updateParent(answer1, answer2);
              },
            ),
            Text(
                widget.content.data![0].card![1].data!.choice![0].answerValue!),
            Radio(
              activeColor: themeColor,
              value: 0,
              groupValue: answer2,
              onChanged: (val) {
                setState(() {
                  answer2 = val!;
                  // print('hooo $val');
                });
                widget.updateParent(answer1, answer2);
              },
            ),
            Text(
                widget.content.data![0].card![1].data!.choice![1].answerValue!),
          ],
        ),
      ],
    );
  }
}

class ListIdentity extends StatelessWidget {
  final String text;
  final Widget child;

  const ListIdentity({Key? key, required this.text, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height -
    //     AppBar().preferredSize.height -
    //     MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Row(
      children: [
        Container(
          width: width * 0.25,
          child: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 13,
            ),
          ),
        ),
        SizedBox(width: 10),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 5,
            ),
            decoration: BoxDecoration(
              color: Colors.grey[200]!.withOpacity(0.5),
              borderRadius: BorderRadius.circular(5),
            ),
            child: child,
          ),
        )
      ],
    );
  }
}
