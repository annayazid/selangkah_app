import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Sehat/screens/components/components.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';

class StressScreeningTemplate extends StatefulWidget {
  StressScreeningTemplate({Key? key}) : super(key: key);

  @override
  _StressScreeningTemplateState createState() =>
      _StressScreeningTemplateState();
}

class _StressScreeningTemplateState extends State<StressScreeningTemplate> {
  double _value1 = 0.0;
  String answer1 = 'test answer';
  int _currentIndex = 0;
  bool showDialog = false;

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return SehatScaffold(
      appBarTitle: 'test title',
      child: Container(
        height: height - AppBar().preferredSize.height,
        width: width,
        child: Stack(
          children: [
            Container(
              width: width,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      width: width * 0.9,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            width: width * 0.175,
                            height: 7,
                            decoration: BoxDecoration(color: themeColor),
                          ),
                          Container(
                            width: width * 0.175,
                            height: 7,
                            decoration:
                                BoxDecoration(color: indicatorUnselected),
                          ),
                          Container(
                            width: width * 0.175,
                            height: 7,
                            decoration:
                                BoxDecoration(color: indicatorUnselected),
                          ),
                          Container(
                            width: width * 0.175,
                            height: 7,
                            decoration:
                                BoxDecoration(color: indicatorUnselected),
                          ),
                          Container(
                            width: width * 0.15,
                            height: 7,
                            decoration:
                                BoxDecoration(color: indicatorUnselected),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    UnconstrainedBox(
                      child: Container(
                        // width: width * 0.6,
                        padding: EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 10,
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 2.5,
                            color: Colors.grey[600]!,
                          ),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          'test page title'.toUpperCase(),
                          style: TextStyle(
                            fontSize: width * 0.045,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[600],
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    buildQuestion(
                      width,
                      SliderTheme(
                        data: SliderTheme.of(context).copyWith(
                          activeTrackColor: Colors.white.withOpacity(0.5),
                          inactiveTrackColor: Colors.white.withOpacity(0.5),
                          trackShape: RoundedRectSliderTrackShape(),
                          trackHeight: 8,
                          thumbShape:
                              RoundSliderThumbShape(enabledThumbRadius: 12.0),
                          thumbColor: Colors.white,
                          overlayColor: Colors.white.withAlpha(32),
                          overlayShape:
                              RoundSliderOverlayShape(overlayRadius: 28.0),
                          // tickMarkShape: RoundSliderTickMarkShape(),
                          activeTickMarkColor: Colors.transparent,
                          inactiveTickMarkColor: Colors.transparent,
                          // valueIndicatorShape: PaddleSliderValueIndicatorShape(),
                          // valueIndicatorColor: Colors.white,
                          // valueIndicatorTextStyle: TextStyle(
                          //   color: Colors.white,
                          // ),
                        ),
                        child: Slider(
                          value: _value1,
                          min: 0,
                          max: 4,
                          divisions: 4,
                          onChanged: (value) {
                            setState(
                              () {
                                _value1 = value;
                              },
                            );
                          },
                        ),
                      ),
                      _value1,
                      '01',
                      '10',
                      answer1,
                      'test question',
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: width * 0.85,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Visibility(
                            visible: (_currentIndex == 0 || _currentIndex == 1)
                                ? false
                                : true,
                            child: Container(
                              decoration: BoxDecoration(
                                color: themeColor,
                                shape: BoxShape.circle,
                              ),
                              child: IconButton(
                                icon: Icon(
                                  Icons.arrow_back,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  setState(() {
                                    _currentIndex--;
                                  });
                                },
                              ),
                            ),
                          ),
                          _currentIndex == 5
                              ? ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: themeColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20),
                                    ),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      showDialog = true;
                                    });
                                  },
                                  child: Text(
                                    'proceed'.tr().toString(),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: width * 0.04,
                                    ),
                                  ),
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                    color: themeColor,
                                    shape: BoxShape.circle,
                                  ),
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.arrow_forward,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        if (_currentIndex < 5) {
                                          if (_currentIndex == 1) {
                                            //for question1 and question2
                                          } else if (_currentIndex == 2) {
                                            //for question3 and question4
                                          } else if (_currentIndex == 3) {
                                            //for question5 and question6
                                          } else if (_currentIndex == 4) {
                                            //for question7 and question8
                                          } else if (_currentIndex == 5) {
                                            //for question9 and question10
                                          }
                                          _currentIndex++;
                                        }
                                      });
                                    },
                                  ),
                                )
                        ],
                      ),
                    ),
                    // Text(q1.toString()),
                    // Text(q2.toString()),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
