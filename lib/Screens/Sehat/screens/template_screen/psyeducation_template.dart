import 'package:flutter/material.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';

class PsyEducationTemplate extends StatefulWidget {
  const PsyEducationTemplate({Key? key}) : super(key: key);

  @override
  _PsyEducationTemplateState createState() => _PsyEducationTemplateState();
}

class _PsyEducationTemplateState extends State<PsyEducationTemplate> {
  bool? watch1;
  @override
  void initState() {
    super.initState();
    watch1 = false;
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return SehatScaffold(
      appBarTitle: 'Test title'.toUpperCase(),
      child: Container(
        height: height - AppBar().preferredSize.height,
        width: width,
        child: Stack(
          children: [
            Container(
              width: width,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      width: width * 0.9,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            width: width * 0.175,
                            height: 7,
                            decoration: BoxDecoration(color: themeColor),
                          ),
                          Container(
                            width: width * 0.175,
                            height: 7,
                            decoration:
                                BoxDecoration(color: indicatorUnselected),
                          ),
                          Container(
                            width: width * 0.175,
                            height: 7,
                            decoration:
                                BoxDecoration(color: indicatorUnselected),
                          ),
                          Container(
                            width: width * 0.175,
                            height: 7,
                            decoration:
                                BoxDecoration(color: indicatorUnselected),
                          ),
                          Container(
                            width: width * 0.15,
                            height: 7,
                            decoration:
                                BoxDecoration(color: indicatorUnselected),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    UnconstrainedBox(
                      child: Container(
                        width: width * 0.6,
                        padding: EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 10,
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 2.5,
                            color: Colors.grey[600]!,
                          ),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          'test page title'.toUpperCase(),
                          style: TextStyle(
                            fontSize: width * 0.045,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: width * 0.85,
                      decoration: BoxDecoration(
                        color: themeColor,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      padding: EdgeInsets.symmetric(
                        vertical: 15,
                        horizontal: 30,
                      ),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 25,
                          ),
                          // buildRow(
                          //   width,
                          //   'question',
                          //   '1. ',
                          //   watch1 == true
                          //       ? buildWatched(width)
                          //       : buildWatch(
                          //           width,
                          //           () {
                          //             setState(() {
                          //               watch1 = true;
                          //             });
                          //           },
                          //         ),
                          // ),
                          SizedBox(
                            height: 25,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
