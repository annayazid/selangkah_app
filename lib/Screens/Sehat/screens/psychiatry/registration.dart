import 'package:date_format/date_format.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/utils/constants.dart';

class RegistrationPsychiatry extends StatefulWidget {
  final Appointment? appointment;

  const RegistrationPsychiatry({Key? key, this.appointment}) : super(key: key);

  @override
  State<RegistrationPsychiatry> createState() => _RegistrationPsychiatryState();
}

class _RegistrationPsychiatryState extends State<RegistrationPsychiatry> {
  @override
  void initState() {
    context.read<GetSehatClinicCubit>().getClinic();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SehatScaffold(
      appBarTitle: 'psychiatry_subsidy'.tr(),
      child: BlocConsumer<GetSehatClinicCubit, GetSehatClinicState>(
        listener: (context, state) {},
        builder: (context, state) {
          if (state is GetSehatClinicLoaded) {
            return SehatClinicLoaded(
                sehatClinic: state.sehatClinic,
                appointment: widget.appointment);
          } else {
            return SpinKitFadingCircle(
              color: kPrimaryColor,
              size: 20,
            );
          }
        },
      ),
    );
  }
}

class SehatClinicLoaded extends StatefulWidget {
  final SehatClinic sehatClinic;
  final Appointment? appointment;

  const SehatClinicLoaded(
      {Key? key, required this.sehatClinic, this.appointment})
      : super(key: key);

  @override
  State<SehatClinicLoaded> createState() => _SehatClinicLoadedState();
}

class _SehatClinicLoadedState extends State<SehatClinicLoaded> {
  String? selectedClinicId;
  String? selectedDateTime;

  @override
  void initState() {
    if (widget.appointment != null) {
      selectedClinicId = widget.appointment!.data!.idProvider;
      context.read<GetSehatDateCubit>().getDateTime(selectedClinicId!);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height -
    //     AppBar().preferredSize.height -
    //     MediaQuery.of(context).padding.top;

    return Column(
      children: [
        SizedBox(height: 10),
        Container(
          width: width * 0.95,
          padding: EdgeInsets.all(25),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                offset: Offset(0, 0),
                blurRadius: 5,
                // spreadRadius: 3,
              ),
            ],
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'registration'.tr(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF438C8B),
                ),
              ),
              SizedBox(height: 10),
              Divider(
                color: Colors.black,
                height: 1,
              ),
              SizedBox(height: 15),
              Text(
                'swabook_clinic'.tr(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF438C8B),
                ),
              ),
              SizedBox(height: 10),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(),
                  borderRadius: BorderRadius.circular(10),
                  color: widget.appointment != null
                      ? Color(0xFFD9D9D9)
                      : Colors.white,
                ),
                child: InputDecorator(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: 15,
                    ),
                    errorStyle:
                        TextStyle(color: Colors.redAccent, fontSize: 16.0),
                    border: InputBorder.none,
                  ),
                  child: DropdownButtonHideUnderline(
                    child: IgnorePointer(
                      ignoring: widget.appointment != null ? true : false,
                      child: DropdownButton<String>(
                        items: widget.sehatClinic.data!.map((e) {
                          return DropdownMenuItem<String>(
                            value: e.idProvider,
                            // value:widget.appointment
                            child: Text(
                              e.ppvName!,
                              style: TextStyle(
                                fontSize: 14,
                              ),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 4,
                            ),
                          );
                        }).toList(),
                        isExpanded: true,
                        icon: Icon(
                          Icons.keyboard_arrow_down,
                          size: 30,
                          color: Color(0xFF438C8B),
                        ),
                        // value: 'idChoosen',
                        hint: Text(
                          'swabook_clinic_sub'.tr(),
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        value: selectedClinicId,
                        onChanged: (String? value) {
                          setState(() {
                            selectedClinicId = value;
                          });
                          print(value);

                          selectedDateTime = null;

                          if (selectedClinicId != null) {
                            context
                                .read<GetSehatDateCubit>()
                                .getDateTime(value!);
                          }
                        },
                      ),
                    ),
                  ),
                ),
              ),
              BlocConsumer<GetSehatDateCubit, GetSehatDateState>(
                listener: (context, state) {},
                builder: (context, state) {
                  if (state is GetSehatDateLoaded) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 15),
                        Text(
                          'date'.tr(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF438C8B),
                          ),
                        ),
                        SizedBox(height: 10),
                        state.sehatDate.data!.isNotEmpty
                            ? Container(
                                decoration: BoxDecoration(
                                  border: Border.all(),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: InputDecorator(
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(
                                      horizontal: 15,
                                    ),
                                    errorStyle: TextStyle(
                                        color: Colors.redAccent,
                                        fontSize: 16.0),
                                    border: InputBorder.none,
                                  ),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      items: state.sehatDate.data!.map((e) {
                                        return DropdownMenuItem<String>(
                                          value: formatDate(e.slotDate!,
                                              [yyyy, '-', mm, '-', dd]),
                                          child: Text(
                                            formatDate(e.slotDate!,
                                                [dd, ' ', MM, ' ', yyyy]),
                                            style: TextStyle(
                                              fontSize: 14,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 4,
                                          ),
                                        );
                                      }).toList(),
                                      isExpanded: true,
                                      icon: Icon(
                                        Icons.keyboard_arrow_down,
                                        size: 30,
                                        color: Color(0xFF438C8B),
                                      ),
                                      hint: Text(
                                        'selectdate'.tr(),
                                        style: TextStyle(
                                          fontSize: 14,
                                        ),
                                      ),
                                      value: selectedDateTime,
                                      onChanged: (String? value) {
                                        setState(() {
                                          selectedDateTime = value;
                                        });
                                        print(value);
                                      },
                                    ),
                                  ),
                                ),
                              )
                            : Text(
                                'no_date'.tr(),
                              ),
                        if (selectedDateTime != null) ...[
                          SizedBox(height: 15),
                          Text(
                            'time'.tr(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF438C8B),
                            ),
                          ),
                          SizedBox(height: 15),
                          Text(
                            '${state.sehatDate.data![state.sehatDate.data!.indexWhere((element) => formatDate(element.slotDate!, [yyyy, '-', mm, '-', dd]) == selectedDateTime)].timeStart!.substring(0, 5)} - ${state.sehatDate.data![state.sehatDate.data!.indexWhere((element) => formatDate(element.slotDate!, [
                                      yyyy,
                                      '-',
                                      mm,
                                      '-',
                                      dd
                                    ]) == selectedDateTime)].timeEnd!.substring(0, 5)}',
                          )
                        ],
                      ],
                    );
                  } else if (state is GetSehatDateInitial) {
                    return Container();
                  } else {
                    return Column(
                      children: [
                        SizedBox(height: 15),
                        SpinKitFadingCircle(
                          color: kPrimaryColor,
                          size: 20,
                        ),
                      ],
                    );
                  }
                },
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.all(10),
          width: double.infinity,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor:
                  selectedClinicId != null && selectedDateTime != null
                      ? kPrimaryColor
                      : Colors.grey,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10), // <-- Radius
              ),
            ),
            onPressed: () {
              if (selectedClinicId != null && selectedDateTime != null) {
                if (widget.appointment != null) {
                  // String idProvider, String idTest, String idProgram, String date
                  context.read<SubmitCubit>().changeAppointment(
                        selectedClinicId!,
                        widget.appointment!.data!.testId!,
                        widget.appointment!.data!.idProgramAppointment!,
                        selectedDateTime!,
                      );
                } else {
                  String language =
                      EasyLocalization.of(context)!.locale.languageCode == 'en'
                          ? 'EN'
                          : 'MY';
                  context.read<SubmitCubit>().setAppointment(
                      selectedClinicId!, selectedDateTime!, language);
                }
              }
            },
            child: BlocConsumer<SubmitCubit, SubmitState>(
              listener: (context, state) {
                if (state is SubmitLoaded) {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) => BlocProvider(
                        create: (context) => CertificateCubit(),
                        child: PsychiatryCertificate(),
                      ),
                    ),
                  );
                }
              },
              builder: (context, state) {
                if (state is SubmitLoaded || state is SubmitInitial)
                  return Text(
                    'submit'.tr(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                    ),
                  );
                else {
                  return SpinKitFadingCircle(
                    color: Colors.white,
                    size: 14,
                  );
                }
              },
            ),
          ),
        ),
      ],
    );
  }
}
