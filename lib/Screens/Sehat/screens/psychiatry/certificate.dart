import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'package:easy_localization/easy_localization.dart' as ea;
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/utils/constants.dart';

class PsychiatryCertificate extends StatefulWidget {
  const PsychiatryCertificate({Key? key}) : super(key: key);

  @override
  _PsychiatryCertificateState createState() => _PsychiatryCertificateState();
}

class _PsychiatryCertificateState extends State<PsychiatryCertificate> {
  @override
  void initState() {
    super.initState();
    context.read<CertificateCubit>().getCertificateDetail();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CertificateCubit, CertificateState>(
      listener: (context, state) {},
      builder: (context, state) {
        print('state is $state');
        if (state is CertificateLoaded) {
          return CertificateLoadedWidget(
              certificate: state.certificate,
              appointment: state.appointment,
              identification: state.identification,
              scorePanel: state.scorePanel);
        } else if (state is RegistrationLoaded) {
          return RegistrationPsychiatry();
        } else {
          return SehatScaffold(
            appBarTitle: 'Psychiatry Subsidy Screening Registration',
            child: SpinKitFadingCircle(
              color: kPrimaryColor,
              size: 20,
            ),
          );
        }
      },
    );
  }
}

class WeirdBorder extends ShapeBorder {
  final double radius;
  final double pathWidth;

  WeirdBorder({required this.radius, this.pathWidth = 1});

  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.zero;

  @override
  Path getInnerPath(Rect rect, {TextDirection? textDirection}) {
    return Path()
      ..fillType = PathFillType.evenOdd
      ..addPath(getOuterPath(rect, textDirection: textDirection!), Offset.zero);
  }

  @override
  Path getOuterPath(Rect rect, {TextDirection? textDirection}) =>
      _createPath(rect);

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection? textDirection}) {}

  @override
  ShapeBorder scale(double t) => WeirdBorder(radius: radius);

  Path _createPath(Rect rect) {
    final innerRadius = radius + pathWidth;
    final innerRect = Rect.fromLTRB(rect.left + pathWidth, rect.top + pathWidth,
        rect.right - pathWidth, rect.bottom - pathWidth);

    final outer = Path.combine(PathOperation.difference, Path()..addRect(rect),
        _createBevels(rect, radius));
    final inner = Path.combine(PathOperation.difference,
        Path()..addRect(innerRect), _createBevels(rect, innerRadius));
    return Path.combine(PathOperation.difference, outer, inner);
  }

  Path _createBevels(Rect rect, double radius) {
    return Path()
      ..addOval(
          Rect.fromCircle(center: Offset(rect.left, rect.top), radius: radius))
      ..addOval(Rect.fromCircle(
          center: Offset(rect.left + rect.width, rect.top), radius: radius))
      ..addOval(Rect.fromCircle(
          center: Offset(rect.left, rect.top + rect.height), radius: radius))
      ..addOval(Rect.fromCircle(
          center: Offset(rect.left + rect.width, rect.top + rect.height),
          radius: radius));
  }
}

class CertificateLoadedWidget extends StatefulWidget {
  final Certificate certificate;
  final Appointment appointment;
  final String identification;
  final List<ScorePanel> scorePanel;
  const CertificateLoadedWidget({
    Key? key,
    required this.certificate,
    required this.identification,
    required this.scorePanel,
    required this.appointment,
  }) : super(key: key);

  @override
  _CertificateLoadedWidgetState createState() =>
      _CertificateLoadedWidgetState();
}

class _CertificateLoadedWidgetState extends State<CertificateLoadedWidget> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return SehatScaffold(
      appBarTitle: 'psychiatry_subsidy'.tr(),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              // height: 500,
              margin: EdgeInsets.all(10),
              // padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
              decoration: ShapeDecoration(
                shape: WeirdBorder(radius: 30, pathWidth: 4),
                color: Color(0xFFFFDFA8),
              ),
              // BoxDecoration(
              //   border: Border.all(
              //     width: 5,
              //     color: Color(0xFFFFDFA8),
              //   ),
              // ),
              child: Container(
                margin: EdgeInsets.all(8),
                padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
                decoration: ShapeDecoration(
                  shape: WeirdBorder(radius: 12, pathWidth: 2),
                  color: Color(0xFFFFDFA8),
                ),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Image.asset(
                      'assets/images/sehat/sehat_logo.png',
                      color: Colors.white.withOpacity(0.2),
                      colorBlendMode: BlendMode.modulate,
                      height: height * 0.1,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Text(
                                'SUBSIDI INTERVENSI PSIKIATRI SEHAT',
                                style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  fontSize: 24,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(width: 5),
                            Image.asset(
                              'assets/images/Asas/jata.png',
                              height: width * 0.25,
                            ),
                          ],
                        ),
                        SizedBox(height: 40),
                        Container(
                          width: width * 0.8,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                flex: 2,
                                child: Text(
                                  'NAMA',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                              Text(
                                ':',
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                ),
                              ),
                              SizedBox(width: 10),
                              Expanded(
                                flex: 3,
                                child: Text(
                                  widget.certificate.data!.accName!,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ),
                                  // textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                        Container(
                          width: width * 0.8,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                flex: 2,
                                child: Text(
                                  'NOMBOR I/C',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                              Text(
                                ':',
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                ),
                              ),
                              SizedBox(width: 10),
                              Expanded(
                                flex: 3,
                                child: Text(
                                  widget.identification,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ),
                                  // textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                        Container(
                          width: width * 0.8,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                flex: 2,
                                child: Text(
                                  'ALAMAT',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                              Text(
                                ':',
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                ),
                              ),
                              SizedBox(width: 10),
                              Expanded(
                                flex: 3,
                                child: Text(
                                  '${widget.certificate.data!.address1} ${widget.certificate.data!.address2}, ${widget.certificate.data!.postcode}, ${widget.certificate.data!.city}, ${widget.certificate.data!.state}',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ),
                                  // textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 60),
                        Text(
                          widget.certificate.data!.ppvName!,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                          ),
                        ),
                        SizedBox(height: 20),
                        Text(
                          formatDate(widget.certificate.data!.appDate!,
                              [dd, ' ', MM, ' ', yyyy]),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                          ),
                        ),
                        SizedBox(height: 20),
                        Table(border: TableBorder.all(), columnWidths: {
                          0: FixedColumnWidth(
                              width * 0.3), // fixed to 100 width
                          1: FlexColumnWidth(),
                        }, // Allows to add a border decoration around your table
                            children: [
                              TableRow(children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: Text(
                                    'SOALAN',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: Text(
                                    'TAHAP',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ]),
                              ...widget.scorePanel.map((e) {
                                return TableRow(children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10),
                                    child: Text(
                                      e.title!,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),

                                  //loop
                                  Column(
                                    children: e.score!.map((f) {
                                      return Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10),
                                        child: Text(
                                          '${f.name}: ${f.level}',
                                          textAlign: TextAlign.center,
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                ]);
                              }).toList(),
                            ]),
                        SizedBox(height: 100),
                        Text(
                          'Sijil ini hanya bertujuan untuk mendapatkan surat rujukan klinik Selcare sahaja',
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 15,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            // SizedBox(height: 20),
            if (widget.certificate.data!.canChange == 1)
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                width: double.infinity,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Color(0xFF438C8B),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10), // <-- Radius
                    ),
                  ),
                  onPressed: () {
                    Alert(
                      context: context,
                      style: AlertStyle(
                        isCloseButton: true,
                      ),
                      type: AlertType.info,
                      title: 'venue_fixed'.tr(),
                      desc: 'venue_fixed_desc'.tr(),
                      onWillPopActive: false,
                      buttons: [
                        DialogButton(
                          child: Text(
                            'Okay',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                builder: (context) => MultiBlocProvider(
                                  providers: [
                                    BlocProvider(
                                      create: (context) =>
                                          GetSehatClinicCubit(),
                                    ),
                                    BlocProvider(
                                      create: (context) => GetSehatDateCubit(),
                                    ),
                                    BlocProvider(
                                      create: (context) => SubmitCubit(),
                                    ),
                                  ],
                                  child: RegistrationPsychiatry(
                                      appointment: widget.appointment),
                                ),
                              ),
                            );
                          },
                          color: kPrimaryColor,
                        ),
                      ],
                    ).show();
                    // Navigator.of(context).pushReplacement(
                    //   MaterialPageRoute(
                    //     builder: (context) => MultiBlocProvider(
                    //       providers: [
                    //         BlocProvider(
                    //           create: (context) => GetSehatClinicCubit(),
                    //         ),
                    //         BlocProvider(
                    //           create: (context) => GetSehatDateCubit(),
                    //         ),
                    //         BlocProvider(
                    //           create: (context) => SubmitCubit(),
                    //         ),
                    //       ],
                    //       child: RegistrationPsychiatry(
                    //           appointment: widget.appointment),
                    //     ),
                    //   ),
                    // );
                  },
                  child: Text(
                    'reschedule'.tr(),
                  ),
                ),
              ),
            SizedBox(height: 15),
          ],
        ),
      ),
    );
  }
}
