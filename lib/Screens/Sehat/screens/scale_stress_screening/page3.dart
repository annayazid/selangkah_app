import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Sehat/screens/components/components.dart';
import 'package:selangkah_new/Screens/Sehat/screens/scale_stress_screening/answer.dart';
import 'package:selangkah_new/Screens/Sehat/screens/scale_stress_screening/setting.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';

class SSSPageThree extends StatefulWidget {
  const SSSPageThree({Key? key}) : super(key: key);

  @override
  _SSSPageThreeState createState() => _SSSPageThreeState();
}

class _SSSPageThreeState extends State<SSSPageThree> {
  double _value1 = 0.0;
  double _value2 = 0.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height -
    //     AppBar().preferredSize.height -
    //     MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Container(
              width: width * 0.9,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    width: width * 0.17,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width * 0.17,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width * 0.17,
                    height: 7,
                    decoration: BoxDecoration(color: themeColor),
                  ),
                  Container(
                    width: width * 0.17,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width * 0.17,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 40,
            ),
            buildQuestion(
              width,
              SliderTheme(
                data: SliderTheme.of(context).copyWith(
                  activeTrackColor: Colors.white.withOpacity(0.5),
                  inactiveTrackColor: Colors.white.withOpacity(0.5),
                  trackShape: RoundedRectSliderTrackShape(),
                  trackHeight: 8,
                  thumbShape: RoundSliderThumbShape(enabledThumbRadius: 12.0),
                  thumbColor: Colors.white,
                  overlayColor: Colors.white.withAlpha(32),
                  overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
                  // tickMarkShape: RoundSliderTickMarkShape(),
                  activeTickMarkColor: Colors.transparent,
                  inactiveTickMarkColor: Colors.transparent,
                  // valueIndicatorShape: PaddleSliderValueIndicatorShape(),
                  // valueIndicatorColor: Colors.white,
                  // valueIndicatorTextStyle: TextStyle(
                  //   color: Colors.white,
                  // ),
                ),
                child: Slider(
                  value: sliderStressIndicator[4].toDouble(),
                  min: 0.0,
                  max: 4.0,
                  onChanged: (value) {
                    setState(
                      () {
                        _value1 = value;

                        if (_value1.round() == 0) {
                          sliderStressIndicator[4] = 0;
                          stressAnswer[4] = 'stress_screening_ans1'.tr();
                        }
                        if (_value1.round() == 1) {
                          sliderStressIndicator[4] = 1;
                          stressAnswer[4] = 'stress_screening_ans2'.tr();
                        }
                        if (_value1.round() == 2) {
                          sliderStressIndicator[4] = 2;
                          stressAnswer[4] = 'stress_screening_ans3'.tr();
                        }
                        if (_value1.round() == 3) {
                          sliderStressIndicator[4] = 3;
                          stressAnswer[4] = 'stress_screening_ans4'.tr();
                        }
                        if (_value1.round() == 4) {
                          sliderStressIndicator[4] = 4;
                          stressAnswer[4] = 'stress_screening_ans5'.tr();
                        }
                      },
                    );
                  },
                ),
              ),
              _value1,
              '05',
              '10',
              stressAnswer[4],
              question[4].questionText!,
            ),
            SizedBox(
              height: 20,
            ),
            buildQuestion(
              width,
              SliderTheme(
                data: SliderTheme.of(context).copyWith(
                  activeTrackColor: Colors.white.withOpacity(0.5),
                  inactiveTrackColor: Colors.white.withOpacity(0.5),
                  trackShape: RoundedRectSliderTrackShape(),
                  trackHeight: 8,
                  thumbShape: RoundSliderThumbShape(enabledThumbRadius: 12.0),
                  thumbColor: Colors.white,
                  overlayColor: Colors.white.withAlpha(32),
                  overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
                  // tickMarkShape: RoundSliderTickMarkShape(),
                  activeTickMarkColor: Colors.transparent,
                  inactiveTickMarkColor: Colors.transparent,
                  // valueIndicatorShape: PaddleSliderValueIndicatorShape(),
                  // valueIndicatorColor: Colors.white,
                  // valueIndicatorTextStyle: TextStyle(
                  //   color: Colors.white,
                  // ),
                ),
                child: Slider(
                  value: sliderStressIndicator[5].toDouble(),
                  min: 0.0,
                  max: 4.0,
                  onChanged: (value) {
                    setState(
                      () {
                        _value2 = value;

                        if (_value2.round() == 0) {
                          sliderStressIndicator[5] = 0;
                          stressAnswer[5] = 'stress_screening_ans1'.tr();
                        }
                        if (_value2.round() == 1) {
                          sliderStressIndicator[5] = 1;
                          stressAnswer[5] = 'stress_screening_ans2'.tr();
                        }
                        if (_value2.round() == 2) {
                          sliderStressIndicator[5] = 2;
                          stressAnswer[5] = 'stress_screening_ans3'.tr();
                        }
                        if (_value2.round() == 3) {
                          sliderStressIndicator[5] = 3;
                          stressAnswer[5] = 'stress_screening_ans4'.tr();
                        }
                        if (_value2.round() == 4) {
                          sliderStressIndicator[5] = 4;
                          stressAnswer[5] = 'stress_screening_ans5'.tr();
                        }
                      },
                    );
                  },
                ),
              ),
              _value2,
              '06',
              '10',
              stressAnswer[5],
              question[5].questionText!,
            ),
          ],
        ),
      ),
    );
  }
}
