import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/Sehat/models/stressQuestion.dart';
import 'package:selangkah_new/Screens/Sehat/screens/components/components.dart';
import 'package:selangkah_new/Screens/Sehat/screens/scale_stress_screening/answer.dart';
import 'package:selangkah_new/Screens/Sehat/screens/scale_stress_screening/page1.dart';
import 'package:selangkah_new/Screens/Sehat/screens/scale_stress_screening/page2.dart';
import 'package:selangkah_new/Screens/Sehat/screens/scale_stress_screening/page3.dart';
import 'package:selangkah_new/Screens/Sehat/screens/scale_stress_screening/page4.dart';
import 'package:selangkah_new/Screens/Sehat/screens/scale_stress_screening/page5.dart';
import 'package:selangkah_new/Screens/Sehat/screens/scale_stress_screening/setting.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class ScaleStressScreening extends StatefulWidget {
  const ScaleStressScreening({Key? key}) : super(key: key);

  @override
  _ScaleStressScreeningState createState() => _ScaleStressScreeningState();
}

class _ScaleStressScreeningState extends State<ScaleStressScreening> {
  int total = 0;
  int _currentIndex = 0;
  bool showDialog = false;
  bool showDialogComplete = false;
  String? language;

  Widget callPage(int currentIndex) {
    switch (currentIndex) {
      case 0:
        return StressScaleScreen();
      case 1:
        return SSSPageOne();
      case 2:
        return SSSPageTwo();
      case 3:
        return SSSPageThree();
      case 4:
        return SSSPageFour();
      case 5:
        return SSSPageFive();

      default:
        return StressScaleScreen();
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await getQuestion();
      await getAnswer();
      await getCurrentAnswer();
    });
  }

  getQuestion() async {
    question.clear();
    language = EasyLocalization.of(context)!.locale.languageCode;
    if (language == 'ms') {
      setState(() {
        language = 'MY';
      });
    } else {
      setState(() {
        language = 'EN';
      });
    }
    final Map<String, String> map = {
      'language': language!,
      'token': TOKEN,
    };

    //print('get question cat 2');

    final responseSo = await http.post(
      Uri.parse('$API_URL_MENTAL/get_quest_cat2'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    var data = json.decode(responseSo.body);
    // //print('haiiii $data1');
    if (this.mounted) {
      setState(() {
        for (var data in data['Data']) {
          question.add(StressQuestion.fromJson(data));
        }
      });
    }

    setState(() {
      totalpages = (question.length / 2).round() + 1;
    });

    // pages.add(value);

    //print('hi question ${question.length}');
  }

  Future<bool> sentQuestion(answer, questionid) async {
    bool status = false;
    // bool statusCheck = false;
    int session = SehatGlobalVar.currentSession!;
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    Map<String, String> map = {
      'id_question': questionid,
      'answer': '$answer',
      'token': TOKEN,
      'id_session': '$session',
      'id_selangkah_user': id,
    };

    print(map);

    print('calling set answer 2');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/set_ans_cat2'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    //print(response.body);

    if (response.body != '[]') {
      var data = json.decode(response.body);
      if (data['Data'] == true) {
        status = true;
      }
    }

    //check answer if got
    // map = {
    //   'id_question': questionid,
    //   'id_selangkah_user': id,
    //   'id_session': '$session',
    //   'token': TOKEN,
    // };

    // //print(map);

    // //print('calling check answer');

    // final responseCheck = await http.post(
    //   '$API_URL_MENTAL/check_ans',
    //   headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    //   body: map,
    // );

    // //print(responseCheck.body);

    // if (responseCheck.body != '[]') {
    //   var data = json.decode(responseCheck.body);
    //   if (data['Data'] == true) {
    //     statusCheck = true;
    //   }
    // }

    // if (status) {
    //   return true;
    // } else {
    //   return false;
    // }

    return status;
  }

  getAnswer() async {
    List<int> answers = [];
    int session = SehatGlobalVar.currentSession!;
    SecureStorage secureStorage = SecureStorage();

    final Map<String, String> map = {
      'token': TOKEN,
      'id_session': '$session',
      'id_selangkah_user': await secureStorage.readSecureData('userId'),
    };

    //print('get answer cat 2');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_ans_cat2'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    if (response.body != '[]') {
      var data = json.decode(response.body);

      for (var data1 in data['Data']) {
        answers.add(int.parse(data1['answer']));
      }
    }
    setState(() {
      for (int i = 0; i < sliderStressIndicator.length; i++) {
        if (answers.asMap().containsKey(i)) {
          sliderStressIndicator[i] = answers[i];
        } else {
          sliderStressIndicator[i] = 0;
        }
      }
    });

    if (answers.length == 2) {
      setState(() {
        _currentIndex = 2;
      });
    } else if (answers.length == 4) {
      setState(() {
        _currentIndex = 3;
      });
    } else if (answers.length == 6) {
      setState(() {
        _currentIndex = 4;
      });
    } else if (answers.length == 8) {
      setState(() {
        _currentIndex = 5;
      });
    } else if (answers.length == 10) {
      int totalCat2 = await SehatRepositories.getTotal('2');

      setState(() {
        total = totalCat2;
        showDialog = true;
      });
    }
  }

  getCurrentAnswer() {
    for (int i = 0; i < question.length; i++) {
      if (sliderStressIndicator[i] == 0) {
        stressAnswer.insert(i, 'stress_screening_ans1'.tr().toString());
      } else if (sliderStressIndicator[i] == 1) {
        stressAnswer.insert(i, 'stress_screening_ans2'.tr().toString());
      } else if (sliderStressIndicator[i] == 2) {
        stressAnswer.insert(i, 'stress_screening_ans3'.tr().toString());
      } else if (sliderStressIndicator[i] == 3) {
        stressAnswer.insert(i, 'stress_screening_ans4'.tr().toString());
      } else if (sliderStressIndicator[i] == 4) {
        stressAnswer.insert(i, 'stress_screening_ans5'.tr().toString());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return SehatScaffold(
      appBarTitle: 'stress_scale_screening'.tr(),
      child: Container(
        height: height - AppBar().preferredSize.height,
        width: width,
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  callPage(_currentIndex),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: width * 0.85,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Visibility(
                          visible: (_currentIndex == 0 || _currentIndex == 1)
                              ? false
                              : true,
                          child: Container(
                            decoration: BoxDecoration(
                              color: themeColor,
                              shape: BoxShape.circle,
                            ),
                            child: IconButton(
                              icon: Icon(
                                Icons.arrow_back,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                setState(() {
                                  _currentIndex--;
                                });
                              },
                            ),
                          ),
                        ),
                        _currentIndex == 5
                            ? ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: themeColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                ),
                                onPressed: () async {
                                  bool stat;
                                  stat = await sentQuestion(
                                      sliderStressIndicator[8], question[8].id);
                                  if (stat == true) {
                                    stat = await sentQuestion(
                                        sliderStressIndicator[9],
                                        question[9].id);
                                    if (stat) {
                                      int totalCat2 =
                                          await SehatRepositories.getTotal('2');
                                      setState(() {
                                        total = totalCat2;
                                        showDialogComplete = true;
                                      });
                                    }
                                  }
                                },
                                child: Text(
                                  'proceed'.tr().toString(),
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: width * 0.04,
                                  ),
                                ),
                              )
                            : Container(
                                decoration: BoxDecoration(
                                  color: themeColor,
                                  shape: BoxShape.circle,
                                ),
                                child: IconButton(
                                  icon: Icon(
                                    Icons.arrow_forward,
                                    color: Colors.white,
                                  ),
                                  onPressed: () async {
                                    bool stat;

                                    if (_currentIndex == 0) {
                                      setState(() {
                                        _currentIndex = 1;
                                      });
                                    } else if (_currentIndex < 5) {
                                      if (_currentIndex == 1) {
                                        //for question1 and question2
                                        stat = await sentQuestion(
                                            sliderStressIndicator[0],
                                            question[0].id);
                                        if (stat == true) {
                                          stat = await sentQuestion(
                                              sliderStressIndicator[1],
                                              question[1].id);
                                          if (stat) {
                                            setState(() {
                                              _currentIndex = 2;
                                            });
                                          }
                                        }
                                      } else if (_currentIndex == 2) {
                                        //for question3 and question4
                                        stat = await sentQuestion(
                                            sliderStressIndicator[2],
                                            question[2].id);
                                        if (stat == true) {
                                          stat = await sentQuestion(
                                              sliderStressIndicator[3],
                                              question[3].id);
                                          if (stat) {
                                            setState(() {
                                              _currentIndex = 3;
                                            });
                                          }
                                        }
                                      } else if (_currentIndex == 3) {
                                        //for question5 and question6
                                        stat = await sentQuestion(
                                            sliderStressIndicator[4],
                                            question[4].id);
                                        if (stat == true) {
                                          stat = await sentQuestion(
                                              sliderStressIndicator[5],
                                              question[5].id);
                                          if (stat) {
                                            setState(() {
                                              _currentIndex = 4;
                                            });
                                          }
                                        }
                                      } else if (_currentIndex == 4) {
                                        //for question7 and question8
                                        stat = await sentQuestion(
                                            sliderStressIndicator[6],
                                            question[6].id);
                                        if (stat == true) {
                                          stat = await sentQuestion(
                                              sliderStressIndicator[7],
                                              question[7].id);
                                          if (stat) {
                                            setState(() {
                                              _currentIndex = 5;
                                            });
                                          }
                                        }
                                      }
                                    }
                                  },
                                ),
                              )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
            Visibility(
              visible: showDialog,
              child: buildSuccessDialog(
                height,
                width,
                '',
                'stress_mark'.tr().toString(),
                '$total',
                '40',
                () {
                  // Navigator.of(context).pop();
                  setState(() {
                    showDialog = false;
                  });
                },
                '',
              ),
            ),
            Visibility(
              visible: showDialogComplete,
              child: buildSuccessDialog(
                height,
                width,
                '',
                'stress_mark'.tr().toString(),
                '$total',
                '40',
                () {
                  Navigator.of(context).pop();
                },
                '',
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class StressScaleScreen extends StatelessWidget {
  const StressScaleScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height -
    //     AppBar().preferredSize.height -
    //     MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Container(
              width: width * 0.9,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    width: width * 0.175,
                    height: 7,
                    decoration: BoxDecoration(color: themeColor),
                  ),
                  Container(
                    width: width * 0.175,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width * 0.175,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width * 0.175,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width * 0.15,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 40,
            ),
            UnconstrainedBox(
              child: Container(
                padding: EdgeInsets.symmetric(
                  vertical: 5,
                  horizontal: 10,
                ),
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 2.5,
                    color: Colors.grey[600]!,
                  ),
                  borderRadius: BorderRadius.circular(20),
                ),
                alignment: Alignment.center,
                child: Text(
                  'stress_scale_screening'.tr().toUpperCase(),
                  style: TextStyle(
                    fontSize: width * 0.045,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[600],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: width * 0.85,
              decoration: BoxDecoration(
                color: themeColor,
                borderRadius: BorderRadius.circular(20),
              ),
              padding: EdgeInsets.symmetric(
                vertical: 15,
                horizontal: 25,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  RichText(
                    // textAlign: TextAlign.justify,
                    text: TextSpan(
                      text: 'stress_screening_instruction1'.tr().toString(),
                      style: TextStyle(
                        fontSize: width * 0.04,
                        color: Colors.white,
                      ),
                      children: [
                        TextSpan(
                            text:
                                'stress_screening_instruction2'.tr().toString(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ))
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'stress_screening_instruction3'.tr().toString(),
                    style: TextStyle(
                      fontSize: width * 0.04,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
