import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Sehat/screens/components/components.dart';
import 'package:selangkah_new/Screens/Sehat/screens/scale_stress_screening/answer.dart';
import 'package:selangkah_new/Screens/Sehat/screens/scale_stress_screening/setting.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';

class SSSPageFive extends StatefulWidget {
  const SSSPageFive({Key? key}) : super(key: key);

  @override
  _SSSPageFiveState createState() => _SSSPageFiveState();
}

class _SSSPageFiveState extends State<SSSPageFive> {
  double _value1 = 0.0;
  double _value2 = 0.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height -
    //     AppBar().preferredSize.height -
    //     MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Container(
              width: width * 0.9,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    width: width * 0.17,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width * 0.17,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width * 0.17,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width * 0.17,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width * 0.17,
                    height: 7,
                    decoration: BoxDecoration(color: themeColor),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 40,
            ),
            buildQuestion(
              width,
              SliderTheme(
                data: SliderTheme.of(context).copyWith(
                  activeTrackColor: Colors.white.withOpacity(0.5),
                  inactiveTrackColor: Colors.white.withOpacity(0.5),
                  trackShape: RoundedRectSliderTrackShape(),
                  trackHeight: 8,
                  thumbShape: RoundSliderThumbShape(enabledThumbRadius: 12.0),
                  thumbColor: Colors.white,
                  overlayColor: Colors.white.withAlpha(32),
                  overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
                  // tickMarkShape: RoundSliderTickMarkShape(),
                  activeTickMarkColor: Colors.transparent,
                  inactiveTickMarkColor: Colors.transparent,
                  // valueIndicatorShape: PaddleSliderValueIndicatorShape(),
                  // valueIndicatorColor: Colors.white,
                  // valueIndicatorTextStyle: TextStyle(
                  //   color: Colors.white,
                  // ),
                ),
                child: Slider(
                  value: sliderStressIndicator[8].toDouble(),
                  min: 0.0,
                  max: 4.0,
                  onChanged: (value) {
                    setState(
                      () {
                        _value1 = value;

                        if (_value1.round() == 0) {
                          sliderStressIndicator[8] = 0;
                          stressAnswer[8] = 'stress_screening_ans1'.tr();
                        }
                        if (_value1.round() == 1) {
                          sliderStressIndicator[8] = 1;
                          stressAnswer[8] = 'stress_screening_ans2'.tr();
                        }
                        if (_value1.round() == 2) {
                          sliderStressIndicator[8] = 2;
                          stressAnswer[8] = 'stress_screening_ans3'.tr();
                        }
                        if (_value1.round() == 3) {
                          sliderStressIndicator[8] = 3;
                          stressAnswer[8] = 'stress_screening_ans4'.tr();
                        }
                        if (_value1.round() == 4) {
                          sliderStressIndicator[8] = 4;
                          stressAnswer[8] = 'stress_screening_ans5'.tr();
                        }
                      },
                    );
                  },
                ),
              ),
              _value1,
              '09',
              '10',
              stressAnswer[8],
              question[8].questionText!,
            ),
            SizedBox(
              height: 20,
            ),
            buildQuestion(
              width,
              SliderTheme(
                data: SliderTheme.of(context).copyWith(
                  activeTrackColor: Colors.white.withOpacity(0.5),
                  inactiveTrackColor: Colors.white.withOpacity(0.5),
                  trackShape: RoundedRectSliderTrackShape(),
                  trackHeight: 8,
                  thumbShape: RoundSliderThumbShape(enabledThumbRadius: 12.0),
                  thumbColor: Colors.white,
                  overlayColor: Colors.white.withAlpha(32),
                  overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
                  // tickMarkShape: RoundSliderTickMarkShape(),
                  activeTickMarkColor: Colors.transparent,
                  inactiveTickMarkColor: Colors.transparent,
                  // valueIndicatorShape: PaddleSliderValueIndicatorShape(),
                  // valueIndicatorColor: Colors.white,
                  // valueIndicatorTextStyle: TextStyle(
                  //   color: Colors.white,
                  // ),
                ),
                child: Slider(
                  value: sliderStressIndicator[9].toDouble(),
                  min: 0.0,
                  max: 4.0,
                  onChanged: (value) {
                    setState(
                      () {
                        _value2 = value;

                        if (_value2.round() == 0) {
                          sliderStressIndicator[9] = 0;
                          stressAnswer[9] = 'stress_screening_ans1'.tr();
                        }
                        if (_value2.round() == 1) {
                          sliderStressIndicator[9] = 1;
                          stressAnswer[9] = 'stress_screening_ans2'.tr();
                        }
                        if (_value2.round() == 2) {
                          sliderStressIndicator[9] = 2;
                          stressAnswer[9] = 'stress_screening_ans3'.tr();
                        }
                        if (_value2.round() == 3) {
                          sliderStressIndicator[9] = 3;
                          stressAnswer[9] = 'stress_screening_ans4'.tr();
                        }
                        if (_value2.round() == 4) {
                          sliderStressIndicator[9] = 4;
                          stressAnswer[9] = 'stress_screening_ans5'.tr();
                        }
                      },
                    );
                  },
                ),
              ),
              _value2,
              '10',
              '10',
              stressAnswer[9],
              question[9].questionText!,
            ),
          ],
        ),
      ),
    );
  }
}
