import 'dart:ui';

import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';
import 'package:selangkah_new/utils/constants.dart';

Container buildQuestion(
    double width,
    Widget child,
    double val,
    String currentQuestion,
    String totalQuestion,
    String currentSlider,
    String question) {
  return Container(
    width: width * 0.85,
    decoration: BoxDecoration(
      color: themeColor,
      borderRadius: BorderRadius.circular(20),
    ),
    padding: EdgeInsets.symmetric(
      vertical: 15,
      horizontal: 20,
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              currentQuestion,
              style: TextStyle(
                color: Colors.white,
                fontSize: width * 0.07,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              ' of',
              style: TextStyle(
                color: Colors.grey[300],
                fontSize: width * 0.04,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              ' ' + totalQuestion,
              style: TextStyle(
                color: Colors.grey[300],
                fontSize: width * 0.045,
                fontWeight: FontWeight.bold,
              ),
            ),
            Spacer(),
            Icon(
              Icons.check_circle,
              color: Colors.white,
              size: 30,
            )
          ],
        ),
        SizedBox(
          height: 30,
        ),
        Text(
          question,
          style: TextStyle(
            color: Colors.white,
            fontSize: width * 0.04,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.start,
        ),
        SizedBox(
          height: 10,
        ),
        Align(
          alignment: Alignment.centerRight,
          child: UnconstrainedBox(
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 10,
              ),
              // width: width * 0.4,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.white,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(13),
              ),
              child: Text(
                currentSlider,
                style: TextStyle(
                  color: Colors.white,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        // Visibility(
        //   visible: val == 33.33 || val == 66.67 ? false : true,
        //   child: SizedBox(
        //     height: 14,
        //   ),
        // ),
        child,
        Row(
          children: [
            Text(
              'stress_screening_ans1'.tr().toString(),
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            Spacer(),
            Text(
              'stress_screening_ans5'.tr().toString(),
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ],
        )
      ],
    ),
  );
}

Container buildLiteracyQuestion(
    double width,
    Widget child,
    String currentQuestion,
    String totalQuestion,
    String currentSlider,
    String question) {
  return Container(
    width: width * 0.85,
    decoration: BoxDecoration(
      color: themeColor,
      borderRadius: BorderRadius.circular(20),
    ),
    padding: EdgeInsets.symmetric(
      vertical: 15,
      horizontal: 20,
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              currentQuestion,
              style: TextStyle(
                color: Colors.white,
                fontSize: width * 0.055,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              ' of',
              style: TextStyle(
                color: Colors.grey[300],
                fontSize: width * 0.04,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              ' ' + totalQuestion,
              style: TextStyle(
                color: Colors.grey[300],
                fontSize: width * 0.045,
                fontWeight: FontWeight.bold,
              ),
            ),
            Spacer(),
            Icon(
              Icons.check_circle,
              color: Colors.white,
              size: 30,
            )
          ],
        ),
        SizedBox(
          height: 30,
        ),
        Text(
          question,
          style: TextStyle(
            color: Colors.white,
            fontSize: width * 0.04,
          ),
          textAlign: TextAlign.start,
        ),
        SizedBox(
          height: 10,
        ),
        Align(
          alignment: Alignment.centerRight,
          child: UnconstrainedBox(
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 10,
              ),
              // width: width * 0.4,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.white,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(13),
              ),
              child: Text(
                currentSlider,
                style: TextStyle(
                  color: Colors.white,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        child,
      ],
    ),
  );
}

Stack buildSuccessDialog(double height, double width, String title, text1,
    marks, totMarks, press, text2) {
  return Stack(
    clipBehavior: Clip.none,
    alignment: Alignment.center,
    children: [
      ClipRRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
          child: Container(
            height: height - AppBar().preferredSize.height,
            width: width,
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.5),
            ),
          ),
        ),
      ),
      UnconstrainedBox(
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Container(
              width: width * 0.8,
              padding: EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 20,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0),
                  ),
                ],
              ),
              child: Column(
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontSize: width * 0.04,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(height: 15),
                  Image.asset(
                    'assets/images/sehat/Success_Mark.png',
                    width: 200,
                  ),
                  SizedBox(height: 15),
                  Container(
                    alignment: Alignment.center,
                    width: width * 0.7,
                    child: Text(
                      text1,
                      style: TextStyle(
                        fontSize: width * 0.04,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(height: 15),
                  marks == ''
                      ? Text(
                          text2,
                          style: TextStyle(
                            color: themeColor,
                            fontSize: width * 0.07,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              marks,
                              style: TextStyle(
                                color: themeColor,
                                fontSize: width * 0.07,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              '/',
                              style: TextStyle(
                                color: themeColor,
                                fontSize: width * 0.05,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              totMarks,
                              style: TextStyle(
                                color: themeColor,
                                fontSize: width * 0.05,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                  SizedBox(height: 15),
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: themeColor,
                      padding: EdgeInsets.symmetric(
                        horizontal: 30,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    onPressed: press,
                    child: Text(
                      'OK',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            // Positioned(
            //   bottom: -25,
            //   child: Container(
            //     width: width * 0.8,
            //     child: Row(
            //       mainAxisAlignment: MainAxisAlignment.center,
            //       children: [
            //         RaisedButton(
            //           color: themeColor,
            //           padding: EdgeInsets.symmetric(
            //             horizontal: 30,
            //           ),
            //           shape: RoundedRectangleBorder(
            //             borderRadius: BorderRadius.circular(20),
            //           ),
            //           onPressed: press,
            //           child: Text(
            //             'OK',
            //             style: TextStyle(
            //               color: Colors.white,
            //             ),
            //           ),
            //         ),
            //       ],
            //     ),
            //   ),
            // )
          ],
        ),
      )
    ],
  );
}

Stack buildPsyEduDialog(double height, double width, text1, press, text2) {
  return Stack(
    clipBehavior: Clip.none,
    alignment: Alignment.center,
    children: [
      ClipRRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
          child: Container(
            height: height - AppBar().preferredSize.height,
            width: width,
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.5),
            ),
          ),
        ),
      ),
      UnconstrainedBox(
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Container(
              width: width * 0.8,
              padding: EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 20,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0),
                  ),
                ],
              ),
              child: Column(
                children: [
                  Image.asset(
                    'assets/images/sehat/Success_Mark.png',
                    width: 200,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    alignment: Alignment.center,
                    width: width * 0.7,
                    child: Text(
                      text1.toUpperCase(),
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: themeColor,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    alignment: Alignment.center,
                    width: width * 0.7,
                    child: Text(
                      text2.toUpperCase(),
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: -25,
              child: Container(
                width: width * 0.8,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: themeColor,
                        padding: EdgeInsets.symmetric(
                          horizontal: 30,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                      ),
                      onPressed: press,
                      child: Text(
                        'OK',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      )
    ],
  );
}

Stack buildSuccessDialogNormaltext(double height, double width, press, text1) {
  return Stack(
    clipBehavior: Clip.none,
    alignment: Alignment.center,
    children: [
      ClipRRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
          child: Container(
            height: height - AppBar().preferredSize.height,
            width: width,
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.5),
            ),
          ),
        ),
      ),
      UnconstrainedBox(
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Container(
              width: width * 0.8,
              padding: EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 20,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0),
                  ),
                ],
              ),
              child: Column(
                children: [
                  Image.asset(
                    'assets/images/sehat/Success_Mark.png',
                    width: 200,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    width: width * 0.6,
                    child: Text(
                      text1,
                      style: TextStyle(
                        fontSize: width * 0.04,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: -25,
              child: Container(
                width: width * 0.8,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.symmetric(
                          horizontal: 30,
                        ),
                        backgroundColor: themeColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                      ),
                      onPressed: press,
                      child: Text(
                        'OK',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      )
    ],
  );
}

Container customLeading(double width, text) {
  return Container(
    width: width * 0.25,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          child: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 13,
            ),
          ),
        ),
        Text(
          '*',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 13,
            color: kPrimaryColor,
          ),
        ),
        Text(
          ':',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: width * 0.04,
          ),
        ),
      ],
    ),
  );
}

Widget buildRow(double width, text, number, Widget watch) {
  return Container(
    margin: EdgeInsets.only(
      top: 15,
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      // crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: width * 0.08,
          child: Text(
            number,
            style: TextStyle(
              color: Colors.white,
              fontSize: 13,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Expanded(
          child: Text(
            text,
            style: TextStyle(
              color: Colors.white,
              fontSize: 13,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        watch
      ],
    ),
  );
}

InkWell buildWatch(double width, press) {
  return InkWell(
    onTap: press,
    child: Container(
      alignment: Alignment.center,
      width: width * 0.25,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      padding: EdgeInsets.all(8),
      child: Text(
        'watch'.tr().toUpperCase(),
        style: TextStyle(
          color: themeColor,
          fontWeight: FontWeight.bold,
          fontSize: 13,
        ),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    ),
  );
}

Widget buildWatched(double width, press) {
  return InkWell(
    onTap: press,
    child: Container(
      alignment: Alignment.center,
      width: width * 0.25,
      decoration: BoxDecoration(
        color: Colors.green,
        borderRadius: BorderRadius.circular(20),
      ),
      padding: EdgeInsets.all(8),
      child: Text(
        'watched'.tr().toUpperCase(),
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: 13,
        ),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    ),
  );
}
