import 'package:flutter/material.dart';

class SehatScaffold extends StatelessWidget {
  final String appBarTitle;
  final Widget child;

  const SehatScaffold(
      {Key? key, required this.appBarTitle, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: true,
        title: Text(
          appBarTitle,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 15.0,
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: "Roboto",
          ),
        ),
      ),
      body: Column(
        children: [
          Container(
              height: height - AppBar().preferredSize.height,
              width: width,
              child: child),
        ],
      ),
    );
  }
}
