import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_screen.dart';
import 'package:selangkah_new/Screens/Sehat/screens/module/module_pathway.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class IactWelcome extends StatelessWidget {
  final String categoryName;
  final String categoryId;

  const IactWelcome(
      {Key? key, required this.categoryName, required this.categoryId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;

    SecureStorage().writeSecureData('checkFirstTimeIact', '1');

    return SehatScaffold(
      appBarTitle: 'Mental Sehat',
      child: Container(
        height: height - AppBar().preferredSize.height,
        width: width,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 10),
              Container(
                padding: EdgeInsets.all(10),
                child: Text(
                  'Selamat Datang Ke i-ACT For Life',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF676B74),
                  ),
                  textAlign: TextAlign.center,
                ),
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 3,
                    color: Color(0xFF676B74),
                  ),
                  borderRadius: BorderRadius.circular(15),
                ),
              ),
              SizedBox(height: 10),
              Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(20),
                width: width,
                decoration: BoxDecoration(
                  color: Color(0xFF438C8B),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Column(
                  children: [
                    Image.asset('assets/images/sehat/iact_pembukaan.png'),
                    SizedBox(height: 15),
                    Text(
                      'i-ACT For Life adalah satu program yang direka untuk membantu anda mencegah dan menguruskan tekanan harian anda dengan lebih baik. Bukan itu sahaja, ia berupaya untuk menjadikan anda seorang yang lebih fleksibel, lebih bersikap belas pada diri sendiri, dan meningkatkan daya tindak anda. i-ACT For Life disajikan dalam bentuk infografik, latihan audio, dan video-video pendek.',
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.justify,
                    ),
                    SizedBox(height: 15),
                    Text(
                      'Sebelum kita bermula, sila jawap beberapa soalan mengenai diri anda dan mari ukur tahap tekanan anda sebelum program. Ini penting agar kita boleh menilai sejauh mana program ini berkesan mengurangkan tekanan anda dan penambahbaikan i-ACT For Life boleh dilakukan dari semasa ke semasa.',
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.justify,
                    ),
                    SizedBox(height: 15),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Align(
                alignment: Alignment.centerRight,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Color(0xFF438C8B),
                    shape: CircleBorder(),
                  ),
                  onPressed: () async {
                    //redirect dass

                    //if dah jawab dass
                    int session = SehatGlobalVar.currentSession!;
                    Journey journey =
                        await SehatRepositories.getJourney(session);

                    if (journey.data!.any(
                        (element) => element.idMentalQuestionCat == '13')) {
                      Score score = await SehatRepositories.getScore(
                          GlobalVariables.language!);
                      for (var i = 0; i < score.data!.length; i++) {
                        if (score.data![i].idMentalQuestionCat == '13') {
                          showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(10),
                                  ),
                                ),
                                backgroundColor: Colors.white,
                                content: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      'title_dass'.tr(),
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(height: 15),
                                    Image.asset(
                                      'assets/images/sehat/Success_Mark.png',
                                      width: 200,
                                    ),
                                    SizedBox(height: 10),
                                    ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: score.data![i].child!.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        print(
                                            score.data![i].child![index].name);
                                        return Column(
                                          children: [
                                            Text(
                                              '${'text1_dass'.tr()} ${score.data![i].child![index].name} ${'text2_dass'.tr()}',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                            Text(
                                              score.data![i].child![index]
                                                  .level!,
                                              style: TextStyle(
                                                color: Color(
                                                  int.parse(
                                                      '0xFF${score.data![i].child![index].color!.substring(1, score.data![i].child![0].color!.length)}'),
                                                ),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20,
                                              ),
                                            ),
                                            SizedBox(height: 10),
                                          ],
                                        );
                                      },
                                    ),
                                    SizedBox(height: 10),
                                    ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        backgroundColor: themeColor,
                                        padding: EdgeInsets.symmetric(
                                          horizontal: 30,
                                        ),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                      ),
                                      onPressed: () {
                                        //redirect to accordion

                                        Navigator.of(context).pushReplacement(
                                          MaterialPageRoute(
                                            builder: (context) => BlocProvider(
                                              create: (context) =>
                                                  GetPathwayCubit(),
                                              child: ModulePathway(
                                                title: categoryName,
                                                idModule: categoryId,
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                      child: Text(
                                        'OK',
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          );
                          break;
                        }
                      }
                    } else {
                      await Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => BlocProvider(
                            create: (context) => GetContentCubit(),
                            child: ContentScreen(
                              title:
                                  'Depression Anxiety Stress Scale (DASS-21)',
                              idCategory: '13',
                              isQuestionnaire: true,
                            ),
                          ),
                        ),
                      );

                      await Future.delayed(Duration(milliseconds: 200));

                      Navigator.of(context).pop();
                    }

                    //if belum jawab dass
                  },
                  child: Icon(
                    FontAwesomeIcons.arrowRight,
                    size: 15,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
