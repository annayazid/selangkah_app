import 'package:another_flushbar/flushbar.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:getwidget/getwidget.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_screen.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';

class ModulePathway extends StatefulWidget {
  final String title;
  final String idModule;

  const ModulePathway({Key? key, required this.title, required this.idModule})
      : super(key: key);

  @override
  _ModulePathwayState createState() => _ModulePathwayState();
}

class _ModulePathwayState extends State<ModulePathway> {
  @override
  void initState() {
    SehatGlobalVar.canNextQuiz = true;
    context
        .read<GetPathwayCubit>()
        .getPathway(widget.idModule, GlobalVariables.language!);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;

    return SehatScaffold(
      appBarTitle: widget.title,
      child: Container(
          height: height - AppBar().preferredSize.height,
          width: width,
          child: BlocBuilder<GetPathwayCubit, GetPathwayState>(
            builder: (context, state) {
              if (state is GetPathwayLoaded) {
                return GetPathwayLoadedWidget(
                  idModule: widget.idModule,
                  pathway: state.pathway,
                  activityList: state.activityList,
                  activityComplete: state.activityComplete,
                  pathwayCanAccess: state.pathwayCanAccess,
                  isReceiveNotification: state.isReceiveNotification,
                );
              } else {
                return SpinKitFadingCircle(
                  color: kPrimaryColor,
                  size: 20,
                );
              }
            },
          )),
    );
  }
}

class GetPathwayLoadedWidget extends StatefulWidget {
  final String idModule;
  final Pathway pathway;
  final List<Activity> activityList;
  final ActivityComplete activityComplete;
  final List<bool> pathwayCanAccess;
  final bool isReceiveNotification;

  const GetPathwayLoadedWidget(
      {Key? key,
      required this.idModule,
      required this.pathway,
      required this.activityList,
      required this.activityComplete,
      required this.pathwayCanAccess,
      required this.isReceiveNotification})
      : super(key: key);

  @override
  State<GetPathwayLoadedWidget> createState() => _GetPathwayLoadedWidgetState();
}

class _GetPathwayLoadedWidgetState extends State<GetPathwayLoadedWidget> {
  List<bool> accordiance = [];
  int? activeIndex;

  @override
  void initState() {
    isReceiveNotification = widget.isReceiveNotification;
    widget.pathway.data!.forEach((element) {
      accordiance.add(false);
    });
    super.initState();
  }

  int daysBetween(DateTime from, DateTime to) {
    // from = DateTime(from.year, from.month, from.day);
    // to = DateTime(to.year, to.month, to.day);
    // return (to.difference(from).inHours / 24).round();

    return to.day - from.day;

    // return to.subtract(Duration(days: 10)).day - from.day;
  }

  List<bool> canAccess = [];

  bool isSomethingCollapse = false;

  bool? isReceiveNotification;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(15),
      child: Column(
        children: [
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: widget.pathway.data!.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0.0, 1.0), //(x,y)
                      blurRadius: 4.0,
                    ),
                  ],
                ),
                child: IgnorePointer(
                  ignoring: (isSomethingCollapse && index != activeIndex) ||
                      !widget.pathwayCanAccess[index],
                  child: GFAccordion(
                    contentPadding: EdgeInsets.all(15),
                    titlePadding: EdgeInsets.all(15),
                    onToggleCollapsed: (bool collapse) {
                      setState(() {
                        isSomethingCollapse = collapse;
                        accordiance[index] = collapse;
                        activeIndex = index;

                        for (var i = 0;
                            i < widget.activityList[index].data!.length;
                            i++) {
                          canAccess.add(false);
                        }
                      });

                      if (!accordiance[index]) {
                        canAccess.clear();
                      }
                    },
                    titleBorderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                      bottomLeft: (accordiance[index])
                          ? Radius.circular(0)
                          : Radius.circular(10),
                      bottomRight: (accordiance[index])
                          ? Radius.circular(0)
                          : Radius.circular(10),
                    ),
                    contentBorderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                    margin: EdgeInsets.zero,
                    collapsedTitleBackgroundColor:
                        widget.pathwayCanAccess[index]
                            ? Colors.white
                            : Color(0xFFE6E6E6),
                    expandedTitleBackgroundColor: Color(0xFF438C8B),
                    titleChild: Text(
                      widget.pathway.data![index].pathwayName!,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: accordiance[index] ? Colors.white : Colors.black,
                      ),
                    ),
                    contentChild: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: widget.activityList[index].data!.length,
                      itemBuilder: (BuildContext context, int indexActivity) {
                        // List<bool> canAccess = List<bool>.filled(
                        //     widget.activityList[index].data.length, false);
                        canAccess[0] = true;

                        //check not yet complete
                        if (!widget.activityComplete.data!.any((element) =>
                            element.idMentalActivity ==
                            widget
                                .activityList[index].data![indexActivity].id)) {
                          if (indexActivity == 0) {
                            canAccess[0] = true;
                          } else {
                            //check previous activity if comppleted or not

                            //if complete
                            if (widget.activityComplete.data!.any((element) =>
                                element.idMentalActivity ==
                                widget.activityList[index]
                                    .data![indexActivity - 1].id)) {
                              //if sama day
                              if (widget.activityList[index]
                                      .data![indexActivity].day ==
                                  widget.activityList[index]
                                      .data![indexActivity - 1].day) {
                                canAccess[indexActivity] = true;
                              } else {
                                //check if dah lepas day

                                //get date
                                DateTime now = DateTime.now();

                                int indexActivityComplete = widget
                                    .activityComplete.data!
                                    .indexWhere((element) =>
                                        element.idMentalActivity ==
                                        widget.activityList[index]
                                            .data![indexActivity - 1].id);

                                // print(indexActivityComplete);

                                int dayBetween = daysBetween(
                                    DateTime.parse(widget
                                        .activityComplete
                                        .data![indexActivityComplete]
                                        .completedTs!),
                                    now);
                                print('indexActivity $indexActivity');

                                print('daybetween $dayBetween');

                                if (dayBetween > 0) {
                                  canAccess[indexActivity] = true;
                                }

                                // if (int.parse(widget.activityList[index]
                                //         .data[indexActivity].day) <=
                                //     dayBetween) {
                                //   canAccess[indexActivity] = true;
                                // }
                              }
                            }

                            //if not
                            else {}
                          }

                          //sampai sini

                          // print('canaccess ${canAccess.length}');

                          //get date
                          // DateTime now = DateTime.now();

                          // int indexActivityComplete = widget.activityComplete.data
                          //     .indexWhere((element) =>
                          //         element.idMentalActivity ==
                          //         widget.activityList[index].data[indexActivity].id);

                          // //get difference
                          // int dayBetween = daysBetween(
                          //     DateTime.parse(widget.activityComplete
                          //             .data[indexActivityComplete].completedTs)
                          //         .subtract(Duration(days: 3)),
                          //     now);

                          // // print(
                          // //     'nameactibity ke ${widget.activityList[index].data[indexActivity].activityName}');
                          // print(
                          //     'day ke ${widget.activityList[index].data[indexActivity].day}');

                          // print('days between is dayBetween $dayBetween');

                          // //get difference date
                          // if (int.parse(widget
                          //         .activityList[index].data[indexActivity].day) <=
                          //     dayBetween) {
                          //   if (indexActivity != 0) {
                          //     print('masuk sini');
                          //     print('index activity is $indexActivity');
                          //     if (widget.activityComplete.data.any((element) =>
                          //         element.idMentalActivity ==
                          //         widget.activityList[index].data[indexActivity - 1]
                          //             .id)) {
                          //       canAccess[indexActivity] = true;
                          //     } else {}
                          //   }
                          // }

                        } else {
                          canAccess[indexActivity] = true;
                        }

                        // canAccess.forEach((element) {
                        //   log('canaccess $element');
                        // });

                        return GestureDetector(
                          onTap: () async {
                            if (canAccess[indexActivity]) {
                              await Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => BlocProvider(
                                    create: (context) => GetContentCubit(),
                                    child: ContentScreen(
                                      title: widget.activityList[index]
                                          .data![indexActivity].activityName!,
                                      idCategory: widget.activityList[index]
                                          .data![indexActivity].id!,
                                      isQuestionnaire: false,
                                    ),
                                  ),
                                ),
                              );

                              await Future.delayed(Duration(milliseconds: 200));

                              context.read<GetPathwayCubit>().getPathway(
                                  widget.idModule, GlobalVariables.language!);
                            } else {
                              Flushbar(
                                borderColor: Colors.black,
                                animationDuration: Duration(milliseconds: 250),
                                backgroundColor: Colors.white,
                                borderWidth: 2,
                                icon: Icon(FontAwesomeIcons.exclamation),
                                // message: LoremText,
                                // title: 'Permission needed to continue',
                                titleText: Text(
                                  'complete_previous_activity'.tr(),
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                  ),
                                ),
                                messageText: Text(
                                  'complete_previous_activity_desc'.tr(),
                                  style: TextStyle(color: Colors.black),
                                ),
                                mainButton: TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text(
                                    'Okay',
                                    style: TextStyle(color: Colors.blue),
                                  ),
                                ),
                                duration: Duration(seconds: 7),
                              ).show(context);
                            }
                          },
                          child: Container(
                            color: Colors.white,
                            width: double.infinity,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 10),
                                        child: Text(
                                          widget
                                              .activityList[index]
                                              .data![indexActivity]
                                              .activityName!,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ),
                                    ),
                                    if (widget.activityComplete.data!.any(
                                        (element) =>
                                            element.idMentalActivity ==
                                            widget.activityList[index]
                                                .data![indexActivity].id))
                                      Icon(
                                        FontAwesomeIcons.solidCircleCheck,
                                        color: Color(0xFF60B05E),
                                        size: 15,
                                      ),
                                    if (!widget.activityComplete.data!.any(
                                            (element) =>
                                                element.idMentalActivity ==
                                                widget.activityList[index]
                                                    .data![indexActivity].id) &&
                                        canAccess[indexActivity]) ...[
                                      Icon(
                                        FontAwesomeIcons.circleRight,
                                        color: Color(0xFFC5162E),
                                        size: 15,
                                      ),
                                    ] else if (!widget.activityComplete.data!
                                        .any((element) =>
                                            element.idMentalActivity ==
                                            widget.activityList[index]
                                                .data![indexActivity].id)) ...[
                                      Icon(
                                        FontAwesomeIcons.lock,
                                        color: Colors.black54,
                                        size: 15,
                                      ),
                                    ],
                                  ],
                                ),
                                Divider(color: Colors.grey)
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                    collapsedIcon: Icon(
                      FontAwesomeIcons.chevronDown,
                      size: 14,
                      color: accordiance[index] ? Colors.white : Colors.black,
                    ),
                    expandedIcon: Icon(
                      FontAwesomeIcons.chevronUp,
                      size: 14,
                      color: accordiance[index] ? Colors.white : Colors.black,
                    ),
                  ),
                ),
              );

              // return ExpandablePanel(
              //   header: Text(
              //     widget.pathway.data[index].pathwayName,
              //     style: TextStyle(
              //       fontWeight: FontWeight.bold,
              //     ),
              //   ),
              //   expanded: Column(
              //     children: [
              //       Text('test'),
              //       SizedBox(height: 20),
              //     ],
              //   ),
              // );
            },
          ),
          SizedBox(height: 20),
          Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
            ),
            child: CheckboxListTile(
              controlAffinity: ListTileControlAffinity.leading,
              contentPadding: EdgeInsets.all(5),
              value: isReceiveNotification,
              onChanged: (value) {
                setState(() {
                  isReceiveNotification = !isReceiveNotification!;
                });

                //call api here
                if (value!) {
                  SehatRepositories.setNotification('0');
                } else {
                  SehatRepositories.setNotification('1');
                }

                Flushbar(
                  borderColor: Colors.black,
                  animationDuration: Duration(milliseconds: 250),
                  backgroundColor: Colors.white,
                  borderWidth: 2,
                  icon: Icon(FontAwesomeIcons.exclamation),
                  // message: LoremText,
                  // title: 'Permission needed to continue',
                  titleText: Text(
                    isReceiveNotification!
                        ? 'will_receive_noti'.tr()
                        : 'will_not_receive_noti'.tr(),
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                    ),
                  ),
                  messageText: Text(
                    isReceiveNotification!
                        ? 'will_receive_noti_desc'.tr()
                        : 'will_not_receive_noti_desc'.tr(),
                    style: TextStyle(color: Colors.black),
                  ),
                  mainButton: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Okay',
                      style: TextStyle(color: Colors.blue),
                    ),
                  ),
                  duration: Duration(seconds: 7),
                ).show(context);
              },
              title: Text(
                'tick_here_notification'.tr(),
                style: TextStyle(fontSize: 14),
              ),
              checkColor: Colors.white,
              activeColor: themeColor,
            ),
          ),
        ],
      ),
    );
  }
}
