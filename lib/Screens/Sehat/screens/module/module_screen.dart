import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:selangkah_new/Screens/Sehat/screens/act_welcome.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_screen.dart';
import 'package:selangkah_new/Screens/Sehat/screens/module/module_pathway.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class ModuleScreen extends StatefulWidget {
  const ModuleScreen({Key? key}) : super(key: key);

  @override
  _ModuleScreenState createState() => _ModuleScreenState();
}

class _ModuleScreenState extends State<ModuleScreen> {
  @override
  Widget build(BuildContext context) {
    String language =
        EasyLocalization.of(context)!.locale.languageCode == 'en' ? 'EN' : 'MY';

    context.read<GetModuleCubit>().getModule('7', language);

    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;

    return SehatScaffold(
      appBarTitle: 'Mental Health Module',
      child: Container(
        height: height - AppBar().preferredSize.height,
        width: width,
        child: BlocBuilder<GetModuleCubit, GetModuleState>(
          builder: (context, state) {
            if (state is GetModuleLoaded) {
              return GetModuleLoadedWidget(
                category: state.category,
                journey: state.journey,
              );
            } else {
              return SpinKitFadingCircle(
                color: kPrimaryColor,
                size: 14,
              );
            }
          },
        ),
      ),
    );
  }
}

class GetModuleLoadedWidget extends StatelessWidget {
  final Category category;
  final Journey journey;

  const GetModuleLoadedWidget(
      {Key? key, required this.category, required this.journey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height;
    // double width = MediaQuery.of(context).size.width;

    return GridView.builder(
      shrinkWrap: true,
      padding: EdgeInsets.all(15),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 15,
        mainAxisSpacing: 15,
        childAspectRatio: (9 / 18),
      ),
      itemCount: category.data!.length,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          onTap: () async {
            if (category.data![index].haveChild == 1) {
              //if chache null

              if (category.data![index].id == '8') {
                String? checkFirstTimeIact =
                    await SecureStorage().readSecureData('checkFirstTimeIact');

                //check dass
                int session = SehatGlobalVar.currentSession!;
                Journey journey = await SehatRepositories.getJourney(session);

                if (checkFirstTimeIact == null ||
                    !journey.data!.any(
                        (element) => element.idMentalQuestionCat == '13')) {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => IactWelcome(
                        categoryId: category.data![index].id!,
                        categoryName: category.data![index].categoryName!,
                      ),
                    ),
                  );
                } else {
                  Navigator.of(context)
                      .push(
                        MaterialPageRoute(
                          builder: (context) => BlocProvider(
                            create: (context) => GetPathwayCubit(),
                            child: ModulePathway(
                              title: category.data![index].categoryName!,
                              idModule: category.data![index].id!,
                            ),
                          ),
                        ),
                      )
                      .then((value) => context
                          .read<GetModuleCubit>()
                          .getModule('7', GlobalVariables.language!));
                }
              } else {
                String? checkFirstTimeAct =
                    await SecureStorage().readSecureData('checkFirstTimeAct');

                int session = SehatGlobalVar.currentSession!;
                Journey journey = await SehatRepositories.getJourney(session);
                if (checkFirstTimeAct == null ||
                    !journey.data!.any(
                        (element) => element.idMentalQuestionCat == '13')) {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ActWelcome(
                        categoryId: category.data![index].id!,
                        categoryName: category.data![index].categoryName!,
                      ),
                    ),
                  );
                } else {
                  Navigator.of(context)
                      .push(
                        MaterialPageRoute(
                          builder: (context) => BlocProvider(
                            create: (context) => GetPathwayCubit(),
                            child: ModulePathway(
                              title: category.data![index].categoryName!,
                              idModule: category.data![index].id!,
                            ),
                          ),
                        ),
                      )
                      .then((value) => context
                          .read<GetModuleCubit>()
                          .getModule('7', GlobalVariables.language!));
                }
              }
            } else {
              Navigator.of(context)
                  .push(
                    MaterialPageRoute(
                      builder: (context) => BlocProvider(
                        create: (context) => GetContentCubit(),
                        child: ContentScreen(
                          title: category.data![index].categoryName!,
                          idCategory: category.data![index].id!,
                          isQuestionnaire: true,
                        ),
                      ),
                    ),
                  )
                  .then((value) => context
                      .read<GetModuleCubit>()
                      .getModule('7', GlobalVariables.language!));
            }
          },
          borderRadius: BorderRadius.circular(20),
          child: Stack(
            fit: StackFit.expand,
            children: [
              if (journey.data!.any((element) =>
                  element.idMentalQuestionCat == category.data![index].id))
                Positioned(
                  top: 10,
                  left: 10,
                  child: Icon(
                    FontAwesomeIcons.solidCircleCheck,
                    color: Color(0xFF60B05E),
                    size: 15,
                  ),
                ),
              Ink(
                height: 100,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0.0, 1.0), //(x,y)
                      blurRadius: 4.0,
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    SizedBox(height: 30),
                    Text(
                      category.data![index].categoryName!,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 20),
                    Expanded(
                      child: Text(
                        category.data![index].description!,
                        style: TextStyle(
                          // fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff686868),
                        ),
                        textAlign: TextAlign.center,
                        // overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

//if child
// Navigator.of(context).push(
//                     MaterialPageRoute(
//                       builder: (context) => BlocProvider(
//                         create: (context) => GetPathwayCubit(),
//                         child: ModulePathway(
//                           title: category.data[index].categoryName,
//                           idModule: category.data[index].id,
//                         ),
//                       ),
//                     ),
//                   );

//if no child
// Navigator.of(context).push(
//                 MaterialPageRoute(
//                   builder: (context) => BlocProvider(
//                     create: (context) => GetContentCubit(),
//                     child: ContentScreen(
//                       title: category.data[index].categoryName,
//                       idCategory: category.data[index].id,
//                       isQuestionnaire: true,
//                     ),
//                   ),
//                 ),
//               );
