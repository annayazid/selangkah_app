import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/Sehat/models/psyeducation.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

List<PsyEducations> psycho101 = [];
List<PsyEducations> selfHelpCoping = [];
List<PsyEducations> seekHelp = [];
List<PsyEducations> mco = [];

List<PsyEducations> psycho = [];
List<PsyEducations> population = [];
List<PsyEducations> work = [];
List<PsyEducations> self = [];
List<PsyEducations> family = [];
List<PsyEducations> addiction = [];
List<PsyEducations> disorder = [];

String? data1,
    data2,
    data3,
    data4,
    data5,
    data6,
    data7,
    data8,
    data9,
    data10,
    data11;
bool showPsyEduDialog = false;

Future<bool> setVidStatus(idVideo) async {
  bool status = false;
  SecureStorage secureStorage = SecureStorage();
  String id = await secureStorage.readSecureData('userId');
  int session = SehatGlobalVar.currentSession!;

  final Map<String, String> map = {
    'id_video': idVideo,
    'token': TOKEN,
    'id_selangkah_user': id,
    'id_session': '$session',
  };

  //print('calling set vid status');

  final response = await http.post(
    Uri.parse('$API_URL_MENTAL/set_vid_status'),
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    body: map,
  );

  var data = json.decode(response.body);

  if (data['Data'] != 'no data') {
    status = true;
  }

  return status;
}

checkWatchedVideo() {
  bool stat = false;
  bool stat1 = false;
  bool stat2 = false;
  bool stat3 = false;
  bool stat4 = false;
  bool stat5 = false;
  bool stat6 = false;
  bool stat7 = false;
  bool stat8 = false;
  bool stat9 = false;
  bool stat10 = false;

  if (stat == true &&
      stat1 == true &&
      stat2 == true &&
      stat3 == true &&
      stat4 == true &&
      stat5 == true &&
      stat6 == true &&
      stat7 == true &&
      stat8 == true &&
      stat9 == true &&
      stat10 == true) {
    showPsyEduDialog = true;
  }
}
