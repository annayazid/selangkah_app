import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:selangkah_new/Screens/Sehat/screens/components/components.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/video_services.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

import 'package:url_launcher/url_launcher.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:http/http.dart' as http;

class PsyEduPage1 extends StatefulWidget {
  const PsyEduPage1({Key? key}) : super(key: key);

  @override
  _PsyEduPage1State createState() => _PsyEduPage1State();
}

class _PsyEduPage1State extends State<PsyEduPage1> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await getVidStatus();
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Container(
              width: width * 0.9,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: themeColor),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            UnconstrainedBox(
              child: Container(
                width: width * 0.6,
                padding: EdgeInsets.symmetric(
                  vertical: 5,
                  horizontal: 10,
                ),
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 2.5,
                    color: Colors.grey[600]!,
                  ),
                  borderRadius: BorderRadius.circular(20),
                ),
                alignment: Alignment.center,
                child: Text(
                  data1!.toUpperCase(),
                  style: TextStyle(
                    fontSize: width * 0.045,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[600],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: width * 0.85,
              decoration: BoxDecoration(
                color: themeColor,
                borderRadius: BorderRadius.circular(20),
              ),
              padding: EdgeInsets.symmetric(
                vertical: 15,
                horizontal: 30,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 5,
                  ),
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: psycho101.length,
                    itemBuilder: (context, index) {
                      return buildRow(
                        width,
                        psycho101[index].name,
                        psycho101[index].displayNo! + '.',
                        psycho101[index].status == '1'
                            ? buildWatched(
                                width,
                                () async {
                                  await launchUrl(
                                      Uri.parse(psycho101[index].url!),
                                      mode: LaunchMode.externalApplication);
                                  // launch(psycho101[index].url);
                                },
                              )
                            : buildWatch(
                                width,
                                () async {
                                  bool stat =
                                      await setVidStatus(psycho101[index].id);
                                  if (stat == true) {
                                    getVidStatus();

                                    await launchUrl(
                                      Uri.parse(psycho101[index].url!),
                                      mode: LaunchMode.externalApplication,
                                    );

                                    // launch(psycho101[index].url);
                                  }
                                },
                              ),
                      );
                    },
                  ),
                  SizedBox(
                    height: 25,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  getVidStatus() async {
    String language = EasyLocalization.of(context)!.locale.languageCode;
    int session = SehatGlobalVar.currentSession!;
    SecureStorage secureStorage = SecureStorage();

    if (language == 'ms') {
      setState(() {
        language = 'MY';
      });
    } else {
      setState(() {
        language = 'EN';
      });
    }

    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
      'id_selangkah_user': await secureStorage.readSecureData('userId'),
      'id_session': '$session'
    };

    //print('calling get vid status');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_vid_status'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    var data = json.decode(response.body);

    print(data1);

    if (data['Data'] != 'no data') {
      if (data['Data'][data1] != null) {
        for (var datas in data['Data'][data1]) {
          for (int i = 0; i < psycho101.length; i++) {
            if (datas['id'] == psycho101[i].id) {
              setState(() {
                psycho101[i].status = datas['status'];
              });
            }
          }
        }
      }
    }
    checkWatchedVideo();
  }
}
