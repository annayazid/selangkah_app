import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/Sehat/screens/components/components.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/video_services.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';
import 'package:url_launcher/url_launcher.dart';

class PsyEduPage5 extends StatefulWidget {
  const PsyEduPage5({Key? key}) : super(key: key);

  @override
  _PsyEduPage5State createState() => _PsyEduPage5State();
}

class _PsyEduPage5State extends State<PsyEduPage5> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await getVidStatus('init');
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Container(
              width: width * 0.9,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: themeColor),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                  Container(
                    width: width / 13,
                    height: 7,
                    decoration: BoxDecoration(color: indicatorUnselected),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            UnconstrainedBox(
              child: Container(
                width: width * 0.6,
                padding: EdgeInsets.symmetric(
                  vertical: 5,
                  horizontal: 10,
                ),
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 2.5,
                    color: Colors.grey[600]!,
                  ),
                  borderRadius: BorderRadius.circular(20),
                ),
                alignment: Alignment.center,
                child: Text(
                  data5!.toUpperCase(),
                  style: TextStyle(
                    fontSize: width * 0.045,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[600],
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: width * 0.85,
              decoration: BoxDecoration(
                color: themeColor,
                borderRadius: BorderRadius.circular(20),
              ),
              padding: EdgeInsets.symmetric(
                vertical: 15,
                horizontal: 30,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 5,
                  ),
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: psycho.length,
                    itemBuilder: (context, index) {
                      return buildRow(
                        width,
                        psycho[index].name,
                        psycho[index].displayNo! + '.',
                        psycho[index].status == '1'
                            ? buildWatched(
                                width,
                                () async {
                                  await launchUrl(
                                    Uri.parse(psycho[index].url!),
                                    mode: LaunchMode.externalApplication,
                                  );

                                  // await launch(psycho[index].url);
                                },
                              )
                            : buildWatch(
                                width,
                                () async {
                                  bool stat =
                                      await setVidStatus(psycho[index].id);
                                  if (stat == true) {
                                    getVidStatus('func');
                                    await launchUrl(
                                      Uri.parse(psycho[index].url!),
                                      mode: LaunchMode.externalApplication,
                                    );
                                    // await launch(psycho[index].url);
                                  }
                                },
                              ),
                      );
                    },
                  ),
                  SizedBox(
                    height: 25,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  getVidStatus(from) async {
    String language = EasyLocalization.of(context)!.locale.languageCode;
    int session = SehatGlobalVar.currentSession!;
    SecureStorage secureStorage = SecureStorage();

    if (language == 'ms') {
      setState(() {
        language = 'MY';
      });
    } else {
      setState(() {
        language = 'EN';
      });
    }

    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
      'id_selangkah_user': await secureStorage.readSecureData('userId'),
      'id_session': '$session'
    };

    //print('calling get vid status');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_vid_status'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    var data = json.decode(response.body);

    if (data['Data'] != 'no data') {
      if (data['Data'][data5] != null) {
        for (var datas in data['Data'][data5]) {
          for (int i = 0; i < psycho.length; i++) {
            if (datas['id'] == psycho[i].id) {
              setState(() {
                psycho[i].status = datas['status'];
              });
            }
          }
        }
      }
    }
    if (from == 'func') {
      checkWatchedVideo();
    }
  }
}
