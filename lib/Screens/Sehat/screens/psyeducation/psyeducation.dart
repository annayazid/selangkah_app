import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/Sehat/models/psyeducation.dart';
import 'package:selangkah_new/Screens/Sehat/screens/components/components.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/page1.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/page10.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/page11.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/page2.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/page3.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/page4.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/page5.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/page6.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/page7.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/page8.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/page9.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/video_services.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class PsyEducation extends StatefulWidget {
  final int? index;
  const PsyEducation({Key? key, this.index}) : super(key: key);

  @override
  _PsyEducationState createState() => _PsyEducationState();
}

class _PsyEducationState extends State<PsyEducation> {
  bool isloading = true;
  int _currentIndex = 0;

  Widget? callPage(int currentIndex) {
    switch (currentIndex) {
      case 0:
        return PsyEduPage1();
      case 1:
        return PsyEduPage2();
      case 2:
        return PsyEduPage3();
      case 3:
        return PsyEduPage4();
      case 4:
        return PsyEduPage5();
      case 5:
        return PsyEduPage6();
      case 6:
        return PsyEduPage7();
      case 7:
        return PsyEduPage8();
      case 8:
        return PsyEduPage9();
      case 9:
        return PsyEduPage10();
      case 10:
        return PsyEduPage11();

      default:
        return null;
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await getVidUrl();
      // await getVidStatus();
    });
  }

  getVidUrl() async {
    psycho101.clear();
    selfHelpCoping.clear();
    seekHelp.clear();
    mco.clear();
    psycho.clear();
    population.clear();
    work.clear();
    self.clear();
    family.clear();
    addiction.clear();
    disorder.clear();

    //
    data1 = '';
    data2 = '';
    data3 = '';
    data4 = '';
    data5 = '';
    data6 = '';
    data7 = '';
    data8 = '';
    data9 = '';
    data10 = '';
    data11 = '';

    String language = EasyLocalization.of(context)!.locale.languageCode;
    if (language == 'ms') {
      setState(() {
        language = 'MY';
        data1 = 'Psikopendidikan 101';
        data2 = 'Tips Usaha Diri / Daya Tindak';
        data3 = 'Cara Mengatasi';
        data4 = 'Video berkaitan PKP / Video related with MCO';
        data5 = 'Psikopendidikan ';
        data6 = 'Populasi Spesifik';
        data7 = 'Tempat Kerja';
        data8 = 'Tips Bantu Kendiri';
        data9 = 'Keluarga';
        data10 = 'Ketagihan';
        data11 = 'Gangguan Tertentu';
      });
    } else {
      setState(() {
        language = 'EN';
        data1 = 'Psychoeducation 101';
        data2 = 'Self-Help / Coping Tips';
        data3 = 'Seeking Help';
        data4 = 'MCO-Related Videos';
        data5 = 'Psychoeducation ';
        data6 = 'Specific Population';
        data7 = 'Workplace';
        data8 = 'Self Help';
        data9 = 'Family';
        data10 = 'Addiction';
        data11 = 'Specific Disorders';
      });
    }
    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
    };

    print('calling get vid');

    final responseSo = await http.post(
      Uri.parse('$API_URL_MENTAL/get_vid_url'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    var data = json.decode(responseSo.body);
    //fix here

    var psycho101s = data['Data'][data1];
    var selfHelpCopings = data['Data'][data2];
    var seekHelps = data['Data'][data3];
    var mcos = data['Data'][data4];
    var psychos = data['Data'][data5];
    var populations = data['Data'][data6];
    var works = data['Data'][data7];
    var selfs = data['Data'][data8];
    var familys = data['Data'][data9];
    var addictions = data['Data'][data10];
    var disorders = data['Data'][data11];

    //fix here
    if (psycho101.isEmpty) {
      for (var datas in psycho101s) {
        psycho101.add(PsyEducations.fromJson(datas));
      }
    }

    if (selfHelpCoping.isEmpty) {
      for (var datas in selfHelpCopings) {
        selfHelpCoping.add(PsyEducations.fromJson(datas));
      }
    }

    if (seekHelp.isEmpty) {
      for (var datas in seekHelps) {
        seekHelp.add(PsyEducations.fromJson(datas));
      }
    }

    if (mco.isEmpty) {
      for (var datas in mcos) {
        mco.add(PsyEducations.fromJson(datas));
      }
    }

    if (psycho.isEmpty) {
      for (var datas in psychos) {
        psycho.add(PsyEducations.fromJson(datas));
      }
    }

    if (population.isEmpty) {
      for (var datas in populations) {
        population.add(PsyEducations.fromJson(datas));
      }
    }

    if (work.isEmpty) {
      for (var datas in works) {
        work.add(PsyEducations.fromJson(datas));
      }
    }

    if (self.isEmpty) {
      for (var datas in selfs) {
        self.add(PsyEducations.fromJson(datas));
      }
    }

    if (family.isEmpty) {
      for (var datas in familys) {
        family.add(PsyEducations.fromJson(datas));
      }
    }

    if (addiction.isEmpty) {
      for (var datas in addictions) {
        addiction.add(PsyEducations.fromJson(datas));
      }
    }

    if (disorder.isEmpty) {
      for (var datas in disorders) {
        disorder.add(PsyEducations.fromJson(datas));
      }
    }

    // if (population.isEmpty) {
    //   for (var datas in populations) {
    //     population.add(PsyEducations.fromJson(datas));
    //   }
    // }

    // if (work.isEmpty) {
    //   for (var datas in works) {
    //     work.add(PsyEducations.fromJson(datas));
    //   }
    // }

    // if (self.isEmpty) {
    //   for (var datas in selfhelps) {
    //     self.add(PsyEducations.fromJson(datas));
    //   }
    // }

    // if (seek.isEmpty) {
    //   for (var datas in seekhelps) {
    //     seek.add(PsyEducations.fromJson(datas));
    //   }
    // }

    // if (family.isEmpty) {
    //   for (var datas in families) {
    //     family.add(PsyEducations.fromJson(datas));
    //   }
    // }

    // if (addiction.isEmpty) {
    //   for (var datas in addictions) {
    //     addiction.add(PsyEducations.fromJson(datas));
    //   }
    // }

    // if (disorder.isEmpty) {
    //   for (var datas in disorders) {
    //     disorder.add(PsyEducations.fromJson(datas));
    //   }
    // }

    setState(() {
      isloading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return SehatScaffold(
      appBarTitle: 'psychoeducation_videos'.tr().toString(),
      child: isloading == true
          ? Container(
              height: height - AppBar().preferredSize.height,
              width: width,
              child: SpinKitFadingCircle(
                color: kPrimaryColor,
                size: 50,
              ),
            )
          : Container(
              height: height - AppBar().preferredSize.height,
              width: width,
              child: Stack(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      children: [
                        callPage(_currentIndex)!,
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          width: width * 0.85,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Visibility(
                                visible: (_currentIndex == 0) ? false : true,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: themeColor,
                                    shape: BoxShape.circle,
                                  ),
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.arrow_back,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _currentIndex--;
                                      });
                                    },
                                  ),
                                ),
                              ),
                              Visibility(
                                visible: _currentIndex == 10 ? false : true,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: themeColor,
                                    shape: BoxShape.circle,
                                  ),
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.arrow_forward,
                                      color: Colors.white,
                                    ),
                                    onPressed: () async {
                                      if (_currentIndex < 11) {
                                        setState(() {
                                          _currentIndex++;
                                        });
                                      }
                                    },
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 20),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: showPsyEduDialog,
                    child: buildPsyEduDialog(
                      height,
                      width,
                      'congratulations'.tr(),
                      () {
                        setState(() {
                          showPsyEduDialog = false;
                        });
                      },
                      'watched_all_videos'.tr(),
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  getVidStatus() async {
    String language = EasyLocalization.of(context)!.locale.languageCode;
    int session = SehatGlobalVar.currentSession!;
    SecureStorage secureStorage = SecureStorage();

    if (language == 'ms') {
      setState(() {
        language = 'MY';
      });
    } else {
      setState(() {
        language = 'EN';
      });
    }

    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
      'id_selangkah_user': await secureStorage.readSecureData('userId'),
      'id_session': '$session'
    };

    //print('calling get vid status');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_vid_status'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    var data = json.decode(response.body);

    if (data['Data'] != 'no data') {
      for (var datas in data['Data'][data1]) {
        for (int i = 0; i < psycho.length; i++) {
          if (datas['id'] == psycho[i].id) {
            setState(() {
              psycho[i].status = datas['status'];
            });
          }
        }
      }
    }
    checkWatchedVideo();
  }
}
