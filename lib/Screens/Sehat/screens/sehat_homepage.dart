import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:selangkah_new/Screens/Sehat/screens/confirm_identity/confirmIdentity.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_screen.dart';
import 'package:selangkah_new/Screens/Sehat/screens/module/module_screen.dart';
import 'package:selangkah_new/Screens/Sehat/screens/psyeducation/psyeducation.dart';
import 'package:selangkah_new/Screens/Sehat/screens/questionnaire/questionnaire_screen.dart';
import 'package:selangkah_new/Screens/Sehat/screens/whatsdoc/whatsdoc.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:selangkah_new/utils/global.dart';

class SehatHomepage extends StatefulWidget {
  const SehatHomepage({Key? key}) : super(key: key);

  @override
  _SehatHomepageState createState() => _SehatHomepageState();
}

class _SehatHomepageState extends State<SehatHomepage> {
  bool ratingDialog = false;
  bool commentBox = false;
  double ratingValue = 5;

  final nameController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    GlobalFunction.screenJourney('18');
    nameController.text = '';
    super.initState();
  }

  void sendFeedbackForm() {
    if (commentBox) {
      if (_formKey.currentState!.validate()) {
        // No any error in validation
        _formKey.currentState!.save();

        print(nameController.text);
        print(ratingValue);

        context
            .read<CommentCubit>()
            .sendFeedback(ratingValue, nameController.text);
      }
    } else {
      print(nameController.text);
      print(ratingValue);

      context
          .read<CommentCubit>()
          .sendFeedback(ratingValue, nameController.text);
    }
  }

  void showThankYouDialog() {
    Alert(
      context: context,
      style: AlertStyle(
        animationType: AnimationType.grow,
        isCloseButton: false,
      ),

      // type: AlertType.warning,
      title: 'thank_you_feedback'.tr(),
      onWillPopActive: true,
      buttons: [
        DialogButton(
          child: Text(
            'Okay',
            style: TextStyle(
              color: Colors.white,
              fontSize: 15,
            ),
            textAlign: TextAlign.center,
          ),
          onPressed: () {
            SehatGlobalVar.journeyFirst = true;
            Navigator.of(context).pop();
            Navigator.of(context).pop();
          },
          color: kPrimaryColor,
        ),
      ],
    ).show();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () async {
        // Navigator.of(context).pop();
        // return showBackDialog();

        Journey journey =
            await SehatRepositories.getJourney(SehatGlobalVar.currentSession!);
        print(SehatGlobalVar.journeyGlobal);
        print(journey);
        if (identical(journey, SehatGlobalVar.journeyGlobal)) {
          print('test');
          return true;
        } else {
          print('tes1t');
          setState(() {
            ratingDialog = true;
          });
          return false;
        }

        // if (journey.data[0].status1 ==
        //         SehatGlobalVar.journeyGlobal.data[0].status1 &&
        //     journey.data[0].status2 ==
        //         SehatGlobalVar.journeyGlobal.data[0].status2 &&
        //     journey.data[0].status3 ==
        //         SehatGlobalVar.journeyGlobal.data[0].status3 &&
        //     journey.data[0].status4 ==
        //         SehatGlobalVar.journeyGlobal.data[0].status4 &&
        //     journey.data[0].status5 ==
        //         SehatGlobalVar.journeyGlobal.data[0].status5 &&
        //     journey.data[0].status6 ==
        //         SehatGlobalVar.journeyGlobal.data[0].status6) {
        //   return true;
        // } else {
        //   setState(() {
        //     ratingDialog = true;
        //   });
        //   return false;
        // }
      },
      child: SehatScaffold(
        appBarTitle: 'Selangor Mental Sehat',
        child: Container(
          height: height - AppBar().preferredSize.height,
          width: width,
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    SehatBanner(),
                    SehatInformation(),
                  ],
                ),
              ),
              Visibility(
                visible: ratingDialog,
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      ratingDialog = false;
                    });
                  },
                  child: Container(
                    width: double.infinity,
                    height: double.infinity,
                    color: Colors.black.withOpacity(0.7),
                    child: Center(
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 15),
                        // height: height * 0.5,
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 0.5,
                              color: Colors.grey,
                              spreadRadius: 0.5,
                            ),
                          ],
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          // mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              'sehat_feedback_title'.tr(),
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 17,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: 20),
                            RatingBar.builder(
                              initialRating: 5,
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: false,
                              itemCount: 5,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 4.0),
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (rating) {
                                setState(() {
                                  ratingValue = rating;
                                });
                                if (rating <= 3) {
                                  setState(() {
                                    commentBox = true;
                                  });
                                } else {
                                  setState(() {
                                    commentBox = false;
                                  });
                                }
                              },
                            ),
                            if (commentBox)
                              SizedBox(
                                width: width * 0.7,
                                child: Form(
                                  key: _formKey,
                                  child: TextFormField(
                                    controller: nameController,
                                    decoration: InputDecoration(
                                        hintText: 'tell_comment'.tr()),
                                    validator: (value) =>
                                        value == '' ? 'mandatory'.tr() : null,
                                  ),
                                ),
                              ),
                            SizedBox(height: 20),
                            SizedBox(
                              width: width * 0.4,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: kPrimaryColor,
                                ),
                                onPressed: () {
                                  sendFeedbackForm();
                                },
                                child: BlocConsumer<CommentCubit, CommentState>(
                                  listener: (context, state) {
                                    if (state is CommentSent) {
                                      // Navigator.of(context).pop();
                                      setState(() {
                                        ratingDialog = false;
                                      });

                                      showThankYouDialog();
                                    }
                                  },
                                  builder: (context, state) {
                                    if (state is CommentInitial ||
                                        state is CommentSent) {
                                      return Text('submit'.tr());
                                    } else {
                                      return SpinKitFadingCircle(
                                        color: Colors.white,
                                        size: 15,
                                      );
                                    }
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SehatInformation extends StatefulWidget {
  const SehatInformation({
    Key? key,
  }) : super(key: key);

  @override
  _SehatInformationState createState() => _SehatInformationState();
}

class _SehatInformationState extends State<SehatInformation> {
  @override
  void initState() {
    context.read<GetHomepageCubit>().getHomepage();
    super.initState();
  }

  void showStressDialog(int stressScore) {
    Alert(
      closeFunction: () {
        SehatGlobalVar.dismissedPopUpStress = true;
      },
      context: context,
      style: AlertStyle(
        titleStyle: TextStyle(
          color: Color(0xFF515660),
          fontSize: 15,
        ),
        isCloseButton: false,
      ),
      content: Container(
        height: 150,
        child: Column(
          children: [
            // Text(
            //   '35/40',
            //   style: TextStyle(
            //     color: themeColor,
            //   ),
            // ),
            SizedBox(height: 10),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: '$stressScore',
                    style: TextStyle(
                      color: themeColor,
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: '/40',
                    style: TextStyle(
                      color: themeColor,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Row(
                children: [
                  Icon(
                    FontAwesomeIcons.solidCircleCheck,
                    color: themeColor,
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    child: Text(
                      'recommended_watch_video'.tr(),
                      style: TextStyle(
                        color: Color(0xFF676B74),
                        fontSize: 15,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Row(
                children: [
                  Icon(
                    FontAwesomeIcons.solidCircleCheck,
                    color: themeColor,
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    child: Text(
                      'recommended_whastdoc'.tr(),
                      style: TextStyle(
                        color: Color(0xFF676B74),
                        fontSize: 15,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      image: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Image.asset(
          "assets/images/sehat/notification_stress.png",
          height: 75,
        ),
      ),
      title: 'your_stress_score'.tr(),
      buttons: [
        DialogButton(
          child: Text(
            'OK',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            SehatGlobalVar.dismissedPopUpStress = true;
            Navigator.of(context).pop();
          },
          color: themeColor,
          radius: BorderRadius.circular(20),
        ),
      ],
    ).show();
  }

  void showMentalDialog() {
    Alert(
      closeFunction: () {
        SehatGlobalVar.dismissedPopUpMental = true;
      },
      context: context,
      style: AlertStyle(
        titleStyle: TextStyle(
          color: Color(0xFF515660),
          fontSize: 15,
        ),
        isCloseButton: false,
      ),
      content: Container(
        height: 50,
        child: Column(
          children: [
            // Text(
            //   '35/40',
            //   style: TextStyle(
            //     color: themeColor,
            //   ),
            // ),
            Expanded(
              child: Row(
                children: [
                  Icon(
                    FontAwesomeIcons.solidCircleCheck,
                    color: themeColor,
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    child: Text(
                      'recommended_mental'.tr(),
                      style: TextStyle(
                        color: Color(0xFF676B74),
                        fontSize: 15,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      image: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Image.asset(
          "assets/images/sehat/notification_stress.png",
          height: 75,
        ),
      ),
      title: '',
      buttons: [
        DialogButton(
          child: Text(
            'OK',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            SehatGlobalVar.dismissedPopUpMental = true;
            Navigator.of(context).pop();
          },
          color: themeColor,
          radius: BorderRadius.circular(20),
        ),
      ],
    ).show();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      // height: 475,
      margin: EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 20,
      ),
      padding: EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 15,
      ),
      decoration: BoxDecoration(
        color: Color(0xFFF0EFF0),
        borderRadius: BorderRadius.circular(20),
        border: Border.all(
          color: Color(0xFF438C8B),
          width: 3,
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 3,
            offset: Offset(0, 5), // changes position of shadow
          ),
        ],
      ),
      child: BlocConsumer<GetHomepageCubit, GetHomepageState>(
        listener: (context, state) {
          if (state is GetHomepagePopUpStress) {
            showStressDialog(state.totalStress);
          } else if (state is GetHomepagePopUpMental) {
            showMentalDialog();
          }
        },
        builder: (context, state) {
          if (state is GetHomepageLoaded) {
            return HomepageLoadedWidget(
              journey: state.journey,
              name: state.name,
              imgData: state.imgData,
              status: state.status,
              category: state.category,
              score: state.score,
              scorePanel: state.scorePanel,
              notiText: state.notiText,
              literacyScore: state.literacyScore,
              slotAppointment: state.slotAppointment,
              gotCertificate: state.gotCertificate,
            );
          } else {
            return Container(
              height: 475,
              child: SpinKitFadingCircle(
                color: themeColor,
                size: 30,
              ),
            );
          }
        },
      ),
    );
  }
}

class HomepageLoadedWidget extends StatelessWidget {
  final Journey? journey;
  final String? name;
  final Uint8List? imgData;
  final String? status;
  final Category? category;
  final Score? score;
  final List<ScorePanel?>? scorePanel;
  final String? notiText;
  final String? literacyScore;
  final SlotAppointment? slotAppointment;
  final bool? gotCertificate;

  const HomepageLoadedWidget({
    Key? key,
    this.journey,
    this.name,
    this.imgData,
    this.status,
    this.category,
    this.score,
    this.scorePanel,
    this.notiText,
    this.literacyScore,
    this.slotAppointment,
    this.gotCertificate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(literacyScore);
    void showRestartDialog() {
      Alert(
        context: context,
        type: AlertType.warning,
        title: 'are_you_sure_restart'.tr(),
        buttons: [
          DialogButton(
            child: Text(
              'yes'.tr(),
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () {
              context.read<GetHomepageCubit>().updateSession();
              Navigator.of(context).pop();
            },
            color: Colors.green,
            radius: BorderRadius.circular(10),
          ),
          DialogButton(
            splashColor: Colors.red,
            child: Text(
              'no'.tr(),
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
            color: Colors.red,
            radius: BorderRadius.circular(10),
          ),
        ],
      ).show();
    }

    // double width = MediaQuery.of(context).size.width;
    return Column(
      children: [
        CircleAvatar(
          backgroundImage: () {
            if (imgData == null) {
              return AssetImage('assets/images/Asas/male_avatar.png');
            } else {
              return MemoryImage(imgData!);
            }
          }() as ImageProvider,
          backgroundColor: Colors.blueGrey,
          radius: 40,
        ),
        SizedBox(height: 10),

        Text(
          name!,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),

        SizedBox(height: 15),

        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Status: '),
            Container(
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: Color(0xFF438C8B),
                borderRadius: BorderRadius.circular(7.5),
              ),
              child: Text(
                status!,
                style: TextStyle(
                  color: Colors.white,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),

        SizedBox(height: 15),
        StaggeredGrid.count(
          children: scorePanel!.map((e) {
            return Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                children: [
                  Text(
                    e!.title!,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Divider(color: Colors.black),
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: e.score!.length,
                    itemBuilder: (BuildContext context, int indexList) {
                      return Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                '${e.score![indexList].name} : ',
                                style: TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 5,
                                    vertical: 3,
                                  ),
                                  decoration: BoxDecoration(
                                    color: e.score![indexList].color,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Text(
                                    e.score![indexList].level!,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 10,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                        ],
                      );
                    },
                  ),
                ],
              ),
            );
          }).toList(),
          // physics: NeverScrollableScrollPhysics(),
          // shrinkWrap: true,
          // staggeredTileBuilder: (index) => StaggeredTile.fit(1),
          crossAxisCount: 2,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5,
          // itemCount: scorePanel!.length,
          // itemBuilder: (context, index) {
          //   // return Container(
          //   //   margin: EdgeInsets.all(10),
          //   //   color: Colors.red,
          //   // );

          // },
        ),

        // for (var i = 0; i < count; i++) {

        // }

        if (literacyScore != '' &&
            literacyScore != null &&
            literacyScore != '0') ...[
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('${'literacy_score'.tr()} : '),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 5,
                  vertical: 3,
                ),
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Text(
                  '$literacyScore/160',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
          SizedBox(height: 10),
        ],

        if (notiText != "") ...[
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Color(0xFFFFEB3A),
            ),
            child: Text(
              notiText!,
              // LoremText,
              textAlign: TextAlign.center,
            ),
          ),
        ],
        SizedBox(height: 10),

        Row(
          children: [
            //identity
            Expanded(
              child: InkWell(
                onTap: () {
                  String language =
                      EasyLocalization.of(context)!.locale.languageCode == 'en'
                          ? 'EN'
                          : 'MY';
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MultiBlocProvider(
                        providers: [
                          BlocProvider(
                            create: (context) => GetIdentityCubit(),
                          ),
                          BlocProvider(
                            create: (context) => SendIdentityCubit(),
                          ),
                          BlocProvider(
                            create: (context) => GetContentCubit(),
                          ),
                        ],
                        child: ConfirmIdentity(language: language),
                      ),
                      settings: RouteSettings(name: 'Confirm identity page'),
                    ),
                  ).then((value) =>
                      context.read<GetHomepageCubit>().getHomepage());
                },
                child: Section(
                  title: category!.data![0].categoryName!,
                  color: Color(0xFF91E3F5),
                  icon: 'assets/images/sehat/sehat_identity.png',
                  tick: journey!.data!
                          .any((element) => element.idMentalQuestionCat == '1')
                      ? true
                      : false,
                ),
              ),
            ),
            SizedBox(width: 10),

            //stress scale screening
            Expanded(
              child: InkWell(
                onTap: () {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (context) => ScaleStressScreening(),
                  //     settings: RouteSettings(name: 'Stress scale page'),
                  //   ),
                  // ).then((value) =>
                  //     context.read<GetHomepageCubit>().getHomepage());
                  Navigator.of(context)
                      .push(
                        MaterialPageRoute(
                          builder: (context) => BlocProvider(
                            create: (context) => GetModuleCubit(),
                            child: QuestionnaireScreen(
                              title: category!.data![1].categoryName!,
                            ),
                          ),
                        ),
                      )
                      .then((value) =>
                          context.read<GetHomepageCubit>().getHomepage());
                },
                child: Section(
                  title: category!.data![1].categoryName!,
                  color: Color(0xFFF8C863),
                  icon: 'assets/images/sehat/sehat_questionaire.png',
                  tick: journey!.data!.any((element) =>
                              element.idMentalQuestionCat == '12') &&
                          journey!.data!.any((element) =>
                              element.idMentalQuestionCat == '13') &&
                          journey!.data!.any((element) =>
                              element.idMentalQuestionCat == '14') &&
                          journey!.data!.any(
                              (element) => element.idMentalQuestionCat == '15')
                      ? true
                      : false,
                ),
              ),
            ),
            SizedBox(width: 10),

            //risk screening
            Expanded(
              child: InkWell(
                onTap: () {
                  Navigator.of(context)
                      .push(
                        MaterialPageRoute(
                          builder: (context) => BlocProvider(
                            create: (context) => GetContentCubit(),
                            child: ContentScreen(
                              title: category!.data![2].categoryName!,
                              idCategory: '3',
                              isQuestionnaire: true,
                            ),
                          ),
                        ),
                      )
                      .then((value) =>
                          context.read<GetHomepageCubit>().getHomepage());
                },
                child: Section(
                  title: category!.data![2].categoryName!,
                  color: Color(0xFFFB983E),
                  icon: 'assets/images/sehat/sehat_risk.png',
                  tick: journey!.data!
                          .any((element) => element.idMentalQuestionCat == '3')
                      ? true
                      : false,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 10),

        //2nd row
        Row(
          children: [
            //moduleeee
            Expanded(
              child: InkWell(
                onTap: () {
                  Navigator.of(context)
                      .push(
                        MaterialPageRoute(
                          builder: (context) => BlocProvider(
                            create: (context) => GetModuleCubit(),
                            child: ModuleScreen(),
                          ),
                        ),
                      )
                      .then((value) =>
                          context.read<GetHomepageCubit>().getHomepage());
                },
                child: Section(
                  title: category!.data![3].categoryName!,
                  color: Color(0xFFFDA4B3),
                  icon: 'assets/images/sehat/sehat_module.png',
                  tick: journey!.data!
                          .any((element) => element.idMentalQuestionCat == '7')
                      ? true
                      : false,
                ),
              ),
            ),
            SizedBox(width: 10),

            //video
            Expanded(
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PsyEducation(),
                      settings: RouteSettings(name: 'Psyeducation page'),
                    ),
                  ).then((value) =>
                      context.read<GetHomepageCubit>().getHomepage());
                },
                child: Section(
                  title: category!.data![4].categoryName!,
                  color: Color(0xFFEB9BF2),
                  icon: 'assets/images/sehat/sehat_videos.png',
                  tick: journey!.data!.any(
                    (element) =>
                        element.idMentalQuestionCat == '4' ? true : false,
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),

            //whatsdoc
            Expanded(
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => BlocProvider(
                        create: (context) => GetWhatsdocCubit(),
                        child: WhatsDoc(),
                      ),
                      settings: RouteSettings(name: 'Whastdoc page'),
                    ),
                  ).then((value) =>
                      context.read<GetHomepageCubit>().getHomepage());
                },
                child: Section(
                  title: category!.data![5].categoryName!,
                  color: Color(0xFF88DED9),
                  icon: 'assets/images/sehat/sehat_hotline.png',
                  tick: journey!.data!
                          .any((element) => element.idMentalQuestionCat == '5')
                      ? true
                      : false,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 10),
        //mental health literacy
        Row(
          children: [
            Expanded(
              child: InkWell(
                onTap: () {
                  Navigator.of(context)
                      .push(
                        MaterialPageRoute(
                          builder: (context) => BlocProvider(
                            create: (context) => GetContentCubit(),
                            child: ContentScreen(
                              title: category!.data![6].categoryName!,
                              idCategory: '6',
                              isQuestionnaire: true,
                            ),
                          ),
                        ),
                      )
                      .then((value) =>
                          context.read<GetHomepageCubit>().getHomepage());
                },
                child: Section(
                  title: category!.data![6].categoryName!,
                  color: Color(0xFFADE780),
                  icon: 'assets/images/sehat/sehat_literacy.png',
                  tick: journey!.data!.any((element) =>
                      element.idMentalQuestionCat == '6' ? true : false),
                ),
              ),
            ),
            SizedBox(width: 10),

            //psychiatry
            // if (flagButton)

            () {
              if (category!.data!.any((element) => element.id == '16')) {
                return Expanded(
                  child: InkWell(
                    onTap: () {
                      //kalau dah pernah buat appointment
                      if (gotCertificate!) {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => MultiBlocProvider(
                              providers: [
                                BlocProvider(
                                  create: (context) => GetSehatClinicCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => GetSehatDateCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => SubmitCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => CertificateCubit(),
                                ),
                              ],
                              child: PsychiatryCertificate(),
                            ),
                          ),
                        );
                        //kalau tak pernah buat appointment
                      } else {
                        //kalau eligible
                        if (slotAppointment!.data!) {
                          //kalau ada slot available
                          if (slotAppointment!.slot == "") {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => MultiBlocProvider(
                                  providers: [
                                    BlocProvider(
                                      create: (context) =>
                                          GetSehatClinicCubit(),
                                    ),
                                    BlocProvider(
                                      create: (context) => GetSehatDateCubit(),
                                    ),
                                    BlocProvider(
                                      create: (context) => SubmitCubit(),
                                    ),
                                    BlocProvider(
                                      create: (context) => CertificateCubit(),
                                    ),
                                  ],
                                  child: PsychiatryCertificate(),
                                ),
                              ),
                            );

                            //kalau slot full
                          } else {
                            Alert(
                              context: context,
                              // style: AlertStyle(
                              //   isCloseButton: true,
                              // ),
                              type: AlertType.info,
                              title: 'slot_full_title'.tr(),
                              desc: 'slot_full_desc'.tr(),
                              onWillPopActive: false,
                              buttons: [
                                DialogButton(
                                  child: Text(
                                    'Okay',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  color: kPrimaryColor,
                                ),
                              ],
                            ).show();
                          }

                          //kalau tak eligible
                        } else {
                          Alert(
                            context: context,
                            // style: AlertStyle(
                            //   isCloseButton: true,
                            // ),
                            type: AlertType.info,
                            title: 'not_eligible'.tr(),
                            desc: 'noteligible_psikiatri'.tr(),
                            onWillPopActive: false,
                            buttons: [
                              DialogButton(
                                child: Text(
                                  'Okay',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                color: kPrimaryColor,
                              ),
                            ],
                          ).show();
                        }
                      }
                      // if (flagButton)
                      //   Navigator.of(context).push(
                      //     MaterialPageRoute(
                      //       builder: (context) => MultiBlocProvider(
                      //         providers: [
                      //           BlocProvider(
                      //             create: (context) => GetSehatClinicCubit(),
                      //           ),
                      //           BlocProvider(
                      //             create: (context) => GetSehatDateCubit(),
                      //           ),
                      //           BlocProvider(
                      //             create: (context) => SubmitCubit(),
                      //           ),
                      //           BlocProvider(
                      //             create: (context) => CertificateCubit(),
                      //           ),
                      //         ],
                      //         child: PsychiatryCertificate(),
                      //       ),
                      //     ),
                      //   );
                      // if (!flagButton) {
                      //   Alert(
                      //     context: context,
                      //     // style: AlertStyle(
                      //     //   isCloseButton: true,
                      //     // ),
                      //     type: AlertType.info,
                      //     title: 'not_eligible'.tr(),
                      //     desc: 'noteligible_psikiatri'.tr(),
                      //     onWillPopActive: false,
                      //     buttons: [
                      //       DialogButton(
                      //         child: Text(
                      //           'Okay',
                      //           style: TextStyle(
                      //             color: Colors.white,
                      //             fontSize: 15,
                      //           ),
                      //           textAlign: TextAlign.center,
                      //         ),
                      //         onPressed: () {
                      //           Navigator.of(context).pop();
                      //         },
                      //         color: kPrimaryColor,
                      //       ),
                      //     ],
                      //   ).show();
                      // }
                    },
                    child: Section(
                      title: category!.data![7].categoryName!,
                      // title: 'Psychiatry Subsidy Screening Registration',
                      color: Color(0xFF96414D),
                      icon: 'assets/images/sehat/sehat_psychiatry.png',
                      tick: journey!.data!.any(
                              (element) => element.idMentalQuestionCat == '8')
                          ? true
                          : false,
                    ),
                  ),
                );
              } else {
                return Spacer();
              }
            }(),

            SizedBox(width: 10),
            Spacer(),
          ],
        ),
        // if (journey.data[0].status6 == "1")
        if (journey!.data!.any((element) => element.idMentalQuestionCat == '6'))
          Column(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    showRestartDialog();
                  },
                  child: Container(
                    width: 100,
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      color: Colors.redAccent,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 2,
                          child: Center(
                            child: Text(
                              'restart'.tr(),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 13,
                              ),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Icon(
                            FontAwesomeIcons.arrowRotateRight,
                            size: 13,
                            color: Colors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
            ],
          ),
      ],
    );
  }
}

class Section extends StatelessWidget {
  final Color color;
  final String icon;
  final String title;
  final bool tick;

  const Section({
    Key? key,
    required this.color,
    required this.icon,
    required this.title,
    required this.tick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 120,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: color,
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Align(
                  alignment: Alignment.bottomRight,
                  child: Image.asset(
                    icon,
                    height: 40,
                  ),
                ),
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                  maxLines: 4,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
        ),

        //if tick true
        if (tick)
          Positioned(
            top: 5,
            left: 5,
            child: Icon(
              FontAwesomeIcons.solidCircleCheck,
              color: Color(0xFF60B05E),
              size: 15,
            ),
          ),
      ],
    );
  }
}

class SehatBanner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      height: 200,
      margin: EdgeInsets.only(
        left: 20,
        right: 20,
        top: 20,
        bottom: 10,
      ),
      padding: EdgeInsets.all(15),
      color: Color(0xFF438C8B),
      child: Row(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Flexible(
                  child: Text(
                    'update_mental_status'.tr(),
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                    ),
                    maxLines: 4,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(height: 10),
                Flexible(
                  child: Text(
                    'mental_package'.tr(),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                    maxLines: 5,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          ),
          Image.asset(
            'assets/images/sehat/sehat_banner.png',
            width: width * 0.55,
          ),
        ],
      ),
    );
  }
}
