import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';

class ContentOne extends StatefulWidget {
  final CardContent cardContent;
  final Function(
    String idQuestion,
    String answer,
    String idMentalContent,
    String contentType,
    bool canNext,
  ) updateParent;
  final String idModule;
  final List<String> answerTextList;
  final List<String> answerId;

  const ContentOne(
      {Key? key,
      required this.cardContent,
      required this.updateParent,
      required this.idModule,
      required this.answerTextList,
      required this.answerId})
      : super(key: key);

  @override
  State<ContentOne> createState() => _ContentOneState();
}

class _ContentOneState extends State<ContentOne> {
  int answerIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  // void init() {
  //   print('masuk init');
  //   widget.cardContent.data.choice.forEach((element) {
  //     answerTextList.add(element.answerValue);
  //     answerId.add(element.answerId);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      widget.updateParent(
        widget.cardContent.data!.id!,
        widget.answerId[answerIndex],
        widget.cardContent.idMentalContent!,
        widget.cardContent.contentType!,
        true,
      );
    });
    double width = MediaQuery.of(context).size.width;

    widget.answerTextList.forEach((element) {
      print(element);
    });

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text.rich(
          TextSpan(
            children: [
              TextSpan(text: '${'question'.tr()} '),
              TextSpan(text: widget.cardContent.data!.displayNo),

              if (widget.idModule == '12') TextSpan(text: ' ${'of'.tr()} 10'),

              if (widget.idModule == '13') TextSpan(text: ' ${'of'.tr()} 21'),

              if (widget.idModule == '14') TextSpan(text: ' ${'of'.tr()} 9'),

              if (widget.idModule == '15') TextSpan(text: ' ${'of'.tr()} 15'),

              if (widget.idModule == '6') TextSpan(text: ' ${'of'.tr()} 35'),

              // TextSpan(text: '${widget.totalQuestion}'),
            ],
            style: TextStyle(
              color: Colors.white,
              fontSize: 17,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        SizedBox(height: 20),
        Text(
          widget.cardContent.data!.questionText!,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        SizedBox(height: 10),
        if (widget.cardContent.data!.indicator != 'yesno' &&
            widget.cardContent.data!.indicator != 'multiple') ...[
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                widget.answerTextList[answerIndex],
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          SizedBox(height: 10),
        ],

        //rating number
        if (widget.cardContent.data!.indicator == 'ratingnumber') ...[
          Slider(
            inactiveColor: Colors.white,
            activeColor: Colors.white,
            value: answerIndex.toDouble(),
            onChanged: (value) {
              setState(() {
                answerIndex = value.toInt();
              });

              //String idQuestion,
              // String answer,
              // String idMentalContent,
              // String contentType
              widget.updateParent(
                widget.cardContent.data!.id!,
                widget.answerId[answerIndex],
                widget.cardContent.idMentalContent!,
                widget.cardContent.contentType!,
                true,
              );
            },
            min: 0,
            max: widget.cardContent.data!.choice!.length.toDouble() - 1,
            divisions: widget.cardContent.data!.choice!.length,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     children: widget.cardContent.data.choice.map((e) {
            //   return Expanded(
            //     child: Center(
            //       child: Text(
            //         '${widget.cardContent.data.choice.indexOf(e)}',
            //         style: TextStyle(
            //           color: Colors.white,
            //         ),
            //       ),
            //     ),
            //   );
            // }).toList()
            children: [
              Container(
                padding: EdgeInsets.only(left: 17),
                width: width * 0.3,
                child: Text(
                  '1',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(right: 17),
                width: width * 0.3,
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    '${widget.cardContent.data!.choice!.length}',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          )
        ],

        //rating wording
        if (widget.cardContent.data!.indicator == 'ratingwording') ...[
          Slider(
            inactiveColor: Colors.white,
            activeColor: Colors.white,
            value: answerIndex.toDouble(),
            onChanged: (value) {
              setState(() {
                answerIndex = value.toInt();
              });

              //String idQuestion,
              // String answer,
              // String idMentalContent,
              // String contentType
              widget.updateParent(
                widget.cardContent.data!.id!,
                widget.answerId[answerIndex],
                widget.cardContent.idMentalContent!,
                widget.cardContent.contentType!,
                true,
              );
            },
            min: 0,
            max: widget.cardContent.data!.choice!.length.toDouble() - 1,
            divisions: widget.cardContent.data!.choice!.length,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     children: widget.cardContent.data.choice.map((e) {
            //   return Expanded(
            //     child: Center(
            //       child: Text(
            //         '${widget.cardContent.data.choice.indexOf(e)}',
            //         style: TextStyle(
            //           color: Colors.white,
            //         ),
            //       ),
            //     ),
            //   );
            // }).toList()
            children: [
              Container(
                padding: EdgeInsets.only(left: 17),
                width: width * 0.3,
                child: Text(
                  widget.cardContent.data!.choice![0].answerValue!,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(right: 17),
                width: width * 0.3,
                child: Text(
                  widget
                      .cardContent
                      .data!
                      .choice![widget.cardContent.data!.choice!.length - 1]
                      .answerValue!,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.right,
                ),
              ),
            ],
          )
        ],

        //if yes no
        if (widget.cardContent.data!.indicator == 'yesno') ...[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    answerIndex = 0;
                    // print('hooo $val');
                  });
                },
                child: Container(
                  width: width * 0.3,
                  padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                  decoration: BoxDecoration(
                    color: answerIndex == 0 ? Colors.white : Color(0xFF8DB4B2),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    children: [
                      Radio(
                        activeColor: themeColor,
                        value: 0,
                        groupValue: answerIndex,
                        onChanged: (val) {
                          setState(() {
                            answerIndex = val!;
                            // print('hooo $val');
                          });
                          widget.updateParent(
                            widget.cardContent.data!.id!,
                            widget.answerId[answerIndex],
                            widget.cardContent.idMentalContent!,
                            widget.cardContent.contentType!,
                            true,
                          );
                        },
                      ),
                      Text(widget.cardContent.data!.choice![0].answerValue!),
                    ],
                  ),
                ),
              ),

              //
              GestureDetector(
                onTap: () {
                  setState(() {
                    answerIndex = 1;
                    // print('hooo $val');
                  });
                },
                child: Container(
                  width: width * 0.3,
                  padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                  decoration: BoxDecoration(
                    color: answerIndex == 1 ? Colors.white : Color(0xFF8DB4B2),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    children: [
                      Radio(
                        activeColor: themeColor,
                        value: 1,
                        groupValue: answerIndex,
                        onChanged: (val) {
                          setState(() {
                            answerIndex = val!;
                            // print('hooo $val');
                          });
                          widget.updateParent(
                            widget.cardContent.data!.id!,
                            widget.answerId[answerIndex],
                            widget.cardContent.idMentalContent!,
                            widget.cardContent.contentType!,
                            true,
                          );
                        },
                      ),
                      Text(widget.cardContent.data!.choice![1].answerValue!),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],

        //if multiple
        if (widget.cardContent.data!.indicator == 'multiple') ...[
          ListView.builder(
            shrinkWrap: true,
            itemCount: widget.cardContent.data!.choice!.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        answerIndex = index;
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                      decoration: BoxDecoration(
                        color: answerIndex == index
                            ? Colors.white
                            : Color(0xFF8DB4B2),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        children: [
                          Radio(
                            activeColor: themeColor,
                            value: index,
                            groupValue: answerIndex,
                            onChanged: (val) {
                              setState(() {
                                answerIndex = val!;
                                // print('hooo $val');
                              });
                              widget.updateParent(
                                widget.cardContent.data!.id!,
                                widget.answerId[answerIndex],
                                widget.cardContent.idMentalContent!,
                                widget.cardContent.contentType!,
                                true,
                              );
                            },
                          ),
                          Text(
                            widget
                                .cardContent.data!.choice![index].answerValue!,
                            style: TextStyle(
                              color: themeColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              );
            },
          ),
        ],

        if (widget.cardContent.data!.indicator == null) ...[
          Slider(
            inactiveColor: Colors.white,
            activeColor: Colors.white,
            value: answerIndex.toDouble(),
            onChanged: (value) {
              setState(() {
                answerIndex = value.toInt();
              });

              //String idQuestion,
              // String answer,
              // String idMentalContent,
              // String contentType
              widget.updateParent(
                widget.cardContent.data!.id!,
                widget.answerId[answerIndex],
                widget.cardContent.idMentalContent!,
                widget.cardContent.contentType!,
                true,
              );
            },
            min: 0,
            max: widget.cardContent.data!.choice!.length.toDouble() - 1,
            divisions: widget.cardContent.data!.choice!.length,
          ),
        ],
      ],
    );
  }
}
