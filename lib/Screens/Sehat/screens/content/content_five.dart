import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pinch_zoom/pinch_zoom.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';

class ContentFive extends StatefulWidget {
  final CardContent cardContent;
  final Function(
    String idQuestion,
    String answer,
    String idMentalContent,
    String contentType,
    bool canNext,
  ) updateParent;

  const ContentFive(
      {Key? key, required this.cardContent, required this.updateParent})
      : super(key: key);

  @override
  State<ContentFive> createState() => _ContentTwoState();
}

class _ContentTwoState extends State<ContentFive> {
  @override
  void initState() {
    // init();
    super.initState();
  }

  void init() async {
    widget.updateParent(
      widget.cardContent.data!.id!,
      '1',
      widget.cardContent.idMentalContent!,
      widget.cardContent.contentType!,
      true,
    );
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      widget.updateParent(
        widget.cardContent.data!.id!,
        '1',
        widget.cardContent.idMentalContent!,
        widget.cardContent.contentType!,
        true,
      );
    });
    // return
    // CachedNetworkImage(
    //   imageUrl: widget.cardContent.data.image,
    // );
    return Container(
      height: 250,
      child: PinchZoom(
        child: CachedNetworkImage(
          imageUrl: widget.cardContent.data!.image!,
        ),
        resetDuration: const Duration(milliseconds: 100),
        maxScale: 2.5,
        onZoomStart: () {
          print('Start zooming');
        },
        onZoomEnd: () {
          print('Stop zooming');
        },
      ),
    );
  }
}
