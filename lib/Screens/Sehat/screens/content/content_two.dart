import 'package:flutter/material.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:easy_localization/easy_localization.dart';

class ContentTwo extends StatefulWidget {
  final CardContent cardContent;
  final Function(
    String idQuestion,
    String answer,
    String idMentalContent,
    String contentType,
    bool canNext,
  ) updateParent;

  const ContentTwo(
      {Key? key, required this.cardContent, required this.updateParent})
      : super(key: key);

  @override
  State<ContentTwo> createState() => _ContentTwoState();
}

class _ContentTwoState extends State<ContentTwo> {
  @override
  void initState() {
    init();
    super.initState();
  }

  void init() async {
    widget.updateParent(
      widget.cardContent.data!.id!,
      '1',
      widget.cardContent.idMentalContent!,
      widget.cardContent.contentType!,
      true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.white,
          shape: StadiumBorder(),
        ),
        onPressed: () {
          widget.updateParent(
            widget.cardContent.data!.id!,
            '1',
            widget.cardContent.idMentalContent!,
            widget.cardContent.contentType!,
            true,
          );
          // launch(widget.cardContent.data.url);
          launchUrl(
            Uri.parse(widget.cardContent.data!.url!),
            mode: LaunchMode.externalApplication,
          );
        },
        child: Text(
          'watch'.tr().toUpperCase(),
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Color(0xFF438C8B),
          ),
        ),
      ),
    );
  }
}
