import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_screen.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';
import 'package:selangkah_new/utils/global.dart';

class ContentEight extends StatefulWidget {
  final CardContent cardContent;
  final Function(
    String idQuestion,
    String answer,
    String idMentalContent,
    String contentType,
    bool canNext,
  ) updateParent;

  const ContentEight(
      {Key? key, required this.cardContent, required this.updateParent})
      : super(key: key);

  @override
  State<ContentEight> createState() => _ContentEightState();
}

class _ContentEightState extends State<ContentEight> {
  @override
  void initState() {
    init();
    super.initState();
  }

  void init() async {
    // widget.updateParent(
    //   widget.cardContent.data.id,
    //   '1',
    //   widget.cardContent.idMentalContent,
    //   widget.cardContent.contentType,
    //   false,
    // );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.white,
          shape: StadiumBorder(),
        ),
        onPressed: () async {
          // launch(widget.cardContent.data.url);

          int? session = SehatGlobalVar.currentSession;
          Journey journey = await SehatRepositories.getJourney(session!);

          if (journey.data!.any((element) =>
              element.idMentalQuestionCat == '15' && element.score != '-1')) {
            Score score =
                await SehatRepositories.getScore(GlobalVariables.language!);
            for (var i = 0; i < score.data!.length; i++) {
              // if(widget.content.data[index].card[i].)

              if (score.data![i].idMentalQuestionCat == '15') {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                      ),
                      backgroundColor: Colors.white,
                      content: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            'title_gds'.tr(),
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: 15),
                          Image.asset(
                            'assets/images/sehat/Success_Mark.png',
                            width: 200,
                          ),
                          SizedBox(height: 10),
                          // Container(
                          //   alignment: Alignment.center,
                          //   width: width * 0.7,
                          //   child: Text(
                          //     text1,
                          //     style: TextStyle(
                          //       fontSize: width * 0.04,
                          //       fontWeight: FontWeight.bold,
                          //     ),
                          //   ),
                          // ),
                          SizedBox(height: 10),
                          Text(
                            '${'text1_gds'.tr()} ${score.data![i].child![0].score}/15',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: 10),
                          Text(
                            score.data![i].child![0].level!,
                            style: TextStyle(
                              color: Color(
                                int.parse(
                                    '0xFF${score.data![i].child![0].color!.substring(1, score.data![i].child![0].color!.length)}'),
                              ),
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),

                          SizedBox(height: 10),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: themeColor,
                              padding: EdgeInsets.symmetric(
                                horizontal: 30,
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              'OK',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                );
                break;
              }
            }
          } else {
            await Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => BlocProvider(
                  create: (context) => GetContentCubit(),
                  child: ContentScreen(
                    title: 'Geriatric Depression Scale (GDS-15)',
                    idCategory: '15',
                    isQuestionnaire: true,
                  ),
                ),
              ),
            );

            // await Future.delayed(Duration(milliseconds: 200));

            // Navigator.of(context).pop();
          }

          // widget.updateParent(
          //   widget.cardContent.data.id,
          //   '1',
          //   widget.cardContent.idMentalContent,
          //   widget.cardContent.contentType,
          //   true,
          // );
        },
        child: Text(
          'GDS-15',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Color(0xFF438C8B),
          ),
        ),
      ),
    );
  }
}
