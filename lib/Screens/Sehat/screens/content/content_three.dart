import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';

class ContentThree extends StatefulWidget {
  final CardContent cardContent;
  final Function(
    String idQuestion,
    String answer,
    String idMentalContent,
    String contentType,
    bool canNext,
  ) updateParent;

  const ContentThree(
      {Key? key, required this.cardContent, required this.updateParent})
      : super(key: key);

  @override
  State<ContentThree> createState() => _ContentTwoState();
}

class _ContentTwoState extends State<ContentThree> {
  @override
  void initState() {
    // init();
    super.initState();
  }

  void init() async {
    widget.updateParent(
      widget.cardContent.data!.id!,
      '1',
      widget.cardContent.idMentalContent!,
      widget.cardContent.contentType!,
      true,
    );
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      widget.updateParent(
        widget.cardContent.data!.id!,
        '1',
        widget.cardContent.idMentalContent!,
        widget.cardContent.contentType!,
        true,
      );
    });

    return Html(
      data: widget.cardContent.data!.articleText,
      style: {
        // tables will have the below background color
        "body": Style(color: Colors.white),
        "p": Style(color: Colors.white),
      },
      // defaultTextStyle: TextStyle(
      //   color: Colors.white,
      // ),
    );
  }
}
