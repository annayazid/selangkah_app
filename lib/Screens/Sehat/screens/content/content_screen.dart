import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_eight.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_five.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_four.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_one.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_seven.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_six.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_three.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_two.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';
import 'package:selangkah_new/utils/constants.dart';

class ContentScreen extends StatelessWidget {
  final String title;
  final String idCategory;
  final bool isQuestionnaire;

  const ContentScreen(
      {Key? key,
      required this.title,
      required this.idCategory,
      required this.isQuestionnaire})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String language =
        EasyLocalization.of(context)!.locale.languageCode == 'en' ? 'EN' : 'MY';

    context
        .read<GetContentCubit>()
        .getContent(idCategory, language, isQuestionnaire);
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return SehatScaffold(
      appBarTitle: 'Mental Sehat',
      child: Container(
        height: height - AppBar().preferredSize.height,
        width: width,
        child: BlocBuilder<GetContentCubit, GetContentState>(
          builder: (context, state) {
            if (state is GetContentLoaded) {
              return GetContentLoadedWidget(
                content: state.content,
                title: title,
                idCategory: idCategory,
                isQuestionnaire: isQuestionnaire,
              );
            } else {
              return SpinKitFadingCircle(
                color: kPrimaryColor,
                size: 20,
              );
            }
          },
        ),
      ),
    );
  }
}

class GetContentLoadedWidget extends StatefulWidget {
  final Content content;
  final String title;
  final String idCategory;
  final bool isQuestionnaire;

  const GetContentLoadedWidget(
      {Key? key,
      required this.content,
      required this.title,
      required this.idCategory,
      required this.isQuestionnaire})
      : super(key: key);

  @override
  _GetContentLoadedWidgetState createState() => _GetContentLoadedWidgetState();
}

class _GetContentLoadedWidgetState extends State<GetContentLoadedWidget> {
  List<SendAnswer> answerList = [];

  bool canClickNext = true;
  bool updated = false;

  refresh(
    String idQuestionNew,
    String answerNew,
    String idMentalContentNew,
    String contentTypeNew,
    bool canNextNew,
  ) {
    // if (!answerList.contains(SendAnswer(
    //     idQuestionNew, answerNew, idMentalContentNew, contentTypeNew))) {
    //   answerList.add(SendAnswer(
    //       idQuestionNew, answerNew, idMentalContentNew, contentTypeNew));
    // }

    if (answerList.isNotEmpty) {
      if (answerList.any((element) => element.idQuestion == idQuestionNew)) {
        //update here

        //get index element
        int indexElement = answerList
            .indexWhere((element) => element.idQuestion == idQuestionNew);
        print('indexElement $indexElement');

        answerList[indexElement] = SendAnswer(idQuestionNew, answerNew,
            idMentalContentNew, contentTypeNew, canNextNew);
        // answerList
        //     .removeWhere((element) => element.idQuestion == idQuestionNew);
        // answerList.add(SendAnswer(idQuestionNew, answerNew, idMentalContentNew,
        //     contentTypeNew, canNextNew));
      } else {
        answerList.add(SendAnswer(idQuestionNew, answerNew, idMentalContentNew,
            contentTypeNew, canNextNew));
      }

      for (var i = 0; i < answerList.length; i++) {
        print('$i ${answerList[i].idQuestion}');
      }
      // for (var i = 0; i < answerList.length; i++) {
      //   if (answerList[i].contentType == contentTypeNew &&
      //       answerList[i].idMentalContent == idMentalContentNew &&
      //       answerList[i].idQuestion == idQuestionNew) {
      //     answerList[i].answer = answerNew;
      //   }
      // }
    } else {
      answerList.add(SendAnswer(idQuestionNew, answerNew, idMentalContentNew,
          contentTypeNew, canNextNew));
    }

    // SehatRepositories.setAnswer(
    //   idMentalContent: idMentalContentNew,
    //   contentType: contentTypeNew,
    //   answer: answerNew,
    //   idQuestion: idQuestionNew,
    // );

    if (answerList.every((element) => element.canNext) && !updated) {
      updated = true;
    }

    print('length ${answerList.length}');
  }

  int index = 0;

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    String language =
        EasyLocalization.of(context)!.locale.languageCode == 'en' ? 'EN' : 'MY';
    return SingleChildScrollView(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            child: Text(
              widget.title.toUpperCase(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Color(0xFF676B74),
              ),
              textAlign: TextAlign.center,
            ),
            decoration: BoxDecoration(
              border: Border.all(
                width: 3,
                color: Color(0xFF676B74),
              ),
              borderRadius: BorderRadius.circular(15),
            ),
          ),
          SizedBox(height: 10),

          Container(
            padding: EdgeInsets.all(20),
            width: width,
            decoration: BoxDecoration(
              color: Color(0xFF438C8B),
              borderRadius: BorderRadius.circular(15),
            ),
            child: QuestionnaireContent(
              card: widget.content.data![index].card!,
              updateParent: refresh,
              idCategory: widget.idCategory,
            ),
          ),

          SizedBox(height: 10),

          Row(
            children: [
              if (index != 0)
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Color(0xFF438C8B),
                    shape: CircleBorder(),
                  ),
                  onPressed: () {
                    setState(() {
                      index--;
                    });
                  },
                  child: Icon(
                    FontAwesomeIcons.arrowLeft,
                    size: 15,
                  ),
                ),
              Spacer(),
              if (index < widget.content.data!.length)
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: () {
                      // print(
                      //     'color is ${answerList.every((element) => element.canNext)}');
                      if (canClickNext) {
                        return Color(0xFF438C8B);
                      } else {
                        return Colors.grey;
                      }
                    }(),
                    shape: CircleBorder(),
                  ),
                  onPressed: () async {
                    // answerList.forEach((element) {
                    //   print('idQuestion ${element.idQuestion}');
                    //   print('idMentalContent ${element.idMentalContent}');
                    //   print('contentType ${element.contentType}');
                    //   print('canNext ${element.canNext}');
                    //   print('--------------');
                    // });
                    print(
                        'element is ${answerList.length} ${answerList.every((element) => element.canNext)}');

                    // answerList.forEach((element) {
                    //   print('--------------');
                    //   print('idQuestion ${element.idQuestion}');
                    //   print('idMentalContent ${element.idMentalContent}');
                    //   print('contentType ${element.contentType}');
                    //   print('canNext ${element.canNext}');
                    //   print('--------------');
                    // });

                    // answerList.sort((a, b) {
                    //   return a.idQuestion.compareTo(b.idQuestion);
                    // });

                    print('cannextquiz ${SehatGlobalVar.canNextQuiz}');
                    if (answerList.isNotEmpty &&
                        !answerList.any((element) => !element.canNext) &&
                        SehatGlobalVar.canNextQuiz) {
                      if (index < widget.content.data!.length - 1) {
                        // print('idQuestion $idQuestion');
                        // print('answer $answer');
                        // print('idMentalContent $idMentalContent');
                        // print('contentType $contentType');
                        setState(() {
                          index++;
                        });

                        print(answerList.length);

                        //call api set ans
                        for (var i = 0; i < answerList.length; i++) {
                          SehatRepositories.setAnswer(
                            idMentalContent: answerList[i].idMentalContent,
                            contentType: answerList[i].contentType,
                            answer: answerList[i].answer,
                            idQuestion: answerList[i].idQuestion,
                            idCategory: widget.idCategory,
                          );
                        }

                        answerList.clear();
                      } else {
                        print(answerList.length);

                        // print('idQuestion $idQuestion');
                        // print('answer $answer');
                        // print('idMentalContent $idMentalContent');
                        // print('contentType $contentType');

                        //call api set ans

                        for (var i = 0; i < answerList.length; i++) {
                          await SehatRepositories.setAnswer(
                            idMentalContent: answerList[i].idMentalContent,
                            contentType: answerList[i].contentType,
                            answer: answerList[i].answer,
                            idQuestion: answerList[i].idQuestion,
                            idCategory: widget.idCategory,
                          );
                        }

                        //set score
                        await SehatRepositories.setScore(widget.idCategory);

                        //get score
                        Score score =
                            await SehatRepositories.getScore(language);

                        //CLEAR LIST
                        // answerList.clear();

                        if (widget.isQuestionnaire) {
                          //if pss - 10
                          if (widget.idCategory == '12') {
                            for (var i = 0; i < score.data!.length; i++) {
                              // if(widget.content.data[index].card[i].)

                              if (score.data![i].idMentalQuestionCat ==
                                  widget.idCategory) {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10),
                                        ),
                                      ),
                                      backgroundColor: Colors.white,
                                      content: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(
                                            'title_pss'.tr(),
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          SizedBox(height: 15),
                                          Image.asset(
                                            'assets/images/sehat/Success_Mark.png',
                                            width: 200,
                                          ),
                                          SizedBox(height: 10),
                                          // Container(
                                          //   alignment: Alignment.center,
                                          //   width: width * 0.7,
                                          //   child: Text(
                                          //     text1,
                                          //     style: TextStyle(
                                          //       fontSize: width * 0.04,
                                          //       fontWeight: FontWeight.bold,
                                          //     ),
                                          //   ),
                                          // ),
                                          SizedBox(height: 10),
                                          Text(
                                            '${'text1_pss'.tr()} ${score.data![i].child![0].score}/40',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          SizedBox(height: 10),
                                          Text(
                                            score.data![i].child![0].level!,
                                            style: TextStyle(
                                              color: Color(
                                                int.parse(
                                                    '0xFF${score.data![i].child![0].color!.substring(1, score.data![i].child![0].color!.length)}'),
                                              ),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20,
                                            ),
                                          ),

                                          SizedBox(height: 10),
                                          TextButton(
                                            style: TextButton.styleFrom(
                                              backgroundColor: themeColor,
                                              padding: EdgeInsets.symmetric(
                                                horizontal: 30,
                                              ),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                              Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              'OK',
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                );
                                break;
                              }
                            }
                          } else if (widget.idCategory == '13' ||
                              widget.idCategory == '19') {
                            for (var i = 0; i < score.data!.length; i++) {
                              // if(widget.content.data[index].card[i].)

                              if (widget.idCategory == '19') {
                                SehatRepositories.sendDassComplete();
                              }

                              if (score.data![i].idMentalQuestionCat == '13') {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10),
                                        ),
                                      ),
                                      backgroundColor: Colors.white,
                                      content: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(
                                            'title_dass'.tr(),
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          SizedBox(height: 15),
                                          Image.asset(
                                            'assets/images/sehat/Success_Mark.png',
                                            width: 200,
                                          ),
                                          SizedBox(height: 10),
                                          // Container(
                                          //   alignment: Alignment.center,
                                          //   width: width * 0.7,
                                          //   child: Text(
                                          //     text1,
                                          //     style: TextStyle(
                                          //       fontSize: width * 0.04,
                                          //       fontWeight: FontWeight.bold,
                                          //     ),
                                          //   ),
                                          // ),

                                          ListView.builder(
                                            shrinkWrap: true,
                                            itemCount:
                                                score.data![i].child!.length,
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              print(score
                                                  .data![i].child![index].name);
                                              return Column(
                                                children: [
                                                  Text(
                                                    '${'text1_dass'.tr()} ${score.data![i].child![index].name} ${'text2_dass'.tr()}',
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  Text(
                                                    score.data![i].child![index]
                                                        .level!,
                                                    style: TextStyle(
                                                      color: Color(
                                                        int.parse(
                                                            '0xFF${score.data![i].child![index].color!.substring(1, score.data![i].child![0].color!.length)}'),
                                                      ),
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 20,
                                                    ),
                                                  ),
                                                  SizedBox(height: 10),
                                                ],
                                              );
                                            },
                                          ),

                                          SizedBox(height: 10),
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor: themeColor,
                                              padding: EdgeInsets.symmetric(
                                                horizontal: 30,
                                              ),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                              Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              'OK',
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                );
                                break;
                              }
                            }
                          } else if (widget.idCategory == '14') {
                            //check if score more than 0

                            for (var i = 0; i < score.data!.length; i++) {
                              if (score.data![i].idMentalQuestionCat ==
                                  widget.idCategory) {
                                // if(widget.content.data[index].card[i].)

                                if (score.data![i].child![0].score.toInt() ==
                                    0) {
                                  //here

                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) => BlocProvider(
                                        create: (context) => GetContentCubit(),
                                        child: ContentScreen(
                                          title: widget.title,
                                          idCategory: '17',
                                          isQuestionnaire: true,
                                        ),
                                      ),
                                    ),
                                  );
                                } else {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        backgroundColor: Colors.white,
                                        content: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Text(
                                              'title_phq'.tr(),
                                              style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                            SizedBox(height: 15),
                                            Image.asset(
                                              'assets/images/sehat/Success_Mark.png',
                                              width: 200,
                                            ),
                                            SizedBox(height: 10),
                                            // Container(
                                            //   alignment: Alignment.center,
                                            //   width: width * 0.7,
                                            //   child: Text(
                                            //     text1,
                                            //     style: TextStyle(
                                            //       fontSize: width * 0.04,
                                            //       fontWeight: FontWeight.bold,
                                            //     ),
                                            //   ),
                                            // ),
                                            SizedBox(height: 10),
                                            Text(
                                              '${'text1_phq'.tr()} ${score.data![i].child![0].score}/27',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                            SizedBox(height: 10),
                                            Text(
                                              score.data![i].child![0].level!,
                                              style: TextStyle(
                                                color: Color(
                                                  int.parse(
                                                      '0xFF${score.data![i].child![0].color!.substring(1, score.data![i].child![0].color!.length)}'),
                                                ),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20,
                                              ),
                                            ),

                                            SizedBox(height: 10),
                                            ElevatedButton(
                                              style: ElevatedButton.styleFrom(
                                                backgroundColor: themeColor,
                                                padding: EdgeInsets.symmetric(
                                                  horizontal: 30,
                                                ),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                ),
                                              ),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                                Navigator.of(context).pop();
                                              },
                                              child: Text(
                                                'OK',
                                                style: TextStyle(
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                  );
                                }

                                break;
                              }
                            }
                          } else if (widget.idCategory == '15') {
                            for (var i = 0; i < score.data!.length; i++) {
                              // if(widget.content.data[index].card[i].)

                              if (score.data![i].idMentalQuestionCat ==
                                  widget.idCategory) {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10),
                                        ),
                                      ),
                                      backgroundColor: Colors.white,
                                      content: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(
                                            'title_gds'.tr(),
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          SizedBox(height: 15),
                                          Image.asset(
                                            'assets/images/sehat/Success_Mark.png',
                                            width: 200,
                                          ),
                                          SizedBox(height: 10),
                                          // Container(
                                          //   alignment: Alignment.center,
                                          //   width: width * 0.7,
                                          //   child: Text(
                                          //     text1,
                                          //     style: TextStyle(
                                          //       fontSize: width * 0.04,
                                          //       fontWeight: FontWeight.bold,
                                          //     ),
                                          //   ),
                                          // ),
                                          SizedBox(height: 10),
                                          Text(
                                            '${'text1_gds'.tr()} ${score.data![i].child![0].score}/15',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          SizedBox(height: 10),
                                          Text(
                                            score.data![i].child![0].level!,
                                            style: TextStyle(
                                              color: Color(
                                                int.parse(
                                                    '0xFF${score.data![i].child![0].color!.substring(1, score.data![i].child![0].color!.length)}'),
                                              ),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20,
                                            ),
                                          ),

                                          SizedBox(height: 10),
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor: themeColor,
                                              padding: EdgeInsets.symmetric(
                                                horizontal: 30,
                                              ),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                              Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              'OK',
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                );
                                break;
                              }
                            }
                          } else if (widget.idCategory == '3') {
                            for (var i = 0; i < score.data!.length; i++) {
                              // if(widget.content.data[index].card[i].)

                              if (score.data![i].idMentalQuestionCat ==
                                  widget.idCategory) {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10),
                                        ),
                                      ),
                                      backgroundColor: Colors.white,
                                      content: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Image.asset(
                                            'assets/images/sehat/Success_Mark.png',
                                            width: 200,
                                          ),
                                          SizedBox(height: 10),
                                          // Container(
                                          //   alignment: Alignment.center,
                                          //   width: width * 0.7,
                                          //   child: Text(
                                          //     text1,
                                          //     style: TextStyle(
                                          //       fontSize: width * 0.04,
                                          //       fontWeight: FontWeight.bold,
                                          //     ),
                                          //   ),
                                          // ),
                                          SizedBox(height: 10),
                                          Text(
                                            answerList[0].answer == '18'
                                                ? 'completed_risk_screening'
                                                    .tr()
                                                : 'help_selcare_clinic'.tr(),
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          SizedBox(height: 10),
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor: themeColor,
                                              padding: EdgeInsets.symmetric(
                                                horizontal: 30,
                                              ),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                              Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              'OK',
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                );
                                break;
                              }
                            }
                          } else if (widget.idCategory == '17') {
                            //check if score more than 0

                            for (var i = 0; i < score.data!.length; i++) {
                              if (score.data![i].idMentalQuestionCat == '14') {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10),
                                        ),
                                      ),
                                      backgroundColor: Colors.white,
                                      content: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(
                                            'title_phq'.tr(),
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          SizedBox(height: 15),
                                          Image.asset(
                                            'assets/images/sehat/Success_Mark.png',
                                            width: 200,
                                          ),
                                          SizedBox(height: 10),
                                          // Container(
                                          //   alignment: Alignment.center,
                                          //   width: width * 0.7,
                                          //   child: Text(
                                          //     text1,
                                          //     style: TextStyle(
                                          //       fontSize: width * 0.04,
                                          //       fontWeight: FontWeight.bold,
                                          //     ),
                                          //   ),
                                          // ),
                                          SizedBox(height: 10),
                                          Text(
                                            '${'text1_phq'.tr()} ${score.data![i].child![0].score}/27',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          SizedBox(height: 10),
                                          Text(
                                            score.data![i].child![0].level!,
                                            style: TextStyle(
                                              color: Color(
                                                int.parse(
                                                    '0xFF${score.data![i].child![0].color!.substring(1, score.data![i].child![0].color!.length)}'),
                                              ),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20,
                                            ),
                                          ),

                                          SizedBox(height: 10),
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor: themeColor,
                                              padding: EdgeInsets.symmetric(
                                                horizontal: 30,
                                              ),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                              Navigator.of(context).pop();
                                              Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              'OK',
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                );

                                break;
                              }
                            }
                          } else {
                            showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                  ),
                                  backgroundColor: Colors.white,
                                  content: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Image.asset(
                                        'assets/images/sehat/Success_Mark.png',
                                        width: 200,
                                      ),
                                      SizedBox(height: 10),
                                      // Container(
                                      //   alignment: Alignment.center,
                                      //   width: width * 0.7,
                                      //   child: Text(
                                      //     text1,
                                      //     style: TextStyle(
                                      //       fontSize: width * 0.04,
                                      //       fontWeight: FontWeight.bold,
                                      //     ),
                                      //   ),
                                      // ),
                                      SizedBox(height: 10),
                                      Text(
                                        'thank_you_answer'.tr(),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                      SizedBox(height: 10),
                                      ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          backgroundColor: themeColor,
                                          padding: EdgeInsets.symmetric(
                                            horizontal: 30,
                                          ),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                        ),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          'OK',
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            );
                          }
                        } else {
                          showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(10),
                                  ),
                                ),
                                backgroundColor: Colors.white,
                                content: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    SizedBox(height: 20),
                                    // Image.asset(
                                    //   'assets/images/sehat/Success_Mark.png',
                                    //   width: 200,
                                    // ),
                                    Icon(
                                      FontAwesomeIcons.solidCircleCheck,
                                      color: Colors.green,
                                      size: 75,
                                    ),
                                    SizedBox(height: 30),
                                    Text(
                                      'congratulations'.tr().toUpperCase(),
                                      style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: themeColor,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Text(
                                      '${'you_have_completed'.tr()}'
                                          .toUpperCase(),
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    Text(
                                      '"${widget.title}"'.toUpperCase(),
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(height: 10),
                                    ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        backgroundColor: themeColor,
                                        padding: EdgeInsets.symmetric(
                                          horizontal: 30,
                                        ),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                        Navigator.of(context).pop();

                                        //TODO redirect dass 21 if iact and act last question

                                        if (widget.idCategory == '83' ||
                                            widget.idCategory == '74') {
                                          showDialog(
                                            context: context,
                                            barrierDismissible: false,
                                            builder: (context) {
                                              return AlertDialog(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                    Radius.circular(10),
                                                  ),
                                                ),
                                                content: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Text(
                                                      'Sila ambil saringan DASS-21 untuk mengetahui perkembangan kesihatan mental anda selepas selesai menjalani program i-ACT for Life',
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    SizedBox(height: 10),
                                                    ElevatedButton(
                                                      style: ElevatedButton
                                                          .styleFrom(
                                                        backgroundColor:
                                                            themeColor,
                                                      ),
                                                      onPressed: () async {
                                                        // Navigator.of(context)
                                                        //     .pop();
                                                        await SehatRepositories
                                                            .getDassFollowupId();

                                                        Navigator.of(context)
                                                            .pushReplacement(
                                                          MaterialPageRoute(
                                                            builder: (context) =>
                                                                BlocProvider(
                                                              create: (context) =>
                                                                  GetContentCubit(),
                                                              child:
                                                                  ContentScreen(
                                                                title:
                                                                    'DEPRESSION ANXIETY STRESS SCALE (DASS-21)',
                                                                idCategory:
                                                                    '19',
                                                                isQuestionnaire:
                                                                    true,
                                                              ),
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                      child: Text('DASS-21'),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            },
                                          );
                                        }
                                      },
                                      child: Text(
                                        'OK',
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          );
                        }
                      }
                    } else {
                      Fluttertoast.showToast(
                          msg: 'Please complete the task first');
                    }
                  },
                  child: Icon(
                    FontAwesomeIcons.arrowRight,
                    size: 15,
                  ),
                ),
            ],
          ),

          // PageView.builder(
          //   itemCount: content.data.length,
          //   itemBuilder: (context, index) {
          //     return Container(
          //       color: Colors.white,
          //       child: Container(
          //         padding: EdgeInsets.all(20),
          //         decoration: BoxDecoration(
          //           color: Color(0xFF438C8B),
          //         ),
          //         child: Text('test'),
          //       ),
          //     );
          //   },
          // ),
        ],
      ),
    );
  }
}

class QuestionnaireContent extends StatefulWidget {
  final List<CardContent> card;
  final Function(
    String idQuestion,
    String answer,
    String idMentalContent,
    String contentType,
    bool canNext,
  ) updateParent;
  final String idCategory;

  const QuestionnaireContent(
      {Key? key,
      required this.card,
      required this.updateParent,
      required this.idCategory})
      : super(key: key);

  @override
  _QuestionnaireContentState createState() => _QuestionnaireContentState();
}

class _QuestionnaireContentState extends State<QuestionnaireContent> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: widget.card.length,
      itemBuilder: (BuildContext context, int index) {
        if (widget.card[index].contentType == '1') {
          print('masuk if');
          return ContentOne(
            cardContent: widget.card[index],
            updateParent: widget.updateParent,
            idModule: widget.idCategory,
            answerTextList: widget.card[index].data!.choice!
                .map((e) => e.answerValue!)
                .toList(),
            answerId: widget.card[index].data!.choice!
                .map((e) => e.answerId!)
                .toList(),
          );
        } else {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //content 1 = questionnaire
              // if (widget.card[index].contentType == '1')
              //   ContentOne(
              //     cardContent: widget.card[index],
              //     updateParent: widget.updateParent,
              //     idModule: widget.idCategory,
              //   ),

              //content 2 = video
              if (widget.card[index].contentType == '2')
                ContentTwo(
                  cardContent: widget.card[index],
                  updateParent: widget.updateParent,
                ),

              //content 3 = article
              if (widget.card[index].contentType == '3')
                ContentThree(
                  cardContent: widget.card[index],
                  updateParent: widget.updateParent,
                ),

              //content 4 = audio
              if (widget.card[index].contentType == '4')
                ContentFour(
                  cardContent: widget.card[index],
                  updateParent: widget.updateParent,
                ),

              //content 5 = image
              if (widget.card[index].contentType == '5')
                ContentFive(
                  cardContent: widget.card[index],
                  updateParent: widget.updateParent,
                ),

              //content 6 = download
              if (widget.card[index].contentType == '6')
                ContentSix(
                  cardContent: widget.card[index],
                  updateParent: widget.updateParent,
                ),

              //quiz
              if (widget.card[index].contentType == '7')
                ContentSeven(
                  cardContent: widget.card[index],
                  updateParent: widget.updateParent,
                  // answerIndex: 0,
                ),

              //redirect gds
              if (widget.card[index].contentType == '8')
                ContentEight(
                  cardContent: widget.card[index],
                  updateParent: widget.updateParent,
                ),

              //redirect dass
              // if (widget.card[index].contentType == '9')
              //   ContentNine(
              //     cardContent: widget.card[index],
              //     updateParent: widget.updateParent,
              //   ),
            ],
          );
        }

        // if (card[index].contentType == '1') {
        //   //questionnaire
        //   return Column(
        //     children: [
        //       ContentOne(
        //         cardContent: card[index],
        //         totalQuestion: totalElement,
        //       ),
        //       SizedBox(height: 20),
        //     ],
        //   );
        // } else if (card[index].contentType == '2') {
        //   return Text('this is content 2');
        // } else if (card[index].contentType == '3') {
        //   return Text('this is content 3');
        // } else if (card[index].contentType == '4') {
        //   return Text('this is content 4');
        // } else {
        //   //gambar
        //   return Image.network(card[index].data.image);
        // }
      },
    );
  }
}
