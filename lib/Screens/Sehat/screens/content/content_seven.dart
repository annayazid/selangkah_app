// ignore_for_file: must_be_immutable

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';

class ContentSeven extends StatefulWidget {
  final CardContent cardContent;
  final Function(
    String idQuestion,
    String answer,
    String idMentalContent,
    String contentType,
    bool canNext,
  ) updateParent;
  int? answerIndex;

  ContentSeven(
      {super.key,
      required this.cardContent,
      required this.updateParent,
      this.answerIndex});

  @override
  State<ContentSeven> createState() => _ContentSevenState();
}

class _ContentSevenState extends State<ContentSeven> {
  // int? answerIndex;

  List<String> answerTextList = [];

  List<String> answerId = [];

  @override
  void initState() {
    init();
    super.initState();
  }

  void init() {
    // answerIndex = widget.answerIndex;
    // answerIndex = widget.answerIndex;
    // widget.updateParent(
    //   widget.cardContent.data.id,
    //   answerId[answerIndex],
    //   widget.cardContent.idMentalContent,
    //   widget.cardContent.contentType,
    //   false,
    // );
    SehatGlobalVar.canNextQuiz = true;
    widget.cardContent.data!.choice!.forEach((element) {
      answerTextList.add(element.answerValue!);
      answerId.add(element.answerId!);
    });
  }

  @override
  Widget build(BuildContext context) {
    // double width = MediaQuery.of(context).size.width;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text.rich(
          TextSpan(
            children: [
              TextSpan(text: '${'question'.tr()} '),
              TextSpan(text: widget.cardContent.data!.displayNo),
              // TextSpan(text: '${widget.totalQuestion}'),
            ],
            style: TextStyle(
              color: Colors.white,
              fontSize: 17,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        SizedBox(height: 20),
        Text(
          widget.cardContent.data!.questionText!,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        SizedBox(height: 10),
        // Align(
        //   alignment: Alignment.centerRight,
        //   child: Container(
        //     padding: EdgeInsets.all(5),
        //     decoration: BoxDecoration(
        //       border: Border.all(color: Colors.white, width: 2),
        //       borderRadius: BorderRadius.circular(10),
        //     ),
        //     child: Text(
        //       answerTextList[answerIndex],
        //       style: TextStyle(
        //         color: Colors.white,
        //         fontWeight: FontWeight.bold,
        //       ),
        //     ),
        //   ),
        // ),
        // Radio(
        //   toggleable: false,
        //   activeColor: theme_color,
        //   value: 10,
        //   groupValue: answerIndex,
        //   onChanged: (val) {
        //     SehatGlobalVar.canNextQuiz = true;

        //     setState(() {
        //       answerIndex = val;
        //       // print('hooo $val');
        //     });
        //   },
        // ),
        ListView.builder(
          shrinkWrap: true,
          itemCount: widget.cardContent.data!.choice!.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                GestureDetector(
                  onTap: () {
                    SehatGlobalVar.canNextQuiz = true;
                    setState(() {
                      widget.answerIndex = index;
                    });
                    widget.updateParent(
                      widget.cardContent.data!.id!,
                      answerId[widget.answerIndex!],
                      widget.cardContent.idMentalContent!,
                      widget.cardContent.contentType!,
                      true,
                    );
                    // setState(() {
                    //   answerIndex = null;
                    // });
                  },
                  child: Container(
                    padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                    decoration: BoxDecoration(
                      // color: answerIndex == index
                      //     ? Colors.white
                      //     : Color(0xFF8DB4B2),
                      color: () {
                        if (widget.answerIndex == index &&
                            widget.cardContent.data!.choice![index]
                                    .correctFlag ==
                                '1')
                          return Colors.green;
                        else if (widget.answerIndex == index &&
                            widget.cardContent.data!.choice![index]
                                    .correctFlag ==
                                '0')
                          return Colors.red;
                        else
                          return Color(0xFF8DB4B2);
                      }(),
                      // ? Colors.green
                      // : Color(0xFF8DB4B2),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      children: [
                        Radio(
                          activeColor: themeColor,
                          value: index,
                          groupValue: widget.answerIndex,
                          onChanged: (val) {
                            SehatGlobalVar.canNextQuiz = true;

                            setState(() {
                              widget.answerIndex = val;
                              // print('hooo $val');
                            });
                            widget.updateParent(
                              widget.cardContent.data!.id!,
                              answerId[widget.answerIndex!],
                              widget.cardContent.idMentalContent!,
                              widget.cardContent.contentType!,
                              true,
                            );

                            // setState(() {
                            //   answerIndex = null;
                            // });
                          },
                        ),
                        Text(
                          widget.cardContent.data!.choice![index].answerValue!,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 10),
              ],
            );
          },
        ),
        SizedBox(height: 10),
        Row(
          children: [
            Container(
              height: 25,
              width: 50,
              decoration: BoxDecoration(
                color: Colors.green,
                border: Border.all(
                  color: Colors.white,
                  width: 2,
                ),
              ),
            ),
            SizedBox(width: 20),
            Text(
              'correct_answer'.tr(),
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        SizedBox(height: 10),
        Row(
          children: [
            Container(
              height: 25,
              width: 50,
              decoration: BoxDecoration(
                color: Colors.red,
                border: Border.all(
                  color: Colors.white,
                  width: 2,
                ),
              ),
            ),
            SizedBox(width: 20),
            Text(
              'incorrect_answer'.tr(),
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
      ],
    );
  }
}
