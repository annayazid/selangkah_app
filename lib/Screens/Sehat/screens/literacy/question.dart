import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Sehat/models/literacyQuestion.dart';
import 'package:selangkah_new/Screens/Sehat/screens/components/components.dart';
import 'package:selangkah_new/Screens/Sehat/screens/literacy/answer.dart';
import 'package:selangkah_new/Screens/Sehat/screens/literacy/literacy_services.dart';

class LiteracyQuestions extends StatefulWidget {
  final LiteracyQuestion question;
  final double minQues, maxQues;
  final String from;
  final double value;

  const LiteracyQuestions({
    Key? key,
    required this.question,
    required this.from,
    required this.minQues,
    required this.maxQues,
    required this.value,
  }) : super(key: key);

  @override
  _LiteracyQuestionsState createState() => _LiteracyQuestionsState();
}

class _LiteracyQuestionsState extends State<LiteracyQuestions> {
  String answer = '';
  var f = NumberFormat("00", "en_US");

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      // getCurrentAnswer();
    });
  }

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height -
    //     AppBar().preferredSize.height -
    //     MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Container(
      // height: height - AppBar().preferredSize.height,
      width: width,
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 40,
            ),
            buildLiteracyQuestion(
              width,
              SliderTheme(
                data: SliderTheme.of(context).copyWith(
                  activeTrackColor: Colors.white.withOpacity(0.5),
                  inactiveTrackColor: Colors.white.withOpacity(0.5),
                  trackShape: RoundedRectSliderTrackShape(),
                  trackHeight: 8,
                  thumbShape: RoundSliderThumbShape(enabledThumbRadius: 12.0),
                  thumbColor: Colors.white,
                  overlayColor: Colors.white.withAlpha(32),
                  overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
                  activeTickMarkColor: Colors.transparent,
                  inactiveTickMarkColor: Colors.transparent,
                ),
                child: Slider(
                  value: sliderIndicator[
                      int.parse(widget.question.displayNo!) - 1],
                  min: widget.minQues,
                  max: widget.maxQues,
                  onChanged: (value) {
                    setState(
                      () {
                        if (widget.from == 'section1') {
                          section1Answer(value);
                        } else if (widget.from == 'section2') {
                          section2Answer(value);
                        } else if (widget.from == 'section3') {
                          section1Answer(value);
                        } else if (widget.from == 'section4') {
                          section4Answer(value);
                        } else if (widget.from == 'section5') {
                          section5Answer(value);
                        }
                      },
                    );
                  },
                ),
              ),
              widget.question.displayNo!,
              f.format(totalQuestion),
              literacyAnswer[int.parse(widget.question.displayNo!) - 1],
              widget.question.questionText!,
            ),
          ],
        ),
      ),
    );
  }

  section1Answer(value) {
    if (value.round() == 1) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_1_answer_1_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 1.0;
      });
    } else if (value.round() == 2) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_1_answer_2_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 2.0;
      });
    } else if (value.round() == 3) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_1_answer_3_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 3.0;
      });
    } else if (value.round() == 4) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_1_answer_4_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 4.0;
      });
    }
  }

  section2Answer(value) {
    if (value.round() == 1) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_2_answer_1_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 1.0;
      });
    } else if (value.round() == 2) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_2_answer_2_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 2.0;
      });
    } else if (value.round() == 3) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_2_answer_3_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 3.0;
      });
    } else if (value.round() == 4) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_2_answer_4_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 4.0;
      });
    }
  }

  section5Answer(value) {
    if (value.round() == 1) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_5_answer_1_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 1.0;
      });
    } else if (value.round() == 2) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_5_answer_2_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 2.0;
      });
    } else if (value.round() == 3) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_5_answer_3_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 3.0;
      });
    } else if (value.round() == 4) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_5_answer_4_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 4.0;
      });
    } else if (value.round() == 5) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_5_answer_5_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 5.0;
      });
    }
  }

  section4Answer(value) {
    if (value.round() == 1) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_4_answer_1_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 1.0;
      });
    } else if (value.round() == 2) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_4_answer_2_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 2.0;
      });
    } else if (value.round() == 3) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_4_answer_3_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 3.0;
      });
    } else if (value.round() == 4) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_4_answer_4_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 4.0;
      });
    } else if (value.round() == 5) {
      setState(() {
        literacyAnswer[int.parse(widget.question.displayNo!) - 1] =
            'section_4_answer_5_title'.tr();
        sliderIndicator[int.parse(widget.question.displayNo!) - 1] = 5.0;
      });
    }
  }

  // getCurrentAnswer() {
  //   if (widget.from == 'section1') {
  //     if (sliderIndicator[int.parse(widget.question.displayNo) - 1] == 1.0) {
  //       setState(() {
  //         answer = 'Tidak';
  //       });
  //       print('hi tak jadi1');
  //     } else if (sliderIndicator[int.parse(widget.question.displayNo) - 1] ==
  //         2.0) {
  //       setState(() {
  //         answer = 'Mungkin Tidak';
  //       });
  //       print('hi tak jadi2');
  //     } else if (sliderIndicator[int.parse(widget.question.displayNo) - 1] ==
  //         3.0) {
  //       setState(() {
  //         answer = 'Mungkin Ya';
  //       });
  //       print('hi tak jad3');
  //     } else if (sliderIndicator[int.parse(widget.question.displayNo) - 1] ==
  //         4.0) {
  //       setState(() {
  //         answer = 'Ya';
  //       });
  //       print('hi tak jadi4');
  //     }
  //   }
  // }
}
