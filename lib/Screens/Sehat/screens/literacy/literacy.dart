import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/Sehat/models/literacyQuestion.dart';
import 'package:selangkah_new/Screens/Sehat/screens/components/components.dart';
import 'package:selangkah_new/Screens/Sehat/screens/literacy/answer.dart';
import 'package:selangkah_new/Screens/Sehat/screens/literacy/literacy_services.dart';
import 'package:selangkah_new/Screens/Sehat/screens/literacy/question.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/Screens/Sehat/utility/sehat_color.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

// int _currentIndex = 0;
// int _section = 0;

class Literacy extends StatefulWidget {
  const Literacy({Key? key}) : super(key: key);

  @override
  _LiteracyState createState() => _LiteracyState();
}

class _LiteracyState extends State<Literacy> {
  List sect1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  List sect2 = [11, 12, 13];
  List sect3 = [14, 15, 16, 17];
  List sect4 = [18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
  List sect5 = [32, 33, 34, 35, 36, 37, 38, 39];
  List enableNavigate = [0, 11, 14, 18, 32];
  int total = 0;
  bool isLoading = true;
  bool showDialog = false;
  bool complete = false;

  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
    sliderIndicator = List.filled(35, 1.0);
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await getLiteracyQuestion();
      await getLiteracyAnswer();
      await getAnswer();
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return SehatScaffold(
      appBarTitle: 'mental_health_literacy'.tr(),
      child: isLoading
          ? Container(
              height: height - AppBar().preferredSize.height,
              width: width,
              child: SpinKitFadingCircle(
                color: kPrimaryColor,
                size: 50,
              ),
            )
          : Container(
              height: height - AppBar().preferredSize.height,
              width: width,
              child: Stack(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          width: width * 0.9,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Container(
                                width: width * 0.175,
                                height: 7,
                                decoration: BoxDecoration(
                                    color: sect1.contains(_currentIndex)
                                        ? themeColor
                                        : indicatorUnselected),
                              ),
                              Container(
                                width: width * 0.175,
                                height: 7,
                                decoration: BoxDecoration(
                                    color: sect2.contains(_currentIndex)
                                        ? themeColor
                                        : indicatorUnselected),
                              ),
                              Container(
                                width: width * 0.175,
                                height: 7,
                                decoration: BoxDecoration(
                                    color: sect3.contains(_currentIndex)
                                        ? themeColor
                                        : indicatorUnselected),
                              ),
                              Container(
                                width: width * 0.175,
                                height: 7,
                                decoration: BoxDecoration(
                                    color: sect4.contains(_currentIndex)
                                        ? themeColor
                                        : indicatorUnselected),
                              ),
                              Container(
                                width: width * 0.15,
                                height: 7,
                                decoration: BoxDecoration(
                                    color: sect5.contains(_currentIndex)
                                        ? themeColor
                                        : indicatorUnselected),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        pages[_currentIndex],
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          width: width * 0.85,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Visibility(
                                visible: (_currentIndex == 0) ? false : true,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: themeColor,
                                    shape: BoxShape.circle,
                                  ),
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.arrow_back,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _currentIndex--;
                                      });
                                    },
                                  ),
                                ),
                              ),
                              _currentIndex == 39
                                  ? ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        backgroundColor: themeColor,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                      ),
                                      onPressed: () async {
                                        bool stat = false;
                                        stat = await sentAnswerLiteracy(
                                            sliderIndicator[34]
                                                .toStringAsFixed(0),
                                            section5[6].id);

                                        if (stat) {
                                          total = await getTotal();
                                          if (total != 0) {
                                            setState(() {
                                              complete = true;
                                              showDialog = true;
                                            });
                                          }
                                        }
                                      },
                                      child: Text(
                                        'proceed'.tr().toString(),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: width * 0.04,
                                        ),
                                      ),
                                    )
                                  : Container(
                                      decoration: BoxDecoration(
                                        color: themeColor,
                                        shape: BoxShape.circle,
                                      ),
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.arrow_forward,
                                          color: Colors.white,
                                        ),
                                        onPressed: () async {
                                          bool stat = false;
                                          if (enableNavigate
                                              .contains(_currentIndex)) {
                                            setState(() {
                                              _currentIndex++;
                                            });
                                          } else if (_currentIndex < 39) {
                                            //for API submission of every answer
                                            if (_currentIndex == 1) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[0]
                                                      .toStringAsFixed(0),
                                                  section1[0].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 2;
                                                });
                                              }
                                            } else if (_currentIndex == 2) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[1]
                                                      .toStringAsFixed(0),
                                                  section1[1].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 3;
                                                });
                                              }
                                            } else if (_currentIndex == 3) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[2]
                                                      .toStringAsFixed(0),
                                                  section1[2].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 4;
                                                });
                                              }
                                            } else if (_currentIndex == 4) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[3]
                                                      .toStringAsFixed(0),
                                                  section1[3].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 5;
                                                });
                                              }
                                            } else if (_currentIndex == 5) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[4]
                                                      .toStringAsFixed(0),
                                                  section1[4].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 6;
                                                });
                                              }
                                            } else if (_currentIndex == 6) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[5]
                                                      .toStringAsFixed(0),
                                                  section1[5].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 7;
                                                });
                                              }
                                            } else if (_currentIndex == 7) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[6]
                                                      .toStringAsFixed(0),
                                                  section1[6].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 8;
                                                });
                                              }
                                            } else if (_currentIndex == 8) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[7]
                                                      .toStringAsFixed(0),
                                                  section1[7].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 9;
                                                });
                                              }
                                            } else if (_currentIndex == 9) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[8]
                                                      .toStringAsFixed(0),
                                                  section1[8].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 10;
                                                });
                                              }
                                            } else if (_currentIndex == 10) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[9]
                                                      .toStringAsFixed(0),
                                                  section1[9].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 11;
                                                });
                                              }
                                            } else if (_currentIndex == 12) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[10]
                                                      .toStringAsFixed(0),
                                                  section2[0].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 13;
                                                });
                                              }
                                            } else if (_currentIndex == 13) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[11]
                                                      .toStringAsFixed(0),
                                                  section2[1].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 14;
                                                });
                                              }
                                            } else if (_currentIndex == 15) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[12]
                                                      .toStringAsFixed(0),
                                                  section3[0].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 16;
                                                });
                                              }
                                            } else if (_currentIndex == 16) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[13]
                                                      .toStringAsFixed(0),
                                                  section3[1].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 17;
                                                });
                                              }
                                            } else if (_currentIndex == 17) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[14]
                                                      .toStringAsFixed(0),
                                                  section3[2].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 18;
                                                });
                                              }
                                            } else if (_currentIndex == 19) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[15]
                                                      .toStringAsFixed(0),
                                                  section4[0].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 20;
                                                });
                                              }
                                            } else if (_currentIndex == 20) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[16]
                                                      .toStringAsFixed(0),
                                                  section4[1].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 21;
                                                });
                                              }
                                            } else if (_currentIndex == 21) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[17]
                                                      .toStringAsFixed(0),
                                                  section4[2].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 22;
                                                });
                                              }
                                            } else if (_currentIndex == 22) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[18]
                                                      .toStringAsFixed(0),
                                                  section4[3].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 23;
                                                });
                                              }
                                            } else if (_currentIndex == 23) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[19]
                                                      .toStringAsFixed(0),
                                                  section4[4].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 24;
                                                });
                                              }
                                            } else if (_currentIndex == 24) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[20]
                                                      .toStringAsFixed(0),
                                                  section4[5].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 25;
                                                });
                                              }
                                            } else if (_currentIndex == 25) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[21]
                                                      .toStringAsFixed(0),
                                                  section4[6].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 26;
                                                });
                                              }
                                            } else if (_currentIndex == 26) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[22]
                                                      .toStringAsFixed(0),
                                                  section4[7].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 27;
                                                });
                                              }
                                            } else if (_currentIndex == 27) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[23]
                                                      .toStringAsFixed(0),
                                                  section4[8].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 28;
                                                });
                                              }
                                            } else if (_currentIndex == 28) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[24]
                                                      .toStringAsFixed(0),
                                                  section4[9].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 29;
                                                });
                                              }
                                            } else if (_currentIndex == 29) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[25]
                                                      .toStringAsFixed(0),
                                                  section4[10].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 30;
                                                });
                                              }
                                            } else if (_currentIndex == 30) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[26]
                                                      .toStringAsFixed(0),
                                                  section4[11].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 31;
                                                });
                                              }
                                            } else if (_currentIndex == 31) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[27]
                                                      .toStringAsFixed(0),
                                                  section4[12].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 32;
                                                });
                                              }
                                            } else if (_currentIndex == 33) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[28]
                                                      .toStringAsFixed(0),
                                                  section5[0].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 34;
                                                });
                                              }
                                            } else if (_currentIndex == 34) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[29]
                                                      .toStringAsFixed(0),
                                                  section5[1].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 35;
                                                });
                                              }
                                            } else if (_currentIndex == 35) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[30]
                                                      .toStringAsFixed(0),
                                                  section5[2].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 36;
                                                });
                                              }
                                            } else if (_currentIndex == 36) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[31]
                                                      .toStringAsFixed(0),
                                                  section5[3].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 37;
                                                });
                                              }
                                            } else if (_currentIndex == 37) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[32]
                                                      .toStringAsFixed(0),
                                                  section5[4].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 38;
                                                });
                                              }
                                            } else if (_currentIndex == 38) {
                                              stat = await sentAnswerLiteracy(
                                                  sliderIndicator[33]
                                                      .toStringAsFixed(0),
                                                  section5[5].id);

                                              if (stat) {
                                                setState(() {
                                                  _currentIndex = 39;
                                                });
                                              }
                                            }
                                          }
                                        },
                                      ),
                                    )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: showDialog,
                    child: buildPsyEduDialog(
                      height,
                      width,
                      'congratulations'.tr(),
                      () {
                        if (complete) {
                          setState(() {
                            showDialog = false;
                          });
                          Navigator.of(context).pop();
                        } else {
                          setState(() {
                            showDialog = false;
                          });
                        }
                      },
                      'you_have_completed_all_questions'.tr(),
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  getLiteracyQuestion() async {
    section1.clear();
    section2.clear();
    section3.clear();
    section4.clear();
    section5.clear();
    section1Ques.clear();
    section2Ques.clear();
    section3Ques.clear();
    section4Ques.clear();
    section5Ques.clear();
    pages.clear();
    String language = EasyLocalization.of(context)!.locale.languageCode;
    if (language == 'ms') {
      setState(() {
        language = 'MY';
      });
    } else {
      setState(() {
        language = 'EN';
      });
    }
    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
    };

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_quest_cat6'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    var data = json.decode(response.body);
    // //print('haiiii $data1');
    var _section1 = data['Data']['1'];
    var _section2 = data['Data']['2'];
    var _section3 = data['Data']['3'];
    var _section4 = data['Data']['4'];
    var _section5 = data['Data']['5'];

    if (section1.isEmpty) {
      for (var datas in _section1) {
        section1.add(LiteracyQuestion.fromJson(datas));
      }
      for (var datas in section1) {
        section1Ques.add(LiteracyQuestions(
          question: datas,
          minQues: 1.0,
          maxQues: 4.0,
          from: "section1",
          value: 1.0,
        ));
      }
      if (section1Ques.length == section1.length) {
        pages.add(LiteracyScreen(
          instruction: 'section_1_intro'.tr().toString(),
          text1: 'section_1_answer_1_title'.tr().toString() + ' =',
          text1desc: 'section_1_answer_1_answer'.tr().toString(),
          text2: 'section_1_answer_2_title'.tr().toString() + ' =',
          text2desc: 'section_1_answer_2_answer'.tr().toString(),
          text3: 'section_1_answer_3_title'.tr().toString() + ' =',
          text3desc: 'section_1_answer_3_answer'.tr().toString(),
          text4: 'section_1_answer_4_title'.tr().toString() + ' =',
          text4desc: 'section_1_answer_4_answer'.tr().toString(),
        ));
      }
      for (var datas in section1Ques) {
        pages.add(datas);
      }
    }

    if (section2.isEmpty) {
      for (var datas in _section2) {
        section2.add(LiteracyQuestion.fromJson(datas));
      }
      for (var datas in section2) {
        section2Ques.add(LiteracyQuestions(
          question: datas,
          minQues: 1.0,
          maxQues: 4.0,
          from: "section2",
          value: 1.0,
        ));
      }

      pages.add(LiteracyScreen(
        instruction: 'section_2_intro'.tr().toString(),
        text1: 'section_2_answer_1_title'.tr().toString() + ' =',
        text1desc: 'section_2_answer_1_answer'.tr().toString(),
        text2: 'section_2_answer_2_title'.tr().toString() + ' =',
        text2desc: 'section_2_answer_2_answer'.tr().toString(),
        text3: 'section_2_answer_3_title'.tr().toString() + ' =',
        text3desc: 'section_2_answer_3_answer'.tr().toString(),
        text4: 'section_2_answer_4_title'.tr().toString() + ' =',
        text4desc: 'section_2_answer_4_answer'.tr().toString(),
      ));

      for (var datas in section2Ques) {
        pages.add(datas);
      }
    }

    if (section3.isEmpty) {
      for (var datas in _section3) {
        section3.add(LiteracyQuestion.fromJson(datas));
      }
      for (var datas in section3) {
        section3Ques.add(LiteracyQuestions(
          question: datas,
          minQues: 1.0,
          maxQues: 4.0,
          from: "section3",
          value: 1.0,
        ));
      }

      pages.add(LiteracyScreen(
        instruction: 'section_3_intro'.tr().toString(),
        text1: 'section_3_answer_1_title'.tr().toString() + ' =',
        text1desc: 'section_3_answer_1_answer'.tr().toString(),
        text2: 'section_3_answer_2_title'.tr().toString() + ' =',
        text2desc: 'section_3_answer_2_answer'.tr().toString(),
        text3: 'section_3_answer_3_title'.tr().toString() + ' =',
        text3desc: 'section_3_answer_3_answer'.tr().toString(),
        text4: 'section_3_answer_4_title'.tr().toString() + ' =',
        text4desc: 'section_3_answer_4_answer'.tr().toString(),
      ));

      for (var datas in section3Ques) {
        pages.add(datas);
      }
    }

    if (section4.isEmpty) {
      for (var datas in _section4) {
        section4.add(LiteracyQuestion.fromJson(datas));
      }
      for (var datas in section4) {
        section4Ques.add(LiteracyQuestions(
          question: datas,
          minQues: 1.0,
          maxQues: 5.0,
          from: "section4",
          value: 1.0,
        ));
      }

      pages.add(LiteracyScreen(
        instruction: 'section_4_intro'.tr().toString(),
        text1: 'section_4_answer_1_title'.tr().toString() + ' =',
        text1desc: 'section_4_answer_1_answer'.tr().toString(),
        text2: 'section_4_answer_2_title'.tr().toString() + ' =',
        text2desc: 'section_4_answer_2_answer'.tr().toString(),
        text3: 'section_4_answer_3_title'.tr().toString() + ' =',
        text3desc: 'section_4_answer_3_answer'.tr().toString(),
        text4: 'section_4_answer_4_title'.tr().toString() + ' =',
        text4desc: 'section_4_answer_4_answer'.tr().toString(),
        text5: 'section_4_answer_5_title'.tr().toString() + ' =',
        text5desc: 'section_4_answer_5_answer'.tr().toString(),
      ));

      for (var datas in section4Ques) {
        pages.add(datas);
      }
    }
    if (section5.isEmpty) {
      for (var datas in _section5) {
        section5.add(LiteracyQuestion.fromJson(datas));
      }
      for (var datas in section5) {
        section5Ques.add(LiteracyQuestions(
          question: datas,
          minQues: 1.0,
          maxQues: 5.0,
          from: "section5",
          value: 1.0,
        ));
      }

      pages.add(LiteracyScreen(
        instruction: 'section_5_intro'.tr().toString(),
        text1: 'section_5_answer_1_title'.tr().toString() + ' =',
        text1desc: 'section_5_answer_1_answer'.tr().toString(),
        text2: 'section_5_answer_2_title'.tr().toString() + ' =',
        text2desc: 'section_5_answer_2_answer'.tr().toString(),
        text3: 'section_5_answer_3_title'.tr().toString() + ' =',
        text3desc: 'section_5_answer_3_answer'.tr().toString(),
        text4: 'section_5_answer_4_title'.tr().toString() + ' =',
        text4desc: 'section_5_answer_4_answer'.tr().toString(),
        text5: 'section_5_answer_5_title'.tr().toString() + ' =',
        text5desc: 'section_5_answer_5_answer'.tr().toString(),
      ));

      for (var datas in section5Ques) {
        pages.add(datas);
      }
    }

    totalQuestion = section1.length +
        section2.length +
        section3.length +
        section4.length +
        section5.length;

    //print('hi saya literacy $totalQuestion');

    setState(() {
      isLoading = false;
    });
  }

  getLiteracyAnswer() async {
    List<int> answers = [];
    int session = SehatGlobalVar.currentSession!;
    SecureStorage secureStorage = SecureStorage();

    final Map<String, String> map = {
      'token': TOKEN,
      'id_session': '$session',
      'id_selangkah_user': await secureStorage.readSecureData('userId'),
    };

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_ans_cat6'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    if (response.body != '[]') {
      var data = json.decode(response.body);

      for (var data1 in data['Data']) {
        answers.add(int.parse(data1['answer']));
      }
    }

    for (int i = 0; i < sliderIndicator.length; i++) {
      if (answers.asMap().containsKey(i)) {
        sliderIndicator[i] = answers[i].toDouble();
      } else {
        sliderIndicator[i] = 1.0;
      }
    }

    //first section
    if (answers.length == 1) {
      setState(() {
        _currentIndex = 2;
      });
    } else if (answers.length == 2) {
      setState(() {
        _currentIndex = 3;
      });
    } else if (answers.length == 3) {
      setState(() {
        _currentIndex = 4;
      });
    } else if (answers.length == 4) {
      setState(() {
        _currentIndex = 5;
      });
    } else if (answers.length == 5) {
      setState(() {
        _currentIndex = 6;
      });
    } else if (answers.length == 6) {
      setState(() {
        _currentIndex = 7;
      });
    } else if (answers.length == 7) {
      setState(() {
        _currentIndex = 8;
      });
    } else if (answers.length == 8) {
      setState(() {
        _currentIndex = 9;
      });
    } else if (answers.length == 9) {
      setState(() {
        _currentIndex = 10;
      });
    } else if (answers.length == 10) {
      setState(() {
        _currentIndex = 11;
      });
    }

    //second section
    if (answers.length == 11) {
      setState(() {
        _currentIndex = 13;
      });
    } else if (answers.length == 12) {
      setState(() {
        _currentIndex = 14;
      });
    }

    //third section
    if (answers.length == 13) {
      setState(() {
        _currentIndex = 16;
      });
    } else if (answers.length == 14) {
      setState(() {
        _currentIndex = 17;
      });
    } else if (answers.length == 15) {
      setState(() {
        _currentIndex = 18;
      });
    }

    //fourth section
    if (answers.length == 16) {
      setState(() {
        _currentIndex = 20;
      });
    } else if (answers.length == 17) {
      setState(() {
        _currentIndex = 21;
      });
    } else if (answers.length == 18) {
      setState(() {
        _currentIndex = 22;
      });
    } else if (answers.length == 19) {
      setState(() {
        _currentIndex = 23;
      });
    } else if (answers.length == 20) {
      setState(() {
        _currentIndex = 24;
      });
    } else if (answers.length == 21) {
      setState(() {
        _currentIndex = 25;
      });
    } else if (answers.length == 22) {
      setState(() {
        _currentIndex = 26;
      });
    } else if (answers.length == 23) {
      setState(() {
        _currentIndex = 27;
      });
    } else if (answers.length == 24) {
      setState(() {
        _currentIndex = 28;
      });
    } else if (answers.length == 25) {
      setState(() {
        _currentIndex = 29;
      });
    } else if (answers.length == 26) {
      setState(() {
        _currentIndex = 30;
      });
    } else if (answers.length == 27) {
      setState(() {
        _currentIndex = 31;
      });
    } else if (answers.length == 28) {
      setState(() {
        _currentIndex = 32;
      });
    }

    //fifth section
    if (answers.length == 29) {
      setState(() {
        _currentIndex = 34;
      });
    } else if (answers.length == 30) {
      setState(() {
        _currentIndex = 35;
      });
    } else if (answers.length == 31) {
      setState(() {
        _currentIndex = 36;
      });
    } else if (answers.length == 32) {
      setState(() {
        _currentIndex = 37;
      });
    } else if (answers.length == 33) {
      setState(() {
        _currentIndex = 38;
      });
    } else if (answers.length == 34) {
      setState(() {
        _currentIndex = 39;
      });
    } else if (answers.length == 35) {
      setState(() {
        showDialog = true;
      });
    }
  }

  getAnswer() {
    //print('hi saya answer $sliderIndicator');
    literacyAnswer.clear();
    for (int i = 0; i < section1.length; i++) {
      if (sliderIndicator[i].round() == 1) {
        literacyAnswer.insert(i, 'section_1_answer_1_title'.tr());
      } else if (sliderIndicator[i].round() == 2) {
        literacyAnswer.insert(i, 'section_1_answer_2_title'.tr());
      } else if (sliderIndicator[i].round() == 3) {
        literacyAnswer.insert(i, 'section_1_answer_2_title'.tr());
      } else if (sliderIndicator[i].round() == 4) {
        literacyAnswer.insert(i, 'section_1_answer_3_title'.tr());
      }
    }

    for (int i = 10; i < 12; i++) {
      if (sliderIndicator[i].round() == 1) {
        literacyAnswer.insert(i, 'section_2_answer_1_title'.tr());
      } else if (sliderIndicator[i].round() == 2) {
        literacyAnswer.insert(i, 'section_2_answer_2_title'.tr());
      } else if (sliderIndicator[i].round() == 3) {
        literacyAnswer.insert(i, 'section_2_answer_3_title'.tr());
      } else if (sliderIndicator[i].round() == 4) {
        literacyAnswer.insert(i, 'section_2_answer_4_title'.tr());
      }
    }

    for (int i = 12; i < 15; i++) {
      if (sliderIndicator[i].round() == 1) {
        literacyAnswer.insert(i, 'section_3_answer_1_title'.tr());
      } else if (sliderIndicator[i].round() == 2) {
        literacyAnswer.insert(i, 'section_3_answer_2_title'.tr());
      } else if (sliderIndicator[i].round() == 3) {
        literacyAnswer.insert(i, 'section_3_answer_3_title'.tr());
      } else if (sliderIndicator[i].round() == 4) {
        literacyAnswer.insert(i, 'section_3_answer_4_title'.tr());
      }
    }

    for (int i = 15; i < 28; i++) {
      if (sliderIndicator[i].round() == 1) {
        literacyAnswer.insert(i, 'section_4_answer_1_title'.tr());
      } else if (sliderIndicator[i].round() == 2) {
        literacyAnswer.insert(i, 'section_4_answer_2_title'.tr());
      } else if (sliderIndicator[i].round() == 3) {
        literacyAnswer.insert(i, 'section_4_answer_3_title'.tr());
      } else if (sliderIndicator[i].round() == 4) {
        literacyAnswer.insert(i, 'section_4_answer_4_title'.tr());
      } else if (sliderIndicator[i].round() == 5) {
        literacyAnswer.insert(i, 'section_4_answer_5_title'.tr());
      }
    }
    for (int i = 28; i < 35; i++) {
      if (sliderIndicator[i].round() == 1) {
        literacyAnswer.insert(i, 'section_5_answer_1_title'.tr());
      } else if (sliderIndicator[i].round() == 2) {
        literacyAnswer.insert(i, 'section_5_answer_2_title'.tr());
      } else if (sliderIndicator[i].round() == 3) {
        literacyAnswer.insert(i, 'section_5_answer_3_title'.tr());
      } else if (sliderIndicator[i].round() == 4) {
        literacyAnswer.insert(i, 'section_5_answer_4_title'.tr());
      } else if (sliderIndicator[i].round() == 5) {
        literacyAnswer.insert(i, 'section_5_answer_5_title'.tr());
      }
    }
    //print('hi saya answer2 ${literacyAnswer[0]} ${literacyAnswer.length}');
  }
}

class LiteracyScreen extends StatelessWidget {
  final String? instruction,
      text1,
      text2,
      text3,
      text4,
      text5,
      text1desc,
      text2desc,
      text3desc,
      text4desc,
      text5desc;
  const LiteracyScreen({
    Key? key,
    this.instruction,
    this.text1,
    this.text2,
    this.text3,
    this.text4,
    this.text5,
    this.text1desc,
    this.text2desc,
    this.text3desc,
    this.text4desc,
    this.text5desc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height -
    //     AppBar().preferredSize.height -
    //     MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: width * 0.85,
              decoration: BoxDecoration(
                color: themeColor,
                borderRadius: BorderRadius.circular(20),
              ),
              padding: EdgeInsets.symmetric(
                vertical: 15,
                horizontal: 25,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  RichText(
                    // textAlign: TextAlign.justify,
                    text: TextSpan(
                      text: instruction,
                      style: TextStyle(
                        fontSize: width * 0.04,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    text1!,
                    style: TextStyle(
                      fontSize: width * 0.04,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    text1desc!,
                    style: TextStyle(
                      fontSize: width * 0.04,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    text2!,
                    style: TextStyle(
                      fontSize: width * 0.04,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    text2desc!,
                    style: TextStyle(
                      fontSize: width * 0.04,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    text3!,
                    style: TextStyle(
                      fontSize: width * 0.04,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    text3desc!,
                    style: TextStyle(
                      fontSize: width * 0.04,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    text4!,
                    style: TextStyle(
                      fontSize: width * 0.04,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    text4desc!,
                    style: TextStyle(
                      fontSize: width * 0.04,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  if (text5 != null)
                    Text(
                      text5!,
                      style: TextStyle(
                        fontSize: width * 0.04,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  if (text5desc != null)
                    Text(
                      text5desc!,
                      style: TextStyle(
                        fontSize: width * 0.04,
                        color: Colors.white,
                      ),
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
