import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/Sehat/models/literacyQuestion.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

List<LiteracyQuestion> section1 = [];
List<LiteracyQuestion> section2 = [];
List<LiteracyQuestion> section3 = [];
List<LiteracyQuestion> section4 = [];
List<LiteracyQuestion> section5 = [];

List<Widget> pages = [];

List<Widget> section1Ques = [];
List<Widget> section2Ques = [];
List<Widget> section3Ques = [];
List<Widget> section4Ques = [];
List<Widget> section5Ques = [];

int totalQuestion = 0;

Future<bool> sentAnswerLiteracy(answer, questionid) async {
  bool status = false;
  int session = SehatGlobalVar.currentSession!;
  SecureStorage secureStorage = SecureStorage();
  String id = await secureStorage.readSecureData('userId');

  Map<String, String> map = {
    'id_question': questionid,
    'answer': answer,
    'token': TOKEN,
    'id_session': '$session',
    'id_selangkah_user': id,
  };

  print(map);

  final response = await http.post(
    Uri.parse('$API_URL_MENTAL/set_ans_cat6'),
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    body: map,
  );
  //print(response.body);

  if (response.body != '[]') {
    var data = json.decode(response.body);
    //print('hi saya result literacy $data');
    if (data['Data'] == true) {
      status = true;
    }
  }

  //check answer if got
  // bool statusCheck = false;

  // map = {
  //   'id_question': '11',
  //   'id_selangkah_user': id,
  //   'id_session': '$session',
  //   'token': TOKEN,
  // };

  // //print(map);

  // //print('calling check answer');

  // final responseCheck = await http.post(
  //   '$API_URL_MENTAL/check_ans',
  //   headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  //   body: map,
  // );

  // //print(responseCheck.body);

  // if (responseCheck.body != '[]') {
  //   var data = json.decode(responseCheck.body);
  //   if (data['Data'] == true) {
  //     statusCheck = true;
  //   }
  // }

  if (status) {
    return true;
  } else {
    return false;
  }
}

Future<int> getTotal() async {
  int total = 0;
  int session = SehatGlobalVar.currentSession!;
  SecureStorage secureStorage = SecureStorage();
  String id = await secureStorage.readSecureData('userId');

  final Map<String, String> map = {
    'category_no': '6',
    'token': TOKEN,
    'id_session': '$session',
    'id_selangkah_user': id,
  };

  final response = await http.post(
    Uri.parse('$API_URL_MENTAL/set_user_scoring'),
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    body: map,
  );

  if (response.body != '[]') {
    var data = json.decode(response.body);
    total = data['Data'];
  }

  return total;
}
