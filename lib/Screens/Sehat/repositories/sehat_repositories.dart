import 'dart:convert';
import 'dart:developer';

import 'package:date_format/date_format.dart';
import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class SehatRepositories {
  //get identity
  static Future<Identity> getIdentity() async {
    SecureStorage secureStorage = SecureStorage();

    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    //print('calling post get identity');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_sehat_identity'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    Identity identity = identityFromJson(response.body);

    return identity;
  }

  //set identity
  static Future<void> setIdentity(
    String age,
    String gender,
    String religion,
    String district,
    String income,
    String education,
    String workStatus,
    String maritalStatus,
    String houseHold,
    DateTime dob,
  ) async {
    SecureStorage secureStorage = SecureStorage();

    String id = await secureStorage.readSecureData('userId');

    int session = SehatGlobalVar.currentSession!;

    print(formatDate(dob, [yyyy, '-', mm, '-', dd]));

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'age': age,
      'gender': gender,
      'religion': religion,
      'district': district,
      'monthly_income': income,
      'education': education,
      'id_session': '$session',
      'work_status': workStatus,
      'marital_status': maritalStatus,
      'household_quantity': houseHold,
      'dob': formatDate(dob, [yyyy, '-', mm, '-', dd]),
      'token': TOKEN,
    };

    //print('calling post set identity');

    await http.post(
      Uri.parse('$API_URL_MENTAL/set_sehat_identity'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    //print(response.body);
  }

  //get session
  static Future<Session> getSession() async {
    SecureStorage secureStorage = SecureStorage();

    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    //print('calling post get session');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_user_session'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    Session session = sessionFromJson(response.body);

    return session;
  }

  //set session
  static Future<void> setSession() async {
    SecureStorage secureStorage = SecureStorage();

    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    //print('calling post set session');

    await http.post(
      Uri.parse('$API_URL_MENTAL/set_start_session_new'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );
  }

  static Future<Certificate> getCertificateDetails() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      // 'id_selangkah_user': '36471',
    };

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_appt_dtl'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    if (response.body == '{"Code":200,"Data":null}') {
      return Certificate();
    } else {
      Certificate certificate = certificateFromJson(response.body);
      return certificate;
    }
  }

  static Future<Appointment> getAppointment() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      // 'id_selangkah_user': '36471',
    };

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_appointment_new'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    if (response.body == '{"Code":200,"Data":[]}') {
      return Appointment();
    } else {
      Appointment appointment = appointmentFromJson(response.body);
      return appointment;
    }
  }

  static Future<void> changeAppointment(
      String idProvider, String idTest, String idProgram, String date) async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'id_provider': idProvider,
      'test_id': idTest,
      'id_program_appointment': idProgram,
      'slot_date': date,
    };

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/change_appointment'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
    // var data = jsonDecode(response.body);
    // bool reschedule = data['Data'];
    // print(reschedule);
    // return reschedule;
  }

  //get journey
  static Future<Journey> getJourney(int session) async {
    SecureStorage secureStorage = SecureStorage();

    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'id_session': '$session',
      'token': TOKEN,
    };

    print('calling post get journey new');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_sehat_journey_new'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    Journey journey = journeyFromJson(response.body);

    return journey;
  }

  //get profile
  static Future<RawProfile?> getProfile() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id': id,
      'token': TOKEN,
    };

    //print('calling post get profile');

    final response = await http.post(
      Uri.parse('$API_URL/check_user_profile'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    RawProfile? profile = rawProfileFromJson(response.body);

    return profile;
  }

  //set answer cat3
  static Future<bool> setAnswerCat3(String answer) async {
    SecureStorage secureStorage = SecureStorage();

    String id = await secureStorage.readSecureData('userId');

    int session = SehatGlobalVar.currentSession!;

    Map<String, String> map = {
      'id_question': '11',
      'answer': answer,
      'id_session': '$session',
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    //print('calling post set answer cat3');

    await http.post(
      Uri.parse('$API_URL_MENTAL/set_ans_cat3'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    //check answer if got
    bool statusCheck = false;

    map = {
      'id_question': '11',
      'id_selangkah_user': id,
      'id_session': '$session',
      'token': TOKEN,
    };

    //print(map);

    //print('calling check answer');

    final responseCheck = await http.post(
      Uri.parse('$API_URL_MENTAL/check_ans'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    //print(responseCheck.body);

    if (responseCheck.body != '[]') {
      var data = json.decode(responseCheck.body);
      if (data['Data'] == true) {
        statusCheck = true;
      }
    }

    if (statusCheck) {
      return true;
    } else {
      return false;
    }
  }

  //set whatsdoc
  static Future<void> setAnswerCat5() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');
    int session = SehatGlobalVar.currentSession!;

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'id_session': '$session',
      'token': TOKEN,
    };

    //print('calling post set whatsdoc');

    await http.post(
      Uri.parse('$API_URL_MENTAL/set_status_cat5'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );
  }

  //get total answer
  static Future<int> getTotal(String category) async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');
    int session = SehatGlobalVar.currentSession!;

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'id_session': '$session',
      'category_no': category,
      'token': TOKEN,
    };

    print(map);

    //print('calling post get total score');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/set_user_scoring'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    Total total = totalFromJson(response.body);

    int totalConverted = total.data!;

    return totalConverted;
  }

  //get category
  static Future<Category> getCategory(String language) async {
    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
    };

    //print('calling post get category');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_cat_new'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    Category category = categoryFromJson(response.body);

    // Category category = categoryFromJson(
    //     '{"Code":200,"Data":[{"id":"1","category_name":"Pengesahan Identiti","display_no":"1","have_child":0},{"id":"2","category_name":"Ujian Saringan Kesihatan Mental","display_no":"2","have_child":1},{"id":"3","category_name":"Saringan Risiko","display_no":"3","have_child":0},{"id":"4","category_name":"Video Pendidikan Psiko SEHAT","display_no":"5","have_child":0},{"id":"5","category_name":"Talian Sokongan Psikososial Sehat","display_no":"6","have_child":0},{"id":"6","category_name":"Skala Literasi Kesihatan Mental","display_no":"7","have_child":0},{"id":"7","category_name":"Modul Kesihatan Mental","display_no":"4","have_child":1}]}');

    return category;
  }

  //send feedback
  static Future<void> sendFeedback(
    double rating,
    String comment,
  ) async {
    String id = await SecureStorage().readSecureData('userId');

    int ratingInt = rating.round();

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'rating': ratingInt.toString(),
      'comment': comment,
      'token': TOKEN,
    };

    //print('calling post get category');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/user_feedback'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }

  static Future<void> setAppointment(
    String idProvider,
    String date,
    String language,
  ) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'id_provider': idProvider,
      'slot_date': date,
      'id_session': '${SehatGlobalVar.currentSession}',
      'language': language,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/create_appointment'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }

  static Future<SehatLine> getSehatLine() async {
    final Map<String, String> map = {
      'token': TOKEN,
    };

    //print('calling post get category');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/retrieve_phonenumber'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    SehatLine sehatLine = sehatLineFromJson(response.body);

    // SehatLine sehatLine = sehatLineFromJson(
    //     '{"Code":200,"Data":{"tel_1":"1700-82-7536","tel_2":"1700-82-7536"}}');

    return sehatLine;
  }

  //get module
  static Future<Category> getCategoryModule(
      String idCategory, String language) async {
    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
      'id_cat': idCategory,
    };

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_module'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    Category category = categoryFromJson(response.body);
    return category;
  }

  //get activity
  static Future<Content> getContent(
    String idCategory,
    String language,
    bool isQuestionnaire,
  ) async {
    Map<String, String> map = {};

    if (isQuestionnaire) {
      map = {
        'language': language,
        'token': TOKEN,
        'id_cat': idCategory,
      };
    } else {
      map = {
        'language': language,
        'token': TOKEN,
        'id_activity': idCategory,
      };
    }

    print(map);
    print('calling get content 1');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_content'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    // print(response.body);
    log(response.body);

    // String newString = response.body.replaceAll('"data":[]"', '"data":null"');

    // log(newString);

    Content content = contentFromJson(response.body);
    // Content content = contentFromJson(
    //     '{"Code":200,"Data":[{"page":1,"card":[{"id_mental_content":"34","content_type":"5","data":{"id":"4","image":"https://app.selangkah.my/cache/general/sehat2/i-ACT%20Pembukaan%201.png","indicator":null}},{"id_mental_content":"35","content_type":"3","data":{"id":"5","article_text":"<body>Assalamualaikum dan selamat datang ke i-ACT for Life, sebuah program pencegahan dan\npengurusan tekanan yang mengandungi elemen kerohanian Islam dan berasaskan Acceptance\nCommitment Therapy (Terapi Penerimaan dan Komitmen!\n\nSebagai seorang manusia biasa, anda mungkin menghadapi cabaran dari segi pembelajaran,\nkewangan, atau perhubungan yang boleh menjejaskan kesihatan mental anda. Apabila\nmenghadapi cabaran-cabaran tersebut, anda mungkin akan rasa:\n<ul><li><strong>Bimbang</strong>: Perasaan risau, takut, dan ketidakpastian</li>\n<li><strong>Murung</strong>: Perasaan putus asa, tiada motivasi, dan sedih</li>\n<li><strong>Tertekan</strong>: Rasa sangat tertekan dalam memastikan tanggungjawab dari segi pembelajaran,\nkewangan, dan pekerjaan dilakukan dengan sebaiknya</li></ul></body>","indicator":null}}]},{"page":2,"card":[{"id_mental_content":"170","content_type":"5","data":{"id":"25","image":"https://app.selangkah.my/cache/general/sehat2/i-ACT%20Pembukaan%202.png","indicator":null}},{"id_mental_content":"171","content_type":"3","data":null}]},{"page":3,"card":[{"id_mental_content":"172","content_type":"5","data":{"id":"27","image":"https://app.selangkah.my/cache/general/sehat2/i-ACT%20Pembukaan%203.png","indicator":null}},{"id_mental_content":"173","content_type":"3","data":{"id":"28","article_text":"<body>Untuk mendapat manfaat sepenuhnya daripada program ini, anda perlu memberi komitmen 10-15 minit setiap hari selama program ini berlangsung.\r\n\rLatihan harian ini direka khusus untuk membantu anda membina kemahiran yang diperlukan agar dapat bertahan menghadapi cabaran masa depan.</body>","indicator":null}}]}]}');

    return content;
  }

  //get pathway
  static Future<Pathway> getPathway(String idModule, String language) async {
    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
      'id_module': idModule,
    };

    print(map);
    print('calling get pathway');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_pathway'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    Pathway pathway = pathwayFromJson(response.body);
    return pathway;
  }

  //get pathway
  static Future<Activity> getActivity(String idPathway, String language) async {
    final Map<String, String> map = {
      'language': language,
      'token': TOKEN,
      'id_pathway': idPathway,
    };

    print(map);
    print('calling get activity');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_activity'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    Activity activity = activityFromJson(response.body);
    return activity;
  }

  //set answer
  static Future<void> setAnswer({
    required String idQuestion,
    required String answer,
    required String contentType,
    required String idMentalContent,
    required String idCategory,
  }) async {
    String id = await SecureStorage().readSecureData('userId');

    Map<String, String> map;

    if (idCategory == '19') {
      map = {
        'id_selangkah_user': id,
        'id_session': '${SehatGlobalVar.currentSession}',
        'token': TOKEN,
        'id_question': idQuestion,
        'answer': answer,
        'content_type': contentType,
        'id_mental_content': idMentalContent,
        'id_dass_followup': SehatGlobalVar.idDassFollowup!,
      };
    } else {
      map = {
        'id_selangkah_user': id,
        'id_session': '${SehatGlobalVar.currentSession}',
        'token': TOKEN,
        'id_question': idQuestion,
        'answer': answer,
        'content_type': contentType,
        'id_mental_content': idMentalContent,
      };
    }

    print(map);
    print('calling set answer');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/set_ans'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print('response answer is ${response.body}');
  }

  //set score
  static Future<void> setScore(String idCategory) async {
    String id = await SecureStorage().readSecureData('userId');

    Map<String, String> map;

    if (idCategory == '19') {
      map = {
        'id_session': '${SehatGlobalVar.currentSession}',
        'category_no': idCategory,
        'id_selangkah_user': id,
        'token': TOKEN,
        'id_dass_followup': SehatGlobalVar.idDassFollowup!,
      };
    } else {
      map = {
        'id_session': '${SehatGlobalVar.currentSession}',
        'category_no': idCategory,
        'id_selangkah_user': id,
        'token': TOKEN,
      };
    }

    print(map);
    print('calling set score');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/set_user_scoring_new'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }

  //get score
  static Future<Score> getScore(String language) async {
    String id = await SecureStorage().readSecureData('userId');
    final Map<String, String> map = {
      'id_session': '${SehatGlobalVar.currentSession}',
      'id_selangkah_user': id,
      'language': language,
      'token': TOKEN,
    };

    print(map);
    print('calling get score');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_notification_score'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    Score score = scoreFromJson(response.body);
    // Score score = scoreFromJson(
    //     '{"Code":200,"Data":[{"id_mental_question_cat":"1","title":null,"child":[]},{"id_mental_question_cat":"15","title":"GDS-15","child":[]},{"id_mental_question_cat":"12","title":"PSS-10","child":[{"name":"Tekanan","score":"17","color":"#FF5733","level":"SEDERHANA"}]},{"id_mental_question_cat":"12","title":"PSS-10","child":[{"name":"Tekanan","score":"17","color":"#FF5733","level":"SEDERHANA"}]}]}');
    return score;

    // Score score = scoreFromJson(response.body);
    // // Score score = scoreFromJson(
    // //     '{"Code":200,"Data":[{"id_mental_question_cat":"1","title":null,"child":[]},{"id_mental_question_cat":"15","title":"GDS-15","child":[]},{"id_mental_question_cat":"12","title":"PSS-10","child":[{"name":"Tekanan","score":"17","color":"#FF5733","level":"SEDERHANA"}]},{"id_mental_question_cat":"12","title":"PSS-10","child":[{"name":"Tekanan","score":"17","color":"#FF5733","level":"SEDERHANA"}]}]}');
    // return score;
  }

  static Future<Score> getApptScore(String language) async {
    String id = await SecureStorage().readSecureData('userId');
    final Map<String, String> map = {
      'id_session': '${SehatGlobalVar.currentSession}',
      'id_selangkah_user': id,
      'language': language,
      'token': TOKEN,
    };

    print(map);
    print('calling get new score');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_appt_score'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    Score score = scoreFromJson(response.body);
    // Score score = scoreFromJson(
    //     '{"Code":200,"Data":[{"id_mental_question_cat":"1","title":null,"child":[]},{"id_mental_question_cat":"15","title":"GDS-15","child":[]},{"id_mental_question_cat":"12","title":"PSS-10","child":[{"name":"Tekanan","score":"17","color":"#FF5733","level":"SEDERHANA"}]},{"id_mental_question_cat":"12","title":"PSS-10","child":[{"name":"Tekanan","score":"17","color":"#FF5733","level":"SEDERHANA"}]}]}');
    return score;
  }

  //get score
  static Future<ActivityComplete> getActivityComplete() async {
    String id = await SecureStorage().readSecureData('userId');
    final Map<String, String> map = {
      'id_session': '${SehatGlobalVar.currentSession}',
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    print(map);
    print('calling get activity complete');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_user_activity'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    ActivityComplete activityComplete = activityCompleteFromJson(response.body);
    // Score score = scoreFromJson(
    //     '{"Code":200,"Data":[{"id_mental_question_cat":"1","title":null,"child":[]},{"id_mental_question_cat":"15","title":"GDS-15","child":[]},{"id_mental_question_cat":"12","title":"PSS-10","child":[{"name":"Tekanan","score":"17","color":"#FF5733","level":"SEDERHANA"}]},{"id_mental_question_cat":"12","title":"PSS-10","child":[{"name":"Tekanan","score":"17","color":"#FF5733","level":"SEDERHANA"}]}]}');
    return activityComplete;
  }

  //get noti text
  static Future<String> getNotiText(String warningId) async {
    String id = await SecureStorage().readSecureData('userId');
    final Map<String, String> map = {
      'id_session': '${SehatGlobalVar.currentSession}',
      'id_selangkah_user': id,
      'token': TOKEN,
      'language': GlobalVariables.language!,
      'warning_flag': warningId,
      'id_mental_session': '${SehatGlobalVar.currentSession}',
    };

    print(map);
    print('calling noti text');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_noti_text'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
    var data = json.decode(response.body);
    String text = data["Data"];
    return text;
  }

  static Future<SlotAppointment> getWarnStatus() async {
    String id = await SecureStorage().readSecureData('userId');
    final Map<String, String> map = {
      'token': TOKEN,
      'id_session': '${SehatGlobalVar.currentSession}',
      'id_selangkah_user': id,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_warning_status'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
    // var data = json.decode(response.body);
    // bool text = data["Data"];
    // return text;

    SlotAppointment slotAppointment = slotAppointmentFromJson(response.body);
    return slotAppointment;
  }

  //get clinic
  static Future<SehatClinic> getClinic() async {
    final Map<String, String> map = {
      'token': TOKEN,
    };

    print('calling get clinic');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_provider'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    SehatClinic sehatClinic = sehatClinicFromJson(response.body);
    return sehatClinic;
  }

  //get date
  static Future<SehatDate> getDate(String idProvider) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'id_provider': idProvider,
    };

    print('calling get date');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/get_slot_date'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    SehatDate sehatDate = sehatDateFromJson(response.body);
    return sehatDate;
  }

  //get notification
  static Future<bool> getNotification() async {
    SecureStorage secureStorage = SecureStorage();

    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    // final random = math.Random();
    // return random.nextBool();

    //print('calling post get identity');

    final response = await http.post(
      Uri.parse('$API_URL/get_sehat_noti'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    var data = json.decode(response.body);
    String notification = data['Data'];

    if (notification == '0') {
      return true;
    } else {
      return false;
    }
  }

  //set score
  static Future<void> setNotification(String onOff) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'onoff': onOff,
      'token': TOKEN,
    };

    print(map);
    print('calling set score');

    final response = await http.post(
      Uri.parse('$API_URL/update_sehat_noti'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }

  //get dass followup id
  //get date
  static Future<void> getDassFollowupId() async {
    String id = await SecureStorage().readSecureData('userId');

    Session session = await SehatRepositories.getSession();
    int sessionConverted = int.parse(session.session!.id!);
    SehatGlobalVar.currentSession = sessionConverted;

    final Map<String, String> map = {
      'token': TOKEN,
      'id_session': '${SehatGlobalVar.currentSession}',
      'id_selangkah_user': id,
    };

    print(map);
    print('calling get dass id');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/set_dass_followup'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    var data = jsonDecode(response.body);
    String dassId = data['Data'];

    SehatGlobalVar.idDassFollowup = dassId;
  }

  //api bila dass siap jawab
  static Future<void> sendDassComplete() async {
    final Map<String, String> map = {
      'token': TOKEN,
      'id_session': '${SehatGlobalVar.currentSession}',
      'id_dass_followup': SehatGlobalVar.idDassFollowup!,
    };

    print('calling get sendDass');

    final response = await http.post(
      Uri.parse('$API_URL_MENTAL/set_dass_complete'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }
}

class SehatGlobalVar {
  static Journey? journeyGlobal;
  static bool journeyFirst = true;
  static int? currentSession;
  static bool dismissedPopUpStress = false;
  static bool dismissedPopUpMental = false;

  //answer api
  static int currentContentIndex = 0;
  static String? contentType;
  static String? answer;
  static String? idQuestion;
  static String? idMentalContent;

  //sehat
  static bool canNextQuiz = true;

  static String? idDassFollowup;
}
