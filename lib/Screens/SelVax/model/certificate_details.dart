// To parse this JSON data, do
//
//     final certificateDetails = certificateDetailsFromJson(jsonString);

import 'dart:convert';

CertificateDetails certificateDetailsFromJson(String str) =>
    CertificateDetails.fromJson(json.decode(str));

String certificateDetailsToJson(CertificateDetails data) =>
    json.encode(data.toJson());

class CertificateDetails {
  int? code;
  String? status;
  List<Datum>? data;

  CertificateDetails({
    this.code,
    this.status,
    this.data,
  });

  factory CertificateDetails.fromJson(Map<String, dynamic> json) =>
      CertificateDetails(
        code: json["Code"],
        status: json["Status"],
        data: json["Data"] == null
            ? []
            : List<Datum>.from(json["Data"]!.map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Status": status,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  String? id;
  String? accName;
  String? icNo;
  DateTime? date;
  String? ppvName;
  String? programName;
  String? journeyStatus;
  String? session1Start;
  String? session1End;
  String? idAppointment;
  String? batchNo;
  String? dependent;

  Datum({
    this.id,
    this.accName,
    this.icNo,
    this.date,
    this.ppvName,
    this.programName,
    this.journeyStatus,
    this.session1Start,
    this.session1End,
    this.idAppointment,
    this.batchNo,
    this.dependent,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        accName: json["acc_name"],
        icNo: json["ic_no"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        ppvName: json["ppv_name"],
        programName: json["program_name"],
        journeyStatus: json["journey_status"],
        session1Start: json["session1_start"],
        session1End: json["session1_end"],
        idAppointment: json["id_appointment"],
        batchNo: json["batch_no"],
        dependent: json["dependent"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "acc_name": accName,
        "ic_no": icNo,
        "date":
            "${date!.year.toString().padLeft(4, '0')}-${date!.month.toString().padLeft(2, '0')}-${date!.day.toString().padLeft(2, '0')}",
        "ppv_name": ppvName,
        "program_name": programName,
        "journey_status": journeyStatus,
        "session1_start": session1Start,
        "session1_end": session1End,
        "id_appointment": idAppointment,
        "batch_no": batchNo,
        "dependent": dependent,
      };
}
