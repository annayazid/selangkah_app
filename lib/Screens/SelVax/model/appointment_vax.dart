// To parse this JSON data, do
//
//     final appointmentVax = appointmentVaxFromJson(jsonString);

import 'dart:convert';

AppointmentVax appointmentVaxFromJson(String str) =>
    AppointmentVax.fromJson(json.decode(str));

String appointmentVaxToJson(AppointmentVax data) => json.encode(data.toJson());

class AppointmentVax {
  int? code;
  String? status;
  List<AppointmentVaxData>? data;

  AppointmentVax({
    this.code,
    this.status,
    this.data,
  });

  factory AppointmentVax.fromJson(Map<String, dynamic> json) => AppointmentVax(
        code: json["Code"],
        status: json["Status"],
        data: json["Data"] == null
            ? []
            : List<AppointmentVaxData>.from(
                json["Data"]!.map((x) => AppointmentVaxData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Status": status,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class AppointmentVaxData {
  String? id;
  String? accName;
  String? icNo;
  DateTime? date;
  String? ppvName;
  String? programName;
  String? journeyStatus;
  String? session1Start;
  String? session1End;
  String? idAppointment;

  AppointmentVaxData({
    this.id,
    this.accName,
    this.icNo,
    this.date,
    this.ppvName,
    this.programName,
    this.journeyStatus,
    this.session1Start,
    this.session1End,
    this.idAppointment,
  });

  factory AppointmentVaxData.fromJson(Map<String, dynamic> json) =>
      AppointmentVaxData(
        id: json["id"],
        accName: json["acc_name"],
        icNo: json["ic_no"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        ppvName: json["ppv_name"],
        programName: json["program_name"],
        journeyStatus: json["journey_status"],
        session1Start: json["session1_start"],
        session1End: json["session1_end"],
        idAppointment: json["id_appointment"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "acc_name": accName,
        "ic_no": icNo,
        "date":
            "${date!.year.toString().padLeft(4, '0')}-${date!.month.toString().padLeft(2, '0')}-${date!.day.toString().padLeft(2, '0')}",
        "ppv_name": ppvName,
        "program_name": programName,
        "journey_status": journeyStatus,
        "session1_start": session1Start,
        "session1_end": session1End,
        "id_appointment": idAppointment,
      };
}
