// To parse this JSON data, do
//
//     final programVaccine = programVaccineFromJson(jsonString);

import 'dart:convert';

ProgramVaccine programVaccineFromJson(String str) =>
    ProgramVaccine.fromJson(json.decode(str));

String programVaccineToJson(ProgramVaccine data) => json.encode(data.toJson());

class ProgramVaccine {
  int? code;
  List<VaccineData>? data;

  ProgramVaccine({
    this.code,
    this.data,
  });

  factory ProgramVaccine.fromJson(Map<String, dynamic> json) => ProgramVaccine(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<VaccineData>.from(
                json["Data"]!.map((x) => VaccineData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class VaccineData {
  String? id;
  String? idProgram;
  String? openAppointment;
  String? url;
  String? name;
  String? programName;
  String? enable;
  String? createdBy;
  DateTime? createdTs;

  VaccineData({
    this.id,
    this.idProgram,
    this.openAppointment,
    this.url,
    this.name,
    this.programName,
    this.enable,
    this.createdBy,
    this.createdTs,
  });

  factory VaccineData.fromJson(Map<String, dynamic> json) => VaccineData(
        id: json["id"],
        idProgram: json["id_program"],
        openAppointment: json["open_appointment"],
        url: json["url"],
        name: json["name"],
        programName: json["program_name"],
        enable: json["enable"],
        createdBy: json["created_by"],
        createdTs: json["created_ts"] == null
            ? null
            : DateTime.parse(json["created_ts"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_program": idProgram,
        "open_appointment": openAppointment,
        "url": url,
        "name": name,
        "program_name": programName,
        "enable": enable,
        "created_by": createdBy,
        "created_ts": createdTs?.toIso8601String(),
      };
}
