// To parse this JSON data, do
//
//     final appointment = appointmentFromJson(jsonString);

import 'dart:convert';

Appointment appointmentFromJson(String str) =>
    Appointment.fromJson(json.decode(str));

String appointmentToJson(Appointment data) => json.encode(data.toJson());

class Appointment {
  Appointment({
    this.status,
    this.data,
  });

  String? status;
  List<AppointmentData>? data;

  factory Appointment.fromJson(Map<String, dynamic> json) => Appointment(
        status: json["status"],
        data: json["data"] == null
            ? []
            : List<AppointmentData>.from(
                json["data"]!.map((x) => AppointmentData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class AppointmentData {
  AppointmentData({
    this.accName,
    this.dob,
    this.gender,
    this.citizen,
    this.icNo,
    this.idType,
    this.phoneNo,
    this.appDate1,
    this.generic1,
    this.batchNo1,
    this.manufacturer1,
    this.picName1,
    this.ppvName1,
    this.appDate2,
    this.generic2,
    this.batchNo2,
    this.manufacturer2,
    this.picName2,
    this.ppvName2,
    this.lastAssessment,
    this.completed1,
    this.completed2,
    this.ppvName,
    this.ppvAddressLine1,
    this.ppvAddressLine2,
    this.ppvPostcode,
    this.ppvCity,
    this.ppvState,
    this.compname,
    this.idPpv,
    this.sentToMimos,
    this.sentToMimosAt,
    this.picMmc1,
    this.picMmc2,
    this.expiryDose1,
    this.expiryDose2,
  });

  String? accName;
  DateTime? dob;
  String? gender;
  String? citizen;
  String? icNo;
  String? idType;
  String? phoneNo;
  DateTime? appDate1;
  String? generic1;
  String? batchNo1;
  String? manufacturer1;
  String? picName1;
  String? ppvName1;
  DateTime? appDate2;
  String? generic2;
  String? batchNo2;
  String? manufacturer2;
  String? picName2;
  String? ppvName2;
  DateTime? lastAssessment;
  int? completed1;
  int? completed2;
  String? ppvName;
  String? ppvAddressLine1;
  dynamic ppvAddressLine2;
  String? ppvPostcode;
  String? ppvCity;
  String? ppvState;
  String? compname;
  String? idPpv;
  String? sentToMimos;
  dynamic sentToMimosAt;
  String? picMmc1;
  String? picMmc2;
  DateTime? expiryDose1;
  DateTime? expiryDose2;

  factory AppointmentData.fromJson(Map<String, dynamic> json) =>
      AppointmentData(
        accName: json["acc_name"],
        dob: json["dob"] == null ? null : DateTime.parse(json["dob"]),
        gender: json["gender"],
        citizen: json["citizen"],
        icNo: json["ic_no"],
        idType: json["id_type"],
        phoneNo: json["phone_no"],
        appDate1: json["app_date1"] == null
            ? null
            : DateTime.parse(json["app_date1"]),
        generic1: json["generic_1"],
        batchNo1: json["batch_no1"],
        manufacturer1: json["manufacturer1"],
        picName1: json["pic_name1"],
        ppvName1: json["ppv_name1"],
        appDate2: json["app_date2"] == null
            ? null
            : DateTime.parse(json["app_date2"]),
        generic2: json["generic_2"],
        batchNo2: json["batch_no2"],
        manufacturer2: json["manufacturer2"],
        picName2: json["pic_name2"],
        ppvName2: json["ppv_name2"],
        lastAssessment: json["last_assessment"] == null
            ? null
            : DateTime.parse(json["last_assessment"]),
        completed1: json["completed_1"],
        completed2: json["completed_2"],
        ppvName: json["ppv_name"],
        ppvAddressLine1: json["ppv_address_line1"],
        ppvAddressLine2: json["ppv_address_line2"],
        ppvPostcode: json["ppv_postcode"],
        ppvCity: json["ppv_city"],
        ppvState: json["ppv_state"],
        compname: json["compname"],
        idPpv: json["id_ppv"],
        sentToMimos: json["sent_to_mimos"],
        sentToMimosAt: json["sent_to_mimos_at"],
        picMmc1: json["pic_mmc1"],
        picMmc2: json["pic_mmc2"],
        expiryDose1: json["expiry_dose1"] == null
            ? null
            : DateTime.parse(json["expiry_dose1"]),
        expiryDose2: json["expiry_dose2"] == null
            ? null
            : DateTime.parse(json["expiry_dose2"]),
      );

  Map<String, dynamic> toJson() => {
        "acc_name": accName,
        "dob":
            "${dob!.year.toString().padLeft(4, '0')}-${dob!.month.toString().padLeft(2, '0')}-${dob!.day.toString().padLeft(2, '0')}",
        "gender": gender,
        "citizen": citizen,
        "ic_no": icNo,
        "id_type": idType,
        "phone_no": phoneNo,
        "app_date1":
            "${appDate1!.year.toString().padLeft(4, '0')}-${appDate1!.month.toString().padLeft(2, '0')}-${appDate1!.day.toString().padLeft(2, '0')}",
        "generic_1": generic1,
        "batch_no1": batchNo1,
        "manufacturer1": manufacturer1,
        "pic_name1": picName1,
        "ppv_name1": ppvName1,
        "app_date2":
            "${appDate2!.year.toString().padLeft(4, '0')}-${appDate2!.month.toString().padLeft(2, '0')}-${appDate2!.day.toString().padLeft(2, '0')}",
        "generic_2": generic2,
        "batch_no2": batchNo2,
        "manufacturer2": manufacturer2,
        "pic_name2": picName2,
        "ppv_name2": ppvName2,
        "last_assessment": lastAssessment?.toIso8601String(),
        "completed_1": completed1,
        "completed_2": completed2,
        "ppv_name": ppvName,
        "ppv_address_line1": ppvAddressLine1,
        "ppv_address_line2": ppvAddressLine2,
        "ppv_postcode": ppvPostcode,
        "ppv_city": ppvCity,
        "ppv_state": ppvState,
        "compname": compname,
        "id_ppv": idPpv,
        "sent_to_mimos": sentToMimos,
        "sent_to_mimos_at": sentToMimosAt,
        "pic_mmc1": picMmc1,
        "pic_mmc2": picMmc2,
        "expiry_dose1":
            "${expiryDose1!.year.toString().padLeft(4, '0')}-${expiryDose1!.month.toString().padLeft(2, '0')}-${expiryDose1!.day.toString().padLeft(2, '0')}",
        "expiry_dose2":
            "${expiryDose2!.year.toString().padLeft(4, '0')}-${expiryDose2!.month.toString().padLeft(2, '0')}-${expiryDose2!.day.toString().padLeft(2, '0')}",
      };
}
