import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/SelVax/selvax.dart';

part 'appointment_vax_state.dart';

class AppointmentVaxCubit extends Cubit<AppointmentVaxState> {
  AppointmentVaxCubit() : super(AppointmentVaxInitial());

  Future<void> getAppointmentVax() async {
    emit(AppointmentVaxLoading());

    //
    AppointmentVax appointmentVax =
        await SelangkahVaxRepositories.getAppointmentVax();

    emit(AppointmentVaxLoaded(appointmentVax));
  }
}
