part of 'appointment_vax_cubit.dart';

@immutable
abstract class AppointmentVaxState {
  const AppointmentVaxState();
}

class AppointmentVaxInitial extends AppointmentVaxState {
  const AppointmentVaxInitial();
}

class AppointmentVaxLoading extends AppointmentVaxState {
  const AppointmentVaxLoading();
}

class AppointmentVaxLoaded extends AppointmentVaxState {
  final AppointmentVax appointmentVax;

  AppointmentVaxLoaded(this.appointmentVax);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AppointmentVaxLoaded &&
        other.appointmentVax == appointmentVax;
  }

  @override
  int get hashCode => appointmentVax.hashCode;
}
