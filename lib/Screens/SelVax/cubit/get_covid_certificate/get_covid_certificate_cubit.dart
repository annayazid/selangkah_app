import 'package:bloc/bloc.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/SelVax/selvax.dart';

part 'get_covid_certificate_state.dart';

class GetCovidCertificateCubit extends Cubit<GetCovidCertificateState> {
  GetCovidCertificateCubit() : super(GetCovidCertificateInitial());

  Future<void> getCovidCert() async {
    Appointment? vaccineAppointment =
        await SelangkahVaxRepositories.getAppointment();

    if (vaccineAppointment.data != null) {
      vaccineAppointment.data!.removeWhere(
          (element) => element.appDate1 == null || element.appDate2 == null);

      if (vaccineAppointment.data != null) {
        for (var i = 0; i < vaccineAppointment.data!.length; i++) {
          if (vaccineAppointment.data![i].appDate1 != null &&
              vaccineAppointment.data![i].appDate2 != null) {
            vaccineAppointment.data![i].gender =
                vaccineAppointment.data![i].gender == 'M' ? 'Male' : 'Female';

            if (vaccineAppointment.data![i].citizen != 'To be updated') {
              vaccineAppointment.data![i].citizen =
                  CountryPickerUtils.getCountryByIsoCode(
                          vaccineAppointment.data![i].citizen!)
                      .name;
            }

            if (vaccineAppointment.data![i].idType == 'ICNO') {
              vaccineAppointment.data![i].idType = 'I/C Number';
            } else if (vaccineAppointment.data![i].idType == 'POLICE') {
              vaccineAppointment.data![i].idType = 'Police';
            } else if (vaccineAppointment.data![i].idType == 'ARMY') {
              vaccineAppointment.data![i].idType = 'Army';
            } else if (vaccineAppointment.data![i].idType == 'UNHCR') {
              vaccineAppointment.data![i].idType = 'UNHCR';
            } else if (vaccineAppointment.data![i].idType == 'PASSPORT') {
              vaccineAppointment.data![i].idType = 'Passport';
            } else if (vaccineAppointment.data![i].idType == 'PERMRES') {
              vaccineAppointment.data![i].idType = 'Permanent Resident';
            } else if (vaccineAppointment.data![i].idType == 'SELANGKAH') {
              vaccineAppointment.data![i].idType = 'selangkah'.tr();
            } else if (vaccineAppointment.data![i].idType == 'OTHERS') {
              vaccineAppointment.data![i].idType = 'Others';
            } else {
              vaccineAppointment.data![i].idType = 'I/C Number';
            }

            if (vaccineAppointment.data![i].phoneNo != 'To be updated') {
              vaccineAppointment.data![i].phoneNo =
                  vaccineAppointment.data![i].phoneNo!.replaceAll('+', '');

              if (vaccineAppointment.data![i].phoneNo!.substring(0, 1) == '6') {
                vaccineAppointment.data![i].phoneNo = vaccineAppointment
                    .data![i].phoneNo!
                    .substring(1, vaccineAppointment.data![i].phoneNo!.length);
              }
            }
          }
        }
      }
    }

    emit(GetCovidCertificateLoaded(vaccineAppointment));
  }
}
