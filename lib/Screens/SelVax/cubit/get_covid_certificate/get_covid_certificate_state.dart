part of 'get_covid_certificate_cubit.dart';

@immutable
abstract class GetCovidCertificateState {
  const GetCovidCertificateState();
}

class GetCovidCertificateInitial extends GetCovidCertificateState {
  const GetCovidCertificateInitial();
}

class GetCovidCertificateLoading extends GetCovidCertificateState {
  const GetCovidCertificateLoading();
}

class GetCovidCertificateLoaded extends GetCovidCertificateState {
  final Appointment appointment;

  GetCovidCertificateLoaded(this.appointment);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetCovidCertificateLoaded &&
        other.appointment == appointment;
  }

  @override
  int get hashCode => appointment.hashCode;
}
