import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/SelVax/selvax.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';

part 'check_profile_state.dart';

class CheckProfileCubit extends Cubit<CheckProfileState> {
  CheckProfileCubit() : super(CheckProfileInitial());

  Future<void> checkProfile() async {
    emit(CheckProfileLoading());

    //check here

    RawProfile profile = await EKYCRepositories.getProfile();

    String? name = profile.data?[2];
    String? idCard = profile.data?[4];
    String? idType = profile.data?[17];
    String? citizenship = profile.data?[6];
    String? gender = profile.data?[7];
    String? dob = profile.data?[8];
    String? email = profile.data?[15];
    String? phone = profile.data?[19];
    String? address1 = profile.data?[9];
    String? postcode = profile.data?[11];
    String? city = profile.data?[12];
    String? state = profile.data?[13];
    String? country = profile.data?[14];
    // String? status = profile.data?[20];

    if (name == null ||
        idCard == null ||
        idType == null ||
        citizenship == null ||
        gender == null ||
        dob == null ||
        email == null ||
        phone == null ||
        address1 == null ||
        postcode == null ||
        city == null ||
        state == null ||
        country == null) {
      emit(CheckProfileIncomplete());
    } else {
      print('masuk sini');

      ProgramVaccine programVaccine =
          await SelangkahVaxRepositories.getProgram();

      CertificateDetails certificateDetails =
          await SelangkahVaxRepositories.getCert();

      emit(CheckProfileComplete(programVaccine, certificateDetails));
    }
  }
}
