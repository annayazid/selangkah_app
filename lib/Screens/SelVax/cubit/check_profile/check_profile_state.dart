part of 'check_profile_cubit.dart';

@immutable
abstract class CheckProfileState {
  const CheckProfileState();
}

class CheckProfileInitial extends CheckProfileState {
  const CheckProfileInitial();
}

class CheckProfileLoading extends CheckProfileState {
  const CheckProfileLoading();
}

class CheckProfileIncomplete extends CheckProfileState {
  const CheckProfileIncomplete();
}

class CheckProfileComplete extends CheckProfileState {
  final ProgramVaccine programVaccine;
  final CertificateDetails certificateDetails;

  CheckProfileComplete(this.programVaccine, this.certificateDetails);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CheckProfileComplete &&
        other.programVaccine == programVaccine &&
        other.certificateDetails == certificateDetails;
  }

  @override
  int get hashCode => programVaccine.hashCode ^ certificateDetails.hashCode;
}
