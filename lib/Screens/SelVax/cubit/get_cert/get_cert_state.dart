part of 'get_cert_cubit.dart';

@immutable
abstract class GetCertState {
  const GetCertState();
}

class GetCertInitial extends GetCertState {
  const GetCertInitial();
}

class GetCertLoading extends GetCertState {
  const GetCertLoading();
}

class GetCertLoaded extends GetCertState {
  final CertificateDetails certPrimary;
  final CertificateDetails certDependent;
  final String userId;

  GetCertLoaded(this.certPrimary, this.certDependent, this.userId);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetCertLoaded &&
        other.certPrimary == certPrimary &&
        other.certDependent == certDependent &&
        other.userId == userId;
  }

  @override
  int get hashCode =>
      certPrimary.hashCode ^ certDependent.hashCode ^ userId.hashCode;
}
