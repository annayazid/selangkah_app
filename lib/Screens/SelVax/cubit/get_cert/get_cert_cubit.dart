import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/SelVax/selvax.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

part 'get_cert_state.dart';

class GetCertCubit extends Cubit<GetCertState> {
  GetCertCubit() : super(GetCertInitial());

  Future<void> getCert() async {
    emit(GetCertLoading());

    //process

    CertificateDetails certificateDetails =
        await SelangkahVaxRepositories.getCert();

    String id = await SecureStorage().readSecureData('userId');

    CertificateDetails certPrimary = CertificateDetails(data: []);
    CertificateDetails certDependent = CertificateDetails(data: []);

    for (var i = 0; i < certificateDetails.data!.length; i++) {
      if (certificateDetails.data![i].dependent == '0') {
        certPrimary.data!.add(certificateDetails.data![i]);
      } else {
        certDependent.data!.add(certificateDetails.data![i]);
      }
    }

    emit(GetCertLoaded(certPrimary, certDependent, id));
  }
}
