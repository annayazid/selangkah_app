import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/SelVax/selvax.dart';

part 'notice_state.dart';

class NoticeCubit extends Cubit<NoticeState> {
  NoticeCubit() : super(NoticeInitial());

  Future<void> getNotice(String language) async {
    emit(NoticeLoading());

    //process
    String notice = await SelangkahVaxRepositories.getNotice();

    emit(NoticeLoaded(notice));
  }
}
