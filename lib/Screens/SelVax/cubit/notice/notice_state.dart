part of 'notice_cubit.dart';

@immutable
abstract class NoticeState {
  const NoticeState();
}

class NoticeInitial extends NoticeState {
  const NoticeInitial();
}

class NoticeLoading extends NoticeState {
  const NoticeLoading();
}

class NoticeLoaded extends NoticeState {
  final String notice;

  NoticeLoaded(this.notice);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is NoticeLoaded && other.notice == notice;
  }

  @override
  int get hashCode => notice.hashCode;
}
