import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/SelVax/selvax.dart';

class CovidModule extends StatefulWidget {
  final Appointment appointment;

  const CovidModule({super.key, required this.appointment});

  @override
  State<CovidModule> createState() => _CovidModuleState();
}

class _CovidModuleState extends State<CovidModule> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return SelvaxScaffold(
      appBarTitle: 'SelVax',
      content: Column(
        children: [
          Container(
            height: height * 0.2,
            width: double.infinity,
            child: Stack(
              fit: StackFit.expand,
              children: [
                Image.asset(
                  'assets/images/SelVax/vaccine_banner_bg.png',
                  fit: BoxFit.fitHeight,
                ),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'COVID-19 Vaccine'.tr(),
                        style: TextStyle(
                          color: Color(0xFF027A77),
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          _selangkahButton(
            width,
            'assets/images/SelVax/booster_shot.png',
            'book_appointment'.tr(),
            '',
            () async {
              // String id = await SecureStorage().readSecureData('userId');
              // Navigator.of(context).push(
              //   MaterialPageRoute(
              //     builder: (context) => SelvaxWeb(
              //       appBarTitle: 'Selvax',
              //       url: '${widget.vaccineData.url}$id',
              //     ),
              //   ),
              // );

              Navigator.of(context, rootNavigator: true).push(
                MaterialPageRoute(
                  builder: (context) => BlocProvider(
                    create: (context) => NoticeCubit(),
                    child: SelvaxNotice(),
                  ),
                ),
              );
            },
          ),
          SizedBox(height: 10),
          _selangkahButton(
            width,
            'assets/images/SelVax/receipt.png',
            'vaccine_cert'.tr(),
            '',
            () async {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => BlocProvider(
                    create: (context) => GetCovidCertificateCubit(),
                    child: CovidCertificate(),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  Container _selangkahButton(width, icon, title, subtitle, function) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.black.withOpacity(0.1),
              offset: Offset(0.0, 0.0), //(x,y)
              blurRadius: 10.0,
              spreadRadius: 3),
        ],
        borderRadius: BorderRadius.circular(12),
      ),
      width: width * 0.95,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
        ),
        onPressed: function,
        child: Column(
          children: [
            SizedBox(height: 10),
            Image.asset(
              icon,
              width: 60,
              height: 60,
            ),
            SizedBox(height: 5),
            Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 18,
                color: Color(0xff000000),
              ),
            ),
            SizedBox(height: 5),
            Text(
              subtitle,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 14,
                color: Color(0xff686868),
              ),
            ),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
