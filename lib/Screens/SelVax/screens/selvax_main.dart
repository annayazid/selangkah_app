import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:selangkah_new/Screens/SelVax/selvax.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/VerifyProfile/verify_profile.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class SelvaxMain extends StatefulWidget {
  SelvaxMain({Key? key}) : super(key: key);

  @override
  State<SelvaxMain> createState() => _SelvaxMainState();
}

class _SelvaxMainState extends State<SelvaxMain> {
  @override
  void initState() {
    GlobalFunction.screenJourney('11');
    context.read<CheckProfileCubit>().checkProfile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top -
        AppBar().preferredSize.height;

    return SelvaxScaffold(
      appBarTitle: 'SelVax',
      content: Container(
        child: BlocConsumer<CheckProfileCubit, CheckProfileState>(
          listener: (context, state) {
            if (state is CheckProfileIncomplete) {
              Fluttertoast.showToast(
                msg: "updateProfile".tr(),
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
              );

              //redirect edit profile

              Navigator.of(context)
                  .push(
                MaterialPageRoute(
                  builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                        create: (context) => GetUserDetailsCubit(),
                      ),
                      BlocProvider(
                        create: (context) => UpdateDetailCubit(),
                      ),
                    ],
                    child: VerifyProfile(),
                  ),
                ),
              )
                  .then((value) async {
                await Future.delayed(Duration(seconds: 1));
                String? email = await SecureStorage().readSecureData('email');

                print('email is $email');

                if (email == null || email == 'null' || email == '') {
                  Navigator.of(context).pop();
                  // Navigator.of(context).pop();
                } else {
                  context.read<CheckProfileCubit>().checkProfile();
                }
              });
            }
          },
          builder: (context, state) {
            if (state is CheckProfileComplete) {
              return ProfileCompletedWidget(
                programVaccine: state.programVaccine,
                certificateDetails: state.certificateDetails,
              );
            } else {
              return Container(
                height: height,
                child: Center(
                  child: SpinKitFadingCircle(
                    color: kPrimaryColor,
                    size: 30,
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}

class ProfileCompletedWidget extends StatelessWidget {
  final ProgramVaccine programVaccine;
  final CertificateDetails certificateDetails;

  const ProfileCompletedWidget(
      {super.key,
      required this.programVaccine,
      required this.certificateDetails});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Column(
      children: [
        Container(
          height: height * 0.2,
          width: double.infinity,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Image.asset(
                'assets/images/SelVax/vaccine_banner_bg.png',
                fit: BoxFit.fitHeight,
              ),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'selvax_title'.tr(),
                      style: TextStyle(
                        color: Color(0xFF027A77),
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),

        //covid starts here
        // if (appointment.data!.isNotEmpty) ...[
        //   SizedBox(height: 10),
        //   _selangkahButton(
        //     width,
        //     'assets/images/SelVax/booster_shot.png',
        //     'COVID-19 Vaccine'.tr(),
        //     '',
        //     () async {
        //       Navigator.of(context).push(
        //         MaterialPageRoute(
        //           builder: (context) => CovidModule(
        //             appointment: appointment,
        //           ),
        //         ),
        //       );

        //       //sso to web
        //       // String id = await SecureStorage().readSecureData('userId');
        //       // Navigator.of(context).push(
        //       //   MaterialPageRoute(
        //       //     builder: (context) => SelvaxWeb(
        //       //       appBarTitle: 'Selvax',
        //       //       url:
        //       //           'https://vaksin.selangkah.my/vax-2-cert-pdf?id_selangkah_user=152386',
        //       //     ),
        //       //   ),
        //       // );

        //       // Navigator.of(context).push(
        //       //   MaterialPageRoute(
        //       //     builder: (context) => VaccineModule(
        //       //       vaccineData: programVaccine.data![index],
        //       //     ),
        //       //   ),
        //       // );
        //     },
        //   ),
        // ],

        // //covid ends here
        SizedBox(height: 10),
        ListView.builder(
          shrinkWrap: true,
          itemCount: programVaccine.data!.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                _selangkahButton(
                  width,
                  'assets/images/SelVax/booster_shot.png',
                  programVaccine.data![index].name,
                  '',
                  () async {
                    // Navigator.of(context).push(
                    //   MaterialPageRoute(
                    //     builder: (context) => BlocProvider(
                    //       create: (context) => BubbleBoosterCubit(),
                    //       child: VaxIndividuBooster(),
                    //     ),
                    //   ),
                    // );

                    //sso to web
                    // String id = await SecureStorage().readSecureData('userId');
                    // Navigator.of(context).push(
                    //   MaterialPageRoute(
                    //     builder: (context) => SelvaxWeb(
                    //       appBarTitle: 'Selvax',
                    //       url:
                    //           'https://vaksin.selangkah.my/vax-2-cert-pdf?id_selangkah_user=152386',
                    //     ),
                    //   ),
                    // );

                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => VaccineModule(
                          vaccineData: programVaccine.data![index],
                          certDetails: certificateDetails,
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(height: 10),
              ],
            );
          },
        ),

        // SizedBox(height: 10),
        // _selangkahButton(
        //   width,
        //   'assets/images/SelVax/receipt.png',
        //   'List of Appointments',
        //   '',
        //   () {
        //     // Navigator.of(context).push(
        //     //   MaterialPageRoute(
        //     //     builder: (context) => BlocProvider(
        //     //       create: (context) => ReceiptVaccineCubit(),
        //     //       child: ReceiptVaccineScreen(),
        //     //     ),
        //     //   ),
        //     // );

        //     Navigator.of(context).push(
        //       MaterialPageRoute(
        //         builder: (context) => BlocProvider(
        //           create: (context) => AppointmentVaxCubit(),
        //           child: AppointmentVaxScreen(),
        //         ),
        //       ),
        //     );
        //   },
        // ),
      ],
    );
  }

  Container _selangkahButton(width, icon, title, subtitle, function) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.black.withOpacity(0.1),
              offset: Offset(0.0, 0.0), //(x,y)
              blurRadius: 10.0,
              spreadRadius: 3),
        ],
        borderRadius: BorderRadius.circular(12),
      ),
      width: width * 0.95,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
        ),
        onPressed: function,
        child: Column(
          children: [
            SizedBox(height: 10),
            Image.asset(
              icon,
              width: 60,
              height: 60,
            ),
            SizedBox(height: 5),
            Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 18,
                color: Color(0xff000000),
              ),
            ),
            SizedBox(height: 5),
            Text(
              subtitle,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 14,
                color: Color(0xff686868),
              ),
            ),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
