import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/SelVax/selvax.dart';

class AppointmentVaxScreen extends StatefulWidget {
  const AppointmentVaxScreen({super.key});

  @override
  State<AppointmentVaxScreen> createState() => _AppointmentVaxScreenState();
}

class _AppointmentVaxScreenState extends State<AppointmentVaxScreen> {
  @override
  void initState() {
    context.read<AppointmentVaxCubit>().getAppointmentVax();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    // double width = MediaQuery.of(context).size.width;
    return SelvaxScaffold(
      appBarTitle: 'SelVax',
      content: Column(
        children: [
          Container(
            height: height * 0.2,
            width: double.infinity,
            child: Stack(
              fit: StackFit.expand,
              children: [
                Image.asset(
                  'assets/images/SelVax/vaccine_banner_bg.png',
                  fit: BoxFit.fitHeight,
                ),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'List of Appointments',
                        style: TextStyle(
                          color: Color(0xFF027A77),
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }
}
