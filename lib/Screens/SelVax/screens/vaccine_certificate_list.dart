import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:selangkah_new/Screens/SelVax/selvax.dart';
import 'package:selangkah_new/utils/constants.dart';

class VaccineCertificateList extends StatefulWidget {
  final String vaccineName;

  const VaccineCertificateList({super.key, required this.vaccineName});

  @override
  State<VaccineCertificateList> createState() => _VaccineCertificateListState();
}

class _VaccineCertificateListState extends State<VaccineCertificateList> {
  @override
  void initState() {
    context.read<GetCertCubit>().getCert();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print('name is ${widget.vaccineName}');
    double height = MediaQuery.of(context).size.height;

    return SelvaxScaffold(
      appBarTitle: 'SelVax',
      content: Column(
        children: [
          Container(
            height: height * 0.2,
            width: double.infinity,
            child: Stack(
              fit: StackFit.expand,
              children: [
                Image.asset(
                  'assets/images/SelVax/vaccine_banner_bg.png',
                  fit: BoxFit.fitHeight,
                ),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'vaccine_cert'.tr(),
                        style: TextStyle(
                          color: Color(0xFF027A77),
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          BlocBuilder<GetCertCubit, GetCertState>(
            builder: (context, state) {
              if (state is GetCertLoaded) {
                return CertLoadedWidget(
                  certPrimary: state.certPrimary,
                  certDependent: state.certDependent,
                  userId: state.userId,
                  vaccineName: widget.vaccineName,
                );
              } else {
                return Container(
                  height: height,
                  child: Center(
                    child: SpinKitFadingCircle(
                      color: kPrimaryColor,
                      size: 30,
                    ),
                  ),
                );
              }
            },
          )
        ],
      ),
    );
  }
}

class CertLoadedWidget extends StatelessWidget {
  final CertificateDetails certPrimary;
  final CertificateDetails certDependent;
  final String userId;
  final String vaccineName;

  const CertLoadedWidget(
      {super.key,
      required this.certPrimary,
      required this.certDependent,
      required this.userId,
      required this.vaccineName});

  @override
  Widget build(BuildContext context) {
    return certPrimary.data!
                .any((element) => element.programName == vaccineName) ||
            certDependent.data!
                .any((element) => element.programName == vaccineName)
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (certPrimary.data!.isNotEmpty) ...[
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Text(
                    'selvax_primary_cert'.tr(),
                    style: TextStyle(color: Colors.black54),
                  ),
                ),
                SizedBox(height: 5),
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: certPrimary.data!.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (certPrimary.data![index].programName == vaccineName) {
                      return Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              offset: Offset(0.0, 0.0), //(x,y)
                              blurRadius: 10.0,
                              spreadRadius: 3,
                            ),
                          ],
                          borderRadius: BorderRadius.circular(10),
                        ),
                        width: double.infinity,
                        margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        certPrimary.data![index].accName!,
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(height: 5),
                                      Text(certPrimary.data![index].icNo!),
                                    ],
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (context) => SelvaxWeb(
                                          appBarTitle: 'Selvax',
                                          url:
                                              'https://vaksin.selangkah.my/vax-2-cert-pdf?id_selangkah_user=${certPrimary.data![index].id}',
                                        ),
                                      ),
                                    );
                                  },
                                  child: Text(
                                    'view_certificate'.tr(),
                                    style: TextStyle(
                                      color: Colors.blue,
                                      decoration: TextDecoration.underline,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    } else {
                      return Container();
                    }
                  },
                ),
              ],
              SizedBox(height: 10),
              if (certDependent.data!.isNotEmpty) ...[
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Text(
                    'selvax_dependent_cert'.tr(),
                    style: TextStyle(color: Colors.black54),
                  ),
                ),
                SizedBox(height: 5),
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: certDependent.data!.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (certDependent.data![index].programName == vaccineName) {
                      return Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              offset: Offset(0.0, 0.0), //(x,y)
                              blurRadius: 10.0,
                              spreadRadius: 3,
                            ),
                          ],
                          borderRadius: BorderRadius.circular(10),
                        ),
                        width: double.infinity,
                        margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        certDependent.data![index].accName!,
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(height: 5),
                                      Text(certDependent.data![index].icNo!),
                                    ],
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (context) => SelvaxWeb(
                                          appBarTitle: 'Selvax',
                                          url:
                                              'https://vaksin.selangkah.my/vax-2-cert-pdf?id_selangkah_user=${certDependent.data![index].id}',
                                        ),
                                      ),
                                    );
                                  },
                                  child: Text(
                                    'view_certificate'.tr(),
                                    style: TextStyle(
                                      color: Colors.blue,
                                      decoration: TextDecoration.underline,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    } else {
                      return Container();
                    }
                  },
                ),
              ],
            ],
          )
        : Text(
            'no_data_cert'.tr(),
            style: TextStyle(
              color: Colors.black,
            ),
          );
  }
}
