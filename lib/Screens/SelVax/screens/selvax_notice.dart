import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/SelVax/selvax.dart';

class SelvaxNotice extends StatefulWidget {
  const SelvaxNotice({Key? key}) : super(key: key);

  @override
  _SelvaxNoticeState createState() => _SelvaxNoticeState();
}

class _SelvaxNoticeState extends State<SelvaxNotice> {
  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height -
    //     AppBar().preferredSize.height -
    //     MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;

    String language =
        EasyLocalization.of(context)!.locale.languageCode == 'en' ? 'EN' : 'MY';

    context.read<NoticeCubit>().getNotice(language);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/images/selangkah_logo.png",
          width: width * 0.45,
        ),
      ),
      body: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: true,
            title: Text(
              'selvax_title'.tr(),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 15.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontFamily: "Roboto",
              ),
            ),
            centerTitle: true,
          ),
          body: BlocBuilder<NoticeCubit, NoticeState>(
            builder: (context, state) {
              if (state is NoticeLoaded) {
                return NoticeLoadedWidget(notice: state.notice);
              } else {
                return Container();
              }
            },
          )),
    );
  }
}

class NoticeLoadedWidget extends StatelessWidget {
  final String notice;

  const NoticeLoadedWidget({Key? key, required this.notice}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height -
    //     AppBar().preferredSize.height -
    //     MediaQuery.of(context).padding.top;
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            width: double.infinity,
            child: Stack(
              alignment: Alignment.center,
              children: [
                Image.asset(
                  'assets/images/SelVax/vaccine_banner_bg.png',
                  fit: BoxFit.fitHeight,
                ),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'selvax_title'.tr(),
                        style: TextStyle(
                          color: Color(0xFF027A77),
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        'selvax_subtitle'.tr(),
                        style: TextStyle(
                          color: Color(0xFF027A77),
                          fontSize: 15,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          Container(
            padding: EdgeInsets.all(15),
            child: Text(
              notice,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
