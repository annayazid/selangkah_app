import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:selangkah_new/Screens/SelVax/selvax.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/endpoint.dart';

class CovidCertificate extends StatefulWidget {
  CovidCertificate({Key? key}) : super(key: key);

  @override
  State<CovidCertificate> createState() => _CovidCertificateState();
}

class _CovidCertificateState extends State<CovidCertificate> {
  @override
  void initState() {
    context.read<GetCovidCertificateCubit>().getCovidCert();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return SelvaxScaffold(
      appBarTitle: 'SelVax',
      content: Column(
        children: [
          Container(
            height: height * 0.2,
            width: double.infinity,
            child: Stack(
              fit: StackFit.expand,
              children: [
                Image.asset(
                  'assets/images/SelVax/vaccine_banner_bg.png',
                  fit: BoxFit.fitHeight,
                ),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'vaccine_cert'.tr(),
                        style: TextStyle(
                          color: Color(0xFF027A77),
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          BlocBuilder<GetCovidCertificateCubit, GetCovidCertificateState>(
            builder: (context, state) {
              if (state is GetCovidCertificateLoaded) {
                return CertCovidLoadedWidget(
                  appointment: state.appointment,
                );
              } else {
                return Column(
                  children: [
                    SizedBox(height: 50),
                    SpinKitFadingCircle(
                      color: kPrimaryColor,
                      size: 30,
                    ),
                  ],
                );
              }
            },
          )
        ],
      ),
    );
  }
}

class CertCovidLoadedWidget extends StatelessWidget {
  final Appointment appointment;

  const CertCovidLoadedWidget({super.key, required this.appointment});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: appointment.data!.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                offset: Offset(0.0, 0.0), //(x,y)
                blurRadius: 10.0,
                spreadRadius: 3,
              ),
            ],
            borderRadius: BorderRadius.circular(10),
          ),
          width: double.infinity,
          margin: EdgeInsets.all(10),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      appointment.data![index].accName!,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 5),
                    Text(appointment.data![index].icNo!),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  String idNo = appointment.data![index].icNo!;
                  String phone = appointment.data![index].phoneNo!;
                  String url =
                      '$SELANGKAH_VAX/vax-cert-pdf?id_no=$idNo&phone_no=$phone';
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => SelvaxWeb(
                        appBarTitle: 'Selvax',
                        url: url,
                      ),
                    ),
                  );
                },
                child: Text(
                  'view_certificate'.tr(),
                  style: TextStyle(
                    color: Colors.blue,
                    decoration: TextDecoration.underline,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
