import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/SelVax/selvax.dart';
import 'package:selangkah_new/utils/AppConstant.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class SelangkahVaxRepositories {
  static Future<String> getNotice() async {
    final Map<String, String> map = {
      'token': TOKEN,
      'language': '',
    };

    final response = await http.post(
      Uri.parse('$API_URL_VAX/get_disable_notes'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    var data = jsonDecode(response.body);

    String notice = data['Data'];

    print(notice);

    return notice;
  }

  static Future<AppointmentVax> getAppointmentVax() async {
    final Map<String, String> map = {
      'token': TOKEN,
      'language': '',
    };

    final response = await http.post(
      Uri.parse('$SARING_DATA/get_app_vax'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    AppointmentVax appointmentVax = appointmentVaxFromJson(response.body);

    return appointmentVax;
  }

  static Future<ProgramVaccine> getProgram() async {
    final Map<String, String> map = {
      'token': TOKEN,
    };

    final response = await http.post(
      Uri.parse('$SARING_DATA/get_program_vaccine'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
    ProgramVaccine programVaccine = programVaccineFromJson(response.body);
    return programVaccine;
  }

  static Future<CertificateDetails> getCert() async {
    String id = await SecureStorage().readSecureData('userId');
    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      // 'id_selangkah_user': "1784326"
    };

    final response = await http.post(
      // Uri.parse('$SARING_DATA/get_app_vax'),
      Uri.parse('$SARING_DATA/get_app_vax'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
    if (!response.body.contains('Empty') && !response.body.contains('empty')) {
      CertificateDetails certificateDetails =
          certificateDetailsFromJson(response.body);
      return certificateDetails;
    } else {
      CertificateDetails certificateDetails = CertificateDetails(data: []);
      return certificateDetails;
    }
  }

  static Future<Appointment> getAppointment() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userSelId');

    //call remedi token
    Map jsonBody = {
      "grant_type": "client_credentials",
      "client_id": secret_id,
      "client_secret": secret_key,
      "scope": "*"
    };

    //print('calling token');
    final response = await http.post(
      Uri.parse('$REMEDI/oauth/token'),
      headers: {"Content-Type": "application/json"},
      body: jsonEncode(jsonBody),
    );

    var data = jsonDecode(response.body);
    String accessToken = data['access_token'];

    //calling appointment
    //print('calling post get appointment');
    final response2 = await http.get(
      Uri.parse('$REMEDI/api/selangkah/selvax/status/$id/withdependent'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': accessToken,
      },
      // body: map,
    );

    print('calling remedi');
    print(response2.body);

    if (response2.body.contains("fail")) {
      return Appointment();
    }

    Appointment appointment = appointmentFromJson(response2.body);

    return appointment;
  }
}
