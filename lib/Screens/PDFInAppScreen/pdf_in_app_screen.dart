import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:selangkah_new/Screens/SelangkahAppBarScaffold/scaffold.dart';
import 'package:selangkah_new/utils/constants.dart';

class PDFInAppPageScreen extends StatefulWidget {
  final String? url;
  final String? title;

  const PDFInAppPageScreen({super.key, required this.url, required this.title});

  @override
  State<PDFInAppPageScreen> createState() => _PDFInAppPageScreenState();
}

class _PDFInAppPageScreenState extends State<PDFInAppPageScreen> {
  int totalPages = 0;
  int currentPage = 0;
  bool pdfReady = true;
  late PDFViewController pdfViewController;
  @override
  Widget build(BuildContext context) {
    print('Privacy Url');
    print(widget.url);
    return TopBarScaffold(
      title: widget.title!,
      content: Stack(
        children: <Widget>[
          PDFView(
            filePath: widget.url,
            autoSpacing: true,
            enableSwipe: true,
            pageSnap: true,
            swipeHorizontal: true,
            nightMode: false,
            onError: (e) {
              print(e);
            },
            onRender: (pages) {
              setState(() {
                totalPages = pages ?? 0;
                pdfReady = true;
              });
            },
            onViewCreated: (PDFViewController vc) {
              pdfViewController = vc;
            },
            // onPageChanged: (int page, int total) {
            //   setState(() {});
            // },
            onPageError: (page, e) {},
          ),
          !pdfReady
              ? Center(
                  // child: CircularProgressIndicator(),
                  child: SpinKitFadingCircle(
                    color: kPrimaryColor,
                    size: 30,
                  ),
                )
              : Offstage(),
          Positioned(
            bottom: 20,
            right: 20,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                currentPage > 0
                    ?
                    // FloatingActionButton.extended(
                    //     backgroundColor: Colors.red,
                    //     label: Text("Go to ${_currentPage - 1}"),
                    //     onPressed: () {
                    //       _currentPage -= 1;
                    //       _pdfViewController.setPage(_currentPage);
                    //     },
                    //   )
                    GestureDetector(
                        onTap: () {
                          currentPage = currentPage - 1;
                          pdfViewController.setPage(currentPage);
                        },
                        child: Container(
                          child: Icon(
                            FontAwesomeIcons.chevronLeft,
                          ),
                        ),
                      )
                    // Container(
                    //     padding: EdgeInsets.all(5),
                    //     decoration: BoxDecoration(
                    //       borderRadius: BorderRadius.circular(5),
                    //       color: Colors.white,
                    //       boxShadow: [
                    //         BoxShadow(
                    //           color: Colors.grey,
                    //           offset: Offset(0.0, 1.0), //(x,y)
                    //           blurRadius: 4.0,
                    //         ),
                    //       ],
                    //     ),
                    //     child: Icon(FontAwesomeIcons.chevronLeft),
                    //   )
                    : Offstage(),
                SizedBox(width: 10),
                currentPage + 1 < totalPages
                    ?
                    // FloatingActionButton.extended(
                    //     backgroundColor: Colors.green,
                    //     label: Text("Go to ${_currentPage + 1}"),
                    //     onPressed: () {
                    //       _currentPage += 1;
                    //       _pdfViewController.setPage(_currentPage);
                    //     },
                    //   )
                    GestureDetector(
                        onTap: () {
                          currentPage += 1;
                          pdfViewController.setPage(currentPage);
                        },
                        child: Container(
                          child: Icon(
                            FontAwesomeIcons.chevronRight,
                          ),
                        ),
                      )
                    : Offstage(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
