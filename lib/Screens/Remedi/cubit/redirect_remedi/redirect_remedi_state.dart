part of 'redirect_remedi_cubit.dart';

@immutable
abstract class RedirectRemediState {
  const RedirectRemediState();
}

class RedirectRemediInitial extends RedirectRemediState {
  const RedirectRemediInitial();
}

class RedirectRemediLoading extends RedirectRemediState {
  const RedirectRemediLoading();
}

class RedirectRemediNotRegistered extends RedirectRemediState {
  const RedirectRemediNotRegistered();
}

class RedirectRemediNotComplete extends RedirectRemediState {
  const RedirectRemediNotComplete();
}

class RedirectRemediRegistered extends RedirectRemediState {
  final String url;
  final String accessToken;

  RedirectRemediRegistered(this.url, this.accessToken);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RedirectRemediRegistered && other.url == url;
  }

  @override
  int get hashCode => url.hashCode;
}
