import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Remedi/remedi.dart';
import 'package:selangkah_new/utils/endpoint.dart';

part 'redirect_remedi_state.dart';

class RedirectRemediCubit extends Cubit<RedirectRemediState> {
  RedirectRemediCubit() : super(RedirectRemediInitial());

  Future<void> redirectRemedi(String redirect) async {
    emit(RedirectRemediLoading());

    //process

    RawProfile profile = await RemediRepositories.getProfile();

    String? name = profile.data?[2];
    String? idCard = profile.data?[4];
    String? idType = profile.data?[17];
    String? citizenship = profile.data?[6];
    String? gender = profile.data?[7];
    String? dob = profile.data?[8];
    String? email = profile.data?[15];
    String? phone = profile.data?[19];
    String? address1 = profile.data?[9];
    String? postcode = profile.data?[11];
    String? city = profile.data?[12];
    String? state = profile.data?[13];
    String? country = profile.data?[14];
    String? userToken = profile.data?[3];
    String? race = profile.data![16]!;
    String phoneNum = profile.data![19]!;

    String selId = profile.data![1]!;

    if (name == null ||
        idCard == null ||
        idType == null ||
        citizenship == null ||
        gender == null ||
        dob == null ||
        email == null ||
        phone == null ||
        address1 == null ||
        postcode == null ||
        city == null ||
        state == null ||
        country == null) {
      print('incomplete');
      // yield ProfileIncomplete();
      emit(RedirectRemediNotComplete());
    } else {
      print('complete');

      if (userToken == null) {
        //register here

        String jsonPost =
            '{"selangkah_id":"$selId","redirect": "$redirect","user_info":{"first_name":"$name","last_name":"","id_no":"$idCard","id_type":"$idType","citizenship":"$citizenship","gender":"$gender","dob":"$dob","email":"$email","race":"$race"},"user_telephone":{"tel_mobile":"6$phoneNum"},"user_address":{"user_addr1":"$address1","user_postcode":"$postcode","user_city":"$city","user_state":"$state","user_country":"$country"}}';

        await RemediRepositories.registerRemedi(jsonPost);

        //process register
      } else {
        //redirect

        String url =
            '$REMEDI/api/selangkah/login?selangkah_id=$selId&remedi_user_token=$userToken&redirect=$redirect';

        print('url is $url');

        String accessToken = await RemediRepositories.getAccessToken();

        log('access token $accessToken');

        // emit(RedirectRemediRegistered(url, accessToken));
      }
    }
  }
}
