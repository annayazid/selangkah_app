import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:selangkah_new/Screens/Remedi/remedi.dart';

class RemediScreen extends StatefulWidget {
  final String redirect;

  const RemediScreen({super.key, required this.redirect});

  @override
  State<RemediScreen> createState() => _RemediScreenState();
}

class _RemediScreenState extends State<RemediScreen> {
  @override
  void initState() {
    context.read<RedirectRemediCubit>().redirectRemedi('SELMENU');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: BlocConsumer<RedirectRemediCubit, RedirectRemediState>(
        listener: (context, state) {},
        builder: (context, state) {
          print('state is $state');
          if (state is RedirectRemediRegistered) {
            return RemediWebview(
              url: state.url,
              accessToken: state.accessToken,
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}

class RemediWebview extends StatefulWidget {
  final String url;
  final String accessToken;

  const RemediWebview(
      {super.key, required this.url, required this.accessToken});

  @override
  State<RemediWebview> createState() => _RemediWebviewState();
}

class _RemediWebviewState extends State<RemediWebview> {
  @override
  Widget build(BuildContext context) {
    return InAppWebView(
      initialUrlRequest: URLRequest(
        headers: {'Authorization': widget.accessToken},
        url: Uri.parse(widget.url),
      ),
    );
  }
}
