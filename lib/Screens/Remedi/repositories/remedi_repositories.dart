import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/utils/AppConstant.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class RemediRepositories {
  static Future<String> getAccessToken() async {
    Map jsonBody = {
      "grant_type": "client_credentials",
      "client_id": secret_id,
      "client_secret": secret_key,
      "scope": "*"
    };

    final response = await http.post(
      Uri.parse('$REMEDI/oauth/token'),
      headers: {"Content-Type": "application/json"},
      body: jsonEncode(jsonBody),
    );

    var data = jsonDecode(response.body);
    String accessToken = data['access_token'];
    return accessToken;
  }

  static Future<RawProfile> getProfile() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id': id,
      'token': TOKEN,
    };

    //print('calling post GET_PROFILE');

    final response = await http.post(
      Uri.parse('$API_URL/check_user_profile'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    RawProfile? profile = rawProfileFromJson(response.body);
    return profile!;
  }

  static Future<void> registerRemedi(String jsonData) async {
    String accessToken = await getAccessToken();
    //print('calling post GET_PROFILE');

    await http.post(
      Uri.parse("$REMEDI/api/selangkah/register"),
      headers: {
        'Authorization': accessToken,
        'Content-Type': 'application/json'
      },
      body: jsonData,
    );
  }
}
