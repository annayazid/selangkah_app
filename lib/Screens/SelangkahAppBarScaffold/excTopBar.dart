import 'package:flutter/material.dart';
import 'package:selangkah_new/utils/constants.dart';

class SelangkahScaffold extends StatelessWidget {
  final Widget content;

  const SelangkahScaffold({super.key, required this.content});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Image.asset(
            "assets/images/Scaffold/selangkah_logo.png",
            width: width * 0.45,
          ),
        ),
        body: content,
      ),
    );
  }
}
