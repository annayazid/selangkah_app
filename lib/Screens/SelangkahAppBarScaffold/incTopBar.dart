import 'package:flutter/material.dart';
import 'package:selangkah_new/utils/constants.dart';

class SelangkahBarScaffold extends StatelessWidget {
  final String title;
  final Widget content;

  const SelangkahBarScaffold(
      {super.key, required this.title, required this.content});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/images/Scaffold/selangkah_logo.png",
          width: size.width * 0.45,
        ),
      ),
      body: Scaffold(
        appBar: AppBar(
          backgroundColor: kPrimaryColor,
          centerTitle: true,
          // automaticallyImplyLeading: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          title: Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 15,
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: 'Roboto',
            ),
          ),
        ),
        body: content,
      ),
    );
  }
}
