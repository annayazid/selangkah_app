import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/ResetPassword/reset_pass.dart';
import 'package:selangkah_new/Screens/SelangkahAppBarScaffold/scaffold.dart';
import 'package:selangkah_new/Screens/SignIn/sign_in.dart';
import 'package:selangkah_new/Screens/VerifyOTP/verifyOTP.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';

class VerifyOTP extends StatefulWidget {
  final String fromScreen;
  final String? username;
  final String phone;
  final String? password;
  final bool? hardReset;

  const VerifyOTP(
      {super.key,
      required this.fromScreen,
      this.username,
      required this.phone,
      this.password,
      this.hardReset});

  @override
  State<VerifyOTP> createState() => _VerifyOTPState();
}

class _VerifyOTPState extends State<VerifyOTP> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _otpTextController = TextEditingController();

  @override
  void initState() {
    GlobalFunction.screenJourney('3');
    context.read<OtpVerifyCubit>().requestOTP(widget.phone);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(context.deviceLocale.languageCode);
    // Size size = MediaQuery.of(context).size;
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top -
        MediaQuery.of(context).padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return TopBarScaffold(
        title: 'verifyotp'.tr(),
        content: SingleChildScrollView(
          child: Center(
            child: Container(
              width: width * 0.9,
              padding: EdgeInsets.symmetric(vertical: 25),
              height: height,
              child: Form(
                key: _formKey,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'verifyotp'.tr(),
                      style:
                          TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 15),
                    Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child: Container(
                            // margin: EdgeInsets.symmetric(vertical: 10),
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(width: 1, color: Colors.grey),
                            ),
                            child: TextFormField(
                              controller: _otpTextController,
                              keyboardType: TextInputType.number,
                              validator: verifyOTPValidator,
                              cursorColor: kPrimaryColor,
                              decoration: InputDecoration(
                                icon: Icon(
                                  Icons.vpn_key,
                                  color: kPrimaryColor,
                                ),
                                hintText: 'otp'.tr().toString(),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 10),
                        BlocBuilder<OtpVerifyCubit, OtpVerifyState>(
                          builder: (context, state) {
                            print(state);
                            return ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                backgroundColor: state is OtpVerifyWait
                                    ? Colors.grey
                                    : kPrimaryColor,
                              ),
                              onPressed: () async {
                                if (state is OtpVerifyInitial ||
                                    state is OtpInitialAfterFirst) {
                                  await context
                                      .read<OtpVerifyCubit>()
                                      .requestOTP(widget.phone);
                                } else {
                                  Fluttertoast.showToast(
                                      msg: "liveness_wait".tr());
                                }
                              },
                              child: state is OtpVerifyLoading
                                  ? SpinKitFadingCircle(
                                      color: Colors.white,
                                      size: 18,
                                    )
                                  : Center(
                                      child: Text(
                                        'request_otp'.tr(),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 13,
                                        ),
                                      ),
                                    ),
                            );
                          },
                        ),
                      ],
                    ),
                    // SizedBox(height: 15),
                    BlocBuilder<OtpVerifyCubit, OtpVerifyState>(
                      builder: (context, state) {
                        print(state);
                        if (state is OtpVerifyWait) {
                          return Container(
                            padding: EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 10),
                                Text(
                                  '${'otp_successful'.tr()} ${DateFormat("d MMMM yyyy, HH:mm").format(DateTime.now())}.',
                                ),
                                SizedBox(height: 10),
                                // Text('${'otp_sent_number'.tr()} $phoneMask.'),

                                Text(
                                    '${'otp_sent_number'.tr()} ${widget.phone}.'),
                                SizedBox(height: 10),

                                Text(
                                    '${'please_wait_otp'.tr()} (${state.duration.inMinutes}:${state.duration.inSeconds.remainder(60)})')
                              ],
                            ),
                          );
                        }
                        if (state is OtpInitialAfterFirst) {
                          return Container(
                            padding: EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 10),
                                Text(
                                  'OTP expired. Please request for another one',
                                  style: TextStyle(color: Colors.red),
                                ),
                                SizedBox(height: 5),
                              ],
                            ),
                          );
                        } else {
                          return Container();
                        }
                      },
                    ),
                    Spacer(),
                    Container(
                      width: width * 0.9,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          backgroundColor: kPrimaryColor,
                        ),
                        onPressed: () {
                          final isValid = _formKey.currentState!.validate();
                          if (!isValid) {
                            print(_otpTextController);

                            return;
                          } else {
                            _formKey.currentState!.save();
                            context.read<OtpVerifyCubit>().verifyOTP(
                                  language: context.deviceLocale.languageCode,
                                  from: widget.fromScreen,
                                  phoneNum: widget.phone,
                                  otp: _otpTextController.text,
                                  username: widget.username,
                                  pass: widget.password,
                                  hardReset: widget.hardReset,
                                );
                          }
                        },
                        child: BlocConsumer<OtpVerifyCubit, OtpVerifyState>(
                          listener: (context, state) async {
                            GlobalVariables.language =
                                EasyLocalization.of(context)!
                                            .locale
                                            .languageCode ==
                                        'en'
                                    ? 'EN'
                                    : 'MY';

                            print(state);
                            if (state is OtpVerifyLoaded) {
                              BlocProvider(
                                create: (context) => GetHomepageDataCubit(),
                                child: HomePageScreen(),
                              ).launch(
                                context,
                                isNewTask: true,
                              );

                              // await Future.delayed(Duration(seconds: 2));
                              // HomePageScreen().launch(context, isNewTask: true);
                            }
                            if (state is OtpVerifyReset) {
                              Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                  builder: (context) => BlocProvider(
                                    create: (context) => ResetPassCubit(),
                                    child: ResetPasswordPage(
                                        phoneNum: widget.phone),
                                  ),
                                ),
                              );
                            }

                            if (state is ChangePassLoaded) {
                              MultiBlocProvider(
                                providers: [
                                  BlocProvider(
                                    create: (context) => PrivacyTermCubit(),
                                  ),
                                  BlocProvider(
                                    create: (context) => CheckUserCubit(),
                                  ),
                                ],
                                child: SignInScreen(),
                              ).launch(context, isNewTask: true);
                            }

                            if (state is ChangePassFailed) {
                              Navigator.of(context).pop();
                            }
                          },
                          builder: (context, state) {
                            if (state is OtpVerifyLoading) {
                              return Padding(
                                padding: EdgeInsets.all(18.0),
                                child: SpinKitFadingCircle(
                                  color: Colors.white,
                                  size: 18,
                                ),
                              );
                            } else {
                              return Container(
                                child: Center(
                                  child: Padding(
                                    padding: EdgeInsets.all(18.0),
                                    child: Text(
                                      'next'.tr(),
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16),
                                      // textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              );
                            }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  String? verifyOTPValidator(String? value) {
    print(value);
    if (value!.length == 0) {
      return 'otprequired'.tr();
    } else if (value.length != 6) {
      return 'invalidotp'.tr();
    }
    return null;
  }
}
