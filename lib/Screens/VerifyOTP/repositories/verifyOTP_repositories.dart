import 'package:selangkah_new/utils/endpoint.dart';
import 'package:http/http.dart' as http;

class VerifyOtpNewRepositories {
  static Future<void> getOtp(String phoneNumber) async {
    final Map<String, String> map = {
      // 'dataset': 'request_otp_app',
      'phonenumber': phoneNumber,
      'token': TOKEN
    };

    //print('calling post REQUEST_OTP');

    final response = await http.post(
      Uri.parse('$API_URL/request_otp_app'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }

  static Future<String> verifyOtp(String phoneNumber, String otp) async {
    final Map<String, String> map = {
      'phonenumber': phoneNumber,
      'otp': otp,
      'token': TOKEN,
    };

    //print('calling post VERIFY_OTP');

    final response = await http.post(
      Uri.parse('$API_URL/verify_otp'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    return response.body;
  }

  static Future<String> signupNewUser(String language, String username,
      String phoneNumber, String password, String source) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'name': username,
      'phone': phoneNumber,
      'pwd': password,
      'ctzen': 'MY',
      'source': source,
      'language': language,
    };

    final response = await http.post(
      Uri.parse('$API_URL/signup_new_user'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
    return response.body;
  }
}
