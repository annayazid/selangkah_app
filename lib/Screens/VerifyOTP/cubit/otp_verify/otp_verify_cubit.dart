import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:meta/meta.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/ForgotPassword/forgot_pass.dart';
import 'package:selangkah_new/Screens/ResetPassword/reset_pass.dart';
import 'package:selangkah_new/Screens/SignIn/sign_in.dart';
import 'package:selangkah_new/Screens/VerifyOTP/verifyOTP.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

part 'otp_verify_state.dart';

class OtpVerifyCubit extends Cubit<OtpVerifyState> {
  OtpVerifyCubit() : super(OtpVerifyInitial());

  Timer? _timer;

  Future<void> requestOTP(String phoneNum) async {
    emit(OtpVerifyLoading());

    await VerifyOtpNewRepositories.getOtp(phoneNum);

    DateTime dateTimeNow = DateTime.now();
    DateTime dateTime1min = DateTime.now().add(Duration(minutes: 1));

    int _counter = 60;

    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (_counter > 0) {
        dateTime1min = dateTime1min.subtract(Duration(seconds: 1));
        _counter--;
        Duration duration = dateTime1min.difference(dateTimeNow);
        emit(OtpVerifyWait(duration));
      } else {
        _timer!.cancel();
        emit(OtpInitialAfterFirst());
      }
    });
  }

  Future<void> verifyOTP(
      {required String language,
      required String from,
      required String phoneNum,
      required String otp,
      String? username,
      String? pass,
      bool? hardReset}) async {
    _timer!.cancel();
    emit(OtpVerifySending());

    var data = await VerifyOtpNewRepositories.verifyOtp(phoneNum, otp);
    if (data.contains('true')) {
      if (from == 'register') {
        String source = '';
        if (Platform.isAndroid) {
          source = 'android';
        } else if (Platform.isIOS) {
          source = 'ios';
        }

        String userSignUp = await VerifyOtpNewRepositories.signupNewUser(
            language, username!, phoneNum, pass!, source);

        if (userSignUp.contains('false')) {
          Fluttertoast.showToast(msg: 'userRegistered'.tr());
        } else if (userSignUp.contains('500')) {
          Fluttertoast.showToast(msg: 'registerFailed'.tr());
        } else {
          String id = await SignInRepositories.checkUserLogin(phoneNum, pass);
          SecureStorage secureStorage = new SecureStorage();
          await secureStorage.writeSecureData('userId', id);
          await AppSplashRepositories.updateUserToken();
          await AppSplashRepositories.getUserProfile();
          emit(OtpVerifyLoaded());
        }
      } else if (from == 'forgotpass') {
        await ForgotRepositories.resetCount(phoneNum);
        emit(OtpVerifyReset());
      } else {
        //change password
        bool resetpass = await ResetAccRepositories.resetCount(phoneNum, pass!);
        if (resetpass) {
          await Future.delayed(Duration(seconds: 2));
          await AppSplashRepositories.getUserProfile();
          Fluttertoast.showToast(msg: 'succes_reset_pass_login'.tr());
          emit(ChangePassLoaded());
        } else {
          await Future.delayed(Duration(seconds: 2));
          Fluttertoast.showToast(msg: 'passwordfail'.tr());
          emit(ChangePassFailed());
        }

        // if (hardReset!) {
        //   await ForgotRepositories.resetCount(phoneNum);
        //   Fluttertoast.showToast(
        //     msg: 'succes_reset_pass_login'.tr(),
        //     toastLength: Toast.LENGTH_LONG,
        //     gravity: ToastGravity.BOTTOM,
        //   );
        // } else {
        //   Fluttertoast.showToast(
        //     msg: 'success'.tr(),
        //     toastLength: Toast.LENGTH_LONG,
        //     gravity: ToastGravity.CENTER,
        //   );
        // }
      }
    } else {
      Fluttertoast.showToast(msg: 'invalidotp'.tr());
    }
  }
}
