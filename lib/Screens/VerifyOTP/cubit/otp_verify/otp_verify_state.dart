part of 'otp_verify_cubit.dart';

@immutable
abstract class OtpVerifyState {
  const OtpVerifyState();
}

class OtpVerifyInitial extends OtpVerifyState {
  const OtpVerifyInitial();
}

class OtpVerifyLoading extends OtpVerifyState {
  const OtpVerifyLoading();
}

class OtpInitialAfterFirst extends OtpVerifyState {
  const OtpInitialAfterFirst();
}

class OtpVerifyWait extends OtpVerifyState {
  final Duration duration;

  OtpVerifyWait(this.duration);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is OtpVerifyWait && other.duration == duration;
  }

  @override
  int get hashCode => duration.hashCode;
}

class OtpVerifySending extends OtpVerifyState {
  const OtpVerifySending();
}

class OtpVerifyLoaded extends OtpVerifyState {
  const OtpVerifyLoaded();
}

class OtpVerifyReset extends OtpVerifyState {
  const OtpVerifyReset();
}

class ChangePassLoaded extends OtpVerifyState {
  const ChangePassLoaded();
}

class ChangePassFailed extends OtpVerifyState {
  const ChangePassFailed();
}
