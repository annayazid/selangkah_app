import 'dart:convert';

import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/utils/AppConstant.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';
import 'package:http/http.dart' as http;

class ProfilePageRepositories {
  static Future<Appointment> getAppointment() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userSelId');

    //call remedi token
    Map jsonBody = {
      "grant_type": "client_credentials",
      "client_id": secret_id,
      "client_secret": secret_key,
      "scope": "*"
    };

    //print('calling token');
    final response = await http.post(
      Uri.parse('$REMEDI/oauth/token'),
      headers: {"Content-Type": "application/json"},
      body: jsonEncode(jsonBody),
    );

    var data = jsonDecode(response.body);
    String accessToken = data['access_token'];

    //calling appointment
    //print('calling post get appointment');
    final response2 = await http.get(
      Uri.parse('$REMEDI/api/selangkah/selvax/status/$id/withdependent'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': accessToken,
      },
      // body: map,
    );

    print('calling remedi');
    print(response2.body);

    if (response2.body.contains("fail")) {
      return Appointment();
    }

    Appointment appointment = appointmentFromJson(response2.body);

    return appointment;
  }

  static Future<Work> getWork() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'user_id': id,
      'token': TOKEN,
    };

    final response = await http.post(
      Uri.parse('$API_URL/get_work_profile'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    Work work = workFromJson(response.body);
    return work;
  }

  static Future<void> updateProfilePic(String? base64Img) async {
    SecureStorage secureStorage = SecureStorage();
    String? id = await secureStorage.readSecureData('userId');
    final Map<String, String?> map = {
      'id': id,
      'profile_pic': base64Img,
      'token': TOKEN,
    };

    //print('call post update_profile_pic');

    await http.post(
      Uri.parse('$API_URL/update_profile_pic'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );
  }
}
