// To parse this JSON data, do
//
//     final work = workFromJson(jsonString);

import 'dart:convert';

Work workFromJson(String str) => Work.fromJson(json.decode(str));

String workToJson(Work data) => json.encode(data.toJson());

class Work {
  Work({
    this.code,
    this.data,
  });

  int? code;
  List<WorkData>? data;

  factory Work.fromJson(Map<String, dynamic> json) => Work(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<WorkData>.from(
                json["Data"]!.map((x) => WorkData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class WorkData {
  WorkData({
    this.workAddress1,
    this.workAddress2,
    this.workPostcode,
    this.workCity,
    this.workState,
    this.workCountry,
    this.occupation,
  });

  String? workAddress1;
  String? workAddress2;
  String? workPostcode;
  String? workCity;
  String? workState;
  String? workCountry;
  String? occupation;

  factory WorkData.fromJson(Map<String, dynamic> json) => WorkData(
        workAddress1: json["work_address1"],
        workAddress2: json["work_address2"],
        workPostcode: json["work_postcode"],
        workCity: json["work_city"],
        workState: json["work_state"],
        workCountry: json["work_country"],
        occupation: json["occupation"],
      );

  Map<String, dynamic> toJson() => {
        "work_address1": workAddress1,
        "work_address2": workAddress2,
        "work_postcode": workPostcode,
        "work_city": workCity,
        "work_state": workState,
        "work_country": workCountry,
        "occupation": occupation,
      };
}
