part of 'health_record_cubit.dart';

@immutable
abstract class HealthRecordState {
  const HealthRecordState();
}

class HealthRecordInitial extends HealthRecordState {
  const HealthRecordInitial();
}

class HealthRecordLoading extends HealthRecordState {
  const HealthRecordLoading();
}

class HealthRecordLoaded extends HealthRecordState {
  final PhysicalRecord physicalRecord;

  HealthRecordLoaded(this.physicalRecord);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is HealthRecordLoaded &&
        other.physicalRecord == physicalRecord;
  }

  @override
  int get hashCode => physicalRecord.hashCode;
}
