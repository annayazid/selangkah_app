import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'health_record_state.dart';

class HealthRecordCubit extends Cubit<HealthRecordState> {
  HealthRecordCubit() : super(HealthRecordInitial());

  Future<void> getHealthRecord() async {
    emit(HealthRecordLoading());

    PhysicalRecord physicalRecord = await LhrRepositories.getHealthRecord();

    emit(HealthRecordLoaded(physicalRecord));
  }
}
