part of 'appointment_cubit.dart';

@immutable
abstract class AppointmentState {
  const AppointmentState();
}

class AppointmentInitial extends AppointmentState {
  const AppointmentInitial();
}

class AppointmentLoading extends AppointmentState {
  const AppointmentLoading();
}

class AppointmentLoaded extends AppointmentState {
  final BookAppointment bookAppointment;
  final GetAppointment listAppointment;

  AppointmentLoaded(this.bookAppointment, this.listAppointment);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AppointmentLoaded &&
        other.bookAppointment == bookAppointment &&
        other.listAppointment == listAppointment;
  }

  @override
  int get hashCode => bookAppointment.hashCode ^ listAppointment.hashCode;
}
