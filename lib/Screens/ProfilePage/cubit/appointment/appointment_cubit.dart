import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'appointment_state.dart';

class AppointmentCubit extends Cubit<AppointmentState> {
  AppointmentCubit() : super(AppointmentInitial());

  Future<void> getAppointment() async {
    emit(AppointmentLoading());

    BookAppointment bookAppointment = await LhrRepositories.bookAppointment();

    GetAppointment listAppointment = await LhrRepositories.getAppointmentList();

    emit(AppointmentLoaded(bookAppointment, listAppointment));
  }
}
