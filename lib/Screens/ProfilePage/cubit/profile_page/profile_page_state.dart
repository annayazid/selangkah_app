part of 'profile_page_cubit.dart';

@immutable
abstract class ProfilePageState {
  const ProfilePageState();
}

class ProfilePageInitial extends ProfilePageState {
  const ProfilePageInitial();
}

class ProfilePageLoading extends ProfilePageState {
  const ProfilePageLoading();
}

class ProfilePageLoaded extends ProfilePageState {
  final UserProfile? userProfile;
  final Work work;
  final Uint8List? profilePic;

  ProfilePageLoaded(this.userProfile, this.work, this.profilePic);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProfilePageLoaded &&
        other.userProfile == userProfile &&
        other.work == work &&
        other.profilePic == profilePic;
  }

  @override
  int get hashCode {
    return userProfile.hashCode ^ work.hashCode ^ profilePic.hashCode;
  }
}
