import 'dart:convert';
import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';

part 'profile_page_state.dart';

class ProfilePageCubit extends Cubit<ProfilePageState> {
  ProfilePageCubit() : super(ProfilePageInitial());

  Future<void> getUserProfile() async {
    emit(ProfilePageLoading());

    UserProfile? userProfile = await AppSplashRepositories.getUserProfile();
    Work? work = await ProfilePageRepositories.getWork();

    print(work.data![0].occupation);
    print(work.data![0].workState);

    //change SL into space
    String id = userProfile!.selId!.substring(0, 2) +
        ' ' +
        userProfile.selId!.substring(2, 6) +
        ' ' +
        userProfile.selId!.substring(6, 11);

    String? gender;

    // print('gender is ' + profile.data[7]);
    if (userProfile.gender == 'M' || userProfile.gender == 'Male') {
      gender = 'male'.tr();
    } else if (userProfile.gender == 'F' || userProfile.gender == 'Female') {
      gender = 'female'.tr();
    } else {
      gender = userProfile.gender;
    }

    Uint8List? profilePic;
    String? base64Img = userProfile.profilePic;
    if (base64Img != '') {
      profilePic = base64.decode(base64Img!);
    }

    userProfile = userProfile.copyWith(selId: id, gender: gender);

    emit(ProfilePageLoaded(userProfile, work, profilePic));
  }
}
