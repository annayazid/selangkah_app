import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'referral_letter_state.dart';

class ReferralLetterCubit extends Cubit<ReferralLetterState> {
  ReferralLetterCubit() : super(ReferralLetterInitial());

  Future<void> getReferLetter() async {
    emit(ReferralLetterLoading());

    RetrieveDocument? referralLetter =
        await LhrRepositories.retrieveDocument('5');

    emit(ReferralLetterLoaded(referralLetter));
  }
}
