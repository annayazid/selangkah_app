part of 'referral_letter_cubit.dart';

@immutable
abstract class ReferralLetterState {
  const ReferralLetterState();
}

class ReferralLetterInitial extends ReferralLetterState {
  const ReferralLetterInitial();
}

class ReferralLetterLoading extends ReferralLetterState {
  const ReferralLetterLoading();
}

class ReferralLetterLoaded extends ReferralLetterState {
  final RetrieveDocument? referralLetter;

  ReferralLetterLoaded(this.referralLetter);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ReferralLetterLoaded &&
        other.referralLetter == referralLetter;
  }

  @override
  int get hashCode => referralLetter.hashCode;
}
