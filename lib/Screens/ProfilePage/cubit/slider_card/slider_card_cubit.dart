import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';

part 'slider_card_state.dart';

class SliderCardCubit extends Cubit<SliderCardState> {
  SliderCardCubit() : super(SliderCardInitial());

  Future<void> getSliderCard(String? page) async {
    emit(SliderCardLoading());
    if (page == 'SelangkahID') {
      Work? work = await ProfilePageRepositories.getWork();
      emit(SliderCardSelangkahID(work));
    } else if (page == 'SelangkahVax') {
      emit(SliderCardSelangkahVax());
    } else if (page == 'HAYAT') {
      emit(
        SliderCardHAYAT(),
      );
    }
  }
}
