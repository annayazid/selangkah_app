part of 'slider_card_cubit.dart';

@immutable
abstract class SliderCardState {
  const SliderCardState();
}

class SliderCardInitial extends SliderCardState {
  const SliderCardInitial();
}

class SliderCardLoading extends SliderCardState {
  const SliderCardLoading();
}

class SliderCardSelangkahID extends SliderCardState {
  final Work work;

  SliderCardSelangkahID(this.work);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SliderCardSelangkahID && other.work == work;
  }

  @override
  int get hashCode => work.hashCode;
}

class SliderCardSelangkahVax extends SliderCardState {
  const SliderCardSelangkahVax();
}

class SliderCardHAYAT extends SliderCardState {
  const SliderCardHAYAT();
  // final PhysicalPatient physicalPatient;
  // final GetAppointment getAppointment;
  // final LabResult labResult;
  // final BackgroundIllness backgroundIllness;
  // final PhysicalRecord physicalRecord;
  // final BookAppointment appointment;
  // final RetrieveDocument? prescription;
  // final RetrieveDocument? medicalCert;
  // final RetrieveDocument? vaccineCert;
  // final BackgroundIllness archivedBackgroundIllness;
  // final GetAppointment archivedAppointment;
  // final LabResult archivedLabResult;
  // final ArchivedRecord? archivedPrescription;
  // final ArchivedRecord? archivedMedicalCert;
  // final ArchivedRecord? archivedVaccineCert;

  // const SliderCardHAYAT(
  //   this.physicalPatient,
  //   this.getAppointment,
  //   this.labResult,
  //   this.backgroundIllness,
  //   this.physicalRecord,
  //   this.appointment,
  //   this.prescription,
  //   this.medicalCert,
  //   this.vaccineCert,
  //   this.archivedBackgroundIllness,
  //   this.archivedAppointment,
  //   this.archivedLabResult,
  //   this.archivedPrescription,
  //   this.archivedMedicalCert,
  //   this.archivedVaccineCert,
  // );

  // @override
  // bool operator ==(Object other) {
  //   if (identical(this, other)) return true;

  //   return other is SliderCardHAYAT &&
  //       other.physicalPatient == physicalPatient &&
  //       other.getAppointment == getAppointment &&
  //       other.labResult == labResult &&
  //       other.backgroundIllness == backgroundIllness &&
  //       other.physicalRecord == physicalRecord &&
  //       other.appointment == appointment &&
  //       other.prescription == prescription &&
  //       other.medicalCert == medicalCert &&
  //       other.vaccineCert == vaccineCert &&
  //       other.archivedBackgroundIllness == archivedBackgroundIllness &&
  //       other.archivedAppointment == archivedAppointment &&
  //       other.archivedLabResult == archivedLabResult &&
  //       other.archivedPrescription == archivedPrescription &&
  //       other.archivedMedicalCert == archivedMedicalCert &&
  //       other.archivedVaccineCert == archivedVaccineCert;
  // }

  // @override
  // int get hashCode {
  //   return physicalPatient.hashCode ^
  //       getAppointment.hashCode ^
  //       labResult.hashCode ^
  //       backgroundIllness.hashCode ^
  //       physicalRecord.hashCode ^
  //       appointment.hashCode ^
  //       prescription.hashCode ^
  //       medicalCert.hashCode ^
  //       vaccineCert.hashCode ^
  //       archivedBackgroundIllness.hashCode ^
  //       archivedAppointment.hashCode ^
  //       archivedLabResult.hashCode ^
  //       archivedPrescription.hashCode ^
  //       archivedMedicalCert.hashCode ^
  //       archivedVaccineCert.hashCode;
  // }
}

class SliderCardError extends SliderCardState {
  const SliderCardError();
}
