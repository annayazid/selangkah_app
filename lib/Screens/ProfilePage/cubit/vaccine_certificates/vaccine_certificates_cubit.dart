import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'vaccine_certificates_state.dart';

class VaccineCertificatesCubit extends Cubit<VaccineCertificatesState> {
  VaccineCertificatesCubit() : super(VaccineCertificatesInitial());

  Future<void> getVaccineCCert() async {
    emit(VaccineCertificatesLoading());

    RetrieveDocument? vaccineCert = await LhrRepositories.retrieveDocument('3');

    emit(VaccineCertificatesLoaded(vaccineCert));
  }
}
