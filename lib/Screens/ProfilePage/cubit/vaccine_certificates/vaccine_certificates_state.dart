part of 'vaccine_certificates_cubit.dart';

@immutable
abstract class VaccineCertificatesState {
  const VaccineCertificatesState();
}

class VaccineCertificatesInitial extends VaccineCertificatesState {
  const VaccineCertificatesInitial();
}

class VaccineCertificatesLoading extends VaccineCertificatesState {
  const VaccineCertificatesLoading();
}

class VaccineCertificatesLoaded extends VaccineCertificatesState {
  final RetrieveDocument? vaccineCert;

  VaccineCertificatesLoaded(this.vaccineCert);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is VaccineCertificatesLoaded &&
        other.vaccineCert == vaccineCert;
  }

  @override
  int get hashCode => vaccineCert.hashCode;
}
