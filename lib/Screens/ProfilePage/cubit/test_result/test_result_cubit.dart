import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'test_result_state.dart';

class TestResultCubit extends Cubit<TestResultState> {
  TestResultCubit() : super(TestResultInitial());

  Future<void> getTestResult() async {
    emit(TestResultLoading());

    LabResult labResult = await LhrRepositories.getLabResult();

    emit(TestResultLoaded(labResult));
  }
}
