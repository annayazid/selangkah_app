part of 'test_result_cubit.dart';

@immutable
abstract class TestResultState {
  const TestResultState();
}

class TestResultInitial extends TestResultState {
  const TestResultInitial();
}

class TestResultLoading extends TestResultState {
  const TestResultLoading();
}

class TestResultLoaded extends TestResultState {
  final LabResult labResult;

  TestResultLoaded(this.labResult);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TestResultLoaded && other.labResult == labResult;
  }

  @override
  int get hashCode => labResult.hashCode;
}
