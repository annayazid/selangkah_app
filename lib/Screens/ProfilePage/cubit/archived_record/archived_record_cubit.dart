import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'archived_record_state.dart';

class ArchivedRecordCubit extends Cubit<ArchivedRecordState> {
  ArchivedRecordCubit() : super(ArchivedRecordInitial());

  Future<void> getArchivedRecord() async {
    emit(ArchivedRecordLoading());

    BackgroundIllness archivedBackgroundIllness =
        await LhrRepositories.getArchiveBackgroundIllness();

    GetAppointment archivedAppointment =
        await LhrRepositories.getArchiveAppointment();

    ArchivedRecord? archivedReferralLetter =
        await LhrRepositories.getArchivedRecord('5');

    LabResult archivedLabResult = await LhrRepositories.getArchiveLabResult();

    ArchivedRecord? archivedMedicalCert =
        await LhrRepositories.getArchivedRecord('2');

    ArchivedRecord? archivedPrescription =
        await LhrRepositories.getArchivedRecord('1');

    ArchivedRecord? archivedVaccineCert =
        await LhrRepositories.getArchivedRecord('3');

    ArchivedRecord? archivedInvoice =
        await LhrRepositories.getArchivedRecord('6');

    emit(ArchivedRecordLoaded(
        archivedBackgroundIllness,
        archivedAppointment,
        archivedReferralLetter,
        archivedLabResult,
        archivedMedicalCert,
        archivedPrescription,
        archivedVaccineCert,
        archivedInvoice));
  }
}
