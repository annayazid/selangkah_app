part of 'archived_record_cubit.dart';

@immutable
abstract class ArchivedRecordState {
  const ArchivedRecordState();
}

class ArchivedRecordInitial extends ArchivedRecordState {
  const ArchivedRecordInitial();
}

class ArchivedRecordLoading extends ArchivedRecordState {
  const ArchivedRecordLoading();
}

class ArchivedRecordLoaded extends ArchivedRecordState {
  final BackgroundIllness archivedBackgroundIllness;

  final GetAppointment archivedAppointment;

  final ArchivedRecord? archivedReferralLetter;

  final LabResult archivedLabResult;

  final ArchivedRecord? archivedMedicalCert;

  final ArchivedRecord? archivedPrescription;

  final ArchivedRecord? archivedVaccineCert;

  final ArchivedRecord? archivedInvoice;

  ArchivedRecordLoaded(
      this.archivedBackgroundIllness,
      this.archivedAppointment,
      this.archivedReferralLetter,
      this.archivedLabResult,
      this.archivedMedicalCert,
      this.archivedPrescription,
      this.archivedVaccineCert,
      this.archivedInvoice);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ArchivedRecordLoaded &&
        other.archivedBackgroundIllness == archivedBackgroundIllness &&
        other.archivedAppointment == archivedAppointment &&
        other.archivedReferralLetter == archivedReferralLetter &&
        other.archivedLabResult == archivedLabResult &&
        other.archivedMedicalCert == archivedMedicalCert &&
        other.archivedPrescription == archivedPrescription &&
        other.archivedVaccineCert == archivedVaccineCert &&
        other.archivedInvoice == archivedInvoice;
  }

  @override
  int get hashCode {
    return archivedBackgroundIllness.hashCode ^
        archivedAppointment.hashCode ^
        archivedReferralLetter.hashCode ^
        archivedLabResult.hashCode ^
        archivedMedicalCert.hashCode ^
        archivedPrescription.hashCode ^
        archivedVaccineCert.hashCode ^
        archivedInvoice.hashCode;
  }
}
