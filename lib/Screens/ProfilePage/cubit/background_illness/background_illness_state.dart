part of 'background_illness_cubit.dart';

@immutable
abstract class BackgroundIllnessState {
  const BackgroundIllnessState();
}

class BackgroundIllnessInitial extends BackgroundIllnessState {
  const BackgroundIllnessInitial();
}

class BackgroundIllnessLoading extends BackgroundIllnessState {
  const BackgroundIllnessLoading();
}

class BackgroundIllnessLoaded extends BackgroundIllnessState {
  final BackgroundIllness backgroundIllness;

  BackgroundIllnessLoaded(this.backgroundIllness);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is BackgroundIllnessLoaded &&
        other.backgroundIllness == backgroundIllness;
  }

  @override
  int get hashCode => backgroundIllness.hashCode;
}
