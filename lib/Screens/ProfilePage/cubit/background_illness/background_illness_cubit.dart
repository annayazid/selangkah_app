import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'background_illness_state.dart';

class BackgroundIllnessCubit extends Cubit<BackgroundIllnessState> {
  BackgroundIllnessCubit() : super(BackgroundIllnessInitial());

  Future<void> getBackgroundIllness() async {
    emit(BackgroundIllnessLoading());

    BackgroundIllness backgroundIllness =
        await LhrRepositories.getBackgroundIllness();

    emit(BackgroundIllnessLoaded(backgroundIllness));
  }
}
