import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'invoice_state.dart';

class InvoiceCubit extends Cubit<InvoiceState> {
  InvoiceCubit() : super(InvoiceInitial());

  Future<void> getInvoice() async {
    emit(InvoiceLoading());

    RetrieveDocument? invoice = await LhrRepositories.retrieveDocument('6');

    emit(InvoiceLoaded(invoice));
  }
}
