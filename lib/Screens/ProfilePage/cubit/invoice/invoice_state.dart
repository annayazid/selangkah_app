part of 'invoice_cubit.dart';

@immutable
abstract class InvoiceState {
  const InvoiceState();
}

class InvoiceInitial extends InvoiceState {
  const InvoiceInitial();
}

class InvoiceLoading extends InvoiceState {
  const InvoiceLoading();
}

class InvoiceLoaded extends InvoiceState {
  final RetrieveDocument? invoice;

  InvoiceLoaded(this.invoice);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is InvoiceLoaded && other.invoice == invoice;
  }

  @override
  int get hashCode => invoice.hashCode;
}
