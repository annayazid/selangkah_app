part of 'medical_certificates_cubit.dart';

@immutable
abstract class MedicalCertificatesState {
  const MedicalCertificatesState();
}

class MedicalCertificatesInitial extends MedicalCertificatesState {
  const MedicalCertificatesInitial();
}

class MedicalCertificatesLoading extends MedicalCertificatesState {
  const MedicalCertificatesLoading();
}

class MedicalCertificatesLoaded extends MedicalCertificatesState {
  final RetrieveDocument? medicalCert;

  MedicalCertificatesLoaded(this.medicalCert);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MedicalCertificatesLoaded &&
        other.medicalCert == medicalCert;
  }

  @override
  int get hashCode => medicalCert.hashCode;
}
