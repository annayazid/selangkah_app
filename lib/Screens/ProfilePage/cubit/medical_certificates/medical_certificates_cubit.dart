import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'medical_certificates_state.dart';

class MedicalCertificatesCubit extends Cubit<MedicalCertificatesState> {
  MedicalCertificatesCubit() : super(MedicalCertificatesInitial());

  Future<void> getMedicalCert() async {
    emit(MedicalCertificatesLoading());

    RetrieveDocument? medicalCert = await LhrRepositories.retrieveDocument('2');

    emit(MedicalCertificatesLoaded(medicalCert));
  }
}
