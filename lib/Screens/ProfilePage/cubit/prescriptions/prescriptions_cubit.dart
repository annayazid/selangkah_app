import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'prescriptions_state.dart';

class PrescriptionsCubit extends Cubit<PrescriptionsState> {
  PrescriptionsCubit() : super(PrescriptionsInitial());

  Future<void> getPrescriptions() async {
    emit(PrescriptionsLoading());

    RetrieveDocument? prescription =
        await LhrRepositories.retrieveDocument('1');

    emit(PrescriptionsLoaded(prescription));
  }
}
