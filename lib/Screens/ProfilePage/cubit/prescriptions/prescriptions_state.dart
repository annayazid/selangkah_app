part of 'prescriptions_cubit.dart';

@immutable
abstract class PrescriptionsState {
  const PrescriptionsState();
}

class PrescriptionsInitial extends PrescriptionsState {
  const PrescriptionsInitial();
}

class PrescriptionsLoading extends PrescriptionsState {
  const PrescriptionsLoading();
}

class PrescriptionsLoaded extends PrescriptionsState {
  final RetrieveDocument? prescription;

  PrescriptionsLoaded(this.prescription);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is PrescriptionsLoaded && other.prescription == prescription;
  }

  @override
  int get hashCode => prescription.hashCode;
}
