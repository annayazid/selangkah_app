import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:meta/meta.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';

part 'update_image_state.dart';

class UpdateImageCubit extends Cubit<UpdateImageState> {
  UpdateImageCubit() : super(UpdateImageInitial());

  Future<void> updateImage(String? base64Img) async {
    emit(UpdateImageLoading());
    await ProfilePageRepositories.updateProfilePic(
      base64Img,
    );
    toast('updateSuccess'.tr().toString());
    emit(UpdateImageFinish());
  }
}
