part of 'update_image_cubit.dart';

@immutable
abstract class UpdateImageState {
  const UpdateImageState();
}

class UpdateImageInitial extends UpdateImageState {
  const UpdateImageInitial();
}

class UpdateImageLoading extends UpdateImageState {
  const UpdateImageLoading();
}

class UpdateImageFinish extends UpdateImageState {
  const UpdateImageFinish();
}
