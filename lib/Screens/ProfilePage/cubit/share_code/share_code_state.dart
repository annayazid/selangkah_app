part of 'share_code_cubit.dart';

@immutable
abstract class ShareCodeState {
  const ShareCodeState();
}

class ShareCodeInitial extends ShareCodeState {
  const ShareCodeInitial();
}

class ShareCodeLoading extends ShareCodeState {
  const ShareCodeLoading();
}

class ShareCodeLoaded extends ShareCodeState {
  final String code;

  ShareCodeLoaded(this.code);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ShareCodeLoaded && other.code == code;
  }

  @override
  int get hashCode => code.hashCode;
}
