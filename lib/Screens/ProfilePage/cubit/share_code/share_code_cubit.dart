import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'share_code_state.dart';

class ShareCodeCubit extends Cubit<ShareCodeState> {
  ShareCodeCubit() : super(ShareCodeInitial());

  Future<void> getShareCode(String? idDoc, String? idSaring) async {
    emit(ShareCodeLoading());

    String code = await LhrRepositories.shareCode(idDoc, idSaring);

    emit(ShareCodeLoaded(code));
  }
}
