import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/EditWorkplace/edit_workplace.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/Screens/VerifyProfile/verify_profile.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';

class SliderCardSelangkahIDWidget extends StatefulWidget {
  final UserProfile? userProfile;
  final Work? work;
  const SliderCardSelangkahIDWidget({
    Key? key,
    required this.userProfile,
    required this.work,
  }) : super(key: key);

  @override
  State<SliderCardSelangkahIDWidget> createState() =>
      _SliderCardSelangkahIDWidgetState();
}

class _SliderCardSelangkahIDWidgetState
    extends State<SliderCardSelangkahIDWidget> {
  Future<void> redirectEditProfile() async {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => GetUserDetailsCubit(),
              ),
              BlocProvider(
                create: (context) => UpdateDetailCubit(),
              ),
            ],
            child: VerifyProfile(),
          );
        },
      ),
    ).then((value) => context.read<ProfilePageCubit>().getUserProfile());
  }

  Future<void> redirectEditWork() async {
    Navigator.of(context)
        .push(
          MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (context) => WorkCubit(),
                ),
                BlocProvider(
                  create: (context) => UpdateWorkCubit(),
                ),
              ],
              child: EditWorkplace(),
            ),
          ),
        )
        .then((value) =>
            context.read<SliderCardCubit>().getSliderCard('SelangkahID'));
  }

  @override
  void initState() {
    GlobalFunction.screenJourney('5');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      children: [
        Row(
          children: [
            SizedBox(width: 25),
            Text(
              'edit'.tr(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 13,
              ),
            ),
            IconButton(
                icon: Container(
                  decoration: BoxDecoration(
                    color: kPrimaryColor,
                    shape: BoxShape.circle,
                  ),
                  padding: EdgeInsets.all(3),
                  child: Icon(
                    Icons.edit,
                    color: Colors.white,
                    size: 17,
                  ),
                ),
                onPressed: () {
                  redirectEditProfile();
                }),
          ],
        ),
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                offset: Offset(0.0, 1.0), //(x,y)
                blurRadius: 4.0,
              ),
            ],
          ),
          width: size.width * 0.85,
          child: Column(
            children: [
              SizedBox(height: 10),
              SelangkahIDData(
                icon: FontAwesomeIcons.solidUser,
                category: 'name'.tr(),
                data: widget.userProfile!.name!,
              ),
              SizedBox(height: 10),
              SelangkahIDData(
                icon: FontAwesomeIcons.phone,
                category: 'phonenumber'.tr(),
                data: widget.userProfile!.phoneNo!,
              ),
              SizedBox(height: 10),
              SelangkahIDData(
                icon: FontAwesomeIcons.solidEnvelope,
                category: 'email'.tr(),
                data: widget.userProfile!.email != null
                    ? widget.userProfile!.email!
                    : '',
              ),
              SizedBox(height: 10),
              SelangkahIDData(
                icon: FontAwesomeIcons.solidIdCard,
                category: 'icno'.tr(),
                data: widget.userProfile!.idNo != null
                    ? widget.userProfile!.idNo!
                    : '',
              ),
              SizedBox(height: 10),
              SelangkahIDData(
                icon: FontAwesomeIcons.globe,
                category: 'citizen'.tr(),
                data: widget.userProfile!.citizen != null
                    ? widget.userProfile!.citizen!
                    : '',
              ),
              SizedBox(height: 10),
              SelangkahIDData(
                icon: FontAwesomeIcons.venusMars,
                category: 'gender'.tr(),
                data: widget.userProfile!.gender != null
                    ? widget.userProfile!.gender!
                    : '',
              ),
              SizedBox(height: 10),
              SelangkahIDData(
                icon: FontAwesomeIcons.calendar,
                category: 'dob'.tr(),
                data: widget.userProfile!.dob != null
                    ? widget.userProfile!.dob!
                    : '',
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
        SizedBox(height: 10),
        Row(
          children: [
            SizedBox(width: 25),
            Text(
              'edit'.tr(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 13,
              ),
            ),
            IconButton(
                icon: Container(
                  decoration: BoxDecoration(
                    color: kPrimaryColor,
                    shape: BoxShape.circle,
                  ),
                  padding: EdgeInsets.all(3),
                  child: Icon(
                    Icons.edit,
                    color: Colors.white,
                    size: 17,
                  ),
                ),
                onPressed: () {
                  redirectEditWork();
                }),
          ],
        ),
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                offset: Offset(0.0, 1.0), //(x,y)
                blurRadius: 4.0,
              ),
            ],
          ),
          width: size.width * 0.85,
          child: Column(
            children: [
              SizedBox(height: 10),
              SelangkahIDData(
                icon: FontAwesomeIcons.solidUser,
                category: 'occupation'.tr(),
                data: widget.work!.data![0].occupation != null
                    ? widget.work!.data![0].occupation!
                    : '',
              ),
              SizedBox(height: 10),
              SelangkahIDData(
                icon: FontAwesomeIcons.mapPin,
                category: 'address'.tr(),
                data: widget.work!.data![0].workAddress1 != null &&
                        widget.work!.data![0].workAddress2 != null
                    ? '${widget.work!.data![0].workAddress1!} ${widget.work!.data![0].workAddress2!}'
                    : '',
              ),
              SizedBox(height: 10),
              SelangkahIDData(
                icon: FontAwesomeIcons.solidEnvelope,
                category: 'postcode'.tr(),
                data: widget.work!.data![0].workPostcode != null
                    ? widget.work!.data![0].workPostcode!
                    : '',
              ),
              SizedBox(height: 10),
              SelangkahIDData(
                icon: FontAwesomeIcons.city,
                category: 'city'.tr(),
                data: widget.work!.data![0].workCity != null
                    ? widget.work!.data![0].workCity!
                    : '',
              ),
              SizedBox(height: 10),
              SelangkahIDData(
                icon: FontAwesomeIcons.solidFlag,
                category: 'state'.tr(),
                data: widget.work!.data![0].workState != null
                    ? widget.work!.data![0].workState!
                    : '',
              ),
              SizedBox(height: 10),
              SelangkahIDData(
                icon: FontAwesomeIcons.globe,
                category: 'country'.tr(),
                data: widget.work!.data![0].workCountry != null
                    ? widget.work!.data![0].workCountry!
                    : '',
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ],
    );
  }
}

class SelangkahIDData extends StatelessWidget {
  final IconData icon;
  final String category;
  final String data;

  const SelangkahIDData(
      {super.key,
      required this.icon,
      required this.category,
      required this.data});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            SizedBox(width: 20),
            Icon(
              icon,
              color: Color(0xFFFDB514),
              size: 15,
            ),
            SizedBox(width: 10),
            Text(
              category,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
        SizedBox(width: 10),
        Container(
          width: size.width * 0.3,
          child: Text(
            data,
            style: TextStyle(
              color: Colors.grey,
              fontSize: 15,
            ),
          ),
        ),
      ],
    );
  }
}
