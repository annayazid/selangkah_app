import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:url_launcher/url_launcher.dart';

class ShareCodeDialog extends StatefulWidget {
  final String? idDoc;
  final String? idDataSaring;

  const ShareCodeDialog(
      {super.key, required this.idDoc, required this.idDataSaring});

  @override
  State<ShareCodeDialog> createState() => _ShareCodeDialogState();
}

class _ShareCodeDialogState extends State<ShareCodeDialog> {
  @override
  void initState() {
    super.initState();
    context
        .read<ShareCodeCubit>()
        .getShareCode(widget.idDoc, widget.idDataSaring);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      scrollable: true,
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisSize:
          //     MainAxisSize.min,
          children: [
            Center(
              child: Image.asset(
                'assets/images/lhr/share.png',
                height: 30,
              ),
            ),
            SizedBox(height: 15),
            Center(
              child: Text(
                'share_record'.tr(),
                style: TextStyle(
                  color: Color(0xFF505660),
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
            SizedBox(height: 15),
            Center(
              child: Linkify(
                text: 'share_dialog'.tr(),
                // text: 'hardcode',
                style: TextStyle(
                  color: Color(0xFF898DA1),
                  fontSize: 15,
                ),
                textAlign: TextAlign.center,
                options: LinkifyOptions(humanize: false),
                onOpen: (link) {
                  launchUrl(
                    Uri.parse(link.url),
                    mode: LaunchMode.externalApplication,
                  );
                },
              ),
            ),
            SizedBox(height: 15),
            BlocBuilder<ShareCodeCubit, ShareCodeState>(
              builder: (context, state) {
                if (state is ShareCodeLoaded) {
                  return Center(
                    child: Text(
                      // '7C1-Y4N'.tr(),
                      state.code,
                      style: TextStyle(
                        // fontWeight:
                        //     FontWeight
                        //         .bold,
                        fontSize: 50,
                      ),
                    ),
                  );
                } else {
                  return SpinKitFadingCircle(
                    color: kPrimaryColor,
                    size: 20,
                  );
                }
              },
            ),
            SizedBox(height: 15),
            Center(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  backgroundColor: Color(0xFFEF0056),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'done'.tr(),
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
