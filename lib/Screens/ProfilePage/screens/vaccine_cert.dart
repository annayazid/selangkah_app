import 'package:date_format/date_format.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:url_launcher/url_launcher.dart';

class VaccineCert extends StatefulWidget {
  final Appointment? appointment;
  final int index;

  const VaccineCert(
      {super.key, required this.appointment, required this.index});

  @override
  _VaccineCertState createState() => _VaccineCertState();
}

class _VaccineCertState extends State<VaccineCert> {
  GlobalKey<FlipCardState> cardKey = GlobalKey<FlipCardState>();
  final frontKey = GlobalKey();
  bool _showQrCard = false;
  var _uid;
  Widget? _qrImage;
  Widget? _qrImageRear;
  String? _qrData;

  @override
  void initState() {
    super.initState();
    genQrCode();
  }

  void genQrCode() {
    String idNo = widget.appointment!.data![widget.index].icNo!;
    String phone = widget.appointment!.data![widget.index].phoneNo!;
    String qrData =
        '$SELANGKAH_VAX/digital-vax-cert?id_no=$idNo&phone_no=$phone';

    setState(() {
      _qrData = qrData;
      _qrImage = QrImage(
        data: qrData,
        version: QrVersions.auto,
        size: 120,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height -
    //     AppBar().preferredSize.height -
    //     MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Container(
      child: FlipCard(
        flipOnTouch: false,
        key: cardKey,
        direction: FlipDirection.HORIZONTAL, // default
        front: _buildFront(width),
        back: _buildRear(),
      ),
    );
  }

  _buildFront(double width) {
    return Column(
      children: [
        Container(
          key: frontKey,
          padding: EdgeInsets.all(20),
          width: double.infinity,
          decoration: BoxDecoration(
            color: Color(0xFFF7EB95),
            borderRadius: BorderRadius.circular(40),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              VaccineHeader(),
              SizedBox(height: 10),
              Text(
                'This is to certify that',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Color(0xFFC66C1C),
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.bold,
                  fontSize: 12,
                ),
              ),
              SizedBox(height: 20),
              VaccinePersonalInfo(
                category: 'Name:',
                data: widget.appointment!.data![widget.index].accName,
              ),
              VaccinePersonalInfo(
                category: 'Date Of Birth:',
                data: formatDate(
                  widget.appointment!.data![widget.index].dob!,
                  [yyyy, '-', mm, '-', dd],
                ),
              ),
              VaccinePersonalInfo(
                category: 'Gender:',
                data: widget.appointment!.data![widget.index].gender,
              ),
              VaccinePersonalInfo(
                category: 'Nationality:',
                data: widget.appointment!.data![widget.index].citizen,
              ),
              VaccinePersonalInfo(
                category:
                    '${widget.appointment!.data![widget.index].idType ?? 'I/C Number'}:',
                data: widget.appointment!.data![widget.index].icNo,
              ),
              SizedBox(height: 20),
              Text(
                'has been vaccinated against the',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Color(0xFFC66C1C),
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.bold,
                  fontSize: 12,
                ),
              ),
              Text(
                'Coronavirus Disease (COVID-19) in accordance',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Color(0xFFC66C1C),
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.bold,
                  fontSize: 12,
                ),
              ),
              Text(
                'with the international Health Regulations',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Color(0xFFC66C1C),
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.bold,
                  fontSize: 12,
                ),
              ),
              SizedBox(height: 20),
              VaccineDoseInfo(
                dose: 1,
                appointmentDate:
                    widget.appointment!.data![widget.index].appDate1,
                vaccineName: widget.appointment!.data![widget.index].generic1,
                manifactureName:
                    widget.appointment!.data![widget.index].manufacturer1,
                batchNo: widget.appointment!.data![widget.index].batchNo1,
                picName: widget.appointment!.data![widget.index].picName1,
                ppvName: widget.appointment!.data![widget.index].ppvName1,
                picMMC: widget.appointment!.data![widget.index].picMmc1,
              ),
              SizedBox(height: 10),
              VaccineDoseInfo(
                dose: 2,
                appointmentDate:
                    widget.appointment!.data![widget.index].appDate2,
                vaccineName: widget.appointment!.data![widget.index].generic2,
                manifactureName:
                    widget.appointment!.data![widget.index].manufacturer2,
                batchNo: widget.appointment!.data![widget.index].batchNo2,
                picName: widget.appointment!.data![widget.index].picName2,
                ppvName: widget.appointment!.data![widget.index].ppvName2,
                picMMC: widget.appointment!.data![widget.index].picMmc1,
              ),
              SizedBox(height: 30),
              VaccineFooter(
                handleShowQrCard: _handleShowQrCard,
                uid: _uid,
                qrImage: _qrImage!,
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            GestureDetector(
              onTap: () async {
                // SecureStorage secureStorage = SecureStorage();

                // String idNo = await secureStorage.readSecureData('guestIdCard');
                // String phone =
                //     await secureStorage.readSecureData('guestPhoneNum');

                String idNo = widget.appointment!.data![widget.index].icNo!;
                String phone = widget.appointment!.data![widget.index].phoneNo!;

                phone = phone.replaceAll('+', '');

                if (phone.substring(0, 1) == '6') {
                  phone = phone.substring(1, phone.length);
                }

                launchUrl(
                    Uri.parse(
                        '$SELANGKAH_VAX/vax-cert-mini-pdf?id_no=$idNo&phone_no=$phone'),
                    mode: LaunchMode.externalApplication);
              },
              child: Container(
                width: width * 0.3,
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  color: kPrimaryColor,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Text(
                  'download_mini'.tr(),
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            GestureDetector(
              onTap: () async {
                String idNo = widget.appointment!.data![widget.index].icNo!;
                String phone = widget.appointment!.data![widget.index].phoneNo!;

                phone = phone.replaceAll('+', '');

                if (phone.substring(0, 1) == '6') {
                  phone = phone.substring(1, phone.length);
                }

                // SecureStorage secureStorage = SecureStorage();

                // String idNo = await secureStorage.readSecureData('guestIdCard');
                // String phone =
                //     await secureStorage.readSecureData('guestPhoneNum');
                print(
                    '$SELANGKAH_VAX/vax-cert-pdf?id_no=$idNo&phone_no=$phone');
                launchUrl(
                    Uri.parse(
                        '$SELANGKAH_VAX/vax-cert-pdf?id_no=$idNo&phone_no=$phone'),
                    mode: LaunchMode.externalApplication);
              },
              child: Container(
                width: width * 0.3,
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  color: kPrimaryColor,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Text(
                  'download_full'.tr(),
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  _buildRear() {
    return Container(
      height: 500,
      alignment: Alignment.center,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(40), color: Color(0xFFF7EB95)),
        padding: const EdgeInsets.all(20.0),
        child: Container(
          height: 255,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40), color: Colors.white),
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              GestureDetector(
                  onTap: () => _handleShowQrCard(),
                  child: Container(child: _qrImageRear)),
              Text('Scan your Selangkah Vax Certificate here',
                  style: TextStyle(fontSize: 11)),
            ],
          ),
        ),
      ),
    );
  }

  _handleShowQrCard() {
    bool qrShow = _showQrCard = !_showQrCard;
    // print('QRSHOW: $qrShow');

    setState(() {
      cardKey.currentState!.toggleCard();
      _qrImageRear = QrImage(
        data: _qrData!,
        version: QrVersions.auto,
        size: 200,
      );
      _showQrCard = qrShow;
    });
  }
}

class VaccineHeader extends StatelessWidget {
  const VaccineHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.red,
      height: 70,
      child: Row(
        children: [
          //jata selangor
          Expanded(
            child: FittedBox(
              child: Column(
                children: [
                  Container(
                    height: 45,
                    child: Image.asset(
                      'assets/images/ProfilePage/Jata Negeri Selangor.png',
                      // fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(height: 2),
                  Text(
                    'STATE GOVERNMENT',
                    style: TextStyle(
                      fontSize: 5,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    'OF SELANGOR',
                    style: TextStyle(
                      fontSize: 5,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 2,
                    ),
                  ),
                  Text(
                    'MALAYSIA',
                    style: TextStyle(
                      fontSize: 5,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 4,
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(width: 10),
          //text vaccine
          Expanded(
            flex: 3,
            child: Column(
              children: [
                // Expanded(
                //   child: FittedBox(
                //     child: Text(
                //       'INTERNATIONAL',
                //       style: TextStyle(
                //         fontWeight: FontWeight.w900,
                //         letterSpacing: 3,
                //         // fontSize: 18,
                //       ),
                //     ),
                //   ),
                // ),
                Expanded(
                  child: FittedBox(
                    child: Text(
                      'CERTIFICATE OF',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        // letterSpacing: 2,
                        // fontSize: 18,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: FittedBox(
                    child: Text(
                      'VACCINATION',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        letterSpacing: 2,
                        // fontSize: 20,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class VaccineDoseInfo extends StatelessWidget {
  final int? dose;
  final DateTime? appointmentDate;
  final String? vaccineName;
  final String? manifactureName;
  final String? batchNo;
  final String? picName;
  final String? ppvName;
  final String? picMMC;

  const VaccineDoseInfo(
      {super.key,
      this.dose,
      this.appointmentDate,
      this.vaccineName,
      this.manifactureName,
      this.batchNo,
      this.picName,
      this.ppvName,
      this.picMMC});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        // clipBehavior: Clip.none,
        // overflow: Overflow.visible,
        // fit: StackFit.passthrough,
        children: [
          Container(
            // height: 100,
            padding: EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 15,
            ),
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              border: Border.all(
                color: Color(0xFFACA36C),
                width: 2.5,
              ),
            ),
            child: Column(
              children: [
                VaccineDoseCard(
                  category: 'Date:',
                  data: appointmentDate != null
                      // ? DateFormat('d MMMM yyyy')
                      //     .format(appointmentDate)
                      //     .toUpperCase()
                      // ? DateFormat('yyyy-MM-dd').format(appointmentDate)
                      ? formatDate(
                          appointmentDate!,
                          [yyyy, '-', mm, '-', dd],
                        )
                      : 'To be updated',
                ),
                VaccineDoseCard(
                  category: 'Vaccine:',
                  data: 'COVID-19 VACCINE',
                ),
                VaccineDoseCard(
                  category: 'Brand:',
                  data: vaccineName != null ? vaccineName!.toUpperCase() : '',
                ),
                VaccineDoseCard(
                  category: 'Manufacturer:',
                  data: manifactureName != null
                      ? manifactureName!.toUpperCase()
                      : '',
                ),
                VaccineDoseCard(
                  category: 'Batch No:',
                  data: batchNo != null ? batchNo!.toUpperCase() : '',
                ),
                VaccineDoseCard(
                  category: 'Medical Officer:',
                  data: picName != null && picMMC != null
                      ? '${picName!.toUpperCase()} (${picMMC!.toUpperCase()})'
                      : 'To be updated',
                ),
                VaccineDoseCard(
                  category: 'Location:',
                  data: ppvName != null ? ppvName!.toUpperCase() : '',
                ),
                VaccineDoseCard(
                  category: 'Country:',
                  data: 'MYS',
                ),
              ],
            ),
          ),
          Positioned(
            top: -3,
            child: Container(
              color: Color(0xFFF7EB95),
              child: Text(
                'Dose $dose',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 12,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class VaccineDoseCard extends StatelessWidget {
  final String category;
  final String data;

  const VaccineDoseCard(
      {super.key, required this.category, required this.data});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Text(
              category,
              style: TextStyle(
                fontSize: 12,
              ),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
        SizedBox(width: 10),
        Container(
          width: size.width * 0.3,
          child: Text(
            data,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 12,
            ),
          ),
        ),
      ],
    );
  }
}

class VaccinePersonalInfo extends StatelessWidget {
  final String? category;
  final String? data;

  const VaccinePersonalInfo(
      {super.key, required this.category, required this.data});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Text(
              category != null ? category! : '',
              style: TextStyle(
                fontSize: 12,
              ),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
        SizedBox(width: 10),
        Container(
          width: size.width * 0.4,
          child: Text(
            data != null ? data! : 'To be updated',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 12,
            ),
          ),
        ),
      ],
    );
  }
}

class VaccineFooter extends StatelessWidget {
  final Function handleShowQrCard;
  final uid;
  final Widget qrImage;

  const VaccineFooter(
      {super.key,
      required this.handleShowQrCard,
      required this.uid,
      required this.qrImage});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Expanded(
            flex: 4,
            child: Column(
              children: [
                Row(
                  children: [
                    VaccinFooterImageDetails(
                      text: 'HEALTHCARE PROVIDER',
                      imgPath: 'assets/images/ProfilePage/SELCARE.png',
                      textName: 'SELCARE CLINIC SDN BHD',
                    ),
                    VaccinFooterImageDetails(
                      text: 'DIGITAL PARTNER',
                      imgPath: 'assets/images/ProfilePage/Selangkah Logo.png',
                      textName: 'SELANGKAH VENTURES SDN BHD',
                    ),
                  ],
                ),
                SizedBox(height: 5),
                Container(
                  height: 2,
                  color: Color(0xFFFCE16D),
                ),
                SizedBox(height: 5),
                Text(
                  'This certificate is prepared in accordance with WHO Digital Documentation of COVID-19 Certificates: Vaccination Status (DDCC:VS) Technical Specifications and Implementation Guidance 2021',
                  style: TextStyle(
                    color: Color(0xFFDB9861),
                    fontStyle: FontStyle.italic,
                    fontSize: 11,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: GestureDetector(
                onTap: () => handleShowQrCard(),
                child: Container(child: qrImage)),
          ),
        ],
      ),
    );
  }
}

class VaccinFooterImageDetails extends StatelessWidget {
  final String text;
  final String imgPath;
  final String textName;

  const VaccinFooterImageDetails(
      {super.key,
      required this.text,
      required this.imgPath,
      required this.textName});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xFFFDDB5E),
            ),
            child: Text(
              text,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: kPrimaryColor,
                fontSize: 7,
              ),
            ),
          ),
          SizedBox(height: 5),
          Container(
            height: 22.5,
            child: Image.asset(
              imgPath,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: 5),
          Text(
            textName,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 6,
            ),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }
}
