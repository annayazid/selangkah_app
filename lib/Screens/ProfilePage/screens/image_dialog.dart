import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';

class ImageDialog extends StatefulWidget {
  final Uint8List? imgbytes;

  const ImageDialog({this.imgbytes});

  @override
  _ImageDialogState createState() => _ImageDialogState();
}

class _ImageDialogState extends State<ImageDialog> {
  XFile? _imageFile;

  Future<void> _pickImage(ImageSource source) async {
    XFile? selected = await ImagePicker().pickImage(
      source: source,
      imageQuality: 25,
    );

    if (selected != null) {
      setState(() {
        _imageFile = selected;
      });
      _cropImage();
    }
  }

  Future<void> _cropImage() async {
    CroppedFile? cropped = await ImageCropper().cropImage(
      sourcePath: _imageFile!.path,
      cropStyle: CropStyle.circle,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
      ],
      compressQuality: 25,
    );

    setState(() {
      _imageFile = XFile(cropped!.path);
    });
  }

  void _uploadImage() async {
    var base64Img = base64.encode(await _imageFile!.readAsBytes());
    // print('uploading image');

    context.read<UpdateImageCubit>().updateImage(base64Img);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 500,
        height: 500,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                backgroundColor: Colors.blueGrey,
                radius: 60,
                child: ClipOval(
                  child: _imageFile == null
                      ? widget.imgbytes == null
                          ? Image.asset(
                              'assets/images/ProfilePage/male_avatar.png')
                          : Image.memory(widget.imgbytes!)
                      : Image.file(File(_imageFile!.path)),
                ),
              ),
              SizedBox(height: 20),
              ElevatedButton(
                onPressed: () async {
                  // await SecureStorage()
                  //     .deleteSecureData('cameraProfilePicture');
                  GlobalFunction.displayDisclosure(
                    icon: Icon(
                      Icons.camera_alt_outlined,
                      color: Colors.blue,
                      size: 30,
                    ),
                    title: 'cameraProfilePictureTitle'.tr(),
                    description: 'cameraProfilePicture'.tr(),
                    key: 'cameraProfilePicture',
                  ).then((value) async {
                    if (value) {
                      if (await Permission.camera.request().isGranted) {
                        _pickImage(ImageSource.camera);
                      } else {
                        Flushbar(
                          backgroundColor: Colors.white,
                          // message: LoremText,
                          // title: 'Permission needed to continue',
                          titleText: Text(
                            'permission_continue'.tr(),
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                            ),
                          ),
                          messageText: Text(
                            'continue_permission'.tr(),
                            style: TextStyle(color: Colors.black),
                          ),
                          mainButton: TextButton(
                            onPressed: AppSettings.openAppSettings,
                            child: Text(
                              'App Settings',
                              style: TextStyle(color: Colors.blue),
                            ),
                          ),
                          duration: Duration(seconds: 7),
                        ).show(context);
                      }
                    }
                  });
                },
                child: Text(
                  'take_photo'.tr(),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  backgroundColor: kPrimaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(35),
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  GlobalFunction.displayDisclosure(
                    icon: Icon(
                      Icons.folder_open_rounded,
                      color: Colors.blue,
                      size: 30,
                    ),
                    title: 'fileProfilePictureTitle'.tr(),
                    description: 'fileProfilePicture'.tr(),
                    key: 'fileProfilePicture',
                  ).then((value) {
                    if (value) {
                      _pickImage(ImageSource.gallery);
                    }
                  });
                },
                child: Text(
                  'choose_existing_photo'.tr(),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  backgroundColor: kPrimaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(35),
                  ),
                ),
              ),
              SizedBox(height: 50),
              _imageFile == null
                  ? Container()
                  : Container(
                      width: 100,
                      child: ElevatedButton(
                        onPressed: () => _uploadImage(),
                        child: BlocConsumer<UpdateImageCubit, UpdateImageState>(
                          listener: (context, state) {
                            if (state is UpdateImageFinish) {
                              Navigator.of(context).pop();
                            }
                          },
                          builder: (context, state) {
                            if (state is UpdateImageInitial) {
                              return Text(
                                'done'.tr(),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                ),
                              );
                            } else if (state is UpdateImageLoading) {
                              return UpdateProfileLoadingWidget();
                            } else {
                              return Text(
                                'done'.tr(),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                ),
                              );
                            }
                          },
                        ),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(35),
                          ),
                        ),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}

class UpdateProfileLoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SpinKitFadingCircle(
      color: Colors.white,
      size: 20,
    );
  }
}
