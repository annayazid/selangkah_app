import 'dart:io';

import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getwidget/getwidget.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/Screens/WebDownload/web_download.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';

class ResultsAccordian extends StatefulWidget {
  @override
  _ResultsAccordianState createState() => _ResultsAccordianState();
}

class _ResultsAccordianState extends State<ResultsAccordian> {
  bool accordiance = false;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      width: width * 0.85,
      decoration: BoxDecoration(
        // borderRadius: BorderRadius.circular(10),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20)),
        color: Color(0xFF7B80F5),
        // boxShadow: [
        //   BoxShadow(
        //     color: Colors.grey,
        //     offset: Offset(0.0, 1.0), //(x,y)
        //     blurRadius: 4.0,
        //   ),
        // ],
      ),
      child: Column(
        children: [
          SizedBox(height: 8),
          GFAccordion(
            collapsedTitleBackgroundColor: Color(0xFFF6F6F9),
            expandedTitleBackgroundColor: Color(0xFFF6F6F9),
            contentPadding: EdgeInsets.all(15),
            titlePadding: EdgeInsets.all(15),
            onToggleCollapsed: (bool collapse) {
              setState(() {
                accordiance = collapse;
                print(collapse);
              });
            },
            titleBorderRadius: BorderRadius.only(
              topLeft: Radius.circular(0),
              topRight: Radius.circular(0),
              bottomLeft:
                  (accordiance) ? Radius.circular(0) : Radius.circular(10),
              bottomRight:
                  (accordiance) ? Radius.circular(0) : Radius.circular(10),
            ),
            contentBorderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10),
            ),
            margin: EdgeInsets.zero,
            titleChild: Text(
              'test_results'.tr(),
              style: TextStyle(
                color: Color(0xFF54586C),
                fontSize: accordiance ? 16 : 15,
                // fontWeight: accordiance ? FontWeight.bold : FontWeight.normal,
                // color: accordiance[index] ? Colors.white : Colors.black,
              ),
            ),
            contentChild: Column(
              children: [
                BlocBuilder<TestResultCubit, TestResultState>(
                  builder: (context, state) {
                    if (state is TestResultLoaded) {
                      if (state.labResult.data!.length != 0) {
                        return ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: state.labResult.data!.length,
                            itemBuilder: (BuildContext context, int index) {
                              List<String> splitted = state
                                  .labResult.data![index].testName!
                                  .split('.');

                              print(splitted);

                              String formatFile = splitted.last;

                              List<String> splittedResult =
                                  splitted.sublist(0, splitted.length - 1);

                              String resultFileName = splittedResult.join('.');
                              print('Disini!');
                              print(resultFileName);
                              print(formatFile);

                              return Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 10),
                                width: double.infinity,
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      // crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Stack(
                                          children: [
                                            if (state.labResult.data![index]
                                                    .canDelete ==
                                                0)
                                              Image.asset(
                                                  "assets/images/lhr/pdf.png",
                                                  width: 40,
                                                  height: 40),
                                            if (state.labResult.data![index]
                                                    .canDelete ==
                                                1)
                                              formatFile == 'pdf' ||
                                                      formatFile == 'PDF'
                                                  ? Image.asset(
                                                      "assets/images/lhr/pdf.png",
                                                      width: 40,
                                                      height: 40)
                                                  : Image.asset(
                                                      "assets/images/lhr/jpg.png",
                                                      width: 40,
                                                      height: 40),
                                            if (state.labResult.data![index]
                                                    .redFlag ==
                                                '1')
                                              Positioned(
                                                bottom: 0,
                                                right: 0,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Colors.white,
                                                  ),
                                                  child: Icon(
                                                    FontAwesomeIcons
                                                        .circleExclamation,
                                                    size: 20,
                                                    color: Color(0xFFEF0056),
                                                  ),
                                                ),
                                              ),
                                          ],
                                        ),
                                        // Image.asset("assets/images/lhr/jpg.png",
                                        //     width: 40, height: 40),
                                        SizedBox(width: 10),
                                        Expanded(
                                          child: Container(
                                            // padding: EdgeInsets.symmetric(vertical: 10),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  state.labResult.data![index]
                                                              .canDelete ==
                                                          0
                                                      ? state
                                                          .labResult
                                                          .data![index]
                                                          .testName!
                                                      : resultFileName,
                                                  style: TextStyle(
                                                    color: Color(0xFF54586C),
                                                  ),
                                                ),
                                                SizedBox(height: 10),
                                                Text(
                                                  formatDate(
                                                      state.labResult
                                                          .data![index].date!,
                                                      [
                                                        hh,
                                                        ':',
                                                        nn,
                                                        ' ',
                                                        am,
                                                        ', ',
                                                        MM,
                                                        ' ',
                                                        dd,
                                                        ', ',
                                                        yyyy
                                                      ]),
                                                  style: TextStyle(
                                                    fontSize: 11,
                                                    fontWeight: FontWeight.bold,
                                                    color: Color(0xFFAFB2C3),
                                                  ),
                                                ),
                                                // SizedBox(height: 10),
                                                // if (widget.getAppointment.data[indexAppointment]
                                                //         .canChange ==
                                                //     1)
                                              ],
                                            ),
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        if (state.labResult.data![index]
                                                .findings !=
                                            '')
                                          GestureDetector(
                                            onTap: () {
                                              showDialog(
                                                context: context,
                                                builder: (_) {
                                                  return AlertDialog(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20),
                                                    ),
                                                    scrollable: true,
                                                    content:
                                                        SingleChildScrollView(
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        // mainAxisSize:
                                                        //     MainAxisSize.min,
                                                        children: [
                                                          Center(
                                                            child: Text(
                                                              state
                                                                  .labResult
                                                                  .data![index]
                                                                  .testName!,
                                                              style: TextStyle(
                                                                color: Color(
                                                                    0xFF505660),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 16,
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(height: 15),
                                                          Text(
                                                            'dr_comment'.tr(),
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xFF505660),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                            textAlign:
                                                                TextAlign.left,
                                                          ),
                                                          SizedBox(height: 10),
                                                          Text(
                                                            state
                                                                .labResult
                                                                .data![index]
                                                                .findings!,
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xFF898DA1),
                                                              fontStyle:
                                                                  FontStyle
                                                                      .italic,
                                                            ),
                                                            textAlign:
                                                                TextAlign.left,
                                                          ),
                                                          SizedBox(height: 10),
                                                          Center(
                                                            child:
                                                                ElevatedButton(
                                                              style:
                                                                  ElevatedButton
                                                                      .styleFrom(
                                                                shape: RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            20)),
                                                                backgroundColor:
                                                                    Color(
                                                                        0xFFEF0056),
                                                              ),
                                                              onPressed: () {
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              },
                                                              child: Text(
                                                                'done'.tr(),
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                },
                                              );
                                            },
                                            child: Image.asset(
                                                "assets/images/lhr/comment.png",
                                                width: 33,
                                                height: 33),
                                          ),
                                        SizedBox(width: 5),
                                        PopupMenuButton(
                                          offset: Offset(0, 25),
                                          padding: EdgeInsets.all(10),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          itemBuilder: (context) => [
                                            if (state.labResult.data![index]
                                                    .canDelete ==
                                                0) ...[
                                              if (state.labResult.data![index]
                                                      .url !=
                                                  '')
                                                PopupMenuItem(
                                                    value: 1,
                                                    child: Row(
                                                      children: [
                                                        Text(
                                                            'lab_results'.tr()),
                                                        Spacer(),
                                                        Image.asset(
                                                            "assets/images/lhr/result.png",
                                                            width: 15,
                                                            height: 15),
                                                      ],
                                                    )),
                                              if (state.labResult.data![index]
                                                      .pdfDocu !=
                                                  '')
                                                PopupMenuItem(
                                                    value: 2,
                                                    child: Row(
                                                      children: [
                                                        Text('dr_report'.tr()),
                                                        Spacer(),
                                                        Image.asset(
                                                            "assets/images/lhr/report.png",
                                                            width: 15,
                                                            height: 15)
                                                      ],
                                                    )),
                                            ],
                                            if (state.labResult.data![index]
                                                    .canDelete ==
                                                1)
                                              PopupMenuItem(
                                                  value: 3,
                                                  child: Row(
                                                    children: [
                                                      Text('swabook_download'
                                                          .tr()),
                                                      Spacer(),
                                                      Image.asset(
                                                          "assets/images/lhr/download.png",
                                                          width: 15,
                                                          height: 15)
                                                    ],
                                                  )),
                                            PopupMenuItem(
                                                value: 4,
                                                child: Row(
                                                  children: [
                                                    Text('archive'.tr()),
                                                    Spacer(),
                                                    Image.asset(
                                                        "assets/images/lhr/archive.png",
                                                        width: 15,
                                                        height: 15)
                                                  ],
                                                )),
                                            PopupMenuItem(
                                                value: 5,
                                                child: Row(
                                                  children: [
                                                    Text('delete'.tr()),
                                                    Spacer(),
                                                    Image.asset(
                                                        "assets/images/lhr/delete.png",
                                                        width: 15,
                                                        height: 15)
                                                  ],
                                                )),
                                            PopupMenuItem(
                                                value: 6,
                                                child: Row(
                                                  children: [
                                                    Text('share'.tr()),
                                                    Spacer(),
                                                    Image.asset(
                                                        "assets/images/lhr/share.png",
                                                        width: 15,
                                                        height: 15)
                                                  ],
                                                )),
                                          ],
                                          onSelected: (value) async {
                                            if (value == 1) {
                                              String url = await LhrRepositories
                                                  .downloadTestDoc(state
                                                      .labResult
                                                      .data![index]
                                                      .url!);
                                              downloadLabResult(context, url);
                                            }
                                            if (value == 2) {
                                              String url = state.labResult
                                                  .data![index].pdfDocu!;
                                              downloadReport(context, url);
                                            }
                                            if (value == 3) {
                                              download(context, index,
                                                  state.labResult);
                                            }
                                            if (value == 4) {
                                              bool archive =
                                                  await LhrRepositories
                                                      .archiveDoc(
                                                          idSaring: state
                                                              .labResult
                                                              .data![index]
                                                              .idDataSaring);

                                              if (archive) {
                                                print('record archived');
                                                Fluttertoast.showToast(
                                                    msg:
                                                        'record_archived'.tr());
                                                context
                                                    .read<TestResultCubit>()
                                                    .getTestResult();
                                                context
                                                    .read<ArchivedRecordCubit>()
                                                    .getArchivedRecord();
                                              } else {
                                                Fluttertoast.showToast(
                                                    msg: 'try_again'.tr());
                                              }
                                            }
                                            if (value == 5) {
                                              if (state.labResult.data![index]
                                                      .canDelete ==
                                                  1)
                                                showDialog(
                                                    context: context,
                                                    builder: (_) => AlertDialog(
                                                          shape:
                                                              RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        20),
                                                          ),
                                                          scrollable: true,
                                                          content:
                                                              SingleChildScrollView(
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              // mainAxisSize:
                                                              //     MainAxisSize.min,
                                                              children: [
                                                                Center(
                                                                    child: Icon(
                                                                  FontAwesomeIcons
                                                                      .trashCan,
                                                                  color: Color(
                                                                      0xFF505660),
                                                                )),
                                                                SizedBox(
                                                                    height: 15),
                                                                Center(
                                                                  child: Text(
                                                                    'delete_file'
                                                                        .tr(),
                                                                    style:
                                                                        TextStyle(
                                                                      color: Color(
                                                                          0xFF505660),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontSize:
                                                                          16,
                                                                    ),
                                                                  ),
                                                                ),
                                                                SizedBox(
                                                                    height: 15),
                                                                Center(
                                                                  child: Text(
                                                                    'delete_file_desc'
                                                                        .tr(),
                                                                    style:
                                                                        TextStyle(
                                                                      color: Color(
                                                                          0xFF898DA1),
                                                                    ),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .center,
                                                                  ),
                                                                ),
                                                                SizedBox(
                                                                    height: 15),
                                                                Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    Expanded(
                                                                      child:
                                                                          ElevatedButton(
                                                                        style: ElevatedButton
                                                                            .styleFrom(
                                                                          shape:
                                                                              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                                                          backgroundColor:
                                                                              Colors.white,
                                                                        ),
                                                                        onPressed:
                                                                            () {
                                                                          Navigator.of(context)
                                                                              .pop();
                                                                        },
                                                                        child:
                                                                            Text(
                                                                          'cancel'
                                                                              .tr(),
                                                                          style:
                                                                              TextStyle(
                                                                            color:
                                                                                Color(0xFF505660),
                                                                            fontWeight:
                                                                                FontWeight.bold,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    SizedBox(
                                                                        width:
                                                                            15),
                                                                    Expanded(
                                                                      child:
                                                                          ElevatedButton(
                                                                        style: ElevatedButton
                                                                            .styleFrom(
                                                                          shape:
                                                                              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                                                          backgroundColor:
                                                                              Color(0xFFEF0056),
                                                                        ),
                                                                        onPressed:
                                                                            () async {
                                                                          bool
                                                                              delete =
                                                                              await LhrRepositories.deleteDoc(idSaring: state.labResult.data![index].idDataSaring);

                                                                          if (delete) {
                                                                            print('record deleted');
                                                                            Fluttertoast.showToast(msg: 'record_deleted'.tr());
                                                                            context.read<TestResultCubit>().getTestResult();
                                                                          } else {
                                                                            Fluttertoast.showToast(msg: 'try_again'.tr());
                                                                          }

                                                                          Navigator.of(context)
                                                                              .pop();
                                                                        },
                                                                        child:
                                                                            Text(
                                                                          'delete'
                                                                              .tr(),
                                                                          style:
                                                                              TextStyle(
                                                                            color:
                                                                                Colors.white,
                                                                            fontWeight:
                                                                                FontWeight.bold,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ));

                                              if (state.labResult.data![index]
                                                      .canDelete ==
                                                  0)
                                                showDialog(
                                                    context: context,
                                                    builder: (_) => AlertDialog(
                                                          shape:
                                                              RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        20),
                                                          ),
                                                          scrollable: true,
                                                          content: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            // mainAxisSize:
                                                            //     MainAxisSize.min,
                                                            children: [
                                                              Center(
                                                                  child: Icon(
                                                                FontAwesomeIcons
                                                                    .lock,
                                                                color: Color(
                                                                    0xFF505660),
                                                              )),
                                                              SizedBox(
                                                                  height: 15),
                                                              Center(
                                                                child: Text(
                                                                  'cant_delete_file'
                                                                      .tr(),
                                                                  style:
                                                                      TextStyle(
                                                                    color: Color(
                                                                        0xFF505660),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        16,
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                  height: 15),
                                                              Center(
                                                                child: Text(
                                                                  'cant_delete_file_desc'
                                                                      .tr(),
                                                                  style:
                                                                      TextStyle(
                                                                    color: Color(
                                                                        0xFF898DA1),
                                                                  ),
                                                                  textAlign:
                                                                      TextAlign
                                                                          .center,
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                  height: 15),
                                                              Center(
                                                                child:
                                                                    ElevatedButton(
                                                                  style: ElevatedButton
                                                                      .styleFrom(
                                                                    shape: RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(20)),
                                                                    backgroundColor:
                                                                        Color(
                                                                            0xFFEF0056),
                                                                  ),
                                                                  onPressed:
                                                                      () {
                                                                    Navigator.of(
                                                                            context)
                                                                        .pop();
                                                                  },
                                                                  child: Text(
                                                                    'Okay'.tr(),
                                                                    style:
                                                                        TextStyle(
                                                                      color: Colors
                                                                          .white,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ));
                                            }
                                            if (value == 6) {
                                              showDialog(
                                                context: context,
                                                builder: (_) => BlocProvider(
                                                  create: (context) =>
                                                      ShareCodeCubit(),
                                                  child: ShareCodeDialog(
                                                    idDoc: null,
                                                    idDataSaring: state
                                                        .labResult
                                                        .data![index]
                                                        .idDataSaring!,
                                                  ),
                                                ),
                                              );
                                            }
                                          },
                                          child: Icon(
                                            // FontAwesomeIcons.archive,
                                            Icons.more_horiz,
                                            color: Color(0xFF505660),
                                            size: 30,
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 10),
                                    Divider(color: Colors.grey)
                                  ],
                                ),
                              );
                            });
                      } else {
                        return Center(
                          child: Column(
                            children: [
                              SizedBox(height: 10),
                              Text(
                                'no_record'.tr(),
                                style: TextStyle(
                                  color: Color(0xFFA9ABB5),
                                ),
                              ),
                              SizedBox(height: 10),
                            ],
                          ),
                        );
                      }
                    } else {
                      return SpinKitFadingCircle(
                        size: 15,
                        color: Colors.red,
                      );
                    }
                  },
                ),
                SizedBox(height: 10),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    backgroundColor: Color(0xFFEF0056),
                  ),
                  onPressed: () async {
                    bool status;
                    if (Platform.isIOS) {
                      status = await Permission.photos.request().isGranted;
                    } else {
                      status = await Permission.storage.request().isGranted;
                    }

                    if (status) {
                      await Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => UploadDocumentScreen(
                            selectedDocumentType: 4,
                            selectedDocumentTypeName: 'test_results'.tr(),
                          ),
                        ),
                      );

                      context.read<TestResultCubit>().getTestResult();
                    } else {
                      Alert(
                        onWillPopActive: false,
                        context: context,
                        type: AlertType.info,
                        title: "permission_needed".tr(),
                        desc: 'storage_permission_needed'.tr(),
                        buttons: [
                          DialogButton(
                            child: Text(
                              'open_settings_storage'.tr(),
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                            onPressed: AppSettings.openAppSettings,
                            color: kPrimaryColor,
                            radius: BorderRadius.circular(20),
                          )
                        ],
                      ).show();
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/images/lhr/uploadcloud.png',
                          width: 15),
                      SizedBox(width: 10),
                      Text(
                        'upload_test_result'.tr(),
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            contentBackgroundColor: Color(0xFFF6F6F9),
            collapsedIcon: Icon(
              FontAwesomeIcons.chevronDown,
              color: Color(0xFF979BAC),
              size: 14,
              // color: accordiance[index] ? Colors.white : Colors.black,
            ),
            expandedIcon: Icon(
              FontAwesomeIcons.chevronUp,
              color: Color(0xFF979BAC),
              size: 14,
              // color: accordiance[index] ? Colors.white : Colors.black,
            ),
          ),
        ],
      ),
    );
  }

  downloadReport(BuildContext context, String url) async {
    GlobalFunction.displayDisclosure(
      icon: Icon(
        Icons.camera_alt_outlined,
        color: Colors.blue,
        size: 30,
      ),
      title: 'fileProfilePictureTitle'.tr(),
      description: 'fileDownloadStorage'.tr(),
      key: 'fileDownloadStorage',
    ).then((value) async {
      if (value) {
        if (await Permission.storage.request().isGranted) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => WebDownload(
                url: url,
              ),
            ),
          );
        } else {
          Flushbar(
            backgroundColor: Colors.white,
            // message: LoremText,
            // title: 'Permission needed to continue',
            titleText: Text(
              'permission_continue'.tr(),
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
            messageText: Text(
              'continue_permission'.tr(),
              style: TextStyle(color: Colors.black),
            ),
            mainButton: TextButton(
              onPressed: AppSettings.openAppSettings,
              child: Text(
                'App Settings',
                style: TextStyle(color: Colors.blue),
              ),
            ),
            duration: Duration(seconds: 7),
          ).show(context);
        }
      }
    });
  }

  downloadLabResult(BuildContext context, String url) async {
    GlobalFunction.displayDisclosure(
      icon: Icon(
        Icons.camera_alt_outlined,
        color: Colors.blue,
        size: 30,
      ),
      title: 'fileProfilePictureTitle'.tr(),
      description: 'fileDownloadStorage'.tr(),
      key: 'fileDownloadStorage',
    ).then((value) async {
      if (value) {
        if (await Permission.storage.request().isGranted) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => WebDownload(
                url: url,
              ),
            ),
          );
        } else {
          Flushbar(
            backgroundColor: Colors.white,
            // message: LoremText,
            // title: 'Permission needed to continue',
            titleText: Text(
              'permission_continue'.tr(),
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
            messageText: Text(
              'continue_permission'.tr(),
              style: TextStyle(color: Colors.black),
            ),
            mainButton: TextButton(
              onPressed: AppSettings.openAppSettings,
              child: Text(
                'App Settings',
                style: TextStyle(color: Colors.blue),
              ),
            ),
            duration: Duration(seconds: 7),
          ).show(context);
        }
      }
    });
  }

  download(BuildContext context, int indexResult, LabResult labResult) async {
    GlobalFunction.displayDisclosure(
      icon: Icon(
        Icons.camera_alt_outlined,
        color: Colors.blue,
        size: 30,
      ),
      title: 'fileProfilePictureTitle'.tr(),
      description: 'fileDownloadStorage'.tr(),
      key: 'fileDownloadStorage',
    ).then((value) async {
      if (value) {
        if (await Permission.storage.request().isGranted) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => WebDownload(
                url: labResult.data![indexResult].url!,
              ),
            ),
          );
        } else {
          Flushbar(
            backgroundColor: Colors.white,
            // message: LoremText,
            // title: 'Permission needed to continue',
            titleText: Text(
              'permission_continue'.tr(),
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
            messageText: Text(
              'continue_permission'.tr(),
              style: TextStyle(color: Colors.black),
            ),
            mainButton: TextButton(
              onPressed: AppSettings.openAppSettings,
              child: Text(
                'App Settings',
                style: TextStyle(color: Colors.blue),
              ),
            ),
            duration: Duration(seconds: 7),
          ).show(context);
        }
      }
    });
  }
}
