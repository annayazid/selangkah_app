import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getwidget/getwidget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';

class PhysicalDataAccordian extends StatefulWidget {
  @override
  _PhysicalDataAccordianState createState() => _PhysicalDataAccordianState();
}

class _PhysicalDataAccordianState extends State<PhysicalDataAccordian> {
  bool accordiance = false;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width * 0.85,
      decoration: BoxDecoration(
        // borderRadius: BorderRadius.circular(10),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20)),
        color: Color(0xFFFF4C34),
        // boxShadow: [
        //   BoxShadow(
        //     color: Colors.grey,
        //     offset: Offset(0.0, 1.0), //(x,y)
        //     blurRadius: 4.0,
        //   ),
        // ],
      ),
      child: Column(
        children: [
          SizedBox(height: 8),
          GFAccordion(
            collapsedTitleBackgroundColor: Color(0xFFF6F6F9),
            expandedTitleBackgroundColor: Color(0xFFF6F6F9),
            contentPadding: EdgeInsets.all(15),
            titlePadding: EdgeInsets.all(15),
            onToggleCollapsed: (bool collapse) {
              setState(() {
                accordiance = collapse;
              });
            },
            titleBorderRadius: BorderRadius.only(
              topLeft: Radius.circular(0),
              topRight: Radius.circular(0),
              bottomLeft:
                  (accordiance) ? Radius.circular(0) : Radius.circular(10),
              bottomRight:
                  (accordiance) ? Radius.circular(0) : Radius.circular(10),
            ),
            contentBorderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10),
            ),
            margin: EdgeInsets.zero,
            titleChild: Row(
              children: [
                Text(
                  'physical_data'.tr(),
                  style: TextStyle(
                    color: Color(0xFF54586C),
                    fontSize: accordiance ? 16 : 15,
                    // fontWeight:
                    // accordiance ? FontWeight.bold : FontWeight.normal,
                    // color: accordiance[index] ? Colors.white : Colors.black,
                  ),
                ),
                SizedBox(width: 5),
                if (!accordiance)
                  BlocBuilder<HealthRecordCubit, HealthRecordState>(
                    builder: (context, state) {
                      if (state is HealthRecordLoaded) {
                        return ExclamationMark(
                          physicalRecord: state.physicalRecord,
                        );
                      } else {
                        return Container();
                      }
                    },
                  )
              ],
            ),
            // contentChild: PhysicalDataRecord(
            //   physicalRecord: widget.widget.physicalRecord,
            //   from: 'profile',
            // ),
            contentChild: Column(
              children: [
                BlocBuilder<HealthRecordCubit, HealthRecordState>(
                  builder: (context, state) {
                    if (state is HealthRecordLoaded) {
                      return ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: state.physicalRecord.data!.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Column(
                            children: [
                              GestureDetector(
                                onTap: () async {
                                  await Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) => MultiBlocProvider(
                                        providers: [
                                          BlocProvider(
                                            create: (context) =>
                                                RecordTrendCubit(),
                                          ),
                                          BlocProvider(
                                            create: (context) =>
                                                DeleteRecordCubit(),
                                          ),
                                          BlocProvider(
                                            create: (context) =>
                                                DropdownMeasurementCubit(),
                                          ),
                                        ],
                                        child: RecordTrend(
                                            parameter: state.physicalRecord
                                                .data![index]!.parameter,
                                            title: state.physicalRecord
                                                .data![index]!.title,
                                            unit: state.physicalRecord
                                                .data![index]!.unit,
                                            image: state.physicalRecord
                                                .data![index]!.logo,
                                            color: int.parse(
                                                '0xFF${state.physicalRecord.data![index]!.color}')),
                                      ),
                                    ),
                                  );

                                  context
                                      .read<HealthRecordCubit>()
                                      .getHealthRecord();
                                },
                                child: Container(
                                  // padding: EdgeInsets.all(10),
                                  // color: Colors.grey,
                                  child: Row(
                                    children: [
                                      Stack(
                                        children: [
                                          Container(
                                            padding: EdgeInsets.all(5),
                                            decoration: BoxDecoration(
                                                color: Color(0xFFEBEDF3),
                                                shape: BoxShape.circle),
                                            child: Image.network(
                                              state.physicalRecord.data![index]!
                                                  .logo!,
                                              width: 40,
                                              height: 40,
                                            ),
                                          ),
                                          if (state.physicalRecord.data![index]!
                                                  .statusWarning ==
                                              1)
                                            Positioned(
                                                bottom: 0,
                                                right: 0,
                                                child: Image.asset(
                                                    "assets/images/lhr/alert.png",
                                                    width: 15,
                                                    height: 15)),
                                          if (state.physicalRecord.data![index]!
                                                  .statusWarning ==
                                              2)
                                            Positioned(
                                                bottom: 0,
                                                right: 0,
                                                child: Image.asset(
                                                    "assets/images/lhr/exclamation.png",
                                                    width: 15,
                                                    height: 15)),
                                        ],
                                      ),
                                      SizedBox(width: 10),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              state.physicalRecord.data![index]!
                                                  .title!,
                                              style: TextStyle(
                                                color: Color(0xFFAFB2C3),
                                                fontSize: 11,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                            SizedBox(height: 5),
                                            Text(
                                              state.physicalRecord.data![index]!
                                                  .dataInput!,
                                              style: TextStyle(
                                                color: Color(0xFF54586C),
                                                fontSize: 13,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Icon(
                                            Icons.navigate_next_sharp,
                                            color: Color(0xFFC7C9D1),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 10),
                            ],
                          );
                        },
                      );
                    } else {
                      return SpinKitFadingCircle(
                        size: 15,
                        color: Colors.red,
                      );
                    }
                  },
                ),
                SizedBox(height: 10),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    backgroundColor: Color(0xFFEF0056),
                  ),
                  onPressed: () async {
                    await context
                        .read<DropdownMeasurementCubit>()
                        .getDropdownMeasurement();

                    //flag from profile page
                    // context
                    //     .read<SliderBloc>()
                    //     .add(GetCardDetails('saring_result'));
                  },
                  child: BlocConsumer<DropdownMeasurementCubit,
                      DropdownMeasurementState>(
                    listener: (context, state) async {
                      if (state is DropdownMeasurementLoaded) {
                        await Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => MultiBlocProvider(
                              providers: [
                                BlocProvider(
                                  create: (context) => InputMeasurementCubit(),
                                ),
                                BlocProvider(
                                  create: (context) => ReturnAnswerCubit(),
                                ),
                              ],
                              child: AddMeasurement(
                                parameter: 'Weight',
                                from: 'Physical Data Page',
                                dropdown: state.dropdown,
                              ),
                            ),
                          ),
                        );
                        context.read<HealthRecordCubit>().getHealthRecord();
                      }
                    },
                    builder: (context, state) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            FontAwesomeIcons.plus,
                            size: 15,
                            color: Colors.white,
                          ),
                          SizedBox(width: 10),
                          Text(
                            'add_data'.tr(),
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ],
            ),
            contentBackgroundColor: Color(0xFFF6F6F9),
            collapsedIcon: Icon(
              FontAwesomeIcons.chevronDown,
              color: Color(0xFF979BAC),
              size: 14,
              // color: accordiance[index] ? Colors.white : Colors.black,
            ),
            expandedIcon: Icon(
              FontAwesomeIcons.chevronUp,
              color: Color(0xFF979BAC),
              size: 14,
              // color: accordiance[index] ? Colors.white : Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}

class ExclamationMark extends StatefulWidget {
  final PhysicalRecord physicalRecord;

  const ExclamationMark({super.key, required this.physicalRecord});

  @override
  State<ExclamationMark> createState() => _ExclamationMarkState();
}

class _ExclamationMarkState extends State<ExclamationMark> {
  bool message = false;
  @override
  void initState() {
    if (widget.physicalRecord.data!.length != 0) {
      for (var i = 0; i < widget.physicalRecord.data!.length; i++) {
        if (widget.physicalRecord.data![i]!.statusWarning == 2) {
          message = true;
        }
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (message) {
      return Image.asset("assets/images/lhr/exclamation.png",
          width: 20, height: 20);
    } else {
      return Container();
    }
  }
}
