import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:date_format/date_format.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getwidget/getwidget.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';
import 'package:selangkah_new/Screens/WebDownload/web_download.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:url_launcher/url_launcher.dart';

class AppointmentAccordian extends StatefulWidget {
  @override
  _AppointmentAccordianState createState() => _AppointmentAccordianState();
}

class _AppointmentAccordianState extends State<AppointmentAccordian> {
  void showPopUpDialog() {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          contentPadding: EdgeInsets.symmetric(
            horizontal: 30,
            vertical: 25,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          scrollable: true,
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'hospital_notify'.tr(),
                style: TextStyle(
                  color: Color(0xFF898DA1),
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  backgroundColor: Color(0xFFEF0056),
                ),
                onPressed: () async {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'Okay',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    ).then((value) => context.read<AppointmentCubit>().getAppointment());
  }

  bool accordiance = false;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width * 0.85,
      decoration: BoxDecoration(
        // borderRadius: BorderRadius.circular(10),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20)),
        color: Color(0xFFCD37C7),
        // boxShadow: [
        //   BoxShadow(
        //     color: Colors.grey,
        //     offset: Offset(0.0, 1.0), //(x,y)
        //     blurRadius: 4.0,
        //   ),
        // ],
      ),
      child: Column(
        children: [
          SizedBox(height: 8),
          BlocBuilder<AppointmentCubit, AppointmentState>(
            builder: (context, state) {
              if (state is AppointmentLoaded) {
                return GFAccordion(
                  collapsedTitleBackgroundColor: Color(0xFFF6F6F9),
                  expandedTitleBackgroundColor: Color(0xFFF6F6F9),
                  contentPadding: EdgeInsets.all(15),
                  titlePadding: EdgeInsets.all(15),
                  onToggleCollapsed: (bool collapse) {
                    setState(() {
                      accordiance = collapse;
                    });
                  },
                  titleBorderRadius: BorderRadius.only(
                    topLeft: Radius.circular(0),
                    topRight: Radius.circular(0),
                    bottomLeft: (accordiance)
                        ? Radius.circular(0)
                        : Radius.circular(10),
                    bottomRight: (accordiance)
                        ? Radius.circular(0)
                        : Radius.circular(10),
                  ),
                  contentBorderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                  margin: EdgeInsets.zero,
                  titleChild: Row(
                    children: [
                      Text(
                        'appointments'.tr(),
                        style: TextStyle(
                          color: Color(0xFF54586C),
                          fontSize: accordiance ? 16 : 15,
                          // fontWeight: accordiance ? FontWeight.bold : FontWeight.normal,
                          // color: accordiance[index] ? Colors.white : Colors.black,
                        ),
                      ),
                      SizedBox(width: 5),
                      if (state.bookAppointment.data!.length != 0)
                        if (!accordiance)
                          Image.asset(
                            "assets/images/lhr/alert.png",
                            width: 20,
                            height: 20,
                          ),
                    ],
                  ),
                  contentChild: state.bookAppointment.data!.length != 0 ||
                          state.listAppointment.data!.length != 0
                      ? Column(
                          children: [
                            if (state.bookAppointment.data!.length != 0)
                              ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: state.bookAppointment.data!.length,
                                  itemBuilder: (BuildContext _, int index) {
                                    return Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      width: double.infinity,
                                      child: Column(
                                        // crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Image.asset(
                                                "assets/images/lhr/alert.png",
                                                width: 20,
                                                height: 20,
                                              ),
                                              SizedBox(width: 10),
                                              Expanded(
                                                child: Text(
                                                  state
                                                      .bookAppointment
                                                      .data![index]
                                                      .serviceName!,
                                                  style: TextStyle(
                                                    color: Color(0xFF505660),
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(height: 15),
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20)),
                                              backgroundColor:
                                                  Color(0xFFEF0056),
                                            ),
                                            onPressed: () {
                                              if (state
                                                      .bookAppointment
                                                      .data![index]
                                                      .canWarning ==
                                                  1) {
                                                showDialog(
                                                  // barrierDismissible: false,
                                                  context: context,
                                                  builder: (_) =>
                                                      ConfirmationPopUp(),
                                                ).then((value) {
                                                  if (value ?? false) {
                                                    //navigate to book slot saring
                                                    Navigator.of(context)
                                                        .push(
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            MultiBlocProvider(
                                                          providers: [
                                                            BlocProvider(
                                                              create: (context) =>
                                                                  GetProviderCubit(),
                                                            ),
                                                            BlocProvider(
                                                              create: (context) =>
                                                                  GetDateTimeCubit(),
                                                            ),
                                                            BlocProvider(
                                                              create: (context) =>
                                                                  CreateAppointmentCubit(),
                                                            ),
                                                          ],
                                                          child: BookSlot(
                                                            showDate: state
                                                                .bookAppointment
                                                                .data![index]
                                                                .showDate!,
                                                            id: state
                                                                .bookAppointment
                                                                .data![index]
                                                                .testid!,
                                                            service: state
                                                                .bookAppointment
                                                                .data![index]
                                                                .serviceName!,
                                                            isCreateAppointment:
                                                                true,
                                                            idProgramAppointment:
                                                                '',
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                        // showDialog(
                                                        //   // barrierDismissible: false,
                                                        //   context: context,
                                                        //   builder: (_) => BookSlotPopup(),
                                                        // )
                                                        .then((value) {
                                                      if (value == null) {
                                                        context
                                                            .read<
                                                                AppointmentCubit>()
                                                            .getAppointment();
                                                      } else {
                                                        showPopUpDialog();
                                                      }
                                                    });
                                                  }
                                                });
                                              } else {
                                                Navigator.of(context)
                                                    .push(
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        MultiBlocProvider(
                                                      providers: [
                                                        BlocProvider(
                                                          create: (context) =>
                                                              GetProviderCubit(),
                                                        ),
                                                        BlocProvider(
                                                          create: (context) =>
                                                              GetDateTimeCubit(),
                                                        ),
                                                        BlocProvider(
                                                          create: (context) =>
                                                              CreateAppointmentCubit(),
                                                        ),
                                                      ],
                                                      child: BookSlot(
                                                        showDate: state
                                                            .bookAppointment
                                                            .data![index]
                                                            .showDate!,
                                                        id: state
                                                            .bookAppointment
                                                            .data![index]
                                                            .testid!,
                                                        service: state
                                                            .bookAppointment
                                                            .data![index]
                                                            .serviceName!,
                                                        isCreateAppointment:
                                                            true,
                                                        idProgramAppointment:
                                                            '',
                                                      ),
                                                    ),
                                                  ),
                                                )
                                                    .then((value) {
                                                  if (value == null) {
                                                    context
                                                        .read<
                                                            AppointmentCubit>()
                                                        .getAppointment();
                                                  } else {
                                                    showPopUpDialog();
                                                  }
                                                });
                                              }
                                            },
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Icon(
                                                  FontAwesomeIcons.calendar,
                                                  size: 15,
                                                  color: Colors.white,
                                                ),
                                                SizedBox(width: 10),
                                                Text(
                                                  'book_app'.tr(),
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              shape: RoundedRectangleBorder(
                                                  side: BorderSide(
                                                    color: Color(0xFFEF0056),
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20)),
                                              backgroundColor: Colors.white,
                                            ),
                                            onPressed: () {
                                              showDialog(
                                                  context: context,
                                                  builder: (_) => BlocProvider(
                                                        create: (context) =>
                                                            AppointmentOptionCubit(),
                                                        child: NotInterestPopup(
                                                            testId: state
                                                                .bookAppointment
                                                                .data![index]
                                                                .testid!),
                                                      )).then((value) {
                                                if (value) {
                                                  context
                                                      .read<AppointmentCubit>()
                                                      .getAppointment();
                                                }
                                              });
                                            },
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                  'not_interest'.tr(),
                                                  style: TextStyle(
                                                    color: Color(0xFFEF0056),
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              shape: RoundedRectangleBorder(
                                                  side: BorderSide(
                                                    color: Color(0xFFEF0056),
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20)),
                                              backgroundColor: Colors.white,
                                            ),
                                            onPressed: () {
                                              showDialog(
                                                  context: context,
                                                  builder: (_) => BlocProvider(
                                                        create: (context) =>
                                                            AppointmentOptionCubit(),
                                                        child: AlreadyBookedPopup(
                                                            testId: state
                                                                .bookAppointment
                                                                .data![index]
                                                                .testid!),
                                                      )).then((value) {
                                                if (value) {
                                                  context
                                                      .read<AppointmentCubit>()
                                                      .getAppointment();
                                                }
                                              });
                                            },
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                  'booked_already'.tr(),
                                                  style: TextStyle(
                                                    color: Color(0xFFEF0056),
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(height: 10),
                                          Divider(color: Colors.grey)
                                        ],
                                      ),
                                    );
                                  }),
                            if (state.listAppointment.data!.length != 0)
                              ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: state.listAppointment.data!.length,
                                  itemBuilder:
                                      (BuildContext _, int indexAppointment) {
                                    return Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      width: double.infinity,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Expanded(
                                                child: Text(
                                                  state
                                                      .listAppointment
                                                      .data![indexAppointment]
                                                      .testName!,
                                                  style: TextStyle(
                                                    color: Color(0xFF505660),
                                                    // fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(width: 10),
                                              PopupMenuButton(
                                                offset: Offset(0, 25),
                                                padding: EdgeInsets.all(10),
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20)),
                                                itemBuilder: (_) => [
                                                  if (state
                                                          .listAppointment
                                                          .data![
                                                              indexAppointment]
                                                          .canChange ==
                                                      1)
                                                    PopupMenuItem(
                                                        value: 1,
                                                        child: Row(
                                                          children: [
                                                            Text(
                                                              'reschedule'.tr(),
                                                              style: TextStyle(
                                                                  color: Color(
                                                                      0xFF1F212C)),
                                                            ),
                                                            Spacer(),
                                                            Image.asset(
                                                                "assets/images/lhr/reschedule.png",
                                                                width: 19,
                                                                height: 19)
                                                          ],
                                                        )),
                                                  if (state
                                                          .listAppointment
                                                          .data![
                                                              indexAppointment]
                                                          .referral !=
                                                      '')
                                                    PopupMenuItem(
                                                        value: 2,
                                                        child: Row(
                                                          children: [
                                                            Text('refer_letter'
                                                                .tr()),
                                                            Spacer(),
                                                            Image.asset(
                                                                "assets/images/lhr/referral.png",
                                                                width: 15,
                                                                height: 15)
                                                          ],
                                                        )),
                                                  if (state
                                                          .listAppointment
                                                          .data![
                                                              indexAppointment]
                                                          .apptCard !=
                                                      '')
                                                    PopupMenuItem(
                                                        value: 3,
                                                        child: Row(
                                                          children: [
                                                            Text(
                                                                'appointment_card'
                                                                    .tr()),
                                                            Spacer(),
                                                            Image.asset(
                                                                "assets/images/lhr/appcard.png",
                                                                width: 15,
                                                                height: 15)
                                                          ],
                                                        )),
                                                  PopupMenuItem(
                                                      value: 4,
                                                      child: Row(
                                                        children: [
                                                          Text('archive'.tr()),
                                                          Spacer(),
                                                          Image.asset(
                                                              "assets/images/lhr/archive.png",
                                                              width: 15,
                                                              height: 15)
                                                        ],
                                                      )),
                                                ],
                                                onSelected: (value) async {
                                                  if (value == 1) {
                                                    showDialog(
                                                        context: context,
                                                        builder:
                                                            (_) => AlertDialog(
                                                                  shape:
                                                                      RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            20),
                                                                  ),
                                                                  scrollable:
                                                                      true,
                                                                  content:
                                                                      SingleChildScrollView(
                                                                    child:
                                                                        Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      // mainAxisSize:
                                                                      //     MainAxisSize.min,
                                                                      children: [
                                                                        Center(
                                                                            child:
                                                                                Icon(
                                                                          FontAwesomeIcons
                                                                              .calendarDay,
                                                                          color:
                                                                              Color(0xFF505660),
                                                                        )),
                                                                        SizedBox(
                                                                            height:
                                                                                15),
                                                                        Center(
                                                                          child:
                                                                              Text(
                                                                            'confirm_changes'.tr(),
                                                                            style:
                                                                                TextStyle(
                                                                              color: Color(0xFF505660),
                                                                              fontWeight: FontWeight.bold,
                                                                              fontSize: 16,
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        SizedBox(
                                                                            height:
                                                                                15),
                                                                        Center(
                                                                          child:
                                                                              Text(
                                                                            'confirm_changes_desc'.tr(),
                                                                            style:
                                                                                TextStyle(
                                                                              color: Color(0xFF898DA1),
                                                                            ),
                                                                            textAlign:
                                                                                TextAlign.center,
                                                                          ),
                                                                        ),
                                                                        SizedBox(
                                                                            height:
                                                                                15),
                                                                        Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.center,
                                                                          children: [
                                                                            Expanded(
                                                                              child: ElevatedButton(
                                                                                style: ElevatedButton.styleFrom(
                                                                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                                                                  backgroundColor: Colors.white,
                                                                                ),
                                                                                onPressed: () {
                                                                                  Navigator.of(context).pop();
                                                                                },
                                                                                child: Text(
                                                                                  'cancel'.tr(),
                                                                                  style: TextStyle(
                                                                                    color: Color(0xFF505660),
                                                                                    fontWeight: FontWeight.bold,
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                            SizedBox(width: 15),
                                                                            Expanded(
                                                                              child: ElevatedButton(
                                                                                style: ElevatedButton.styleFrom(
                                                                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                                                                  backgroundColor: Color(0xFFEF0056),
                                                                                ),
                                                                                onPressed: () async {
                                                                                  Navigator.of(context)
                                                                                      .pushReplacement(
                                                                                    MaterialPageRoute(
                                                                                      builder: (context) => MultiBlocProvider(
                                                                                        providers: [
                                                                                          BlocProvider(
                                                                                            create: (context) => GetProviderCubit(),
                                                                                          ),
                                                                                          BlocProvider(
                                                                                            create: (context) => GetDateTimeCubit(),
                                                                                          ),
                                                                                          BlocProvider(
                                                                                            create: (context) => CreateAppointmentCubit(),
                                                                                          ),
                                                                                        ],
                                                                                        child: BookSlot(
                                                                                          showDate: state.listAppointment.data![indexAppointment].showDate!,
                                                                                          id: state.listAppointment.data![indexAppointment].testId!,
                                                                                          service: state.listAppointment.data![indexAppointment].testName!,
                                                                                          isCreateAppointment: false,
                                                                                          idProgramAppointment: state.listAppointment.data![indexAppointment].idProgramAppointment!,
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                  )
                                                                                      .then((value) async {
                                                                                    if (value == null) {
                                                                                      context.read<AppointmentCubit>().getAppointment();
                                                                                    } else {
                                                                                      showPopUpDialog();
                                                                                    }
                                                                                  });
                                                                                },
                                                                                child: Text(
                                                                                  'confirm_otp'.tr(),
                                                                                  style: TextStyle(
                                                                                    color: Colors.white,
                                                                                    fontWeight: FontWeight.bold,
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ));
                                                  }
                                                  if (value == 2) {
                                                    downloadReferral(
                                                        context,
                                                        indexAppointment,
                                                        state.listAppointment);
                                                  }
                                                  if (value == 3) {
                                                    downloadAppCard(
                                                        context,
                                                        indexAppointment,
                                                        state.listAppointment);
                                                  }
                                                  if (value == 4) {
                                                    bool archive =
                                                        await LhrRepositories
                                                            .archiveDoc(
                                                      idApp: state
                                                          .listAppointment
                                                          .data![
                                                              indexAppointment]
                                                          .idProgramAppointment,
                                                    );

                                                    if (archive) {
                                                      print(
                                                          'appointment archived');
                                                      Fluttertoast.showToast(
                                                          msg: 'record_archived'
                                                              .tr());
                                                      context
                                                          .read<
                                                              AppointmentCubit>()
                                                          .getAppointment();
                                                      context
                                                          .read<
                                                              ArchivedRecordCubit>()
                                                          .getArchivedRecord();
                                                    } else {
                                                      Fluttertoast.showToast(
                                                          msg:
                                                              'try_again'.tr());
                                                    }
                                                  }
                                                },
                                                child: Icon(
                                                  // FontAwesomeIcons.archive,
                                                  Icons.more_horiz,
                                                  color: Color(0xFF505660),
                                                  size: 30,
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(height: 10),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Icon(
                                                // FontAwesomeIcons.calendarAlt,
                                                Icons.calendar_today_outlined,
                                                size: 35,
                                                color: Color(0xFF54586C),
                                              ),
                                              SizedBox(width: 10),
                                              Expanded(
                                                child: Container(
                                                  padding: EdgeInsets.all(7),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(
                                                        state
                                                                    .listAppointment
                                                                    .data![
                                                                        indexAppointment]
                                                                    .date !=
                                                                null
                                                            ? formatDate(
                                                                state
                                                                    .listAppointment
                                                                    .data![
                                                                        indexAppointment]
                                                                    .date!,
                                                                [
                                                                  MM,
                                                                  ' ',
                                                                  dd,
                                                                  ', ',
                                                                  yyyy
                                                                ],
                                                              )
                                                            : 'date_tba'.tr(),
                                                        style: TextStyle(
                                                          color:
                                                              Color(0xFF54586C),
                                                        ),
                                                      ),
                                                      SizedBox(height: 10),
                                                      Text(
                                                        state
                                                                        .listAppointment
                                                                        .data![
                                                                            indexAppointment]
                                                                        .timeStart !=
                                                                    null &&
                                                                state
                                                                        .listAppointment
                                                                        .data![
                                                                            indexAppointment]
                                                                        .timeEnd !=
                                                                    null
                                                            ? state
                                                                        .listAppointment
                                                                        .data![
                                                                            indexAppointment]
                                                                        .timeEnd! ==
                                                                    ""
                                                                ? state
                                                                    .listAppointment
                                                                    .data![
                                                                        indexAppointment]
                                                                    .timeStart!
                                                                    .substring(
                                                                        0, 5)
                                                                : '${state.listAppointment.data![indexAppointment].timeStart!.substring(0, 5)} - ${state.listAppointment.data![indexAppointment].timeEnd!.substring(0, 5)}'
                                                            : 'time_tba'.tr(),

                                                        //     widget
                                                        //         .widget
                                                        //         .listAppointment
                                                        //         .data![
                                                        //             indexAppointment]
                                                        //         .timeEnd ==
                                                        //     ''
                                                        // ? '${state.listAppointment.data![indexAppointment].timeStart!.substring(0, 5)}'
                                                        // : '${state.listAppointment.data![indexAppointment].timeStart!.substring(0, 5)} - ${state.listAppointment.data![indexAppointment].timeEnd!.substring(0, 5)}',
                                                        style: TextStyle(
                                                          fontSize: 11,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color:
                                                              Color(0xFFAFB2C3),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                          SizedBox(height: 10),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Icon(
                                                Icons.pin_drop_outlined,
                                                size: 35,
                                                color: Color(0xFF54586C),
                                              ),
                                              SizedBox(width: 10),
                                              Expanded(
                                                child: Container(
                                                  padding: EdgeInsets.all(7),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(
                                                        state
                                                            .listAppointment
                                                            .data![
                                                                indexAppointment]
                                                            .location!,
                                                        style: TextStyle(
                                                          color:
                                                              Color(0xFF54586C),
                                                        ),
                                                      ),
                                                      SizedBox(height: 10),
                                                      Text(
                                                        state
                                                            .listAppointment
                                                            .data![
                                                                indexAppointment]
                                                            .address!,
                                                        style: TextStyle(
                                                          fontSize: 11,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color:
                                                              Color(0xFFAFB2C3),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                          SizedBox(height: 10),
                                          Divider(color: Colors.grey)
                                        ],
                                      ),
                                    );
                                  }),
                          ],
                        )
                      : Center(
                          child: Column(
                            children: [
                              SizedBox(height: 10),
                              Text(
                                'no_record'.tr(),
                                style: TextStyle(
                                  color: Color(0xFFA9ABB5),
                                ),
                              ),
                              SizedBox(height: 10),
                            ],
                          ),
                        ),
                  contentBackgroundColor: Color(0xFFF6F6F9),
                  collapsedIcon: Icon(
                    FontAwesomeIcons.chevronDown,
                    color: Color(0xFF979BAC),
                    size: 14,
                    // color: accordiance[index] ? Colors.white : Colors.black,
                  ),
                  expandedIcon: Icon(
                    FontAwesomeIcons.chevronUp,
                    color: Color(0xFF979BAC),
                    size: 14,
                    // color: accordiance[index] ? Colors.white : Colors.black,
                  ),
                );
              } else {
                return GFAccordion(
                  collapsedTitleBackgroundColor: Color(0xFFF6F6F9),
                  expandedTitleBackgroundColor: Color(0xFFF6F6F9),
                  contentPadding: EdgeInsets.all(15),
                  titlePadding: EdgeInsets.all(15),
                  onToggleCollapsed: (bool collapse) {
                    setState(() {
                      accordiance = collapse;
                    });
                  },
                  titleBorderRadius: BorderRadius.only(
                    topLeft: Radius.circular(0),
                    topRight: Radius.circular(0),
                    bottomLeft: (accordiance)
                        ? Radius.circular(0)
                        : Radius.circular(10),
                    bottomRight: (accordiance)
                        ? Radius.circular(0)
                        : Radius.circular(10),
                  ),
                  contentBorderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                  margin: EdgeInsets.zero,
                  titleChild: Row(
                    children: [
                      Text(
                        'appointments'.tr(),
                        style: TextStyle(
                          color: Color(0xFF54586C),
                          fontSize: accordiance ? 16 : 15,
                          // fontWeight: accordiance ? FontWeight.bold : FontWeight.normal,
                          // color: accordiance[index] ? Colors.white : Colors.black,
                        ),
                      ),
                      SizedBox(width: 5),
                    ],
                  ),
                  contentChild: Column(
                    children: [
                      SpinKitFadingCircle(
                        size: 15,
                        color: Colors.red,
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                  contentBackgroundColor: Color(0xFFF6F6F9),
                  collapsedIcon: Icon(
                    FontAwesomeIcons.chevronDown,
                    color: Color(0xFF979BAC),
                    size: 14,
                    // color: accordiance[index] ? Colors.white : Colors.black,
                  ),
                  expandedIcon: Icon(
                    FontAwesomeIcons.chevronUp,
                    color: Color(0xFF979BAC),
                    size: 14,
                    // color: accordiance[index] ? Colors.white : Colors.black,
                  ),
                );
              }
            },
          ),
        ],
      ),
    );
  }

  downloadAppCard(BuildContext context, int indexAppointment,
      GetAppointment listAppointment) async {
    GlobalFunction.displayDisclosure(
      icon: Icon(
        Icons.camera_alt_outlined,
        color: Colors.blue,
        size: 30,
      ),
      title: 'fileProfilePictureTitle'.tr(),
      description: 'fileDownloadStorage'.tr(),
      key: 'fileDownloadStorage',
    ).then((value) async {
      if (value) {
        if (await Permission.storage.request().isGranted) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => WebDownload(
                url: listAppointment.data![indexAppointment].apptCard!,
              ),
            ),
          );
        } else {
          Flushbar(
            backgroundColor: Colors.white,
            // message: LoremText,
            // title: 'Permission needed to continue',
            titleText: Text(
              'permission_continue'.tr(),
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
            messageText: Text(
              'continue_permission'.tr(),
              style: TextStyle(color: Colors.black),
            ),
            mainButton: TextButton(
              onPressed: AppSettings.openAppSettings,
              child: Text(
                'App Settings',
                style: TextStyle(color: Colors.blue),
              ),
            ),
            duration: Duration(seconds: 7),
          ).show(context);
        }
      }
    });
  }

  downloadReferral(BuildContext context, int indexAppointment,
      GetAppointment listAppointment) async {
    GlobalFunction.displayDisclosure(
      icon: Icon(
        Icons.camera_alt_outlined,
        color: Colors.blue,
        size: 30,
      ),
      title: 'fileProfilePictureTitle'.tr(),
      description: 'fileDownloadStorage'.tr(),
      key: 'fileDownloadStorage',
    ).then((value) async {
      if (value) {
        if (await Permission.storage.request().isGranted) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => WebDownload(
                url: listAppointment.data![indexAppointment].referral!,
              ),
            ),
          );
        } else {
          Flushbar(
            backgroundColor: Colors.white,
            // message: LoremText,
            // title: 'Permission needed to continue',
            titleText: Text(
              'permission_continue'.tr(),
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
            messageText: Text(
              'continue_permission'.tr(),
              style: TextStyle(color: Colors.black),
            ),
            mainButton: TextButton(
              onPressed: AppSettings.openAppSettings,
              child: Text(
                'App Settings',
                style: TextStyle(color: Colors.blue),
              ),
            ),
            duration: Duration(seconds: 7),
          ).show(context);
        }
      }
    });
  }
}

class ConfirmationPopUp extends StatefulWidget {
  @override
  _ConfirmationPopUpState createState() => _ConfirmationPopUpState();
}

class _ConfirmationPopUpState extends State<ConfirmationPopUp> {
  var controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height;
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      scrollable: true,
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisSize:
          //     MainAxisSize.min,
          children: [
            Center(
              child: Text(
                'important_notice'.tr(),
                style: TextStyle(
                  color: Color(0xFF505660),
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
            SizedBox(height: 20),
            Text(
              'consultation_1'.tr(),
              style: TextStyle(color: Color(0xFF898DA1)),
            ),
            SizedBox(height: 10),
            Text.rich(
              TextSpan(
                children: [
                  TextSpan(
                    text: 'consultation_2'.tr(),
                    style: TextStyle(color: Color(0xFF898DA1)),
                  ),
                  TextSpan(
                    text: 'consultation_3'.tr().toUpperCase(),
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Color(0xFF505660)),
                  ),
                  TextSpan(
                    text: 'consultation_4'.tr(),
                    style: TextStyle(color: Color(0xFF898DA1)),
                  ),
                ],
              ),
            ),
            // Text(
            //     'Untuk rawatan di klinik panel di bawah Program Selangor Saring, pihak Kerajaan Negeri Selangor hanya akan membiayai kos KHIDMAT KONSULTASI sahaja. Sebarang kos tambahan perlu ditanggung oleh peserta program.'),
            SizedBox(height: 10),
            Linkify(
              text: 'consultation_5'.tr(),
              style: TextStyle(fontSize: 16, color: Color(0xFF898DA1)),
              options: LinkifyOptions(
                humanize: false,
                excludeLastPeriod: true,
              ),
              onOpen: (link) {
                launchUrl(
                  Uri.parse(link.url),
                  mode: LaunchMode.externalApplication,
                );
              },
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                    child: Text(
                      'cancel'.tr(),
                      style: TextStyle(
                        color: Color(0xFF505660),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 15),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: Color(0xFFEF0056),
                    ),
                    onPressed: () async {
                      Navigator.of(context).pop(true);
                    },
                    child: Text(
                      'agree'.tr(),
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

// class BookSlotPopup extends StatefulWidget {
//   @override
//   _BookSlotPopupState createState() => _BookSlotPopupState();
// }

// class _BookSlotPopupState extends State<BookSlotPopup> {
//   var controller = ScrollController();

//   @override
//   Widget build(BuildContext context) {
//     // double height = MediaQuery.of(context).size.height;
//     return AlertDialog(
//       shape: RoundedRectangleBorder(
//         borderRadius: BorderRadius.circular(20),
//       ),
//       scrollable: true,
//       content: SingleChildScrollView(
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           // mainAxisSize:
//           //     MainAxisSize.min,
//           children: [
//             Center(
//                 child: Icon(
//               Icons.calendar_month_rounded,
//               color: Color(0xFF505660),
//             )),
//             SizedBox(height: 15),
//             Center(
//               child: Text(
//                 'Book An Appointment'.tr(),
//                 style: TextStyle(
//                   color: Color(0xFF505660),
//                   fontWeight: FontWeight.bold,
//                   fontSize: 16,
//                 ),
//               ),
//             ),
//             SizedBox(height: 15),
//             TextFormField(
//               decoration: InputDecoration(
//                   hintText: 'Choose A Location',
//                   hintStyle: TextStyle(color: Color(0xFFC9CCDB))),
//             ),
//             SizedBox(height: 15),
//             TextFormField(
//               decoration: InputDecoration(
//                   hintText: 'Pick A Date',
//                   hintStyle: TextStyle(color: Color(0xFFC9CCDB))),
//             ),
//             SizedBox(height: 15),
//             TextFormField(
//               decoration: InputDecoration(
//                   hintText: 'Pick A Time',
//                   hintStyle: TextStyle(color: Color(0xFFC9CCDB))),
//             ),
//             SizedBox(height: 15),
//             Center(
//               child: ElevatedButton(
//                 style: ElevatedButton.styleFrom(
//                   padding: EdgeInsets.symmetric(horizontal: 40),
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(20)),
//                   backgroundColor: Color(0xFFEF0056),
//                 ),
//                 onPressed: () {
//                   Navigator.of(context).pop();
//                 },
//                 child: Text(
//                   'bookslot'.tr(),
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontWeight: FontWeight.bold,
//                   ),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );

//   }
// }

class NotInterestPopup extends StatefulWidget {
  final String? testId;
  const NotInterestPopup({
    Key? key,
    this.testId,
  }) : super(key: key);

  @override
  State<NotInterestPopup> createState() => _NotInterestPopupState();
}

class _NotInterestPopupState extends State<NotInterestPopup> {
  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    TextEditingController reason = TextEditingController();
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      scrollable: true,
      content: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            // mainAxisSize:
            //     MainAxisSize.min,
            children: [
              Center(
                child: Text(
                  'not_interest'.tr(),
                  style: TextStyle(
                    color: Color(0xFF505660),
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ),
              SizedBox(height: 15),
              Center(
                child: Text(
                  'ignore_app_desc'.tr(),
                  style: TextStyle(
                    color: Color(0xFF898DA1),
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 15),
              TextFormField(
                controller: reason,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'enter_text'.tr();
                  }
                  return null;
                },
                decoration: InputDecoration(
                    hintText: 'your_reason'.tr(),
                    hintStyle: TextStyle(color: Color(0xFFC9CCDB))),
              ),
              SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        backgroundColor: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop(false);
                      },
                      child: Text(
                        'cancel'.tr(),
                        style: TextStyle(
                          color: Color(0xFF505660),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        backgroundColor: Color(0xFFEF0056),
                      ),
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          await context
                              .read<AppointmentOptionCubit>()
                              .sendOption(widget.testId!, '3', reason.text);
                          print('send feedback to API');
                          Navigator.of(context).pop(true);
                        }
                      },
                      child: BlocConsumer<AppointmentOptionCubit,
                          AppointmentOptionState>(
                        listener: (context, stateApp) {
                          if (stateApp is AppointmentOptionFailed) {
                            Fluttertoast.showToast(msg: 'try_again'.tr());
                          }
                          if (stateApp is AppointmentOptionLoaded) {
                            showDialog(
                                context: context,
                                builder: (_) => FeedbackSentPopup());
                          }
                          //check back
                        },
                        builder: (context, stateApp) {
                          if (stateApp is AppointmentOptionLoading) {
                            return SpinKitFadingCircle(
                              color: Colors.white,
                              size: 20,
                            );
                          } else {
                            return Text(
                              'submit'.tr(),
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            );
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class NotInterestedPopup extends StatefulWidget {
  final String? testId;
  const NotInterestedPopup({
    Key? key,
    this.testId,
  }) : super(key: key);

  @override
  State<NotInterestedPopup> createState() => _NotInterestedPopupState();
}

class _NotInterestedPopupState extends State<NotInterestedPopup> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      scrollable: true,
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisSize:
          //     MainAxisSize.min,
          children: [
            Center(
              child: Image.asset(
                'assets/images/lhr/calendar_ignore.png',
                height: 30,
              ),
              //     child: Icon(
              //   FontAwesomeIcons
              //       .calendarXmark,
              //   color:
              //       Color(0xFF505660),
              // )
            ),
            SizedBox(height: 15),
            Center(
              child: Text(
                'ignore_app'.tr(),
                style: TextStyle(
                  color: Color(0xFF505660),
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
            SizedBox(height: 15),
            Center(
              child: Text(
                'ignore_app_desc'.tr(),
                style: TextStyle(
                  color: Color(0xFF898DA1),
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                    child: Text(
                      'no'.tr(),
                      style: TextStyle(
                        color: Color(0xFF505660),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 15),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: Color(0xFFEF0056),
                    ),
                    onPressed: () async {
                      await context
                          .read<AppointmentOptionCubit>()
                          .sendOption(widget.testId!, '3', 'null');
                      Navigator.of(context).pop(true);

                      print('send feedback to API');
                    },
                    child: BlocConsumer<AppointmentOptionCubit,
                        AppointmentOptionState>(
                      listener: (context, stateApp) {
                        if (stateApp is AppointmentOptionFailed) {
                          Fluttertoast.showToast(msg: 'try_again'.tr());
                        }
                        //check back
                      },
                      builder: (context, stateApp) {
                        if (stateApp is AppointmentOptionLoading) {
                          return SpinKitFadingCircle(
                            color: Colors.white,
                            size: 20,
                          );
                        } else {
                          return Text(
                            'yes'.tr(),
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          );
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class AlreadyBookedPopup extends StatefulWidget {
  final String? testId;

  const AlreadyBookedPopup({super.key, this.testId});

  @override
  State<AlreadyBookedPopup> createState() => _AlreadyBookedPopupState();
}

class _AlreadyBookedPopupState extends State<AlreadyBookedPopup> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController location = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      scrollable: true,
      content: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            // mainAxisSize:
            //     MainAxisSize.min,
            children: [
              Center(
                child: Text(
                  'already_booked'.tr(),
                  style: TextStyle(
                    color: Color(0xFF505660),
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ),
              SizedBox(height: 15),
              Center(
                child: Text(
                  'already_booked_desc'.tr(),
                  style: TextStyle(
                    color: Color(0xFF898DA1),
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 15),
              TextFormField(
                controller: location,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'enter_text'.tr();
                  }
                  return null;
                },
                decoration: InputDecoration(
                    hintText: 'follow_up_loc'.tr(),
                    hintStyle: TextStyle(color: Color(0xFFC9CCDB))),
              ),
              SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        backgroundColor: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop(false);
                      },
                      child: Text(
                        'cancel'.tr(),
                        style: TextStyle(
                          color: Color(0xFF505660),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        backgroundColor: Color(0xFFEF0056),
                      ),
                      onPressed: () async {
                        print(location.text);
                        if (_formKey.currentState!.validate()) {
                          await context
                              .read<AppointmentOptionCubit>()
                              .sendOption(widget.testId!, '6', location.text);
                          print('send feedback to API');
                          Navigator.of(context).pop(true);
                        }
                      },
                      child: BlocConsumer<AppointmentOptionCubit,
                          AppointmentOptionState>(
                        listener: (context, stateApp) {
                          if (stateApp is AppointmentOptionFailed) {
                            Fluttertoast.showToast(msg: 'try_again'.tr());
                          }
                          if (stateApp is AppointmentOptionLoaded) {
                            showDialog(
                                context: context,
                                builder: (_) => FeedbackSentPopup());
                          }
                          //check back
                        },
                        builder: (context, stateApp) {
                          if (stateApp is AppointmentOptionLoading) {
                            return SpinKitFadingCircle(
                              color: Colors.white,
                              size: 20,
                            );
                          } else {
                            return Text(
                              'submit'.tr(),
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            );
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class FeedbackSentPopup extends StatelessWidget {
  const FeedbackSentPopup({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      scrollable: true,
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisSize:
          //     MainAxisSize.min,
          children: [
            Center(
              child:
                  Image.asset('assets/images/lhr/green_tick.png', height: 40),
            ),
            SizedBox(height: 15),
            Center(
              child: Text(
                'feedback_recorded'.tr(),
                style: TextStyle(
                  color: Color(0xFF898DA1),
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 15),
            Center(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(horizontal: 40),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  backgroundColor: Color(0xFFEF0056),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'done'.tr(),
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
