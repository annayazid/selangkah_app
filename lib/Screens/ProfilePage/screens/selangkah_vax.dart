import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';

class SelangkahCardSelangkahVaxWidget extends StatefulWidget {
  final Appointment? appointment;
  const SelangkahCardSelangkahVaxWidget({
    Key? key,
    required this.appointment,
  }) : super(key: key);

  @override
  State<SelangkahCardSelangkahVaxWidget> createState() =>
      _SelangkahCardSelangkahVaxWidgetState();
}

class _SelangkahCardSelangkahVaxWidgetState
    extends State<SelangkahCardSelangkahVaxWidget> {
  bool _showQr = false;
  bool vaccineCert = false;

  @override
  void initState() {
    if (widget.appointment!.data != null) {
      if (widget.appointment!.data![0].completed1 == 1 &&
          widget.appointment!.data![0].completed2 == 1) {
        setState(() {
          vaccineCert = true;
        });
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // double height =
    //     MediaQuery.of(context).size.height - AppBar().preferredSize.height;
    // double width = MediaQuery.of(context).size.width;
    return Container(
      // padding: EdgeInsets.all(10),
      // decoration: BoxDecoration(
      //   borderRadius: BorderRadius.circular(40),
      //   color: Colors.white,
      // ),

      padding: EdgeInsets.all(10),
      decoration: !_showQr
          ? BoxDecoration(
              borderRadius: vaccineCert
                  ? BorderRadius.circular(40)
                  : BorderRadius.circular(10),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 1.0), //(x,y)
                  blurRadius: 4.0,
                ),
              ],
            )
          : BoxDecoration(),

      width: size.width * 0.85,
      child: Stack(
        children: [
          vaccineCert
              ? VaccineCert(
                  appointment: widget.appointment,
                  index: 0,
                )
              // Container()
              : SelangkahVaxContent(
                  name: widget.appointment!.data![0].accName!,
                  ic: widget.appointment!.data![0].icNo!,
                  dateDeclaration:
                      // widget.appointment.data[0].lastAssessment != null
                      //     ? DateFormat('d MMMM yyyy hh:mm a')
                      //         .format(widget.appointment.data[0].lastAssessment)
                      //     :
                      'To be Updated',
                  dateDose1:
                      // widget.appointment.data[0].appDate1 != null
                      //     ? DateFormat('d MMMM yyyy')
                      //         .format(widget.appointment.data[0].appDate1)
                      //     :
                      'To be Updated',
                  batchNo1: widget.appointment!.data![0].batchNo1!,
                  completedDose1: widget.appointment!.data![0].completed1! == 1
                      ? true
                      : false,
                  dateDose2:
                      // widget.appointment!.data![0].appDate2 != null
                      //     ? DateFormat('d MMMM yyyy')
                      //         .format(widget.appointment.data[0].appDate2)
                      //     :
                      'To be Updated',
                  batchNo2: widget.appointment!.data![0].batchNo2!,
                  completedDose2: widget.appointment!.data![0].completed2! == 1
                      ? true
                      : false,
                  place: widget.appointment!.data![0].ppvName!,
                ),
        ],
      ),
    );
  }
}

class SelangkahVaxContent extends StatelessWidget {
  final String name;
  final String ic;
  final String dateDeclaration;
  final String dateDose1;
  final String batchNo1;
  final bool completedDose1;
  final String dateDose2;
  final String batchNo2;
  final bool completedDose2;
  final String place;

  const SelangkahVaxContent(
      {super.key,
      required this.name,
      required this.ic,
      required this.dateDeclaration,
      required this.dateDose1,
      required this.batchNo1,
      required this.completedDose1,
      required this.dateDose2,
      required this.batchNo2,
      required this.completedDose2,
      required this.place});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child:
                Image.asset('assets/images/ProfilePage/selangkahVax_logo.png'),
          ),
          SizedBox(height: 10),
          Text(
            '$name ($ic)',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 10),
          Text(
            'details'.tr(),
            style: TextStyle(
              color: Color(0xFFB55CC3),
              fontSize: 14,
            ),
          ),
          SizedBox(height: 5),
          Container(
            height: height * 0.65,
            child: Stack(
              children: [
                Positioned(
                  left: width * 0.11,
                  child: Container(
                    margin: EdgeInsets.symmetric(vertical: height * 0.05),
                    height: height * 0.4,
                    width: 3,
                    decoration: BoxDecoration(
                      color: Colors.grey,
                    ),
                  ),
                ),
                Positioned(
                  right: 10,
                  child: Column(
                    children: [
                      // SelangkahVaxContentCard(
                      //   title: 'PENDAFTARAN',
                      //   description: '20 Mei 2021 10 00 AM',
                      //   imgPath: 'assets/images/selVax/pendaftaran.png',
                      // ),
                      SizedBox(height: 20),
                      SelangkahVaxContentCard(
                        title: 'update_health_declaration'.tr(),
                        description: Text(
                          dateDeclaration,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        imgPath: 'assets/images/ProfilePage/update.png',
                      ),
                      SizedBox(height: 20),
                      SelangkahVaxContentCard(
                        title: 'dose_1_info'.tr(),
                        description: Column(
                          children: [
                            VaccinationJourneyCardDetails(
                              category: '${'date'.tr()}: ',
                              data: '$dateDose1',
                            ),
                            VaccinationJourneyCardDetails(
                              category: '${'batch_no'.tr()}: ',
                              data: '$batchNo1'.toUpperCase(),
                            ),
                            VaccinationJourneyCardDetails(
                              category: '${'place'.tr()}: ',
                              data: '$place'.toUpperCase(),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Status: '),
                                Expanded(
                                  child: Text(
                                    () {
                                      if (completedDose1) {
                                        return 'vaccinated'.tr();
                                      } else {
                                        return 'not_vaccinated'.tr();
                                      }
                                    }(),
                                    style: TextStyle(
                                      color: () {
                                        if (completedDose1) {
                                          return Colors.green;
                                        } else {
                                          return Colors.red;
                                        }
                                      }(),
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        imgPath: 'assets/images/ProfilePage/dose.png',
                      ),
                      SizedBox(height: 20),
                      SelangkahVaxContentCard(
                        title: 'dose_2_info'.tr(),
                        description: Column(
                          children: [
                            VaccinationJourneyCardDetails(
                              category: '${'date'.tr()}: ',
                              data: '$dateDose2',
                            ),
                            VaccinationJourneyCardDetails(
                              category: '${'batch_no'.tr()}: ',
                              data: '$batchNo2'.toUpperCase(),
                            ),
                            VaccinationJourneyCardDetails(
                              category: '${'place'.tr()}: ',
                              data: '$place'.toUpperCase(),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Status: '),
                                Expanded(
                                  child: Text(
                                    () {
                                      if (completedDose2) {
                                        return 'vaccinated'.tr();
                                      } else {
                                        return 'not_vaccinated'.tr();
                                      }
                                    }(),
                                    style: TextStyle(
                                      color: () {
                                        if (completedDose2) {
                                          return Colors.green;
                                        } else {
                                          return Colors.red;
                                        }
                                      }(),
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        imgPath: 'assets/images/ProfilePage/dose.png',
                      ),
                      SizedBox(height: 20),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class VaccinationJourneyCardDetails extends StatelessWidget {
  final String category;
  final String data;

  const VaccinationJourneyCardDetails(
      {super.key, required this.category, required this.data});

  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Text(
              category,
              style: TextStyle(
                fontSize: 12,
              ),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
        Expanded(
          child: Text(
            data,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 12,
            ),
          ),
        ),
      ],
    );
  }
}

class SelangkahVaxContentCard extends StatelessWidget {
  final String title;
  final Widget description;
  final String imgPath;

  const SelangkahVaxContentCard(
      {super.key,
      required this.title,
      required this.description,
      required this.imgPath});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width * 0.75,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              width: width * 0.6,
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 2.0,
                  ),
                ],
              ),
              // color: Colors.red,

              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Color(0xFF008001),
                    ),
                    child: Text(
                      title,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 10),
                  description,
                ],
              ),
            ),
          ),
          Positioned(
            top: 10,
            left: 10,
            child: Image.asset(
              imgPath,
              height: 50,
            ),
          ),
        ],
      ),
    );
  }
}
