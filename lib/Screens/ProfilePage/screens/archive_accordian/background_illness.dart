import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getwidget/getwidget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';

class BackgroundIllnessArchived extends StatefulWidget {
  @override
  _BackgroundIllnessArchivedState createState() =>
      _BackgroundIllnessArchivedState();
}

class _BackgroundIllnessArchivedState extends State<BackgroundIllnessArchived> {
  bool accordiance = false;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width * 0.85,
      decoration: BoxDecoration(
        // borderRadius: BorderRadius.circular(10),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
          bottomLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
        color: Color(0xFFFF347D),
        // boxShadow: [
        //   BoxShadow(
        //     color: Colors.grey,
        //     offset: Offset(0.0, 1.0), //(x,y)
        //     blurRadius: 4.0,
        //   ),
        // ],
      ),
      child: Column(
        children: [
          SizedBox(height: 8),
          GFAccordion(
            collapsedTitleBackgroundColor: Color(0xFFF6F6F9),
            expandedTitleBackgroundColor: Color(0xFFF6F6F9),
            contentPadding: EdgeInsets.all(15),
            titlePadding: EdgeInsets.all(15),
            onToggleCollapsed: (bool collapse) {
              setState(() {
                accordiance = collapse;
              });
            },
            titleBorderRadius: BorderRadius.only(
              topLeft: Radius.circular(0),
              topRight: Radius.circular(0),
              bottomLeft:
                  (accordiance) ? Radius.circular(0) : Radius.circular(10),
              bottomRight:
                  (accordiance) ? Radius.circular(0) : Radius.circular(10),
            ),
            contentBorderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10),
            ),
            margin: EdgeInsets.zero,
            titleChild: Text(
              'background_illness'.tr(),
              style: TextStyle(
                color: Color(0xFF54586C),
                fontSize: accordiance ? 16 : 15,
                // fontWeight: accordiance ? FontWeight.bold : FontWeight.normal,
                // color: accordiance[index] ? Colors.white : Colors.black,
              ),
            ),
            contentChild: BlocBuilder<ArchivedRecordCubit, ArchivedRecordState>(
              builder: (context, state) {
                if (state is ArchivedRecordLoaded) {
                  return ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.archivedBackgroundIllness.data!.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      state.archivedBackgroundIllness
                                          .data![index],
                                      style: TextStyle(
                                        color: Color(0xFF505660),
                                        // fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  PopupMenuButton(
                                    offset: Offset(0, 25),
                                    padding: EdgeInsets.all(10),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    itemBuilder: (context) => [
                                      PopupMenuItem(
                                          value: 1,
                                          child: Row(
                                            children: [
                                              Text('unarchive'.tr()),
                                              Spacer(),
                                              Image.asset(
                                                  "assets/images/lhr/unarchive.png",
                                                  width: 15,
                                                  height: 15)
                                            ],
                                          )),
                                    ],
                                    onSelected: (value) async {
                                      if (value == 1) {
                                        bool archive = await LhrRepositories
                                            .unarchiveBackgroundIllness(
                                                illness: state
                                                    .archivedBackgroundIllness
                                                    .data![index]);

                                        if (archive) {
                                          print(
                                              'background illness unarchived');
                                          Fluttertoast.showToast(
                                              msg: 'record_unarchived'.tr());

                                          context
                                              .read<ArchivedRecordCubit>()
                                              .getArchivedRecord();

                                          context
                                              .read<BackgroundIllnessCubit>()
                                              .getBackgroundIllness();
                                        } else {
                                          Fluttertoast.showToast(
                                              msg: 'try_again'.tr());
                                        }
                                      }
                                    },
                                    child: Icon(
                                      // FontAwesomeIcons.archive,
                                      Icons.more_horiz,
                                      color: Color(0xFF505660),
                                      size: 30,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15),
                            ],
                          ),
                        );
                      });
                } else {
                  return Column(
                    children: [
                      SpinKitFadingCircle(
                        size: 15,
                        color: Colors.red,
                      ),
                      SizedBox(height: 10),
                    ],
                  );
                }
              },
            ),
            contentBackgroundColor: Color(0xFFF6F6F9),
            collapsedIcon: Icon(
              FontAwesomeIcons.chevronDown,
              color: Color(0xFF979BAC),
              size: 14,
              // color: accordiance[index] ? Colors.white : Colors.black,
            ),
            expandedIcon: Icon(
              FontAwesomeIcons.chevronUp,
              color: Color(0xFF979BAC),
              size: 14,
              // color: accordiance[index] ? Colors.white : Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
