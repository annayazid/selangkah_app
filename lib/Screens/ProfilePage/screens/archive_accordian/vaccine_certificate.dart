import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getwidget/getwidget.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/Screens/WebDownload/web_download.dart';
import 'package:selangkah_new/utils/global.dart';

class VaccineCertificateArchived extends StatefulWidget {
  @override
  _VaccineCertificateArchivedState createState() =>
      _VaccineCertificateArchivedState();
}

class _VaccineCertificateArchivedState
    extends State<VaccineCertificateArchived> {
  bool accordiance = false;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width * 0.85,
      decoration: BoxDecoration(
        // borderRadius: BorderRadius.circular(10),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20)),
        color: Color(0xFF4BE5C9),
        // boxShadow: [
        //   BoxShadow(
        //     color: Colors.grey,
        //     offset: Offset(0.0, 1.0), //(x,y)
        //     blurRadius: 4.0,
        //   ),
        // ],
      ),
      child: Column(
        children: [
          SizedBox(height: 8),
          GFAccordion(
            collapsedTitleBackgroundColor: Color(0xFFF6F6F9),
            expandedTitleBackgroundColor: Color(0xFFF6F6F9),
            contentPadding: EdgeInsets.all(15),
            titlePadding: EdgeInsets.all(15),
            onToggleCollapsed: (bool collapse) {
              setState(() {
                accordiance = collapse;
              });
            },
            titleBorderRadius: BorderRadius.only(
              topLeft: Radius.circular(0),
              topRight: Radius.circular(0),
              bottomLeft:
                  (accordiance) ? Radius.circular(0) : Radius.circular(10),
              bottomRight:
                  (accordiance) ? Radius.circular(0) : Radius.circular(10),
            ),
            contentBorderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10),
            ),
            margin: EdgeInsets.zero,
            titleChild: Text(
              'vaccine_cert'.tr(),
              style: TextStyle(
                color: Color(0xFF54586C),
                fontSize: accordiance ? 16 : 15,
                // fontWeight: accordiance ? FontWeight.bold : FontWeight.normal,
                // color: accordiance[index] ? Colors.white : Colors.black,
              ),
            ),
            contentChild: BlocBuilder<ArchivedRecordCubit, ArchivedRecordState>(
              builder: (context, state) {
                if (state is ArchivedRecordLoaded) {
                  return ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.archivedVaccineCert!.data!.length,
                      itemBuilder: (BuildContext context, int index) {
                        List<String> splitted = state
                            .archivedVaccineCert!.data![index].documentDir!
                            .split('.');

                        print(splitted);

                        String formatFile = splitted.last;

                        List<String> splittedVaccine =
                            splitted.sublist(0, splitted.length - 1);

                        String vaccineFileName = splittedVaccine.join('.');
                        print('Disini!');
                        print(vaccineFileName);
                        print(formatFile);

                        return Column(
                          children: [
                            SizedBox(height: 10),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              // crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                formatFile == 'pdf' || formatFile == 'PDF'
                                    ? Image.asset("assets/images/lhr/pdf.png",
                                        width: 40, height: 40)
                                    : Image.asset("assets/images/lhr/jpg.png",
                                        width: 40, height: 40),
                                SizedBox(width: 10),
                                Expanded(
                                  child: Container(
                                    // padding: EdgeInsets.symmetric(vertical: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          vaccineFileName,
                                          style: TextStyle(
                                            color: Color(0xFF54586C),
                                          ),
                                        ),
                                        SizedBox(height: 10),
                                        Text(
                                          formatDate(
                                              state.archivedVaccineCert!
                                                  .data![index].createdTs!,
                                              [
                                                hh,
                                                ':',
                                                nn,
                                                ' ',
                                                am,
                                                ', ',
                                                MM,
                                                ' ',
                                                dd,
                                                ', ',
                                                yyyy
                                              ]),
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xFFAFB2C3),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(width: 10),
                                PopupMenuButton(
                                  offset: Offset(0, 25),
                                  padding: EdgeInsets.all(10),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20)),
                                  itemBuilder: (context) => [
                                    PopupMenuItem(
                                        value: 1,
                                        child: Row(
                                          children: [
                                            Text('swabook_download'.tr()),
                                            Spacer(),
                                            Image.asset(
                                                "assets/images/lhr/download.png",
                                                width: 15,
                                                height: 15)
                                          ],
                                        )),
                                    PopupMenuItem(
                                        value: 2,
                                        child: Row(
                                          children: [
                                            Text('unarchive'.tr()),
                                            Spacer(),
                                            Image.asset(
                                                "assets/images/lhr/unarchive.png",
                                                width: 15,
                                                height: 15)
                                          ],
                                        )),
                                    PopupMenuItem(
                                        value: 3,
                                        child: Row(
                                          children: [
                                            Text('delete'.tr()),
                                            Spacer(),
                                            Image.asset(
                                                "assets/images/lhr/delete.png",
                                                width: 15,
                                                height: 15)
                                          ],
                                        )),
                                    PopupMenuItem(
                                        value: 4,
                                        child: Row(
                                          children: [
                                            Text('share'.tr()),
                                            Spacer(),
                                            Image.asset(
                                                "assets/images/lhr/share.png",
                                                width: 15,
                                                height: 15)
                                          ],
                                        )),
                                  ],
                                  onSelected: (value) async {
                                    if (value == 1) {
                                      String url =
                                          await LhrRepositories.downloadDoc(
                                              state.archivedVaccineCert!
                                                  .data![index].id!);

                                      GlobalFunction.displayDisclosure(
                                        icon: Icon(
                                          Icons.camera_alt_outlined,
                                          color: Colors.blue,
                                          size: 30,
                                        ),
                                        title: 'fileProfilePictureTitle'.tr(),
                                        description: 'fileDownloadStorage'.tr(),
                                        key: 'fileDownloadStorage',
                                      ).then((value) async {
                                        if (value) {
                                          if (await Permission.storage
                                              .request()
                                              .isGranted) {
                                            //navigate to new web downloader

                                            Navigator.of(context).push(
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    WebDownload(
                                                  url: url,
                                                ),
                                              ),
                                            );
                                          } else {
                                            Flushbar(
                                              backgroundColor: Colors.white,
                                              // message: LoremText,
                                              // title: 'Permission needed to continue',
                                              titleText: Text(
                                                'permission_continue'.tr(),
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 15,
                                                ),
                                              ),
                                              messageText: Text(
                                                'continue_permission'.tr(),
                                                style: TextStyle(
                                                    color: Colors.black),
                                              ),
                                              mainButton: TextButton(
                                                onPressed:
                                                    AppSettings.openAppSettings,
                                                child: Text(
                                                  'App Settings',
                                                  style: TextStyle(
                                                      color: Colors.blue),
                                                ),
                                              ),
                                              duration: Duration(seconds: 7),
                                            ).show(context);
                                          }
                                        }
                                      });
                                    }
                                    if (value == 2) {
                                      bool archive =
                                          await LhrRepositories.unarchiveDoc(
                                              idDoc: state.archivedVaccineCert!
                                                  .data![index].id);

                                      if (archive) {
                                        print('record archived');
                                        Fluttertoast.showToast(
                                            msg: 'record_unarchived'.tr());
                                        context
                                            .read<ArchivedRecordCubit>()
                                            .getArchivedRecord();
                                        context
                                            .read<VaccineCertificatesCubit>()
                                            .getVaccineCCert();
                                      } else {
                                        Fluttertoast.showToast(
                                            msg: 'try_again'.tr());
                                      }
                                    }
                                    if (value == 3) {
                                      if (state.archivedVaccineCert!
                                              .data![index].canDelete ==
                                          1)
                                        showDialog(
                                            context: context,
                                            builder: (_) => AlertDialog(
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20),
                                                  ),
                                                  scrollable: true,
                                                  content:
                                                      SingleChildScrollView(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      // mainAxisSize:
                                                      //     MainAxisSize.min,
                                                      children: [
                                                        Center(
                                                            child: Icon(
                                                          FontAwesomeIcons
                                                              .trashCan,
                                                          color:
                                                              Color(0xFF505660),
                                                        )),
                                                        SizedBox(height: 15),
                                                        Center(
                                                          child: Text(
                                                            'delete_file'.tr(),
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xFF505660),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 16,
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(height: 15),
                                                        Center(
                                                          child: Text(
                                                            'delete_file_desc'
                                                                .tr(),
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xFF898DA1),
                                                            ),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        SizedBox(height: 15),
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Expanded(
                                                              child:
                                                                  ElevatedButton(
                                                                style: ElevatedButton
                                                                    .styleFrom(
                                                                  shape: RoundedRectangleBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              20)),
                                                                  backgroundColor:
                                                                      Colors
                                                                          .white,
                                                                ),
                                                                onPressed: () {
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop();
                                                                },
                                                                child: Text(
                                                                  'cancel'.tr(),
                                                                  style:
                                                                      TextStyle(
                                                                    color: Color(
                                                                        0xFF505660),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            SizedBox(width: 15),
                                                            Expanded(
                                                              child:
                                                                  ElevatedButton(
                                                                style: ElevatedButton
                                                                    .styleFrom(
                                                                  shape: RoundedRectangleBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              20)),
                                                                  backgroundColor:
                                                                      Color(
                                                                          0xFFEF0056),
                                                                ),
                                                                onPressed:
                                                                    () async {
                                                                  bool delete = await LhrRepositories.deleteDoc(
                                                                      idDoc: state
                                                                          .archivedVaccineCert!
                                                                          .data![
                                                                              index]
                                                                          .id);

                                                                  if (delete) {
                                                                    print(
                                                                        'record deleted');
                                                                    Fluttertoast
                                                                        .showToast(
                                                                            msg:
                                                                                'record_deleted'.tr());
                                                                    context
                                                                        .read<
                                                                            ArchivedRecordCubit>()
                                                                        .getArchivedRecord();
                                                                  } else {
                                                                    Fluttertoast
                                                                        .showToast(
                                                                            msg:
                                                                                'try_again'.tr());
                                                                  }

                                                                  Navigator.of(
                                                                          context)
                                                                      .pop();
                                                                },
                                                                child: Text(
                                                                  'delete'.tr(),
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ));
                                      if (state.archivedVaccineCert!
                                              .data![index].canDelete ==
                                          0)
                                        showDialog(
                                            context: context,
                                            builder: (_) => AlertDialog(
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20),
                                                  ),
                                                  scrollable: true,
                                                  content: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    // mainAxisSize:
                                                    //     MainAxisSize.min,
                                                    children: [
                                                      Center(
                                                          child: Icon(
                                                        FontAwesomeIcons.lock,
                                                        color:
                                                            Color(0xFF505660),
                                                      )),
                                                      SizedBox(height: 15),
                                                      Center(
                                                        child: Text(
                                                          'cant_delete_file'
                                                              .tr(),
                                                          style: TextStyle(
                                                            color: Color(
                                                                0xFF505660),
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 16,
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(height: 15),
                                                      Center(
                                                        child: Text(
                                                          'cant_delete_file_desc'
                                                              .tr(),
                                                          style: TextStyle(
                                                            color: Color(
                                                                0xFF898DA1),
                                                          ),
                                                          textAlign:
                                                              TextAlign.center,
                                                        ),
                                                      ),
                                                      SizedBox(height: 15),
                                                      Center(
                                                        child: ElevatedButton(
                                                          style: ElevatedButton
                                                              .styleFrom(
                                                            shape: RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            20)),
                                                            backgroundColor:
                                                                Color(
                                                                    0xFFEF0056),
                                                          ),
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                          child: Text(
                                                            'Okay'.tr(),
                                                            style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ));
                                    }
                                    if (value == 4) {
                                      showDialog(
                                        context: context,
                                        builder: (_) => BlocProvider(
                                          create: (context) => ShareCodeCubit(),
                                          child: ShareCodeDialog(
                                            idDoc: state.archivedVaccineCert!
                                                .data![index].id!,
                                            idDataSaring: null,
                                          ),
                                        ),
                                      );
                                    }
                                  },
                                  child: Icon(
                                    // FontAwesomeIcons.archive,
                                    Icons.more_horiz,
                                    color: Color(0xFF505660),
                                    size: 30,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 10),
                            Divider(color: Colors.grey)
                          ],
                        );
                      });
                } else {
                  return Column(
                    children: [
                      SpinKitFadingCircle(
                        size: 15,
                        color: Colors.red,
                      ),
                      SizedBox(height: 10),
                    ],
                  );
                }
              },
            ),
            contentBackgroundColor: Color(0xFFF6F6F9),
            collapsedIcon: Icon(
              FontAwesomeIcons.chevronDown,
              color: Color(0xFF979BAC),
              size: 14,
              // color: accordiance[index] ? Colors.white : Colors.black,
            ),
            expandedIcon: Icon(
              FontAwesomeIcons.chevronUp,
              color: Color(0xFF979BAC),
              size: 14,
              // color: accordiance[index] ? Colors.white : Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
