import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getwidget/getwidget.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/Screens/WebDownload/web_download.dart';
import 'package:selangkah_new/utils/global.dart';

class AppointmentArchived extends StatefulWidget {
  @override
  _AppointmentArchivedState createState() => _AppointmentArchivedState();
}

class _AppointmentArchivedState extends State<AppointmentArchived> {
  bool accordiance = false;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width * 0.85,
      decoration: BoxDecoration(
        // borderRadius: BorderRadius.circular(10),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20)),
        color: Color(0xFFCD37C7),
        // boxShadow: [
        //   BoxShadow(
        //     color: Colors.grey,
        //     offset: Offset(0.0, 1.0), //(x,y)
        //     blurRadius: 4.0,
        //   ),
        // ],
      ),
      child: Column(
        children: [
          SizedBox(height: 8),
          GFAccordion(
            collapsedTitleBackgroundColor: Color(0xFFF6F6F9),
            expandedTitleBackgroundColor: Color(0xFFF6F6F9),
            contentPadding: EdgeInsets.all(15),
            titlePadding: EdgeInsets.all(15),
            onToggleCollapsed: (bool collapse) {
              setState(() {
                accordiance = collapse;
              });
            },
            titleBorderRadius: BorderRadius.only(
              topLeft: Radius.circular(0),
              topRight: Radius.circular(0),
              bottomLeft:
                  (accordiance) ? Radius.circular(0) : Radius.circular(10),
              bottomRight:
                  (accordiance) ? Radius.circular(0) : Radius.circular(10),
            ),
            contentBorderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10),
            ),
            margin: EdgeInsets.zero,
            titleChild: Text(
              'appointment'.tr(),
              style: TextStyle(
                color: Color(0xFF54586C),
                fontSize: accordiance ? 16 : 15,
                // fontWeight: accordiance ? FontWeight.bold : FontWeight.normal,
                // color: accordiance[index] ? Colors.white : Colors.black,
              ),
            ),
            contentChild: BlocBuilder<ArchivedRecordCubit, ArchivedRecordState>(
              builder: (context, state) {
                if (state is ArchivedRecordLoaded) {
                  return ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.archivedAppointment.data!.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      state.archivedAppointment.data![index]
                                          .testName!,
                                      style: TextStyle(
                                        color: Color(0xFF505660),
                                        // fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  PopupMenuButton(
                                    offset: Offset(0, 25),
                                    padding: EdgeInsets.all(10),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    itemBuilder: (context) => [
                                      if (state.archivedAppointment.data![index]
                                              .referral !=
                                          '')
                                        PopupMenuItem(
                                            value: 1,
                                            child: Row(
                                              children: [
                                                Text('refer_letter'.tr()),
                                                Spacer(),
                                                Image.asset(
                                                    "assets/images/lhr/referral.png",
                                                    width: 15,
                                                    height: 15)
                                              ],
                                            )),
                                      if (state.archivedAppointment.data![index]
                                              .apptCard !=
                                          '')
                                        PopupMenuItem(
                                            value: 2,
                                            child: Row(
                                              children: [
                                                Text('appointment_card'.tr()),
                                                Spacer(),
                                                Image.asset(
                                                    "assets/images/lhr/appcard.png",
                                                    width: 15,
                                                    height: 15)
                                              ],
                                            )),
                                      PopupMenuItem(
                                          value: 3,
                                          child: Row(
                                            children: [
                                              Text('unarchive'.tr()),
                                              Spacer(),
                                              Image.asset(
                                                  "assets/images/lhr/unarchive.png",
                                                  width: 15,
                                                  height: 15)
                                            ],
                                          )),
                                    ],
                                    onSelected: (value) async {
                                      if (value == 1) {
                                        String url = state.archivedAppointment
                                            .data![index].referral!;
                                        downloadReferral(context, url);
                                      }
                                      if (value == 2) {
                                        String url = state.archivedAppointment
                                            .data![index].apptCard!;
                                        downloadAppCard(context, url);
                                      }
                                      if (value == 3) {
                                        bool archive =
                                            await LhrRepositories.unarchiveDoc(
                                          idApp: state
                                              .archivedAppointment
                                              .data![index]
                                              .idProgramAppointment,
                                        );

                                        if (archive) {
                                          print('appointment unarchived');
                                          Fluttertoast.showToast(
                                              msg: 'record_unarchived'.tr());

                                          context
                                              .read<ArchivedRecordCubit>()
                                              .getArchivedRecord();
                                          context
                                              .read<AppointmentCubit>()
                                              .getAppointment();
                                        } else {
                                          Fluttertoast.showToast(
                                              msg: 'try_again'.tr());
                                        }
                                      }
                                    },
                                    child: Icon(
                                      // FontAwesomeIcons.archive,
                                      Icons.more_horiz,
                                      color: Color(0xFF505660),
                                      size: 30,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 10),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(
                                    // FontAwesomeIcons.calendarAlt,
                                    Icons.calendar_today_outlined,
                                    size: 35,
                                    color: Color(0xFF54586C),
                                  ),
                                  SizedBox(width: 10),
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.all(7),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            state.archivedAppointment
                                                        .data![index].date !=
                                                    null
                                                ? formatDate(
                                                    state.archivedAppointment
                                                        .data![index].date!,
                                                    [MM, ' ', dd, ', ', yyyy])
                                                : 'date_tba'.tr(),
                                            style: TextStyle(
                                              color: Color(0xFF54586C),
                                            ),
                                          ),
                                          SizedBox(height: 10),
                                          Text(
                                            state.archivedAppointment
                                                        .data![index].timeEnd ==
                                                    ''
                                                ? '${state.archivedAppointment.data![index].timeStart!.substring(0, 5)}'
                                                : '${state.archivedAppointment.data![index].timeStart!.substring(0, 5)} - ${state.archivedAppointment.data![index].timeEnd!.substring(0, 5)}',
                                            style: TextStyle(
                                              fontSize: 11,
                                              fontWeight: FontWeight.bold,
                                              color: Color(0xFFAFB2C3),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(height: 10),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.pin_drop_outlined,
                                    size: 35,
                                    color: Color(0xFF54586C),
                                  ),
                                  SizedBox(width: 10),
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.all(7),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            state.archivedAppointment
                                                .data![index].location!,
                                            style: TextStyle(
                                              color: Color(0xFF54586C),
                                            ),
                                          ),
                                          SizedBox(height: 10),
                                          Text(
                                            state.archivedAppointment
                                                .data![index].address!,
                                            style: TextStyle(
                                              fontSize: 11,
                                              fontWeight: FontWeight.bold,
                                              color: Color(0xFFAFB2C3),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(height: 10),
                              Divider(color: Colors.grey)
                            ],
                          ),
                        );
                      });
                } else {
                  return Column(
                    children: [
                      SpinKitFadingCircle(
                        size: 15,
                        color: Colors.red,
                      ),
                      SizedBox(height: 10),
                    ],
                  );
                }
              },
            ),
            contentBackgroundColor: Color(0xFFF6F6F9),
            collapsedIcon: Icon(
              FontAwesomeIcons.chevronDown,
              color: Color(0xFF979BAC),
              size: 14,
              // color: accordiance[index] ? Colors.white : Colors.black,
            ),
            expandedIcon: Icon(
              FontAwesomeIcons.chevronUp,
              color: Color(0xFF979BAC),
              size: 14,
              // color: accordiance[index] ? Colors.white : Colors.black,
            ),
          ),
        ],
      ),
    );
  }

  downloadAppCard(BuildContext context, String url) async {
    GlobalFunction.displayDisclosure(
      icon: Icon(
        Icons.camera_alt_outlined,
        color: Colors.blue,
        size: 30,
      ),
      title: 'fileProfilePictureTitle'.tr(),
      description: 'fileDownloadStorage'.tr(),
      key: 'fileDownloadStorage',
    ).then((value) async {
      if (value) {
        if (await Permission.storage.request().isGranted) {
          //navigate to new Web downloader

          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => WebDownload(
                url: url,
              ),
            ),
          );
        } else {
          Flushbar(
            backgroundColor: Colors.white,
            // message: LoremText,
            // title: 'Permission needed to continue',
            titleText: Text(
              'permission_continue'.tr(),
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
            messageText: Text(
              'continue_permission'.tr(),
              style: TextStyle(color: Colors.black),
            ),
            mainButton: TextButton(
              onPressed: AppSettings.openAppSettings,
              child: Text(
                'App Settings',
                style: TextStyle(color: Colors.blue),
              ),
            ),
            duration: Duration(seconds: 7),
          ).show(context);
        }
      }
    });
  }

  downloadReferral(BuildContext context, String url) async {
    GlobalFunction.displayDisclosure(
      icon: Icon(
        Icons.camera_alt_outlined,
        color: Colors.blue,
        size: 30,
      ),
      title: 'fileProfilePictureTitle'.tr(),
      description: 'fileDownloadStorage'.tr(),
      key: 'fileDownloadStorage',
    ).then((value) async {
      if (value) {
        if (await Permission.storage.request().isGranted) {
          //navigate to new Web downloader

          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => WebDownload(
                url: url,
              ),
            ),
          );
        } else {
          Flushbar(
            backgroundColor: Colors.white,
            // message: LoremText,
            // title: 'Permission needed to continue',
            titleText: Text(
              'permission_continue'.tr(),
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
            messageText: Text(
              'continue_permission'.tr(),
              style: TextStyle(color: Colors.black),
            ),
            mainButton: TextButton(
              onPressed: AppSettings.openAppSettings,
              child: Text(
                'App Settings',
                style: TextStyle(color: Colors.blue),
              ),
            ),
            duration: Duration(seconds: 7),
          ).show(context);
        }
      }
    });
  }
}
