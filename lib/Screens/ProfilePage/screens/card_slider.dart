import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';

class CardSlider extends StatefulWidget {
  final String? page;

  const CardSlider({super.key, required this.page});

  @override
  State<CardSlider> createState() => _CardSliderState();
}

class _CardSliderState extends State<CardSlider> {
  int? _current;
  List<String> cardList = [];

  @override
  void initState() {
    cardList.add('SelangkahID');
    // if (widget.appointment.data != null &&
    //     (widget.appointment.data![0].appDate1 != null &&
    //         widget.appointment.data![0].appDate2 != null)) {
    //   cardList.add('SelangkahVax');
    // }
    // cardList.add('SelangkahVax');
    cardList.add('HAYAT');

    if (widget.page != null) {
      _current = cardList.indexOf(widget.page!);

      //if not found set to zero
      if (_current == -1) {
        _current = 0;
      }
    } else {
      _current = 0;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height =
        MediaQuery.of(context).size.height - AppBar().preferredSize.height;
    return Container(
      margin: EdgeInsets.only(top: height * 0.18),
      height: height * 0.10,
      child: CarouselSlider(
        items: [
          Container(
            margin: EdgeInsets.fromLTRB(10, 0, 10, height * 0.01),
            decoration: BoxDecoration(
              // color: Colors.white,
              color: Color(0xFFA70000),
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 1.0), //(x,y)
                  blurRadius: 4.0,
                ),
              ],
            ),
            width: double.infinity,
            padding: EdgeInsets.all(5),
            alignment: Alignment.center,
            child: Container(
              decoration: BoxDecoration(
                // color: Color(0xFFA70000),
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
              ),
              width: double.infinity,
              padding: EdgeInsets.all(5),
              alignment: Alignment.center,
              child: Text(
                'user_details'.tr().toUpperCase(),
                style: TextStyle(
                  // color: Colors.white,
                  color: Color(0xFFA70000),
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          if (cardList.contains('SelangkahVax'))
            Container(
              margin: EdgeInsets.fromLTRB(10, 0, 10, height * 0.01),
              decoration: BoxDecoration(
                // color: Colors.white,
                color: Color(0xFF13A89E),
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 4.0,
                  ),
                ],
              ),
              width: double.infinity,
              padding: EdgeInsets.all(5),
              alignment: Alignment.center,
              child: Container(
                decoration: BoxDecoration(
                  // color: Color(0xFFA70000),
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                ),
                width: double.infinity,
                padding: EdgeInsets.all(5),
                alignment: Alignment.center,
                child: Text(
                  'SELANGKAH VAX',
                  style: TextStyle(
                    // color: Colors.white,
                    color: Color(0xFF13A89E),
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
          Container(
            margin: EdgeInsets.fromLTRB(10, 0, 10, height * 0.01),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 1.0), //(x,y)
                  blurRadius: 4.0,
                ),
              ],
              color: Color(0xFFEF0056),
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              ),
            ),
            width: double.infinity,
            padding: EdgeInsets.all(5),
            alignment: Alignment.center,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
              ),
              width: double.infinity,
              padding: EdgeInsets.all(5),
              alignment: Alignment.center,
              child: Text(
                'medical_record'.tr().toUpperCase(),
                style: TextStyle(
                    color: Color(0xFFEF0056),
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
              ),
            ),
          ),
        ],
        options: CarouselOptions(
          height: height * 0.27,
          viewportFraction: 0.9,
          initialPage: _current!,
          enableInfiniteScroll: false,
          // enlargeCenterPage: true,
          scrollDirection: Axis.horizontal,

          onPageChanged: (index, _) async {
            setState(() {
              _current = index;
            });
            print(index);

            context.read<SliderCardCubit>().getSliderCard(cardList[index]);
            // context.read<SliderBloc>().add(GetCardDetails(cardList[index]));
          },
        ),
      ),
    );
  }
}
