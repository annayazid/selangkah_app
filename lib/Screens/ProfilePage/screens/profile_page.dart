import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:lottie/lottie.dart';
import 'package:qr_flutter/qr_flutter.dart';

import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/Screens/Settings/delete_account/delete_account_cubit.dart';
import 'package:selangkah_new/Screens/Settings/settings.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:shimmer/shimmer.dart';

class ProfilePage extends StatefulWidget {
  final String page;

  const ProfilePage({super.key, required this.page});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  Future<void> redirectEditImage(Uint8List? profilePic) async {
    showDialog(
      context: context,
      builder: (context) => BlocProvider(
        create: (context) => UpdateImageCubit(),
        child: ImageDialog(imgbytes: profilePic),
      ),
    ).then((value) => context.read<ProfilePageCubit>().getUserProfile());
  }

  @override
  void initState() {
    context.read<ProfilePageCubit>().getUserProfile();
    // if (widget.page == 'SelangkahID') {
    context.read<SliderCardCubit>().getSliderCard(widget.page);
    // }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double height =
        MediaQuery.of(context).size.height - AppBar().preferredSize.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(

        //Profile Scaffold
        // appBar: AppBar(
        //   backgroundColor: kPrimaryColor,
        //   centerTitle: true,
        //   // automaticallyImplyLeading: true,
        //   leading: IconButton(
        //     icon: Icon(Icons.arrow_back_ios),
        //     onPressed: () {
        //       Navigator.of(context).pop();
        //     },
        //   ),
        //   title: Text(
        //     'Profile',
        //     textAlign: TextAlign.center,
        //     style: TextStyle(
        //       fontSize: 15,
        //       color: Colors.white,
        //       fontWeight: FontWeight.bold,
        //       fontFamily: 'Roboto',
        //     ),
        //   ),
        //   actions: [
        //     IconButton(
        //         icon: Icon(
        //           Icons.settings,
        //           color: Colors.white,
        //           size: 30,
        //         ),
        //         onPressed: () {}),
        //   ],
        // ),

        //Selangkah Scaffold
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.white,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios_new,
                color: Colors.grey,
                size: 30,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              }),
          title: Image.asset(
            "assets/images/Scaffold/selangkah_logo.png",
            width: width * 0.45,
          ),
          actions: [
            IconButton(
                icon: Icon(
                  Icons.settings,
                  color: Colors.grey,
                  size: 30,
                ),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => BlocProvider(
                        create: (context) => DeleteAccountCubit(),
                        child: SettingsScreen(),
                      ),
                    ),
                  );
                }),
          ],
        ),
        body: BlocBuilder<ProfilePageCubit, ProfilePageState>(
          builder: (context, state) {
            if (state is ProfilePageLoaded) {
              return SingleChildScrollView(
                child: Column(
                  children: [
                    Stack(children: [
                      //background yellow
                      Container(
                        height: height * 0.23,
                        width: size.width,
                        child: Image.asset(
                          'assets/images/ProfilePage/background_profile_page.png',
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                      Container(
                        height: height * 0.13,
                        padding: EdgeInsets.all(10),
                        margin: EdgeInsets.fromLTRB(27, height * 0.02, 27, 0),
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              offset: Offset(0.0, 1.0), //(x,y)
                              blurRadius: 4.0,
                            ),
                          ],
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(20),
                          ),
                        ),
                        child: Row(
                          children: [
                            // SizedBox(width: 15),
                            Container(
                              child: Stack(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      redirectEditImage(state.profilePic!);
                                    },
                                    child: CircleAvatar(
                                      // backgroundImage: state.profilePic == null
                                      //     ? AssetImage(
                                      //         'assets/images/ProfilePage/male_avatar.png')
                                      //     : MemoryImage(state.profilePic!)
                                      //         as ImageProvider,
                                      //if DP null, tak keluar gambar avatar
                                      backgroundImage: () {
                                        if (state.profilePic == null) {
                                          // return AssetImage(
                                          //     'assets/images/Asas/male_avatar.png');
                                          return AssetImage(
                                              'assets/images/ProfilePage/male_avatar.png');
                                        } else {
                                          return MemoryImage(state.profilePic!);
                                        }
                                      }() as ImageProvider,

                                      backgroundColor: Colors.blueGrey,
                                      radius: height * 0.05,
                                    ),
                                  ),
                                  Positioned(
                                    right: 0,
                                    bottom: 5,
                                    child: GestureDetector(
                                      onTap: () {
                                        redirectEditImage(state.profilePic);
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: kPrimaryColor,
                                        ),
                                        child: Icon(
                                          Icons.camera_alt,
                                          color: Colors.white,
                                          size: 15.5,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            // SizedBox(width: 15),
                            Expanded(
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 5),
                                alignment: Alignment.center,
                                // padding: EdgeInsets.fromLTRB(45, 5, 45, 5),

                                // padding: EdgeInsets.all(15),
                                // decoration: BoxDecoration(
                                //   color: Color(0xFFA70000),
                                // color: kPrimaryColor,
                                // color: Colors.white,
                                //   borderRadius: BorderRadius.all(
                                //     Radius.circular(15),
                                //   ),
                                // ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  // crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      state.userProfile!.selId!,
                                      // 'Selangkah ID',
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Text(
                                      state.userProfile!.name!,
                                      // 'User Name',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.black,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              child: QrImage(
                                padding: EdgeInsets.all(7),
                                backgroundColor: Colors.white,
                                data: state.userProfile!.selId!
                                    .replaceAll(' ', ''),
                                // data: 'QR',
                                version: QrVersions.auto,
                                size: height * 0.10,
                              ),
                            ),
                          ],
                        ),
                      ),
                      CardSlider(page: widget.page),
                    ]),
                    SizedBox(height: 10),
                    BlocBuilder<SliderCardCubit, SliderCardState>(
                      builder: (context, sliderState) {
                        print(sliderState);
                        if (sliderState is SliderCardSelangkahID) {
                          return SliderCardSelangkahIDWidget(
                            userProfile: state.userProfile,
                            work: sliderState.work,
                          );
                        } else if (sliderState is SliderCardHAYAT) {
                          return SelangkahCardLHRWiget();
                          // return SelangkahCardHAYATWidget(
                          //   physicalPatient: sliderState.physicalPatient,
                          //   getAppointment: sliderState.getAppointment,
                          //   backgroundIllness: sliderState.backgroundIllness,
                          //   labResult: sliderState.labResult,
                          //   physicalRecord: sliderState.physicalRecord,
                          //   appointment: sliderState.appointment,
                          //   medicalCert: sliderState.medicalCert,
                          //   prescription: sliderState.prescription,
                          //   vaccineCert: sliderState.vaccineCert,
                          //   archivedBackgroundIllness:
                          //       sliderState.archivedBackgroundIllness,
                          //   archivedAppointment:
                          //       sliderState.archivedAppointment,
                          //   archivedLabResult: sliderState.archivedLabResult,
                          //   archivedMedicalCert:
                          //       sliderState.archivedMedicalCert,
                          //   archivedPrescription:
                          //       sliderState.archivedPrescription,
                          //   archivedVaccineCert:
                          //       sliderState.archivedVaccineCert,
                          // );
                        } else if (sliderState is SliderCardError) {
                          return Container(
                            width: width * 0.85,
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(0.0, 1.0), //(x,y)
                                  blurRadius: 4.0,
                                ),
                              ],
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.white,
                            ),
                            child: Column(
                              children: [
                                // Image.asset('assets/images/error.gif'),
                                Lottie.asset('assets/images/lhr/error.json'),
                                SizedBox(height: 20),

                                Text(
                                  'there_something_wrong'.tr(),
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(height: 20),
                              ],
                            ),
                          );
                        } else {
                          return Shimmer.fromColors(
                            baseColor: Colors.grey.shade300,
                            highlightColor: Colors.white,
                            period: Duration(seconds: 2),
                            child: Container(
                              margin: EdgeInsets.symmetric(vertical: 20),
                              width: size.width * 0.85,
                              height: height * 0.6,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                // boxShadow: [
                                //   BoxShadow(
                                //     color: Colors.grey,
                                //     offset: Offset(0.0, 1.0), //(x,y)
                                //     blurRadius: 4.0,
                                //   ),
                                // ],
                              ),
                            ),
                          );
                        }
                      },
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              );
            } else {
              return SpinKitFadingCircle(
                color: kPrimaryColor,
                size: 35,
              );
            }
          },
        ));
  }
}
