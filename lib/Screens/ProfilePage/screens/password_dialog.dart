import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class PasswordDialog extends StatefulWidget {
  final String? id;
  final String? phone;
  final bool? isEdit;

  const PasswordDialog({super.key, this.id, this.phone, this.isEdit});

  @override
  _PasswordDialogState createState() => _PasswordDialogState();
}

class _PasswordDialogState extends State<PasswordDialog> {
  final passwordController = TextEditingController();

  Future<void> checkPassword() async {
    SecureStorage secureStorage = SecureStorage();

    String phonenumber = await secureStorage.readSecureData('userPhoneNo');
    // String deviceId = 'b779072b08ae39d5';
    String deviceId = await GlobalFunction.getDeviceId();

    Map<String, String?> map;
    if (widget.phone != null) {
      map = {
        'phonenumber': widget.phone,
        'pwd': passwordController.text,
        'device_id': deviceId,
        'token': TOKEN,
      };
    } else {
      map = {
        'phonenumber': phonenumber,
        'pwd': passwordController.text,
        'device_id': deviceId,
        'token': TOKEN,
      };
    }

    print(map);

    // print('calling post LOGIN');

    final response = await http.post(
      Uri.parse('$API_URL/check_user_login'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    var data = json.decode(response.body);

    if (data['Data'] != false) {
      Fluttertoast.showToast(
        msg: 'success'.tr(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
      Navigator.of(context).pop(false);
    } else {
      //
      Fluttertoast.showToast(
        msg: 'signinerror'.tr(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
      Navigator.of(context).pop(true);
    }
  }

  bool show = false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      scrollable: true,
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisSize:
          //     MainAxisSize.min,
          children: [
            Center(
              child: Text(
                'passwordrequired'.tr(),
                style: TextStyle(
                  color: Color(0xFF505660),
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
            // SizedBox(height: 15),
            // Center(
            //   child: Text(
            //     'Please enter your\npassword to proceed',
            //     style: TextStyle(
            //       color: Color(0xFF505660),
            //       fontWeight: FontWeight.bold,
            //       // color: Color(0xFF898DA1),
            //       // fontStyle: FontStyle.italic,
            //     ),
            //     textAlign: TextAlign.center,
            //   ),
            // ),
            SizedBox(height: 15),
            TextField(
              controller: passwordController,
              decoration: InputDecoration(
                hintText: "password".tr(),
                suffixIcon: IconButton(
                  onPressed: () {
                    setState(() {
                      show = !show;
                    });
                  },
                  icon: Icon(
                    show
                        ? FontAwesomeIcons.solidEye
                        : FontAwesomeIcons.solidEyeSlash,
                    size: 17.5,
                  ),
                ),
              ),
              obscureText: show ? false : true,
            ),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'cancel'.tr(),
                      style: TextStyle(
                        color: Color(0xFF505660),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 15),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: Color(0xFFEF0056),
                    ),
                    onPressed: () {
                      checkPassword();
                    },
                    child: Text(
                      'proceed'.tr(),
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  // Widget build(BuildContext context) {
  //   return AlertDialog(
  //     actions: [
  //       RaisedButton(
  //         shape:
  //             RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
  //         color: kPrimaryColor,
  //         onPressed: () => checkPassword(),
  //         child: Text('show'.tr()),
  //       ),
  //     ],
  //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
  //     title: Text('password'.tr()),
  //     content: TextField(
  //       controller: passwordController,
  //       decoration: InputDecoration(
  //         hintText: "enter_your_password".tr(),
  //         suffixIcon: IconButton(
  //           onPressed: () {
  //             setState(() {
  //               show = !show;
  //             });
  //           },
  //           icon: Icon(
  //             show ? FontAwesomeIcons.solidEye : FontAwesomeIcons.solidEyeSlash,
  //             size: 17.5,
  //           ),
  //         ),
  //       ),
  //       obscureText: show ? false : true,
  //     ),
  //   );
  // }
}
