import 'dart:ui';

import 'package:date_format/date_format.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/utils/global.dart';

class SelangkahCardLHRWiget extends StatefulWidget {
  SelangkahCardLHRWiget({Key? key}) : super(key: key);

  @override
  State<SelangkahCardLHRWiget> createState() => _SelangkahCardLHRWigetState();
}

class _SelangkahCardLHRWigetState extends State<SelangkahCardLHRWiget> {
  bool accordiance = false;
  String _onPressed = 'records';
  bool mask = false;
  bool firstTime = true;

  @override
  void initState() {
    GlobalFunction.screenJourney('49');
    context.read<HealthRecordCubit>().getHealthRecord();
    context.read<BackgroundIllnessCubit>().getBackgroundIllness();
    context.read<AppointmentCubit>().getAppointment();
    context.read<ReferralLetterCubit>().getReferLetter();
    context.read<TestResultCubit>().getTestResult();
    context.read<MedicalCertificatesCubit>().getMedicalCert();
    context.read<PrescriptionsCubit>().getPrescriptions();
    context.read<VaccineCertificatesCubit>().getVaccineCCert();
    context.read<InvoiceCubit>().getInvoice();
    context.read<ArchivedRecordCubit>().getArchivedRecord();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Column(
      children: [
        //appointment reminder
        BlocBuilder<AppointmentCubit, AppointmentState>(
          builder: (context, state) {
            if (state is AppointmentLoaded) {
              return UpcomingApp(
                listAppointment: state.listAppointment,
              );
            } else {
              return Container();
            }
          },
        ),

        //exclamation mark explanation box
        BlocBuilder<HealthRecordCubit, HealthRecordState>(
          builder: (context, state) {
            if (state is HealthRecordLoaded) {
              return WarningMessage(physicalRecord: state.physicalRecord);
            } else {
              return Container();
            }
          },
        ),

        //follow up appt message
        BlocBuilder<AppointmentCubit, AppointmentState>(
          builder: (context, state) {
            if (state is AppointmentLoaded) {
              if (state.bookAppointment.data!.length != 0) {
                return FollowUpAppMessage();
              } else {
                return Container();
              }
            } else {
              return Container();
            }
          },
        ),

        //tab bar (record/archived)
        Container(
          margin: EdgeInsets.all(15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    _onPressed = 'records';
                    mask = false;
                    firstTime = true;
                  });
                },
                child: Container(
                  padding: EdgeInsets.all(15),
                  width: width * 0.3,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        bottomLeft: Radius.circular(20)),
                    border: Border.all(
                      color: Color(0xFFEEF5FF),
                    ),
                    color: _onPressed == 'records'
                        ? Color(0xFFEF0056)
                        : Colors.white,
                  ),
                  child: Text(
                    'record'.tr(),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: _onPressed == 'records'
                            ? Colors.white
                            : Colors.black),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              BlocBuilder<ArchivedRecordCubit, ArchivedRecordState>(
                builder: (context, state) {
                  if (state is ArchivedRecordLoaded) {
                    return GestureDetector(
                      onTap: () async {
                        setState(() {
                          _onPressed = 'archive';
                        });

                        if (state.archivedBackgroundIllness.data!.length != 0 ||
                            state.archivedAppointment.data!.length != 0 ||
                            state.archivedReferralLetter != null ||
                            state.archivedLabResult.data!.length != 0 ||
                            state.archivedMedicalCert != null ||
                            state.archivedPrescription != null ||
                            state.archivedVaccineCert != null ||
                            state.archivedInvoice != null) {
                          if (firstTime)
                            showDialog(
                              context: context,
                              builder: (context) {
                                return PasswordDialog();
                              },
                            ).then((value) {
                              if (value != null) {
                                setState(() {
                                  mask = value;
                                  if (!mask) {
                                    firstTime = false;
                                  }
                                  if (mask) {
                                    _onPressed = 'records';
                                    firstTime = true;
                                  }
                                });
                              } else {
                                setState(() {
                                  mask = true;
                                  _onPressed = 'records';
                                  firstTime = true;
                                });
                              }
                            });
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.all(15),
                        width: width * 0.3,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(20),
                              bottomRight: Radius.circular(20)),
                          border: Border.all(
                            color: Color(0xFFEEF5FF),
                          ),
                          color: _onPressed == 'archive'
                              ? Color(0xFFEF0056)
                              : Colors.white,
                        ),
                        child: Text(
                          'archive'.tr(),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: _onPressed == 'archive'
                                  ? Colors.white
                                  : Colors.black),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    );
                  } else {
                    return GestureDetector(
                      onTap: () async {},
                      child: Container(
                        padding: EdgeInsets.all(15),
                        width: width * 0.3,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(20),
                              bottomRight: Radius.circular(20)),
                          border: Border.all(
                            color: Color(0xFFEEF5FF),
                          ),
                          color: _onPressed == 'archive'
                              ? Color(0xFFEF0056)
                              : Colors.white,
                        ),
                        child: Text(
                          'archive'.tr(),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: _onPressed == 'archive'
                                  ? Colors.white
                                  : Colors.black),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    );
                  }
                },
              ),
            ],
          ),
        ),
        if (_onPressed == 'records') ...[
          SizedBox(height: 10),
          PhysicalDataAccordian(),
          SizedBox(height: 10),
          BackgroundIllnessAccordian(),
          SizedBox(height: 10),
          AppointmentAccordian(),
          SizedBox(height: 10),
          ReferralLettersAccordian(),
          SizedBox(height: 10),
          ResultsAccordian(),
          SizedBox(height: 10),
          MedicalCertificatesAccordian(),
          SizedBox(height: 10),
          PrescriptionsAccordian(),
          SizedBox(height: 10),
          VaccineCertificateAccordian(),
          SizedBox(height: 10),
          InvoicesAccordian(),
          SizedBox(height: 15),
        ],
        if (_onPressed == 'archive') ...[
          BlocBuilder<ArchivedRecordCubit, ArchivedRecordState>(
            builder: (context, state) {
              if (state is ArchivedRecordLoaded) {
                if (state.archivedBackgroundIllness.data!.length != 0 ||
                    state.archivedAppointment.data!.length != 0 ||
                    state.archivedReferralLetter != null ||
                    state.archivedLabResult.data!.length != 0 ||
                    state.archivedMedicalCert != null ||
                    state.archivedPrescription != null ||
                    state.archivedVaccineCert != null ||
                    state.archivedInvoice != null) {
                  return Stack(
                    children: [
                      Column(
                        children: [
                          if (state.archivedBackgroundIllness.data!.length !=
                              0) ...[
                            SizedBox(height: 10),
                            BackgroundIllnessArchived(),
                          ],
                          if (state.archivedAppointment.data!.length != 0) ...[
                            SizedBox(height: 10),
                            AppointmentArchived(),
                          ],
                          if (state.archivedReferralLetter != null) ...[
                            SizedBox(height: 10),
                            ReferralLetterArchived(),
                          ],
                          if (state.archivedLabResult.data!.length != 0) ...[
                            SizedBox(height: 10),
                            ResultsArchived(),
                          ],
                          if (state.archivedMedicalCert != null) ...[
                            SizedBox(height: 10),
                            MedicalCertificatesArchived(),
                          ],
                          if (state.archivedPrescription != null) ...[
                            SizedBox(height: 10),
                            PrescriptionsArchived(),
                          ],
                          if (state.archivedVaccineCert != null) ...[
                            SizedBox(height: 10),
                            VaccineCertificateArchived(),
                          ],
                          if (state.archivedInvoice != null) ...[
                            SizedBox(height: 10),
                            InvoiceArchived(),
                          ],
                          SizedBox(height: 15),
                        ],
                      ),
                      if (mask)
                        Positioned.fill(
                          child: ClipRect(
                            child: BackdropFilter(
                              filter:
                                  ImageFilter.blur(sigmaX: 3.5, sigmaY: 3.5),
                              child: Container(
                                color: Colors.black.withOpacity(0),
                              ),
                            ),
                          ),
                        )
                    ],
                  );
                } else {
                  return Center(
                    child: Column(
                      children: [
                        SizedBox(height: 10),
                        Text(
                          'No record available',
                          style: TextStyle(
                            color: Color(0xFFA9ABB5),
                          ),
                        ),
                        SizedBox(height: 10),
                      ],
                    ),
                  );
                }
              } else {
                return Center(
                  child: Column(
                    children: [
                      SizedBox(height: 10),
                      SpinKitFadingCircle(
                        color: Color(0xFFEF0056),
                        size: 20,
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                );
              }
            },
          ),
        ],
      ],
    );
  }
}

class FollowUpAppMessage extends StatelessWidget {
  const FollowUpAppMessage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width * 0.85,
      margin: EdgeInsets.all(10),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
            decoration: BoxDecoration(
              border: Border.all(
                // width: 5,
                color: Color(0xFFFFA800),
              ),
              borderRadius: BorderRadius.circular(12),
              color: Color(0xFFFFEFD0),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 1,
                  child: Image.asset("assets/images/lhr/alert.png",
                      width: 35, height: 35),
                ),
                SizedBox(width: 15),
                Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'further_consultation'.tr(),
                          style: TextStyle(
                            color: Color(0xFFFFA800),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          'further_consultation_desc'.tr(),
                          style: TextStyle(
                            color: Color(0xFFFFA800),
                          ),
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class WarningMessage extends StatefulWidget {
  final PhysicalRecord physicalRecord;

  const WarningMessage({super.key, required this.physicalRecord});

  @override
  State<WarningMessage> createState() => _WarningMessageState();
}

class _WarningMessageState extends State<WarningMessage> {
  bool message = false;
  @override
  void initState() {
    if (widget.physicalRecord.data!.length != 0) {
      for (var i = 0; i < widget.physicalRecord.data!.length; i++) {
        if (widget.physicalRecord.data![i]!.statusWarning == 2) {
          message = true;
        }
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    if (message) {
      return Container(
        width: width * 0.85,
        margin: EdgeInsets.all(10),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
              decoration: BoxDecoration(
                border: Border.all(
                  // width: 5,
                  color: Color(0xFFFF4588),
                ),
                borderRadius: BorderRadius.circular(12),
                color: Color(0xFFFFEAF1),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 1,
                    child: Image.asset("assets/images/lhr/exclamation.png",
                        width: 35, height: 35),
                  ),
                  SizedBox(width: 15),
                  Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'critical_levels'.tr(),
                            style: TextStyle(
                              color: Color(0xFFEF0056),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 5),
                          Text(
                            'critical_levels_desc'.tr(),
                            style: TextStyle(
                              color: Color(0xFFEF0056),
                            ),
                          ),
                        ],
                      )),
                ],
              ),
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }
}

class UpcomingApp extends StatefulWidget {
  final GetAppointment listAppointment;

  const UpcomingApp({super.key, required this.listAppointment});

  @override
  State<UpcomingApp> createState() => _UpcomingAppState();
}

class _UpcomingAppState extends State<UpcomingApp> {
  List<DateTime> appDate = [];
  List<DateTime> isAfter = [];
  List<int> indexMax = [];
  bool appointment = false;
  DateTime? maxDate;

  @override
  void initState() {
    if (widget.listAppointment.data!.length != 0) {
      for (var i = 0; i < widget.listAppointment.data!.length; i++) {
        if (widget.listAppointment.data![i].date != null) {
          appDate.add(widget.listAppointment.data![i].date!);
          if (widget.listAppointment.data![i].date!.isAfter(DateTime.now())) {
            isAfter.add(widget.listAppointment.data![i].date!);
            appointment = true;
            print('Appointment is: $appointment');
            // print('${isAfter[i]} is before ${DateTime.now()}');
          }
        }
      }

      if (appointment) {
        maxDate = isAfter[0];
        isAfter.forEach((DateTime date) {
          if (date.isBefore(maxDate!)) {
            maxDate = date;
          }
        });

        for (var i = 0; i < widget.listAppointment.data!.length; i++) {
          if (widget.listAppointment.data![i].date == maxDate) {
            indexMax.add(i);
          }
        }
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(maxDate);
    print(indexMax.length);
    print(indexMax);
    double width = MediaQuery.of(context).size.width;
    if (appointment) {
      return Container(
        width: width * 0.85,
        margin: EdgeInsets.all(10),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'upcoming_appointment'.tr(),
                  style: TextStyle(
                    color: Color(0xFF5F6782),
                    fontSize: 16,
                  ),
                ),
                SizedBox(width: 10),
                Icon(
                  FontAwesomeIcons.circleExclamation,
                  size: 20,
                  color: Color(0xFFEF0056),
                ),
              ],
            ),
            SizedBox(height: 15),
            Container(
              padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
              decoration: BoxDecoration(
                border: Border.all(
                  // width: 5,
                  // color: Color(0xFF9EDCFF),
                  color: Color(0xFF4B75A6),
                ),
                borderRadius: BorderRadius.circular(12),
                color: Color(0xFFE8F8FF),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 1,
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      // mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          formatDate(maxDate!, [
                            dd,
                            // formatDate(widget.getAppointment.data[0].date, [
                            //   dd,
                          ]),
                          style: TextStyle(
                              color: Color(0xFF4B75A6),
                              fontSize: 28,
                              fontWeight: FontWeight.bold),
                          // textAlign: TextAlign.center,
                        ),
                        Text(
                          formatDate(maxDate!, [
                            M,
                          ]),
                          style: TextStyle(
                              color: Color(0xFF4B75A6),
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                          // textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    flex: 3,
                    child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: indexMax.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.listAppointment.data![indexMax[index]]
                                          .timeEnd ==
                                      ''
                                  ? '${widget.listAppointment.data![indexMax[index]].timeStart!.substring(0, 5)}'
                                  : '${widget.listAppointment.data![indexMax[index]].timeStart!.substring(0, 5)} - ${widget.listAppointment.data![indexMax[index]].timeEnd!.substring(0, 5)}',
                              style: TextStyle(
                                color: Color(0xFF4B75A6),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              widget.listAppointment.data![indexMax[index]]
                                  .testName!,
                              style: TextStyle(
                                // color: Color(0xFF76ACDE),
                                color: Color(0xFF4B75A6),
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              widget.listAppointment.data![indexMax[index]]
                                  .location!,
                              style: TextStyle(
                                // color: Color(0xFF76ACDE),
                                color: Color(0xFF4B75A6),
                              ),
                            ),
                            SizedBox(height: 15),
                          ],
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }
}
