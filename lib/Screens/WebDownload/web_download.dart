import 'dart:convert';
import 'dart:io';

import 'package:android_path_provider/android_path_provider.dart';
import 'package:app_settings/app_settings.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';

import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:easy_localization/easy_localization.dart';

class WebDownload extends StatefulWidget {
  final String url;

  const WebDownload({super.key, required this.url});

  @override
  _WebDownloadState createState() => _WebDownloadState();
}

class _WebDownloadState extends State<WebDownload> {
  InAppWebViewController? webView;

  bool isLoaded = false;

  @override
  void dispose() {
    // timer.cancel();
    super.dispose();
  }

  bool redirected = false;

  @override
  void initState() {
    init();
    super.initState();
  }

  void init() async {
    if (Platform.isIOS) {
      Navigator.of(context).pop();
      launchUrl(
        Uri.parse(widget.url),
        mode: LaunchMode.externalApplication,
      );
    }
  }

  _createFileFromBase64(
      String base64content, String fileName, String yourExtension) async {
    var bytes = base64Decode(base64content.replaceAll('\n', ''));
    final output = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    final file = File("${output!.path}/$fileName.$yourExtension");
    await file.writeAsBytes(bytes.buffer.asUint8List());
    print("${output.path}/$fileName.$yourExtension");
    await OpenFilex.open("${output.path}/$fileName.$yourExtension");
    // webView.goBack();
    Navigator.of(context).pop();

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    print('url is ${widget.url}');
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/images/Scaffold/selangkah_logo.png",
          width: width * 0.45,
        ),
      ),
      body: Stack(
        children: [
          InAppWebView(
            initialUrlRequest: URLRequest(url: Uri.parse(widget.url)),
            onWebViewCreated: (controller) {
              webView = controller;
              print("onWebViewCreated");

              Fluttertoast.showToast(
                msg: 'please_wait_download'.tr(),
                toastLength: Toast.LENGTH_LONG,
              );

              controller.addJavaScriptHandler(
                handlerName: 'blobToBase64Handler',
                callback: (data) async {
                  if (data.isNotEmpty) {
                    final String receivedFileInBase64 = data[0];
                    final String receivedMimeType = data[1];

                    print('receivedMimeType is $receivedMimeType');

                    // NOTE: create a method that will handle your extensions
                    List<String> splittedExtension =
                        receivedMimeType.split('/');
                    final String yourExtension =
                        splittedExtension.last; // 'pdf'

                    final String name = splittedExtension.first;

                    DateTime now = DateTime.now();

                    String dateNow = formatDate(now, [dd, mm, yyyy, HH, nn]);

                    _createFileFromBase64(
                        receivedFileInBase64, '$dateNow', yourExtension);
                  }
                },
              );
            },
            androidOnPermissionRequest: (controller, origin, resources) async {
              return PermissionRequestResponse(
                resources: resources,
                action: PermissionRequestResponseAction.GRANT,
              );
            },

            initialOptions: InAppWebViewGroupOptions(
              crossPlatform: InAppWebViewOptions(
                useShouldOverrideUrlLoading: true,
                mediaPlaybackRequiresUserGesture: false,
                useOnDownloadStart: true,
              ),
              android: AndroidInAppWebViewOptions(
                hardwareAcceleration: true,
              ),
              ios: IOSInAppWebViewOptions(
                allowsInlineMediaPlayback: true,
              ),
            ),
            onConsoleMessage: (controller, consoleMessage) async {},
            onLoadStop: (controller, url) async {
              setState(() {
                isLoaded = true;
              });
            },
            onFindResultReceived: (controller, activeMatchOrdinal,
                numberOfMatches, isDoneCounting) async {},
            onDownloadStartRequest: (controller, onDownloadStartRequest) async {
              final status = await Permission.storage.request().isGranted;
              // final status2 =
              //     await Permission.manageExternalStorage.request().isGranted;

              if (status) {
                //base64 downlaoder
                // var jsContent =
                //     await rootBundle.loadString("assets/js/base64.js");
                // await controller.evaluateJavascript(
                //     source: jsContent.replaceAll("blobUrlPlaceholder",
                //         onDownloadStartRequest.url.toString()));

                //flutter downloader
                String path = '';

                if (Platform.isIOS) {
                  path = (await getApplicationDocumentsDirectory()).path;
                } else {
                  // path = (await getExternalStorageDirectory())!.path;

                  path = await AndroidPathProvider.downloadsPath;
                  path = '$path/selangkah';

                  if (!await Directory(path).exists()) {
                    Directory myNewDir =
                        await Directory(path).create(recursive: true);
                    path = myNewDir.path;
                  }
                }

                Navigator.of(context).pop();

                String dateNow =
                    formatDate(DateTime.now(), [dd, mm, yyyy, HH, nn]);

                try {
                  await FlutterDownloader.enqueue(
                    fileName: '$dateNow.pdf',
                    url: onDownloadStartRequest.url.toString(),
                    savedDir: path,
                    showNotification:
                        true, // show download progress in status bar (for Android)
                    openFileFromNotification:
                        true, // click on notification to open downloaded file (for Android)
                  ).then((value) async {
                    bool waitTask = true;
                    while (waitTask) {
                      String query =
                          "SELECT * FROM task WHERE task_id='" + value! + "'";
                      var _tasks =
                          await FlutterDownloader.loadTasksWithRawQuery(
                              query: query);

                      String taskStatus = _tasks![0].status.toString();
                      int taskProgress = _tasks[0].progress;
                      if (taskStatus == "DownloadTaskStatus(3)" &&
                          taskProgress == 100) {
                        waitTask = false;
                        // path = path + '/' + _tasks[0].filename!;
                      }

                      // print('path bawah is $path');

                      await Future.delayed(Duration(seconds: 1));
                      await FlutterDownloader.open(taskId: value);
                    }

                    // print('opening file at $path');
                    // OpenFilex.open(path);
                  });
                } on Exception catch (e) {
                  print(e);
                }
              } else {
                Alert(
                  onWillPopActive: false,
                  context: context,
                  type: AlertType.info,
                  title: "permission_needed".tr(),
                  desc: 'storage_permission_needed'.tr(),
                  buttons: [
                    DialogButton(
                      child: Text(
                        'open_settings_storage'.tr(),
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      onPressed: AppSettings.openAppSettings,
                      color: kPrimaryColor,
                      radius: BorderRadius.circular(20),
                    )
                  ],
                ).show();
              }
            },
            // shouldOverrideUrlLoading: (controller, navigationAction) async {
            //   var uri = navigationAction.url;

            //   if (![
            //     "http",
            //     "https",
            //     "file",
            //     "chrome",
            //     "data",
            //     "javascript",
            //     "about",
            //   ].contains(uri)) {
            //     print('url override is $uri');
            //     if (await canLaunch(uri)) {
            //       // Launch the App
            //       launch(uri);
            //       // and cancel the request
            //       // return ShouldOverrideUrlLoadingAction.CANCEL;
            //     }
            //   } else {
            //     print('tak masuk override');
            //   }

            //   return ShouldOverrideUrlLoadingAction.ALLOW;
            // },
          ),
          if (!isLoaded)
            SpinKitRing(
              color: kPrimaryColor,
              size: 30,
              lineWidth: 5,
            ),
        ],
      ),
    );
  }
}
