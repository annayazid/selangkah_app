import 'dart:convert';
import 'dart:io';

import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:selangkah_new/utils/constants.dart';

class SelangkahWebview extends StatefulWidget {
  final String appBarTitle;
  final String url;

  const SelangkahWebview(
      {super.key, required this.appBarTitle, required this.url});

  @override
  State<SelangkahWebview> createState() => _SelangkahWebviewState();
}

class _SelangkahWebviewState extends State<SelangkahWebview> {
  late InAppWebViewController webView;

  bool isLoaded = false;

  _createFileFromBase64(
      String base64content, String fileName, String yourExtension) async {
    var bytes = base64Decode(base64content.replaceAll('\n', ''));
    final output = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    final file = File("${output!.path}/$fileName.$yourExtension");
    await file.writeAsBytes(bytes.buffer.asUint8List());
    print("${output.path}/$fileName.$yourExtension");
    await OpenFilex.open("${output.path}/$fileName.$yourExtension");
    // webView.goBack();

    setState(() {});
  }

  @override
  void initState() {
    init();
    super.initState();
  }

  void init() async {
    // get the WebStorageManager instance
    WebStorageManager webStorageManager = WebStorageManager.instance();

    if (Platform.isAndroid) {
      // if current platform is Android, delete all data.
      await webStorageManager.android.deleteAllData();
    } else if (Platform.isIOS) {
      // if current platform is iOS, delete all data for "flutter.dev".
      var records = await webStorageManager.ios
          .fetchDataRecords(dataTypes: IOSWKWebsiteDataType.values);
      var recordsToDelete = <IOSWKWebsiteDataRecord>[];
      for (var record in records) {
        if (record.displayName == 'flutter.dev') {
          recordsToDelete.add(record);
        }
      }
      await webStorageManager.ios.removeDataFor(
          dataTypes: IOSWKWebsiteDataType.values, dataRecords: recordsToDelete);
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;

    print('url is ${widget.url}');
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/images/Scaffold/selangkah_logo.png",
          width: width * 0.45,
        ),
      ),
      body: Column(
        children: [
          AppBar(
            centerTitle: true,
            automaticallyImplyLeading: true,
            backgroundColor: kPrimaryColor,
            title: Text(
              widget.appBarTitle,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 15.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontFamily: "Roboto",
              ),
            ),
          ),
          Container(
            height: height - AppBar().preferredSize.height,
            width: width,
            child: Stack(
              children: [
                InAppWebView(
                  initialUrlRequest: URLRequest(
                    url: Uri.parse(widget.url),
                  ),
                  androidOnPermissionRequest:
                      (controller, origin, resources) async {
                    return PermissionRequestResponse(
                      resources: resources,
                      action: PermissionRequestResponseAction.GRANT,
                    );
                  },
                  initialOptions: InAppWebViewGroupOptions(
                    crossPlatform: InAppWebViewOptions(
                      useShouldOverrideUrlLoading: true,
                      mediaPlaybackRequiresUserGesture: false,
                      useOnDownloadStart: true,
                    ),
                    android: AndroidInAppWebViewOptions(
                      hardwareAcceleration: true,
                    ),
                    ios: IOSInAppWebViewOptions(
                      allowsInlineMediaPlayback: true,
                    ),
                  ),
                  onWebViewCreated: (InAppWebViewController controller) async {
                    webView = controller;

                    print("onWebViewCreated");

                    controller.addJavaScriptHandler(
                      handlerName: 'blobToBase64Handler',
                      callback: (data) async {
                        if (data.isNotEmpty) {
                          final String receivedFileInBase64 = data[0];
                          final String receivedMimeType = data[1];

                          print('receivedMimeType is $receivedMimeType');

                          // NOTE: create a method that will handle your extensions
                          List<String> splittedExtension =
                              receivedMimeType.split('/');
                          final String yourExtension =
                              splittedExtension.last; // 'pdf'

                          DateTime now = DateTime.now();

                          String dateNow =
                              formatDate(now, [dd, mm, yyyy, HH, nn]);

                          _createFileFromBase64(receivedFileInBase64,
                              'YS_receipt_$dateNow', yourExtension);
                        }
                      },
                    );
                  },
                  onConsoleMessage: (controller, consoleMessage) {
                    // print(consoleMessage);
                  },
                  onLoadStop: (controller, url) {
                    setState(() {
                      isLoaded = true;
                    });
                  },
                  onDownloadStartRequest:
                      (controller, downloadStartRequest) async {
                    print('masuk download request');
                    var jsContent =
                        await rootBundle.loadString("assets/js/base64.js");
                    await controller.evaluateJavascript(
                        source: jsContent.replaceAll("blobUrlPlaceholder",
                            downloadStartRequest.url.toString()));
                  },
                ),
                if (!isLoaded)
                  SpinKitRing(
                    color: kPrimaryColor,
                    size: 30,
                    lineWidth: 5,
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
