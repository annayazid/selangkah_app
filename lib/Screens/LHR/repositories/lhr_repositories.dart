import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class LhrRepositories {
  //get identity
  static Future<ProfileLHR> getProfile() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id': id,
      'token': TOKEN,
    };

    //print('calling post get profile');

    final response = await http.post(
      Uri.parse('$API_URL/check_user_profile'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    ProfileLHR profile = profileFromJson(response.body);

    return profile;
  }

  static Future<PhysicalRecord> getHealthRecord() async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');
    String? language = GlobalVariables.language;

    final Map<String, String?> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'language': language,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/get_health_record'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    PhysicalRecord physicalRecord = physicalRecordFromJson(response.body);

    // PhysicalRecord physicalRecord = physicalRecordFromJson(
    //     '{"Code":200,"Data":[{"Sequence":"1","parameter":"Weight","unit":"kg","show":"1","data_input":"50 kg","red_flag":"0","color_flag":"#18A934","redflag_description":"Normal","color":"ffc8dd","logo":"https:\/\/app.selangkah.my\/cache\/general\/lhr\/Weight%20Scale.png"},{"Sequence":"2","parameter":"Height","unit":"cm","show":"1","data_input":"22 cm","red_flag":"0","color_flag":"#18A934","redflag_description":"Normal","color":"ffc8dd","logo":"https:\/\/app.selangkah.my\/cache\/general\/lhr\/ruler.png"},{"Sequence":"3","parameter":"BMI","unit":"","show":"1","data_input":"1033.06 \/ Obese","red_flag":"1","color_flag":"#D90429","redflag_description":"Obese","color":"ffc8dd","logo":"https:\/\/app.selangkah.my\/cache\/general\/lhr\/scale.png"},{"Sequence":"4","parameter":"Waist Circumference","unit":"cm","show":"1","data_input":"22 cm","red_flag":"0","color_flag":"#18A934","redflag_description":"Normal","color":"ffc8dd","logo":"https:\/\/app.selangkah.my\/cache\/general\/lhr\/waist.png"},{"Sequence":"5","parameter":"Blood Glucose","unit":"mmol\/L","show":"1","data_input":"100 mmol\/L","red_flag":"1","color_flag":"#D90429","redflag_description":"Not Normal","color":"ffc8dd","logo":"https:\/\/app.selangkah.my\/cache\/general\/lhr\/bloodglucose.png"},{"Sequence":"7","parameter":"Blood Pressure","unit":"mmHg","show":"1","data_input":"23 \/ 21 mmHg","red_flag":"1","color_flag":"#D90429","redflag_description":"Low Blood Pressure","color":"ffc8dd","logo":"https:\/\/app.selangkah.my\/cache\/general\/lhr\/BloodPressure.png"},{"Sequence":"8","parameter":"Heart Rate","unit":"bpm","show":"1","data_input":"22 bpm","red_flag":"0","color_flag":"#18A934","redflag_description":"Normal","color":"ffc8dd","logo":"https:\/\/app.selangkah.my\/cache\/general\/lhr\/heartrate.png"}]}');

    return physicalRecord;
  }

  static Future<PhysicalPatient> getPhysicalRecord() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    final response = await http.post(
      Uri.parse('$SARING_DATA/get_physical_exam'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    PhysicalPatient physicalPatient = physicalPatientFromJson(response.body);

    // PhysicalPatient physicalPatient = physicalPatientFromJson(
    //     '{"Code" : 200,"Data" : [{"Date" : "30-3-2022","Height" : "180 cm","Weight" : "75 kg","Waist Circumference" : "25 inch","Blood Pressure" : "88 mm hg"},{"Date" : "29-3-2022","Height" : "180 cm","Weight" : "70 kg","Waist Circumference" : "24 inch","Blood Pressure" : "90 mm hg"},{"Date" : "28-3-2022","Height" : "180 cm","Weight" : "65 kg","Waist Circumference" : "23 inch","Blood Pressure" : "89 mm hg"}]}');

    return physicalPatient;
  }

  static Future<bool> sendFeedback(
      String testId, String statusCall, String remark) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'test_id': testId,
      'id_status_call': statusCall,
      'remark': remark,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/return_appointment_feedback'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    // dev.log(response.body);
    print('API return_appointment_feedback');
    print(response.body);

    if (response.body.contains('true')) {
      return true;
    } else {
      return false;
    }
  }

  static Future<GetAppointment> getAppointmentList() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    final response = await http.post(
      Uri.parse('$API_LHR/get_appointment'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    // dev.log(response.body);

    GetAppointment getAppointment = getAppointmentFromJson(response.body);

    // GetAppointment getAppointment = getAppointmentFromJson(
    //     '{"Code":200,"Data":[{"id_program_appointment":"67805","test_id":"9","test_name":"Breast Cancer Consultion","location":"SELCARE CLINIC (SENTOSA)","time_start":"09:00:00","time_end":"15:00:00","address":"NO.25, JALAN DATUK DAGANG 39,","date":"2023-08-20","referral":"https://selangorsaring.selangkah.my/saring-admin/referral_letter_gen.php?id_coupon_applicant=151974&sid=82986&follow_up=1&id_user=82986&id_test_type=9","can_change":1,"can_delete":1,"appt_card":"https://selangorsaring.selangkah.my/saring-admin/referral_letter_gen.php?id_coupon_applicant=151974&sid=82986&follow_up=1&id_user=82986&id_test_type=9"}]}');

    return getAppointment;
  }

  static Future<LabResult> getLabResult() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    final response = await http.post(
      Uri.parse('$API_LHR/get_test_result'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );
    print(response.body);
    try {
      LabResult labResult = labResultFromJson(response.body);
      return labResult;
    } catch (e) {
      LabResult labResult = LabResult(code: 200, data: []);
      return labResult;
    }
    // LabResult labResult = labResultFromJson(response.body);

    // LabResult labResult = labResultFromJson(
    //     '{"Code":200,"Data":[{"id_data_saring":"60546","date":"2022-08-20","test_name":"Eye Screening (Saringan Mata)","url":"https://selangorsaring.selangkah.my/saring-admin/eye_result_pdf?sid=36471&id_program_appointment=17886","service_name":"Eye Screening (Saringan Mata)","referral_letter":"","pdf_docu":"","findings":"","red_flag":"1","can_delete":0},{"id_data_saring":"63038","date":"2023-01-31","test_name":"CAP5340142865720144922.jpg","url":"https://app.selangkah.my/storage/lhr/LHRTS0063038.jpg","service_name":"CAP5340142865720144922.jpg","referral_letter":"","pdf_docu":"https://app.selangkah.my/storage/lhr/LHRTS0063038.jpg","findings":"","red_flag":null,"can_delete":1}]}');

    // return labResult;
  }

  static Future<BackgroundIllness> getBackgroundIllness() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    final response = await http.post(
      Uri.parse('$API_LHR/get_background_illness'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    BackgroundIllness backgroundIllness =
        backgroundIllnessFromJson(response.body);

    // BackgroundIllness backgroundIllness = backgroundIllnessFromJson(
    //     '{"Code":200,"Data":["Hypertension (Darah Tinggi) [ Saring 2023 - Self Declaration ]"]}');

    return backgroundIllness;
  }

  static Future<BookAppointment> bookAppointment() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    final response = await http.post(
      Uri.parse('$SARING_DATA/get_book_appointment2'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

// try {

//     BookAppointment appointment = bookAppointmentFromJson(response.body);
//     return appointment;
// } catch (e) {
// }
    BookAppointment appointment = bookAppointmentFromJson(response.body);

    // BookAppointment appointment = bookAppointmentFromJson(
    //     '{"Code":200,"Data":[{"testid":"9","service_name":"NCD-CKD CONSULTATION (Rundingan Am NCD-CKD)","can_warning":1}]}');

    return appointment;
  }

  static Future<RetrieveDocument?> retrieveDocument(String type) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
      'document_type': type,
    };

    final response = await http.post(
      Uri.parse('$API_LHR/get_document_record'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );
    print(response.body);

    var data = json.decode(response.body);

    if (data['Data'] == false) {
      return null;
    } else {
      RetrieveDocument document = retrieveDocumentFromJson(response.body);
      return document;
    }
  }

  static Future<BackgroundIllness> getArchiveBackgroundIllness() async {
    String id = await SecureStorage().readSecureData('userId');

    Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
    };

    print(map);
    print('calling post archive background illness');

    final response = await http.post(
      Uri.parse('$API_LHR/get_background_illness_archive'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    BackgroundIllness backgroundIllness =
        backgroundIllnessFromJson(response.body);
    return backgroundIllness;
  }

  static Future<GetAppointment> getArchiveAppointment() async {
    String id = await SecureStorage().readSecureData('userId');

    Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/get_appointment_archive'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    GetAppointment getAppointment = getAppointmentFromJson(response.body);

    return getAppointment;
  }

  static Future<LabResult> getArchiveLabResult() async {
    String id = await SecureStorage().readSecureData('userId');

    Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/get_test_result_archive'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    LabResult getLabResult = labResultFromJson(response.body);

    return getLabResult;
  }

  static Future<ArchivedRecord?> getArchivedRecord(String docType) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'document_type': docType
    };

    print('calling post get archive record');

    final response = await http.post(
      Uri.parse('$API_LHR/get_archive_record'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    if (response.body.contains('false')) {
      return null;
    } else {
      ArchivedRecord archivedRecord = archivedRecordFromJson(response.body);
      return archivedRecord;
    }
  }

  static Future<bool> unarchiveDoc(
      {String? idDoc, String? idSaring, String? idApp}) async {
    String id = await SecureStorage().readSecureData('userId');

    Map<String, String>? map;

    if (idDoc != null) {
      map = {'token': TOKEN, 'id_selangkah_user': id, 'id_document': idDoc};
    } else if (idSaring != null) {
      map = {
        'token': TOKEN,
        'id_selangkah_user': id,
        'id_data_saring': idSaring
      };
    } else if (idApp != null) {
      map = {
        'token': TOKEN,
        'id_selangkah_user': id,
        'id_program_appointment': idApp
      };
    }
    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/unarchive_document'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    if (response.body.contains('true')) {
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> unarchiveBackgroundIllness({String? illness}) async {
    String id = await SecureStorage().readSecureData('userId');

    Map<String, String?> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'text': illness
    };

    print(map);
    print('calling post unarchived');

    final response = await http.post(
      Uri.parse('$API_LHR/unarchive_background_illness'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    if (response.body.contains('true')) {
      return true;
    } else {
      return false;
    }
  }

  static Future<String> downloadDoc(String docID) async {
    String id = await SecureStorage().readSecureData('userId');
    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
      'id_document': docID,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/download_document'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    // var data = json.decode(response.body);

    // if (data['Data'] == false) {
    //   return null;
    // } else {
    // DownloadDocument url = downloadDocumentFromJson(response.body);
    var data = json.decode(response.body);
    print(data['Data']['document_link']);
    return data['Data']['document_link'];

    // LabResult labResult = labResultFromJson(
    //     '{"Code":200,"Data":[{"test_id":"9","booked":"1","date":["2022-06-18","2022-06-18"],"test_name":["Blood Test (Ujian Darah)","Urine Test (Ujian Air Kencing)"],"red_flag":"1","url":["https://selangorsaring.selangkah.my/saring-admin/lab_report.php?id_coupon_applicant=27074&sid=15132","https://selangorsaring.selangkah.my/saring-admin/lab_report.php?id_coupon_applicant=27075&sid=15132"],"service_name":"NCD-CKD CONSULTATION (Rundingan Am NCD-CKD)","pdf_docu":"","referral_letter":"","findings":"","can_warning":1},{"test_id":"4","booked":"1","date":["2022-06-18"],"test_name":["Cervical Screening (Saringan Serviks)"],"red_flag":"0","url":["https://selangorsaring.selangkah.my/saring-admin/lab_report.php?id_coupon_applicant=27071&sid=15132"],"service_name":"Colposcopy","referral_letter":"","pdf_docu":"","findings":"","can_warning":0},{"test_id":"1","booked":"0","date":["2022-06-18"],"test_name":["Breast Cancer Screening (Saringan Kanser Payudara)"],"red_flag":"1","url":[],"service_name":"Breast Cancer Screening (Saringan Kanser Payudara)","referral_letter":"","pdf_docu":"","findings":"","can_warning":0},{"test_id":"5","booked":"0","date":["2022-06-18"],"test_name":["Colorectal Cancer Screening (Saringan Kanser Kolorektal)"],"red_flag":"0","url":["https://selangorsaring.selangkah.my/saring-admin/lab_report.php?id_coupon_applicant=45750&sid=15132"],"service_name":"Colonoscopy","referral_letter":"","pdf_docu":"","findings":"","can_warning":0},{"test_id":"10","booked":"0","date":["2022-06-18"],"test_name":["Eye Screening (Saringan Mata)"],"red_flag":"0","url":["https://selangorsaring.selangkah.my/saring-admin/eye_result_pdf?sid=15132&id_coupon_applicant=45755"],"service_name":"Eye Screening (Saringan Mata)","referral_letter":"https://selangorsaring.selangkah.my/saring-admin/referral_letter.php?sid=15132&id_coupon_applicant=45755","pdf_docu":"","findings":"$LoremText$LoremText","can_warning":0},{"test_id":"0","booked":"0","date":["2022-08-01"],"test_name":["Colposcopy"],"red_flag":"0","url":[""],"service_name":"Colposcopy","referral_letter":"","pdf_docu":"","findings":"Colposcopy Findings=\\n=","can_warning":0}]}');
  }

  static Future<String> downloadTestDoc(String link) async {
    String id = await SecureStorage().readSecureData('userId');
    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
      'link': link,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/download_document'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    // var data = json.decode(response.body);

    // if (data['Data'] == false) {
    //   return null;
    // } else {
    // DownloadDocument url = downloadDocumentFromJson(response.body);
    var data = json.decode(response.body);
    print(data['Data']['document_link']);
    return data['Data']['document_link'];

    // LabResult labResult = labResultFromJson(
    //     '{"Code":200,"Data":[{"test_id":"9","booked":"1","date":["2022-06-18","2022-06-18"],"test_name":["Blood Test (Ujian Darah)","Urine Test (Ujian Air Kencing)"],"red_flag":"1","url":["https://selangorsaring.selangkah.my/saring-admin/lab_report.php?id_coupon_applicant=27074&sid=15132","https://selangorsaring.selangkah.my/saring-admin/lab_report.php?id_coupon_applicant=27075&sid=15132"],"service_name":"NCD-CKD CONSULTATION (Rundingan Am NCD-CKD)","pdf_docu":"","referral_letter":"","findings":"","can_warning":1},{"test_id":"4","booked":"1","date":["2022-06-18"],"test_name":["Cervical Screening (Saringan Serviks)"],"red_flag":"0","url":["https://selangorsaring.selangkah.my/saring-admin/lab_report.php?id_coupon_applicant=27071&sid=15132"],"service_name":"Colposcopy","referral_letter":"","pdf_docu":"","findings":"","can_warning":0},{"test_id":"1","booked":"0","date":["2022-06-18"],"test_name":["Breast Cancer Screening (Saringan Kanser Payudara)"],"red_flag":"1","url":[],"service_name":"Breast Cancer Screening (Saringan Kanser Payudara)","referral_letter":"","pdf_docu":"","findings":"","can_warning":0},{"test_id":"5","booked":"0","date":["2022-06-18"],"test_name":["Colorectal Cancer Screening (Saringan Kanser Kolorektal)"],"red_flag":"0","url":["https://selangorsaring.selangkah.my/saring-admin/lab_report.php?id_coupon_applicant=45750&sid=15132"],"service_name":"Colonoscopy","referral_letter":"","pdf_docu":"","findings":"","can_warning":0},{"test_id":"10","booked":"0","date":["2022-06-18"],"test_name":["Eye Screening (Saringan Mata)"],"red_flag":"0","url":["https://selangorsaring.selangkah.my/saring-admin/eye_result_pdf?sid=15132&id_coupon_applicant=45755"],"service_name":"Eye Screening (Saringan Mata)","referral_letter":"https://selangorsaring.selangkah.my/saring-admin/referral_letter.php?sid=15132&id_coupon_applicant=45755","pdf_docu":"","findings":"$LoremText$LoremText","can_warning":0},{"test_id":"0","booked":"0","date":["2022-08-01"],"test_name":["Colposcopy"],"red_flag":"0","url":[""],"service_name":"Colposcopy","referral_letter":"","pdf_docu":"","findings":"Colposcopy Findings=\\n=","can_warning":0}]}');
  }

  static Future<bool> deleteDoc({String? idDoc, String? idSaring}) async {
    String id = await SecureStorage().readSecureData('userId');

    Map<String, String?> map;

    if (idSaring == null) {
      map = {'token': TOKEN, 'id_selangkah_user': id, 'id_document': idDoc};
    } else {
      map = {
        'token': TOKEN,
        'id_selangkah_user': id,
        'id_data_saring': idSaring
      };
    }
    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/delete_document'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    if (response.body.contains('true')) {
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> archiveDoc(
      {String? idDoc, String? idSaring, String? idApp}) async {
    String id = await SecureStorage().readSecureData('userId');

    Map<String, String>? map;

    if (idDoc != null) {
      map = {'token': TOKEN, 'id_selangkah_user': id, 'id_document': idDoc};
    } else if (idSaring != null) {
      map = {
        'token': TOKEN,
        'id_selangkah_user': id,
        'id_data_saring': idSaring
      };
    } else if (idApp != null) {
      map = {
        'token': TOKEN,
        'id_selangkah_user': id,
        'id_program_appointment': idApp
      };
    }
    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/archive_document'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    if (response.body.contains('true')) {
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> archiveBackgroundIllness({String? illness}) async {
    String id = await SecureStorage().readSecureData('userId');

    Map<String, String?> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'text': illness
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/archive_background_illness'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    if (response.body.contains('true')) {
      return true;
    } else {
      return false;
    }
  }

  static Future<RecordTrendData> getRecordTrend(String title) async {
    String? language = GlobalVariables.language;
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String?> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'param_name': title,
      'language': language,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/get_health_record_single'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print('calling here');
    print(response.body);

    RecordTrendData recordTrendData = recordTrendDataFromJson(response.body);

    return recordTrendData;
  }

  static Future<InputAddMeasurement> getInput(String title) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'record': title,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/program_form'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    InputAddMeasurement input = inputAddMeasurementFromJson(response.body);

    return input;
  }

  static Future<DropdownAddMeasurement> getDropdown() async {
    String? language = GlobalVariables.language;
    final Map<String, String?> map = {
      'token': TOKEN,
      'language': language,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/dropdown_record'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    DropdownAddMeasurement dropdown =
        dropdownAddMeasurementFromJson(response.body);

    return dropdown;
  }

  static Future<void> sendRecord(
      String answer, String date, String time) async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'json_answer': answer,
      'date': date,
      'time': time,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/return_answer'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
  }

  static Future<bool> deleteRecord(
      String param, String date, String time) async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'record': param,
      'date': date,
      'time': time,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/delete_record'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
    if (response.body.contains('true')) {
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> uploadDocumment(
    String documentType,
    File documentFile,
    String fileName,
  ) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'document_type': documentType,
      'filename': fileName,
    };

    log(map.toString());

    print('size of the file is ${documentFile.lengthSync()} ');

    if (documentFile.lengthSync() < 10380902.4) {
      var request =
          http.MultipartRequest('POST', Uri.parse('$API_LHR/upload_document'));
      request.fields.addAll(map);
      request.files.add(
        await http.MultipartFile.fromPath('file', documentFile.path),
      );

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        log(await response.stream.bytesToString());
        return true;
      } else {
        log(response.statusCode.toString());
        log(response.reasonPhrase!);
        return false;
      }
    } else {
      print('file too large');
      return false;
    }
  }

  static Future<File?> compressImage(File file) async {
    try {
      final compressedFile = await FlutterNativeImage.compressImage(file.path,
          quality: 100, percentage: 10);
      return File(compressedFile.path);
    } catch (e) {
      return null; //If any error occurs during compression, the process is stopped.
    }
  }

  static Future<String> shareCode(String? idDoc, String? idSaring) async {
    String id = await SecureStorage().readSecureData('userId');
    final Map<String, String> map;
    if (idSaring != null) {
      map = {
        'id_selangkah_user': id,
        'token': TOKEN,
        'id_data_saring': idSaring
      };
    } else {
      map = {
        'id_selangkah_user': id,
        'token': TOKEN,
        'id_cms_document_user': idDoc!
      };
    }

    print(map);

    final response = await http.post(
      Uri.parse('$API_LHR/get_share_code'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );
    print(response.body);

    var data = json.decode(response.body);
    String code = data['Data']['code'];
    return code;
  }
}
