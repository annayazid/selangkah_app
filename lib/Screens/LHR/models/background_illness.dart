// To parse this JSON data, do
//
//     final backgroundIllness = backgroundIllnessFromJson(jsonString);

import 'dart:convert';

BackgroundIllness backgroundIllnessFromJson(String str) =>
    BackgroundIllness.fromJson(json.decode(str));

String backgroundIllnessToJson(BackgroundIllness data) =>
    json.encode(data.toJson());

class BackgroundIllness {
  BackgroundIllness({
    this.code,
    this.data,
  });

  int? code;
  List<String>? data;

  factory BackgroundIllness.fromJson(Map<String, dynamic> json) =>
      BackgroundIllness(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<String>.from(json["Data"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null ? null : List<dynamic>.from(data!.map((x) => x)),
      };
}
