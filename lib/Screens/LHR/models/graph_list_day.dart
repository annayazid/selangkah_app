class DayData {
  int? month;
  int? year;
  List<GraphDataDay>? graphData;
  DayData({
    this.month,
    this.year,
    this.graphData,
  });
}

class GraphDataDay {
  DateTime? date;
  int? day;
  double? record;
  GraphDataDay({
    this.date,
    this.day,
    this.record,
  });
}
