// To parse this JSON data, do
//
//     final bookAppointment = bookAppointmentFromJson(jsonString);

import 'dart:convert';

BookAppointment bookAppointmentFromJson(String str) =>
    BookAppointment.fromJson(json.decode(str));

class BookAppointment {
  BookAppointment({
    this.code,
    this.data,
  });

  int? code;
  List<BookAppointmentData>? data;

  factory BookAppointment.fromJson(Map<String, dynamic> json) =>
      BookAppointment(
        code: json["Code"],
        data: List<BookAppointmentData>.from(
            json["Data"].map((x) => BookAppointmentData.fromJson(x))),
      );
}

class BookAppointmentData {
  BookAppointmentData({
    this.testid,
    this.serviceName,
    this.canWarning,
    this.showDate,
  });

  String? testid;
  String? serviceName;
  int? canWarning;
  int? showDate;

  factory BookAppointmentData.fromJson(Map<String, dynamic> json) =>
      BookAppointmentData(
        testid: json["testid"],
        serviceName: json["service_name"],
        canWarning: json["can_warning"],
        showDate: json["show_date"],
      );

  Map<String, dynamic> toJson() => {
        "testid": testid,
        "service_name": serviceName,
        "can_warning": canWarning,
        "show_date": showDate,
      };
}
