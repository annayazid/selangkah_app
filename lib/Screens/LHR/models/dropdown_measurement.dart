// To parse this JSON data, do
//
//     final dropdownAddMeasurement = dropdownAddMeasurementFromJson(jsonString);

import 'dart:convert';

DropdownAddMeasurement dropdownAddMeasurementFromJson(String str) =>
    DropdownAddMeasurement.fromJson(json.decode(str));

String dropdownAddMeasurementToJson(DropdownAddMeasurement data) =>
    json.encode(data.toJson());

class DropdownAddMeasurement {
  DropdownAddMeasurement({
    this.code,
    this.data,
  });

  int? code;
  List<DropdownData?>? data;

  factory DropdownAddMeasurement.fromJson(Map<String, dynamic> json) =>
      DropdownAddMeasurement(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<DropdownData>.from(
                json["Data"].map((x) => DropdownData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x!.toJson())),
      };
}

class DropdownData {
  DropdownData({
    this.title,
    this.parameter,
    this.unit,
    this.logo,
    this.image,
  });

  String? parameter;
  String? title;
  String? unit;
  String? logo;
  String? image;

  factory DropdownData.fromJson(Map<String, dynamic> json) => DropdownData(
        title: json["title"] == null ? null : json["title"],
        parameter: json["parameter"] == null ? null : json["parameter"],
        unit: json["unit"] == null ? null : json["unit"],
        logo: json["logo"] == null ? null : json["logo"],
        image: json["image"] == null ? null : json["image"],
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "parameter": parameter == null ? null : parameter,
        "unit": unit == null ? null : unit,
        "logo": logo == null ? null : logo,
        "image": image == null ? null : image,
      };
}
