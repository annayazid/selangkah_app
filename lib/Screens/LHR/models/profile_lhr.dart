// To parse this JSON data, do
//
//     final profile = profileFromJson(jsonString);

import 'dart:convert';

ProfileLHR profileFromJson(String str) => ProfileLHR.fromJson(json.decode(str));

String profileToJson(ProfileLHR data) => json.encode(data.toJson());

class ProfileLHR {
  ProfileLHR({
    this.code,
    this.data,
    this.deviceId,
  });

  int? code;
  List<String?>? data;
  DeviceId? deviceId;

  factory ProfileLHR.fromJson(Map<String, dynamic> json) => ProfileLHR(
        code: json["Code"],
        data: List<String>.from(json["Data"].map((x) => x)),
        deviceId: DeviceId.fromJson(json["device_id"]),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": List<dynamic>.from(data!.map((x) => x)),
        "device_id": deviceId!.toJson(),
      };
}

class DeviceId {
  DeviceId({
    required this.deviceId,
  });

  String deviceId;

  factory DeviceId.fromJson(Map<String, dynamic> json) => DeviceId(
        deviceId: json["device_id"],
      );

  Map<String, dynamic> toJson() => {
        "device_id": deviceId,
      };
}
