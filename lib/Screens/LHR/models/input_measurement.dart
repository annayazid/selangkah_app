// To parse this JSON data, do
//
//     final inputAddMeasurement = inputAddMeasurementFromJson(jsonString);

import 'dart:convert';

InputAddMeasurement inputAddMeasurementFromJson(String str) =>
    InputAddMeasurement.fromJson(json.decode(str));

String inputAddMeasurementToJson(InputAddMeasurement data) =>
    json.encode(data.toJson());

class InputAddMeasurement {
  InputAddMeasurement({
    this.code,
    this.data,
  });

  int? code;
  List<InputAddMeasurementData?>? data;

  factory InputAddMeasurement.fromJson(Map<String, dynamic> json) =>
      InputAddMeasurement(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<InputAddMeasurementData>.from(
                json["Data"].map((x) => InputAddMeasurementData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x!.toJson())),
      };
}

class InputAddMeasurementData {
  InputAddMeasurementData({
    this.parameter,
    this.dataType,
    this.hint,
    this.unit,
    this.show,
  });

  String? parameter;
  String? dataType;
  String? hint;
  String? unit;
  String? show;

  factory InputAddMeasurementData.fromJson(Map<String, dynamic> json) =>
      InputAddMeasurementData(
        parameter: json["parameter"] == null ? null : json["parameter"],
        dataType: json["data_type"] == null ? null : json["data_type"],
        hint: json["hint"] == null ? null : json["hint"],
        unit: json["unit"] == null ? null : json["unit"],
        show: json["show"] == null ? null : json["show"],
      );

  Map<String, dynamic> toJson() => {
        "parameter": parameter == null ? null : parameter,
        "data_type": dataType == null ? null : dataType,
        "hint": hint == null ? null : hint,
        "unit": unit == null ? null : unit,
        "show": show == null ? null : show,
      };
}
