// To parse this JSON data, do
//
//     final labResult = labResultFromJson(jsonString);

import 'dart:convert';

LabResult labResultFromJson(String str) => LabResult.fromJson(json.decode(str));

String labResultToJson(LabResult data) => json.encode(data.toJson());

class LabResult {
  LabResult({
    this.code,
    this.data,
  });

  int? code;
  List<LabResultData>? data;

  factory LabResult.fromJson(Map<String, dynamic> json) => LabResult(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<LabResultData>.from(
                json["Data"].map((x) => LabResultData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class LabResultData {
  LabResultData({
    this.idDataSaring,
    this.date,
    this.testName,
    this.url,
    this.serviceName,
    this.referralLetter,
    this.pdfDocu,
    this.findings,
    this.redFlag,
    this.canDelete,
  });

  String? idDataSaring;
  DateTime? date;
  String? testName;
  String? url;
  String? serviceName;
  String? referralLetter;
  String? pdfDocu;
  String? findings;
  String? redFlag;
  int? canDelete;

  factory LabResultData.fromJson(Map<String, dynamic> json) => LabResultData(
        idDataSaring:
            json["id_data_saring"] == null ? null : json["id_data_saring"],
        date:
            // DateTime.parse(json["date"]) == null
            //     ? null
            // :
            DateTime.parse(json["date"]),
        // date: json["date"] == null ? null : json["date"],
        testName: json["test_name"] == null ? null : json["test_name"],
        url: json["url"] == null ? null : json["url"],
        serviceName: json["service_name"] == null ? null : json["service_name"],
        referralLetter:
            json["referral_letter"] == null ? null : json["referral_letter"],
        pdfDocu: json["pdf_docu"] == null ? null : json["pdf_docu"],
        findings: json["findings"] == null ? null : json["findings"],
        redFlag: json["red_flag"] == null ? null : json["red_flag"],
        canDelete: json["can_delete"] == null ? null : json["can_delete"],
      );

  Map<String, dynamic> toJson() => {
        "id_data_saring": idDataSaring == null ? null : idDataSaring,
        "date": date == null
            ? null
            : "${date!.year.toString().padLeft(4, '0')}-${date!.month.toString().padLeft(2, '0')}-${date!.day.toString().padLeft(2, '0')}",
        "test_name": testName == null ? null : testName,
        "url": url == null ? null : url,
        "service_name": serviceName == null ? null : serviceName,
        "referral_letter": referralLetter == null ? null : referralLetter,
        "pdf_docu": pdfDocu == null ? null : pdfDocu,
        "findings": findings == null ? null : findings,
        "red_flag": redFlag == null ? null : redFlag,
        "can_delete": canDelete == null ? null : canDelete,
      };
}
