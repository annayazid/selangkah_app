// To parse this JSON data, do
//
//     final getAppointment = getAppointmentFromJson(jsonString);

import 'dart:convert';

GetAppointment getAppointmentFromJson(String str) =>
    GetAppointment.fromJson(json.decode(str));

class GetAppointment {
  GetAppointment({
    this.code,
    this.data,
  });

  int? code;
  List<GetAppointmentData>? data;

  factory GetAppointment.fromJson(Map<String, dynamic> json) => GetAppointment(
        code: json["Code"],
        data: List<GetAppointmentData>.from(
            json["Data"].map((x) => GetAppointmentData.fromJson(x))),
      );
}

class GetAppointmentData {
  GetAppointmentData({
    this.idProgramAppointment,
    this.testId,
    this.testName,
    this.location,
    this.timeStart,
    this.timeEnd,
    this.address,
    this.date,
    this.referral,
    this.canChange,
    this.canDelete,
    this.apptCard,
    this.showDate,
  });

  String? idProgramAppointment;
  String? testId;
  String? testName;
  String? location;
  String? timeStart;
  String? timeEnd;
  String? address;
  DateTime? date;
  String? referral;
  int? canChange;
  int? canDelete;
  String? apptCard;
  int? showDate;

  factory GetAppointmentData.fromJson(Map<String, dynamic> json) =>
      GetAppointmentData(
        idProgramAppointment: json["id_program_appointment"],
        testId: json["test_id"] == null ? null : json["test_id"],
        testName: json["test_name"] == null ? null : json["test_name"],
        location: json["location"] == null ? null : json["location"],
        timeStart: json["time_start"] == null ? null : json["time_start"],
        timeEnd: json["time_end"] == null ? null : json["time_end"],
        address: json["address"] == null ? null : json["address"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        referral: json["referral"] == null ? null : json["referral"],
        canChange: json["can_change"] == null ? null : json["can_change"],
        canDelete: json["can_delete"] == null ? null : json["can_delete"],
        apptCard: json["appt_card"] == null ? null : json["appt_card"],
        showDate: json["show_date"] == null ? null : json["show_date"],
      );
}
