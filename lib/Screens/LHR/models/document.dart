// To parse this JSON data, do
//
//     final retrieveDocument = retrieveDocumentFromJson(jsonString);

import 'dart:convert';

RetrieveDocument retrieveDocumentFromJson(String str) =>
    RetrieveDocument.fromJson(json.decode(str));

String retrieveDocumentToJson(RetrieveDocument data) =>
    json.encode(data.toJson());

class RetrieveDocument {
  int? code;
  List<RetrieveDocumentData>? data;

  RetrieveDocument({
    this.code,
    this.data,
  });

  factory RetrieveDocument.fromJson(Map<String, dynamic> json) =>
      RetrieveDocument(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<RetrieveDocumentData>.from(
                json["Data"]!.map((x) => RetrieveDocumentData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class RetrieveDocumentData {
  String? id;
  String? filename;
  DateTime? createdTs;
  int? canDelete;

  RetrieveDocumentData({
    this.id,
    this.filename,
    this.createdTs,
    this.canDelete,
  });

  factory RetrieveDocumentData.fromJson(Map<String, dynamic> json) =>
      RetrieveDocumentData(
        id: json["id"],
        filename: json["filename"],
        createdTs: json["created_ts"] == null
            ? null
            : DateTime.parse(json["created_ts"]),
        canDelete: json["can_delete"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "filename": filename,
        "created_ts": createdTs?.toIso8601String(),
        "can_delete": canDelete,
      };
}
