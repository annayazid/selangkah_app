// To parse this JSON data, do
//
//     final recordTrendData = recordTrendDataFromJson(jsonString);

import 'dart:convert';

RecordTrendData recordTrendDataFromJson(String str) =>
    RecordTrendData.fromJson(json.decode(str));

String recordTrendDataToJson(RecordTrendData data) =>
    json.encode(data.toJson());

class RecordTrendData {
  RecordTrendData({
    this.code,
    this.description,
    this.data,
  });

  int? code;
  String? description;
  List<Datum?>? data;

  factory RecordTrendData.fromJson(Map<String, dynamic> json) =>
      RecordTrendData(
        code: json["Code"] == null ? null : json["Code"],
        description: json["Description"] == null ? null : json["Description"],
        data: json["Data"] == null
            ? null
            : List<Datum>.from(json["Data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Description": description == null ? null : description,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x!.toJson())),
      };
}

class Datum {
  Datum({
    this.date,
    this.time,
    this.record,
    this.unit,
    this.statusWarning,
    this.msg,
    this.statusDescription,
  });

  DateTime? date;
  String? time;
  String? record;
  String? unit;
  String? statusWarning;
  String? msg;
  String? statusDescription;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        date: json["Date"] == null ? null : DateTime.parse(json["Date"]),
        time: json["Time"] == null ? null : json["Time"],
        record: json["Record"] == null ? null : json["Record"],
        unit: json["Unit"] == null ? null : json["Unit"],
        statusWarning:
            json["Status_warning"] == null ? null : json["Status_warning"],
        msg: json["Msg"] == null ? null : json["Msg"],
        statusDescription: json["Status_description"] == null
            ? null
            : json["Status_description"],
      );

  Map<String, dynamic> toJson() => {
        "Date": date == null
            ? null
            : "${date!.year.toString().padLeft(4, '0')}-${date!.month.toString().padLeft(2, '0')}-${date!.day.toString().padLeft(2, '0')}",
        "Time": time == null ? null : time,
        "Record": record == null ? null : record,
        "Unit": unit == null ? null : unit,
        "Status_warning": statusWarning == null ? null : statusWarning,
        "Msg": msg == null ? null : msg,
        "Status_description":
            statusDescription == null ? null : statusDescription,
      };
}
