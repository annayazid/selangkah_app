class MonthData {
  final String? year;
  final String? month;
  final String? monthName;
  final double? average;

  MonthData({
    this.year,
    this.month,
    this.monthName,
    this.average,
  });
}
