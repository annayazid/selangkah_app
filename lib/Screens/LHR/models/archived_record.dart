// To parse this JSON data, do
//
//     final archivedRecord = archivedRecordFromJson(jsonString);

import 'dart:convert';

ArchivedRecord archivedRecordFromJson(String str) =>
    ArchivedRecord.fromJson(json.decode(str));

String archivedRecordToJson(ArchivedRecord data) => json.encode(data.toJson());

class ArchivedRecord {
  int? code;
  List<ArchivedRecordData>? data;

  ArchivedRecord({
    this.code,
    this.data,
  });

  factory ArchivedRecord.fromJson(Map<String, dynamic> json) => ArchivedRecord(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<ArchivedRecordData>.from(
                json["Data"]!.map((x) => ArchivedRecordData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class ArchivedRecordData {
  String? id;
  String? documentDir;
  DateTime? createdTs;
  int? canDelete;

  ArchivedRecordData({
    this.id,
    this.documentDir,
    this.createdTs,
    this.canDelete,
  });

  factory ArchivedRecordData.fromJson(Map<String, dynamic> json) =>
      ArchivedRecordData(
        id: json["id"],
        documentDir: json["document_dir"],
        createdTs: json["created_ts"] == null
            ? null
            : DateTime.parse(json["created_ts"]),
        canDelete: json["can_delete"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "document_dir": documentDir,
        "created_ts": createdTs?.toIso8601String(),
        "can_delete": canDelete,
      };
}
