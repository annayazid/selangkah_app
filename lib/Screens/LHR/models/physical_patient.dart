// To parse this JSON data, do
//
//     final physicalPatient = physicalPatientFromJson(jsonString);

import 'dart:convert';

PhysicalPatient physicalPatientFromJson(String str) =>
    PhysicalPatient.fromJson(json.decode(str));

String physicalPatientToJson(PhysicalPatient data) =>
    json.encode(data.toJson());

class PhysicalPatient {
  PhysicalPatient({
    this.code,
    this.data,
  });

  int? code;
  List<PhysicalPatientData>? data;

  factory PhysicalPatient.fromJson(Map<String, dynamic> json) =>
      PhysicalPatient(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<PhysicalPatientData>.from(
                json["Data"].map((x) => PhysicalPatientData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class PhysicalPatientData {
  PhysicalPatientData({
    this.date,
    this.height,
    this.weight,
    this.waistCircumference,
    this.bloodPressure,
    this.bmi,
  });

  DateTime? date;
  String? height;
  String? weight;
  String? waistCircumference;
  String? bloodPressure;
  String? bmi;

  factory PhysicalPatientData.fromJson(Map<String, dynamic> json) =>
      PhysicalPatientData(
        date: json["Date"] == null ? null : DateTime.parse(json["Date"]),
        height: json["Height"] == null ? null : json["Height"],
        weight: json["Weight"] == null ? null : json["Weight"],
        waistCircumference: json["Waist Circumference"] == null
            ? null
            : json["Waist Circumference"],
        bloodPressure:
            json["Blood Pressure"] == null ? null : json["Blood Pressure"],
        bmi: json["bmi"] == null ? null : json["bmi"],
      );

  Map<String, dynamic> toJson() => {
        "Date": date == null ? null : date!.toIso8601String(),
        "Height": height == null ? null : height,
        "Weight": weight == null ? null : weight,
        "Waist Circumference":
            waistCircumference == null ? null : waistCircumference,
        "Blood Pressure": bloodPressure == null ? null : bloodPressure,
        "bmi": bmi == null ? null : bmi,
      };
}
