// To parse this JSON data, do
//
//     final physicalRecord = physicalRecordFromJson(jsonString);

import 'dart:convert';

PhysicalRecord physicalRecordFromJson(String str) =>
    PhysicalRecord.fromJson(json.decode(str));

String physicalRecordToJson(PhysicalRecord data) => json.encode(data.toJson());

class PhysicalRecord {
  PhysicalRecord({
    this.code,
    this.data,
  });

  int? code;
  List<PhysicalRecordData?>? data;

  factory PhysicalRecord.fromJson(Map<String, dynamic> json) => PhysicalRecord(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null
            ? null
            : List<PhysicalRecordData>.from(
                json["Data"].map((x) => PhysicalRecordData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x!.toJson())),
      };
}

class PhysicalRecordData {
  PhysicalRecordData({
    this.sequence,
    this.title,
    this.parameter,
    this.unit,
    this.show,
    this.dataInput,
    this.statusWarning,
    this.statusDescription,
    this.color,
    this.logo,
  });

  String? sequence;
  String? title;
  String? parameter;
  String? unit;
  String? show;
  String? dataInput;
  int? statusWarning;
  String? statusDescription;
  String? color;
  String? logo;

  factory PhysicalRecordData.fromJson(Map<String, dynamic> json) =>
      PhysicalRecordData(
        sequence: json["Sequence"] == null ? null : json["Sequence"],
        title: json["title"] == null ? null : json["title"],
        parameter: json["parameter"] == null ? null : json["parameter"],
        unit: json["unit"] == null ? null : json["unit"],
        show: json["show"] == null ? null : json["show"],
        dataInput: json["data_input"] == null ? null : json["data_input"],
        statusWarning:
            json["status_warning"] == null ? null : json["status_warning"],
        statusDescription: json["status_description"] == null
            ? null
            : json["status_description"],
        color: json["color"] == null ? null : json["color"],
        logo: json["logo"] == null ? null : json["logo"],
      );

  Map<String, dynamic> toJson() => {
        "Sequence": sequence == null ? null : sequence,
        "title": title == null ? null : title,
        "parameter": parameter == null ? null : parameter,
        "unit": unit == null ? null : unit,
        "show": show == null ? null : show,
        "data_input": dataInput == null ? null : dataInput,
        "status_warning": statusWarning == null ? null : statusWarning,
        "status_description":
            statusDescription == null ? null : statusDescription,
        "color": color == null ? null : color,
        "logo": logo == null ? null : logo,
      };
}

// import 'dart:convert';

// LabResult labResultFromJson(String str) => LabResult.fromJson(json.decode(str));

// String labResultToJson(LabResult data) => json.encode(data.toJson());

// class LabResult {
//     LabResult({
//         this.code,
//         this.data,
//     });

//     int code;
//     List<Datum> data;

//     factory LabResult.fromJson(Map<String, dynamic> json) => LabResult(
//         code: json["Code"],
//         data: List<Datum>.from(json["Data"].map((x) => Datum.fromJson(x))),
//     );

//     Map<String, dynamic> toJson() => {
//         "Code": code,
//         "Data": List<dynamic>.from(data.map((x) => x.toJson())),
//     };
// }

// }
