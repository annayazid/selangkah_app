import 'package:isoweek/isoweek.dart';

class WeeksData {
  Week? week;
  GraphList? graphList;
  WeeksData({
    this.week,
    this.graphList,
  });
}

class GraphList {
  String? name;
  List<GraphData>? graphData;
  String? indicatorName;
  int? indicator;
  GraphList({
    this.name,
    this.graphData,
    this.indicatorName,
    this.indicator,
  });
}

class GraphData {
  DateTime? date;
  int? day;
  double? record;
  GraphData({
    this.date,
    this.day,
    this.record,
  });
}
