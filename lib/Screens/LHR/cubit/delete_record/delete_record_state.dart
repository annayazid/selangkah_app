part of 'delete_record_cubit.dart';

@immutable
abstract class DeleteRecordState {
  const DeleteRecordState();
}

class DeleteRecordInitial extends DeleteRecordState {
  const DeleteRecordInitial();
}

class DeleteRecordLoading extends DeleteRecordState {
  const DeleteRecordLoading();
}

class DeleteRecordDeleted extends DeleteRecordState {
  const DeleteRecordDeleted();
}

class SaringRecordFailed extends DeleteRecordState {
  const SaringRecordFailed();
}

class DeleteRecordFailed extends DeleteRecordState {
  const DeleteRecordFailed();
}
