import 'package:bloc/bloc.dart';
import 'package:date_format/date_format.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'delete_record_state.dart';

class DeleteRecordCubit extends Cubit<DeleteRecordState> {
  DeleteRecordCubit() : super(DeleteRecordInitial());

  Future<void> deleteRecord(
      String param, DateTime dateInput, String timeInput) async {
    emit(DeleteRecordLoading());

    String date;
    // String time;

    print(dateInput);
    print(timeInput);

    date = formatDate(dateInput, [yyyy, '-', mm, '-', dd]);
    // time = formatDate(timeInput, [hh, ':', nn, ':', ss]);

    try {
      // await Future.delayed(Duration(seconds: 1));
      bool delete = await LhrRepositories.deleteRecord(param, date, timeInput);
      if (delete) {
        emit(DeleteRecordDeleted());
      } else {
        emit(SaringRecordFailed());
      }
    } catch (e) {
      print('masuk catch');
      // await Future.delayed(Duration(seconds: 1));
      emit(DeleteRecordFailed());
    }
  }
}
