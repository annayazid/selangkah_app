part of 'physical_data_cubit.dart';

@immutable
abstract class PhysicalDataState {
  const PhysicalDataState();
}

class PhysicalDataInitial extends PhysicalDataState {
  const PhysicalDataInitial();
}

class PhysicalDataLoading extends PhysicalDataState {
  const PhysicalDataLoading();
}

class PhysicalDataLoaded extends PhysicalDataState {
  final String? name;
  final String? district;
  final int? age;
  final Uint8List? imgbytes;
  final PhysicalRecord? physicalRecord;

  PhysicalDataLoaded(
      this.name, this.district, this.age, this.imgbytes, this.physicalRecord);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is PhysicalDataLoaded &&
        other.name == name &&
        other.district == district &&
        other.age == age &&
        other.imgbytes == imgbytes &&
        other.physicalRecord == physicalRecord;
  }

  @override
  int get hashCode {
    return name.hashCode ^
        district.hashCode ^
        age.hashCode ^
        imgbytes.hashCode ^
        physicalRecord.hashCode;
  }
}
