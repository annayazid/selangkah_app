import 'dart:convert';
import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'physical_data_state.dart';

class PhysicalDataCubit extends Cubit<PhysicalDataState> {
  PhysicalDataCubit() : super(PhysicalDataInitial());
  Future<void> getPhysicalData() async {
    emit(PhysicalDataLoading());

    ProfileLHR? profile = await LhrRepositories.getProfile();

    Uint8List? imgbytes;
    String? district;
    DateTime? birthDateConvert;
    int? age;

    String name = profile.data![2]!;
    String? birthDate = profile.data![8];
    if (birthDate != null) {
      birthDateConvert = DateTime.parse(birthDate);
    } else {
      birthDateConvert = null;
    }

    DateTime currentDate = DateTime.now();
    if (birthDateConvert != null) {
      age = currentDate.year - birthDateConvert.year;
      int month1 = currentDate.month;
      int month2 = birthDateConvert.month;
      if (month2 > month1) {
        age--;
      } else if (month1 == month2) {
        int day1 = currentDate.day;
        int day2 = birthDateConvert.day;
        if (day2 > day1) {
          age--;
        }
      }
    } else {
      age = null;
    }

    print(age);

    if (profile.data![18] != null) {
      imgbytes = base64.decode(profile.data![18]!);
    } else {
      imgbytes = null;
    }

    PhysicalRecord physicalRecord = await LhrRepositories.getHealthRecord();

    emit(PhysicalDataLoaded(name, district, age, imgbytes, physicalRecord));
  }
}
