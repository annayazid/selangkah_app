part of 'input_measurement_cubit.dart';

@immutable
abstract class InputMeasurementState {
  const InputMeasurementState();
}

class InputMeasurementInitial extends InputMeasurementState {
  const InputMeasurementInitial();
}

class InputMeasurementLoading extends InputMeasurementState {
  const InputMeasurementLoading();
}

class InputMeasurementLoaded extends InputMeasurementState {
  final InputAddMeasurement input;

  InputMeasurementLoaded(this.input);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is InputMeasurementLoaded && other.input == input;
  }

  @override
  int get hashCode => input.hashCode;
}
