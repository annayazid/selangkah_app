import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'input_measurement_state.dart';

class InputMeasurementCubit extends Cubit<InputMeasurementState> {
  InputMeasurementCubit() : super(InputMeasurementInitial());

  Future<void> getInputMeasurement(String type) async {
    emit(InputMeasurementLoading());

    InputAddMeasurement input = await LhrRepositories.getInput(type);

    emit(InputMeasurementLoaded(input));
  }
}
