part of 'appointment_option_cubit.dart';

@immutable
abstract class AppointmentOptionState {
  const AppointmentOptionState();
}

class AppointmentOptionInitial extends AppointmentOptionState {
  const AppointmentOptionInitial();
}

class AppointmentOptionLoading extends AppointmentOptionState {
  const AppointmentOptionLoading();
}

class AppointmentOptionLoaded extends AppointmentOptionState {
  const AppointmentOptionLoaded();
}

class AppointmentOptionFailed extends AppointmentOptionState {
  const AppointmentOptionFailed();
}
