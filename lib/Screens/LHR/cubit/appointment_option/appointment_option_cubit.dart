import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'appointment_option_state.dart';

class AppointmentOptionCubit extends Cubit<AppointmentOptionState> {
  AppointmentOptionCubit() : super(AppointmentOptionInitial());

  Future<void> sendOption(
      String testId, String statusCall, String remark) async {
    emit(AppointmentOptionLoading());

    // await Future.delayed(Duration(seconds: 1));

    bool sendFeedback =
        await LhrRepositories.sendFeedback(testId, statusCall, remark);

    if (sendFeedback) {
      emit(AppointmentOptionLoaded());
    } else {
      emit(AppointmentOptionFailed());
    }
  }
}
