part of 'dropdown_measurement_cubit.dart';

@immutable
abstract class DropdownMeasurementState {
  const DropdownMeasurementState();
}

class DropdownMeasurementInitial extends DropdownMeasurementState {
  const DropdownMeasurementInitial();
}

class DropdownMeasurementLoading extends DropdownMeasurementState {
  const DropdownMeasurementLoading();
}

class DropdownMeasurementLoaded extends DropdownMeasurementState {
  final DropdownAddMeasurement dropdown;

  DropdownMeasurementLoaded(this.dropdown);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is DropdownMeasurementLoaded && other.dropdown == dropdown;
  }

  @override
  int get hashCode => dropdown.hashCode;
}
