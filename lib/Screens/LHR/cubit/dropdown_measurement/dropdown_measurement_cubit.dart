import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'dropdown_measurement_state.dart';

class DropdownMeasurementCubit extends Cubit<DropdownMeasurementState> {
  DropdownMeasurementCubit() : super(DropdownMeasurementInitial());

  Future<void> getDropdownMeasurement() async {
    emit(DropdownMeasurementLoading());

    DropdownAddMeasurement dropdown = await LhrRepositories.getDropdown();

    emit(DropdownMeasurementLoaded(dropdown));
  }
}
