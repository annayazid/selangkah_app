import 'package:bloc/bloc.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/foundation.dart';
import 'package:isoweek/isoweek.dart';
// import 'package:jiffy/jiffy.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'record_trend_state.dart';

class RecordTrendCubit extends Cubit<RecordTrendState> {
  RecordTrendCubit() : super(RecordTrendInitial());
  Future<void> getRecordTrend(String parameter) async {
    emit(RecordTrendLoading());

    RecordTrendData recordTrendData =
        await LhrRepositories.getRecordTrend(parameter);

    print('Loaded Record Data');

    if (parameter != 'Blood Pressure') {
      RecordTrendData recordTrendDataFake = recordTrendData;

      //ni fake data
      // RecordTrendData recordTrendDataFake = RecordTrendData(
      //   data: [
      //     Datum(
      //       date: DateTime(2022, 11, 21),
      //       record: '70',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 22),
      //       record: '0',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 23),
      //       record: '72',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 24),
      //       record: '73',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 25),
      //       record: '74',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 26),
      //       record: '75',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 27),
      //       record: '76',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 28),
      //       record: '76',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 12, 5),
      //       record: '76',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 12, 7),
      //       record: '77',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 12, 8),
      //       record: '78',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 12, 19),
      //       record: '78',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 12, 20),
      //       record: '78',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //   ],
      // );

      List<int> yearList = [];

      yearList.add(DateTime.now().year);

      for (var i = 0; i < recordTrendDataFake.data!.length; i++) {
        if (!yearList.contains(recordTrendDataFake.data![i]!.date!.year)) {
          yearList.add(recordTrendDataFake.data![i]!.date!.year);
        }
      }

      print('yearList is $yearList');

      yearList = yearList.reversed.toList();

      List<WeeksData> weeksData = [];
      List<DayData> dayDataList = [];

      //create week based on week
      for (var h = 0; h < yearList.length; h++) {
        //week logic
        List<Week> weeks2022 = [];

        var dateTime2022 = List<DateTime>.generate(
          yearList[h] % 4 == 0 ? 366 : 365,
          (i) => DateTime.utc(
            yearList[h],
            1,
            1,
          ).add(Duration(days: i)),
        );

        dateTime2022.forEach((element) {
          if (!weeks2022.any((element2) =>
              element2.toString() == Week.fromDate(element).toString())) {
            weeks2022.add(Week.fromDate(element));
          }
        });

        if (DateTime.now().year == yearList[h]) {
          Week weekNow = Week.fromDate(DateTime.now());

          weeks2022 = weeks2022.sublist(0, weekNow.weekNumber + 1);
        }

        for (var i = 0; i < weeks2022.length; i++) {
          List<DateTime> listDateTime = weeks2022[i].days;

          weeksData.add(
            WeeksData(
              week: weeks2022[i],
              graphList: GraphList(
                indicator: 33,
                indicatorName: 'Week',
                name: 'Weight',
                graphData: () {
                  List<GraphData> graphData = [];
                  listDateTime.forEach((element) {
                    graphData.add(GraphData(
                        date: element,
                        day: () {
                          if (formatDate(element, [DD]) == 'Monday') {
                            return 1;
                          } else if (formatDate(element, [DD]) == 'Tuesday') {
                            return 2;
                          } else if (formatDate(element, [DD]) == 'Wednesday') {
                            return 3;
                          } else if (formatDate(element, [DD]) == 'Thursday') {
                            return 4;
                          } else if (formatDate(element, [DD]) == 'Friday') {
                            return 5;
                          } else if (formatDate(element, [DD]) == 'Saturday') {
                            return 6;
                          } else {
                            return 7;
                          }
                        }(),
                        record: null));
                  });
                  return graphData;
                }(),
              ),
            ),
          );
        }

        for (var i = 0; i < weeksData.length; i++) {
          for (var j = 0; j < weeksData[i].graphList!.graphData!.length; j++) {
            int totalCount = 0;
            double totalValue = 0;

            for (var k = 0; k < recordTrendDataFake.data!.length; k++) {
              if (recordTrendDataFake.data![k]!.date ==
                  weeksData[i].graphList!.graphData![j].date) {
                totalCount++;

                double value = 0;

                if (recordTrendDataFake.data![k]!.record != null) {
                  value = double.parse(recordTrendDataFake.data![k]!.record!);
                }

                totalValue = totalValue + value;
              }

              // weeksData[i].graphList.graphData[j].record =
              //     recordTrendDataFake.data[k].record != '0'
              //         ? double.parse(recordTrendDataFake.data[k].record)
              //         : null;

              if (totalCount == 0 && totalValue == 0) {
                weeksData[i].graphList!.graphData![j].record = null;
              } else {
                double averageValue = totalValue / totalCount;

                String inString = averageValue.toStringAsFixed(2);
                double inDouble = double.parse(inString);

                weeksData[i].graphList!.graphData![j].record = inDouble;
              }
            }
          }
        }

        //-------month logic-----------------------------------------

        List<DayData> dayData = [];

        for (var i = 1;
            i <=
                () {
                  if (DateTime.now().year == yearList[h]) {
                    return DateTime.now().month;
                  } else {
                    return 12;
                  }
                }();
            i++) {
          if (i == 1) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);

                              totalCount++;

                              totalValue = totalValue +
                                  double.parse(
                                      recordTrendDataFake.data![m]!.record!);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 2) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              if (yearList[h] % 4 == 0) {
                                return 29;
                              } else {
                                return 28;
                              }
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue = totalValue +
                                  double.parse(
                                      recordTrendDataFake.data![m]!.record!);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 3) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue = totalValue +
                                  double.parse(
                                      recordTrendDataFake.data![m]!.record!);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 4) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 30;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue = totalValue +
                                  double.parse(
                                      recordTrendDataFake.data![m]!.record!);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 5) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue = totalValue +
                                  double.parse(
                                      recordTrendDataFake.data![m]!.record!);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 6) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 30;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue = totalValue +
                                  double.parse(
                                      recordTrendDataFake.data![m]!.record!);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 7) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue = totalValue +
                                  double.parse(
                                      recordTrendDataFake.data![m]!.record!);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 8) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue = totalValue +
                                  double.parse(
                                      recordTrendDataFake.data![m]!.record!);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 9) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 30;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue = totalValue +
                                  double.parse(
                                      recordTrendDataFake.data![m]!.record!);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 10) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue = totalValue +
                                  double.parse(
                                      recordTrendDataFake.data![m]!.record!);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 11) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 30;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue = totalValue +
                                  double.parse(
                                      recordTrendDataFake.data![m]!.record!);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    // if (j == 1 || j == 20) {
                    //   graphDataDay.add(
                    //     GraphDataDay(
                    //       date: DateTime(yearList[h], i, j),
                    //       day: j,
                    //       // record: j % 3 == 0
                    //       //     ? (77 + Random().nextInt(80 - 77)).toDouble()
                    //       //     : 0,
                    //       record: (77 + Random().nextInt(80 - 77)).toDouble(),
                    //     ),
                    //   );
                    // }

                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue = totalValue +
                                  double.parse(
                                      recordTrendDataFake.data![m]!.record!);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                        // record: (77 + Random().nextInt(80 - 77)).toDouble(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          }
        }

        print('day data is ${dayData.length}');
        dayDataList.addAll(dayData);

        //----------------------------------------------------------------
      }

      weeksData = weeksData.reversed.toList();
      dayDataList = dayDataList.reversed.toList();

      // dayData.forEach((element) {
      //   element.graphData.forEach((elementi) {
      //     print(elementi.date);
      //     print(elementi.record);
      //   });
      // });

      // for (var i = 0; i < dayData.length; i++) {
      //   for (var j = 0; j < dayData[i].graphData.length; j++) {
      //     print(dayData[i].graphData[j].date);
      //     // print(dayData[i].graphData[j].record);
      //   }
      // }

      //end of logic weeks

      emit(RecordTrendLoaded(
        recordTrendData: recordTrendData,
        weeksData: weeksData,
        dayData: dayDataList,
      ));
    }

    //ni blood pressure------------------------------------------------------
    else {
      RecordTrendData recordTrendDataFake = recordTrendData;

      //ni fake data
      // RecordTrendData recordTrendDataFake = RecordTrendData(
      //   data: [
      //     Datum(
      //       date: DateTime(2021, 11, 20),
      //       record: '69/74',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 21),
      //       record: '70/75',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 22),
      //       record: '0/0',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 23),
      //       record: '72/77',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 24),
      //       record: '73/78',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 25),
      //       record: '74/79',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 26),
      //       record: '75/80',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 27),
      //       record: '76/81',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 11, 28),
      //       record: '76/81',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 12, 5),
      //       record: '76/81',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 12, 7),
      //       record: '77/82',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 12, 8),
      //       record: '78/83',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 12, 20),
      //       record: '78/80',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2022, 12, 21),
      //       record: '78/80',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2023, 02, 06),
      //       record: '78/80',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //     Datum(
      //       date: DateTime(2023, 02, 10),
      //       record: '78/80',
      //       time: "08:06:15",
      //       unit: 'kg',
      //     ),
      //   ],
      // );

      List<String> myNumberMaster = [];

      for (var i = 0; i < recordTrendDataFake.data!.length; i++) {
        List<String> myNumber =
            recordTrendDataFake.data![i]!.record!.split('/');

        myNumberMaster.addAll(myNumber);
      }

      List<String> systolic = [];
      List<String> diastolic = [];

      for (var i = 0; i < myNumberMaster.length; i++) {
        if (i % 2 == 0) {
          systolic.add(myNumberMaster[i]);
        } else {
          diastolic.add(myNumberMaster[i]);
        }
      }

      print(systolic);
      print(diastolic);

      //sys
      List<int> yearList = [];

      yearList.add(DateTime.now().year);

      yearList.add(DateTime.now().year);

      for (var i = 0; i < recordTrendDataFake.data!.length; i++) {
        if (!yearList.contains(recordTrendDataFake.data![i]!.date!.year)) {
          yearList.add(recordTrendDataFake.data![i]!.date!.year);
        }
      }

      print(yearList);

      yearList = yearList.reversed.toList();

      List<WeeksData> weeksData = [];
      List<DayData> dayDataList = [];

      //create week based on week
      for (var h = 0; h < yearList.length; h++) {
        //week logic
        List<Week> weeks2022 = [];

        var dateTime2022 = List<DateTime>.generate(
            yearList[h] % 4 == 0 ? 366 : 365,
            (i) => DateTime.utc(
                  yearList[h],
                  1,
                  1,
                ).add(Duration(days: i)));

        dateTime2022.forEach((element) {
          if (!weeks2022.any((element2) =>
              element2.toString() == Week.fromDate(element).toString())) {
            weeks2022.add(Week.fromDate(element));
          }
        });

        if (DateTime.now().year == yearList[h]) {
          Week weekNow = Week.fromDate(DateTime.now());

          weeks2022 = weeks2022.sublist(0, weekNow.weekNumber + 1);
        }

        for (var i = 0; i < weeks2022.length; i++) {
          List<DateTime> listDateTime = weeks2022[i].days;

          weeksData.add(
            WeeksData(
              week: weeks2022[i],
              graphList: GraphList(
                indicator: 33,
                indicatorName: 'Week',
                name: 'Weight',
                graphData: () {
                  List<GraphData> graphData = [];
                  listDateTime.forEach((element) {
                    graphData.add(GraphData(
                        date: element,
                        day: () {
                          if (formatDate(element, [DD]) == 'Monday') {
                            return 1;
                          } else if (formatDate(element, [DD]) == 'Tuesday') {
                            return 2;
                          } else if (formatDate(element, [DD]) == 'Wednesday') {
                            return 3;
                          } else if (formatDate(element, [DD]) == 'Thursday') {
                            return 4;
                          } else if (formatDate(element, [DD]) == 'Friday') {
                            return 5;
                          } else if (formatDate(element, [DD]) == 'Saturday') {
                            return 6;
                          } else {
                            return 7;
                          }
                        }(),
                        record: null));
                  });
                  return graphData;
                }(),
              ),
            ),
          );
        }

        weeksData = weeksData.reversed.toList();

        for (var i = 0; i < weeksData.length; i++) {
          for (var j = 0; j < weeksData[i].graphList!.graphData!.length; j++) {
            // for (var k = 0; k < recordTrendDataFake.data.length; k++) {
            //   if (recordTrendDataFake.data[k].date ==
            //       weeksData[i].graphList.graphData[j].date) {
            //     weeksData[i].graphList.graphData[j].record =
            //         systolic[k] != '0' ? double.parse(systolic[k]) : null;
            //   }
            // }

            int totalCount = 0;
            double totalValue = 0;

            for (var k = 0; k < recordTrendDataFake.data!.length; k++) {
              if (recordTrendDataFake.data![k]!.date ==
                  weeksData[i].graphList!.graphData![j].date) {
                totalCount++;

                double value = 0;

                if (systolic[k] != '0') {
                  value = double.parse(systolic[k]);
                }

                totalValue = totalValue + value;
              }

              // weeksData[i].graphList.graphData[j].record =
              //     recordTrendDataFake.data[k].record != '0'
              //         ? double.parse(recordTrendDataFake.data[k].record)
              //         : null;

              if (totalCount == 0 && totalValue == 0) {
                weeksData[i].graphList!.graphData![j].record = null;
              } else {
                double averageValue = totalValue / totalCount;

                String inString = averageValue.toStringAsFixed(2);
                double inDouble = double.parse(inString);

                weeksData[i].graphList!.graphData![j].record = inDouble;
              }
            }
          }
        }

        //----------------month logic---------------

        List<DayData> dayData = [];

        for (var i = 1;
            i <=
                () {
                  if (DateTime.now().year == yearList[h]) {
                    return DateTime.now().month;
                  } else {
                    return 12;
                  }
                }();
            i++) {
          if (i == 1) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(systolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 2) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              if (yearList[h] % 4 == 0) {
                                return 29;
                              } else {
                                return 28;
                              }
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(systolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 3) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(systolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 4) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 30;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(systolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 5) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(systolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 6) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 30;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(systolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 7) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(systolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 8) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(systolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 9) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 30;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(systolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 10) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(systolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 11) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 30;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(systolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    // if (j == 1 || j == 20) {
                    //   graphDataDay.add(
                    //     GraphDataDay(
                    //       date: DateTime(yearList[h], i, j),
                    //       day: j,
                    //       // record: j % 3 == 0
                    //       //     ? (77 + Random().nextInt(80 - 77)).toDouble()
                    //       //     : 0,
                    //       record: (77 + Random().nextInt(80 - 77)).toDouble(),
                    //     ),
                    //   );
                    // }

                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(systolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                        // record: (77 + Random().nextInt(80 - 77)).toDouble(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          }
        }

        print('day data is ${dayData.length}');
        dayDataList.addAll(dayData);
      }

      dayDataList = dayDataList.reversed.toList();

      //sys
      List<WeeksData> weeksData2 = [];
      List<DayData> dayDataList2 = [];

      //create week based on week
      for (var h = 0; h < yearList.length; h++) {
        //week logic
        List<Week> weeks2022 = [];

        var dateTime2022 = List<DateTime>.generate(
            yearList[h] % 4 == 0 ? 366 : 365,
            (i) => DateTime.utc(
                  yearList[h],
                  1,
                  1,
                ).add(Duration(days: i)));

        dateTime2022.forEach((element) {
          if (!weeks2022.any((element2) =>
              element2.toString() == Week.fromDate(element).toString())) {
            weeks2022.add(Week.fromDate(element));
          }
        });

        if (DateTime.now().year == yearList[h]) {
          Week weekNow = Week.fromDate(DateTime.now());

          weeks2022 = weeks2022.sublist(0, weekNow.weekNumber + 1);
        }

        for (var i = 0; i < weeks2022.length; i++) {
          List<DateTime> listDateTime = weeks2022[i].days;

          weeksData2.add(
            WeeksData(
              week: weeks2022[i],
              graphList: GraphList(
                indicator: 33,
                indicatorName: 'Week',
                name: 'Weight',
                graphData: () {
                  List<GraphData> graphData = [];
                  listDateTime.forEach((element) {
                    graphData.add(GraphData(
                        date: element,
                        day: () {
                          if (formatDate(element, [DD]) == 'Monday') {
                            return 1;
                          } else if (formatDate(element, [DD]) == 'Tuesday') {
                            return 2;
                          } else if (formatDate(element, [DD]) == 'Wednesday') {
                            return 3;
                          } else if (formatDate(element, [DD]) == 'Thursday') {
                            return 4;
                          } else if (formatDate(element, [DD]) == 'Friday') {
                            return 5;
                          } else if (formatDate(element, [DD]) == 'Saturday') {
                            return 6;
                          } else {
                            return 7;
                          }
                        }(),
                        record: null));
                  });
                  return graphData;
                }(),
              ),
            ),
          );
        }

        weeksData2 = weeksData2.reversed.toList();

        for (var i = 0; i < weeksData2.length; i++) {
          for (var j = 0; j < weeksData2[i].graphList!.graphData!.length; j++) {
            int totalCount = 0;
            double totalValue = 0;
            // for (var k = 0; k < recordTrendDataFake.data.length; k++) {
            //   if (recordTrendDataFake.data[k].date ==
            //       weeksData2[i].graphList.graphData[j].date) {
            //     weeksData2[i].graphList.graphData[j].record =
            //         diastolic[k] != '0' ? double.parse(diastolic[k]) : null;
            //   }
            // }

            for (var k = 0; k < recordTrendDataFake.data!.length; k++) {
              if (recordTrendDataFake.data![k]!.date ==
                  weeksData[i].graphList!.graphData![j].date) {
                totalCount++;

                double value = 0;

                if (systolic[k] != '0') {
                  value = double.parse(diastolic[k]);
                }

                totalValue = totalValue + value;
              }

              // weeksData[i].graphList.graphData[j].record =
              //     recordTrendDataFake.data[k].record != '0'
              //         ? double.parse(recordTrendDataFake.data[k].record)
              //         : null;

              if (totalCount == 0 && totalValue == 0) {
                weeksData2[i].graphList!.graphData![j].record = null;
              } else {
                double averageValue = totalValue / totalCount;

                String inString = averageValue.toStringAsFixed(2);
                double inDouble = double.parse(inString);

                weeksData2[i].graphList!.graphData![j].record = inDouble;
              }
            }
          }
        }

        List<DayData> dayData = [];

        for (var i = 1;
            i <=
                () {
                  if (DateTime.now().year == yearList[h]) {
                    return DateTime.now().month;
                  } else {
                    return 12;
                  }
                }();
            i++) {
          if (i == 1) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(diastolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 2) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              if (yearList[h] % 4 == 0) {
                                return 29;
                              } else {
                                return 28;
                              }
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(diastolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 3) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(diastolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 4) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 30;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(diastolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 5) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(diastolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 6) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 30;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(diastolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 7) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(diastolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 8) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(diastolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 9) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 30;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(diastolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 10) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(diastolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else if (i == 11) {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 30;
                            }
                          }();
                      j++) {
                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(diastolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          } else {
            dayData.add(
              DayData(
                month: i,
                year: yearList[h],
                graphData: () {
                  List<GraphDataDay> graphDataDay = [];
                  for (var j = 1;
                      j <=
                          () {
                            if (DateTime.now().month == i &&
                                DateTime.now().year == yearList[h]) {
                              return DateTime.now().day;
                            } else {
                              return 31;
                            }
                          }();
                      j++) {
                    // if (j == 1 || j == 20) {
                    //   graphDataDay.add(
                    //     GraphDataDay(
                    //       date: DateTime(yearList[h], i, j),
                    //       day: j,
                    //       // record: j % 3 == 0
                    //       //     ? (77 + Random().nextInt(80 - 77)).toDouble()
                    //       //     : 0,
                    //       record: (77 + Random().nextInt(80 - 77)).toDouble(),
                    //     ),
                    //   );
                    // }

                    graphDataDay.add(
                      GraphDataDay(
                        date: DateTime(yearList[h], i, j),
                        day: j,
                        record: () {
                          int totalCount = 0;
                          double totalValue = 0;

                          for (var m = 0;
                              m < recordTrendDataFake.data!.length;
                              m++) {
                            if (recordTrendDataFake.data![m]!.date!.year ==
                                    yearList[h] &&
                                recordTrendDataFake.data![m]!.date!.month ==
                                    i &&
                                recordTrendDataFake.data![m]!.date!.day == j) {
                              // record = double.parse(
                              //     recordTrendDataFake.data[m].record);
                              totalCount++;

                              totalValue =
                                  totalValue + double.parse(diastolic[m]);
                              // break;
                            }
                          }

                          if (totalCount != 0 && totalValue != 0) {
                            double value = totalValue / totalCount;
                            String inString = value.toStringAsFixed(2);
                            double record = double.parse(inString);

                            return record;
                          } else {
                            return null;
                          }
                        }(),
                        // record: (77 + Random().nextInt(80 - 77)).toDouble(),
                      ),
                    );
                  }

                  return graphDataDay;
                }(),
              ),
            );
          }
        }

        print('day data is ${dayData.length}');
        dayDataList2.addAll(dayData);
      }

      dayDataList2 = dayDataList2.reversed.toList();

      //month logic

      //end month logic

      emit(
        RecordTrendLoaded(
          recordTrendData: recordTrendData,
          weeksData: weeksData,
          weeksData2: weeksData2,
          dayData: dayDataList,
          dayData2: dayDataList2,
        ),
      );
    }
  }
}
