part of 'record_trend_cubit.dart';

@immutable
abstract class RecordTrendState {
  const RecordTrendState();
}

class RecordTrendInitial extends RecordTrendState {
  const RecordTrendInitial();
}

class RecordTrendLoading extends RecordTrendState {
  const RecordTrendLoading();
}

class RecordTrendLoaded extends RecordTrendState {
  final RecordTrendData? recordTrendData;
  final List<WeeksData>? weeksData;
  final List<WeeksData>? weeksData2;
  final List<DayData>? dayData;
  final List<DayData>? dayData2;

  RecordTrendLoaded({
    this.recordTrendData,
    this.weeksData,
    this.weeksData2,
    this.dayData,
    this.dayData2,
  });

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RecordTrendLoaded &&
        other.recordTrendData == recordTrendData &&
        listEquals(other.weeksData, weeksData) &&
        listEquals(other.weeksData2, weeksData2) &&
        listEquals(other.dayData, dayData) &&
        listEquals(other.dayData2, dayData2);
  }

  @override
  int get hashCode {
    return recordTrendData.hashCode ^
        weeksData.hashCode ^
        weeksData2.hashCode ^
        dayData.hashCode ^
        dayData2.hashCode;
  }
}
