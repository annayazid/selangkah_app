part of 'submit_document_cubit.dart';

@immutable
abstract class SubmitDocumentState {
  const SubmitDocumentState();
}

class SubmitDocumentInitial extends SubmitDocumentState {
  const SubmitDocumentInitial();
}

class SubmitDocumentLoading extends SubmitDocumentState {
  const SubmitDocumentLoading();
}

class SubmitDocumentLoaded extends SubmitDocumentState {
  const SubmitDocumentLoaded();
}

class SubmitDocumentError extends SubmitDocumentState {
  const SubmitDocumentError();
}
