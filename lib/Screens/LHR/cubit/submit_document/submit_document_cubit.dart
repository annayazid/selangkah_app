import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'submit_document_state.dart';

class SubmitDocumentCubit extends Cubit<SubmitDocumentState> {
  SubmitDocumentCubit() : super(SubmitDocumentInitial());

  Future<void> sendDocument(
    String documentType,
    File documentFile,
    String fileName,
  ) async {
    emit(SubmitDocumentLoading());

    //process

    bool success = await LhrRepositories.uploadDocumment(
        documentType, documentFile, fileName);

    if (success) {
      emit(SubmitDocumentLoaded());
    } else {
      emit(SubmitDocumentError());
    }
  }
}
