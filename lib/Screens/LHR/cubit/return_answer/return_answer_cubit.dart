import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

part 'return_answer_state.dart';

class ReturnAnswerCubit extends Cubit<ReturnAnswerState> {
  ReturnAnswerCubit() : super(ReturnAnswerInitial());

  Future<void> submitRecord(
      List<String> input,
      List<TextEditingController> answer,
      DateTime dateInput,
      DateTime timeInput) async {
    emit(ReturnAnswerLoading());

    String jsonAnswer = '';
    String date;
    String time;

    //process jsonAnswer
    for (var i = 0; i < input.length; i++) {
      jsonAnswer = '$jsonAnswer"${input[i]}":"${answer[i].text}",';
    }

    //add curly braces & remove last coma
    jsonAnswer = jsonAnswer.substring(0, jsonAnswer.length - 1);

    jsonAnswer = '{$jsonAnswer}';

    print(jsonAnswer);

    date = formatDate(dateInput, [yyyy, '-', mm, '-', dd]);
    time = formatDate(timeInput, [hh, ':', nn, ':', ss]);

    try {
      print('Anna');
      await Future.delayed(Duration(seconds: 1));
      await LhrRepositories.sendRecord(jsonAnswer, date, time);
      print('Anna Comel');
      emit(ReturnAnswerSent());
    } catch (e) {
      print('masuk catch');
      await Future.delayed(Duration(seconds: 1));
      emit(ReturnAnswerFail());
    }
  }
}
