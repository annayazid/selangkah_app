part of 'return_answer_cubit.dart';

@immutable
abstract class ReturnAnswerState {
  const ReturnAnswerState();
}

class ReturnAnswerInitial extends ReturnAnswerState {
  const ReturnAnswerInitial();
}

class ReturnAnswerLoading extends ReturnAnswerState {
  const ReturnAnswerLoading();
}

class ReturnAnswerSent extends ReturnAnswerState {
  const ReturnAnswerSent();
}

class ReturnAnswerFail extends ReturnAnswerState {
  const ReturnAnswerFail();
}
