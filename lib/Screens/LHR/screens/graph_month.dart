import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

class GraphMonthWidget extends StatefulWidget {
  final List<DayData> dayData;
  final List<DayData>? dayData2;

  const GraphMonthWidget({Key? key, required this.dayData, this.dayData2})
      : super(key: key);

  @override
  State<GraphMonthWidget> createState() => _GraphMonthWidgetState();
}

List<DayData>? dayDataOutside;

class _GraphMonthWidgetState extends State<GraphMonthWidget> {
  int currentPage = 0;
  PageController controller = PageController();
  List<ScrollController> scrollControllerList = [];
  late List<DayData> dayData;
  List<DayData>? dayData2;

  @override
  void initState() {
    // LhrGlobalVar.dayData = widget.dayData;
    dayDataOutside = widget.dayData;
    dayData = widget.dayData;
    widget.dayData.forEach((element) {
      scrollControllerList.add(ScrollController());
    });

    if (widget.dayData2 != null) {
      dayData2 = widget.dayData2;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<FlSpot> spots = [];
    return Container(
      height: 400,
      child: PageView.builder(
        physics: NeverScrollableScrollPhysics(),
        reverse: true,
        onPageChanged: (value) {
          setState(() {
            currentPage = value;
          });
        },
        controller: controller,
        itemCount: dayData.length,
        itemBuilder: (context, index) {
          for (var i = 0; i < widget.dayData[index].graphData!.length; i++) {
            // print();
            // print('$i ${widget.dayData[index].graphData[i].record}');
          }
          List<GraphDataDay> graphData = dayData[index].graphData!;

          if (!graphData.every((element) => element.record == null)) {
            double biggest = 0;
            double lowest = 0;

            for (int i = 0; i < graphData.length; i++) {
              // print(
              //     'data is day ${graphData[i].day} record ${graphData[i].record}');
              // if (graphData[i].record !=
              //         null &&
              //     graphData[i].record >
              //         biggest) {
              //   biggest = graphData[i].record;
              // }

              if (graphData[i].record != null) {
                spots.add(
                  FlSpot(graphData[i].day!.toDouble(), graphData[i].record!),
                );
              } else {
                //find average between two points

                //left side
                int indexLeft = i;
                for (var m = i; m >= 0; m--) {
                  if (graphData[m].record != null) {
                    indexLeft = m;
                    break;
                  }
                }

                //right side
                int indexRight = i;
                for (var m = i; m < graphData.length; m++) {
                  if (graphData[m].record != null) {
                    indexRight = m;
                    break;
                  }
                }

                //get average of those two points
                if (indexRight != i && indexLeft != i) {
                  graphData[i].record = double.parse(
                      ((graphData[indexLeft].record! +
                                  graphData[indexRight].record!) /
                              2)
                          .toStringAsFixed(2));
                } else if (indexRight == i && indexLeft != i) {
                  graphData[i].record = graphData[indexLeft].record;
                } else if (indexLeft == i && indexRight != i) {
                  graphData[i].record = graphData[indexRight].record;
                }

                spots.add(
                  FlSpot(graphData[i].day!.toDouble(), graphData[i].record!),
                );
              }
            }

            GraphDataDay biggestSpot =
                graphData.firstWhere((element) => element.record != null);

            for (var i = 0; i < graphData.length; i++) {
              if (graphData[i].record != null) {
                if (graphData[i].record! > biggestSpot.record!) {
                  biggestSpot = graphData[i];
                }
              }
            }

            biggest = biggestSpot.record!;
            // print('biggest is $biggest');

            GraphDataDay lowestSpot =
                graphData.firstWhere((element) => element.record != null);

            for (var i = 0; i < graphData.length; i++) {
              if (graphData[i].record != null) {
                if (graphData[i].record! < lowestSpot.record!) {
                  lowestSpot = graphData[i];
                }
              }
            }

            lowest = lowestSpot.record!;
            // print('lowest is $lowest');

            //2nd

            List<GraphDataDay>? graphData2;
            if (widget.dayData2 != null) {
              graphData2 = dayData2![index].graphData!;

              for (int i = 0; i < graphData2.length; i++) {
                if (graphData2[i].record != null) {
                  spots.add(
                    FlSpot(
                        graphData2[i].day!.toDouble(), graphData2[i].record!),
                  );
                } else {
                  //find average between two points

                  //left side
                  int indexLeft = i;
                  for (var m = i; m >= 0; m--) {
                    if (graphData2[m].record != null) {
                      indexLeft = m;
                      break;
                    }
                  }

                  //right side
                  int indexRight = i;
                  for (var m = i; m < graphData2.length; m++) {
                    if (graphData2[m].record != null) {
                      indexRight = m;
                      break;
                    }
                  }

                  //get average of those two points
                  if (indexRight != i && indexLeft != i) {
                    graphData2[i].record = double.parse(
                        ((graphData2[indexLeft].record! +
                                    graphData2[indexRight].record!) /
                                2)
                            .toStringAsFixed(2));
                  } else if (indexRight == i && indexLeft != i) {
                    graphData2[i].record = graphData2[indexLeft].record;
                  } else if (indexLeft == i && indexRight != i) {
                    graphData2[i].record = graphData2[indexRight].record;
                  }

                  spots.add(
                    FlSpot(
                        graphData2[i].day!.toDouble(), graphData2[i].record!),
                  );
                }
              }

              GraphDataDay biggestSpot2 =
                  graphData2.firstWhere((element) => element.record != null);

              for (var i = 0; i < graphData2.length; i++) {
                if (graphData2[i].record != null) {
                  if (graphData2[i].record! > biggestSpot.record!) {
                    biggestSpot = graphData2[i];
                  }
                }
              }

              double biggest2 = biggestSpot2.record!;
              // print('biggest is $biggest');

              GraphDataDay lowestSpot2 =
                  graphData2.firstWhere((element) => element.record != null);

              for (var i = 0; i < graphData2.length; i++) {
                if (graphData2[i].record != null) {
                  if (graphData2[i].record! < lowestSpot.record!) {
                    lowestSpot = graphData2[i];
                  }
                }
              }

              if (biggest2 > biggest) {
                biggest = biggest2;
              }

              double lowest2 = lowestSpot2.record!;
              // print('lowest is $lowest');

              if (lowest2 < lowest) {
                lowest = lowest2;
              }
            }

            return Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                      onPressed: () {
                        controller.animateToPage(currentPage + 1,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);
                      },
                      child: Icon(Icons.navigate_before),
                    ),
                    Text(() {
                      if (dayData[index].month == 1) {
                        return 'Jan';
                      } else if (dayData[index].month == 2) {
                        return 'Feb';
                      } else if (dayData[index].month == 3) {
                        return 'Mar';
                      } else if (dayData[index].month == 4) {
                        return 'Apr';
                      } else if (dayData[index].month == 5) {
                        return 'May';
                      } else if (dayData[index].month == 6) {
                        return 'Jun';
                      } else if (dayData[index].month == 7) {
                        return 'Jul';
                      } else if (dayData[index].month == 8) {
                        return 'Aug';
                      } else if (dayData[index].month == 9) {
                        return 'Sep';
                      } else if (dayData[index].month == 10) {
                        return 'Oct';
                      } else if (dayData[index].month == 11) {
                        return 'Nov';
                      } else if (dayData[index].month == 12) {
                        return 'Dec';
                      }
                    }()!),
                    TextButton(
                      onPressed: () {
                        controller.animateToPage(currentPage - 1,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);
                      },
                      child: Icon(Icons.navigate_next),
                    ),
                  ],
                ),
                Text('${dayData[index].year}'),
                SizedBox(height: 10),

                if (!graphData.every((element) => element.record == null))
                  Scrollbar(
                    controller: scrollControllerList[index],
                    thumbVisibility: true,
                    child: SingleChildScrollView(
                      controller: scrollControllerList[index],
                      scrollDirection: Axis.horizontal,
                      child: Container(
                        padding: EdgeInsets.all(10),
                        height: 300,
                        width: 550,
                        child: LineChart(
                          LineChartData(
                            borderData: FlBorderData(
                              show: true,
                              border: Border(
                                bottom: BorderSide(
                                  color: Colors.grey,
                                  width: 1,
                                ),
                                left: BorderSide(
                                  color: Colors.transparent,
                                ),
                                right: BorderSide(
                                  color: Colors.transparent,
                                ),
                                top: BorderSide(
                                  color: Colors.transparent,
                                  // width: 1,
                                ),
                              ),
                            ),
                            lineBarsData: [
                              LineChartBarData(
                                preventCurveOverShooting: true,
                                spots: graphData
                                    .map((e) =>
                                        FlSpot(e.day!.toDouble(), e.record!))
                                    .toList(),
                                isCurved: true,
                                color: Color(0xFF3586FF),
                                belowBarData: BarAreaData(
                                    // gradientColorStops: [0.],
                                    // show: true,
                                    // colors: [
                                    //   Colors.blue.withOpacity(0.1),
                                    //   Colors.white,
                                    // ],
                                    // gradientFrom: Offset(0.5, 0),
                                    // gradientTo: Offset(0.5, 1.8),
                                    ),
                                barWidth: 2.5,
                                isStrokeCapRound: true,
                                // gradientFrom: Offset(0, 5),
                                dotData: FlDotData(
                                  getDotPainter: (
                                    FlSpot spot,
                                    double percent,
                                    LineChartBarData barData,
                                    int index,
                                  ) {
                                    return FlDotCirclePainter(
                                      radius: 2,
                                      color: Color(0xFF3586FF),
                                      strokeColor: Color(0xFF3586FF),
                                      strokeWidth: 1,
                                    );
                                  },
                                  show: true,
                                  // checkToShowDot: (spot, barData) {
                                  //   FlSpot spotNew = FlSpot(
                                  //     spot.x,
                                  //     spots[spots.indexWhere(
                                  //             (element) => element.x == spot.x)]
                                  //         .y,
                                  //   );

                                  //   // print(spotNew.y);

                                  //   if (spotNew.y != null) {
                                  //     return true;
                                  //   } else {
                                  //     return false;
                                  //   }
                                  // },
                                ),
                              ),
                              if (graphData2 != null)
                                LineChartBarData(
                                  preventCurveOverShooting: true,
                                  spots: graphData2
                                      .map((e) =>
                                          FlSpot(e.day!.toDouble(), e.record!))
                                      .toList(),
                                  isCurved: true,
                                  color: Color(0xFFEF0056),
                                  belowBarData: BarAreaData(
                                      // gradientColorStops: [0.],
                                      // show: true,
                                      // colors: [
                                      //   Colors.blue.withOpacity(0.1),
                                      //   Colors.white,
                                      // ],
                                      // gradientFrom: Offset(0.5, 0),
                                      // gradientTo: Offset(0.5, 1.8),
                                      ),
                                  barWidth: 2.5,
                                  isStrokeCapRound: true,
                                  // gradientFrom: Offset(0, 5),
                                  // dotData: FlDotData(
                                  //   getDotPainter: (
                                  //     FlSpot spot,
                                  //     double percent,
                                  //     LineChartBarData barData,
                                  //     int index,
                                  //   ) {
                                  //     return FlDotCirclePainter(
                                  //       radius: 2,
                                  //       color: Color(0xFFEF0056),
                                  //       strokeColor: Color(0xFFEF0056),
                                  //       strokeWidth: 1,
                                  //     );
                                  //   },
                                  //   show: true,
                                  //   checkToShowDot: (spot, barData) {
                                  //     FlSpot spotNew = FlSpot(
                                  //       spot.x,
                                  //       spots[spots.indexWhere((element) =>
                                  //               element.x == spot.x)]
                                  //           .y,
                                  //     );

                                  //     // print(spotNew.y);

                                  //     if (spotNew.y != null) {
                                  //       return true;
                                  //     } else {
                                  //       return false;
                                  //     }
                                  //   },
                                  // ),
                                ),
                            ],
                            minX: 1,
                            maxX: () {
                              if (dayData[index].month == 1) {
                                return 31;
                              } else if (dayData[index].month == 2) {
                                return 28;
                              } else if (dayData[index].month == 3) {
                                return 31;
                              } else if (dayData[index].month == 4) {
                                return 30;
                              } else if (dayData[index].month == 5) {
                                return 31;
                              } else if (dayData[index].month == 6) {
                                return 30;
                              } else if (dayData[index].month == 7) {
                                return 31;
                              } else if (dayData[index].month == 8) {
                                return 31;
                              } else if (dayData[index].month == 9) {
                                return 30;
                              } else if (dayData[index].month == 10) {
                                return 31;
                              } else if (dayData[index].month == 11) {
                                return 30;
                              } else if (dayData[index].month == 12) {
                                return 31;
                              }
                            }()!
                                .toDouble(),
                            minY:
                                ((lowest.ceil() / 10).ceil() * 10).toDouble() -
                                    25,
                            maxY:
                                ((biggest.ceil() / 10).ceil() * 10).toDouble() +
                                    25,
                            titlesData: FlTitlesData(
                              leftTitles: AxisTitles(
                                sideTitles: SideTitles(
                                  interval: 10,
                                ),
                              ),
                              bottomTitles: AxisTitles(
                                axisNameWidget: Text('trend_day'.tr()),
                                sideTitles: SideTitles(
                                  // reservedSize: 30,
                                  // interval: 5,
                                  showTitles: true,
                                ),
                              ),
                              topTitles: AxisTitles(
                                sideTitles: SideTitles(showTitles: false),
                              ),
                              rightTitles: AxisTitles(
                                sideTitles: SideTitles(showTitles: false),
                              ),
                            ),
                            gridData: FlGridData(
                              show: true,
                              drawHorizontalLine: true,
                              checkToShowHorizontalLine: (value) {
                                if (value % 10 == 0) {
                                  return true;
                                } else {
                                  return false;
                                }
                              },
                            ),

                            // axisTitleData: FlAxisTitleData(
                            //   show: true,
                            //   bottomTitle: AxisTitle(
                            //     margin: 0,
                            //     showTitle: true,
                            //     titleText: 'trend_day'.tr(),
                            //     textStyle: TextStyle(
                            //       color: Colors.black,
                            //       fontWeight: FontWeight.bold,
                            //       fontSize: 14,
                            //     ),
                            //   ),
                            // ),
                          ),
                        ),
                      ),
                    ),
                  ),

                //sf chart
                // SfCartesianChart(
                //   legend: Legend(
                //     isVisible: true,
                //     position: LegendPosition.bottom,
                //     // Border color and border width of legend
                //     // borderColor: Colors.black,
                //     // borderWidth: 2,
                //   ),
                //   tooltipBehavior: TooltipBehavior(enable: true),
                //   // Initialize category axis
                //   primaryXAxis: CategoryAxis(
                //     title: AxisTitle(text: 'Day'),
                //   ),
                //   primaryYAxis: NumericAxis(
                //     title: AxisTitle(text: unit),
                //   ),
                //   series: <LineSeries<GraphDataDay, String>>[
                //     LineSeries<GraphDataDay, String>(
                //       name: title,
                //       markerSettings:
                //           MarkerSettings(isVisible: true),
                //       // width: 2.5,
                //       // Bind data source
                //       sortingOrder: SortingOrder.ascending,
                //       dataSource: graphData,
                //       xValueMapper: (GraphDataDay data, _) =>
                //           '${data.day}',
                //       yValueMapper: (GraphDataDay data, _) =>
                //           data.record,
                //       emptyPointSettings: EmptyPointSettings(
                //         mode: EmptyPointMode.drop,
                //       ),
                //     ),
                //   ],
                // ),
              ],
            );
          } else {
            //here if no data

            return Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                      onPressed: () {
                        controller.animateToPage(currentPage + 1,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);
                      },
                      child: Icon(Icons.navigate_before),
                    ),
                    Text(() {
                      if (dayData[index].month == 1) {
                        return 'Jan';
                      } else if (dayData[index].month == 2) {
                        return 'Feb';
                      } else if (dayData[index].month == 3) {
                        return 'Mar';
                      } else if (dayData[index].month == 4) {
                        return 'Apr';
                      } else if (dayData[index].month == 5) {
                        return 'May';
                      } else if (dayData[index].month == 6) {
                        return 'Jun';
                      } else if (dayData[index].month == 7) {
                        return 'Jul';
                      } else if (dayData[index].month == 8) {
                        return 'Aug';
                      } else if (dayData[index].month == 9) {
                        return 'Sep';
                      } else if (dayData[index].month == 10) {
                        return 'Oct';
                      } else if (dayData[index].month == 11) {
                        return 'Nov';
                      } else if (dayData[index].month == 12) {
                        return 'Dec';
                      }
                    }()!),
                    TextButton(
                      onPressed: () {
                        controller.animateToPage(currentPage - 1,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);
                      },
                      child: Icon(Icons.navigate_next),
                    ),
                  ],
                ),
                Text('${dayData[index].year}'),
                SizedBox(height: 10),
                Center(
                    child: Text(
                  'no_data'.tr(),
                  style: TextStyle(
                      color: Color(0xFFC8C7DD),
                      fontWeight: FontWeight.bold,
                      fontSize: 12),
                ))
                // Center(
                //   child: Text(
                //     'No data yet.... Please insert data',
                //   ),
                // ),
              ],
            );
          }
        },
      ),
    );
  }
}
