import 'dart:io';

import 'package:camera/camera.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:image_picker/image_picker.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

class UploadDocumentScreen extends StatefulWidget {
  final int? selectedDocumentType;
  final String? selectedDocumentTypeName;

  const UploadDocumentScreen(
      {super.key, this.selectedDocumentType, this.selectedDocumentTypeName});

  @override
  _UploadDocumentScreenState createState() => _UploadDocumentScreenState();
}

class _UploadDocumentScreenState extends State<UploadDocumentScreen> {
  final _pageController = PageController();
  int? currentPage = 0;
  int? selectedDocumentType;
  File? attachment;
  String? fileName = '';
  int? indexSelectedImage = 0;
  bool? selectedFromGrid = false;
  String? selectedDocumentTypeName;

  List<AssetEntity>? _mediaList = [];

  File? imageFile;

  @override
  void initState() {
    init();
    selectedDocumentTypeName = widget.selectedDocumentTypeName;
    selectedDocumentType = widget.selectedDocumentType;
    super.initState();
  }

  TextEditingController fileNameController = TextEditingController();

  void init() async {
    fileNameController.text = '';
    await PhotoManager.requestPermissionExtend();
    List<AssetPathEntity> albums = await PhotoManager.getAssetPathList(
      onlyAll: true,
      type: RequestType.image,
    );
    print(albums);
    List<AssetEntity> media =
        await albums[0].getAssetListPaged(page: 0, size: 15);
    setState(() {
      _mediaList = media;
    });
  }

  Future<void> _pickImage(ImageSource source) async {
    XFile? selected = await ImagePicker().pickImage(
      source: source,
      imageQuality: 25,
    );

    if (selected != null) {
      setState(() {
        imageFile = File(selected.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: LhrScaffold(
        appBarTitle: 'lifetime'.tr(),
        child: Padding(
          padding: EdgeInsets.only(top: 15, left: 10, right: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'upload_doc'.tr(),
                style: TextStyle(fontSize: 16),
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 10,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: currentPage == 0 ? Colors.blue : Colors.white,
                      border: Border.all(color: Colors.blue),
                    ),
                    child: Text(' '),
                  ),
                  SizedBox(width: 10),
                  Container(
                    width: 10,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: currentPage == 1 ? Colors.blue : Colors.white,
                      border: Border.all(color: Colors.blue),
                    ),
                    child: Text(' '),
                  ),
                ],
              ),
              SizedBox(height: 10),

              //scroll here
              Expanded(
                child: PageView(
                  controller: _pageController,
                  onPageChanged: (value) {
                    setState(() {
                      currentPage = value;
                    });
                  },
                  children: [
                    //first page
                    Container(
                      padding: EdgeInsets.all(15),
                      child: Column(
                        children: [
                          Text('document_type'.tr()),
                          SizedBox(height: 10),
                          Text(
                            'please_select_document'.tr(),
                            style: TextStyle(
                              color: Color(0xFFAFB2C3),
                              // fontSize: 12,
                            ),
                          ),
                          SizedBox(height: 30),

                          //user choose document
                          Container(
                            // color: Colors.grey,
                            width: width * 0.7,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedDocumentType = 4;
                                      selectedDocumentTypeName =
                                          'test_results'.tr();
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: selectedDocumentType == 4
                                          ? Color(0xFF3586FF)
                                          : Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 5),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'assets/images/lhr/Test Icon.png',
                                          color: selectedDocumentType == 4
                                              ? Colors.white
                                              : Colors.black,
                                          height: 40,
                                        ),
                                        SizedBox(width: 20),
                                        Text(
                                          'test_results'.tr(),
                                          style: TextStyle(
                                            color: selectedDocumentType == 4
                                                ? Colors.white
                                                : Color(0xFF54586C),
                                            // fontSize: 12,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedDocumentType = 1;
                                      selectedDocumentTypeName =
                                          'prescriptions'.tr();
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: selectedDocumentType == 1
                                          ? Color(0xFF3586FF)
                                          : Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 5),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'assets/images/lhr/Prescription Icon.png',
                                          color: selectedDocumentType == 1
                                              ? Colors.white
                                              : Colors.black,
                                          height: 40,
                                        ),
                                        SizedBox(width: 20),
                                        Text(
                                          'prescriptions'.tr(),
                                          style: TextStyle(
                                            color: selectedDocumentType == 1
                                                ? Colors.white
                                                : Color(0xFF54586C),
                                            // fontSize: 12,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedDocumentType = 2;
                                      selectedDocumentTypeName =
                                          'medical_cert'.tr();
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: selectedDocumentType == 2
                                          ? Color(0xFF3586FF)
                                          : Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 5),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'assets/images/lhr/Medical Certificate Icon.png',
                                          color: selectedDocumentType == 2
                                              ? Colors.white
                                              : Colors.black,
                                          height: 40,
                                        ),
                                        SizedBox(width: 20),
                                        Text(
                                          'medical_cert'.tr(),
                                          style: TextStyle(
                                            color: selectedDocumentType == 2
                                                ? Colors.white
                                                : Color(0xFF54586C),
                                            // fontSize: 12,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedDocumentType = 3;
                                      selectedDocumentTypeName =
                                          'vaccine_cert'.tr();
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: selectedDocumentType == 3
                                          ? Color(0xFF3586FF)
                                          : Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 5),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'assets/images/lhr/Vaccine Icon.png',
                                          color: selectedDocumentType == 3
                                              ? Colors.white
                                              : Colors.black,
                                          height: 40,
                                        ),
                                        SizedBox(width: 20),
                                        Text(
                                          'vaccine_cert'.tr(),
                                          style: TextStyle(
                                            color: selectedDocumentType == 3
                                                ? Colors.white
                                                : Color(0xFF54586C),
                                            // fontSize: 12,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedDocumentType = 5;
                                      selectedDocumentTypeName =
                                          'refer_letter'.tr();
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: selectedDocumentType == 5
                                          ? Color(0xFF3586FF)
                                          : Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 5),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'assets/images/lhr/referral_letter.png',
                                          color: selectedDocumentType == 5
                                              ? Colors.white
                                              : Colors.black,
                                          height: 40,
                                        ),
                                        SizedBox(width: 20),
                                        Text(
                                          'refer_letter'.tr(),
                                          style: TextStyle(
                                            color: selectedDocumentType == 5
                                                ? Colors.white
                                                : Color(0xFF54586C),
                                            // fontSize: 12,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedDocumentType = 6;
                                      selectedDocumentTypeName =
                                          'invoice_receipt'.tr();
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: selectedDocumentType == 6
                                          ? Color(0xFF3586FF)
                                          : Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 5),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'assets/images/lhr/invoice.png',
                                          color: selectedDocumentType == 6
                                              ? Colors.white
                                              : Colors.black,
                                          height: 40,
                                        ),
                                        SizedBox(width: 20),
                                        Text(
                                          'invoice_receipt'.tr(),
                                          style: TextStyle(
                                            color: selectedDocumentType == 6
                                                ? Colors.white
                                                : Color(0xFF54586C),
                                            // fontSize: 12,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),

                          //button next
                          // SizedBox(height: 50),
                          Spacer(),
                          Container(
                            width: double.infinity,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Color(0xFFEF0056),
                                shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.circular(15), // <-- Radius
                                ),
                              ),
                              onPressed: () {
                                _pageController.nextPage(
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.ease,
                                );
                              },
                              child: Text(
                                'next'.tr(),
                                // style: TextStyle(fontSize: 12),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    //second page
                    Container(
                      padding: EdgeInsets.all(15),
                      child: Column(
                        children: [
                          Text('select_file_capture'.tr()),
                          SizedBox(height: 7.5),
                          Container(
                            width: width * 0.5,
                            child: Text(
                              'pick_existing_capture'.tr(),
                              style: TextStyle(
                                color: Color(0xFFAFB2C3),
                                // fontSize: 12,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(height: 10),
                          if (imageFile != null || attachment != null) ...[
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: EdgeInsets.only(left: 30),
                                child: Text(
                                  'document_name'.tr(),
                                ),
                              ),
                            ),
                            SizedBox(height: 5),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              child: TextField(
                                controller: fileNameController,
                                decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.fromLTRB(20, 5, 5, 20),

                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(color: Colors.grey),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(color: Colors.grey),
                                  ),
                                  // hintText: 'ex_name'.tr(),
                                ),
                              ),
                            ),
                            SizedBox(height: 10),
                          ],

                          // SizedBox(height: 10),
                          // if (_imageFile.length != 0)
                          //   GridView.count(
                          //     scrollDirection: Axis.vertical,
                          //     shrinkWrap: true,
                          //     crossAxisCount: 3,
                          //     children: List.generate(
                          //       12,
                          //       (index) {
                          //         return Container(
                          //           height: 200,
                          //           width: 200,
                          //           // Display the image thumbnail
                          //           child: Image.file(
                          //             _imageFile[index],
                          //             fit: BoxFit.cover,
                          //           ),
                          //         );
                          //       },
                          //     ),
                          //   ),

                          Expanded(
                            child: Container(
                                decoration: BoxDecoration(
                                  color: Color(0xFFF6F6F9),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                margin: EdgeInsets.all(10),
                                padding: EdgeInsets.all(10),
                                // height: height * 0.7,
                                child: () {
                                  if (imageFile == null && attachment == null) {
                                    return Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Image.asset(
                                              'assets/images/lhr/camera_upload.png',
                                              height: 17,
                                              color: Color(0xFF9396A5),
                                            ),
                                            SizedBox(width: 5),
                                            Text(
                                              'gallery'.tr(),
                                              style: TextStyle(
                                                color: Color(0xFF9396A5),
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(height: 10),
                                        Expanded(
                                          child: GridView.builder(
                                            shrinkWrap: true,
                                            itemCount: _mediaList!.length,
                                            gridDelegate:
                                                SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisSpacing: 10,
                                              mainAxisSpacing: 10,
                                              crossAxisCount: 3,
                                            ),
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              return FutureBuilder(
                                                future: _mediaList![index].file,
                                                builder: (BuildContext context,
                                                    snapshot) {
                                                  if (snapshot
                                                          .connectionState ==
                                                      ConnectionState.done)
                                                    return GestureDetector(
                                                      onTap: () {
                                                        setState(() {
                                                          selectedFromGrid =
                                                              true;
                                                          imageFile =
                                                              snapshot.data;

                                                          fileNameController
                                                                  .text =
                                                              imageFile!.path
                                                                  .split('/')
                                                                  .last
                                                                  .split('.')
                                                                  .first;
                                                        });
                                                      },
                                                      child: Stack(
                                                        children: [
                                                          Container(
                                                            height: 100,
                                                            width: 100,
                                                            child: ClipRRect(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          7.5),
                                                              child: Image.file(
                                                                snapshot.data!,
                                                                fit: BoxFit
                                                                    .fitWidth,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  return Container();
                                                },
                                              );
                                            },
                                          ),
                                        ),
                                        SizedBox(height: 5),
                                        GestureDetector(
                                          onTap: () {
                                            _pickImage(ImageSource.gallery);
                                          },
                                          child: Padding(
                                            padding: EdgeInsets.only(
                                              left: 5,
                                              top: 10,
                                            ),
                                            child: Text(
                                              'choose_picture_gallery'.tr(),
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.blue,
                                                decoration:
                                                    TextDecoration.underline,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  } else {
                                    if (attachment != null) {
                                      return Container(
                                        width: double.infinity,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            //assets/images/lhr/jpg.png
                                            //assets/images/lhr/pdf.png

                                            if (fileName!.contains('pdf'))
                                              Image.asset(
                                                'assets/images/lhr/pdf.png',
                                                width: width * 0.2,
                                              ),

                                            if (fileName!.contains('jpg') ||
                                                fileName!.contains('jpeg') ||
                                                fileName!.contains('png') ||
                                                fileName!.contains('JPG') ||
                                                fileName!.contains('JPEG') ||
                                                fileName!.contains('PNG'))
                                              Image.asset(
                                                'assets/images/lhr/jpg.png',
                                                width: width * 0.2,
                                              ),
                                          ],
                                        ),
                                      );
                                    } else {
                                      return Stack(
                                        children: [
                                          Center(
                                            child: Container(
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                  image: FileImage(imageFile!),
                                                  colorFilter: ColorFilter.mode(
                                                    Colors.black
                                                        .withOpacity(0.3),
                                                    BlendMode.darken,
                                                  ),
                                                  // fit: BoxFit.fill,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Center(
                                            child: GestureDetector(
                                              onTap: () async {
                                                setState(() {
                                                  imageFile = null;
                                                });
                                              },
                                              child: Container(
                                                padding: EdgeInsets.all(10),
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    color: Colors.white,
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                ),
                                                child: Text(
                                                  'tap_change_photo'.tr(),
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      );
                                    }
                                  }
                                }()),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Text(
                              'disclaimer_10mb'.tr(),
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                          // SizedBox(height: 20),

                          if (imageFile == null && attachment == null) ...[
                            Container(
                              width: width * 0.7,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Color(0xFFEF0056),
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.circular(15), // <-- Radius
                                  ),
                                ),
                                onPressed: () async {
                                  List<CameraDescription> camera =
                                      await availableCameras();

                                  Navigator.of(context)
                                      .push(
                                    MaterialPageRoute(
                                      builder: (context) => TakePhoto(
                                        camera: camera,
                                      ),
                                    ),
                                  )
                                      .then((value) {
                                    print('value pop is $value');
                                    if (value != null) {
                                      setState(() {
                                        imageFile = value;
                                      });
                                    }
                                  });
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/images/lhr/camera_upload.png',
                                      height: 17,
                                    ),
                                    SizedBox(width: 10),
                                    Text(
                                      'take_a_photo'.tr(),
                                      // style: TextStyle(fontSize: 12),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            // SizedBox(height: 10),

                            Container(
                              width: width * 0.7,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Color(0xFFEF0056),
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.circular(15), // <-- Radius
                                  ),
                                ),
                                onPressed: () async {
                                  // String? resultFile = await FilePicker.platform
                                  //     .saveFile(
                                  //         type: FileType.custom,
                                  //         allowedExtensions: [
                                  //       'jpg',
                                  //       'png',
                                  //       'jpeg',
                                  //       'pdf'
                                  //     ]);

                                  FilePickerResult? filePicked =
                                      await FilePicker.platform.pickFiles(
                                    type: FileType.custom,
                                    allowedExtensions: [
                                      'jpg',
                                      'png',
                                      'jpeg',
                                      'pdf'
                                    ],
                                  );

                                  if (filePicked != null) {
                                    File result =
                                        File(filePicked.files.single.path!);

                                    setState(() {
                                      // if (result != null) {
                                      attachment = result;
                                      fileName =
                                          attachment!.path.split('/').last;
                                      fileNameController.text = attachment!.path
                                          .split('/')
                                          .last
                                          .split('.')
                                          .first;
                                      // }
                                    });

                                    print('filename is $fileName');
                                  }
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/images/lhr/file_upload.png',
                                      height: 17,
                                    ),
                                    SizedBox(width: 10),
                                    Text(
                                      'attach_document'.tr(),
                                      // style: TextStyle(fontSize: 12),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                          if (imageFile != null || attachment != null)
                            Container(
                              width: width * 0.7,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Color(0xFFEF0056),
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.circular(15), // <-- Radius
                                  ),
                                ),
                                onPressed: () async {
                                  if (fileNameController.text != '') {
                                    if (imageFile != null) {
                                      var parts = imageFile!.path.split('/');

                                      var parts2 = parts.last.split('.');

                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) => BlocProvider(
                                            create: (context) =>
                                                SubmitDocumentCubit(),
                                            child: UploadDocumentResult(
                                              documentFile: imageFile,
                                              documentType:
                                                  '$selectedDocumentType',
                                              fileName:
                                                  '${fileNameController.text}.${parts2.last}',
                                              documentTypeName:
                                                  selectedDocumentTypeName,
                                            ),
                                          ),
                                        ),
                                      );
                                    } else if (attachment != null) {
                                      var parts = attachment!.path.split('/');

                                      var parts2 = parts.last.split('.');

                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) => BlocProvider(
                                            create: (context) =>
                                                SubmitDocumentCubit(),
                                            child: UploadDocumentResult(
                                              documentFile: attachment,
                                              documentType:
                                                  '$selectedDocumentType',
                                              fileName:
                                                  '${fileNameController.text}.${parts2.last}',
                                              documentTypeName:
                                                  selectedDocumentTypeName,
                                            ),
                                          ),
                                        ),
                                      );
                                    }
                                  } else {
                                    Fluttertoast.showToast(
                                      msg: 'type_document_name'.tr(),
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white,
                                      fontSize: 16.0,
                                    );
                                  }
                                },
                                child: Text(
                                  'submit'.tr(),
                                  // style: TextStyle(fontSize: 12),
                                ),
                              ),
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
