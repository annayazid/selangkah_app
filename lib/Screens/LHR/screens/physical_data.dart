import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/utils/constants.dart';

class PhysicalData extends StatefulWidget {
  PhysicalData({Key? key}) : super(key: key);

  @override
  State<PhysicalData> createState() => _PhysicalDataState();
}

class _PhysicalDataState extends State<PhysicalData> {
  @override
  void initState() {
    super.initState();
    context.read<PhysicalDataCubit>().getPhysicalData();
  }

  @override
  Widget build(BuildContext context) {
    return LhrScaffold(
      appBarTitle: 'lifetime'.tr(),
      child: BlocBuilder<PhysicalDataCubit, PhysicalDataState>(
        builder: (context, state) {
          print('state is $state');
          if (state is PhysicalDataLoaded) {
            return PhysicalDataLoadedWidget(
              name: state.name!,
              district: state.district!,
              age: state.age!,
              imgData: state.imgbytes!,
              physicalRecord: state.physicalRecord!,
            );
          } else {
            return SpinKitFadingCircle(
              color: kPrimaryColor,
              size: 20,
            );
          }
        },
      ),
    );
  }
}

class PhysicalDataLoadedWidget extends StatelessWidget {
  final String? name;
  final String? district;
  final int? age;
  final Uint8List? imgData;
  final PhysicalRecord? physicalRecord;

  const PhysicalDataLoadedWidget(
      {super.key,
      this.name,
      this.district,
      this.age,
      this.imgData,
      this.physicalRecord});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 15),
          Container(
            width: width * 0.95,
            padding: EdgeInsets.all(35),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  offset: Offset(0, 0),
                  blurRadius: 5,
                  // spreadRadius: 3,
                ),
              ],
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                // CircleAvatar(
                //   backgroundImage: () {
                //     if (imgData == null) {
                //       return AssetImage('assets/icons/male_avatar.png');
                //     } else {
                //       return MemoryImage(imgData);
                //     }
                //   }(),
                //   backgroundColor: Colors.blueGrey,
                //   radius: 60,
                // ),
                SizedBox(height: 20),
                Text(
                  name!,
                  style: TextStyle(fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('${age.toString()} years'),
                    SizedBox(width: 5),
                    Text('/'),
                    SizedBox(width: 5),
                    Text(district != null ? district! : ''),
                  ],
                ),
                SizedBox(height: 20),
                PhysicalDataRecord(
                  physicalRecord: physicalRecord!,
                  from: 'lhr',
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
        ],
      ),
    );
  }
}

class PhysicalDataRecord extends StatelessWidget {
  final PhysicalRecord? physicalRecord;
  final String? from;

  const PhysicalDataRecord({super.key, this.physicalRecord, this.from});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: physicalRecord!.data!.length,
            itemBuilder: (BuildContext context, int index) {
              String color = '0xFF${physicalRecord!.data![index]!.color}';
              int convertedColor = int.parse(color);

              return Container(
                child: Column(
                  children: [
                    PhysicalDataContainer(
                      image: physicalRecord!.data![index]!.logo!,
                      title: physicalRecord!.data![index]!.parameter!,
                      desc: physicalRecord!.data![index]!.dataInput!,
                      unit: physicalRecord!.data![index]!.unit!,
                      color: convertedColor,
                      from: from!,
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              );
            }),
        SizedBox(height: 30),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            // Expanded(
            //   child: GestureDetector(
            //     child: Container(
            //       // color: Colors.red,
            //       // padding: EdgeInsets.all(7),
            //       child: Text(
            //         'Show All Trends',
            //         style: TextStyle(
            //             color: Color(0xFF4A4A49),
            //             fontWeight: FontWeight.bold),
            //         textAlign: TextAlign.center,
            //       ),
            //     ),
            //     onTap: () {
            //       Navigator.of(context).push(
            //         MaterialPageRoute(builder: (context) => Parent()),
            //       );
            //     },
            //   ),
            // ),
            Expanded(
              child: GestureDetector(
                child: Container(
                  // padding: EdgeInsets.all(7),
                  child: Text(
                    'Add Measurement',
                    style: TextStyle(
                        color: Color(0xFF4A4A49), fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
                onTap: () async {
                  await Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => MultiBlocProvider(
                        providers: [
                          BlocProvider(
                            create: (context) => DropdownMeasurementCubit(),
                          ),
                          BlocProvider(
                            create: (context) => InputMeasurementCubit(),
                          ),
                          BlocProvider(
                            create: (context) => ReturnAnswerCubit(),
                          ),
                        ],
                        child: AddMeasurement(from: 'Physical Data Page'),
                      ),
                    ),
                  );
                  //flag from profile page
                  if (from == 'profile')
                    context.read<HealthRecordCubit>().getHealthRecord();

                  //flag from LHR page
                  if (from == 'lhr')
                    context.read<PhysicalDataCubit>().getPhysicalData();
                },
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class PhysicalDataContainer extends StatelessWidget {
  final String? image;
  final String? title;
  final String? desc;
  final String? unit;
  final int? color;
  final String? from;

  const PhysicalDataContainer(
      {super.key,
      this.image,
      this.title,
      this.desc,
      this.unit,
      this.color,
      this.from});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () async {
        //
        await Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => BlocProvider(
              create: (context) => DeleteRecordCubit(),
              child: RecordTrend(
                  title: title, unit: unit, image: image, color: color),
            ),
            // MultiBlocProvider(
            //   providers: [
            //     BlocProvider(
            //       create: (context) => RecordTrendCubit(),
            //     ),
            //     BlocProvider(
            //       create: (context) => DeleteRecordCubit(),
            //     )
            //   ],
            //   child: RecordTrend(
            //       title: title, unit: unit, image: image, color: color),
            // ),
          ),
        );

        print('calling context main');

        //flag from profile page
        if (from == 'profile')
          context.read<HealthRecordCubit>().getHealthRecord();

        //flag from LHR page
        if (from == 'lhr') context.read<PhysicalDataCubit>().getPhysicalData();
      },
      child: Container(
        width: width * 0.80,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Color(0xFFEDEAE7),
          border: Border.all(
            color: Color(0xFFD7D1CA),
          ),
          borderRadius: BorderRadius.circular(30),
        ),
        child: Row(
          children: [
            Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Color(0xFFD7D1CA), shape: BoxShape.circle),
              child: Image.network(image!),
              width: 40,
              height: 40,
            ),
            SizedBox(width: 30),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title!,
                  style: TextStyle(
                    color: Color(0xFF333333),
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  desc!,
                  style: TextStyle(
                    color: Color(0xFF333333),
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
