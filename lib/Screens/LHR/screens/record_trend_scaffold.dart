import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

class RecordTrendScaffold extends StatelessWidget {
  final String? appBarTitle;
  final String? title;
  final String? parameter;
  final Widget? child;

  const RecordTrendScaffold(
      {super.key, this.appBarTitle, this.title, this.parameter, this.child});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      // backgroundColor: kBackgroundColor,
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: true,
        title: Text(
          appBarTitle!,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 15.0,
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: "Roboto",
          ),
        ),
      ),
      body: Column(
        children: [
          Container(
              height: height - AppBar().preferredSize.height,
              width: width,
              child: child),
        ],
      ),
      floatingActionButton: title!.contains('BMI')
          ? null
          : FloatingActionButton(
              onPressed: () {
                context
                    .read<DropdownMeasurementCubit>()
                    .getDropdownMeasurement();
              },
              child: BlocConsumer<DropdownMeasurementCubit,
                  DropdownMeasurementState>(
                listener: (context, state) async {
                  if (state is DropdownMeasurementLoaded) {
                    await Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => MultiBlocProvider(
                          providers: [
                            BlocProvider(
                              create: (context) => InputMeasurementCubit(),
                            ),
                            BlocProvider(
                              create: (context) => ReturnAnswerCubit(),
                            ),
                          ],
                          child: AddMeasurement(
                            parameter: parameter,
                            from: 'Floating Button',
                            dropdown: state.dropdown,
                          ),
                        ),
                      ),
                    );

                    context.read<RecordTrendCubit>().getRecordTrend(parameter!);
                  }
                  //else {
                  //   return SpinKitFadingCircle(
                  //     color: kPrimaryColor,
                  //     size: 20,
                  //   );
                  // }
                },
                builder: (context, state) {
                  return Icon(Icons.add);
                },
              ),
              backgroundColor: Color(0xFFEF0056),
            ),
      floatingActionButtonLocation: title!.contains('BMI')
          ? null
          : FloatingActionButtonLocation.centerFloat,
    );
  }
}
