import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

class HayatScreen extends StatefulWidget {
  HayatScreen({Key? key}) : super(key: key);

  @override
  State<HayatScreen> createState() => _HayatScreenState();
}

// class _PieData {
//   _PieData(this.xData, this.yData, [this.text]);
//   final String xData;
//   final num yData;
//   final String text;
// }

class _HayatScreenState extends State<HayatScreen> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    return LhrScaffold(
      appBarTitle: 'lifetime'.tr(),
      child: SingleChildScrollView(
          child: Column(
        children: [
          SizedBox(height: 15),
          Container(
            width: width * 0.95,
            padding: EdgeInsets.all(35),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  offset: Offset(0, 0),
                  blurRadius: 5,
                  // spreadRadius: 3,
                ),
              ],
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                Text(
                  'HAYAT',
                  style: TextStyle(
                      color: Color(0xFF79521B),
                      fontSize: 27,
                      fontWeight: FontWeight.bold),
                ),
                Divider(
                  color: Color(0xFF79521B),
                  thickness: 1,
                ),
                Text(
                  'lifetime'.tr(),
                  style: TextStyle(
                    color: Color(0xFF79521B),
                    fontSize: 18,
                  ),
                ),
                SizedBox(height: 15),
                // SfCircularChart(
                //     title: ChartTitle(text: 'Sales by sales person'),
                //     legend: Legend(isVisible: true),
                //     series:
                //     ),
                // <PieSeries<_PieData, String>>[
                //   PieSeries<_PieData, String>(
                //       explode: true,
                //       explodeIndex: 0,
                //       //  dataSource: pieData,
                //       xValueMapper: (_PieData data, _) => data.xData,
                //       yValueMapper: (_PieData data, _) => data.yData,
                //       dataLabelMapper: (_PieData data, _) => data.text,
                //       dataLabelSettings:
                //           DataLabelSettings(isVisible: true)),
                // ]

                // series: [
                //   RadialBarSeries(),
                // ],
                SizedBox(height: 100),
                HayatContainer(
                  color: Color(0xFFF7C4A8),
                  image: 'assets/images/lhr/critical_care.png',
                  title: 'Artificial Intelligence',
                  desc: '120 / WHO requirement',
                  percentage: '34 %',
                ),
                SizedBox(height: 20),
                HayatContainer(
                  color: Color(0xFFFFB4BE),
                  image: 'assets/images/lhr/step.png',
                  title: 'Steps',
                  desc: '2650 steps / 10 000 steps',
                  percentage: '21 %',
                ),
                SizedBox(height: 20),
                HayatContainer(
                  color: Color(0xFFF3DFB1),
                  image: 'assets/images/lhr/heart_point.png',
                  title: 'Heart Points',
                  desc: '16 / WHO requirement',
                  percentage: '58 %',
                ),
                SizedBox(height: 30),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: GestureDetector(
                        child: Container(
                          // color: Colors.red,
                          padding: EdgeInsets.all(7),
                          child: Row(
                            children: [
                              Icon(
                                FontAwesomeIcons.chartLine,
                                color: Color(0xFF79521B),
                              ),
                              SizedBox(width: 10),
                              Text(
                                'Physical Data',
                                style: TextStyle(
                                    color: Color(0xFF79521B),
                                    fontWeight: FontWeight.bold),
                                // textAlign: TextAlign.left,
                              ),
                            ],
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => BlocProvider(
                                  create: (context) => PhysicalDataCubit(),
                                  child: PhysicalData()),
                            ),
                          );
                        },
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        child: Container(
                          padding: EdgeInsets.all(7),
                          child: Row(
                            children: [
                              Icon(
                                FontAwesomeIcons.stethoscope,
                                color: Color(0xFF79521B),
                              ),
                              SizedBox(width: 10),
                              Text(
                                'Medical Record',
                                style: TextStyle(
                                    color: Color(0xFF79521B),
                                    fontWeight: FontWeight.bold),
                                // textAlign: TextAlign.end,
                              ),
                            ],
                          ),
                        ),
                        onTap: () {
                          //         Navigator.of(context).push(
                          //   MaterialPageRoute(
                          //     builder: (context) => MultiBlocProvider(
                          //       providers: [
                          //         BlocProvider(
                          //           create: (context) => profileID.ProfileBloc(),
                          //         ),
                          //         BlocProvider(
                          //           create: (context) => profileID.SliderBloc(),
                          //         ),
                          //       ],
                          //       child: profileID.SelangkahIDNewScreen(
                          //         page: 'saring_result',
                          //       ),
                          //     ),
                          //   ),
                          // );
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            width: width * 0.95,
            padding: EdgeInsets.all(35),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  offset: Offset(0, 0),
                  blurRadius: 5,
                  // spreadRadius: 3,
                ),
              ],
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          color: Color(0xFFF7C4A8), shape: BoxShape.circle),
                      child: Image.asset(
                        'assets/images/lhr/critical_care.png',
                        width: 30,
                        height: 30,
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Artificial Intelligence',
                            style: TextStyle(
                              color: Color(0xFF79521B),
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 5),
                          Text(
                            'Thursday, August 11',
                            style: TextStyle(
                              color: Color(0xFF79521B),
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            width: width * 0.95,
            padding: EdgeInsets.all(35),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  offset: Offset(0, 0),
                  blurRadius: 5,
                  // spreadRadius: 3,
                ),
              ],
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          color: Color(0xFFFFB4BE), shape: BoxShape.circle),
                      child: Image.asset(
                        'assets/images/lhr/step.png',
                        width: 30,
                        height: 30,
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Steps',
                            style: TextStyle(
                              color: Color(0xFF79521B),
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 5),
                          Text(
                            'Thursday, August 11',
                            style: TextStyle(
                              color: Color(0xFF79521B),
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            width: width * 0.95,
            padding: EdgeInsets.all(35),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  offset: Offset(0, 0),
                  blurRadius: 5,
                  // spreadRadius: 3,
                ),
              ],
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          color: Color(0xFFF3DFB1), shape: BoxShape.circle),
                      child: Image.asset(
                        'assets/images/lhr/heart_point.png',
                        width: 30,
                        height: 30,
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Heart Points',
                            style: TextStyle(
                              color: Color(0xFF79521B),
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 5),
                          Text(
                            'Thursday, August 11',
                            style: TextStyle(
                              color: Color(0xFF79521B),
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
        ],
      )),
    );
  }
}

class HayatContainer extends StatelessWidget {
  final Color? color;
  final String? image;
  final String? title;
  final String? desc;
  final String? percentage;

  const HayatContainer(
      {super.key,
      this.color,
      this.image,
      this.title,
      this.desc,
      this.percentage});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Color(0xFFEBE6DF),
        borderRadius: BorderRadius.circular(50),
      ),
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(color: color, shape: BoxShape.circle),
              child: Image.asset(
                image!,
                width: 30,
                height: 30,
              ),
            ),
          ),
          // SizedBox(
          //   width: 10,
          // ),
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title!,
                  style: TextStyle(
                    color: Color(0xFF79521B),
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  desc!,
                  style: TextStyle(
                    color: Color(0xFF79521B),
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Text(
              percentage!,
              style: TextStyle(
                color: Color(0xFF79521B),
                fontWeight: FontWeight.bold,
                fontSize: 27,
              ),
            ),
          )
        ],
      ),
    );
  }
}
