import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

class AddMeasurement extends StatefulWidget {
  final String? parameter;
  final String? from;
  final DropdownAddMeasurement? dropdown;

  const AddMeasurement({super.key, this.parameter, this.from, this.dropdown});

  @override
  State<AddMeasurement> createState() => _AddMeasurementState();
}

class _AddMeasurementState extends State<AddMeasurement> {
  final _pageController = PageController();
  DateTime? timeinput;
  DateTime? dateinput;
  String? selectedType;
  String? selectedTypeParam;
  int? indexSelectedType;
  int? currentPage = 0;

  //data nak submit
  List<TextEditingController> inputRecord = [];
  List<String> inputParam = [];

  @override
  void initState() {
    super.initState();
    // timeinput = formatDate(DateTime.now(), [HH, ':', nn, ' ', am]);
    timeinput = DateTime.now();
    dateinput = DateTime.now();
    if (widget.parameter != null) {
      selectedTypeParam = widget.parameter;
      indexSelectedType = widget.dropdown!.data!
          .indexWhere((element) => element!.parameter == selectedTypeParam);
      selectedType = widget.dropdown!.data![indexSelectedType!]!.title;
      context
          .read<InputMeasurementCubit>()
          .getInputMeasurement(selectedTypeParam!);
    }
  }

  // void updateInputRecord(
  //     List<TextEditingController> inputRecord, List<String> input) {
  //   setState(() {
  //     inputRecord = inputRecord;
  //     inputParam = input;
  //   });
  // }

  int indexImage = 0;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    return LhrScaffold(
      appBarTitle: 'lifetime'.tr(),
      child: Padding(
        padding: EdgeInsets.only(top: 30, left: 10, right: 10, bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'add_data'.tr(),
              style: TextStyle(fontSize: 16),
            ),
            SizedBox(height: 25),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 10,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: currentPage == 0 ? Colors.blue : Colors.white,
                    border: Border.all(color: Colors.blue),
                  ),
                  child: Text(' '),
                ),
                SizedBox(width: 10),
                Container(
                  width: 10,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: currentPage == 1 ? Colors.blue : Colors.white,
                    border: Border.all(color: Colors.blue),
                  ),
                  child: Text(' '),
                ),
              ],
            ),
            SizedBox(height: 25),
            Expanded(
              child: PageView(
                // scrollDirection: Axis.horizontal,
                // reverse: true,
                // allowImplicitScrolling: true,
                controller: _pageController,
                onPageChanged: (value) {
                  setState(() {
                    currentPage = value;
                  });
                },
                children: [
                  //first page
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Column(
                      children: [
                        Text('data_type'.tr()),
                        SizedBox(height: 10),
                        Text(
                          'add_data_type'.tr(),
                          style: TextStyle(
                            color: Color(0xFFAFB2C3),
                            // fontSize: 12,
                          ),
                        ),
                        SizedBox(height: 30),

                        //user choose data type
                        Container(
                          width: width * 0.8,
                          child: GridView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              crossAxisSpacing: 15,
                              mainAxisSpacing: 15,
                              childAspectRatio: 2.0,
                            ),
                            itemCount: widget.dropdown!.data!.length,
                            itemBuilder: (BuildContext context, int index) {
                              return GestureDetector(
                                onTap: () async {
                                  setState(() {
                                    selectedTypeParam = widget
                                        .dropdown!.data![index]!.parameter;
                                    selectedType =
                                        widget.dropdown!.data![index]!.title;
                                    indexSelectedType = index;
                                  });

                                  await context
                                      .read<InputMeasurementCubit>()
                                      .getInputMeasurement(selectedTypeParam!);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: selectedTypeParam ==
                                            widget.dropdown!.data![index]!
                                                .parameter
                                        ? Color(0xFF3586FF)
                                        : Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 5),
                                  child: Row(
                                    children: [
                                      Image.network(
                                        widget.dropdown!.data![index]!.logo!,
                                        width: 40,
                                        height: 40,
                                        color: selectedTypeParam ==
                                                widget.dropdown!.data![index]!
                                                    .parameter
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                      SizedBox(width: 10),
                                      Expanded(
                                        child: Text(
                                          widget.dropdown!.data![index]!.title!,
                                          style: TextStyle(
                                            color: selectedTypeParam ==
                                                    widget.dropdown!
                                                        .data![index]!.parameter
                                                ? Colors.white
                                                : Color(0xFF54586C),
                                            // fontSize: 12,
                                            // fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                        ),

                        //button next
                        // SizedBox(height: 50),
                        Spacer(),
                        Container(
                          width: double.infinity,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Color(0xFFEF0056),
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(15), // <-- Radius
                              ),
                            ),
                            onPressed: () async {
                              await context
                                  .read<InputMeasurementCubit>()
                                  .getInputMeasurement(selectedTypeParam!);
                              _pageController.nextPage(
                                duration: Duration(milliseconds: 500),
                                curve: Curves.ease,
                              );
                            },
                            child: Text(
                              'ekyc_next'.tr(),
                              // style: TextStyle(fontSize: 12),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  //second page
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Column(
                      children: [
                        Image.network(
                          widget.dropdown!.data![indexSelectedType!]!.logo!,
                          width: 60,
                          height: 60,
                        ),
                        SizedBox(height: 10),
                        Text(
                          widget.dropdown!.data![indexSelectedType!]!.title!,
                        ),
                        SizedBox(height: 10),
                        Text(
                          'add_data_type'.tr(),
                          style: TextStyle(
                            color: Color(0xFFAFB2C3),
                            // fontSize: 12,
                          ),
                        ),
                        SizedBox(height: 30),

                        //user choose data type
                        Container(
                            width: width * 0.7,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'time'.tr(),
                                  style: TextStyle(
                                    color: Color(0xFFC8C7DD),
                                    fontWeight: FontWeight.bold,
                                    // fontSize: 18,
                                  ),
                                ),
                                SizedBox(height: 10),
                                GestureDetector(
                                  onTap: () async {
                                    TimeOfDay pickedTime = await showTimePicker(
                                          initialTime: TimeOfDay.now(),
                                          context: context,
                                        ) ??
                                        TimeOfDay.now();

                                    print(pickedTime
                                        .format(context)); //output 10:51 PM
                                    DateTime parsedTime = DateFormat.jm().parse(
                                        pickedTime.format(context).toString());
                                    //converting to DateTime so that we can further format on different pattern.

                                    print(
                                        parsedTime); //output 1970-01-01 22:53:00.000

                                    setState(() {
                                      timeinput =
                                          parsedTime; //set the value of text field.
                                    });
                                  },
                                  child: Container(
                                    width: double.infinity,
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Color(0xFF767674),
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Text(
                                      formatDate(
                                          timeinput!, [hh, ':', nn, ' ', am]),
                                      style:
                                          TextStyle(color: Color(0xFF54586C)),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 15),
                                Text(
                                  'date'.tr(),
                                  style: TextStyle(
                                    color: Color(0xFFC8C7DD),
                                    fontWeight: FontWeight.bold,
                                    // fontSize: 18,
                                  ),
                                ),
                                SizedBox(height: 10),
                                GestureDetector(
                                  onTap: () async {
                                    DateTime pickedDate = await showDatePicker(
                                            context: context,
                                            initialDate: DateTime.now(),
                                            firstDate: DateTime(
                                                2000), //DateTime.now() - not to allow to choose before today.
                                            lastDate: DateTime(2101)) ??
                                        DateTime.now();

                                    //you can implement different kind of Date Format here according to your requirement

                                    setState(() {
                                      dateinput =
                                          pickedDate; //set output date to TextField value.
                                    });
                                  },
                                  child: Container(
                                    width: double.infinity,
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Color(0xFF767674),
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Text(
                                      formatDate(
                                          dateinput!, [dd, ' ', MM, ' ', yyyy]),
                                      style:
                                          TextStyle(color: Color(0xFF54586C)),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 15),
                                BlocBuilder<InputMeasurementCubit,
                                    InputMeasurementState>(
                                  builder: (context, state) {
                                    if (state is InputMeasurementLoaded) {
                                      return InputMeasurementLoadedWidget(
                                          index: indexSelectedType!,
                                          selectedType: selectedType!,
                                          selectedTypeParam: selectedTypeParam!,
                                          input: state.input,
                                          submitData:
                                              (List<TextEditingController>
                                                      record,
                                                  List<String> input) {
                                            setState(() {
                                              inputRecord = record;
                                              inputParam = input;
                                            });
                                          });
                                    } else {
                                      return Container();
                                    }
                                  },
                                ),
                              ],
                            )),
                        Spacer(),
                        Container(
                          width: double.infinity,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: selectedType != null &&
                                        dateinput != null &&
                                        timeinput != null &&
                                        inputRecord.isNotEmpty &&
                                        inputRecord.every(
                                            (element) => element.text != '')
                                    ? Color(0xFFEF0056)
                                    : Colors.grey,
                                shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.circular(15), // <-- Radius
                                ),
                              ),
                              onPressed: () {
                                // _pageController.nextPage(
                                //   duration: Duration(milliseconds: 500),
                                //   curve: Curves.ease,
                                // );

                                print(
                                    '$selectedType $dateinput $timeinput $inputRecord');

                                if (selectedType != null &&
                                    dateinput != null &&
                                    timeinput != null &&
                                    inputRecord.isNotEmpty &&
                                    inputRecord.every(
                                        (element) => element.text != '')) {
                                  //handle blood pressure input
                                  if (inputRecord.length == 1) {
                                    context
                                        .read<ReturnAnswerCubit>()
                                        .submitRecord(
                                          inputParam,
                                          inputRecord,
                                          dateinput!,
                                          timeinput!,
                                        );
                                  } else {
                                    if (double.parse(inputRecord[0].text) >
                                        double.parse(inputRecord[1].text)) {
                                      context
                                          .read<ReturnAnswerCubit>()
                                          .submitRecord(
                                            inputParam,
                                            inputRecord,
                                            dateinput!,
                                            timeinput!,
                                          );
                                    } else {
                                      Fluttertoast.showToast(
                                        msg: 'bp_values_error'.tr(),
                                        toastLength: Toast.LENGTH_LONG,
                                      );
                                    }
                                  }
                                } else {
                                  Fluttertoast.showToast(
                                    msg: 'data_submitted'.tr(),
                                    toastLength: Toast.LENGTH_LONG,
                                  );
                                  // if (widget.from == 'Floating Button')
                                  // Navigator.of(context).pop();
                                }
                              },
                              child: BlocConsumer<ReturnAnswerCubit,
                                  ReturnAnswerState>(
                                listener: (context, state) {
                                  print('state is $state');
                                  if (state is ReturnAnswerSent) {
                                    // Navigator.of(context).pop();
                                    Fluttertoast.showToast(
                                      msg: 'data_submitted'.tr(),
                                      toastLength: Toast.LENGTH_LONG,
                                    );
                                    Navigator.of(context).pop();
                                    // if (widget.from == 'Floating Button')
                                    // Navigator.of(context).pop();
                                  }

                                  if (state is ReturnAnswerFail) {
                                    Fluttertoast.showToast(
                                      msg: 'submit_again'.tr(),
                                      toastLength: Toast.LENGTH_LONG,
                                    );
                                    // Navigator.of(context).pop();
                                  }
                                },
                                builder: (context, state) {
                                  if (state is ReturnAnswerLoading) {
                                    return SpinKitCircle(
                                      size: 14,
                                      color: Colors.white,
                                    );
                                  } else {
                                    return Text(
                                      'save'.tr(),
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15,
                                      ),
                                    );
                                  }
                                },
                              )),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

typedef SubmitDataCallBack = void Function(
    List<TextEditingController> inputRecord, List<String> inputParam);

class InputMeasurementLoadedWidget extends StatefulWidget {
  final int? index;
  final String? selectedType;
  final String? selectedTypeParam;
  final InputAddMeasurement? input;
  final SubmitDataCallBack? submitData;

  const InputMeasurementLoadedWidget(
      {super.key,
      this.index,
      this.selectedType,
      this.selectedTypeParam,
      this.input,
      this.submitData});

  @override
  State<InputMeasurementLoadedWidget> createState() =>
      _InputMeasurementLoadedWidgetState();
}

class _InputMeasurementLoadedWidgetState
    extends State<InputMeasurementLoadedWidget> {
  List<TextEditingController> inputRecord = [];
  List<String> inputParam = [];
  @override
  void initState() {
    super.initState();
    for (var i = 0; i < widget.input!.data!.length; i++) {
      inputRecord.add(TextEditingController());
      inputRecord[i].text = '';
      inputParam.add(widget.input!.data![i]!.parameter!);
      print(inputParam[i]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '${widget.selectedType} (${widget.input!.data![0]!.unit})',
          // '${widget.selectedType} Measurement',
          style: TextStyle(
            color: Color(0xFFC8C7DD),
            fontWeight: FontWeight.bold,
            // fontSize: 18,
          ),
        ),
        SizedBox(height: 10),
        ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          // scrollDirection: Axis.horizontal,
          itemCount: widget.input!.data!.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        // width: double.infinity,
                        padding: EdgeInsets.symmetric(
                          horizontal: 10,
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Color(0xFF767674),
                          ),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: TextFormField(
                          onChanged: (value) {
                            widget.submitData!(inputRecord, inputParam);
                          },
                          controller: inputRecord[index],
                          decoration: InputDecoration(
                            hintText: widget.input!.data![index]!.hint,
                            hintStyle: TextStyle(
                              color: Color(0xFFC8C7DD),
                              fontSize: 14,
                            ),
                            border: InputBorder.none,
                            isDense: true,
                          ),
                          // style: TextStyle(
                          //   fontWeight: FontWeight.bold,
                          //   fontSize: 13,
                          // ),
                          // validator: checkValidator,
                          keyboardType:
                              TextInputType.numberWithOptions(decimal: true),
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                              RegExp('[0-9.,]'),
                            ),
                          ],
                        ),
                      ),
                    ),
                    // SizedBox(width: 10),
                    // Text(
                    //   '${widget.input.data[widget.input.data.indexWhere((element) => element.parameter.contains(widget.selectedType))].unit}',
                    //   style: TextStyle(
                    //     // fontWeight: FontWeight.bold,
                    //     color: Color(0xFFC8C7DD),
                    //   ),
                    // )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            );
          },
        ),
      ],
    );
  }
}
