import 'package:date_format/date_format.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';

class GraphWeekWidget extends StatefulWidget {
  final List<WeeksData> weeksData;
  final List<WeeksData>? weeksData2;

  const GraphWeekWidget({Key? key, required this.weeksData, this.weeksData2})
      : super(key: key);

  @override
  _GraphWeekWidgetState createState() => _GraphWeekWidgetState();
}

class _GraphWeekWidgetState extends State<GraphWeekWidget> {
  int currentPage = 0;
  PageController controller = PageController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      child: PageView.builder(
        reverse: true,
        onPageChanged: (value) {
          setState(() {
            currentPage = value;
          });
        },
        controller: controller,
        itemCount: widget.weeksData.length,
        itemBuilder: (context, index) {
          //1st graph
          List<FlSpot> spots = [];
          List<GraphData> graphData =
              widget.weeksData[index].graphList!.graphData!;
          double lowest = 0;
          double biggest = 0;
          if (!graphData.every((element) => element.record == null)) {
            for (int i = 0; i < graphData.length; i++) {
              print(
                  'data is day ${graphData[i].day} record ${graphData[i].record}');
              // if (graphData[i].record !=
              //         null &&
              //     graphData[i].record >
              //         biggest) {
              //   biggest = graphData[i].record;
              // }

              if (graphData[i].record != null) {
                spots.add(
                  FlSpot(graphData[i].day!.toDouble(), graphData[i].record!),
                );
              } else {
                print('masuk sini sebab null');
                //find average between two points

                //left side
                int indexLeft = i;
                for (var m = i; m >= 0; m--) {
                  if (graphData[m].record != null) {
                    indexLeft = m;
                    break;
                  }
                }

                //right side
                int indexRight = i;
                for (var m = i; m < graphData.length; m++) {
                  if (graphData[m].record != null) {
                    indexRight = m;
                    break;
                  }
                }

                //get average of those two points
                if (graphData[i].date!.isBefore(DateTime.now())) {
                  if (indexRight != i && indexLeft != i) {
                    graphData[i].record = double.parse(
                        ((graphData[indexLeft].record! +
                                    graphData[indexRight].record!) /
                                2)
                            .toStringAsFixed(2));
                  } else if (indexRight == i && indexLeft != i) {
                    graphData[i].record = graphData[indexLeft].record;
                  } else if (indexLeft == i && indexRight != i) {
                    graphData[i].record = graphData[indexRight].record;
                  }

                  spots.add(
                    FlSpot(graphData[i].day!.toDouble(), graphData[i].record!),
                  );
                } else {
                  print('masuk sini ke?');
                  // graphData[i].record = null;

                  spots.add(FlSpot.nullSpot);
                }
              }
            }

            GraphData biggestSpot =
                graphData.firstWhere((element) => element.record != null);

            for (var i = 0; i < graphData.length; i++) {
              if (graphData[i].record != null) {
                if (graphData[i].record! > biggestSpot.record!) {
                  biggestSpot = graphData[i];
                }
              }
            }

            biggest = biggestSpot.record!;
            print('biggest is $biggest');

            GraphData lowestSpot =
                graphData.firstWhere((element) => element.record != null);

            for (var i = 0; i < graphData.length; i++) {
              if (graphData[i].record != null) {
                if (graphData[i].record! < lowestSpot.record!) {
                  lowestSpot = graphData[i];
                }
              }
            }

            lowest = lowestSpot.record!;
            print('lowest is $lowest');

            //2nd graph

            List<FlSpot> spots2 = [];

            if (widget.weeksData2 != null) {
              List<GraphData> graphData2 =
                  widget.weeksData2![index].graphList!.graphData!;

              for (int i = 0; i < graphData2.length; i++) {
                print(
                    'data is day ${graphData2[i].day} record ${graphData2[i].record}');
                // if (graphData[i].record !=
                //         null &&
                //     graphData[i].record >
                //         biggest) {
                //   biggest = graphData[i].record;
                // }

                if (graphData2[i].record != null) {
                  spots2.add(
                    FlSpot(
                        graphData2[i].day!.toDouble(), graphData2[i].record!),
                  );
                } else {
                  print('masuk sini sebab null');
                  //find average between two points

                  //left side
                  int indexLeft = i;
                  for (var m = i; m >= 0; m--) {
                    if (graphData2[m].record != null) {
                      indexLeft = m;
                      break;
                    }
                  }

                  //right side
                  int indexRight = i;
                  for (var m = i; m < graphData2.length; m++) {
                    if (graphData2[m].record != null) {
                      indexRight = m;
                      break;
                    }
                  }

                  //get average of those two points
                  if (graphData2[i].date!.isBefore(DateTime.now())) {
                    if (indexRight != i && indexLeft != i) {
                      graphData2[i].record = double.parse(
                          ((graphData2[indexLeft].record! +
                                      graphData2[indexRight].record!) /
                                  2)
                              .toStringAsFixed(2));
                    } else if (indexRight == i && indexLeft != i) {
                      graphData2[i].record = graphData2[indexLeft].record;
                    } else if (indexLeft == i && indexRight != i) {
                      graphData2[i].record = graphData2[indexRight].record;
                    }

                    spots2.add(
                      FlSpot(
                          graphData2[i].day!.toDouble(), graphData2[i].record!),
                    );
                  } else {
                    print('taylor cantik');

                    spots2.add(FlSpot.nullSpot);
                  }
                }
              }

              GraphData biggestSpot2 =
                  graphData2.firstWhere((element) => element.record != null);

              for (var i = 0; i < graphData2.length; i++) {
                if (graphData2[i].record != null) {
                  if (graphData2[i].record! > biggestSpot.record!) {
                    biggestSpot = graphData2[i];
                  }
                }
              }

              double biggest2 = biggestSpot2.record!;

              if (biggest2 > biggest) {
                biggest = biggest2;
              }
              print('biggest is $biggest');

              GraphData lowestSpot2 =
                  graphData2.firstWhere((element) => element.record != null);

              for (var i = 0; i < graphData2.length; i++) {
                if (graphData2[i].record != null) {
                  if (graphData2[i].record! < lowestSpot.record!) {
                    lowestSpot = graphData2[i];
                  }
                }
              }

              double lowest2 = lowestSpot2.record!;

              if (lowest2 < lowest) {
                lowest = lowest2;
              }
              print('lowest is $lowest');
            }

            return Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                      onPressed: () {
                        controller.animateToPage(currentPage + 1,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);
                      },
                      child: Icon(Icons.navigate_before),
                    ),
                    Text('${formatDate(widget.weeksData[index].week!.day(0), [
                          dd,
                          ' ',
                          MM
                        ])} - ${formatDate(widget.weeksData[index].week!.day(6), [
                          dd,
                          ' ',
                          MM
                        ])}'),
                    TextButton(
                      onPressed: () {
                        controller.animateToPage(currentPage - 1,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);
                      },
                      child: Icon(Icons.navigate_next),
                    ),
                  ],
                ),
                Text('${widget.weeksData[index].week!.year}'),
                SizedBox(height: 10),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: LineChart(
                      LineChartData(
                        borderData: FlBorderData(
                          show: true,
                          border: Border(
                            bottom: BorderSide(
                              color: Colors.grey,
                              width: 1,
                            ),
                            left: BorderSide(
                              color: Colors.transparent,
                            ),
                            right: BorderSide(
                              color: Colors.transparent,
                            ),
                            top: BorderSide(
                              color: Colors.transparent,
                              // width: 1,
                            ),
                          ),
                        ),
                        lineBarsData: [
                          LineChartBarData(
                            preventCurveOverShooting: true,
                            spots: widget.weeksData[index].graphList!.graphData!
                                .map(
                                  (e) => e.record != null
                                      ? FlSpot(e.day!.toDouble(), e.record!)
                                      : FlSpot.nullSpot,
                                )
                                .toList(),
                            isCurved: true,
                            color: Color(0xFF3586FF),
                            belowBarData: BarAreaData(
                                // gradientColorStops: [0.],
                                // show: true,
                                // colors: [
                                //   Colors.blue.withOpacity(0.1),
                                //   Colors.white,
                                // ],
                                // gradientFrom: Offset(0.5, 0),
                                // gradientTo: Offset(0.5, 1.8),
                                ),
                            barWidth: 2.5,
                            isStrokeCapRound: true,
                            // gradientFrom: Offset(0, 5),
                            dotData: FlDotData(
                              getDotPainter: (
                                FlSpot spot,
                                double percent,
                                LineChartBarData barData,
                                int index,
                              ) {
                                return FlDotCirclePainter(
                                  radius: 2,
                                  color: Color(0xFF3586FF),
                                  strokeColor: Color(0xFF3586FF),
                                  strokeWidth: 1,
                                );
                              },
                              show: true,
                              // checkToShowDot: (spot, barData) {
                              //   FlSpot spotNew = FlSpot(
                              //     spot.x,
                              //     spots[spots.indexWhere((element) =>
                              //             element.x == spot.x)]
                              //         .y,
                              //   );

                              //   if (spotNew.y != null) {
                              //     return true;
                              //   } else {
                              //     return false;
                              //   }
                              // },
                            ),
                          ),
                          if (spots2.isNotEmpty)
                            LineChartBarData(
                              preventCurveOverShooting: true,
                              spots: widget
                                  .weeksData2![index].graphList!.graphData!
                                  .map((e) => e.record != null
                                      ? FlSpot(e.day!.toDouble(), e.record!)
                                      : FlSpot.nullSpot)
                                  .toList(),

                              isCurved: true,
                              color: Color(0xFFEF0056),
                              belowBarData: BarAreaData(
                                  // gradientColorStops: [0.],
                                  // show: true,
                                  // colors: [
                                  //   Colors.red.withOpacity(0.1),
                                  //   Colors.white,
                                  // ],
                                  // gradientFrom: Offset(0.5, 1),
                                  // gradientTo: Offset(0.5, 1.8),
                                  ),
                              barWidth: 2.5,
                              isStrokeCapRound: true,
                              // gradientFrom: Offset(0, 5),
                              dotData: FlDotData(
                                getDotPainter: (
                                  FlSpot spot,
                                  double percent,
                                  LineChartBarData barData,
                                  int index,
                                ) {
                                  return FlDotCirclePainter(
                                    radius: 2,
                                    color: Color(0xFFEF0056),
                                    strokeColor: Color(0xFFEF0056),
                                    strokeWidth: 1,
                                  );
                                },
                                show: true,
                                // checkToShowDot: (spot, barData) {
                                //   FlSpot spotNew = FlSpot(
                                //     spot.x,
                                //     spots[spots.indexWhere((element) =>
                                //             element.x == spot.x)]
                                //         .y,
                                //   );

                                //   if (spotNew.y != null) {
                                //     return true;
                                //   } else {
                                //     return false;
                                //   }
                                // },
                              ),
                            ),
                        ],
                        minX: 1,
                        maxX: 7,
                        minY:
                            ((lowest.ceil() / 10).ceil() * 10).toDouble() - 10,
                        maxY:
                            ((biggest.ceil() / 10).ceil() * 10).toDouble() + 10,
                        titlesData: FlTitlesData(
                          leftTitles: AxisTitles(
                            axisNameWidget: Text(' '),
                            sideTitles: SideTitles(
                              interval: 10,
                            ),
                          ),
                          bottomTitles: AxisTitles(
                            sideTitles: SideTitles(
                              // reservedSize: 30,
                              // interval: 5,
                              showTitles: true,
                              getTitlesWidget: (value, meta) {
                                if (value == 1) {
                                  return Text('Mon');
                                } else if (value == 2) {
                                  return Text('Tue');
                                } else if (value == 3) {
                                  return Text('Wed');
                                } else if (value == 4) {
                                  return Text('Thu');
                                } else if (value == 5) {
                                  return Text('Fri');
                                } else if (value == 6) {
                                  return Text('Sat');
                                } else if (value == 7) {
                                  return Text('Sun');
                                } else {
                                  return Container();
                                }
                              },
                            ),
                          ),
                          topTitles: AxisTitles(
                            sideTitles: SideTitles(showTitles: false),
                          ),
                          rightTitles: AxisTitles(
                            sideTitles: SideTitles(showTitles: false),
                          ),
                        ),
                        gridData: FlGridData(
                          show: true,
                          drawHorizontalLine: true,
                          checkToShowHorizontalLine: (value) {
                            if (value % 10 == 0) {
                              return true;
                            } else {
                              return false;
                            }
                          },
                        ),

                        // axisTitleData: FlAxisTitleData(
                        //   show: true,
                        //   bottomTitle: AxisTitle(
                        //     margin: 0,
                        //     showTitle: true,
                        //     titleText: 'trend_day'.tr(),
                        //     textStyle: TextStyle(
                        //       color: Colors.black,
                        //       fontWeight: FontWeight.bold,
                        //       fontSize: 14,
                        //     ),
                        //   ),
                        // ),
                      ),
                    ),
                  ),
                ),
                // SfCartesianChart(
                //   legend: Legend(
                //     isVisible: true,
                //     position: LegendPosition.bottom,
                //     // Border color and border width of legend
                //     // borderColor: Colors.black,
                //     // borderWidth: 2,
                //   ),
                //   tooltipBehavior: TooltipBehavior(enable: true),
                //   // Initialize category axis
                //   primaryXAxis: CategoryAxis(
                //     title: AxisTitle(text: 'Day'),
                //   ),
                //   primaryYAxis: NumericAxis(
                //     title: AxisTitle(text: widget.unit),
                //   ),
                //   series: <LineSeries<GraphData, String>>[
                //     LineSeries<GraphData, String>(
                //       name: widget.title == 'Blood Pressure'
                //           ? 'Systolic'
                //           : widget.title,
                //       markerSettings:
                //           MarkerSettings(isVisible: true),
                //       width: 2.5,
                //       // Bind data source
                //       sortingOrder: SortingOrder.ascending,
                //       dataSource: widget
                //           .weeksData[index].graphList.graphData,
                //       xValueMapper: (GraphData data, _) => data.day,
                //       yValueMapper: (GraphData data, _) =>
                //           data.record,
                //       emptyPointSettings: EmptyPointSettings(
                //           mode: EmptyPointMode.drop),
                //     ),
                //     if (widget.monthData2 != null)
                //       LineSeries<GraphData, String>(
                //         name: 'Diastolic',
                //         markerSettings:
                //             MarkerSettings(isVisible: true),
                //         width: 2.5,
                //         // Bind data source
                //         sortingOrder: SortingOrder.ascending,
                //         dataSource: widget
                //             .weeksData2[index].graphList.graphData,
                //         xValueMapper: (GraphData data, _) =>
                //             data.day,
                //         yValueMapper: (GraphData data, _) =>
                //             data.record,
                //         emptyPointSettings: EmptyPointSettings(
                //             mode: EmptyPointMode.drop),
                //       ),
                //   ],
                // ),
              ],
            );
          } else {
            return Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                      onPressed: () {
                        controller.animateToPage(currentPage + 1,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);
                      },
                      child: Icon(Icons.navigate_before),
                    ),
                    Text('${formatDate(widget.weeksData[index].week!.day(0), [
                          dd,
                          ' ',
                          MM
                        ])} - ${formatDate(widget.weeksData[index].week!.day(6), [
                          dd,
                          ' ',
                          MM
                        ])}'),
                    TextButton(
                      onPressed: () {
                        controller.animateToPage(currentPage - 1,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);
                      },
                      child: Icon(Icons.navigate_next),
                    ),
                  ],
                ),
                Text('${widget.weeksData[index].week!.year}'),
                SizedBox(height: 10),
                Center(
                    child: Text(
                  'no_data'.tr(),
                  style: TextStyle(
                      color: Color(0xFFC8C7DD),
                      fontWeight: FontWeight.bold,
                      fontSize: 12),
                ))
              ],
            );
          }
        },
      ),
    );
  }
}
