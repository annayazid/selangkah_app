import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/utils/constants.dart';

class RecordTrend extends StatefulWidget {
  final String? parameter;
  final String? title;
  final String? unit;
  final String? image;
  final int? color;

  const RecordTrend(
      {super.key,
      this.parameter,
      this.title,
      this.unit,
      this.image,
      this.color});

  @override
  State<RecordTrend> createState() => _RecordTrendState();
}

class _RecordTrendState extends State<RecordTrend> {
  @override
  void initState() {
    super.initState();
    context.read<RecordTrendCubit>().getRecordTrend(widget.parameter!);

    print(mostRecentMonday(DateTime.now()));
  }

  DateTime mostRecentMonday(DateTime date) =>
      DateTime(date.year, date.month, date.day - (date.weekday - 1));

  @override
  Widget build(BuildContext context) {
    return RecordTrendScaffold(
      appBarTitle: 'lifetime'.tr(),
      title: widget.title!,
      parameter: widget.parameter!,
      child: BlocBuilder<RecordTrendCubit, RecordTrendState>(
        builder: (context, state) {
          print('state is $state');
          if (state is RecordTrendLoaded) {
            print('here masuk loaded');
            return RecordTrendWidgetLoaded(
              parameter: widget.parameter,
              title: widget.title,
              unit: widget.unit,
              image: widget.image,
              color: widget.color,
              recordTrendData: state.recordTrendData,
              weeksData: state.weeksData,
              weeksData2: state.weeksData2,
              dayData: state.dayData,
              dayData2: state.dayData2,
            );
          } else {
            return SpinKitFadingCircle(
              color: kPrimaryColor,
              size: 20,
            );
          }
        },
      ),
    );
  }
}

class RecordTrendWidgetLoaded extends StatefulWidget {
  final String? parameter;
  final String? title;
  final String? unit;
  final String? image;
  final int? color;
  final RecordTrendData? recordTrendData;

  // const RecordTrendWidgetLoaded(
  //     {super.key,
  //     this.parameter,
  //     this.title,
  //     this.unit,
  //     this.image,
  //     this.color,
  //     this.recordTrendData});

  final List<WeeksData>? weeksData;
  final List<WeeksData>? weeksData2;
  final List<DayData>? dayData;
  final List<DayData>? dayData2;

  const RecordTrendWidgetLoaded(
      {Key? key,
      this.parameter,
      this.title,
      this.unit,
      this.image,
      this.color,
      this.recordTrendData,
      this.weeksData,
      this.weeksData2,
      this.dayData,
      this.dayData2})
      : super(key: key);

  @override
  _RecordTrendWidgetLoadedState createState() =>
      _RecordTrendWidgetLoadedState();
}

class _RecordTrendWidgetLoadedState extends State<RecordTrendWidgetLoaded> {
  // TooltipBehavior _tooltipBehavior;
  PageController controller = PageController();
  List<String> duration = <String>['week'.tr(), 'month'.tr()];
  String selectedDuration = 'week'.tr();
  String selectedGraph = 'week';
  int currentPage = 0;

  @override
  void initState() {
    // widget.dayData.forEach((element) {
    //   scrollControllerList.add(ScrollController());
    // });
    // selectedDuration = duration.first;

    super.initState();
  }

  List<ScrollController> scrollControllerList = [];

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      child: Column(
        children: [
          // SizedBox(height: 15),
          Container(
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    // Container(
                    //   padding: EdgeInsets.all(8),
                    //   decoration: BoxDecoration(
                    //       color: Color(widget.color), shape: BoxShape.circle),
                    //   child: Image.network(widget.image),
                    //   width: 45,
                    //   height: 45,
                    // ),
                    Expanded(
                      child: Row(
                        children: [
                          Text(
                            widget.title!,
                            style: TextStyle(
                              color: Color(0xFF54586C),
                              // fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(width: 5),
                          Text(
                            widget.unit!,
                            style: TextStyle(
                              color: Color(0xFFC8C7DD),
                              fontWeight: FontWeight.bold,
                              fontSize: 11,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Color(0xFF767674),
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          value: selectedDuration,
                          icon: Icon(
                            Icons.keyboard_arrow_down,
                            size: 30,
                            color: Color(0xFF54586C),
                          ),
                          elevation: 16,
                          style: const TextStyle(color: Color(0xFF54586C)),
                          underline: Container(
                            height: 2,
                            color: Color(0xFF979BAC),
                          ),
                          onChanged: (String? value) {
                            // This is called when the user selects an item.
                            setState(() {
                              selectedDuration = value!;
                            });
                          },
                          items: duration
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    )
                  ],
                ),

                SizedBox(height: 15),

                // //chart here week
                if (selectedDuration == 'week'.tr())
                  GraphWeekWidget(
                    weeksData: widget.weeksData!,
                    weeksData2: widget.weeksData2,
                  ),

                //chart here month
                if (selectedDuration == 'month'.tr())
                  GraphMonthWidget(
                    dayData: widget.dayData!,
                    dayData2: widget.dayData2,
                  ),

                if (widget.recordTrendData!.data!.length > 0) ...[
                  Container(
                    width: width * 0.85,
                    margin: EdgeInsets.all(10),
                    child: widget.recordTrendData!.data![0]!.statusWarning ==
                                '1' ||
                            widget.recordTrendData!.data![0]!.statusWarning ==
                                '2'
                        ? Column(
                            children: [
                              SizedBox(height: 20),
                              Container(
                                padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    // width: 5,
                                    color: widget.recordTrendData!.data![0]!
                                                .statusWarning ==
                                            '1'
                                        ? Color(0xFFF4AD08)
                                        : Color(0xFFFF4588),
                                  ),
                                  borderRadius: BorderRadius.circular(12),
                                  color: widget.recordTrendData!.data![0]!
                                              .statusWarning ==
                                          '1'
                                      ? Color(0xFFFDFAE9)
                                      : Color(0xFFFFEAF1),
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Image.asset(
                                          widget.recordTrendData!.data![0]!
                                                      .statusWarning ==
                                                  '1'
                                              ? "assets/images/lhr/alert.png"
                                              : "assets/images/lhr/exclamation.png",
                                          width: 35,
                                          height: 35),
                                    ),
                                    SizedBox(width: 15),
                                    Expanded(
                                        flex: 3,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              widget.recordTrendData!.data![0]!
                                                  .statusDescription!,
                                              style: TextStyle(
                                                color: widget
                                                            .recordTrendData!
                                                            .data![0]!
                                                            .statusWarning ==
                                                        '1'
                                                    ? Color(0xFFED8000)
                                                    : Color(0xFFEF0056),
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            SizedBox(height: 5),
                                            Text(
                                              widget.recordTrendData!.data![0]!
                                                  .msg!,
                                              style: TextStyle(
                                                color: widget
                                                            .recordTrendData!
                                                            .data![0]!
                                                            .statusWarning ==
                                                        '1'
                                                    ? Color(0xFFED8000)
                                                    : Color(0xFFEF0056),
                                              ),
                                            ),
                                          ],
                                        )),
                                  ],
                                ),
                              ),
                            ],
                          )
                        : Container(),
                  ),
                  // SizedBox(height: 20),
                ],

                //description
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    // color: Color(0xFFEDEAE7),
                  ),
                  child: Text(
                    // '--- Description ---',
                    widget.recordTrendData!.description!,
                    style: TextStyle(color: Color(0xFFAFB2C3)),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 30),
                Center(
                  child: Text(
                    'Data',
                    style: TextStyle(
                        color: Color(0xFF54586C), fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(height: 20),

                if (widget.recordTrendData!.data!.length > 0) ...[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    child: Row(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          flex: 2,
                          child: widget.title!.contains('BMI')
                              ? Text(
                                  widget.title!,
                                  style: TextStyle(
                                      color: Color(0xFFC8C7DD),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12),
                                )
                              : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      widget.title!,
                                      style: TextStyle(
                                          color: Color(0xFFC8C7DD),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                    ),
                                    SizedBox(width: 5),
                                    Text(
                                      '(${widget.unit})',
                                      style: TextStyle(
                                          color: Color(0xFFC8C7DD),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                    ),
                                  ],
                                ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Text(
                            'date'.tr(),
                            style: TextStyle(
                                color: Color(0xFFC8C7DD),
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                  // SizedBox(height: 20),
                ],

                if (widget.recordTrendData!.data!.length > 0)
                  ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: widget.recordTrendData!.data!.length,
                      itemBuilder: (context, index) {
                        if (widget.title!.contains('BMI')) {
                          return Column(
                            children: [
                              Container(
                                // width: width * 0.95,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 15),
                                child: GestureDetector(
                                  onTap: () {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          scrollable: true,
                                          content: SingleChildScrollView(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              // mainAxisSize:
                                              //     MainAxisSize.min,
                                              children: [
                                                Text(
                                                  'readout'.tr(),
                                                  style: TextStyle(
                                                    color: Color(0xFF898DA1),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 11,
                                                  ),
                                                ),
                                                SizedBox(height: 15),
                                                Text(
                                                  '${widget.recordTrendData!.data![index]!.record} ${widget.recordTrendData!.data![index]!.unit}',
                                                  style: TextStyle(
                                                      color: Color(0xFF505660),
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 20),
                                                  // textAlign: TextAlign.left,
                                                ),
                                                SizedBox(height: 10),
                                                Text(
                                                  '${widget.recordTrendData!.data![index]!.time}, ${formatDate(widget.recordTrendData!.data![index]!.date!, [
                                                        dd,
                                                        ' ',
                                                        MM,
                                                        ' ',
                                                        yyyy
                                                      ])}',
                                                  style: TextStyle(
                                                      color: Color(0xFF54586C),
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 12),
                                                  // textAlign: TextAlign.left,
                                                ),
                                                SizedBox(height: 10),
                                                Divider(
                                                  color: Color(0xFFE3E5EE),
                                                  thickness: 2,
                                                ),
                                                SizedBox(height: 10),
                                                Text(
                                                  'Status',
                                                  style: TextStyle(
                                                    color: Color(0xFF898DA1),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 11,
                                                  ),
                                                ),
                                                SizedBox(height: 15),
                                                if (widget
                                                            .recordTrendData!
                                                            .data![index]!
                                                            .statusWarning ==
                                                        '1' ||
                                                    widget
                                                            .recordTrendData!
                                                            .data![index]!
                                                            .statusWarning ==
                                                        '2')
                                                  Row(
                                                    children: [
                                                      Image.asset(
                                                          widget
                                                                      .recordTrendData!
                                                                      .data![
                                                                          index]!
                                                                      .statusWarning ==
                                                                  '1'
                                                              ? "assets/images/lhr/alert.png"
                                                              : "assets/images/lhr/exclamation.png",
                                                          width: 15,
                                                          height: 15),
                                                      SizedBox(width: 15),
                                                      Text(
                                                        widget
                                                            .recordTrendData!
                                                            .data![index]!
                                                            .statusDescription!,
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Color(
                                                                0xFF505660)),
                                                      ),
                                                    ],
                                                  ),
                                                if (widget
                                                        .recordTrendData!
                                                        .data![index]!
                                                        .statusWarning ==
                                                    '0')
                                                  Text(
                                                    widget
                                                        .recordTrendData!
                                                        .data![index]!
                                                        .statusDescription!,
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color:
                                                            Color(0xFF505660)),
                                                  ),
                                                SizedBox(height: 10),
                                                Text(
                                                  widget.recordTrendData!
                                                      .data![index]!.msg!,
                                                  style: TextStyle(
                                                      color: Color(0xFF898DA1),
                                                      fontSize: 11),
                                                ),
                                                SizedBox(height: 15),
                                                Container(
                                                  width: double.infinity,
                                                  child: ElevatedButton(
                                                    style: ElevatedButton
                                                        .styleFrom(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          20)),
                                                      backgroundColor:
                                                          Color(0xFFEF0056),
                                                    ),
                                                    onPressed: () {
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                                    child: Text(
                                                      'receive_close_string'
                                                          .tr(),
                                                      style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  },
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: Row(
                                          children: [
                                            Text(
                                              widget.recordTrendData!
                                                  .data![index]!.record!,
                                              style: TextStyle(
                                                  color: widget
                                                              .recordTrendData!
                                                              .data![index]!
                                                              .statusWarning ==
                                                          '2'
                                                      ? Color(0xFFEF0056)
                                                      : Color(0xFF54586C),
                                                  fontWeight: widget
                                                              .recordTrendData!
                                                              .data![index]!
                                                              .statusWarning ==
                                                          '2'
                                                      ? FontWeight.bold
                                                      : FontWeight.normal),
                                            ),
                                            if (widget
                                                    .recordTrendData!
                                                    .data![index]!
                                                    .statusWarning ==
                                                '1')
                                              Expanded(
                                                child: Image.asset(
                                                    "assets/images/lhr/alert.png",
                                                    width: 15,
                                                    height: 15),
                                              ),
                                            if (widget
                                                    .recordTrendData!
                                                    .data![index]!
                                                    .statusWarning ==
                                                '2')
                                              Expanded(
                                                child: Image.asset(
                                                    "assets/images/lhr/exclamation.png",
                                                    width: 15,
                                                    height: 15),
                                              )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Text(
                                          '${formatDate(widget.recordTrendData!.data![index]!.date!, [
                                                dd,
                                                ' ',
                                                MM,
                                                ' ',
                                                yyyy
                                              ])}',
                                          style: TextStyle(
                                              color: Color(0xFF54586C)),
                                        ),
                                      ),
                                      Expanded(
                                        child: Align(
                                          alignment: Alignment.centerRight,
                                          child: PopupMenuButton(
                                            offset: Offset(0, 25),
                                            padding: EdgeInsets.all(10),
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                            itemBuilder: (context) => [
                                              PopupMenuItem(
                                                  value: 1,
                                                  child: Row(
                                                    children: [
                                                      Text('view'.tr()),
                                                      Spacer(),
                                                      Image.asset(
                                                          "assets/images/lhr/view.png",
                                                          width: 15,
                                                          height: 15)
                                                    ],
                                                  )),
                                            ],
                                            onSelected: (value) async {
                                              if (value == 1) {
                                                showDialog(
                                                  context: context,
                                                  builder: (context) {
                                                    return AlertDialog(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20),
                                                      ),
                                                      scrollable: true,
                                                      content:
                                                          SingleChildScrollView(
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          // mainAxisSize:
                                                          //     MainAxisSize.min,
                                                          children: [
                                                            Text(
                                                              'readout'.tr(),
                                                              style: TextStyle(
                                                                color: Color(
                                                                    0xFF898DA1),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 11,
                                                              ),
                                                            ),
                                                            SizedBox(
                                                                height: 15),
                                                            Text(
                                                              '${widget.recordTrendData!.data![index]!.record} ${widget.recordTrendData!.data![index]!.unit}',
                                                              style: TextStyle(
                                                                  color: Color(
                                                                      0xFF505660),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize: 20),
                                                              // textAlign: TextAlign.left,
                                                            ),
                                                            SizedBox(
                                                                height: 10),
                                                            Text(
                                                              '${widget.recordTrendData!.data![index]!.time}, ${formatDate(widget.recordTrendData!.data![index]!.date!, [
                                                                    dd,
                                                                    ' ',
                                                                    MM,
                                                                    ' ',
                                                                    yyyy
                                                                  ])}',
                                                              style: TextStyle(
                                                                  color: Color(
                                                                      0xFF54586C),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize: 12),
                                                              // textAlign: TextAlign.left,
                                                            ),
                                                            SizedBox(
                                                                height: 10),
                                                            Divider(
                                                              color: Color(
                                                                  0xFFE3E5EE),
                                                              thickness: 2,
                                                            ),
                                                            SizedBox(
                                                                height: 10),
                                                            Text(
                                                              'Status',
                                                              style: TextStyle(
                                                                color: Color(
                                                                    0xFF898DA1),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 11,
                                                              ),
                                                            ),
                                                            SizedBox(
                                                                height: 15),
                                                            if (widget
                                                                        .recordTrendData!
                                                                        .data![
                                                                            index]!
                                                                        .statusWarning ==
                                                                    '1' ||
                                                                widget
                                                                        .recordTrendData!
                                                                        .data![
                                                                            index]!
                                                                        .statusWarning ==
                                                                    '2')
                                                              Row(
                                                                children: [
                                                                  Image.asset(
                                                                      widget.recordTrendData!.data![index]!.statusWarning ==
                                                                              '1'
                                                                          ? "assets/images/lhr/alert.png"
                                                                          : "assets/images/lhr/exclamation.png",
                                                                      width: 15,
                                                                      height:
                                                                          15),
                                                                  SizedBox(
                                                                      width:
                                                                          15),
                                                                  Text(
                                                                    widget
                                                                        .recordTrendData!
                                                                        .data![
                                                                            index]!
                                                                        .statusDescription!,
                                                                    style: TextStyle(
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .bold,
                                                                        color: Color(
                                                                            0xFF505660)),
                                                                  ),
                                                                ],
                                                              ),
                                                            if (widget
                                                                    .recordTrendData!
                                                                    .data![
                                                                        index]!
                                                                    .statusWarning! ==
                                                                '0')
                                                              Text(
                                                                widget
                                                                    .recordTrendData!
                                                                    .data![
                                                                        index]!
                                                                    .statusDescription!,
                                                                style: TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    color: Color(
                                                                        0xFF505660)),
                                                              ),
                                                            SizedBox(
                                                                height: 10),
                                                            Text(
                                                              widget
                                                                  .recordTrendData!
                                                                  .data![index]!
                                                                  .msg!,
                                                              style: TextStyle(
                                                                  color: Color(
                                                                      0xFF898DA1),
                                                                  fontSize: 11),
                                                            ),
                                                            SizedBox(
                                                                height: 15),
                                                            Container(
                                                              width: double
                                                                  .infinity,
                                                              child:
                                                                  ElevatedButton(
                                                                style: ElevatedButton
                                                                    .styleFrom(
                                                                  shape: RoundedRectangleBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              20)),
                                                                  backgroundColor:
                                                                      Color(
                                                                          0xFFEF0056),
                                                                ),
                                                                onPressed: () {
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop();
                                                                },
                                                                child: Text(
                                                                  'receive_close_string'
                                                                      .tr(),
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                );
                                              }
                                            },
                                            child: Icon(
                                              // FontAwesomeIcons.archive,
                                              Icons.more_horiz,
                                              color: Color(0xFF505660),
                                              size: 30,
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Divider(
                                color: Color(0xFFF1F1F6),
                                thickness: 1.5,
                              )
                            ],
                          );
                        } else {
                          return Column(
                            children: [
                              Container(
                                // width: width * 0.95,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 15),
                                child: GestureDetector(
                                  onTap: () {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          scrollable: true,
                                          content: SingleChildScrollView(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              // mainAxisSize:
                                              //     MainAxisSize.min,
                                              children: [
                                                Text(
                                                  'readout'.tr(),
                                                  style: TextStyle(
                                                    color: Color(0xFF898DA1),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 11,
                                                  ),
                                                ),
                                                SizedBox(height: 15),
                                                Text(
                                                  '${widget.recordTrendData!.data![index]!.record} ${widget.recordTrendData!.data![index]!.unit}',
                                                  style: TextStyle(
                                                      color: Color(0xFF505660),
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 20),
                                                  // textAlign: TextAlign.left,
                                                ),
                                                SizedBox(height: 10),
                                                Text(
                                                  '${widget.recordTrendData!.data![index]!.time}, ${formatDate(widget.recordTrendData!.data![index]!.date!, [
                                                        dd,
                                                        ' ',
                                                        MM,
                                                        ' ',
                                                        yyyy
                                                      ])}',
                                                  style: TextStyle(
                                                      color: Color(0xFF54586C),
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 12),
                                                  // textAlign: TextAlign.left,
                                                ),
                                                SizedBox(height: 10),
                                                Divider(
                                                  color: Color(0xFFE3E5EE),
                                                  thickness: 2,
                                                ),
                                                SizedBox(height: 10),
                                                Text(
                                                  'Status',
                                                  style: TextStyle(
                                                    color: Color(0xFF898DA1),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 11,
                                                  ),
                                                ),
                                                SizedBox(height: 15),
                                                if (widget
                                                            .recordTrendData!
                                                            .data![index]!
                                                            .statusWarning ==
                                                        '1' ||
                                                    widget
                                                            .recordTrendData!
                                                            .data![index]!
                                                            .statusWarning ==
                                                        '2')
                                                  Row(
                                                    children: [
                                                      Image.asset(
                                                          widget
                                                                      .recordTrendData!
                                                                      .data![
                                                                          index]!
                                                                      .statusWarning ==
                                                                  '1'
                                                              ? "assets/images/lhr/alert.png"
                                                              : "assets/images/lhr/exclamation.png",
                                                          width: 15,
                                                          height: 15),
                                                      SizedBox(width: 15),
                                                      Text(
                                                        widget
                                                            .recordTrendData!
                                                            .data![index]!
                                                            .statusDescription!,
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Color(
                                                                0xFF505660)),
                                                      ),
                                                    ],
                                                  ),
                                                if (widget
                                                        .recordTrendData!
                                                        .data![index]!
                                                        .statusWarning ==
                                                    '0')
                                                  Text(
                                                    widget
                                                        .recordTrendData!
                                                        .data![index]!
                                                        .statusDescription!,
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color:
                                                            Color(0xFF505660)),
                                                  ),
                                                SizedBox(height: 10),
                                                Text(
                                                  widget.recordTrendData!
                                                      .data![index]!.msg!,
                                                  style: TextStyle(
                                                      color: Color(0xFF898DA1),
                                                      fontSize: 11),
                                                ),
                                                SizedBox(height: 15),
                                                Container(
                                                  width: double.infinity,
                                                  child: ElevatedButton(
                                                    style: ElevatedButton
                                                        .styleFrom(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          20)),
                                                      backgroundColor:
                                                          Color(0xFFEF0056),
                                                    ),
                                                    onPressed: () {
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                                    child: Text(
                                                      'receive_close_string'
                                                          .tr(),
                                                      style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  },
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: Row(
                                          children: [
                                            Text(
                                              widget.recordTrendData!
                                                  .data![index]!.record!,
                                              style: TextStyle(
                                                  color: widget
                                                              .recordTrendData!
                                                              .data![index]!
                                                              .statusWarning ==
                                                          '2'
                                                      ? Color(0xFFEF0056)
                                                      : Color(0xFF54586C),
                                                  fontWeight: widget
                                                              .recordTrendData!
                                                              .data![index]!
                                                              .statusWarning ==
                                                          '2'
                                                      ? FontWeight.bold
                                                      : FontWeight.normal),
                                            ),
                                            if (widget
                                                    .recordTrendData!
                                                    .data![index]!
                                                    .statusWarning ==
                                                '1')
                                              Expanded(
                                                child: Image.asset(
                                                    "assets/images/lhr/alert.png",
                                                    width: 15,
                                                    height: 15),
                                              ),
                                            if (widget
                                                    .recordTrendData!
                                                    .data![index]!
                                                    .statusWarning ==
                                                '2')
                                              Expanded(
                                                child: Image.asset(
                                                    "assets/images/lhr/exclamation.png",
                                                    width: 15,
                                                    height: 15),
                                              )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Text(
                                          '${formatDate(widget.recordTrendData!.data![index]!.date!, [
                                                dd,
                                                ' ',
                                                MM,
                                                ' ',
                                                yyyy
                                              ])}',
                                          style: TextStyle(
                                              color: Color(0xFF54586C)),
                                        ),
                                      ),
                                      Expanded(
                                        child: Align(
                                          alignment: Alignment.centerRight,
                                          child: PopupMenuButton(
                                            offset: Offset(0, 25),
                                            padding: EdgeInsets.all(10),
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                            itemBuilder: (context) => [
                                              PopupMenuItem(
                                                  value: 1,
                                                  child: Row(
                                                    children: [
                                                      Text('view'.tr()),
                                                      Spacer(),
                                                      Image.asset(
                                                          "assets/images/lhr/view.png",
                                                          width: 15,
                                                          height: 15)
                                                    ],
                                                  )),
                                              PopupMenuItem(
                                                  value: 2,
                                                  child: Row(
                                                    children: [
                                                      Text('delete'.tr()),
                                                      Spacer(),
                                                      Image.asset(
                                                          "assets/images/lhr/trash.png",
                                                          width: 15,
                                                          height: 15)
                                                    ],
                                                  )),
                                            ],
                                            onSelected: (value) async {
                                              if (value == 1) {
                                                showDialog(
                                                  context: context,
                                                  builder: (context) {
                                                    return AlertDialog(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20),
                                                      ),
                                                      scrollable: true,
                                                      content:
                                                          SingleChildScrollView(
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          // mainAxisSize:
                                                          //     MainAxisSize.min,
                                                          children: [
                                                            Text(
                                                              'readout'.tr(),
                                                              style: TextStyle(
                                                                color: Color(
                                                                    0xFF898DA1),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 11,
                                                              ),
                                                            ),
                                                            SizedBox(
                                                                height: 15),
                                                            Text(
                                                              '${widget.recordTrendData!.data![index]!.record} ${widget.recordTrendData!.data![index]!.unit!}',
                                                              style: TextStyle(
                                                                  color: Color(
                                                                      0xFF505660),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize: 20),
                                                              // textAlign: TextAlign.left,
                                                            ),
                                                            SizedBox(
                                                                height: 10),
                                                            Text(
                                                              '${widget.recordTrendData!.data![index]!.time}, ${formatDate(widget.recordTrendData!.data![index]!.date!, [
                                                                    dd,
                                                                    ' ',
                                                                    MM,
                                                                    ' ',
                                                                    yyyy
                                                                  ])}',
                                                              style: TextStyle(
                                                                  color: Color(
                                                                      0xFF54586C),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize: 12),
                                                              // textAlign: TextAlign.left,
                                                            ),
                                                            SizedBox(
                                                                height: 10),
                                                            Divider(
                                                              color: Color(
                                                                  0xFFE3E5EE),
                                                              thickness: 2,
                                                            ),
                                                            SizedBox(
                                                                height: 10),
                                                            Text(
                                                              'Status',
                                                              style: TextStyle(
                                                                color: Color(
                                                                    0xFF898DA1),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 11,
                                                              ),
                                                            ),
                                                            SizedBox(
                                                                height: 15),
                                                            if (widget
                                                                        .recordTrendData!
                                                                        .data![
                                                                            index]!
                                                                        .statusWarning ==
                                                                    '1' ||
                                                                widget
                                                                        .recordTrendData!
                                                                        .data![
                                                                            index]!
                                                                        .statusWarning ==
                                                                    '2')
                                                              Row(
                                                                children: [
                                                                  Image.asset(
                                                                      widget.recordTrendData!.data![index]!.statusWarning ==
                                                                              '1'
                                                                          ? "assets/images/lhr/alert.png"
                                                                          : "assets/images/lhr/exclamation.png",
                                                                      width: 15,
                                                                      height:
                                                                          15),
                                                                  SizedBox(
                                                                      width:
                                                                          15),
                                                                  Text(
                                                                    widget
                                                                        .recordTrendData!
                                                                        .data![
                                                                            index]!
                                                                        .statusDescription!,
                                                                    style: TextStyle(
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .bold,
                                                                        color: Color(
                                                                            0xFF505660)),
                                                                  ),
                                                                ],
                                                              ),
                                                            if (widget
                                                                    .recordTrendData!
                                                                    .data![
                                                                        index]!
                                                                    .statusWarning ==
                                                                '0')
                                                              Text(
                                                                widget
                                                                    .recordTrendData!
                                                                    .data![
                                                                        index]!
                                                                    .statusDescription!,
                                                                style: TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    color: Color(
                                                                        0xFF505660)),
                                                              ),
                                                            SizedBox(
                                                                height: 10),
                                                            Text(
                                                              widget
                                                                  .recordTrendData!
                                                                  .data![index]!
                                                                  .msg!,
                                                              style: TextStyle(
                                                                  color: Color(
                                                                      0xFF898DA1),
                                                                  fontSize: 11),
                                                            ),
                                                            SizedBox(
                                                                height: 15),
                                                            Container(
                                                              width: double
                                                                  .infinity,
                                                              child:
                                                                  ElevatedButton(
                                                                style: ElevatedButton
                                                                    .styleFrom(
                                                                  shape: RoundedRectangleBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              20)),
                                                                  backgroundColor:
                                                                      Color(
                                                                          0xFFEF0056),
                                                                ),
                                                                onPressed: () {
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop();
                                                                },
                                                                child: Text(
                                                                  'receive_close_string'
                                                                      .tr(),
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                );
                                              }
                                              if (value == 2) {
                                                showDialog(
                                                  context: context,
                                                  builder: (_) => BlocProvider(
                                                    create: (_) =>
                                                        DeleteRecordCubit(),
                                                    child: DeletAlert(
                                                      title: widget.parameter,
                                                      recordTrendData: widget
                                                          .recordTrendData,
                                                      index: index,
                                                    ),
                                                  ),
                                                ).then((value) {
                                                  if (value ?? false) {
                                                    context
                                                        .read<
                                                            RecordTrendCubit>()
                                                        .getRecordTrend(
                                                            widget.parameter!);
                                                  }
                                                });
                                              }
                                            },
                                            child: Icon(
                                              // FontAwesomeIcons.archive,
                                              Icons.more_horiz,
                                              color: Color(0xFF505660),
                                              size: 30,
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Divider(
                                color: Color(0xFFF1F1F6),
                                thickness: 1.5,
                              )
                            ],
                          );
                        }
                      }),
                if (widget.recordTrendData!.data!.length == 0)
                  Center(
                      child: Text(
                    'no_data'.tr(),
                    style: TextStyle(
                        color: Color(0xFFC8C7DD),
                        fontWeight: FontWeight.bold,
                        fontSize: 12),
                  ))
              ],
            ),
          ),
          SizedBox(height: widget.title!.contains('BMI') ? 15 : 60),
        ],
      ),
    );
  }
}

class DeletAlert extends StatelessWidget {
  final String? title;
  final RecordTrendData? recordTrendData;
  final int? index;

  const DeletAlert({super.key, this.title, this.recordTrendData, this.index});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      scrollable: true,
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisSize:
          //     MainAxisSize.min,
          children: [
            Center(
                child: Icon(
              FontAwesomeIcons.trashCan,
              color: Color(0xFF505660),
            )),
            SizedBox(height: 15),
            Center(
              child: Text(
                'delete_record'.tr(),
                style: TextStyle(
                  color: Color(0xFF505660),
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
            SizedBox(height: 15),
            Center(
              child: Text(
                'delete_record_desc'.tr(),
                style: TextStyle(
                  color: Color(0xFF898DA1),
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                    child: Text(
                      'cancel'.tr(),
                      style: TextStyle(
                        color: Color(0xFF505660),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 15),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: Color(0xFFEF0056),
                    ),
                    onPressed: () async {
                      await context.read<DeleteRecordCubit>().deleteRecord(
                            title!,
                            recordTrendData!.data![index!]!.date!,
                            recordTrendData!.data![index!]!.time!,
                          );
                      Navigator.of(context).pop(true);
                    },
                    child: BlocConsumer<DeleteRecordCubit, DeleteRecordState>(
                        listener: (context, state) {
                      print('State is delete $state');
                      if (state is DeleteRecordDeleted) {
                        // Navigator.of(context).pop();
                        Fluttertoast.showToast(
                          msg: 'record_deleted'.tr(),
                          toastLength: Toast.LENGTH_LONG,
                        );
                        // Navigator.of(context).pop();
                      }

                      if (state is SaringRecordFailed) {
                        showDialog(
                          context: context,
                          builder: (context) => CannotDelete(),
                        );

                        // Fluttertoast.showToast(
                        //   msg: 'Unable to delete record from Saring Program',
                        //   toastLength: Toast.LENGTH_LONG,
                        // );
                        // Navigator.of(context).pop();
                      }
                      if (state is DeleteRecordFailed) {
                        Fluttertoast.showToast(
                          msg: 'submit_again'.tr(),
                          toastLength: Toast.LENGTH_LONG,
                        );
                        // Navigator.of(context).pop();
                      }
                    }, builder: (context, state) {
                      if (state is DeleteRecordLoading) {
                        return SpinKitCircle(
                          size: 14,
                          color: Colors.white,
                        );
                      } else {
                        return Text(
                          'delete'.tr(),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        );
                      }
                    }),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class CannotDelete extends StatelessWidget {
  const CannotDelete({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      scrollable: true,
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisSize:
        //     MainAxisSize.min,
        children: [
          Center(
              child: Icon(
            FontAwesomeIcons.lock,
            color: Color(0xFF505660),
          )),
          SizedBox(height: 15),
          Center(
            child: Text(
              'cant_delete_file'.tr(),
              style: TextStyle(
                color: Color(0xFF505660),
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          ),
          SizedBox(height: 15),
          Center(
            child: Text(
              'cant_delete_file_desc'.tr(),
              style: TextStyle(
                color: Color(0xFF898DA1),
              ),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 15),
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                backgroundColor: Color(0xFFEF0056),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'Okay'.tr(),
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}
