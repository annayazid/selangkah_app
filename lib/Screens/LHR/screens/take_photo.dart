import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:image/image.dart' as ImageLib;

class TakePhoto extends StatefulWidget {
  final List<CameraDescription> camera;

  const TakePhoto({super.key, required this.camera});

  @override
  _TakePhotoState createState() => _TakePhotoState();
}

class _TakePhotoState extends State<TakePhoto> with WidgetsBindingObserver {
  CameraController? cameraController;
  List<CameraDescription>? cameras;
  bool flash = false;
  XFile? imageFile;
  int? selectedCameraIndex;
  File? imageFileFinal;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    getCameras();
  }

  getCameras() async {
    setState(() {
      cameras = widget.camera;
    });
    if (cameras!.length > 0) {
      selectedCameraIndex = 0;
      initCamera(
        cameras![selectedCameraIndex!],
      );
    } else {
      print('No camera available');
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (cameraController == null || !cameraController!.value.isInitialized) {
      print('resumed 1');
      return;
    }
    if (state == AppLifecycleState.inactive) {
      print('inactive');
      cameraController?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      print('resumed');
      if (cameraController != null) {
        onNewCameraSelected(cameraController!.description);
      }
    }
  }

  @override
  void dispose() {
    cameraController?.dispose();
    super.dispose();
  }

  Future initCamera(CameraDescription cameraDesc) async {
    if (cameraController != null) {
      await cameraController!.dispose();
    }
    cameraController = CameraController(
      cameraDesc,
      ResolutionPreset.high,
    );
    cameraController!.addListener(() {
      if (mounted) {
        setState(() {});
      }
    });

    if (cameraController!.value.hasError) {
      print('camera has error');
    }
    if (mounted) {
      setState(() {});
    }

    try {
      await cameraController!.initialize();
    } catch (e) {
      print('camera has error: ' + e.toString());
    }
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (cameraController != null) {
      await cameraController!.dispose();
    }

    final CameraController controller = CameraController(
      cameraDescription,
      ResolutionPreset.max,
      imageFormatGroup: ImageFormatGroup.jpeg,
    );

    cameraController = controller;

    cameraController!.addListener(() {
      if (mounted) setState(() {});
      if (cameraController!.value.hasError) {}
    });

    try {
      await cameraController!.initialize();
    } on CameraException catch (e) {
      print(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  onTakePictureButtonPressed(context, size) async {
    await takePicture().then((XFile? file) {
      if (mounted) {
        setState(() {
          imageFile = file;
        });
      }
    });
    if (imageFile != null) {
      readImageDialog(context, size);
    }
  }

  Future<XFile?> takePicture() async {
    if (!cameraController!.value.isInitialized) {
      return null;
    }

    if (cameraController!.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      XFile file = await cameraController!.takePicture();
      return file;
    } on CameraException catch (e) {
      print('error here :' + e.code);
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    if (imageFileFinal == null) {
      return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            child: Container(
          height: size.height,
          width: size.height,
          color: Colors.black,
          child: Stack(
            children: [
              Center(
                child: cameraPreview(size),
              ),
              // cameraOverlay(
              //   padding: 30,
              //   aspectRatio: 1,
              //   color: Color(0x55000000),
              // ),
              Positioned(
                top: 0,
                child: Container(
                  width: size.width,
                  margin: EdgeInsets.only(
                    top: MediaQuery.of(context).padding.top,
                  ),
                  child: Row(
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      Spacer(),
                      IconButton(
                        icon: Icon(
                          flash ? Icons.flash_off : Icons.flash_on,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          setState(() {
                            flash = !flash;
                            flash
                                ? cameraController!
                                    .setFlashMode(FlashMode.torch)
                                : cameraController!.setFlashMode(FlashMode.off);
                          });
                        },
                      ),
                      Spacer(),
                      IconButton(
                        icon: Icon(
                          Icons.settings,
                          color: Colors.white,
                        ),
                        onPressed: () {},
                      ),
                    ],
                  ),
                ),
              ),
              // Align(
              //   alignment: Alignment.center,
              //   child: Icon(
              //     Icons.add,
              //     color: Colors.cyan,
              //   ),
              // ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 100,
                  width: double.infinity,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.5),
                  ),
                  child: MaterialButton(
                    onPressed: () async {
                      await onTakePictureButtonPressed(context, size);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(
                          color: Colors.white,
                          width: 4,
                        ),
                      ),
                      padding: EdgeInsets.all(2),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        )),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text('Upload Document'),
          backgroundColor: Colors.black,
        ),
        backgroundColor: Colors.black,
        body: Stack(
          children: [
            Image.file(imageFileFinal!),
            Positioned(
              left: 0,
              right: 0,
              bottom: 20,
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop(imageFileFinal);
                },
                child: Image.asset(
                  'assets/images/lhr/tick_photo_confirm.png',
                  height: 60,
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  Widget cameraPreview(size) {
    if (cameraController == null || !cameraController!.value.isInitialized) {
      return Text(
        'Loading..',
        style: TextStyle(
          color: Colors.white,
        ),
      );
    }

    // final double previewAspectRatio = 0.7;
    return Center(
      child: CameraPreview(cameraController!),
    );

    // return Transform.scale(
    //   scale: 1 /
    //       (cameraController.value.aspectRatio *
    //           MediaQuery.of(context).size.aspectRatio),
    //   alignment: Alignment.topCenter,
    //   child: CameraPreview(cameraController),
    // );
    // return CameraPreview(cameraController);
  }

  Widget cameraOverlay({double? padding, double? aspectRatio, Color? color}) {
    double width = MediaQuery.of(context).size.width;

    double height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;

    return LayoutBuilder(builder: (context, constraints) {
      double parentAspectRatio = constraints.maxWidth / constraints.maxHeight;
      double horizontalPadding;
      double verticalPadding;

      if (parentAspectRatio < aspectRatio!) {
        horizontalPadding = padding!;
        verticalPadding = (constraints.maxHeight -
                ((constraints.maxWidth - 2 * padding) / aspectRatio)) /
            2;
      } else {
        verticalPadding = padding!;
        horizontalPadding = (constraints.maxWidth -
                ((constraints.maxHeight - 2 * padding) * aspectRatio)) /
            2;
      }
      return Stack(
        fit: StackFit.expand,
        children: [
          // Align(
          //   alignment: Alignment.centerLeft,
          //   child: Container(width: horizontalPadding, color: color),
          // ),
          // Align(
          //   alignment: Alignment.centerRight,
          //   child: Container(width: horizontalPadding, color: color),
          // ),
          // Align(
          //   alignment: Alignment.topCenter,
          //   child: Container(
          //       margin: EdgeInsets.only(
          //           left: horizontalPadding, right: horizontalPadding),
          //       height: verticalPadding - 120,
          //       color: color),
          // ),
          // Align(
          //   alignment: Alignment.bottomCenter,
          //   child: Container(
          //       margin: EdgeInsets.only(
          //           left: horizontalPadding, right: horizontalPadding),
          //       height: verticalPadding - 120,
          //       color: color),
          // ),
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: horizontalPadding,
              vertical: verticalPadding - height * 0.12,
            ),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.cyan),
              // borderRadius: BorderRadius.circular(15),
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: RotatedBox(
                quarterTurns: 1,
                child: Padding(
                  padding: EdgeInsets.only(right: height * 0.06),
                  child: Icon(
                    FontAwesomeIcons.user,
                    color: Colors.cyan.withOpacity(0.5),
                    size: width * 0.35,
                  ),
                ),
              ),
            ),
          ),
        ],
      );
    });
  }

  readImageDialog(BuildContext context, Size size) async {
    // set up the buttons
    // Widget okayButton = RaisedButton(
    //   child: Text(
    //     "Continue".tr().toString(),
    //     style: TextStyle(color: Colors.white),
    //   ),
    //   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
    //   color: Color(0xFF0076A8),
    //   onPressed: () async {},
    // );

    // File pic = File(imageFile.path);
    // String testPic = base64.encode(pic.readAsBytesSync());
    // log(testPic);

    File contrastFile = File(imageFile!.path);
    ImageLib.Image? contrast =
        ImageLib.decodeImage(contrastFile.readAsBytesSync());
    // contrast = ImageLib.copyRotate(contrast, -90);
    contrastFile.writeAsBytesSync(ImageLib.encodeJpg(contrast!));
    // setState(() {
    //   imageCache.clear();
    //   imageCache.clearLiveImages();
    //   // reload();
    // });

    // String testPic = base64.encode(contrastFile.readAsBytesSync());
    // log(testPic);

    // cameraController.stopImageStream();
    cameraController!.dispose();

    setState(() {
      imageFileFinal = contrastFile;
    });

    // Navigator.of(context).pushReplacement(
    //   MaterialPageRoute(
    //     builder: (context) => TakePhotoFinal(imageFile: contrastFile),
    //   ),
    // );

    // Alert(
    //   title: 'Please wait',
    //   style: AlertStyle(
    //     animationType: AnimationType.grow,
    //   ),
    //   closeFunction: () {},
    //   // onWillPopActive: true,
    //   closeIcon: Icon(
    //     Icons.ac_unit,
    //     color: Colors.white,
    //   ),
    //   context: context,
    //   type: AlertType.info,
    //   content: Column(
    //     children: [
    //       SizedBox(height: 20),
    //       SpinKitFadingCircle(
    //         color: Colors.cyan,
    //         size: 30,
    //       ),
    //       // SizedBox(height: 10),
    //     ],
    //   ),
    //   buttons: [],
    // ).show();

    // await Future.delayed(Duration(seconds: 2));

    //change here

    // Navigator.of(context).pushReplacement(
    //   MaterialPageRoute(
    //     builder: (context) => EKYCWaitAccount(),
    //   ),
    // );

    // showDialog(

    //   barrierDismissible: false,
    //   context: context,
    //   child: AlertDialog(
    //     content: Column(
    //       mainAxisSize: MainAxisSize.min,
    //       children: [
    //         Text('Please wait'),
    //         SizedBox(height: 20),
    //         SpinKitFadingCircle(
    //           color: kPrimaryColor,
    //           size: 30,
    //         ),
    //       ],
    //     ),
    //     // content: Text('test'),
    //   ),
    // );

    // showDialog(
    //   context: context,
    //   builder: (context) => BlocProvider(
    //     create: (context) => ReadImageCubit(),
    //     child: ReadImageDialog(
    //       okayButton: okayButton,
    //       pic: File(imageFile.path),
    //       testType: widget.testType,
    //     ),
    //   ),
    // );
  }
}
