import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/utils/constants.dart';

class UploadDocumentResult extends StatefulWidget {
  final String? documentType;
  final File? documentFile;
  final String? fileName;
  final String? documentTypeName;

  const UploadDocumentResult(
      {super.key,
      this.documentType,
      this.documentFile,
      this.fileName,
      this.documentTypeName});

  @override
  _UploadDocumentResultState createState() => _UploadDocumentResultState();
}

class _UploadDocumentResultState extends State<UploadDocumentResult> {
  @override
  void initState() {
    context.read<SubmitDocumentCubit>().sendDocument(
        widget.documentType!, widget.documentFile!, widget.fileName!);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    return LhrScaffold(
      appBarTitle: 'lifetime'.tr(),
      child: Padding(
        padding: EdgeInsets.only(top: 30, left: 10, right: 10, bottom: 30),
        child: Column(
          children: [
            Text(
              'upload_doc'.tr(),
              style: TextStyle(fontSize: 16),
            ),
            BlocBuilder<SubmitDocumentCubit, SubmitDocumentState>(
              builder: (context, state) {
                if (state is SubmitDocumentLoaded) {
                  return Column(
                    children: [
                      SizedBox(height: 50),
                      Image.asset(
                        'assets/images/lhr/tick_uploaded.png',
                        height: 50,
                      ),
                      SizedBox(height: 50),
                      Text(
                        '${'successfully_upload'.tr()} ${widget.documentTypeName}',
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 50),
                      Container(
                        width: width * 0.7,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Color(0xFFEF0056),
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(15), // <-- Radius
                            ),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pop();
                            // Navigator.of(context).push(
                            //   MaterialPageRoute(
                            //     builder: (context) => BlocProvider(
                            //       create: (context) => SubmitDocumentCubit(),
                            //       child: UploadDocumentResult(),
                            //     ),
                            //   ),
                            // );
                          },
                          child: Text(
                            'done'.tr(),
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                      ),
                    ],
                  );
                } else if (state is SubmitDocumentError) {
                  return Column(
                    children: [
                      SizedBox(height: 50),
                      // Image.asset(
                      //   'assets/images/lhr/tick_uploaded.png',
                      //   height: 50,
                      // ),
                      Icon(
                        FontAwesomeIcons.circleXmark,
                        size: 75,
                        color: Colors.red,
                      ),
                      SizedBox(height: 50),
                      Text(
                        'error_message_upload'.tr(),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 50),
                      Container(
                        width: width * 0.7,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Color(0xFFEF0056),
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(15), // <-- Radius
                            ),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pop();
                            // Navigator.of(context).push(
                            //   MaterialPageRoute(
                            //     builder: (context) => BlocProvider(
                            //       create: (context) => SubmitDocumentCubit(),
                            //       child: UploadDocumentResult(),
                            //     ),
                            //   ),
                            // );
                          },
                          child: Text(
                            'done'.tr(),
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                      ),
                    ],
                  );
                } else {
                  return Column(
                    children: [
                      SizedBox(
                        height: 50,
                      ),
                      SpinKitFadingCircle(
                        size: 30,
                        color: kPrimaryColor,
                      ),
                    ],
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
