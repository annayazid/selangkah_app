import 'package:flutter/material.dart';

class LhrScaffold extends StatelessWidget {
  final String? appBarTitle;
  final Widget? child;

  const LhrScaffold({super.key, this.appBarTitle, this.child});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: true,
        title: Text(
          appBarTitle!,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 15.0,
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: "Roboto",
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: height - AppBar().preferredSize.height,
              width: width,
              child: child,
            ),
          ],
        ),
      ),
    );
  }
}
