import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';

part 'get_edit_data_state.dart';

class GetEditDataCubit extends Cubit<GetEditDataState> {
  GetEditDataCubit() : super(GetEditDataInitial());

  Future<void> getEditData({
    required BubbleInfoData bubbleInfoData,
    required String className,
  }) async {
    emit(GetEditDataLoading());

    BubbleRelation bubbleRelation = await BubbleRepositories.getRelation();

    bool boolCanEditDependent = false;

    print('classname is $className');

    if (className.toLowerCase() == "family".tr().toLowerCase() &&
        bubbleInfoData.isAdmin == '0') {
      boolCanEditDependent = true;
    }

    emit(GetEditDataLoaded(bubbleRelation, boolCanEditDependent));
  }
}
