part of 'get_edit_data_cubit.dart';

@immutable
abstract class GetEditDataState {
  const GetEditDataState();
}

class GetEditDataInitial extends GetEditDataState {
  const GetEditDataInitial();
}

class GetEditDataLoading extends GetEditDataState {
  const GetEditDataLoading();
}

class GetEditDataLoaded extends GetEditDataState {
  final BubbleRelation bubbleRelation;
  final bool boolCanEditDependent;

  GetEditDataLoaded(
    this.bubbleRelation,
    this.boolCanEditDependent,
  );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetEditDataLoaded &&
        other.bubbleRelation == bubbleRelation &&
        other.boolCanEditDependent == boolCanEditDependent;
  }

  @override
  int get hashCode => bubbleRelation.hashCode ^ boolCanEditDependent.hashCode;
}
