import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';

part 'check_user_state.dart';

class CheckUserCubit extends Cubit<CheckUserState> {
  CheckUserCubit() : super(CheckUserInitial());

  Future<void> checkUser(String selId) async {
    emit(CheckUserLoading());

    //process

    String id = await BubbleRepositories.checkUserReg(selId);

    print('id is $id');

    if (id == '') {
      emit(CheckUserNotExist());
    } else {
      UserProfile userProfile = await BubbleRepositories.getProfile(id);

      emit(CheckUserVerified(userProfile));
    }
  }
}
