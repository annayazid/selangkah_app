part of 'check_user_cubit.dart';

@immutable
abstract class CheckUserState {
  const CheckUserState();
}

class CheckUserInitial extends CheckUserState {
  const CheckUserInitial();
}

class CheckUserLoading extends CheckUserState {
  const CheckUserLoading();
}

class CheckUserVerified extends CheckUserState {
  final UserProfile userProfile;

  CheckUserVerified(this.userProfile);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CheckUserVerified && other.userProfile == userProfile;
  }

  @override
  int get hashCode => userProfile.hashCode;
}

class CheckUserNotExist extends CheckUserState {
  const CheckUserNotExist();
}
