import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

part 'get_bubble_state.dart';

class GetBubbleCubit extends Cubit<GetBubbleState> {
  GetBubbleCubit() : super(GetBubbleInitial());

  Future<void> getBubbleList() async {
    print('calling bubbleyy');
    emit(GetBubbleLoading());

    BubbleList bubbleList = await BubbleRepositories.getBubbleList();

    String selId = await SecureStorage().readSecureData('userSelId');
    String name = await SecureStorage().readSecureData('userName');

    emit(GetBubbleLoaded(
      name: name,
      bubbleList: bubbleList,
      selId: selId,
    ));
  }

  Future<void> deleteBubble(String bubbleId) async {
    emit(GetBubbleLoading());

    await BubbleRepositories.deleteBubble(bubbleId);

    getBubbleList();

    Fluttertoast.showToast(
      msg: 'success_exit_bubble'.tr(),
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
    );
  }
}
