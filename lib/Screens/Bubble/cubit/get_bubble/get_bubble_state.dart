part of 'get_bubble_cubit.dart';

@immutable
abstract class GetBubbleState {
  const GetBubbleState();
}

class GetBubbleInitial extends GetBubbleState {
  const GetBubbleInitial();
}

class GetBubbleLoading extends GetBubbleState {
  const GetBubbleLoading();
}

class GetBubbleLoaded extends GetBubbleState {
  final String name;
  final String selId;
  final BubbleList bubbleList;

  GetBubbleLoaded(
      {required this.name, required this.selId, required this.bubbleList});

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetBubbleLoaded &&
        other.name == name &&
        other.selId == selId &&
        other.bubbleList == bubbleList;
  }

  @override
  int get hashCode => name.hashCode ^ selId.hashCode ^ bubbleList.hashCode;
}
