part of 'edit_bubble_cubit.dart';

@immutable
abstract class EditBubbleState {
  const EditBubbleState();
}

class EditBubbleInitial extends EditBubbleState {
  const EditBubbleInitial();
}

class EditBubbleLoading extends EditBubbleState {
  const EditBubbleLoading();
}

class EditBubbleLoaded extends EditBubbleState {
  const EditBubbleLoaded();
}
