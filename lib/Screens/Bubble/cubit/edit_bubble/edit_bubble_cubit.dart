import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';

part 'edit_bubble_state.dart';

class EditBubbleCubit extends Cubit<EditBubbleState> {
  EditBubbleCubit() : super(EditBubbleInitial());

  Future<void> editBubble({
    required String bubbleId,
    required String bubbleName,
    required String selectedClassId,
  }) async {
    emit(EditBubbleLoading());

    //process

    await BubbleRepositories.editBubble(
        bubbleId: bubbleId,
        bubbleName: bubbleName,
        selectedClassId: selectedClassId);

    emit(EditBubbleLoaded());
  }
}
