part of 'get_relation_cubit.dart';

@immutable
abstract class GetRelationState {
  const GetRelationState();
}

class GetRelationInitial extends GetRelationState {
  const GetRelationInitial();
}

class GetRelationLoading extends GetRelationState {
  const GetRelationLoading();
}

class GetRelationLoaded extends GetRelationState {
  final BubbleRelation bubbleRelation;

  GetRelationLoaded(this.bubbleRelation);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetRelationLoaded && other.bubbleRelation == bubbleRelation;
  }

  @override
  int get hashCode => bubbleRelation.hashCode;
}
