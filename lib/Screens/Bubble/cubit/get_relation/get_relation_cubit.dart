import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';

part 'get_relation_state.dart';

class GetRelationCubit extends Cubit<GetRelationState> {
  GetRelationCubit() : super(GetRelationInitial());

  Future<void> getRelation() async {
    emit(GetRelationLoading());

    //process

    BubbleRelation bubbleRelation = await BubbleRepositories.getRelation();

    emit(GetRelationLoaded(bubbleRelation));
  }
}
