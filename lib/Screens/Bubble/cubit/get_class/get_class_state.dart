part of 'get_class_cubit.dart';

@immutable
abstract class GetClassState {
  const GetClassState();
}

class GetClassInitial extends GetClassState {
  const GetClassInitial();
}

class GetClassLoading extends GetClassState {
  const GetClassLoading();
}

class GetClassLoaded extends GetClassState {
  final BubbleClass bubbleClass;
  final BubbleMember bubbleMember;
  final String selId;

  GetClassLoaded(this.bubbleClass, this.bubbleMember, this.selId);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetClassLoaded &&
        other.bubbleClass == bubbleClass &&
        other.bubbleMember == bubbleMember &&
        other.selId == selId;
  }

  @override
  int get hashCode =>
      bubbleClass.hashCode ^ bubbleMember.hashCode ^ selId.hashCode;
}

class GetClassOnlyLoaded extends GetClassState {
  final BubbleClass bubbleClass;

  GetClassOnlyLoaded(this.bubbleClass);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetClassOnlyLoaded && other.bubbleClass == bubbleClass;
  }

  @override
  int get hashCode => bubbleClass.hashCode;
}
