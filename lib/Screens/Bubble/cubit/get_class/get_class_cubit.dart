import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

part 'get_class_state.dart';

class GetClassCubit extends Cubit<GetClassState> {
  GetClassCubit() : super(GetClassInitial());

  Future<void> getBubbleClass() async {
    emit(GetClassLoading());

    //process
    BubbleClass bubbleClass = await BubbleRepositories.getBubbleClass();
    BubbleMember bubbleMember = await BubbleRepositories.getBubbleMember();

    String id = await SecureStorage().readSecureData('userSelId');

    emit(GetClassLoaded(bubbleClass, bubbleMember, id));
  }

  Future<void> getBubbleClassOnly() async {
    emit(GetClassLoading());

    //process
    BubbleClass bubbleClass = await BubbleRepositories.getBubbleClass();

    emit(GetClassOnlyLoaded(bubbleClass));
  }
}
