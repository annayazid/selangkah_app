part of 'add_member_cubit.dart';

@immutable
abstract class AddMemberState {
  const AddMemberState();
}

class AddMemberInitial extends AddMemberState {
  const AddMemberInitial();
}

class AddMemberLoading extends AddMemberState {
  const AddMemberLoading();
}

class AddMemberLoaded extends AddMemberState {
  const AddMemberLoaded();
}
