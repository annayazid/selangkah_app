import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';

part 'add_member_state.dart';

class AddMemberCubit extends Cubit<AddMemberState> {
  AddMemberCubit() : super(AddMemberInitial());

  Future<void> addMemberUsingId(
      String bubbleId, UserProfile userProfile) async {
    emit(AddMemberLoading());

    //
    await BubbleRepositories.addMemberUsingId(bubbleId, userProfile);

    emit(AddMemberLoaded());
  }

  Future<void> addMemberRegister({
    required String bubbleId,
    required String relationId,
    required String isAdmin,
    required String name,
    required String isDependent,
  }) async {
    emit(AddMemberLoading());

    //
    await BubbleRepositories.addMemberRegister(
      bubbleId: bubbleId,
      isAdmin: isAdmin,
      isDependent: isDependent,
      name: name,
      relationId: relationId,
    );

    emit(AddMemberLoaded());
  }
}
