part of 'edit_member_cubit.dart';

@immutable
abstract class EditMemberState {
  const EditMemberState();
}

class EditMemberInitial extends EditMemberState {
  const EditMemberInitial();
}

class EditMemberLoading extends EditMemberState {
  const EditMemberLoading();
}

class EditMemberLoaded extends EditMemberState {
  const EditMemberLoaded();
}
