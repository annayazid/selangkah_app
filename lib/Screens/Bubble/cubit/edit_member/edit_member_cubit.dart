import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';

part 'edit_member_state.dart';

class EditMemberCubit extends Cubit<EditMemberState> {
  EditMemberCubit() : super(EditMemberInitial());

  Future<void> editMember({
    required BubbleInfoData bubbleInfoData,
    required String relationId,
    required String isAdmin,
    required String isDependent,
    required String name,
  }) async {
    emit(EditMemberLoading());

    await BubbleRepositories.editMember(
      isAdmin: isAdmin,
      isDependent: isDependent,
      memberUserId: bubbleInfoData.id!,
      name: name,
      relationId: relationId,
    );

    emit(EditMemberLoaded());
  }
}
