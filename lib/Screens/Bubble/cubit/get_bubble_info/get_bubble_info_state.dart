part of 'get_bubble_info_cubit.dart';

@immutable
abstract class GetBubbleInfoState {
  const GetBubbleInfoState();
}

class GetBubbleInfoInitial extends GetBubbleInfoState {
  const GetBubbleInfoInitial();
}

class GetBubbleInfoLoading extends GetBubbleInfoState {
  const GetBubbleInfoLoading();
}

class GetBubbleInfoLoaded extends GetBubbleInfoState {
  final BubbleInfoMember bubbleInfoMember;
  final List<Uint8List?> profilePic;

  GetBubbleInfoLoaded(this.bubbleInfoMember, this.profilePic);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetBubbleInfoLoaded &&
        other.bubbleInfoMember == bubbleInfoMember &&
        listEquals(other.profilePic, profilePic);
  }

  @override
  int get hashCode => bubbleInfoMember.hashCode ^ profilePic.hashCode;
}
