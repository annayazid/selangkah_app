import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:selangkah_new/Screens/Bubble/bubble.dart';

part 'get_bubble_info_state.dart';

class GetBubbleInfoCubit extends Cubit<GetBubbleInfoState> {
  GetBubbleInfoCubit() : super(GetBubbleInfoInitial());

  Future<void> getBubbleInfo(String bubbleId) async {
    emit(GetBubbleInfoLoading());

    //process

    BubbleInfoMember bubbleInfoMember =
        await BubbleRepositories.getBubbleInfo(bubbleId);

    List<Uint8List?> profilePic = [];

    for (var i = 0; i < bubbleInfoMember.data!.length; i++) {
      if (bubbleInfoMember.data![i].profilePic != null) {
        profilePic.add(base64.decode(bubbleInfoMember.data![i].profilePic!));
      } else {
        profilePic.add(null);
      }
    }

    emit(GetBubbleInfoLoaded(bubbleInfoMember, profilePic));
  }

  Future<void> deleteMember(String bubbleMemberId, String bubbleId) async {
    emit(GetBubbleInfoLoading());

    //process

    await BubbleRepositories.deleteMember(bubbleMemberId);

    getBubbleInfo(bubbleId);

    Fluttertoast.showToast(
      msg: 'success_exit_bubble'.tr(),
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
    );
  }
}
