part of 'create_bubble_cubit.dart';

@immutable
abstract class CreateBubbleState {
  const CreateBubbleState();
}

class CreateBubbleInitial extends CreateBubbleState {
  const CreateBubbleInitial();
}

class CreateBubbleLoading extends CreateBubbleState {
  const CreateBubbleLoading();
}

class CreateBubbleLoaded extends CreateBubbleState {
  const CreateBubbleLoaded();
}
