import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';

part 'create_bubble_state.dart';

class CreateBubbleCubit extends Cubit<CreateBubbleState> {
  CreateBubbleCubit() : super(CreateBubbleInitial());

  Future<void> createBubble({
    required String bubbleName,
    required String selectedClassId,
    required List<bool> tick,
    required BubbleMember bubbleMember,
  }) async {
    emit(CreateBubbleLoading());

    //

    print(bubbleName);
    print(selectedClassId);
    print(tick);

    await BubbleRepositories.createBubble(
      bubbleName: bubbleName,
      selectedClassId: selectedClassId,
      tick: tick,
      bubbleMember: bubbleMember,
    );

    Fluttertoast.showToast(
      msg: 'success_exit_bubble'.tr(),
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
    );

    emit(CreateBubbleLoaded());
  }
}
