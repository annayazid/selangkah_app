import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';
import 'package:selangkah_new/utils/constants.dart';

class ConfirmAddMember extends StatelessWidget {
  final UserProfile userProfile;
  final String bubbleId;

  const ConfirmAddMember({
    super.key,
    required this.userProfile,
    required this.bubbleId,
  });

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    return BubbleScaffold(
      child: SingleChildScrollView(
        child: Container(
          height: height,
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    height: height * 0.325,
                    width: double.infinity,
                    child: Image.asset(
                      'assets/images/Bubble/Bg_Large.png',
                      // height: height * 0.3,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Center(
                    child: Column(
                      children: [
                        SizedBox(height: height * 0.175),
                        Container(
                          margin: EdgeInsets.all(15),
                          padding: EdgeInsets.all(20),
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                offset: Offset(0.5, 0.5),
                                blurRadius: 2,
                              ),
                            ],
                          ),
                          child: Column(
                            children: [
                              SizedBox(height: height * 0.10),
                              Text(
                                userProfile.name!,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 15),
                              Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      'role'.tr(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Expanded(
                                    child: Text('Member'),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15),
                              Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      'phonenumber'.tr(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Expanded(
                                    child: Text('+6${userProfile.phoneNo}'),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15),
                              Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      'ID No.',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Expanded(
                                    child: Text(userProfile.selId!),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      SizedBox(height: 15),
                      Center(
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 15, vertical: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: kPrimaryColor,
                          ),
                          child: Text(
                            'confirm_add_member'.tr(),
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      userProfile.profilePic == null ||
                              userProfile.profilePic == ''
                          ? Container(
                              padding: EdgeInsets.all(20),
                              child: Icon(
                                FontAwesomeIcons.user,
                                size: 80,
                              ),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle,
                              ),
                            )
                          : Center(
                              child: CircleAvatar(
                                radius: 65,
                                backgroundColor: Colors.white,
                                child: CircleAvatar(
                                  backgroundColor: Colors.white,
                                  radius: 60,
                                  backgroundImage: () {
                                    if (userProfile.profilePic == null ||
                                        userProfile.profilePic == '') {
                                      return AssetImage(
                                          'assets/images/Bubble/person.png');
                                    } else {
                                      Uint8List imgbytes = base64
                                          .decode(userProfile.profilePic!);
                                      return MemoryImage(imgbytes);
                                    }
                                  }() as ImageProvider,
                                ),
                              ),
                            ),
                    ],
                  ),
                ],
              ),
              Spacer(),
              Container(
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 15),
                child: ElevatedButton(
                  style:
                      ElevatedButton.styleFrom(backgroundColor: kPrimaryColor),
                  onPressed: () {
                    context
                        .read<AddMemberCubit>()
                        .addMemberUsingId(bubbleId, userProfile);
                  },
                  child: BlocConsumer<AddMemberCubit, AddMemberState>(
                    listener: (context, state) {
                      if (state is AddMemberLoaded) {
                        Fluttertoast.showToast(
                          msg: 'success_message_member'.tr().toString(),
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                        );
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                      }
                    },
                    builder: (context, state) {
                      if (state is AddMemberLoading) {
                        return SpinKitFadingCircle(
                          color: Colors.white,
                          size: 14,
                        );
                      } else {
                        return Text('bubble_add_member'.tr());
                      }
                    },
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 15),
                child: ElevatedButton(
                  style:
                      ElevatedButton.styleFrom(backgroundColor: Colors.white),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'cancel'.tr(),
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }
}
