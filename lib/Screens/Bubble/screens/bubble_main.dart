import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:shimmer/shimmer.dart';
import 'package:easy_localization/easy_localization.dart';

class BubbleMain extends StatefulWidget {
  const BubbleMain({Key? key}) : super(key: key);

  @override
  State<BubbleMain> createState() => _BubbleMainState();
}

class _BubbleMainState extends State<BubbleMain> {
  @override
  void initState() {
    GlobalFunction.screenJourney('27');
    context.read<GetBubbleCubit>().getBubbleList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height =
        MediaQuery.of(context).size.height - AppBar().preferredSize.height;

    double width = MediaQuery.of(context).size.width;

    return Stack(
      children: [
        SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: height * 0.35,
                width: double.infinity,
                child: Stack(
                  children: [
                    Container(
                      height: height * 0.35,
                      width: double.infinity,
                      child: Image.asset(
                        'assets/images/Bubble/Bg_Large.png',
                        // height: height * 0.3,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Center(
                      child: Column(
                        children: [
                          SizedBox(height: 10),
                          Expanded(
                            flex: 2,
                            child: BlocBuilder<GetBubbleCubit, GetBubbleState>(
                              builder: (context, state) {
                                if (state is GetBubbleLoaded) {
                                  //name loaded
                                  return Container(
                                    width: width * 0.5,
                                    decoration: BoxDecoration(
                                      color: kPrimaryColor,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 5,
                                      vertical: 10,
                                    ),
                                    child: Center(
                                      child: Text(
                                        state.name,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  );
                                } else {
                                  //name shimmer
                                  return Shimmer.fromColors(
                                    baseColor: Colors.grey.shade300,
                                    highlightColor: Colors.white,
                                    child: Container(
                                      width: width * 0.5,
                                      decoration: BoxDecoration(
                                        color: kPrimaryColor,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      padding: EdgeInsets.symmetric(
                                        horizontal: 5,
                                        vertical: 10,
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                          ),
                          SizedBox(height: 10),
                          Expanded(
                            flex: 5,
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                  color: kPrimaryColor,
                                  width: 10,
                                ),
                              ),
                              child:
                                  BlocBuilder<GetBubbleCubit, GetBubbleState>(
                                builder: (context, state) {
                                  if (state is GetBubbleLoaded) {
                                    //qr loaded
                                    return QrImage(
                                      backgroundColor: Colors.white,
                                      data: state.selId,
                                      version: QrVersions.auto,
                                      // size: width * 0.5,
                                    );
                                  } else {
                                    //qt shimmer
                                    return Shimmer.fromColors(
                                      baseColor: Colors.grey.shade300,
                                      highlightColor: Colors.white,
                                      child: QrImage(
                                        backgroundColor: Colors.white,
                                        data: '',
                                        version: QrVersions.auto,

                                        // size: width * 0.5,
                                      ),
                                    );
                                  }
                                },
                              ),
                            ),
                          ),
                          SizedBox(height: 15),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              BlocBuilder<GetBubbleCubit, GetBubbleState>(
                builder: (context, state) {
                  if (state is GetBubbleLoaded) {
                    //bubble list loaded

                    return ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.bubbleList.data!.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.of(context, rootNavigator: true).push(
                              MaterialPageRoute(
                                builder: (context) => BlocProvider(
                                  create: (context) => GetBubbleInfoCubit(),
                                  child: BubbleInfo(
                                    bubbleName: state
                                        .bubbleList.data![index].bubbleName!,
                                    className: state
                                        .bubbleList.data![index].className!,
                                    bubbleId: state.bubbleList.data![index].id!,
                                  ),
                                ),
                              ),
                            );
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 5),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(0.5, 0.5),
                                  blurRadius: 2,
                                ),
                              ],
                            ),
                            // height: 100,
                            width: double.infinity,
                            child: Row(
                              children: [
                                CircleAvatar(
                                  backgroundColor: Colors.red[100],
                                  radius: 25,
                                  backgroundImage: () {
                                    if (state.bubbleList.data![index]
                                            .className ==
                                        'Family') {
                                      return AssetImage(
                                          'assets/images/Bubble/family-fm.png');
                                    } else if (state.bubbleList.data![index]
                                            .className ==
                                        'Family') {
                                      return AssetImage(
                                          'assets/images/Bubble/family-fm.png');
                                    } else {
                                      return AssetImage(
                                          'assets/images/Bubble/friend.png');
                                    }
                                  }(),
                                ),
                                SizedBox(width: 20),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        state.bubbleList.data![index]
                                            .bubbleName!,
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(height: 10),
                                      Text(
                                        state
                                            .bubbleList.data![index].className!,
                                        style: TextStyle(
                                          fontSize: 13,
                                          color: Colors.grey[600],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(width: 10),
                                Visibility(
                                  visible:
                                      state.bubbleList.data![index].isAdmin ==
                                              '1'
                                          ? true
                                          : false,
                                  child: Row(
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          Navigator.of(context,
                                                  rootNavigator: true)
                                              .push(
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      MultiBlocProvider(
                                                    providers: [
                                                      BlocProvider(
                                                        create: (context) =>
                                                            GetClassCubit(),
                                                      ),
                                                      BlocProvider(
                                                        create: (context) =>
                                                            EditBubbleCubit(),
                                                      ),
                                                    ],
                                                    child: EditBubble(
                                                      bubbleId: state.bubbleList
                                                          .data![index].id!,
                                                      bubbleName: state
                                                          .bubbleList
                                                          .data![index]
                                                          .bubbleName!,
                                                      selectedClassId: state
                                                          .bubbleList
                                                          .data![index]
                                                          .idBubbleClass!,
                                                    ),
                                                  ),
                                                ),
                                              )
                                              .then((value) => context
                                                  .read<GetBubbleCubit>()
                                                  .getBubbleList());
                                        },
                                        child: Image(
                                          image: AssetImage(
                                              "assets/images/Bubble/Edit.png"),
                                          width: height * 0.05,
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                      InkWell(
                                        onTap: () {
                                          Alert(
                                            context: context,
                                            type: AlertType.warning,
                                            title: 'delete_bubble'.tr(),
                                            buttons: [
                                              DialogButton(
                                                child: Text(
                                                  'cancel'.tr(),
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 20),
                                                ),
                                                onPressed: () {
                                                  Navigator.of(context,
                                                          rootNavigator: true)
                                                      .pop();
                                                },
                                                color: Colors.green,
                                              ),
                                              DialogButton(
                                                child: Text(
                                                  'Okay',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 20),
                                                ),
                                                onPressed: () {
                                                  Navigator.of(context,
                                                          rootNavigator: true)
                                                      .pop();
                                                  context
                                                      .read<GetBubbleCubit>()
                                                      .deleteBubble(state
                                                          .bubbleList
                                                          .data![index]
                                                          .id!);
                                                  // Navigator.of(context).pop();
                                                },
                                                color: kPrimaryColor,
                                              ),
                                            ],
                                          ).show();
                                        },
                                        child: Image(
                                          image: AssetImage(
                                              "assets/images/Bubble/Delete.png"),
                                          width: height * 0.05,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  } else {
                    //bubble list shimmer
                    return ListView.builder(
                      shrinkWrap: true,
                      itemCount: 3,
                      itemBuilder: (BuildContext context, int index) {
                        return Shimmer.fromColors(
                          baseColor: Colors.grey.shade300,
                          highlightColor: Colors.white,
                          child: Container(
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 5),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(0.5, 0.5),
                                  blurRadius: 2,
                                ),
                              ],
                            ),
                            height: 100,
                          ),
                        );
                      },
                    );
                  }
                },
              ),
              SizedBox(height: 50),
            ],
          ),
        ),
        BlocBuilder<GetBubbleCubit, GetBubbleState>(
          builder: (context, state) {
            if (state is GetBubbleLoaded) {
              return Positioned.fill(
                bottom: height * 0.01,
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 15),
                    width: double.infinity,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: kPrimaryColor),
                      onPressed: () {
                        Navigator.of(context, rootNavigator: true)
                            .push(
                              MaterialPageRoute(
                                builder: (context) => MultiBlocProvider(
                                  providers: [
                                    BlocProvider(
                                      create: (context) => GetClassCubit(),
                                    ),
                                    BlocProvider(
                                      create: (context) => CreateBubbleCubit(),
                                    ),
                                  ],
                                  child: BubbleCreate(),
                                ),
                              ),
                            )
                            .then((value) =>
                                context.read<GetBubbleCubit>().getBubbleList());
                      },
                      child: Text(
                        'bubble_create_button'.tr(),
                        style: TextStyle(fontSize: 16, color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              );
            } else {
              return Container();
            }
          },
        ),
      ],
    );
  }
}
