import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:shimmer/shimmer.dart';

class AddMemberRegister extends StatefulWidget {
  final String bubbleName;
  final String className;
  final String bubbleId;

  const AddMemberRegister(
      {super.key,
      required this.bubbleName,
      required this.className,
      required this.bubbleId});

  @override
  State<AddMemberRegister> createState() => _AddMemberRegisterState();
}

class _AddMemberRegisterState extends State<AddMemberRegister> {
  @override
  void initState() {
    context.read<GetRelationCubit>().getRelation();
    super.initState();
  }

  TextEditingController nameController = TextEditingController();

  final List<String> roleList = ['Member', 'Admin'];
  String? selectedRole;
  String? selectedRelationId;

  int radioButton = 1;
  int dependent = 1;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;

    // double width = MediaQuery.of(context).size.width;
    return BubbleScaffold(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: height * 0.225,
              child: Stack(
                children: [
                  Container(
                    height: height * 0.225,
                    width: double.infinity,
                    child: Image.asset(
                      'assets/images/Bubble/Bg_Large.png',
                      // height: height * 0.3,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Center(
                    child: Column(
                      children: [
                        SizedBox(height: 15),
                        Center(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: kPrimaryColor,
                            ),
                            child: Text(
                              'bubble_add_member'.tr(),
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          widget.bubbleName.toUpperCase(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          widget.className.toUpperCase(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            BlocBuilder<GetRelationCubit, GetRelationState>(
              builder: (context, state) {
                if (state is GetRelationLoaded) {
                  return Container(
                    margin: EdgeInsets.all(15),
                    padding: EdgeInsets.all(15),
                    // height: 800,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.5, 0.5),
                          blurRadius: 2,
                        ),
                      ],
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'name'.tr(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'name_required'.tr();
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 2.0,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                  width: 2.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 2.0,
                                ),
                              ),
                            ),
                            controller: nameController,
                          ),
                          SizedBox(height: 15),
                          Text(
                            'role'.tr(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(height: 10),
                          DropdownButtonHideUnderline(
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButtonFormField<String>(
                                validator: (value) {
                                  if (value == null) {
                                    return 'Please select role';
                                  } else {
                                    return null;
                                  }
                                },
                                decoration: InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 2.0,
                                    ),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 2.0,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.grey,
                                      width: 2.0,
                                    ),
                                  ),
                                ),
                                isExpanded: true,
                                value: selectedRole,
                                onChanged: (value) {
                                  setState(() {
                                    selectedRole = value;
                                  });
                                },
                                items: roleList.map((e) {
                                  return DropdownMenuItem<String>(
                                    value: e,
                                    child: Text(e),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                          SizedBox(height: 15),
                          Text(
                            'personal_relation'.tr(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(height: 10),
                          DropdownButtonHideUnderline(
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButtonFormField<String>(
                                validator: (value) {
                                  if (value == null) {
                                    return 'Please select relation';
                                  } else {
                                    return null;
                                  }
                                },
                                decoration: InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 2.0,
                                    ),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 2.0,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.grey,
                                      width: 2.0,
                                    ),
                                  ),
                                ),
                                isExpanded: true,
                                value: selectedRelationId,
                                onChanged: (value) {
                                  setState(() {
                                    selectedRelationId = value;
                                  });
                                },
                                items: state.bubbleRelation.data!.map((e) {
                                  return DropdownMenuItem<String>(
                                    value: e.id,
                                    child: Text(e.relName!),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                          SizedBox(height: 10),
                          Row(
                            children: [
                              Radio(
                                  activeColor: kPrimaryColor,
                                  value: 1,
                                  groupValue: radioButton,
                                  onChanged: (val) {
                                    setState(() {
                                      radioButton = val!;
                                      dependent = 1;
                                      // print('hooo $val');
                                    });
                                  }),
                              Text('dependent'.tr()),
                              Radio(
                                  activeColor: kPrimaryColor,
                                  value: 2,
                                  groupValue: radioButton,
                                  onChanged: (val) {
                                    setState(() {
                                      radioButton = val!;
                                      dependent = 0;
                                      // print('hooo $val');
                                    });
                                  }),
                              Text('not_dependent'.tr())
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                } else {
                  return Shimmer.fromColors(
                    baseColor: Colors.grey.shade300,
                    highlightColor: Colors.white,
                    child: Container(
                      height: 400,
                      margin: EdgeInsets.all(15),
                      padding: EdgeInsets.all(15),
                      // height: 800,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0.5, 0.5),
                            blurRadius: 2,
                          ),
                        ],
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  );
                }
              },
            ),
            // SizedBox(height: 20),
            Container(
              margin: EdgeInsets.all(15),
              width: double.infinity,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(backgroundColor: kPrimaryColor),
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    String isAdmin = selectedRole == 'Admin' ? '1' : '0';
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) => BlocProvider(
                          create: (context) => AddMemberCubit(),
                          child: ConfirmAddMemberRegister(
                            bubbleId: widget.bubbleId,
                            isAdmin: isAdmin,
                            name: nameController.text,
                            relationId: selectedRelationId!,
                            isDependent: '$dependent',
                          ),
                        ),
                      ),
                    );
                  }
                },
                child: Text(
                  'bubble_create_button'.tr(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
