import 'package:flutter/material.dart';
import 'package:selangkah_new/utils/constants.dart';

class BubbleScaffold extends StatelessWidget {
  final Widget child;

  const BubbleScaffold({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height -
    //     AppBar().preferredSize.height -
    //     MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(
              Icons.arrow_back,
              color: kPrimaryColor,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Image.asset(
            "assets/images/Scaffold/selangkah_logo.png",
            width: width * 0.45,
          ),
        ),
        body: child,
      ),
    );
  }
}
