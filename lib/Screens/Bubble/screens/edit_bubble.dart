import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:shimmer/shimmer.dart';

class EditBubble extends StatefulWidget {
  final String bubbleId;
  final String bubbleName;
  final String selectedClassId;

  const EditBubble({
    super.key,
    required this.bubbleId,
    required this.bubbleName,
    required this.selectedClassId,
  });

  @override
  State<EditBubble> createState() => _EditBubbleState();
}

class _EditBubbleState extends State<EditBubble> {
  TextEditingController nameController = TextEditingController();

  String? selectedClassId;
  @override
  void initState() {
    nameController.text = widget.bubbleName;
    selectedClassId = widget.selectedClassId;
    context.read<GetClassCubit>().getBubbleClassOnly();
    super.initState();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double height =
        MediaQuery.of(context).size.height - AppBar().preferredSize.height;
    return BubbleScaffold(
      child: Container(
        height: height,
        child: Column(
          children: [
            SizedBox(height: 30),
            Center(
              child: Text(
                'edit_bubble'.tr(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
            ),
            SizedBox(height: 30),
            BlocBuilder<GetClassCubit, GetClassState>(
              builder: (context, state) {
                if (state is GetClassOnlyLoaded) {
                  return Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.5, 0.5),
                          blurRadius: 2,
                        ),
                      ],
                    ),
                    margin: EdgeInsets.all(15),
                    padding: EdgeInsets.all(20),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'name'.tr(),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'name_required'.tr();
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 2.0,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                  width: 2.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 2.0,
                                ),
                              ),
                            ),
                            controller: nameController,
                          ),
                          SizedBox(height: 20),
                          Text(
                            'class_field'.tr(),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                          SizedBox(height: 10),
                          DropdownButtonHideUnderline(
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButtonFormField<String>(
                                validator: (value) {
                                  if (value == null) {
                                    return 'Please select class';
                                  } else {
                                    return null;
                                  }
                                },
                                decoration: InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 2.0,
                                    ),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 2.0,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.grey,
                                      width: 2.0,
                                    ),
                                  ),
                                ),
                                isExpanded: true,
                                value: selectedClassId,
                                onChanged: (value) {
                                  setState(() {
                                    selectedClassId = value;
                                  });
                                },
                                items: state.bubbleClass.data!.map((e) {
                                  return DropdownMenuItem<String>(
                                    value: e.id,
                                    child: Text(e.className!),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                } else {
                  return Shimmer.fromColors(
                    baseColor: Colors.grey.shade300,
                    highlightColor: Colors.white,
                    child: Container(
                      width: double.infinity,
                      height: height * 0.4,
                      margin: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0.5, 0.5),
                            blurRadius: 2,
                          ),
                        ],
                      ),
                    ),
                  );
                }
              },
            ),
            Spacer(),
            Container(
              width: double.infinity,
              margin: EdgeInsets.all(15),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(backgroundColor: kPrimaryColor),
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    print('validated');
                    print(nameController.text);
                    print(selectedClassId);
                  }

                  context.read<EditBubbleCubit>().editBubble(
                        bubbleId: widget.bubbleId,
                        bubbleName: nameController.text,
                        selectedClassId: selectedClassId!,
                      );
                },
                child: BlocConsumer<EditBubbleCubit, EditBubbleState>(
                  listener: (context, state) {
                    if (state is EditBubbleLoaded) {
                      Navigator.of(context).pop();
                    }
                  },
                  builder: (context, state) {
                    if (state is EditBubbleLoading) {
                      return SpinKitFadingCircle(
                        color: Colors.white,
                        size: 14,
                      );
                    } else {
                      return Text('save'.tr());
                    }
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
