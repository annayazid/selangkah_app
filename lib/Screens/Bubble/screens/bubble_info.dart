import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:shimmer/shimmer.dart';

class BubbleInfo extends StatefulWidget {
  final String bubbleName;
  final String className;
  final String bubbleId;

  const BubbleInfo({
    super.key,
    required this.bubbleName,
    required this.className,
    required this.bubbleId,
  });

  @override
  State<BubbleInfo> createState() => _BubbleInfoState();
}

class _BubbleInfoState extends State<BubbleInfo> {
  @override
  void initState() {
    context.read<GetBubbleInfoCubit>().getBubbleInfo(widget.bubbleId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height =
        MediaQuery.of(context).size.height - AppBar().preferredSize.height;

    // double width = MediaQuery.of(context).size.width;
    return BubbleScaffold(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: height * 0.25,
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    height: height * 0.25,
                    width: double.infinity,
                    child: Image.asset(
                      'assets/images/Bubble/Bg_Large.png',
                      // height: height * 0.3,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Center(
                    child: Column(
                      children: [
                        SizedBox(height: 15),
                        Center(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: kPrimaryColor,
                            ),
                            child: Text(
                              'manage_bubble'.tr(),
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        Text(
                          widget.bubbleName.toUpperCase(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 20),
                        Text(
                          widget.className.toUpperCase(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(15),
              child: Text(
                'bubble_member_list'.tr(),
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.5),
              ),
            ),
            BlocBuilder<GetBubbleInfoCubit, GetBubbleInfoState>(
              builder: (context, state) {
                if (state is GetBubbleInfoLoaded) {
                  return GetBubbleInfoLoadedWidget(
                    bubbleInfoMember: state.bubbleInfoMember,
                    profilePic: state.profilePic,
                    bubbleId: widget.bubbleId,
                    bubbleName: widget.bubbleName,
                    className: widget.className,
                  );
                } else {
                  return ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.all(10),
                    shrinkWrap: true,
                    itemCount: 4,
                    itemBuilder: (BuildContext context, int index) {
                      return Shimmer.fromColors(
                        baseColor: Colors.grey.shade300,
                        highlightColor: Colors.white,
                        child: Container(
                          margin: EdgeInsets.all(5),
                          height: 100,
                          width: double.infinity,
                          decoration: BoxDecoration(color: Colors.white),
                        ),
                      );
                    },
                  );
                }
              },
            )
          ],
        ),
      ),
    );
  }
}

class GetBubbleInfoLoadedWidget extends StatelessWidget {
  final BubbleInfoMember bubbleInfoMember;
  final List<Uint8List?> profilePic;
  final String bubbleName;
  final String className;
  final String bubbleId;

  const GetBubbleInfoLoadedWidget(
      {super.key,
      required this.bubbleInfoMember,
      required this.profilePic,
      required this.bubbleName,
      required this.className,
      required this.bubbleId});

  @override
  Widget build(BuildContext context) {
    double height =
        MediaQuery.of(context).size.height - AppBar().preferredSize.height;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                offset: Offset(0.5, 0.5),
                blurRadius: 2,
              ),
            ],
          ),
          margin: EdgeInsets.symmetric(horizontal: 10),
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .push(
                          MaterialPageRoute(
                            builder: (context) => BlocProvider(
                              create: (context) => CheckUserCubit(),
                              child: AddMembersScreen(
                                bubbleId: bubbleId,
                                bubbleName: bubbleName,
                                className: className,
                              ),
                            ),
                          ),
                        )
                        .then((value) => context
                            .read<GetBubbleInfoCubit>()
                            .getBubbleInfo(bubbleId));
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.asset(
                      'assets/images/Bubble/Add_Member.png',
                    ),
                  ),
                ),
              ),
              SizedBox(width: 20),
              Expanded(
                flex: 4,
                child: Text(
                  'bubble_add_member'.tr(),
                  style: TextStyle(fontSize: 16),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Divider(color: Colors.grey, thickness: 2),
        ),
        ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          padding: EdgeInsets.all(5),
          shrinkWrap: true,
          itemCount: bubbleInfoMember.data!.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(0.5, 0.5),
                    blurRadius: 2,
                  ),
                ],
              ),
              child: Row(
                children: [
                  // CircleAvatar(
                  //   radius: 25,
                  //   backgroundColor: Colors.grey.shade300,
                  //   backgroundImage: profilePic[index] == null
                  //       ? AssetImage(
                  //           'assets/images/Bubble/person.png',
                  //         )
                  //       : MemoryImage(profilePic[index]!) as ImageProvider,
                  // ),
                  Container(
                    height: 50,
                    padding: profilePic[index] == null
                        ? EdgeInsets.all(5)
                        : EdgeInsets.all(0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.grey.shade300,
                    ),
                    child: ClipOval(
                      child: profilePic[index] == null
                          ? Image.asset(
                              'assets/images/Bubble/person.png',
                              fit: BoxFit.cover,
                            )
                          : Image.memory(
                              profilePic[index]!,
                              fit: BoxFit.cover,
                            ),
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          bubbleInfoMember.data![index].nickName!,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 5),
                        if (bubbleInfoMember.data![index].isAdmin! == '1')
                          Text(
                            '${bubbleInfoMember.data![index].userNo!} (Admin)',
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        if (bubbleInfoMember.data![index].isAdmin! == '0')
                          Text(
                            '${bubbleInfoMember.data![index].userNo!}',
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                      ],
                    ),
                  ),
                  SizedBox(width: 5),

                  if (bubbleInfoMember.data![index].isAdmin! == '0')
                    InkWell(
                      onTap: () {
                        Navigator.of(context)
                            .push(
                              MaterialPageRoute(
                                builder: (context) => MultiBlocProvider(
                                  providers: [
                                    BlocProvider(
                                      create: (context) => GetEditDataCubit(),
                                    ),
                                    BlocProvider(
                                      create: (context) => EditMemberCubit(),
                                    ),
                                  ],
                                  child: EditMember(
                                    bubbleInfoData:
                                        bubbleInfoMember.data![index],
                                    className: className,
                                    profilePicture: profilePic[index],
                                  ),
                                ),
                              ),
                            )
                            .then((value) => context
                                .read<GetBubbleInfoCubit>()
                                .getBubbleInfo(bubbleId));
                      },
                      child: Image(
                        image: AssetImage("assets/images/Bubble/Edit.png"),
                        width: height * 0.05,
                      ),
                    ),
                  if (bubbleInfoMember.data![index].isAdmin! == '0')
                    InkWell(
                      onTap: () {
                        Alert(
                          context: context,
                          type: AlertType.warning,
                          title: 'remove_dialog'.tr(),
                          buttons: [
                            DialogButton(
                              child: Text(
                                'cancel'.tr(),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                              onPressed: () {
                                Navigator.of(context, rootNavigator: true)
                                    .pop();
                              },
                              color: Colors.green,
                            ),
                            DialogButton(
                              child: Text(
                                'Okay',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                                // context.read<GetBubbleCubit>().deleteBubble(
                                //     state.bubbleList.data![index].id!);
                                // Navigator.of(context).pop();

                                context.read<GetBubbleInfoCubit>().deleteMember(
                                    bubbleInfoMember.data![index].id!,
                                    bubbleId);
                              },
                              color: kPrimaryColor,
                            ),
                          ],
                        ).show();
                      },
                      child: Image(
                        image: AssetImage("assets/images/Bubble/Delete.png"),
                        width: height * 0.05,
                      ),
                    ),
                ],
              ),
            );
          },
        ),
      ],
    );
  }
}
