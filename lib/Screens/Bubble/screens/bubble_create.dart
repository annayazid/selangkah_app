import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:shimmer/shimmer.dart';
import 'package:easy_localization/easy_localization.dart';

class BubbleCreate extends StatefulWidget {
  const BubbleCreate({Key? key}) : super(key: key);

  @override
  State<BubbleCreate> createState() => _BubbleCreateState();
}

class _BubbleCreateState extends State<BubbleCreate> {
  String? selectedClassId;
  TextEditingController bubbleNameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool isShowDialogMember = false;

  List<bool> isTick = [];

  @override
  void initState() {
    bubbleNameController.text = '';
    context.read<GetClassCubit>().getBubbleClass();
    super.initState();
  }

  late BubbleMember bubbleMember;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;

    return BubbleScaffold(
      child: Container(
        height: height,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 20),
              Text(
                'create_new_bubble'.tr(),
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              SizedBox(height: 20),
              Container(
                width: double.infinity,
                margin: EdgeInsets.all(10),
                // padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0.5, 0.5),
                      blurRadius: 2,
                    ),
                  ],
                ),
                child: BlocConsumer<GetClassCubit, GetClassState>(
                  listener: (context, state) {
                    if (state is GetClassLoaded) {
                      setState(() {
                        for (var i = 0;
                            i < state.bubbleMember.data!.length;
                            i++) {
                          isTick.add(false);
                        }

                        bubbleMember = state.bubbleMember;
                      });
                    }
                  },
                  builder: (context, state) {
                    if (state is GetClassLoaded) {
                      //looded widget
                      return Stack(
                        fit: StackFit.loose,
                        children: [
                          Padding(
                            padding: EdgeInsets.all(15),
                            child: Form(
                              // autovalidateMode: AutovalidateMode.,
                              key: _formKey,
                              // key: ,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(height: 10),
                                  Text(
                                    'name'.tr(),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  TextFormField(
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'name_required'.tr();
                                      } else {
                                        return null;
                                      }
                                    },
                                    decoration: InputDecoration(
                                      hintText: 'Bubble Name',
                                      fillColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                          width: 2.0,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide(
                                          color: Colors.grey,
                                          width: 2.0,
                                        ),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide(
                                          color: Colors.black,
                                          width: 2.0,
                                        ),
                                      ),
                                    ),
                                    controller: bubbleNameController,
                                  ),
                                  SizedBox(height: 20),
                                  Text(
                                    'class_field'.tr(),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  DropdownButtonHideUnderline(
                                    child: ButtonTheme(
                                      alignedDropdown: true,
                                      child: DropdownButtonFormField<String>(
                                        validator: (value) {
                                          if (value == null) {
                                            return 'Please select class';
                                          } else {
                                            return null;
                                          }
                                        },
                                        decoration: InputDecoration(
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                              color: Colors.black,
                                              width: 2.0,
                                            ),
                                          ),
                                          errorBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                              color: Colors.black,
                                              width: 2.0,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                              color: Colors.grey,
                                              width: 2.0,
                                            ),
                                          ),
                                        ),
                                        isExpanded: true,
                                        value: selectedClassId,
                                        onChanged: (value) {
                                          setState(() {
                                            selectedClassId = value;
                                          });
                                        },
                                        items: state.bubbleClass.data!.map((e) {
                                          return DropdownMenuItem<String>(
                                            value: e.id,
                                            child: Text(e.className!),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  Text(
                                    'bubble_member_list'.tr(),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Divider(color: Colors.grey, thickness: 2),
                                  SizedBox(height: 10),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              isShowDialogMember = true;
                                            });
                                          },
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: Image.asset(
                                              'assets/images/Bubble/Add_Member.png',
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 20),
                                      Expanded(
                                        flex: 4,
                                        child: Text(
                                          'bubble_add_member'.tr(),
                                          style: TextStyle(fontSize: 16),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 10),
                                  Divider(color: Colors.grey, thickness: 2),
                                  SizedBox(height: 10),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: Image.asset(
                                            'assets/images/Bubble/Bubble_Box.png',
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 20),
                                      Expanded(
                                        flex: 4,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'bubble_you'.tr(),
                                              style: TextStyle(fontSize: 16),
                                            ),
                                            Text(
                                              'ID: ${state.selId}',
                                              style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 5),
                                  ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount: bubbleMember.data!.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      if (isTick[index]) {
                                        return Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    child: Image.asset(
                                                      'assets/images/Bubble/Bubble_Box.png',
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(width: 20),
                                                Expanded(
                                                  flex: 4,
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(
                                                        bubbleMember
                                                            .data![index]
                                                            .nickName!,
                                                        style: TextStyle(
                                                            fontSize: 16),
                                                      ),
                                                      Text(
                                                        'ID: ${bubbleMember.data![index].userNo}',
                                                        style: TextStyle(
                                                          fontSize: 16,
                                                          color: Colors.grey,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 5),
                                          ],
                                        );
                                      } else {
                                        return Material();
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                          if (isShowDialogMember)
                            showDialogMember(width, state),
                        ],
                      );
                    } else {
                      //shimmer
                      return Shimmer.fromColors(
                        baseColor: Colors.grey.shade300,
                        highlightColor: Colors.white,
                        child: Container(
                          color: Colors.white,
                          height: 300,
                        ),
                      );
                    }
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                width: double.infinity,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: kPrimaryColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      print('validated');
                      print(bubbleNameController.text);
                      print(selectedClassId);
                      print(isTick);

                      context.read<CreateBubbleCubit>().createBubble(
                            bubbleName: bubbleNameController.text,
                            selectedClassId: selectedClassId!,
                            tick: isTick,
                            bubbleMember: bubbleMember,
                          );
                    } else {
                      print('not validated');
                    }
                  },
                  child: BlocConsumer<CreateBubbleCubit, CreateBubbleState>(
                    listener: (context, state) {
                      if (state is CreateBubbleLoaded) {
                        Navigator.of(context).pop();
                      }
                    },
                    builder: (context, state) {
                      if (state is CreateBubbleLoading) {
                        return SpinKitFadingCircle(
                          color: Colors.white,
                          size: 14,
                        );
                      } else {
                        return Text('bubble_create_button'.tr());
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  //dialog member
  Widget showDialogMember(double width, GetClassLoaded state) {
    return Center(
      child: Container(
        height: 450,
        margin: EdgeInsets.all(20),
        width: width * 0.8,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.black54.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 20,
              offset: Offset(0, 0), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          children: [
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                child: ListView.builder(
                  itemCount: state.bubbleMember.data!.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (state.selId != state.bubbleMember.data![index].userNo) {
                      return Column(
                        children: [
                          Row(
                            children: [
                              //logo
                              Expanded(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Container(
                                    // height: size.height * 0.07,
                                    child: Image.asset(
                                        'assets/images/Bubble/Bubble_Box.png'),
                                  ),
                                ),
                              ),
                              SizedBox(width: 10),

                              Expanded(
                                flex: 3,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(state
                                        .bubbleMember.data![index].nickName!
                                        .toUpperCase()),
                                    SizedBox(height: 5),
                                    Text(
                                      'ID : ${state.bubbleMember.data![index].userNo!}'
                                          .toUpperCase(),
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(width: 10),

                              Checkbox(
                                value: isTick[index],
                                onChanged: (value) {
                                  setState(() {
                                    isTick[index] = !isTick[index];
                                  });
                                },
                              ),
                            ],
                          ),
                          Divider(),
                        ],
                      );
                    } else {
                      return Material();
                    }
                  },
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10),
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          for (var i = 0; i < isTick.length; i++) {
                            isTick[i] = false;
                          }

                          isShowDialogMember = false;
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: kPrimaryColor,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                          ),
                          border: Border.all(
                            color: Colors.black,
                          ),
                        ),
                        child: Text(
                          'Cancel',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          isShowDialogMember = false;
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: kPrimaryColor,
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(10),
                          ),
                          border: Border.all(color: Colors.black),
                        ),
                        child: Text(
                          'Save',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

// if (_formKey.currentState!.validate())
