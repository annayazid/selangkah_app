import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

class AddMemberQr extends StatefulWidget {
  AddMemberQr({Key? key}) : super(key: key);

  @override
  State<AddMemberQr> createState() => _AddMemberQrState();
}

class _AddMemberQrState extends State<AddMemberQr> {
  MobileScannerController cameraController = MobileScannerController();
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const Text('SELANGKAH'),
        actions: [
          IconButton(
            color: Colors.white,
            icon: ValueListenableBuilder(
              valueListenable: cameraController.torchState,
              builder: (context, state, child) {
                switch (state) {
                  case TorchState.off:
                    return const Icon(Icons.flash_off, color: Colors.grey);
                  case TorchState.on:
                    return const Icon(Icons.flash_on, color: Colors.yellow);
                }
              },
            ),
            iconSize: 32.0,
            onPressed: () => cameraController.toggleTorch(),
          ),
          IconButton(
            color: Colors.white,
            icon: ValueListenableBuilder(
              valueListenable: cameraController.cameraFacingState,
              builder: (context, state, child) {
                switch (state) {
                  case CameraFacing.front:
                    return const Icon(Icons.camera_front);
                  case CameraFacing.back:
                    return const Icon(Icons.camera_rear);
                }
              },
            ),
            iconSize: 32.0,
            onPressed: () => cameraController.switchCamera(),
          ),
        ],
      ),
      body: Stack(
        children: [
          MobileScanner(
            // fit: BoxFit.contain,
            controller: cameraController,
            onDetect: (capture) {
              cameraController.stop();
              final List<Barcode> barcodes = capture.barcodes;
              // for (final barcode in barcodes) {
              //   debugPrint('Barcode found! ${barcode.rawValue}');
              // }

              Navigator.of(context).pop(barcodes[0].rawValue);
            },
          ),
          Center(
            child: Container(
              width: width * 0.6,
              height: height * 0.3,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.red,
                  width: 3,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
