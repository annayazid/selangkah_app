import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';
import 'package:selangkah_new/utils/constants.dart';

class AddMembersScreen extends StatefulWidget {
  final String bubbleName;
  final String className;
  final String bubbleId;

  const AddMembersScreen({
    super.key,
    required this.bubbleName,
    required this.className,
    required this.bubbleId,
  });

  @override
  State<AddMembersScreen> createState() => _AddMembersScreenState();
}

class _AddMembersScreenState extends State<AddMembersScreen> {
  TextEditingController selIdController = TextEditingController();

  @override
  void initState() {
    selIdController.text = '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;

    double width = MediaQuery.of(context).size.width;

    return BubbleScaffold(
      child: SingleChildScrollView(
        child: Container(
          height: height,
          child: Column(
            children: [
              Container(
                height: height * 0.25,
                width: double.infinity,
                child: Stack(
                  children: [
                    Container(
                      height: height * 0.25,
                      width: double.infinity,
                      child: Image.asset(
                        'assets/images/Bubble/Bg_Large.png',
                        // height: height * 0.3,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Center(
                      child: Column(
                        children: [
                          SizedBox(height: 15),
                          Center(
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: kPrimaryColor,
                              ),
                              child: Text(
                                'bubble_add_member'.tr(),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Text(
                            widget.bubbleName.toUpperCase(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 20),
                          Text(
                            widget.className.toUpperCase(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Expanded(
                flex: 4,
                child: Container(
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        offset: Offset(0.5, 0.5),
                        blurRadius: 2,
                      ),
                    ],
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Selangkah ID',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                      Expanded(
                        child: Center(
                          child: TextField(
                            decoration: InputDecoration(
                              hintText: 'selangkahID_bubble'.tr(),
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 2.0,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                  width: 2.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 2.0,
                                ),
                              ),
                            ),
                            controller: selIdController,
                          ),
                        ),
                      ),
                      Center(
                        child: Container(
                          width: width * 0.3,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: kPrimaryColor),
                            onPressed: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              // Navigator.of(context).

                              if (selIdController.text != '') {
                                //process

                                context
                                    .read<CheckUserCubit>()
                                    .checkUser(selIdController.text);
                              } else {
                                Fluttertoast.showToast(
                                  gravity: ToastGravity.BOTTOM,
                                  toastLength: Toast.LENGTH_LONG,
                                  msg: 'selangkahid_required'.tr(),
                                );
                              }
                            },
                            child: BlocConsumer<CheckUserCubit, CheckUserState>(
                              listener: (context, state) {
                                if (state is CheckUserNotExist) {
                                  Fluttertoast.showToast(
                                      msg: 'User doesn\'t exist');
                                }

                                if (state is CheckUserVerified) {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) => BlocProvider(
                                        create: (context) => AddMemberCubit(),
                                        child: ConfirmAddMember(
                                          userProfile: state.userProfile,
                                          bubbleId: widget.bubbleId,
                                        ),
                                      ),
                                    ),
                                  );
                                }
                              },
                              builder: (context, state) {
                                if (state is CheckUserLoading) {
                                  return SpinKitFadingCircle(
                                    color: Colors.white,
                                    size: 14,
                                  );
                                } else {
                                  return Text('bubble_create_button'.tr());
                                }
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  children: [
                    Expanded(
                      child: Divider(
                        color: Color(0xFF505660),
                        height: 20,
                        thickness: 1,
                        // indent: 20,
                        // endIndent: 20,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Container(
                        child: Text(
                          'or'.tr(),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Divider(
                        color: Color(0xFF505660),
                        height: 20,
                        thickness: 1,
                        // indent: 20,
                        // endIndent: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 3,
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .push(
                          MaterialPageRoute(
                            builder: (context) => AddMemberQr(),
                          ),
                        )
                        .then((value) =>
                            context.read<CheckUserCubit>().checkUser(value));
                  },
                  child: Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(15),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.5, 0.5),
                          blurRadius: 2,
                        ),
                      ],
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Expanded(
                          child: Image.asset(
                            'assets/images/Bubble/Grey_QR.png',
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          'bubble_qr_scan'.tr(),
                          style: TextStyle(
                            color: Color(0xFFF5AE08),
                            fontSize: 16,
                          ),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Row(
                  children: [
                    Expanded(
                      child: Divider(
                        color: Color(0xFF505660),
                        height: 20,
                        thickness: 1,
                        // indent: 20,
                        // endIndent: 20,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Container(
                        child: Text(
                          'or'.tr(),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Divider(
                        color: Color(0xFF505660),
                        height: 20,
                        thickness: 1,
                        // indent: 20,
                        // endIndent: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                width: double.infinity,
                child: ElevatedButton(
                  style:
                      ElevatedButton.styleFrom(backgroundColor: kPrimaryColor),
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => BlocProvider(
                          create: (context) => GetRelationCubit(),
                          child: AddMemberRegister(
                            bubbleId: widget.bubbleId,
                            bubbleName: widget.bubbleName,
                            className: widget.className,
                          ),
                        ),
                      ),
                    );
                  },
                  child: Text(
                    'bubble_register'.tr(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
