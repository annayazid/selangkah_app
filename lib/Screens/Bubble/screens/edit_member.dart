import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shimmer/shimmer.dart';

import 'package:selangkah_new/Screens/Bubble/bubble.dart';
import 'package:selangkah_new/utils/constants.dart';

class EditMember extends StatefulWidget {
  final BubbleInfoData bubbleInfoData;
  final String className;
  final Uint8List? profilePicture;

  const EditMember({
    super.key,
    required this.bubbleInfoData,
    required this.className,
    this.profilePicture,
  });

  @override
  State<EditMember> createState() => _EditMemberState();
}

class _EditMemberState extends State<EditMember> {
  TextEditingController nameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String? selectedRole;
  final List<String> roleList = ['Member', 'Admin'];
  String? selectedRelationId;
  int radioButton = 1;
  int dependent = 1;

  @override
  void initState() {
    context.read<GetEditDataCubit>().getEditData(
          bubbleInfoData: widget.bubbleInfoData,
          className: widget.className,
        );
    nameController.text = widget.bubbleInfoData.nickName!;
    selectedRole = widget.bubbleInfoData.isAdmin! == '0' ? 'Member' : 'Admin';

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;

    return BubbleScaffold(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: height * 0.325,
                  width: double.infinity,
                  child: Image.asset(
                    'assets/images/Bubble/Bg_Large.png',
                    // height: height * 0.3,
                    fit: BoxFit.fill,
                  ),
                ),
                Center(
                  child: Column(
                    children: [
                      SizedBox(height: height * 0.175),
                      BlocConsumer<GetEditDataCubit, GetEditDataState>(
                        listener: (context, state) {
                          if (state is GetEditDataLoaded) {
                            print(
                                'id relation is ${widget.bubbleInfoData.idBubblePersonalRel}');
                            if (state.bubbleRelation.data!.any((element) =>
                                element.id ==
                                widget.bubbleInfoData.idBubblePersonalRel)) {
                              // print()
                              setState(() {
                                selectedRelationId =
                                    widget.bubbleInfoData.idBubblePersonalRel;
                              });
                            }
                          }
                        },
                        builder: (context, state) {
                          if (state is GetEditDataLoaded) {
                            return Container(
                              margin: EdgeInsets.all(15),
                              padding: EdgeInsets.all(20),
                              width: double.infinity,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(0.5, 0.5),
                                    blurRadius: 2,
                                  ),
                                ],
                              ),
                              child: Column(
                                children: [
                                  SizedBox(height: height * 0.10),
                                  Form(
                                    key: _formKey,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'name'.tr(),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                          ),
                                        ),
                                        SizedBox(height: 10),
                                        TextFormField(
                                          validator: (value) {
                                            if (value!.isEmpty) {
                                              return 'name_required'.tr();
                                            } else {
                                              return null;
                                            }
                                          },
                                          decoration: InputDecoration(
                                            fillColor: Colors.white,
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                color: Colors.black,
                                                width: 2.0,
                                              ),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                color: Colors.grey,
                                                width: 2.0,
                                              ),
                                            ),
                                            errorBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                color: Colors.black,
                                                width: 2.0,
                                              ),
                                            ),
                                          ),
                                          controller: nameController,
                                        ),
                                        SizedBox(height: 15),
                                        Text(
                                          'role'.tr(),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                          ),
                                        ),
                                        SizedBox(height: 10),
                                        DropdownButtonHideUnderline(
                                          child: ButtonTheme(
                                            alignedDropdown: true,
                                            child:
                                                DropdownButtonFormField<String>(
                                              validator: (value) {
                                                if (value == null) {
                                                  return 'Please select role';
                                                } else {
                                                  return null;
                                                }
                                              },
                                              decoration: InputDecoration(
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  borderSide: BorderSide(
                                                    color: Colors.black,
                                                    width: 2.0,
                                                  ),
                                                ),
                                                errorBorder: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  borderSide: BorderSide(
                                                    color: Colors.black,
                                                    width: 2.0,
                                                  ),
                                                ),
                                                enabledBorder:
                                                    OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey,
                                                    width: 2.0,
                                                  ),
                                                ),
                                              ),
                                              isExpanded: true,
                                              value: selectedRole,
                                              onChanged: (value) {
                                                setState(() {
                                                  selectedRole = value;
                                                });
                                              },
                                              items: roleList.map((e) {
                                                return DropdownMenuItem<String>(
                                                  value: e,
                                                  child: Text(e),
                                                );
                                              }).toList(),
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 15),
                                        if (state.boolCanEditDependent) ...[
                                          Text(
                                            'personal_relation'.tr(),
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                            ),
                                          ),
                                          SizedBox(height: 10),
                                          DropdownButtonHideUnderline(
                                            child: ButtonTheme(
                                              alignedDropdown: true,
                                              child: DropdownButtonFormField<
                                                  String>(
                                                validator: (value) {
                                                  if (value == null) {
                                                    return 'Please select relation';
                                                  } else {
                                                    return null;
                                                  }
                                                },
                                                decoration: InputDecoration(
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    borderSide: BorderSide(
                                                      color: Colors.black,
                                                      width: 2.0,
                                                    ),
                                                  ),
                                                  errorBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    borderSide: BorderSide(
                                                      color: Colors.black,
                                                      width: 2.0,
                                                    ),
                                                  ),
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    borderSide: BorderSide(
                                                      color: Colors.grey,
                                                      width: 2.0,
                                                    ),
                                                  ),
                                                ),
                                                isExpanded: true,
                                                value: selectedRelationId,
                                                onChanged: (value) {
                                                  setState(() {
                                                    selectedRelationId = value;
                                                  });
                                                },
                                                items: state
                                                    .bubbleRelation.data!
                                                    .map((e) {
                                                  return DropdownMenuItem<
                                                      String>(
                                                    value: e.id,
                                                    child: Text(e.relName!),
                                                  );
                                                }).toList(),
                                              ),
                                            ),
                                          ),
                                          SizedBox(height: 10),
                                          Row(
                                            children: [
                                              Radio(
                                                  activeColor: kPrimaryColor,
                                                  value: 1,
                                                  groupValue: radioButton,
                                                  onChanged: (val) {
                                                    setState(() {
                                                      radioButton = val!;
                                                      dependent = 1;
                                                      // print('hooo $val');
                                                    });
                                                  }),
                                              Text('dependent'.tr()),
                                              Radio(
                                                  activeColor: kPrimaryColor,
                                                  value: 2,
                                                  groupValue: radioButton,
                                                  onChanged: (val) {
                                                    setState(() {
                                                      radioButton = val!;
                                                      dependent = 0;
                                                      // print('hooo $val');
                                                    });
                                                  }),
                                              Text('not_dependent'.tr())
                                            ],
                                          ),
                                        ],
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            );
                          } else {
                            return Shimmer.fromColors(
                              baseColor: Colors.grey.shade300,
                              highlightColor: Colors.white,
                              child: Container(
                                margin: EdgeInsets.all(15),
                                padding: EdgeInsets.all(20),
                                width: double.infinity,
                                height: 500,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey,
                                      offset: Offset(0.5, 0.5),
                                      blurRadius: 2,
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }
                        },
                      )
                    ],
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 15),
                    Center(
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: kPrimaryColor,
                        ),
                        child: Text(
                          'edit_member_info'.tr(),
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),

                    BlocBuilder<GetEditDataCubit, GetEditDataState>(
                      builder: (context, state) {
                        if (state is GetEditDataLoaded) {
                          if (widget.profilePicture == null) {
                            return Container(
                              padding: EdgeInsets.all(20),
                              child: Icon(
                                FontAwesomeIcons.user,
                                size: 80,
                              ),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle,
                              ),
                            );
                          } else {
                            return Center(
                              child: CircleAvatar(
                                radius: 65,
                                backgroundColor: Colors.white,
                                child: CircleAvatar(
                                  backgroundColor: Colors.white,
                                  radius: 60,
                                  backgroundImage:
                                      MemoryImage(widget.profilePicture!),
                                ),
                              ),
                            );
                          }
                          // if()
                        } else {
                          return Container();
                        }
                      },
                    )

                    //bloc kat sini jugak

                    // userProfile.profilePic == null ||
                    //         userProfile.profilePic == ''
                    //     ? Container(
                    //         padding: EdgeInsets.all(20),
                    //         child: Icon(
                    //           FontAwesomeIcons.user,
                    //           size: 80,
                    //         ),
                    //         decoration: BoxDecoration(
                    //           color: Colors.white,
                    //           shape: BoxShape.circle,
                    //         ),
                    //       )
                    //     : Center(
                    //         child: CircleAvatar(
                    //           radius: 65,
                    //           backgroundColor: Colors.white,
                    //           child: CircleAvatar(
                    //             backgroundColor: Colors.white,
                    //             radius: 60,
                    //             backgroundImage: () {
                    //               if (userProfile.profilePic == null ||
                    //                   userProfile.profilePic == '') {
                    //                 return AssetImage(
                    //                     'assets/images/Bubble/person.png');
                    //               } else {
                    //                 Uint8List imgbytes = base64
                    //                     .decode(userProfile.profilePic!);
                    //                 return MemoryImage(imgbytes);
                    //               }
                    //             }() as ImageProvider,
                    //           ),
                    //         ),
                    //       ),
                  ],
                ),
              ],
            ),
            // SizedBox(height: 10),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15),
              width: double.infinity,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(backgroundColor: kPrimaryColor),
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    print(nameController.text);
                    print(selectedRole);
                    print(selectedRelationId);
                    print(dependent);

                    context.read<EditMemberCubit>().editMember(
                          bubbleInfoData: widget.bubbleInfoData,
                          relationId: selectedRelationId!,
                          isAdmin: selectedRole == 'Admin' ? '1' : '0',
                          isDependent: '$dependent',
                          name: nameController.text,
                        );
                  }
                },
                child: BlocConsumer<EditMemberCubit, EditMemberState>(
                  listener: (context, state) {
                    if (state is EditMemberLoaded) {
                      Navigator.of(context).pop();
                    }
                  },
                  builder: (context, state) {
                    if (state is EditMemberLoading) {
                      return SpinKitFadingCircle(
                        size: 14,
                        color: Colors.white,
                      );
                    } else {
                      return Text('save'.tr());
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
