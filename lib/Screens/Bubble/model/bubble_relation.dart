// To parse this JSON data, do
//
//     final bubbleRelation = bubbleRelationFromJson(jsonString);

import 'dart:convert';

BubbleRelation bubbleRelationFromJson(String str) =>
    BubbleRelation.fromJson(json.decode(str));

String bubbleRelationToJson(BubbleRelation data) => json.encode(data.toJson());

class BubbleRelation {
  BubbleRelation({
    this.code,
    this.data,
  });

  int? code;
  List<BubbleRelationData>? data;

  factory BubbleRelation.fromJson(Map<String, dynamic> json) => BubbleRelation(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<BubbleRelationData>.from(
                json["Data"]!.map((x) => BubbleRelationData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class BubbleRelationData {
  BubbleRelationData({
    this.id,
    this.relName,
  });

  String? id;
  String? relName;

  factory BubbleRelationData.fromJson(Map<String, dynamic> json) =>
      BubbleRelationData(
        id: json["id"],
        relName: json["rel_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "rel_name": relName,
      };
}
