// To parse this JSON data, do
//
//     final bubbleClass = bubbleClassFromJson(jsonString);

import 'dart:convert';

BubbleClass bubbleClassFromJson(String str) =>
    BubbleClass.fromJson(json.decode(str));

String bubbleClassToJson(BubbleClass data) => json.encode(data.toJson());

class BubbleClass {
  BubbleClass({
    this.code,
    this.data,
  });

  int? code;
  List<BubbleClassData>? data;

  factory BubbleClass.fromJson(Map<String, dynamic> json) => BubbleClass(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<BubbleClassData>.from(
                json["Data"]!.map((x) => BubbleClassData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class BubbleClassData {
  BubbleClassData({
    this.id,
    this.className,
  });

  String? id;
  String? className;

  factory BubbleClassData.fromJson(Map<String, dynamic> json) =>
      BubbleClassData(
        id: json["id"],
        className: json["class_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "class_name": className,
      };
}
