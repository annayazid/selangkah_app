// To parse this JSON data, do
//
//     final bubbleInfoMember = bubbleInfoMemberFromJson(jsonString);

import 'dart:convert';

BubbleInfoMember bubbleInfoMemberFromJson(String str) =>
    BubbleInfoMember.fromJson(json.decode(str));

String bubbleInfoMemberToJson(BubbleInfoMember data) =>
    json.encode(data.toJson());

class BubbleInfoMember {
  BubbleInfoMember({
    this.code,
    this.data,
  });

  int? code;
  List<BubbleInfoData>? data;

  factory BubbleInfoMember.fromJson(Map<String, dynamic> json) =>
      BubbleInfoMember(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<BubbleInfoData>.from(
                json["Data"]!.map((x) => BubbleInfoData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class BubbleInfoData {
  BubbleInfoData({
    this.id,
    this.nickName,
    this.isAdmin,
    this.idSid,
    this.idBubblePersonalRel,
    this.isDependent,
    this.profilePic,
    this.userNo,
    this.phonenumber,
  });

  String? id;
  String? nickName;
  String? isAdmin;
  String? idSid;
  String? idBubblePersonalRel;
  String? isDependent;
  String? profilePic;
  String? userNo;
  String? phonenumber;

  factory BubbleInfoData.fromJson(Map<String, dynamic> json) => BubbleInfoData(
        id: json["id"],
        nickName: json["nick_name"],
        isAdmin: json["is_admin"],
        idSid: json["id_sid"],
        idBubblePersonalRel: json["id_bubble_personal_rel"],
        isDependent: json["is_dependent"],
        profilePic: json["profile_pic"],
        userNo: json["user_no"],
        phonenumber: json["phonenumber"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nick_name": nickName,
        "is_admin": isAdmin,
        "id_sid": idSid,
        "id_bubble_personal_rel": idBubblePersonalRel,
        "is_dependent": isDependent,
        "profile_pic": profilePic,
        "user_no": userNo,
        "phonenumber": phonenumber,
      };
}
