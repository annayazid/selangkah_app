// To parse this JSON data, do
//
//     final bubbleList = bubbleListFromJson(jsonString);

import 'dart:convert';

BubbleList bubbleListFromJson(String str) =>
    BubbleList.fromJson(json.decode(str));

String bubbleListToJson(BubbleList data) => json.encode(data.toJson());

class BubbleList {
  BubbleList({
    this.code,
    this.data,
  });

  int? code;
  List<Datum>? data;

  factory BubbleList.fromJson(Map<String, dynamic> json) => BubbleList(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<Datum>.from(json["Data"]!.map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.bubbleName,
    this.idBubbleClass,
    this.isAdmin,
    this.className,
  });

  String? id;
  String? bubbleName;
  String? idBubbleClass;
  String? isAdmin;
  String? className;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        bubbleName: json["bubble_name"],
        idBubbleClass: json["id_bubble_class"],
        isAdmin: json["is_admin"],
        className: json["class_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "bubble_name": bubbleName,
        "id_bubble_class": idBubbleClass,
        "is_admin": isAdmin,
        "class_name": className,
      };
}
