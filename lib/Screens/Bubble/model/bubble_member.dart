// To parse this JSON data, do
//
//     final bubbleMember = bubbleMemberFromJson(jsonString);

import 'dart:convert';

BubbleMember bubbleMemberFromJson(String str) =>
    BubbleMember.fromJson(json.decode(str));

String bubbleMemberToJson(BubbleMember data) => json.encode(data.toJson());

class BubbleMember {
  BubbleMember({
    this.code,
    this.data,
  });

  int? code;
  List<BubbleMemberData>? data;

  factory BubbleMember.fromJson(Map<String, dynamic> json) => BubbleMember(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<BubbleMemberData>.from(
                json["Data"]!.map((x) => BubbleMemberData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class BubbleMemberData {
  BubbleMemberData({
    this.idSid,
    this.userNo,
    this.nickName,
  });

  String? idSid;
  String? userNo;
  String? nickName;

  factory BubbleMemberData.fromJson(Map<String, dynamic> json) =>
      BubbleMemberData(
        idSid: json["id_sid"],
        userNo: json["user_no"],
        nickName: json["nick_name"],
      );

  Map<String, dynamic> toJson() => {
        "id_sid": idSid,
        "user_no": userNo,
        "nick_name": nickName,
      };
}
