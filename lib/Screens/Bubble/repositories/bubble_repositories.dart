import 'dart:convert';

import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Bubble/bubble.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:http/http.dart' as http;
import 'package:selangkah_new/utils/secure_storage.dart';

class BubbleRepositories {
  static Future<BubbleList> getBubbleList() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    log('calling calling my bubble');
    final response = await http.post(
      Uri.parse('$API_URL/list_my_bubble'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    log(response.body);

    BubbleList bubbleList = bubbleListFromJson(response.body);
    return bubbleList;
  }

  static Future<BubbleClass> getBubbleClass() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    log('calling list bubble class');
    final response = await http.post(
      Uri.parse('$API_URL/list_bubble_class'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    log(response.body);

    BubbleClass bubbleClass = bubbleClassFromJson(response.body);
    return bubbleClass;
  }

  static Future<BubbleMember> getBubbleMember() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    log('calling calling list bubble member');
    final response = await http.post(
      Uri.parse('$API_URL/list_all_my_bubble_members'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    log(response.body);

    BubbleMember bubbleMember = bubbleMemberFromJson(response.body);
    return bubbleMember;
  }

  static Future<void> createBubble({
    required String bubbleName,
    required String selectedClassId,
    required List<bool> tick,
    required BubbleMember bubbleMember,
  }) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'bubble_name': bubbleName,
      'id_sid': id,
      'id_bubble_class': selectedClassId,
    };

    print(map);

    var response = await http.post(
      Uri.parse('$API_URL/create_bubble'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    var data = json.decode(response.body);

    String bubbleId = data['Data'];

    await addMembers(bubbleId: bubbleId);

    print(tick);

    if (tick.every((element) => !element)) {
      //end
    } else {
      String idList = '';

      for (int i = 0; i < bubbleMember.data!.length; i++) {
        if (tick[i]) {
          idList += '${bubbleMember.data![i].idSid},';
        }
      }

      idList = idList.substring(0, idList.length - 1);

      print(idList);

      addMembersMultiple(bubbleId: bubbleId, combinedId: idList);
    }
  }

  static Future<void> addMembers({required String bubbleId}) async {
    String id = await SecureStorage().readSecureData('userId');
    String userName = await SecureStorage().readSecureData('userName');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_bubble_hdr': bubbleId,
      'id_bubble_personal_rel': '0',
      'id_sid': id,
      'is_admin': '1',
      'is_dependent': '1',
      'nick_name': userName,
      'create_sid': '0',
    };

    print(map);

    final responseSo = await http.post(
      Uri.parse('$API_URL/create_bubble_member'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(responseSo.body);
  }

  static Future<void> addMembersMultiple({
    required String bubbleId,
    required String combinedId,
  }) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'id_bubble_hdr': bubbleId,
      'id_sid_list': combinedId,
    };

    print(map);

    final responseSo = await http.post(
      Uri.parse('$API_URL/create_bubble_member_multiple'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );
    print(responseSo.body);
  }

  static Future<BubbleInfoMember> getBubbleInfo(String bubbleId) async {
    final Map<String, String> map = {
      'id_bubble_hdr': bubbleId,
      'token': TOKEN,
    };

    print(map);

    log('calling list bubble info');
    final response = await http.post(
      Uri.parse('$API_URL/list_my_bubble_members'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    BubbleInfoMember bubbleInfoMember = bubbleInfoMemberFromJson(response.body);
    return bubbleInfoMember;
  }

  static Future<void> editBubble({
    required String bubbleId,
    required String bubbleName,
    required String selectedClassId,
  }) async {
    final Map<String, String> map = {
      'id_bubble_hdr': bubbleId,
      'id_bubble_class': selectedClassId,
      'bubble_name': bubbleName,
      'token': TOKEN,
    };
    print(map);

    log('calling edit bubble');
    final responseSo = await http.post(
      Uri.parse('$API_URL/update_bubble'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(responseSo.body);
  }

  static Future<void> deleteBubble(String bubbleId) async {
    final Map<String, String> map = {
      'id_bubble_hdr': bubbleId,
      'token': TOKEN,
    };

    print('calling delete bubble');

    final responseSo = await http.post(
      Uri.parse('$API_URL/del_bubble'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(responseSo.body);
  }

  static Future<String> checkUserReg(String selId) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'user_no': selId,
    };

    print(map);

    final response = await http.post(
      Uri.parse('$API_URL/check_user_reg'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
    var data = jsonDecode(response.body);

    if (data['Data'] != false) {
      return data['Data'];
    } else {
      return '';
    }
  }

  static Future<UserProfile> getProfile(String id) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'id': id,
      // 'id': '82986',
    };

    final response = await http.post(
      Uri.parse('$API_URL/check_user_profile'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    // log(response.body);
    print(response.body
        .substring(response.body.length - 50, response.body.length));

    RawProfile? rawProfile = rawProfileFromJson(response.body);

    UserProfile? userProfile = UserProfile(
      id: rawProfile!.data![0],
      selId: rawProfile.data![1],
      name: rawProfile.data![2],
      token: rawProfile.data![3],
      idNo: rawProfile.data![4],
      language: rawProfile.data![5],
      citizen: rawProfile.data![6],
      gender: rawProfile.data![7],
      dob: rawProfile.data![8],
      address1: rawProfile.data![9],
      address2: rawProfile.data![10],
      postcode: rawProfile.data![11],
      city: rawProfile.data![12],
      state: rawProfile.data![13],
      country: rawProfile.data![14],
      email: rawProfile.data![15],
      race: rawProfile.data![16],
      idType: rawProfile.data![17],
      profilePic: rawProfile.data![18],
      phoneNo: rawProfile.data![19],
      ekycStatus: rawProfile.data![20],
    );

    return userProfile;
  }

  static Future<void> addMemberUsingId(
      String bubbleId, UserProfile userProfile) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'id_bubble_hdr': bubbleId,
      'id_bubble_personal_rel': '0',
      'id_sid': userProfile.id!,
      'is_admin': '0',
      'is_dependent': '0',
      'nick_name': userProfile.name!,
      'create_sid': '0'
    };

    print(map);

    print('calling add member bubble');

    final responseSo = await http.post(
      Uri.parse('$API_URL/create_bubble_member'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(responseSo.body);
  }

  static Future<BubbleRelation> getRelation() async {
    String id = await SecureStorage().readSecureData('userId');

    Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
    };

    print(map);

    print('caliing bubble relation');

    var response = await http.post(
      Uri.parse('$API_URL/list_bubble_pr'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    BubbleRelation bubbleRelation = bubbleRelationFromJson(response.body);
    return bubbleRelation;
  }

  static Future<void> addMemberRegister({
    required String bubbleId,
    required String relationId,
    required String isAdmin,
    required String name,
    required String isDependent,
  }) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'id_bubble_hdr': bubbleId,
      'id_bubble_personal_rel': relationId,
      'id_sid': '0',
      'is_admin': isAdmin,
      'is_dependent': isDependent,
      'nick_name': name,
      'create_sid': '1'
    };

    print(map);

    print('calling add member bubble');

    final responseSo = await http.post(
      Uri.parse('$API_URL/create_bubble_member'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(responseSo.body);
  }

  static Future<void> editMember({
    required String memberUserId,
    required String relationId,
    required String isAdmin,
    required String isDependent,
    required String name,
  }) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'id_bubble_dtl': memberUserId,
      'id_bubble_personal_rel': relationId,
      'is_admin': isAdmin,
      'is_dependent': isDependent,
      'nick_name': name,
    };

    print(map);

    print('calling edit member bubble');

    final responseSo = await http.post(
      Uri.parse('$API_URL/update_bubble_member'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(responseSo.body);
  }

  static Future<void> deleteMember(String id) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'id_bubble_dtl': id,
    };

    print(map);

    print('calling delete member');

    final responseSo = await http.post(
      Uri.parse('$API_URL/del_bubble_member'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(responseSo.body);
  }
}
