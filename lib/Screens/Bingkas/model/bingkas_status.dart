// To parse this JSON data, do
//
//     final bingkasCheckStatus = bingkasCheckStatusFromJson(jsonString);

import 'dart:convert';

BingkasCheckStatus bingkasCheckStatusFromJson(String str) =>
    BingkasCheckStatus.fromJson(json.decode(str));

String bingkasCheckStatusToJson(BingkasCheckStatus data) =>
    json.encode(data.toJson());

class BingkasCheckStatus {
  BingkasCheckStatus({
    this.code,
    this.data,
  });

  int? code;
  List<Datum>? data;

  factory BingkasCheckStatus.fromJson(Map<String, dynamic> json) =>
      BingkasCheckStatus(
        code: json["code"] == null ? null : json["code"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "code": code == null ? null : code,
        "data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.status,
    this.commentUserFeedback,
  });

  int? status;
  String? commentUserFeedback;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        status: json["status"] == null ? null : json["status"],
        commentUserFeedback: json["comment_user_feedback"] == null
            ? null
            : json["comment_user_feedback"],
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "comment_user_feedback":
            commentUserFeedback == null ? null : commentUserFeedback,
      };
}
