import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:selangkah_new/Screens/Bingkas/bingkas.dart';
import 'package:selangkah_new/Screens/VerifyProfile/verify_profile.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class BingkasScreen extends StatefulWidget {
  const BingkasScreen({Key? key}) : super(key: key);

  @override
  _BingkasScreenState createState() => _BingkasScreenState();
}

class _BingkasScreenState extends State<BingkasScreen> {
  @override
  void initState() {
    context.read<ProfileCubit>().checkProfile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BINGKAS'),
      ),
      body: BlocConsumer<ProfileCubit, ProfileState>(
        listener: (context, state) async {
          if (state is ProfileIncomplete) {
            Fluttertoast.showToast(
              msg: "updateProfile".tr(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
            );

            //Redirect edit profile
            Navigator.of(context)
                .push(
              MaterialPageRoute(
                builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => GetUserDetailsCubit(),
                    ),
                    BlocProvider(
                      create: (context) => UpdateDetailCubit(),
                    ),
                  ],
                  child: VerifyProfile(),
                ),
              ),
            )
                .then((value) async {
              await Future.delayed(Duration(seconds: 1));
              String? email = await SecureStorage().readSecureData('email');

              if (email == null || email == 'null') {
                Navigator.of(context).pop();
              } else {
                context.read<ProfileCubit>().checkProfile();
              }
            });
          }
        },
        builder: (context, state) {
          if (state is ProfileComplete) {
            return ProfileCompleteWidget();
          } else {
            return Center(
              child: SpinKitFadingCircle(
                color: kPrimaryColor,
                size: 30,
              ),
            );
          }
        },
      ),
    );
  }
}

class ProfileCompleteWidget extends StatefulWidget {
  const ProfileCompleteWidget({Key? key}) : super(key: key);

  @override
  _ProfileCompleteWidgetState createState() => _ProfileCompleteWidgetState();
}

class _ProfileCompleteWidgetState extends State<ProfileCompleteWidget> {
  @override
  void initState() {
    context.read<BingkasStatusCubit>().getStatus();
    super.initState();
  }

  WebViewController? webViewCon;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BingkasStatusCubit, BingkasStatusState>(
      listener: (context, state) {
        print('state is $state');
        if (state is BingkasStatusRegistered) {}

        if (state is BingkasStatusEwallet) {}

        if (state is BingkasStatusNotRegistered) {
          // launch(state.url);

          launchUrl(
            Uri.parse(state.url),
            mode: LaunchMode.externalApplication,
          );

          Navigator.of(context).pop();
        }
      },
      builder: (context, state) {
        print('state is $state');
        if (state is BingkasStatusNotRegistered) {
          return Center(
            child: SpinKitFadingCircle(
              color: kPrimaryColor,
              size: 30,
            ),
          );
        } else {
          return Center(
            child: SpinKitFadingCircle(
              color: kPrimaryColor,
              size: 30,
            ),
          );
        }
      },
    );
  }
}
