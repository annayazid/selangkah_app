import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Bingkas/bingkas.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class BingkasRepositories {
  static Future<RawProfile> getProfile() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id': id,
      'token': TOKEN,
    };

    //print('calling post GET_PROFILE');

    final response = await http.post(
      Uri.parse('$API_URL/check_user_profile'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    RawProfile? profile = rawProfileFromJson(response.body);

    return profile!;
  }

  static Future<String> getToken() async {
    Map map = {
      "email": EMAIL,
      "password": PASSWORD,
    };

    String body = json.encode(map);

    print('calling post get token');

    final response = await http.post(
      Uri.parse(AUTH),
      headers: {'Content-Type': 'application/json'},
      body: body,
    );

    var data = json.decode(response.body);

    String token = data['data'];
    print(token);

    return token;
  }

  static Future<String> getStatus(String token) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
    };

    var body = json.encode(map);

    //print('calling post GET_PROFILE');

    final response = await http.post(
      Uri.parse('$BINGKAS/check_registered_bingkas_app'),
      // headers: {'Content-Type': 'application/json'},
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer $token',
      },
      body: body,
    );

    log(response.body);

    BingkasCheckStatus profile = bingkasCheckStatusFromJson(response.body);

    return profile.data![0].status.toString();
  }

  static Future<String> getWalletStatus(String token) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
    };

    var body = json.encode(map);

    //print('calling post GET_PROFILE');

    final response = await http.post(
      Uri.parse('$BINGKAS/bingkas_wallet_status'),
      headers: {'Content-Type': 'application/json'},
      body: body,
    );

    print(response.body);

    var data = json.decode(response.body);

    String walletStatus = data['data'].toString();

    return walletStatus;
  }
}

class BingkasGlobalVar {
  static bool firstTime = false;
}
