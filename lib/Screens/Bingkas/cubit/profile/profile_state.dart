part of 'profile_cubit.dart';

@immutable
abstract class ProfileState {
  const ProfileState();
}

class ProfileInitial extends ProfileState {
  const ProfileInitial();
}

class ProfileLoading extends ProfileState {
  const ProfileLoading();
}

class ProfileIncomplete extends ProfileState {
  const ProfileIncomplete();
}

class ProfileComplete extends ProfileState {
  const ProfileComplete();
}
