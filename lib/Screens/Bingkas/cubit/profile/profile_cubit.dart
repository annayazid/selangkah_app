import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Bingkas/bingkas.dart';

part 'profile_state.dart';

class ProfileCubit extends Cubit<ProfileState> {
  ProfileCubit() : super(ProfileInitial());

  Future<void> checkProfile() async {
    emit(ProfileLoading());

    print('calling');

    RawProfile profile = await BingkasRepositories.getProfile();

    String? name = profile.data![2];
    String? idCard = profile.data![4];
    String? idType = profile.data![17];
    String? citizenship = profile.data![6];
    String? gender = profile.data![7];
    String? dob = profile.data![8];
    String? email = profile.data![15];
    String? phone = profile.data![19];
    String? address1 = profile.data![9];
    String? postcode = profile.data![11];
    String? city = profile.data![12];
    String? state = profile.data![13];
    String? country = profile.data![14];

    if (name == null ||
        idCard == null ||
        idType == null ||
        citizenship == null ||
        gender == null ||
        dob == null ||
        email == null ||
        phone == null ||
        address1 == null ||
        postcode == null ||
        city == null ||
        state == null ||
        country == null) {
      print('incomplete');
      // yield ProfileIncomplete();
      emit(ProfileIncomplete());
    } else {
      print('complete');
      emit(ProfileComplete());
    }
  }
}
