part of 'bingkas_status_cubit.dart';

@immutable
abstract class BingkasStatusState {
  const BingkasStatusState();
}

class BingkasStatusInitial extends BingkasStatusState {
  const BingkasStatusInitial();
}

class BingkasStatusLoading extends BingkasStatusState {
  const BingkasStatusLoading();
}

class BingkasStatusRegistered extends BingkasStatusState {
  const BingkasStatusRegistered();
}

class BingkasStatusEwallet extends BingkasStatusState {
  const BingkasStatusEwallet();
}

class BingkasStatusNotRegistered extends BingkasStatusState {
  final String url;

  BingkasStatusNotRegistered({required this.url});

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is BingkasStatusNotRegistered && other.url == url;
  }

  @override
  int get hashCode => url.hashCode;
}
