import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Bingkas/bingkas.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

part 'bingkas_status_state.dart';

class BingkasStatusCubit extends Cubit<BingkasStatusState> {
  BingkasStatusCubit() : super(BingkasStatusInitial());

  Future<void> getStatus() async {
    emit(BingkasStatusLoading());

    //process

    //get token
    String token = await BingkasRepositories.getToken();

    //call register
    String status = await BingkasRepositories.getStatus(token);

    //call wallet status
    String walletStatus = await BingkasRepositories.getWalletStatus(token);

    if (status == '5' || status == '7') {
      if (walletStatus == '1') {
        emit(BingkasStatusRegistered());
      } else {
        //masuk ewallet

        //if not active yet
        //redirect to register

        // emit(BingkasStatusEwallet());

        //if active
        //redirect to bingkas

        String id = await SecureStorage().readSecureData('userId');
        String selId = await SecureStorage().readSecureData('userSelId');

        Map<String, String> queryParams = {
          'user_token': token,
          'id_selangkah_user': id,
          'selangkah_id': selId,
        };

        String queryString = Uri(queryParameters: queryParams).query;

        String url = '$URL_BINGKAS/check-credential-app' + '?' + queryString;

        print(url);
        emit(BingkasStatusNotRegistered(url: url));
      }
    } else {
      String id = await SecureStorage().readSecureData('userId');
      String selId = await SecureStorage().readSecureData('userSelId');

      Map<String, String> queryParams = {
        'user_token': token,
        'id_selangkah_user': id,
        'selangkah_id': selId,
      };

      String queryString = Uri(queryParameters: queryParams).query;

      String url = '$URL_BINGKAS/check-credential-app' + '?' + queryString;

      print(url);
      emit(BingkasStatusNotRegistered(url: url));
    }

    // emit(BingkasStatusLoaded(status));
  }
}
