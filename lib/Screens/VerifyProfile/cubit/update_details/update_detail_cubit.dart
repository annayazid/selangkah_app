import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/VerifyProfile/verify_profile.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

part 'update_detail_state.dart';

class UpdateDetailCubit extends Cubit<UpdateDetailState> {
  UpdateDetailCubit() : super(UpdateDetailInitial());

  Future<void> updateUserDetails(
      {String? userToken,
      required String name,
      required String phone3,
      required String phoneNo,
      required String idNo,
      required String email,
      required String idType,
      required String citizen,
      required String gender,
      required String dobDay,
      required int dobMonthIndex,
      required String dobYear,
      required String race,
      required String address1,
      required String address2,
      required String postcode,
      required String state,
      required String city,
      required String country}) async {
    emit(UpdateDetailLoading());

    String dobCombine;
    if ((dobMonthIndex + 1).toString().length != 2) {
      dobCombine = '0${(dobMonthIndex + 1).toString()}';
    } else {
      dobCombine = '${(dobMonthIndex + 1).toString()}';
    }

    //combine DOB
    String dob = '$dobYear-$dobCombine-$dobDay';

    //combine number
    String phone = '$phone3$phoneNo';

    //change address \ to /
    address1 = address1.replaceAll(r'\', r'/');
    address2 = address2.replaceAll(r'\', r'/');

    print(dob);
    print('address 2 is $address2');
    print('phone is $phone');

    var connectivityResult = await (Connectivity().checkConnectivity());
    // I am connected to a mobile network.
    // I am connected to a wifi network.
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      bool updateSuccess = await VerifyUserProfile.updateUserProfile(
          name: name,
          phone: phone,
          idNo: idNo,
          email: email,
          idType: idType,
          citizen: citizen,
          gender: gender,
          dob: dob,
          race: race,
          address1: address1,
          address2: address2,
          postcode: postcode,
          state: state,
          city: city,
          country: country);

      if (updateSuccess) {
        //setCache
        SecureStorage secureStorage = SecureStorage();
        secureStorage.writeSecureData('userName', name);
        secureStorage.writeSecureData('userPhoneNo', phone);
        secureStorage.writeSecureData('email', email);
        secureStorage.writeSecureData("userIdNo", idNo);
        secureStorage.writeSecureData("userIdType", idType);
        secureStorage.writeSecureData("citizen", citizen);
        secureStorage.writeSecureData("gender", gender);
        secureStorage.writeSecureData("dateOfBirth", dob);
        secureStorage.writeSecureData("race", race);
        secureStorage.writeSecureData("address1", address1);
        secureStorage.writeSecureData("address2", address2);
        secureStorage.writeSecureData("postcode", postcode);
        secureStorage.writeSecureData("city", city);
        secureStorage.writeSecureData("state", state);
        secureStorage.writeSecureData("country", country);

        if (userToken != null) {
          String accessToken = await VerifyUserProfile.getSihatToken();
          await VerifyUserProfile.updateUserRemedi(accessToken);
        }
      }
      emit(UpdateDetailLoaded(updateSuccess));
    } else {
      Fluttertoast.showToast(msg: "nointernet".tr());
      emit(UserNoInternet());
    }
  }
}
