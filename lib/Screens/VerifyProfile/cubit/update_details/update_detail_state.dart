part of 'update_detail_cubit.dart';

@immutable
abstract class UpdateDetailState {
  const UpdateDetailState();
}

class UpdateDetailInitial extends UpdateDetailState {
  const UpdateDetailInitial();
}

class UpdateDetailLoading extends UpdateDetailState {
  const UpdateDetailLoading();
}

class UpdateDetailLoaded extends UpdateDetailState {
  final bool updateSuccess;

  UpdateDetailLoaded(this.updateSuccess);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UpdateDetailLoaded && other.updateSuccess == updateSuccess;
  }

  @override
  int get hashCode => updateSuccess.hashCode;
}

class UserNoInternet extends UpdateDetailState {
  const UserNoInternet();
}
