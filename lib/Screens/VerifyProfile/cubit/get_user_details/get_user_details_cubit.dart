import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/VerifyProfile/verify_profile.dart';

part 'get_user_details_state.dart';

class GetUserDetailsCubit extends Cubit<GetUserDetailsState> {
  GetUserDetailsCubit() : super(GetUserDetailsInitial());

  Future<void> getUserDetails() async {
    emit(GetUserDetailsLoading());

    UserProfile userProfile = await VerifyUserProfile.checkUserProfile();

    emit(GetUserDetailsLoaded(userProfile));
  }
}
