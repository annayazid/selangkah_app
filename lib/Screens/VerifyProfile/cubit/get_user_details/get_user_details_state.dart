part of 'get_user_details_cubit.dart';

@immutable
abstract class GetUserDetailsState {
  const GetUserDetailsState();
}

class GetUserDetailsInitial extends GetUserDetailsState {
  const GetUserDetailsInitial();
}

class GetUserDetailsLoading extends GetUserDetailsState {
  const GetUserDetailsLoading();
}

class GetUserDetailsLoaded extends GetUserDetailsState {
  final UserProfile? userProfile;

  GetUserDetailsLoaded(this.userProfile);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetUserDetailsLoaded && other.userProfile == userProfile;
  }

  @override
  int get hashCode => userProfile.hashCode;
}
