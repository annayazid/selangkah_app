import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/utils/AppConstant.dart';
import 'package:http/http.dart' as http;
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class VerifyUserProfile {
  static Future<String> getSihatToken() async {
    String accessToken;

    Map jsonBody = {
      "grant_type": "client_credentials",
      "client_id": secret_id,
      "client_secret": secret_key,
      "scope": "*"
    };
    // print(jsonBody);

    final response = await http.post(Uri.parse('$REMEDI/oauth/token'),
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(jsonBody));

    var data = jsonDecode(response.body);
    // print(data);

    accessToken = data['token_type'] + " " + data['access_token'];
    return accessToken;
  }

  static Future<bool> updateUserProfile(
      {required String name,
      required String phone,
      required String idNo,
      required String email,
      required String idType,
      required String citizen,
      required String gender,
      required String dob,
      required String race,
      required String address1,
      required String address2,
      required String postcode,
      required String state,
      required String city,
      required String country}) async {
    SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData("userId");

    final Map<String, String> map = {
      'id': id,
      'name': name,
      'phone': phone,
      'idno': idNo,
      'email': email,
      'id_type': idType,
      'ctzn': citizen,
      'gndr': gender,
      'dob': dob,
      'rc': race,
      'add1': address1,
      'add2': address2,
      'pscd': postcode,
      'stt': state,
      'cty': city,
      'ctry': country,
      'token': TOKEN,
    };

    print(map);

    print('calling post UPDATE_PROFILE');

    final response = await http.post(
      Uri.parse('$API_URL/update_profile_user'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response);

    if (response.body.contains('200')) {
      Fluttertoast.showToast(msg: 'updateSuccess'.tr());
      return true;
    } else {
      Fluttertoast.showToast(msg: 'updateFailed'.tr());
      return false;
    }
  }

  static Future<void> updateUserRemedi(String accessToken) async {
    SecureStorage secureStorage = SecureStorage();
    String selId = await secureStorage.readSecureData('userSelId');
    String name = await secureStorage.readSecureData('userName');
    String idNo = await secureStorage.readSecureData('userIdNo');
    String idType = await secureStorage.readSecureData('userIdType');
    String citizen = await secureStorage.readSecureData('citizen');
    String gender = await secureStorage.readSecureData('gender');
    String dob = await secureStorage.readSecureData('dateOfBirth');
    String email = await secureStorage.readSecureData('email');
    String race = await secureStorage.readSecureData('race');
    String phone = await secureStorage.readSecureData('userPhoneNo');
    String address1 = await secureStorage.readSecureData('address1');
    String address2 = await secureStorage.readSecureData('address2');
    String city = await secureStorage.readSecureData('city');
    String state = await secureStorage.readSecureData('state');
    String country = await secureStorage.readSecureData('country');

    String url = "$REMEDI/api/selangkah/update/user";
    String jsonData =
        '{"selangkah_id":"$selId","user_info":{"first_name":"$name","last_name":"","id_no":"$idNo","id_type":"$idType","citizenship":"$citizen","gender":"$gender","dob":"$dob","email":"$email","race":"$race"},"user_telephone":{"tel_mobile":"6$phone"},"user_address":{"user_addr1":"$address1","user_postcode":"$address2","user_city":"$city","user_state":"$state","user_country":"$country"}}';

    print(url);
    print(jsonData);
    print(accessToken);

    await http.post(Uri.parse(url),
        headers: {
          'Authorization': accessToken,
          'Content-Type': 'application/json'
        },
        body: jsonData);
  }

  static Future<UserProfile> checkUserProfile() async {
    SecureStorage secureStorage = SecureStorage();
    String userId = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id': userId,
      'token': TOKEN,
    };

    print('calling post GET_PROFILE');

    final response = await http.post(
      Uri.parse('$API_URL/check_user_profile'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    RawProfile? rawProfile = rawProfileFromJson(response.body);

    UserProfile? userProfile = UserProfile(
      id: rawProfile!.data![0],
      selId: rawProfile.data![1],
      name: rawProfile.data![2],
      token: rawProfile.data![3] != null ? rawProfile.data![3] : '',
      idNo: rawProfile.data![4] != null ? rawProfile.data![4] : '',
      language: rawProfile.data![5] != null ? rawProfile.data![5] : '',
      citizen: rawProfile.data![6] != null ? rawProfile.data![6] : '',
      gender: rawProfile.data![7] != null ? rawProfile.data![7] : '',
      dob: rawProfile.data![8] != null ? rawProfile.data![8] : '',
      address1: rawProfile.data![9] != null ? rawProfile.data![9] : '',
      address2: rawProfile.data![10] != null ? rawProfile.data![10] : '',
      postcode: rawProfile.data![11] != null ? rawProfile.data![11] : '',
      city: rawProfile.data![12] != null ? rawProfile.data![12] : '',
      state: rawProfile.data![13] != null ? rawProfile.data![13] : '',
      country: rawProfile.data![14] != null ? rawProfile.data![14] : '',
      email: rawProfile.data![15] != null ? rawProfile.data![15] : '',
      race: rawProfile.data![16] != null ? rawProfile.data![16] : '',
      idType: rawProfile.data![17] != null ? rawProfile.data![17] : '',
      profilePic: rawProfile.data![18] != null ? rawProfile.data![18] : '',
      phoneNo: rawProfile.data![19] != null ? rawProfile.data![19] : '',
      ekycStatus: rawProfile.data![20],
    );

    return userProfile;
  }
}
