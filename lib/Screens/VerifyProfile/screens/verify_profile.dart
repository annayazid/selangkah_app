import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/SelangkahAppBarScaffold/scaffold.dart';
import 'package:selangkah_new/Screens/VerifyProfile/verify_profile.dart';
import 'package:selangkah_new/utils/AppColors.dart';
import 'package:selangkah_new/utils/AppConstant.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:country_picker/country_picker.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class VerifyProfile extends StatefulWidget {
  final String? message = "";

  @override
  State<VerifyProfile> createState() => _VerifyProfileState();
}

class _VerifyProfileState extends State<VerifyProfile> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController idController = TextEditingController();
  final TextEditingController address1Controller = TextEditingController();
  final TextEditingController address2Controller = TextEditingController();
  final TextEditingController postcodeController = TextEditingController();
  final TextEditingController cityController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  // bool _validate = false;

  String gender = 'pleaseselect'.tr();
  String idType = 'pleaseselect'.tr();
  String citizen = "Malaysia";
  String citizenCode = "MY";
  String country = "Malaysia";
  String countryCode = "MY";
  String userState = "Selangor";
  String race = 'pleaseselect'.tr();
  String phone3 = '010';
  String? dob = '1990-01-01';
  String status = 'INACTIVE';
  bool noEmail = false;

  int dobDayIndex = 0;
  int dobMonthIndex = 0;
  int dobYearIndex = 0;

  String dobDay = '01';
  String dobMonth = 'Jan';
  String dobYear = '1980';

  List<String> days = [];
  final List<String> months = [
    'january'.tr(),
    'february'.tr(),
    'march'.tr(),
    'april'.tr(),
    'may'.tr(),
    'june'.tr(),
    'july'.tr(),
    'august'.tr(),
    'september'.tr(),
    'october'.tr(),
    'november'.tr(),
    'december'.tr(),
  ];
  List<String> years = [];

  void setDayYear() async {
    days.clear();
    for (var i = 1; i <= 31; i++) {
      String no;

      if (i.toString().length != 2) {
        no = '0$i';
      } else {
        no = i.toString();
      }

      setState(() {
        days.add(no);
      });
    }

    years.clear();
    for (var i = 1920; i <= 2023; i++) {
      setState(() {
        years.add(i.toString());
      });
    }

    selId = await SecureStorage().readSecureData('userSelId');
  }

  @override
  void initState() {
    setDayYear();
    context.read<GetUserDetailsCubit>().getUserDetails();
    super.initState();

    if (widget.message != null) {
      toast(
        widget.message,
        length: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
      );
    }
  }

  late String selId;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    Size size = MediaQuery.of(context).size;

    String? validateId(String? value) {
      String patttern = r'(^[A-Za-z0-9]*$)';
      RegExp regExp = new RegExp(patttern);

      if (idType == 'ICNO') {
        if (value!.length != 12) {
          toast('ic_length'.tr());
          return 'ic_length'.tr();
        }
      }

      if (idType == 'pleaseselect') {
        toast('idtyperequired'.tr());
        return 'idtyperequired'.tr();
      }

      if (value == null) {
        toast('idrequired'.tr());
        return 'idrequired'.tr();
      } else if (!regExp.hasMatch(value)) {
        toast('idformat'.tr());
        return 'idformat'.tr();
      } else if (value.length < 5) {
        toast('idlength'.tr());
        return 'idlength'.tr();
      }
      return null;
    }

    String? validatePhoneNum(String? value) {
      String patttern = r'(^[0-9]*$)';
      RegExp regExp = new RegExp(patttern);
      if (value == null) {
        toast('phonerequired'.tr());
        return 'phonerequired'.tr();
      } else if (value.length >= 12) {
        toast('phone10'.tr());
        return 'phone10'.tr();
      } else if (!regExp.hasMatch(value)) {
        toast('phonedigit'.tr());
        return 'phonedigit'.tr();
        // } else if (!value.startsWith('01')) {
        //   toast('phon01'.tr());
        //   return 'phon01'.tr();
      }
      return null;
    }

    return TopBarScaffold(
        title: 'edit_profile'.tr(),
        content: BlocConsumer<GetUserDetailsCubit, GetUserDetailsState>(
            listener: (context, state) {
          if (state is GetUserDetailsLoaded) {
            nameController.text = state.userProfile!.name!;
            phoneController.text = state.userProfile!.phoneNo!
                .substring(3, state.userProfile!.phoneNo!.length);
            emailController.text = state.userProfile!.email!;
            print(emailController.text);
            print('DISANA!!');
            idController.text = state.userProfile!.idNo!;
            address1Controller.text = state.userProfile!.address1!;
            address2Controller.text = state.userProfile!.address2!;
            postcodeController.text = state.userProfile!.postcode!;
            cityController.text = state.userProfile!.city!;
            phone3 = state.userProfile!.phoneNo!.substring(0, 3);
            if (phone3 == '') {
              phone3 = '011';
            }
            idType = state.userProfile!.idType!;
            if (idType == '') {
              idType = 'pleaseselect'.tr();
            }
            citizen = state.userProfile!.citizen!;
            if (citizen == '') {
              citizen = "Malaysia";
              citizenCode = "MY";
            }
            gender = state.userProfile!.gender!;
            if (gender == '') {
              gender = 'pleaseselect'.tr();
            }
            dob = state.userProfile!.dob;
            if (dob == '') {
              dobDay = '01';
              dobMonth = 'Jan';
              dobYear = '1980';
            }
            if (dob != '') {
              dobYear = dob!.substring(0, 4);
              dobMonthIndex = int.parse(dob!.substring(5, 7)) - 1;
              dobDay = dob!.substring(8, 10);
            }
            race = state.userProfile!.race!;
            if (race == '') {
              race = 'pleaseselect'.tr();
            }
            userState = state.userProfile!.state!;
            if (userState == '') {
              userState = "Selangor";
            }
            country = state.userProfile!.country!;
            if (country == '') {
              country = "Malaysia";
              countryCode = "MY";
            }

            status = state.userProfile!.ekycStatus!;
            print(gender);
            print('DISINI!!');
            print(dob);
          }
        }, builder: (context, state) {
          print(state.toString());
          if (state is GetUserDetailsLoaded) {
            return SingleChildScrollView(
                child: Container(
              color: Color(0xFFF5F4F4),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 30),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        '* ' + 'mandatory'.tr(),
                        style: boldTextStyle(color: Colors.red, size: 15),
                      ),
                    ),
                    SizedBox(height: 20),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'name'.tr() + ' *',
                        style:
                            boldTextStyle(color: appTextColorPrimary, size: 15),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'ex_name'.tr(),
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: TextFormField(
                        inputFormatters: [
                          FilteringTextInputFormatter.deny(
                              RegExp(regexToRemoveEmoji)),
                        ],
                        controller: nameController,
                        toolbarOptions: ToolbarOptions(
                            copy: true, paste: false, cut: true, selectAll: true
                            //by default all are disabled 'false'
                            ),
                        readOnly: status.contains('KYC') ? true : false,
                        style: primaryTextStyle(
                          size: 18,
                          color: appTextColorPrimary,
                        ),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(26, 18, 4, 18),
                          filled: true,
                          fillColor: status.contains('KYC')
                              ? Color(0xFFF5F4F4)
                              : appTextBackground,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            // borderSide: BorderSide(
                            //     color: appTextBackground, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),

                            // borderSide:
                            //     BorderSide(color: appTextBackground, width: 0.0),
                          ),
                          // hintText: 'ex_name'.tr(),
                        ),
                        keyboardType: TextInputType.name,
                        onSaved: (String? val) {
                          nameController.text = val!;
                        },
                      ),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'phonenumber'.tr() + ' *',
                        style: boldTextStyle(
                          color: appTextColorPrimary,
                          size: 15,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'ex_phone'.tr(),
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 4,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                              decoration: BoxDecoration(
                                color: status.contains('KYC')
                                    ? Color(0xFFF5F4F4)
                                    : appTextBackground,
                                borderRadius: BorderRadius.circular(40),
                                border: Border.all(width: 0.8),
                              ),
                              child: IgnorePointer(
                                ignoring: status.contains('KYC') ? true : false,
                                child: DropdownButtonFormField<String>(
                                  disabledHint: Text(phone3),
                                  value: phone3,
                                  items: <String>[
                                    '010',
                                    '011',
                                    '012',
                                    '013',
                                    '014',
                                    '015',
                                    '016',
                                    '017',
                                    '018',
                                    '019',
                                  ].map<DropdownMenuItem<String>>(
                                    (String val3) {
                                      return DropdownMenuItem<String>(
                                        value: val3,
                                        child: Text(val3),
                                      );
                                    },
                                  ).toList(),
                                  isExpanded: true,
                                  icon: Icon(Icons.arrow_downward),
                                  iconSize: 24,
                                  elevation: 16,
                                  onChanged: (String? newValue) {
                                    setState(
                                      () {
                                        if (newValue == '010') {
                                          phone3 = '010';
                                        } else if (newValue == '011') {
                                          phone3 = '011';
                                        } else if (newValue == '012') {
                                          phone3 = '012';
                                        } else if (newValue == '013') {
                                          phone3 = '013';
                                        } else if (newValue == '014') {
                                          phone3 = '014';
                                        } else if (newValue == '015') {
                                          phone3 = '015';
                                        } else if (newValue == '016') {
                                          phone3 = '016';
                                        } else if (newValue == '017') {
                                          phone3 = '017';
                                        } else if (newValue == '018') {
                                          phone3 = '018';
                                        } else {
                                          phone3 = '019';
                                        }
                                      },
                                    );
                                  },
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            flex: 6,
                            child: Container(
                              child: TextFormField(
                                validator: validatePhoneNum,
                                inputFormatters: [
                                  FilteringTextInputFormatter.deny(
                                      RegExp(regexToRemoveEmoji)),
                                ],
                                controller: phoneController,
                                readOnly: status.contains('KYC') ? true : false,
                                style: primaryTextStyle(
                                    size: 18, color: appTextColorPrimary),
                                decoration: InputDecoration(
                                  // prefixText: "+6 ",
                                  // hintText: 'ex_phone'.tr(),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(26, 18, 4, 18),
                                  filled: true,
                                  fillColor: status.contains('KYC')
                                      ? Color(0xFFF5F4F4)
                                      : appTextBackground,
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(40),
                                    // borderSide: BorderSide(
                                    //     color: appTextBackground, width: 0.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(40),
                                    // borderSide: BorderSide(
                                    //     color: appTextBackground, width: 0.0),
                                  ),
                                ),
                                keyboardType: TextInputType.phone,
                                onSaved: (String? val) {
                                  phoneController.text = val!;
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'email'.tr() + ' *',
                        style:
                            boldTextStyle(color: appTextColorPrimary, size: 15),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'ex_email'.tr().toString(),
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: TextFormField(
                        validator: (value) {
                          if (value == null) {
                            Fluttertoast.showToast(
                                msg: 'not_format_email'.tr());
                            return 'null'.tr();
                          } else {
                            return null;
                          }
                        },
                        inputFormatters: [
                          FilteringTextInputFormatter.deny(
                              RegExp(regexToRemoveEmoji)),
                        ],
                        readOnly: noEmail ? true : false,
                        controller: emailController,
                        style: primaryTextStyle(size: 18),
                        decoration: InputDecoration(
                          // hintText: 'ex_email'.tr(),
                          contentPadding: EdgeInsets.fromLTRB(26, 18, 0, 18),
                          filled: true,
                          fillColor: appTextBackground,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            // borderSide: BorderSide(
                            //     color: appTextBackground, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            // borderSide: BorderSide(
                            //     color: appTextBackground, width: 0.0),
                          ),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        onSaved: (String? val) {
                          emailController.text = val!;
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(10, 0, 16, 0),
                      child: CheckboxListTile(
                        value: noEmail,
                        onChanged: (value) {
                          setState(() {
                            noEmail = value!;
                            emailController.text = 'noemail@noemail.com';
                          });
                        },
                        title: Text('tick_here_noemail'.tr()),
                        checkColor: Colors.white,
                        activeColor: kPrimaryColor,
                      ),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'idtype'.tr() + ' *',
                        style:
                            boldTextStyle(color: appTextColorPrimary, size: 15),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: Container(
                        padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                        width: size.width * 0.8,
                        decoration: BoxDecoration(
                          color: status.contains('KYC')
                              ? Color(0xFFF5F4F4)
                              : appTextBackground,
                          borderRadius: BorderRadius.circular(40),
                          border: Border.all(width: 0.8),
                        ),
                        child: IgnorePointer(
                          ignoring: status.contains('KYC') ? true : false,
                          child: DropdownButtonFormField<String>(
                            isExpanded: true,
                            validator: (value) {
                              if (value == '' || value == 'pleaseselect'.tr()) {
                                Fluttertoast.showToast(
                                    msg: 'please_choose_ID'.tr());
                                return 'please_choose_ID'.tr();
                              } else {
                                return null;
                              }
                            },
                            value: () {
                              if (idType == 'pleaseselect') {
                                return 'pleaseselect'.tr();
                              } else if (idType == 'ICNO') {
                                return 'icnofield'.tr();
                              } else if (idType == 'POLICE') {
                                return 'police'.tr();
                              } else if (idType == 'ARMY') {
                                return 'army'.tr();
                              } else if (idType == 'POLICE') {
                                return 'UNHCR'.tr();
                              } else if (idType == 'POLICE') {
                                return 'unhcr'.tr();
                              } else if (idType == 'PASSPORT') {
                                return 'passport'.tr();
                              } else if (idType == 'PERMRES') {
                                return 'permres'.tr();
                              } else if (idType == 'SELANGKAH') {
                                return 'selangkah'.tr();
                              } else {
                                return 'others'.tr();
                              }
                            }(),
                            icon: Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            onChanged: (String? newValue) {
                              setState(() {
                                if (newValue == 'pleaseselect'.tr()) {
                                  idType = 'PLEASESELECT';
                                } else if (newValue == 'icnofield'.tr()) {
                                  idType = 'ICNO';
                                } else if (newValue == 'police'.tr()) {
                                  idType = 'POLICE';
                                } else if (newValue == 'army'.tr()) {
                                  idType = 'ARMY';
                                } else if (newValue == 'unhcr'.tr()) {
                                  idType = 'UNHCR';
                                } else if (newValue == 'passport'.tr()) {
                                  idType = 'PASSPORT';
                                } else if (newValue == 'permres'.tr()) {
                                  idType = 'PERMRES';
                                } else if (newValue == 'selangkah'.tr()) {
                                  idType = 'SELANGKAH';
                                  setState(() {
                                    idController.text = selId;
                                  });
                                } else {
                                  idType = 'OTHERS';
                                }
                              });
                            },
                            items: <String>[
                              'pleaseselect'.tr(),
                              'icnofield'.tr(),
                              'police'.tr(),
                              'army'.tr(),
                              'unhcr'.tr(),
                              'passport'.tr(),
                              'permres'.tr(),
                              'selangkah'.tr(),
                              'others'.tr(),
                            ].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'icno'.tr() + ' *',
                        style:
                            boldTextStyle(color: appTextColorPrimary, size: 15),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'ex_ic'.tr(),
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: TextFormField(
                        inputFormatters: [
                          FilteringTextInputFormatter.deny(
                              RegExp(regexToRemoveEmoji)),
                        ],
                        controller: idController,
                        readOnly:
                            status.contains('KYC') || idType == 'SELANGKAH'
                                ? true
                                : false,
                        style: primaryTextStyle(
                            size: 18, color: appTextColorPrimary),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(26, 18, 0, 18),
                          filled: true,
                          fillColor: status.contains('KYC')
                              ? Color(0xFFF5F4F4)
                              : appTextBackground,
                          hintText: () {
                            if (idType == 'ICNO') {
                              return 'ex_ic'.tr();
                            } else {
                              return '';
                            }
                          }(),
                          hintStyle: TextStyle(color: Colors.grey),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            // borderSide: BorderSide(
                            //     color: appTextBackground, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            // borderSide: BorderSide(
                            //     color: appTextBackground, width: 0.0),
                          ),
                        ),
                        keyboardType: () {
                          if (idType == 'ICNO' ||
                              idType == 'POLICE' ||
                              idType == 'ARMY' ||
                              idType == 'PERMRES') {
                            return TextInputType.number;
                          } else {
                            return TextInputType.text;
                          }
                        }(),
                        validator: validateId,
                        onSaved: (String? val) {
                          idController.text = val!;
                        },
                      ),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'citizen'.tr() + ' *',
                        style:
                            boldTextStyle(color: appTextColorPrimary, size: 15),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: GestureDetector(
                        onTap: (() {
                          showCountryPicker(
                            context: context,
                            //Optional.  Can be used to exclude(remove) one ore more country from the countries list (optional).
                            // exclude: <String>['KN', 'MF'],
                            favorite: <String>['MY'],
                            //Optional. Shows phone code before the country name.
                            // showPhoneCode: true,
                            onSelect: (Country chooseCitizen) {
                              print(
                                  'Select country: ${chooseCitizen.displayName}');
                              setState(() {
                                citizen = chooseCitizen.name;
                                citizenCode = chooseCitizen.countryCode;
                              });
                            },
                            // Optional. Sets the theme for the country list picker.
                            countryListTheme: CountryListThemeData(
                              // Optional. Sets the border radius for the bottomsheet.
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(40.0),
                                topRight: Radius.circular(40.0),
                              ),
                              // Optional. Styles the search field.
                              inputDecoration: InputDecoration(
                                labelText: 'Search',
                                hintText: 'Start typing to search',
                                prefixIcon: const Icon(Icons.search),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: const Color(0xFF8C98A8)
                                        .withOpacity(0.2),
                                  ),
                                ),
                              ),
                              // Optional. Styles the text in the search field
                              searchTextStyle: TextStyle(
                                color: Colors.blue,
                                fontSize: 18,
                              ),
                            ),
                          );
                        }),
                        child: Container(
                            padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                            width: size.width * 0.8,
                            height: 50,
                            decoration: BoxDecoration(
                                color: appTextBackground,
                                borderRadius: BorderRadius.circular(40),
                                border: Border.all(width: 0.8)),
                            child: Row(children: [
                              Expanded(
                                  child: Text(
                                citizen,
                                style: TextStyle(fontSize: 16),
                              )),
                              Icon(
                                Icons.arrow_downward,
                                color: Color(0xFF696767),
                              ),
                            ])),
                      ),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'gender'.tr() + ' *',
                        style:
                            boldTextStyle(color: appTextColorPrimary, size: 15),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: Container(
                        padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                        width: size.width * 0.8,
                        decoration: BoxDecoration(
                            color: appTextBackground,
                            borderRadius: BorderRadius.circular(40),
                            border: Border.all(width: 0.8)),
                        child: DropdownButtonFormField<String>(
                          validator: (value) {
                            if (value == '' || value == 'pleaseselect'.tr()) {
                              Fluttertoast.showToast(
                                  msg: 'please_select_gender'.tr());
                              return 'please_select_gender'.tr();
                            } else {
                              return null;
                            }
                          },
                          isExpanded: true,
                          value: () {
                            if (gender == 'M' ||
                                gender == 'm' ||
                                gender == 'Male' ||
                                gender == 'male') {
                              return "male".tr();
                            } else if (gender == 'F' ||
                                gender == 'f' ||
                                gender == 'Female' ||
                                gender == 'female') {
                              return "female".tr();
                            } else {
                              return 'pleaseselect'.tr();
                            }
                          }(),
                          icon: Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          onChanged: (String? newValue) {
                            setState(() {
                              if (newValue == 'male'.tr()) {
                                gender = 'M';
                              } else if (newValue == 'female'.tr()) {
                                gender = 'F';
                              } else {
                                gender = 'pleaseselect'.tr();
                              }
                            });
                          },
                          items: <String>[
                            'pleaseselect'.tr(),
                            'male'.tr(),
                            'female'.tr()
                          ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'dob'.tr() + ' *',
                        style:
                            boldTextStyle(color: appTextColorPrimary, size: 15),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                              decoration: BoxDecoration(
                                color: appTextBackground,
                                borderRadius: BorderRadius.circular(20),
                                border: Border.all(width: 0.8),
                              ),
                              child: DropdownButton<String>(
                                value: months[dobMonthIndex],
                                items: months.map<DropdownMenuItem<String>>(
                                  (String val3) {
                                    return DropdownMenuItem<String>(
                                      value: val3,
                                      child: Text(
                                        val3,
                                        style: TextStyle(fontSize: 15),
                                      ),
                                    );
                                  },
                                ).toList(),
                                isExpanded: true,
                                icon: Icon(Icons.arrow_downward),
                                iconSize: 24,
                                elevation: 16,
                                onChanged: (String? newValue) {
                                  setState(
                                    () {
                                      dobMonth = newValue!;
                                      dobMonthIndex = months.indexOf(newValue);
                                      // print(dobMonthIndex);

                                      //30 feb,april,jun,september,november
                                      if (dobMonthIndex == 3 ||
                                          dobMonthIndex == 5 ||
                                          dobMonthIndex == 8 ||
                                          dobMonthIndex == 10) {
                                        if (dobDay == '31') {
                                          dobDay = '30';
                                        }
                                        days.remove('31');
                                      } else if (dobMonthIndex == 1) {
                                        if (dobDay == '31' || dobDay == '30') {
                                          dobDay = '29';
                                        }
                                        days.remove('31');
                                        days.remove('30');
                                      } else {
                                        days.clear();
                                        for (var i = 1; i <= 31; i++) {
                                          String no;

                                          if (i.toString().length != 2) {
                                            no = '0$i';
                                          } else {
                                            no = i.toString();
                                          }

                                          setState(() {
                                            days.add(no);
                                          });
                                        }
                                      }

                                      // print(days.length);
                                    },
                                  );
                                },
                              ),
                            ),
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                              decoration: BoxDecoration(
                                color: appTextBackground,
                                borderRadius: BorderRadius.circular(20),
                                border: Border.all(width: 0.8),
                              ),
                              child: DropdownButton<String>(
                                value: dobDay,
                                items: days.map<DropdownMenuItem<String>>(
                                  (String val3) {
                                    return DropdownMenuItem<String>(
                                      value: val3,
                                      child: Text(
                                        val3,
                                        style: TextStyle(fontSize: 15),
                                      ),
                                    );
                                  },
                                ).toList(),
                                isExpanded: true,
                                icon: Icon(Icons.arrow_downward),
                                iconSize: 24,
                                elevation: 16,
                                onChanged: (String? newValue) {
                                  setState(
                                    () {
                                      dobDay = newValue!;
                                    },
                                  );
                                },
                              ),
                            ),
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                              decoration: BoxDecoration(
                                color: appTextBackground,
                                borderRadius: BorderRadius.circular(20),
                                border: Border.all(width: 0.8),
                              ),
                              child: DropdownButton<String>(
                                value: dobYear,
                                items: years.map<DropdownMenuItem<String>>(
                                  (String val3) {
                                    return DropdownMenuItem<String>(
                                      value: val3,
                                      child: Text(
                                        val3,
                                        style: TextStyle(fontSize: 15),
                                      ),
                                    );
                                  },
                                ).toList(),
                                isExpanded: true,
                                icon: Icon(Icons.arrow_downward),
                                iconSize: 24,
                                elevation: 16,
                                onChanged: (String? newValue) {
                                  setState(
                                    () {
                                      dobYear = newValue!;
                                    },
                                  );
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'race'.tr() + ' *',
                        style:
                            boldTextStyle(color: appTextColorPrimary, size: 15),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: Container(
                        padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                        width: size.width * 0.8,
                        decoration: BoxDecoration(
                            color: appTextBackground,
                            borderRadius: BorderRadius.circular(40),
                            border: Border.all(width: 0.8)),
                        child: DropdownButtonFormField<String>(
                          validator: (value) {
                            if (value == '' || value == 'pleaseselect'.tr()) {
                              Fluttertoast.showToast(
                                  msg: 'pelase_select_race'.tr());
                              return 'pelase_select_race'.tr();
                            } else {
                              return null;
                            }
                          },
                          isExpanded: true,
                          value: () {
                            // race == 'malay'
                            //   ? 'malay'.tr().toString()
                            //   : race == 'chinese'
                            //       ? 'chinese'.tr().toString()
                            //       : race == 'indian'
                            //           ? 'indian'.tr().toString()
                            //           : 'others'.tr().toString()
                            if (race == 'pleaseselect'.tr()) {
                              return 'pleaseselect'.tr();
                            } else if (race == 'malay') {
                              return 'malay'.tr();
                            } else if (race == 'chinese') {
                              return 'chinese'.tr();
                            } else if (race == 'indian') {
                              return 'indian'.tr();
                            } else {
                              return 'others'.tr();
                            }
                          }(),
                          icon: Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          onChanged: (String? newValue) {
                            setState(() {
                              if (newValue == 'malay'.tr()) {
                                race = 'malay';
                              } else if (newValue == 'chinese'.tr()) {
                                race = 'chinese';
                              } else if (newValue == 'indian'.tr()) {
                                race = 'indian';
                              } else {
                                race = 'others';
                              }
                            });
                          },
                          items: <String>[
                            'pleaseselect'.tr(),
                            'malay'.tr(),
                            'chinese'.tr(),
                            'indian'.tr(),
                            'others'.tr(),
                          ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'address'.tr() + ' *',
                        style:
                            boldTextStyle(color: appTextColorPrimary, size: 15),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'ex_address'.tr(),
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: TextFormField(
                        inputFormatters: [
                          FilteringTextInputFormatter.deny(
                              RegExp(regexToRemoveEmoji)),
                        ],
                        controller: address1Controller,
                        style: primaryTextStyle(size: 18),
                        decoration: InputDecoration(
                          // hintText: 'ex_address'.tr(),
                          contentPadding: EdgeInsets.fromLTRB(26, 18, 0, 18),
                          filled: true,
                          fillColor: appTextBackground,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            // borderSide: BorderSide(
                            //     color: appTextBackground, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            // borderSide: BorderSide(
                            //     color: appTextBackground, width: 0.0),
                          ),
                        ),
                        onSaved: (String? val) {
                          address1Controller.text = val!;
                        },
                      ),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: TextFormField(
                        inputFormatters: [
                          FilteringTextInputFormatter.deny(
                              RegExp(regexToRemoveEmoji)),
                        ],
                        controller: address2Controller,
                        style: primaryTextStyle(size: 18),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(26, 18, 0, 18),
                          filled: true,
                          fillColor: appTextBackground,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            // borderSide: BorderSide(
                            //     color: appTextBackground, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            // borderSide: BorderSide(
                            //     color: appTextBackground, width: 0.0),
                          ),
                        ),
                        onSaved: (String? val) {
                          address2Controller.text = val!;
                        },
                      ),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'postcode'.tr() + ' *',
                        style:
                            boldTextStyle(color: appTextColorPrimary, size: 15),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'ex_postcode'.tr(),
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: TextFormField(
                        inputFormatters: [
                          FilteringTextInputFormatter.deny(
                              RegExp(regexToRemoveEmoji)),
                        ],
                        controller: postcodeController,
                        style: primaryTextStyle(size: 18),
                        decoration: InputDecoration(
                          // hintText: 'ex_postcode'.tr(),
                          contentPadding: EdgeInsets.fromLTRB(26, 18, 0, 18),
                          filled: true,
                          fillColor: appTextBackground,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            // borderSide: BorderSide(
                            //     color: appTextBackground, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            // borderSide: BorderSide(
                            //     color: appTextBackground, width: 0.0),
                          ),
                        ),
                        keyboardType: TextInputType.number,
                        onSaved: (String? val) {
                          postcodeController.text = val!;
                        },
                      ),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'city'.tr() + ' *',
                        style:
                            boldTextStyle(color: appTextColorPrimary, size: 15),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'ex_city'.tr(),
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: TextFormField(
                        inputFormatters: [
                          FilteringTextInputFormatter.deny(
                              RegExp(regexToRemoveEmoji)),
                        ],
                        controller: cityController,
                        style: primaryTextStyle(size: 18),
                        decoration: InputDecoration(
                          // hintText: 'ex_city'.tr(),
                          contentPadding: EdgeInsets.fromLTRB(26, 18, 0, 18),
                          filled: true,
                          fillColor: appTextBackground,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            // borderSide: BorderSide(
                            //     color: appTextBackground, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            // borderSide: BorderSide(
                            //     color: appTextBackground, width: 0.0),
                          ),
                        ),
                        keyboardType: TextInputType.text,
                        onSaved: (String? val) {
                          cityController.text = val!;
                        },
                      ),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'state'.tr() + ' *',
                        style:
                            boldTextStyle(color: appTextColorPrimary, size: 15),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                        child: Container(
                            padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                            width: size.width * 0.8,
                            decoration: BoxDecoration(
                                color: appTextBackground,
                                borderRadius: BorderRadius.circular(40),
                                border: Border.all(width: 0.8)),
                            child: DropdownButtonFormField<String>(
                              isExpanded: true,
                              value: userState,
                              icon: Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              onChanged: (String? newValue) {
                                setState(() {
                                  userState = newValue!;
                                });
                              },
                              items: <String>[
                                "Johor",
                                "Kedah",
                                "Kelantan",
                                "Melaka",
                                "Negeri Sembilan",
                                "Pahang",
                                "Perak",
                                "Perlis",
                                "Pulau Pinang",
                                "Sabah",
                                "Sarawak",
                                "Selangor",
                                "Terengganu",
                "WP Kuala Lumpur",
                "WP Labuan",
                "WP Putrajaya"
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ))),
                    SizedBox(height: 16),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
                      child: Text(
                        'country'.tr() + ' *',
                        style:
                            boldTextStyle(color: appTextColorPrimary, size: 15),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: GestureDetector(
                        onTap: (() {
                          showCountryPicker(
                            context: context,
                            //Optional.  Can be used to exclude(remove) one ore more country from the countries list (optional).
                            // exclude: <String>['KN', 'MF'],
                            favorite: <String>['MY'],
                            //Optional. Shows phone code before the country name.
                            // showPhoneCode: true,
                            onSelect: (Country chooseCountry) {
                              print(
                                  'Select country: ${chooseCountry.displayName}');
                              setState(() {
                                country = chooseCountry.name;
                                countryCode = chooseCountry.countryCode;
                              });
                            },
                            // Optional. Sets the theme for the country list picker.
                            countryListTheme: CountryListThemeData(
                              // Optional. Sets the border radius for the bottomsheet.
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(40.0),
                                topRight: Radius.circular(40.0),
                              ),
                              // Optional. Styles the search field.
                              inputDecoration: InputDecoration(
                                labelText: 'Search',
                                hintText: 'Start typing to search',
                                prefixIcon: const Icon(Icons.search),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: const Color(0xFF8C98A8)
                                        .withOpacity(0.2),
                                  ),
                                ),
                              ),
                              // Optional. Styles the text in the search field
                              searchTextStyle: TextStyle(
                                color: Colors.blue,
                                fontSize: 18,
                              ),
                            ),
                          );
                        }),
                        child: Container(
                            padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                            width: size.width * 0.8,
                            height: 50,
                            decoration: BoxDecoration(
                                color: appTextBackground,
                                borderRadius: BorderRadius.circular(40),
                                border: Border.all(width: 0.8)),
                            child: Row(children: [
                              Expanded(
                                  child: Text(
                                country,
                                style: TextStyle(fontSize: 16),
                              )),
                              Icon(
                                Icons.arrow_downward,
                                color: Color(0xFF696767),
                              ),
                            ])),
                      ),
                    ),
                    SizedBox(height: 25),
                    Center(
                      child: Container(
                        width: width * 0.9,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            backgroundColor: kPrimaryColor,
                          ),
                          onPressed: () {
                            FocusScope.of(context).requestFocus(FocusNode());
                            if (_formKey.currentState!.validate()) {
                              // No any error in validation
                              _formKey.currentState!.save();
                              context
                                  .read<UpdateDetailCubit>()
                                  .updateUserDetails(
                                      // userToken: userToken,
                                      name: nameController.text,
                                      phone3: phone3,
                                      phoneNo: phoneController.text,
                                      idNo: idController.text,
                                      email: emailController.text,
                                      idType: idType,
                                      citizen: citizenCode,
                                      gender: gender,
                                      dobDay: dobDay,
                                      dobMonthIndex: dobMonthIndex,
                                      dobYear: dobYear,
                                      race: race,
                                      address1: address1Controller.text,
                                      address2: address2Controller.text,
                                      postcode: postcodeController.text,
                                      state: userState,
                                      city: cityController.text,
                                      country: countryCode);
                            } else {
                              return;
                              // validation error
                              // setState(() {
                              //   _validate = true;
                              // });
                            }
                          },
                          // textColor: appWhite,
                          // elevation: 4,
                          // shape: RoundedRectangleBorder(
                          //     borderRadius: BorderRadius.circular(80.0)),
                          // padding: EdgeInsets.all(0.0),
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.all(18.0),
                              child: BlocConsumer<UpdateDetailCubit,
                                  UpdateDetailState>(
                                listener: (context, state) {
                                  if (state is UpdateDetailLoaded) {
                                    if (state.updateSuccess)
                                      Navigator.of(context).pop();
                                  }
                                },
                                builder: (context, state) {
                                  if (state is UpdateDetailLoading) {
                                    return SpinKitFadingCircle(
                                      color: Colors.white,
                                      size: 14,
                                    );
                                  } else {
                                    return Text(
                                      'update'.tr(),
                                      style: TextStyle(fontSize: 18),
                                      textAlign: TextAlign.center,
                                    );
                                  }
                                },
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 25),
                  ],
                ),
              ),
            ));
          } else {
            return SpinKitFadingCircle(
              color: Colors.white,
              size: 18,
            );
          }
        }));
  }
}
