import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/Jovian/jovian.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class JovianRepositories {
  static Future<String> getMerchantToken() async {
    final Map<String, String> map = {
      'id_merchant': SHOPIFY_MERCHANT_ID,
      'token': TOKEN,
    };

    //print('calling post GET_PROFILE');

    final response = await http.post(
      Uri.parse('$API_SHOPIFY/get_merchant_token'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);
    var data = json.decode(response.body);

    String token = data["Data"]["api_token"];
    return token;
  }

  static Future<http.Response> checkRegister() async {
    String merchantToken = await getMerchantToken();
    String id = await SecureStorage().readSecureData('guestId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_merchant': SHOPIFY_MERCHANT_ID,
      'id_selangkah_user': id,
      'api_token': merchantToken,
    };

    //print('calling post GET_PROFILE');

    final response = await http.post(
      Uri.parse('$API_SHOPIFY/check_registered_shopify'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    return response;

    // var data = json.decode(response.body);

    // String token = data["Data"]["api_token"];
    // return token;
  }

  static Future<void> registerUser() async {
    String id = await SecureStorage().readSecureData('guestId');
    String merchantToken = await getMerchantToken();

    final Map<String, String> map = {
      'id_merchant': SHOPIFY_MERCHANT_ID,
      'token': TOKEN,
      'id_selangkah_user': id,
      'api_token': merchantToken,
    };

    print(map);

    print('calling post register_customer');

    final response = await http.post(
      Uri.parse('$API_SHOPIFY/register_customer'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print(response.body);

    print('done register');
  }

  static Future<String> getMultipassLink(UserData userData) async {
    final Map<String, String> map = {
      'id_merchant': SHOPIFY_MERCHANT_ID,
      'token': TOKEN,
      'email': userData.data!.email!,
      'created_at': '${userData.data!.createdAt}',
    };

    //print('calling post GET_PROFILE');

    final response = await http.post(
      Uri.parse('$API_SHOPIFY/shopify_multipass_link'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    var data = json.decode(response.body);

    String link = data["Data"];
    return link;
  }
}
