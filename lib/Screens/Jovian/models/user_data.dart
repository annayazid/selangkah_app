// To parse this JSON data, do
//
//     final userData = userDataFromJson(jsonString);

import 'dart:convert';

UserData userDataFromJson(String str) => UserData.fromJson(json.decode(str));

String userDataToJson(UserData data) => json.encode(data.toJson());

class UserData {
  UserData({
    this.code,
    this.data,
  });

  int? code;
  Data? data;

  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
        code: json["Code"],
        data: Data.fromJson(json["Data"]),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data!.toJson(),
      };
}

class Data {
  Data({
    this.email,
    this.createdAt,
  });

  String? email;
  DateTime? createdAt;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        email: json["email"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "email": email,
        "created_at": createdAt!.toIso8601String(),
      };
}
