part of 'check_user_cubit.dart';

@immutable
abstract class CheckUserState {
  const CheckUserState();
}

class CheckUserInitial extends CheckUserState {
  const CheckUserInitial();
}

class CheckUserLoading extends CheckUserState {
  const CheckUserLoading();
}

class CheckUserCompleteProfile extends CheckUserState {
  const CheckUserCompleteProfile();
}

class CheckUserIncompleteProfile extends CheckUserState {
  const CheckUserIncompleteProfile();
}

class CheckUserNotRegister extends CheckUserState {
  const CheckUserNotRegister();
}

class CheckUserRegister extends CheckUserState {
  final String link;

  CheckUserRegister(this.link);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CheckUserRegister && other.link == link;
  }

  @override
  int get hashCode => link.hashCode;
}
