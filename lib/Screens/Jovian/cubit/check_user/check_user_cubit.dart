import 'package:bloc/bloc.dart';
import 'package:http/http.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Bingkas/bingkas.dart';
import 'package:selangkah_new/Screens/Jovian/jovian.dart';

part 'check_user_state.dart';

class CheckUserCubit extends Cubit<CheckUserState> {
  CheckUserCubit() : super(CheckUserInitial());

  Future<void> checkUser() async {
    RawProfile profile = await BingkasRepositories.getProfile();

    String? name = profile.data![2];
    String? idCard = profile.data![4];
    String? idType = profile.data![17];
    String? citizenship = profile.data![6];
    String? gender = profile.data![7];
    String? dob = profile.data![8];
    String? email = profile.data![15];
    String? phone = profile.data![19];
    String? address1 = profile.data![9];
    String? postcode = profile.data![11];
    String? city = profile.data![12];
    String? state = profile.data![13];
    String? country = profile.data![14];

    if (name == null ||
        idCard == null ||
        idType == null ||
        citizenship == null ||
        gender == null ||
        dob == null ||
        email == null ||
        phone == null ||
        address1 == null ||
        postcode == null ||
        city == null ||
        state == null ||
        country == null) {
      print('incomplete');
      emit(CheckUserIncompleteProfile());
    } else {
      print('complete');

      Response register = await JovianRepositories.checkRegister();

      print(register.body);

      if (register.body.contains('false')) {
        //register user here
        await JovianRepositories.registerUser();

        //call balik function

        await Future.delayed(Duration(seconds: 5));
        checkUser();
      } else {
        //get multipass link

        UserData userData = userDataFromJson(register.body);
        String link = await JovianRepositories.getMultipassLink(userData);

        emit(CheckUserRegister(link));
      }
    }
  }
}
