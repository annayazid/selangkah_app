import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:selangkah_new/Screens/Jovian/jovian.dart';
import 'package:selangkah_new/Screens/VerifyProfile/verify_profile.dart';
import 'package:selangkah_new/Screens/WebView/webview.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class JovianScreen extends StatefulWidget {
  const JovianScreen({Key? key}) : super(key: key);

  @override
  State<JovianScreen> createState() => _JovianScreenState();
}

class _JovianScreenState extends State<JovianScreen> {
  @override
  void initState() {
    context.read<CheckUserCubit>().checkUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Jovian Selcare Pharmacy'),
      ),
      body: BlocConsumer<CheckUserCubit, CheckUserState>(
        listener: (context, state) {
          if (state is CheckUserIncompleteProfile) {
            Fluttertoast.showToast(
              msg: "updateProfile".tr(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
            );

            //Redirect edit profile
            Navigator.of(context)
                .push(
              MaterialPageRoute(
                builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => GetUserDetailsCubit(),
                    ),
                    BlocProvider(
                      create: (context) => UpdateDetailCubit(),
                    ),
                  ],
                  child: VerifyProfile(),
                ),
              ),
            )
                .then((value) async {
              await Future.delayed(Duration(seconds: 1));
              String? email = await SecureStorage().readSecureData('email');

              if (email == null || email == 'null') {
                Navigator.of(context).pop();
              } else {
                // Navigator.of(context).pop();

                context.read<CheckUserCubit>().checkUser();
              }
            });
          }

          if (state is CheckUserRegister) {
            print('reirect to ${state.link}');
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => SelangkahWebview(
                  appBarTitle: 'Jovian Selcare Pharmacy',
                  url: state.link,
                ),
              ),
            );
          }
        },
        builder: (context, state) {
          return Center(
            child: SpinKitFadingCircle(
              color: kPrimaryColor,
              size: 35,
            ),
          );
        },
      ),
    );
  }
}
