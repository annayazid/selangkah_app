// To parse this JSON data, do
//
//     final inbox = inboxFromJson(jsonString);

import 'dart:convert';

Inbox inboxFromJson(String str) => Inbox.fromJson(json.decode(str));

String inboxToJson(Inbox data) => json.encode(data.toJson());

class Inbox {
  Inbox({
    this.code,
    this.data,
  });

  int? code;
  List<InboxData>? data;

  factory Inbox.fromJson(Map<String, dynamic> json) => Inbox(
        code: json["Code"],
        data: json["Data"] == null
            ? []
            : List<InboxData>.from(
                json["Data"]!.map((x) => InboxData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "Data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class InboxData {
  InboxData({
    this.id,
    this.title,
    this.message,
    this.imgUrl,
    this.createdTs,
    this.idSelangkahUser,
    this.canDelete,
  });

  String? id;
  String? title;
  String? message;
  String? imgUrl;
  DateTime? createdTs;
  String? idSelangkahUser;
  String? canDelete;

  factory InboxData.fromJson(Map<String, dynamic> json) => InboxData(
        id: json["id"],
        title: json["title"],
        message: json["message"],
        imgUrl: json["img_url"],
        createdTs: json["created_ts"] == null
            ? null
            : DateTime.parse(json["created_ts"]),
        idSelangkahUser: json["id_selangkah_user"],
        canDelete: json["can_delete"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "message": message,
        "img_url": imgUrl,
        "created_ts": createdTs?.toIso8601String(),
        "id_selangkah_user": idSelangkahUser,
        "can_delete": canDelete,
      };
}
