import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Inbox/inbox.dart';

part 'get_inbox_state.dart';

class GetInboxCubit extends Cubit<GetInboxState> {
  GetInboxCubit() : super(GetInboxInitial());

  Future<void> getInbox() async {
    emit(GetInboxLoading());

    Inbox inbox = await InboxRepositories.getInbox();

    emit(GetInboxLoaded(inbox));
  }
}
