part of 'get_inbox_cubit.dart';

@immutable
abstract class GetInboxState {
  const GetInboxState();
}

class GetInboxInitial extends GetInboxState {
  const GetInboxInitial();
}

class GetInboxLoading extends GetInboxState {
  const GetInboxLoading();
}

class GetInboxLoaded extends GetInboxState {
  final Inbox inbox;

  GetInboxLoaded(this.inbox);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetInboxLoaded && other.inbox == inbox;
  }

  @override
  int get hashCode => inbox.hashCode;
}
