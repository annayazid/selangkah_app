part of 'delete_inbox_cubit.dart';

@immutable
abstract class DeleteInboxState {
  const DeleteInboxState();
}

class DeleteInboxInitial extends DeleteInboxState {
  const DeleteInboxInitial();
}

class DeleteInboxLoading extends DeleteInboxState {
  const DeleteInboxLoading();
}

class DeleteInboxDeleted extends DeleteInboxState {
  const DeleteInboxDeleted();
}

class DeleteInboxFailed extends DeleteInboxState {
  const DeleteInboxFailed();
}
