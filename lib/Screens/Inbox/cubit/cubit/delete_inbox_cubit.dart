import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/Inbox/inbox.dart';

part 'delete_inbox_state.dart';

class DeleteInboxCubit extends Cubit<DeleteInboxState> {
  DeleteInboxCubit() : super(DeleteInboxInitial());

  Future<void> deleteInbox(String idInbox) async {
    emit(DeleteInboxLoading());

    try {
      // await Future.delayed(Duration(seconds: 1));
      bool delete = await InboxRepositories.deleteInbox(idInbox);
      if (delete) {
        emit(DeleteInboxDeleted());
      } else {
        emit(DeleteInboxFailed());
      }
    } catch (e) {
      print('masuk catch');
      await Future.delayed(Duration(seconds: 1));
      emit(DeleteInboxFailed());
    }
  }
}
