import 'package:flutter/material.dart';

import 'package:date_format/date_format.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:selangkah_new/Screens/Inbox/inbox.dart';
import 'package:url_launcher/url_launcher.dart';

class InboxDetails extends StatelessWidget {
  final String title;
  final String message;
  final String? imgUrl;
  final DateTime createdTs;

  const InboxDetails(
      {Key? key,
      required this.title,
      required this.message,
      required this.imgUrl,
      required this.createdTs})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return InboxScaffold(
      appBarTitle: 'Inbox'.tr(),
      child: Container(
        height: height - AppBar().preferredSize.height,
        width: width,
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Column(
                    children: [
                      SizedBox(height: 20),
                      Container(
                        padding: EdgeInsets.all(15),
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black,
                              offset: Offset(0.0, 1.0), //(x,y)
                              blurRadius: 2.0,
                            ),
                          ],
                        ),
                        child: Center(
                          child: Text(
                            title,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        padding: EdgeInsets.all(20),
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black,
                              offset: Offset(0.0, 1.0), //(x,y)
                              blurRadius: 2.0,
                            ),
                          ],
                        ),
                        child: Center(
                          child: Column(
                            children: [
                              Text(
                                formatDate(
                                  createdTs,
                                  [yyyy, '-', mm, '-', dd],
                                ),
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 15,
                                ),
                              ),
                              SizedBox(height: 10),
                              if (imgUrl != null) ...[
                                Image.network(
                                  imgUrl!,
                                ),
                                SizedBox(height: 10),
                              ],
                              Linkify(
                                text: message,
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                                options: LinkifyOptions(humanize: false),
                                onOpen: (link) {
                                  launchUrl(
                                    Uri.parse(link.url),
                                    mode: LaunchMode.externalApplication,
                                  );
                                },
                              ),
                              // Text(
                              //   message,
                              //   style: TextStyle(
                              //     fontSize: 20,
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
