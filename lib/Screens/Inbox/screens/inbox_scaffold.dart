import 'package:flutter/material.dart';
import 'package:selangkah_new/utils/constants.dart';

class InboxScaffold extends StatelessWidget {
  final String appBarTitle;
  final Widget child;

  const InboxScaffold(
      {Key? key, required this.appBarTitle, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/images/Scaffold/selangkah_logo.png",
          width: width * 0.45,
        ),
      ),
      body: Column(
        children: [
          AppBar(
            centerTitle: true,
            automaticallyImplyLeading: true,
            backgroundColor: kPrimaryColor,
            title: Text(
              appBarTitle,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 15.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontFamily: "Roboto",
              ),
            ),
          ),
          Container(
            height: height - AppBar().preferredSize.height,
            width: width,
            child: child,
          ),
        ],
      ),
    );
  }
}
