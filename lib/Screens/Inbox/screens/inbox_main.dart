import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:selangkah_new/Screens/Inbox/inbox.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';
import 'package:shimmer/shimmer.dart';

class InboxMain extends StatefulWidget {
  const InboxMain({Key? key}) : super(key: key);

  @override
  State<InboxMain> createState() => _InboxMainState();
}

class _InboxMainState extends State<InboxMain> {
  @override
  void initState() {
    GlobalFunction.screenJourney('6');
    context.read<GetInboxCubit>().getInbox();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top -
        AppBar().preferredSize.height;

    return InboxScaffold(
      appBarTitle: 'Inbox',
      child: Container(
        height: height,
        child: BlocBuilder<GetInboxCubit, GetInboxState>(
          builder: (context, state) {
            if (state is GetInboxLoaded) {
              return InboxLoadedWidget(inbox: state.inbox);
            } else {
              //shimmer
              return InboxLoadingWidget();
            }
          },
        ),
      ),
    );
  }
}

class InboxLoadingWidget extends StatelessWidget {
  const InboxLoadingWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top -
        AppBar().preferredSize.height;
    return ListView.builder(
      shrinkWrap: true,
      itemCount: 10,
      itemBuilder: (BuildContext context, int index) {
        return Shimmer.fromColors(
          baseColor: Colors.grey.shade300,
          highlightColor: Colors.white,
          period: Duration(seconds: 2),
          child: Container(
            margin: EdgeInsets.all(10),
            height: height * 0.15,
            // margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  offset: Offset(0, 0),
                  blurRadius: 5,
                  // spreadRadius: 3,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class InboxLoadedWidget extends StatelessWidget {
  final Inbox inbox;

  const InboxLoadedWidget({super.key, required this.inbox});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return BlocConsumer<DeleteInboxCubit, DeleteInboxState>(
      listener: (context, state) {
        if (state is DeleteInboxDeleted) {
          context.read<GetInboxCubit>().getInbox();
          Fluttertoast.showToast(
            msg: "Message deleted",
            toastLength: Toast.LENGTH_LONG,
            backgroundColor: Colors.black,
            textColor: Colors.white,
            fontSize: 16.0,
          );
        }
      },
      builder: (context, state) {
        return ListView.builder(
          shrinkWrap: true,
          itemCount: inbox.data!.length,
          itemBuilder: (BuildContext context, int index) {
            if (state is DeleteInboxLoading) {
              //shimmer
              return InboxLoadingWidget();
            } else {
              return GestureDetector(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => InboxDetails(
                        title: inbox.data![index].title!,
                        createdTs: inbox.data![index].createdTs!,
                        message: inbox.data![index].message!,
                        imgUrl: inbox.data![index].imgUrl,
                      ),
                    ),
                  );
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  child: Slidable(
                    child: Container(
                      // margin: EdgeInsets.all(10),
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            offset: Offset(0, 0),
                            blurRadius: 5,
                            // spreadRadius: 3,
                          ),
                        ],
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // SizedBox(height: 10),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/Inbox/selangkah_logo_small.png"),
                                width: width * 0.06,
                              ),
                              SizedBox(width: 10),
                              Expanded(
                                child: Text(
                                  inbox.data![index].title!.toUpperCase(),
                                  // LoremText,
                                  style: TextStyle(
                                    // color: Color(0xff505660),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                  ),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              Icon(
                                Icons.navigate_next_sharp,
                                // color: Colors.blue[700],
                                color: kPrimaryColor,
                              ),
                            ],
                          ),
                          SizedBox(height: 10),
                          Text(
                            inbox.data![index].message!,
                            // LoremText,
                            style: TextStyle(
                              // color: Color(0xff505660),
                              fontSize: 13,
                            ),
                            maxLines: 5,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.justify,
                          ),
                          SizedBox(height: 10),
                          Text(
                            formatDate(
                              inbox.data![index].createdTs!,
                              [dd, '-', mm, '-', yyyy],
                            ),
                            style: TextStyle(
                              fontSize: 12,
                              color: Color(0xff505660),
                            ),
                          ),
                          // SizedBox(height: 10),
                        ],
                      ),
                    ),
                    endActionPane: ActionPane(
                      extentRatio: 0.25,
                      motion: ScrollMotion(),
                      children: [
                        SlidableAction(
                          onPressed: (context) {
                            context
                                .read<DeleteInboxCubit>()
                                .deleteInbox(inbox.data![index].id!);
                          },
                          backgroundColor: Color(0xFFFE4A49),
                          foregroundColor: Colors.white,
                          icon: Icons.delete,
                          label: 'Delete',
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }
          },
        );
      },
    );
  }
}
