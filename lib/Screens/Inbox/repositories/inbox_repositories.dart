import 'package:http/http.dart' as http;
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/Inbox/inbox.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class InboxRepositories {
  static Future<Inbox> getInbox() async {
    SecureStorage secureStorage = SecureStorage();

    String id = await secureStorage.readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    log('call inbox');

    final response = await http.post(
      Uri.parse('$API_URL/get_noti_inbox'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    log(response.body);

    Inbox notification = inboxFromJson(response.body);

    return notification;
  }

  static Future<bool> deleteInbox(String id) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'id_noti_inbox': id,
    };

    log('calling delete');

    final response = await http.post(
      Uri.parse('$API_URL/del_noti_inbox'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );
    log(response.body);
    if (response.body.contains('true')) {
      return true;
    } else {
      return false;
    }
  }
}
