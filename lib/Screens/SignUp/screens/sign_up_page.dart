import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Screens/PDFInAppScreen/pdf_in_app_screen.dart';
import 'package:selangkah_new/Screens/SelangkahAppBarScaffold/scaffold.dart';
import 'package:selangkah_new/Screens/SignUp/sign_up.dart';
import 'package:selangkah_new/Screens/VerifyOTP/verifyOTP.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final TextEditingController _fullNameController = TextEditingController();
  final TextEditingController _phoneNumController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  // bool _validate = false;
  bool passwordVisibleOff = true;
  int? _groupValue;
  bool? termCond = false;

  @override
  void initState() {
    GlobalFunction.screenJourney('2');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top -
        MediaQuery.of(context).padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return TopBarScaffold(
        title: 'signUp'.tr(),
        content: SingleChildScrollView(
          child: Center(
            child: Container(
              width: width * 0.9,
              padding: EdgeInsets.symmetric(vertical: 25),
              height: height,
              child: Form(
                key: _formKey,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'signup'.tr(),
                      style:
                          TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 15),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(width: 1, color: Colors.grey)),
                      child: TextFormField(
                        validator: validateFullName,
                        controller: _fullNameController,
                        keyboardType: TextInputType.text,
                        cursorColor: kPrimaryColor,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(20, 18, 0, 18),
                          icon: Icon(
                            Icons.person,
                            color: kPrimaryColor,
                          ),
                          hintText: 'fullname'.tr().toString(),
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                    SizedBox(height: 15),
                    Container(
                      // margin: EdgeInsets.symmetric(vertical: 10),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      // width: width * 0.8,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(width: 1, color: Colors.grey),
                      ),
                      child: TextFormField(
                        validator: validatePhoneNum,
                        controller: _phoneNumController,
                        keyboardType: TextInputType.phone,
                        cursorColor: kPrimaryColor,
                        decoration: InputDecoration(
                          // contentPadding: EdgeInsets.fromLTRB(20, 18, 0, 18),
                          icon: Icon(
                            Icons.phone,
                            color: kPrimaryColor,
                          ),
                          hintText: 'phonenumber'.tr(),
                          prefixText: "+6 ",
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                    SizedBox(height: 15),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(width: 1, color: Colors.grey)),
                      child: TextFormField(
                        validator: validatePassword,
                        controller: _passwordController,
                        keyboardType: TextInputType.visiblePassword,
                        cursorColor: kPrimaryColor,
                        obscureText: passwordVisibleOff,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(20, 18, 0, 18),
                          errorMaxLines: 4,
                          icon: Icon(
                            Icons.lock,
                            color: kPrimaryColor,
                          ),
                          hintText: 'password_placeholder'.tr().toString(),
                          border: InputBorder.none,
                          suffixIcon: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              passwordVisibleOff
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                              color: Theme.of(context).primaryColorDark,
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                passwordVisibleOff = !passwordVisibleOff;
                              });
                            },
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 15),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(width: 1, color: Colors.grey),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'choose_language'.tr(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                            ),
                          ),
                          SizedBox(height: 5),
                          GestureDetector(
                            onTap: () {
                              // _groupValue = 0;
                              // EasyLocalization.of(context).locale = Locale('ms', 'MY');
                            },
                            child: Row(
                              children: [
                                Radio(
                                  activeColor: kPrimaryColor,
                                  value: 0,
                                  groupValue: _groupValue,
                                  onChanged: (newValue) {
                                    setState(() {
                                      _groupValue = newValue;
                                      context.setLocale(Locale('ms', 'MY'));
                                    });
                                  },
                                ),
                                Text('Bahasa Malaysia'),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              // _groupValue = 0;
                              // EasyLocalization.of(context).locale =
                              //     Locale('en', 'US');
                            },
                            child: Row(
                              children: [
                                Radio(
                                  activeColor: kPrimaryColor,
                                  value: 1,
                                  groupValue: _groupValue,
                                  onChanged: (newValue) {
                                    setState(() {
                                      _groupValue = newValue;
                                      context.setLocale(Locale('en', 'US'));
                                    });
                                  },
                                ),
                                Text('English'),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: 24.0,
                            width: 24.0,
                            child: Checkbox(
                              activeColor: kPrimaryColor,
                              value: termCond,
                              onChanged: (bool? value) {
                                print(value);
                                setState(() {
                                  //bloc.add(TermsCondition(tnc: value));
                                  termCond = value;
                                });
                              },
                            ),
                          ),
                          SizedBox(width: 15),
                          //and, privacy, termsofuse
                          Flexible(
                            child: RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: '${'accepttnc'.tr()} ',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                  TextSpan(
                                    text: '${'privacy'.tr()} ',
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () =>
                                          // launch(GlobalVariables.privacy),

                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  PDFInAppPageScreen(
                                                url: GlobalVariables.privacy,
                                                title:
                                                    'privacy'.tr().toString(),
                                              ),
                                            ),
                                          ),
                                    style: TextStyle(
                                      color: Color(0xFFfc4a1a),
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                  TextSpan(
                                    text: '${'and'.tr()} ',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                  TextSpan(
                                    text: '${'termsofuse'.tr()}.',
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () =>
                                          // launch(GlobalVariables.termsofuse),

                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  PDFInAppPageScreen(
                                                url: GlobalVariables.termsofuse,
                                                title: 'termsofuse'
                                                    .tr()
                                                    .toString(),
                                              ),
                                            ),
                                          ),
                                    style: TextStyle(
                                      color: Color(0xFFfc4a1a),
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Spacer(),
                    Container(
                      width: width * 0.9,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          backgroundColor:
                              termCond! ? kPrimaryColor : Colors.grey,
                        ),
                        onPressed: () {
                          final isValid = _formKey.currentState!.validate();
                          if (!isValid) {
                            print(_fullNameController);
                            print(_phoneNumController);
                            print(_passwordController);

                            return;
                          } else {
                            _formKey.currentState!.save();
                          }
                          if (isValid && termCond!) {
                            // otp verification
                            context.read<UserRegCubit>().userRegister(
                                  _phoneNumController.text,
                                );

                            //skip otp verification
                            // context.read<UserRegCubit>().skipOTP(
                            //     language: 'EN',
                            //     from: 'register',
                            //     phoneNum: _phoneNumController.text,
                            //     username: _fullNameController.text,
                            //     pass: _passwordController.text);
                          }
                        },
                        child: BlocConsumer<UserRegCubit, UserRegState>(
                          listener: (context, state) {
                            print(state.toString());
                            if (state is UserVerifyOTP) {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => BlocProvider(
                                    create: (context) => OtpVerifyCubit(),
                                    child: VerifyOTP(
                                      fromScreen: 'register',
                                      username: _fullNameController.text,
                                      phone: _phoneNumController.text,
                                      password: _passwordController.text,
                                    ),
                                  ),
                                ),
                              );
                            }
                            if (state is UserSkipOTPRegister) {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => HomePageScreen(),
                                ),
                              );
                            }
                          },
                          builder: (context, state) {
                            if (state is UserRegLoading) {
                              return Padding(
                                padding: EdgeInsets.all(18.0),
                                child: SpinKitFadingCircle(
                                  color: Colors.white,
                                  size: 18,
                                ),
                              );
                            } else {
                              return Container(
                                child: Center(
                                  child: Padding(
                                    padding: EdgeInsets.all(18.0),
                                    child: Text(
                                      'submit'.tr(),
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16),
                                      // textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              );
                            }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  String? validateFullName(String? value) {
    print(value);
    if (value!.length == 0) {
      return 'namerequired'.tr().toString();
    }
    return null;
  }

  String? validatePhoneNum(String? value) {
    print(value);
    String patttern = r'(^[0-9]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value!.length == 0) {
      return 'phonerequired'.tr().toString();
    } else if (value.length < 10) {
      return 'phone10'.tr().toString();
    } else if (value.length >= 12) {
      return 'phone12'.tr().toString();
    } else if (!regExp.hasMatch(value)) {
      return 'phonedigit'.tr().toString();
    } else if (!value.startsWith('01')) {
      return 'phon01'.tr().toString();
    }
    return null;
  }

  String? validatePassword(String? value) {
    print(value);
    //regex for the password matches (8 length alphanumeric with number)
    RegExp reg = RegExp(r'^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$');

    if (value!.length == 0) {
      return 'passwordrequired'.tr();
    }

    if (!reg.hasMatch(value)) {
      return 'invalid_password_format'.tr();
    }
    return null;
  }
}
