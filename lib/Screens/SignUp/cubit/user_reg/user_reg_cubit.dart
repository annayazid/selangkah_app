import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/SignIn/sign_in.dart';
import 'package:selangkah_new/Screens/VerifyOTP/verifyOTP.dart';

part 'user_reg_state.dart';

class UserRegCubit extends Cubit<UserRegState> {
  UserRegCubit() : super(UserRegInitial());

  Future<void> userRegister(String phoneNum) async {
    emit(UserRegLoading());

    bool userReg = await SignInRepositories.checkUserReg(phoneNum);

    print(userReg);

    if (!userReg) {
      var connectivityResult = await (Connectivity().checkConnectivity());
      // I am connected to a mobile network.
      // I am connected to a wifi network.
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        emit(UserVerifyOTP());
      } else {
        Fluttertoast.showToast(msg: "nointernet".tr());
        emit(UserNoInternet());
      }
    } else {
      Fluttertoast.showToast(msg: "haveuser".tr());
      emit(UserAlreadyHaveAccount());
    }
  }

  Future<void> skipOTP(
      {required String language,
      required String from,
      required String phoneNum,
      required String username,
      required String pass}) async {
    emit(SkipOtpLoading());
    if (from == 'register') {
      String source = '';
      if (Platform.isAndroid) {
        source = 'android';
      } else if (Platform.isIOS) {
        source = 'ios';
      }

      String userSignUp = await VerifyOtpNewRepositories.signupNewUser(
          language, username, phoneNum, pass, source);

      if (userSignUp.contains('false')) {
        Fluttertoast.showToast(msg: 'userRegistered'.tr());
      } else if (userSignUp.contains('500')) {
        Fluttertoast.showToast(msg: 'registerFailed'.tr());
      } else {
        await SignInRepositories.checkUserLogin(phoneNum, pass);
        await AppSplashRepositories.updateUserToken();
        await AppSplashRepositories.getUserProfile();
        emit(UserSkipOTPRegister());
      }
    } else {}
  }
}
