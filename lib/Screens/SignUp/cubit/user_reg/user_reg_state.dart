part of 'user_reg_cubit.dart';

@immutable
abstract class UserRegState {
  const UserRegState();
}

class UserRegInitial extends UserRegState {
  const UserRegInitial();
}

class UserRegLoading extends UserRegState {
  const UserRegLoading();
}

class UserAlreadyHaveAccount extends UserRegState {
  const UserAlreadyHaveAccount();
}

class UserNoInternet extends UserRegState {
  const UserNoInternet();
}

class UserVerifyOTP extends UserRegState {
  const UserVerifyOTP();
}

class SkipOtpLoading extends UserRegState {
  const SkipOtpLoading();
}

class UserSkipOTPRegister extends UserRegState {
  const UserSkipOTPRegister();
}
