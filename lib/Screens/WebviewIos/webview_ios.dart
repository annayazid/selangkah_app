import 'dart:io';

import 'package:flutter/material.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:selangkah_new/utils/constants.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebviewIOS extends StatefulWidget {
  final String appBarTitle;
  final String url;

  const WebviewIOS({super.key, required this.appBarTitle, required this.url});

  @override
  State<WebviewIOS> createState() => _WebviewIOSState();
}

class _WebviewIOSState extends State<WebviewIOS> {
  bool isLoaded = false;

  late WebViewController controller;

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          "assets/images/Scaffold/selangkah_logo.png",
          width: width * 0.45,
        ),
      ),
      body: Column(
        children: [
          AppBar(
            centerTitle: true,
            automaticallyImplyLeading: true,
            backgroundColor: kPrimaryColor,
            title: Text(
              widget.appBarTitle,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 15.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontFamily: "Roboto",
              ),
            ),
          ),
          Container(
            height: height - AppBar().preferredSize.height,
            width: width,
            child: Stack(
              children: [
                WebView(
                  backgroundColor: Colors.white,
                  initialUrl: widget.url,
                  javascriptMode: JavascriptMode.unrestricted,
                  onWebViewCreated: (controller) {
                    controller.clearCache();

                    setState(() {
                      isLoaded = true;
                    });
                  },
                ),
                if (!isLoaded)
                  SpinKitRing(
                    color: kPrimaryColor,
                    size: 30,
                    lineWidth: 5,
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
