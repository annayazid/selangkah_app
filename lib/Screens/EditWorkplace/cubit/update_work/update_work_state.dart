part of 'update_work_cubit.dart';

@immutable
abstract class UpdateWorkState {
  const UpdateWorkState();
}

class UpdateWorkInitial extends UpdateWorkState {
  const UpdateWorkInitial();
}

class UpdateWorkLoading extends UpdateWorkState {
  const UpdateWorkLoading();
}

class UpdateWorkFinish extends UpdateWorkState {
  const UpdateWorkFinish();
}
