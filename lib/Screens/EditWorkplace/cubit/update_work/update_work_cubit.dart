import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:meta/meta.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/EditWorkplace/edit_workplace.dart';

part 'update_work_state.dart';

class UpdateWorkCubit extends Cubit<UpdateWorkState> {
  UpdateWorkCubit() : super(UpdateWorkInitial());

  Future<void> updateWork({
    String? id,
    String? address1,
    String? address2,
    String? postcode,
    String? city,
    String? state,
    String? country,
    String? occupation,
  }) async {
    emit(UpdateWorkLoading());

    await EditWorkRepositories.updateWork(
        id: id,
        address1: address1,
        address2: address2,
        postcode: postcode,
        city: city,
        state: state,
        country: country,
        occupation: occupation);

    toast('updateSuccess'.tr());
    emit(UpdateWorkFinish());
  }
}
