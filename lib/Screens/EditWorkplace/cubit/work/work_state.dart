part of 'work_cubit.dart';

@immutable
abstract class WorkState {
  const WorkState();
}

//load work
class WorkInitial extends WorkState {
  const WorkInitial();
}

class WorkLoading extends WorkState {
  const WorkLoading();
}

class WorkLoaded extends WorkState {
  final Work work;

  WorkLoaded(this.work);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WorkLoaded && other.work == work;
  }

  @override
  int get hashCode => work.hashCode;
}

class WorkError extends WorkState {
  const WorkError();
}
