import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/EditWorkplace/edit_workplace.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';

part 'work_state.dart';

class WorkCubit extends Cubit<WorkState> {
  WorkCubit() : super(WorkInitial());

  Future<void> workDetails(String id) async {
    emit(WorkLoading());

    Work work = await EditWorkRepositories.getWork(id);

    emit(WorkLoaded(work));
  }
}
