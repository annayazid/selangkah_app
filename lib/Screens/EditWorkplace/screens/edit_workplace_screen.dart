import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/EditWorkplace/edit_workplace.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/Screens/SelangkahAppBarScaffold/scaffold.dart';
import 'package:selangkah_new/utils/AppColors.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class EditWorkplace extends StatefulWidget {
  @override
  _EditWorkplaceState createState() => _EditWorkplaceState();
}

class _EditWorkplaceState extends State<EditWorkplace> {
  @override
  void initState() {
    getData();
    super.initState();
  }

  Future<void> getData() async {
    final SecureStorage secureStorage = SecureStorage();

    String id = await secureStorage.readSecureData('userId');
    context.read<WorkCubit>().workDetails(id);
  }

  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    return TopBarScaffold(
      title: 'edit_work'.tr(),
      content: Container(
        color: Color(0xFFF5F4F4),
        height: MediaQuery.of(context).size.height,
        child: Center(
          child: SingleChildScrollView(
            child: BlocConsumer<WorkCubit, WorkState>(
              listener: (context, state) {},
              builder: (context, state) {
                return AnimatedSwitcher(
                  duration: Duration(milliseconds: 500),
                  child: () {
                    if (state is WorkLoading) {
                      return WorkLoadingWidget();
                    } else if (state is WorkLoaded) {
                      return WorkLoadedWidget(work: state.work);
                    } else {
                      return WorkLoadingWidget();
                    }
                  }(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

class WorkLoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SpinKitFadingCircle(
      color: kPrimaryColor,
      size: 35,
    );
  }
}

class WorkLoadedWidget extends StatefulWidget {
  final Work work;

  const WorkLoadedWidget({super.key, required this.work});

  @override
  _WorkLoadedWidgetState createState() => _WorkLoadedWidgetState();
}

class _WorkLoadedWidgetState extends State<WorkLoadedWidget> {
  TextEditingController address1Controller = TextEditingController();
  TextEditingController address2Controller = TextEditingController();
  TextEditingController postcodeController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController stateController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController occupationController = TextEditingController();

  @override
  void initState() {
    assignValueData();
    super.initState();
  }

  void assignValueData() {
    print('Sini!!');
    print(widget.work.data![0].workAddress1);
    if (widget.work.data![0].workAddress1 == null) {
      address1Controller.text = '';
    } else {
      address1Controller.text = widget.work.data![0].workAddress1!;
    }

    if (widget.work.data![0].workAddress2 == null) {
      address2Controller.text = '';
    } else {
      address2Controller.text = widget.work.data![0].workAddress2!;
    }

    if (widget.work.data![0].workPostcode == null) {
      postcodeController.text = '';
    } else {
      postcodeController.text = widget.work.data![0].workPostcode!;
    }

    if (widget.work.data![0].workCity == null) {
      cityController.text = '';
    } else {
      cityController.text = widget.work.data![0].workCity!;
    }

    if (widget.work.data![0].workState == null) {
      stateController.text = 'Selangor';
    } else {
      stateController.text = widget.work.data![0].workState!;
    }

    if (widget.work.data![0].workCountry == null) {
      countryController.text = 'Malaysia';
    } else {
      countryController.text = widget.work.data![0].workCountry!;
    }

    if (widget.work.data![0].occupation == null) {
      occupationController.text = '';
    } else {
      occupationController.text = widget.work.data![0].occupation!;
    }
    // if (stateController.text == '' || stateController.text == null) {
    //   stateController.text = 'Selangor';
    // }

    // if (countryController.text == '' || countryController.text == null) {
    //   countryController.text = 'Malaysia';
    // }
  }

  void updateWork() async {
    final SecureStorage secureStorage = SecureStorage();
    String id = await secureStorage.readSecureData('userId');

    context.read<UpdateWorkCubit>().updateWork(
          id: id,
          address1: address1Controller.text,
          address2: address2Controller.text,
          postcode: postcodeController.text,
          city: cityController.text,
          state: stateController.text,
          country: countryController.text,
          occupation: occupationController.text,
        );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // print(stateController.text);
    // print(countryController.text);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
          child: Text(
            'occupation'.tr().toString(),
            style: boldTextStyle(color: appTextColorPrimary, size: 15),
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
          child: TextFormField(
            controller: occupationController,
            //readOnly: false,
            style: primaryTextStyle(size: 18, color: appTextColorPrimary),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(26, 18, 4, 18),
              filled: true,
              fillColor: appTextBackground,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                // borderSide: BorderSide(color: appTextBackground, width: 0.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                // borderSide: BorderSide(color: appTextBackground, width: 0.0),
              ),
            ),
            keyboardType: TextInputType.text,
            //validator: validatePhoneNum,
            onSaved: (String? val) {
              // if (val == null) {
              //   occupationController.text = val!;
              // }

              occupationController.text = val!;
            },
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
          child: Text(
            'work_address'.tr().toString(),
            style: boldTextStyle(color: appTextColorPrimary, size: 15),
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
          child: TextFormField(
            controller: address1Controller,
            //readOnly: false,
            style: primaryTextStyle(size: 18, color: appTextColorPrimary),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(26, 18, 4, 18),
              filled: true,
              fillColor: appTextBackground,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                // borderSide: BorderSide(color: appTextBackground, width: 0.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                // borderSide: BorderSide(color: appTextBackground, width: 0.0),
              ),
            ),
            keyboardType: TextInputType.text,
            //validator: validatePhoneNum,
            onSaved: (String? val) {
              address1Controller.text = val!;
            },
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
          child: TextFormField(
            controller: address2Controller,
            //readOnly: false,
            style: primaryTextStyle(size: 18, color: appTextColorPrimary),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(26, 18, 4, 18),
              filled: true,
              fillColor: appTextBackground,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                // borderSide: BorderSide(color: appTextBackground, width: 0.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                // borderSide: BorderSide(color: appTextBackground, width: 0.0),
              ),
            ),
            keyboardType: TextInputType.name,
            //validator: validatePhoneNum,
            onSaved: (String? val) {
              address2Controller.text = val!;
            },
          ),
        ),
        SizedBox(height: 16),
        Padding(
          padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
          child: Text(
            'postcode'.tr().toString(),
            style: boldTextStyle(color: appTextColorPrimary, size: 15),
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
          child: TextFormField(
            controller: postcodeController,
            //readOnly: false,
            style: primaryTextStyle(size: 18, color: appTextColorPrimary),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(26, 18, 4, 18),
              filled: true,
              fillColor: appTextBackground,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                // borderSide: BorderSide(color: appTextBackground, width: 0.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                // borderSide: BorderSide(color: appTextBackground, width: 0.0),
              ),
            ),
            keyboardType: TextInputType.number,
            validator: (value) {
              if (value!.length != 5) {
                return 'Postcode must be length of 5 digit';
              } else {
                return null;
              }
            },
            onSaved: (String? val) {
              postcodeController.text = val!;
            },
          ),
        ),
        SizedBox(height: 16),
        Padding(
          padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
          child: Text(
            'city'.tr().toString(),
            style: boldTextStyle(color: appTextColorPrimary, size: 15),
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
          child: TextFormField(
            controller: cityController,
            //readOnly: false,
            style: primaryTextStyle(size: 18, color: appTextColorPrimary),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(26, 18, 4, 18),
              filled: true,
              fillColor: appTextBackground,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                // borderSide: BorderSide(color: appTextBackground, width: 0.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                // borderSide: BorderSide(color: appTextBackground, width: 0.0),
              ),
            ),
            keyboardType: TextInputType.name,
            //validator: validatePhoneNum,
            onSaved: (String? val) {
              cityController.text = val!;
            },
          ),
        ),
        SizedBox(height: 16),
        Padding(
          padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
          child: Text(
            'state'.tr().toString(),
            style: boldTextStyle(color: appTextColorPrimary, size: 15),
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
          child: Container(
            padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
            decoration: BoxDecoration(
                color: appTextBackground,
                borderRadius: BorderRadius.circular(40),
                border: Border.all(width: 0.8)),
            child: DropdownButton<String>(
              isExpanded: true,
              value: stateController.text,
              // stateController.text == '' || stateController.text == null
              // ? 'Selangor'
              // : stateController.text,
              icon: Icon(Icons.arrow_downward),
              iconSize: 24,
              elevation: 16,
              onChanged: (String? newValue) {
                setState(() {
                  stateController.text = newValue!;
                });
              },
              items: <String>[
                "Johor",
                "Kedah",
                "Kelantan",
                "Melaka",
                "Negeri Sembilan",
                "Pahang",
                "Perak",
                "Perlis",
                "Pulau Pinang",
                "Sabah",
                "Sarawak",
                "Selangor",
                "Terengganu",
                "Kuala Lumpur",
                "WP Labuan",
                "WP Putrajaya"
              ].map<DropdownMenuItem<String>>(
                (String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                },
              ).toList(),
            ),
          ),
        ),
        SizedBox(height: 16),
        Padding(
          padding: EdgeInsets.fromLTRB(30, 0, 16, 0),
          child: Text(
            'country'.tr().toString(),
            style: boldTextStyle(color: appTextColorPrimary, size: 15),
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
          child: Container(
            padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
            decoration: BoxDecoration(
                color: appTextBackground,
                borderRadius: BorderRadius.circular(40),
                border: Border.all(width: 0.8)),
            child: DropdownButton<String>(
              isExpanded: true,
              icon: Icon(Icons.arrow_downward),
              iconSize: 24,
              elevation: 16,
              onChanged: (String? newValue) {
                setState(() {
                  countryController.text = newValue!;
                });
              },
              value: countryController.text,
              // countryController.text == '' || countryController.text == null
              //     ? 'Malaysia'
              //     : countryController.text,
              items: <String>[
                "Afghanistan",
                "Albania",
                "Algeria",
                "American Samoa",
                "Andorra",
                "Angola",
                "Anguilla",
                "Antarctica",
                "Antigua and Barbuda",
                "Argentina",
                "Armenia",
                "Aruba",
                "Australia",
                "Austria",
                "Azerbaijan",
                "Bahamas",
                "Bahrain",
                "Bangladesh",
                "Barbados",
                "Belarus",
                "Belgium",
                "Belize",
                "Benin",
                "Bermuda",
                "Bhutan",
                "Bolivia",
                "Bosnia/Herzegowina",
                "Botswana",
                "Bouvet Island",
                "Brazil",
                "British Ind Ocean Ter",
                "Brunei Darussalam",
                "Bulgaria",
                "Burkina Faso",
                "Burundi",
                "Cambodia",
                "Cameroon",
                "Canada",
                "Cape Verde",
                "Cayman Islands",
                "Central African Rep",
                "Chad",
                "Chile",
                "China",
                "Christmas Island",
                "Cocos Islands",
                "Colombia",
                "Comoros",
                "Congo",
                "Congo Democratic Rep",
                "Cook Islands",
                "Costa Rica",
                "Cote D'Ivoire",
                "Croatia",
                "Cuba",
                "Cyprus",
                "Czech Republic",
                "Denmark",
                "Djibouti",
                "Dominica",
                "Dominican Republic",
                "Ecuador",
                "Egypt",
                "El Salvador",
                "Equatorial Guinea",
                "Eritrea",
                "Estonia",
                "Ethiopia",
                "Falkland Islands",
                "Faroe Islands",
                "Fiji",
                "Finland",
                "France",
                "France Metropolitan",
                "French Guiana",
                "French Polynesia",
                "French Southern Ter",
                "Gabon",
                "Gambia",
                "Georgia",
                "Germany",
                "Ghana",
                "Gibraltar",
                "Greece",
                "Greenland",
                "Grenada",
                "Guadeloupe",
                "Guam",
                "Guatemala",
                "Guinea",
                "Guinea Bissau",
                "Guyana",
                "Haiti",
                "Heard And McDonald Is",
                "Honduras",
                "Hong Kong",
                "Hungary",
                "Iceland",
                "India",
                "Indonesia",
                "Iran",
                "Iraq",
                "Ireland",
                "Isle of Man",
                "Israel",
                "Italy",
                "Jamaica",
                "Japan",
                "Jordan",
                "Kazakhstan",
                "Kenya",
                "Kiribati",
                "Korea Dem People's Rep",
                "Korea Republic",
                "Kuwait",
                "Kyrgyzstan",
                "Lao People's Dem Rep",
                "Latvia",
                "Lebanon",
                "Lesotho",
                "Liberia",
                "Libyan Arab Jamahiriya",
                "Liechtenstein",
                "Lithuania",
                "Luxembourg",
                "Macau",
                "Macedonia",
                "Madagascar",
                "Malawi",
                "Malaysia",
                "Maldives",
                "Mali",
                "Malta",
                "Marshall Islands",
                "Martinique",
                "Mauritania",
                "Mauritius",
                "Mayotte",
                "Mexico",
                "Micronesia",
                "Moldova, Republic Of",
                "Monaco",
                "Mongolia",
                "Montenegro",
                "Montserrat",
                "Morocco",
                "Mozambique",
                "Myanmar",
                "Namibia",
                "Nauru",
                "Nepal",
                "Netherlands",
                "Netherlands Antilles",
                "New Caledonia",
                "New Zealand",
                "Nicaragua",
                "Niger",
                "Nigeria",
                "Niue",
                "Norfolk Island",
                "Northern Mariana Islands",
                "Norway",
                "Oman",
                "Pakistan",
                "Palau",
                "Palestinian Territories",
                "Panama",
                "Papua New Guinea",
                "Paraguay",
                "Peru",
                "Philippines",
                "Pitcairn Island",
                "Poland",
                "Portugal",
                "Puerto Rico",
                "Qatar",
                "Reunion",
                "Romania",
                "Russian Federation",
                "Rwanda",
                "Saint Kitts And Nevis",
                "Saint Lucia",
                "Saint Vincent/Grenadines",
                "Samoa",
                "San Marino",
                "Sao Tome and Principe",
                "Saudi Arabia",
                "Senegal",
                "Serbia",
                "Seychelles",
                "Sierra Leones",
                "Singapore",
                "Slovakia (Slovak Republic)",
                "Slovenia",
                "Solomon Islands",
                "Somalia",
                "South Africa",
                "South Georgia/S Sandwich Is",
                "Spain",
                "Sri Lanka",
                "St Helena",
                "St Pierre and Miquelon",
                "Sudan",
                "Suriname",
                "Svalbard/Jan Mayen Islands",
                "Swaziland",
                "Sweden",
                "Switzerland",
                "Syrian Arab Republic",
                "Taiwan",
                "Tajikistan",
                "Tanzania United Republic of",
                "Thailand",
                "Togo",
                "Tokelau",
                "Tonga",
                "Trinidad and Tobago",
                "Tunisia",
                "Turkey",
                "Turkmenistan",
                "Turks And Caicos Islands",
                "Tuvalu",
                "Uganda",
                "Ukraine",
                "United Arab Emirates",
                "United Kingdom",
                "United States",
                "Uruguay",
                "US Minor Outlying Is",
                "Uzbekistan",
                "Vanuatu",
                "Vatican City State",
                "Venezuela",
                "Viet Nam",
                "Virgin Islands British",
                "Virgin Islands US",
                "Wallis/Futuna Islands",
                "Western Sahara",
                "Yemen",
                "Zambia",
                "Zimbabwe",
              ].map<DropdownMenuItem<String>>(
                (String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                },
              ).toList(),
            ),
          ),
        ),
        SizedBox(height: 25),
        Center(
          child: Container(
            width: width * 0.9,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
                backgroundColor: kPrimaryColor,
              ),
              onPressed: () {
                FocusScope.of(context).requestFocus(FocusNode());

                updateWork();
              },
              // textColor: appWhite,
              // elevation: 4,
              // shape: RoundedRectangleBorder(
              //     borderRadius: BorderRadius.circular(80.0)),
              // padding: EdgeInsets.all(0.0),
              child: Center(
                child: Padding(
                  padding: EdgeInsets.all(18.0),
                  child: BlocConsumer<UpdateWorkCubit, UpdateWorkState>(
                    listener: (context, state) {
                      if (state is UpdateWorkFinish) {
                        Navigator.of(context).pop();
                      }
                    },
                    builder: (context, state) {
                      if (state is UpdateWorkLoading) {
                        return SpinKitFadingCircle(
                          color: Colors.white,
                          size: 14,
                        );
                      } else {
                        return Text(
                          'update'.tr(),
                          style: TextStyle(fontSize: 18),
                          textAlign: TextAlign.center,
                        );
                      }
                    },
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(height: 25),
      ],
    );
  }
}
