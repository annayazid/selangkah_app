import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/utils/endpoint.dart';

class EditWorkRepositories {
  static Future<Work> getWork(String id) async {
    final Map<String, String> map = {
      'user_id': id,
      'token': TOKEN,
    };

    // print('call post get_work_profile');

    final response = await http.post(
      Uri.parse('$API_URL/get_work_profile'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    Work work = workFromJson(response.body);
    return work;
  }

  static Future<void> updateWork({
    String? id,
    String? address1,
    String? address2,
    String? postcode,
    String? city,
    String? state,
    String? country,
    String? occupation,
  }) async {
    final Map<String, String?> map = {
      'user_id': id,
      'work_address1': address1,
      'work_address2': address2,
      'work_postcode': postcode,
      'work_city': city,
      'work_state': state,
      'work_country': country,
      'occupation': occupation,
      'token': TOKEN,
    };

    // print(map);

    // print('call post update_work_profile');

    await http.post(
      Uri.parse('$API_URL/update_workplace'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );
  }
}
