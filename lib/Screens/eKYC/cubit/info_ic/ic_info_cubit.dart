import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';

part 'ic_info_state.dart';

class IcInfoCubit extends Cubit<IcInfoState> {
  IcInfoCubit() : super(IcInfoInitial());

  Future<void> getIcDetails(String requestId) async {
    emit(IcInfoLoading());

    //process
    await Future.delayed(Duration(seconds: 1));

    IcDetails icDetails = await EKYCRepositories.getIcDetails(requestId);

    if (icDetails.data!.address1 == '' ||
        icDetails.data!.address2 == '' ||
        icDetails.data!.city == '' ||
        icDetails.data!.gender == '' ||
        icDetails.data!.ic == '' ||
        icDetails.data!.name == '' ||
        icDetails.data!.postcode == '' ||
        icDetails.data!.state == '' ||
        icDetails.data!.address1 == 'Not found' ||
        icDetails.data!.address2 == 'Not found' ||
        icDetails.data!.city == 'Not found' ||
        icDetails.data!.gender == 'Not found' ||
        icDetails.data!.ic == 'Not found' ||
        icDetails.data!.name == 'Not found' ||
        icDetails.data!.postcode == 'Not found' ||
        icDetails.data!.state == 'Not found') {
      await EKYCRepositories.logEkyc('IC_VERIFICATION', 'FAILED', '');
      emit(IcInfoError());
    } else {
      await EKYCRepositories.logEkyc('IC_VERIFICATION', 'PASS', '');

      emit(IcInfoLoaded(icDetails));
    }
  }
}
