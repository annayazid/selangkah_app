part of 'ic_info_cubit.dart';

@immutable
abstract class IcInfoState {
  const IcInfoState();
}

class IcInfoInitial extends IcInfoState {
  const IcInfoInitial();
}

class IcInfoLoading extends IcInfoState {
  const IcInfoLoading();
}

class IcInfoError extends IcInfoState {
  const IcInfoError();
}

class IcInfoLoaded extends IcInfoState {
  final IcDetails icDetails;

  IcInfoLoaded(this.icDetails);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is IcInfoLoaded && other.icDetails == icDetails;
  }

  @override
  int get hashCode => icDetails.hashCode;
}
