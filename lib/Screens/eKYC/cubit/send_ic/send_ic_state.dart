part of 'send_ic_cubit.dart';

@immutable
abstract class SendIcState {
  const SendIcState();
}

class SendIcInitial extends SendIcState {
  const SendIcInitial();
}

class SendIcLoading extends SendIcState {
  const SendIcLoading();
}

class SendIcComplete extends SendIcState {
  final StatusIc statusIc;

  SendIcComplete(this.statusIc);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SendIcComplete && other.statusIc == statusIc;
  }

  @override
  int get hashCode => statusIc.hashCode;
}

class SendIcError extends SendIcState {
  const SendIcError();
}