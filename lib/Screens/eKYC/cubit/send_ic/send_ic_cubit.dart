import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';

part 'send_ic_state.dart';

class SendIcCubit extends Cubit<SendIcState> {
  SendIcCubit() : super(SendIcInitial());

  Future<void> sendIc(String ic) async {
    emit(SendIcLoading());

    StatusIc statusIc = StatusIc();

    //process
    try {
      statusIc = await EKYCRepositories.sendIc(ic);
      emit(SendIcComplete(statusIc));
    } catch (e) {
      emit(SendIcError());
    }
  }
}
