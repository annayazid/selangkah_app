import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';

part 'check_ekyc_state.dart';

class CheckEkycCubit extends Cubit<CheckEkycState> {
  CheckEkycCubit() : super(CheckEkycInitial());

  Future<void> checkEkyc(String ekycId) async {
    emit(CheckEkycLoading());
    //process
    await EKYCRepositories.checkEkyc(ekycId);
    emit(CheckEkycComplete());
  }
}
