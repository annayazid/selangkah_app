part of 'check_ekyc_cubit.dart';

@immutable
abstract class CheckEkycState {
  const CheckEkycState();
}

class CheckEkycInitial extends CheckEkycState {
  const CheckEkycInitial();
}

class CheckEkycLoading extends CheckEkycState {
  const CheckEkycLoading();
}

class CheckEkycComplete extends CheckEkycState {
  const CheckEkycComplete();
}

class CheckEkycError extends CheckEkycState {
  const CheckEkycError();
}
