import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

part 'check_profile_state.dart';

class CheckProfileCubit extends Cubit<CheckProfileState> {
  CheckProfileCubit() : super(CheckProfileInitial());

  Future<void> checkProfile() async {
    emit(CheckProfileLoading());

    int mykadCount = await EKYCRepositories.getMykadCount();
    // int mykadCount = 0;

    print(mykadCount);

    //check
    RawProfile profile = await EKYCRepositories.getProfile();

    // String token = await SelangorSaringRepo.getToken();

    // Coupon coupon = await SelangorSaringRepo.getCoupon(token);

    //check coupon kat sini
    //check coupon kohot -1
    CouponStatus couponStatusLast = await SaringRepositories.getCouponLast();
    print('length coupon last ${couponStatusLast.data!.length}');

    //check coupon kohot latest
    CouponStatus couponStatus = await SaringRepositories.getCoupon();
    print('length coupon ${couponStatus.data!.length}');

    String? name = profile.data?[2];
    String? idCard = profile.data?[4];
    String? idType = profile.data?[17];
    String? citizenship = profile.data?[6];
    String? gender = profile.data?[7];
    String? dob = profile.data?[8];
    String? email = profile.data?[15];
    String? phone = profile.data?[19];
    String? address1 = profile.data?[9];
    String? postcode = profile.data?[11];
    String? city = profile.data?[12];
    String? state = profile.data?[13];
    String? country = profile.data?[14];
    String? status = profile.data?[20];

    if (name == null ||
        idCard == null ||
        idType == null ||
        citizenship == null ||
        gender == null ||
        dob == null ||
        email == null ||
        phone == null ||
        address1 == null ||
        postcode == null ||
        city == null ||
        state == null ||
        country == null) {
      print('incomplete');
      // yield ProfileIncomplete();
      emit(CheckProfileIncomplete());
    } else {
      print('complete');

      // if (status.toLowerCase().contains("kyc")) {
      //   emit(CheckProfileRedirectSaring());
      // } else {
      //   emit(CheckProfileComplete());
      // }

      //testing
      // emit(CheckProfileComplete());

      //ni live
      if (mykadCount < 2) {
        if (couponStatus.data!.length == 0 &&
            couponStatusLast.data!.length == 0) {
          String token = await SaringRepositories.getToken();

          String id = await SecureStorage().readSecureData('userId');
          String selId = await SecureStorage().readSecureData('userSelId');

          String? kohot = await SaringRepositories.getKohot();

          Map<String, String> queryParams = {
            'user_token': token,
            'id_selangkah_user': id,
            'selangkah_id': selId,
            'is_app': '1',
            'bearer_token': token,
            'id_kohot': kohot!,
          };

          String queryString = Uri(queryParameters: queryParams).query;

          String url = '$URL_SARING/check-credential-app?' + queryString;

          print(url);

          emit(CheckProfileRedirectQuestionnaire(url));
        } else if (couponStatus.data!.length == 0) {
          emit(CheckProfileRedirectSaring(true));
        } else {
          if (status!.toLowerCase().contains("kyc")) {
            emit(CheckProfileRedirectSaring(false));
          } else {
            bool bypassed = await SaringRepositories.getStatusBypass();

            if (bypassed) {
              emit(CheckProfileRedirectSaring(false));
            } else {
              emit(CheckProfileComplete());
            }
          }
        }
      } else {
        if (couponStatus.data!.length == 0 &&
            couponStatusLast.data!.length == 0) {
          String token = await SaringRepositories.getToken();

          String id = await SecureStorage().readSecureData('userId');
          String selId = await SecureStorage().readSecureData('userSelId');

          String? kohot = await SaringRepositories.getKohot();

          Map<String, String> queryParams = {
            'user_token': token,
            'id_selangkah_user': id,
            'selangkah_id': selId,
            'is_app': '1',
            'bearer_token': token,
            'id_kohot': kohot!,
          };

          String queryString = Uri(queryParameters: queryParams).query;

          String url = '$URL_SARING/check-credential-app?' + queryString;

          print(url);

          emit(CheckProfileRedirectQuestionnaire(url));
        } else if (couponStatus.data!.length == 0) {
          print('masuk sini menu');

          emit(CheckProfileRedirectSaring(true));
        } else {
          if (!status!.toLowerCase().contains("kyc")) {
            emit(CheckProfileRedirectSaring(false));
          } else {
            bool bypassed = await SaringRepositories.getStatusBypass();

            if (bypassed) {
              print('mmasuk sini');
              emit(CheckProfileRedirectSaring(false));
            } else {
              //we bypass them here
              //call api bypass

              await EKYCRepositories.bypassEKYC();
              emit(CheckProfileRedirectSaring(false));
            }
          }
        }
      }

      //end here okey

      // emit(CheckProfileRedirectSaring());

      // if (status.toLowerCase().contains("kyc")) {
      //   emit(CheckProfileRedirectSaring());
      // } else {
      //   emit(CheckProfileRedirectSaring());
      // }
    }
  }
}
