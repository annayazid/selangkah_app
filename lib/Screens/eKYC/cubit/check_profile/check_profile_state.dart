part of 'check_profile_cubit.dart';

@immutable
abstract class CheckProfileState {
  const CheckProfileState();
}

class CheckProfileInitial extends CheckProfileState {
  const CheckProfileInitial();
}

class CheckProfileLoading extends CheckProfileState {
  const CheckProfileLoading();
}

class CheckProfileIncomplete extends CheckProfileState {
  const CheckProfileIncomplete();
}

class CheckProfileComplete extends CheckProfileState {
  const CheckProfileComplete();
}

class CheckProfileRedirectSaring extends CheckProfileState {
  final bool showRegistration;

  CheckProfileRedirectSaring(this.showRegistration);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CheckProfileRedirectSaring &&
        other.showRegistration == showRegistration;
  }

  @override
  int get hashCode => showRegistration.hashCode;
}

class CheckProfileRedirectQuestionnaire extends CheckProfileState {
  final String url;

  CheckProfileRedirectQuestionnaire(this.url);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CheckProfileRedirectQuestionnaire && other.url == url;
  }

  @override
  int get hashCode => url.hashCode;
}
