import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class EKYCRepositories {
  static Future<RawProfile> getProfile() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id': id,
      'token': TOKEN,
    };

    //print('calling post GET_PROFILE');

    final response = await http.post(
      Uri.parse('$API_URL/check_user_profile'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    RawProfile? profile = rawProfileFromJson(response.body);
    return profile!;
  }

  static Future<StatusIc> sendIc(String pic) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'data': pic,
    };

    // log(map.toString());

    //print('calling post GET_PROFILE');

    final response = await http.post(
      Uri.parse('$API_EKYC/mykad_verification'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print('$API_EKYC/mykad_verification');

    print('response is ${response.body}');

    StatusIc statusIc = statusIcFromJson(response.body);
    return statusIc;
  }

  static Future<void> checkEkyc(String ekycId) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
      'req_id': ekycId,
    };

    // log(map.toString());

    //print('calling post GET_PROFILE');

    final response = await http.post(
      Uri.parse('$API_EKYC/ekyc_result'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print('response is ${response.body}');
  }

  static Future<IcDetails> getIcDetails(String requestId) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'req_id': requestId,
      'token': TOKEN,
    };

    print(map);

    // log(map.toString());

    print('calling post get kad result');
    print('$API_EKYC/EkycGetMyKadResult');

    final response = await http.post(
      Uri.parse('$API_EKYC/EkycGetMyKadResult'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print('response is ${response.body}');

    IcDetails icDetails = icDetailsFromJson(response.body);
    return icDetails;
  }

  static Future<void> bypassEKYC() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'token': TOKEN,
      'id_selangkah_user': id,
    };

    // log(map.toString());

    //print('calling post GET_PROFILE');

    final response = await http.post(
      Uri.parse('$SARING_DATA/ekyc_bypass'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print('response is ${response.body}');
  }

  static Future<void> logEkyc(
      String stage, String status, String logStatus) async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'stage': stage,
      'status': status,
      'log_status': logStatus,
      'token': TOKEN,
    };

    log(map.toString());

    print('calling post log ekyc');

    final response = await http.post(
      Uri.parse('$API_EKYC/set_ekyc_flag'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print('response is ${response.body}');
  }

  static Future<int> getMykadCount() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id_selangkah_user': id,
      'token': TOKEN,
    };

    final response = await http.post(
      Uri.parse('$SARING_DATA/get_mykad_count'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    var data = jsonDecode(response.body);

    int count = int.parse(data['Data'][0]['num']);

    return count;
    // return 0;
  }
}
