import 'package:flutter/material.dart';
import 'package:selangkah_new/utils/constants.dart';

class EKYCScaffold extends StatelessWidget {
  final String appBarTitle;
  final Widget child;

  const EKYCScaffold({Key? key, required this.appBarTitle, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        centerTitle: true,
        automaticallyImplyLeading: true,
        title: Text(
          appBarTitle,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 15.0,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: child,
    );
  }
}
