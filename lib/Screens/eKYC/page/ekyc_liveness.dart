import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';
import 'package:selangkah_new/utils/constants.dart';

class EKYCLiveness extends StatelessWidget {
  final String requestId;

  const EKYCLiveness({Key? key, required this.requestId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top -
        AppBar().preferredSize.height;
    return EKYCScaffold(
      appBarTitle: 'Selangor Saring',
      child: Column(
        children: [
          Expanded(
            flex: 5,
            child: SingleChildScrollView(
              padding: EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Container(
                  //   color: Colors.black,
                  //   height: 1000,
                  // ),
                  SizedBox(height: 10),

                  Center(
                    child: Image.asset(
                      'assets/images/Ekyc/face_liveness.png',
                      height: height * 0.275,
                    ),
                  ),
                  SizedBox(height: 20),

                  Center(
                    child: Text(
                      'ekyc_live_camera_interaction'.tr(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                    ),
                  ),

                  SizedBox(height: 20),

                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text('•'),
                            SizedBox(width: 10),
                            Expanded(
                              child: Text(
                                'make_sure_bright'.tr(),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text('•'),
                            SizedBox(width: 10),
                            Expanded(
                              child: Text(
                                'position_face_frame'.tr(),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text('•'),
                            SizedBox(width: 10),
                            Expanded(
                              child: Text(
                                'follow_instruction_liveness'.tr(),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),

                  SizedBox(height: 40),

                  Row(
                    children: [
                      DotInfo(
                        number: '1',
                        color: Colors.green,
                        finished: true,
                        progress: false,
                      ),
                      RowDot(),
                      DotInfo(
                        number: '2',
                        color: Color(0xFFF4AD08),
                        finished: false,
                        progress: true,
                      ),
                      // RowDot(),
                      // DotInfo(
                      //   number: '3',
                      //   color: Color(0xFFF4AD08),
                      //   finished: false,
                      //   progress: false,
                      // ),
                      RowDot(),
                      DotInfo(
                        number: '3',
                        color: Colors.grey,
                        finished: false,
                        progress: false,
                      ),
                    ],
                  ),
                  SizedBox(height: 10),

                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          'take_photo_nric'.tr(),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Spacer(),
                      Expanded(
                        child: Text(
                          'live_camera_interaction'.tr(),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      // Spacer(),
                      // Expanded(
                      //   child: Text(
                      //     'Take photo of yourself',
                      //     textAlign: TextAlign.center,
                      //   ),
                      // ),
                      Spacer(),
                      Expanded(
                        child: Text(
                          'wait_account_verified'.tr(),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),

                  SizedBox(height: 40),

                  Text(
                    'disclaimer_ekyc'.tr(),
                    style: TextStyle(
                      color: Color(0xFF767674),
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),

                  SizedBox(height: 20),
                ],
              ),
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.all(15),
                width: double.infinity,
                child: ElevatedButton(
                  style:
                      ElevatedButton.styleFrom(backgroundColor: kPrimaryColor),
                  onPressed: () async {
                    // List<CameraDescription> camera = await availableCameras();

                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) => EKYCLivenessWeb(
                          requestId: requestId,
                        ),
                      ),
                    );

                    // Navigator.of(context).pushReplacement(
                    //   MaterialPageRoute(
                    //     builder: (context) => EKYCSelfie(),
                    //   ),
                    // );
                  },
                  child: Text('start'.tr()),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
