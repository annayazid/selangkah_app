import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/global.dart';

class EKYCICDetails extends StatefulWidget {
  final String requestId;

  const EKYCICDetails({Key? key, required this.requestId}) : super(key: key);

  @override
  _EKYCICDetailsState createState() => _EKYCICDetailsState();
}

class _EKYCICDetailsState extends State<EKYCICDetails> {
  @override
  void initState() {
    context.read<IcInfoCubit>().getIcDetails(widget.requestId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top -
        AppBar().preferredSize.height;
    double width = MediaQuery.of(context).size.width;
    return EKYCScaffold(
      appBarTitle: 'Selangor Saring',
      child: BlocConsumer<IcInfoCubit, IcInfoState>(
        listener: (context, state) {
          if (state is IcInfoError) {
            Navigator.of(context).pop();
            Fluttertoast.showToast(
              msg: 'something_wrong_ic'.tr(),
              toastLength: Toast.LENGTH_LONG,
            );

            // Navigator.of(context).pushReplacement(
            //   MaterialPageRoute(
            //     builder: (context) => EKYCFailed(),
            //   ),
            // );
          }
        },
        builder: (context, state) {
          if (state is IcInfoLoaded) {
            return Column(
              children: [
                Expanded(
                  flex: 5,
                  child: SingleChildScrollView(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                          child: Image.asset(
                            'assets/images/Ekyc/ic_blank.png',
                            height: height * 0.25,
                          ),
                        ),
                        SizedBox(height: 80),
                        Text(
                          'ensure_details_correct'.tr(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 30),

                        //name
                        Row(
                          children: [
                            Container(
                              width: width * 0.20,
                              child: Text('${'name'.tr()}: '),
                            ),
                            Expanded(
                              child: Text(state.icDetails.data!.name!),
                            ),
                          ],
                        ),
                        SizedBox(height: 5),

                        //ic number
                        Row(
                          children: [
                            Container(
                              width: width * 0.20,
                              child: Text('${'icno'.tr()}: '),
                            ),
                            Expanded(
                              child: Text(state.icDetails.data!.ic!),
                            ),
                          ],
                        ),
                        SizedBox(height: 5),

                        //address
                        Row(
                          children: [
                            Container(
                              width: width * 0.20,
                              child: Text('${'address'.tr()}: '),
                            ),
                            Expanded(
                              child: Text(state.icDetails.data!.address1!),
                            ),
                          ],
                        ),
                        SizedBox(height: 5),

                        //postcode
                        Row(
                          children: [
                            Container(
                              width: width * 0.20,
                              child: Text('${'postcode'.tr()}: '),
                            ),
                            Expanded(
                              child: Text(state.icDetails.data!.postcode!),
                            ),
                          ],
                        ),
                        SizedBox(height: 5),

                        //city
                        Row(
                          children: [
                            Container(
                              width: width * 0.20,
                              child: Text('${'city'.tr()}: '),
                            ),
                            Expanded(
                              child: Text(state.icDetails.data!.city!),
                            ),
                          ],
                        ),
                        SizedBox(height: 5),

                        //state
                        Row(
                          children: [
                            Container(
                              width: width * 0.20,
                              child: Text('${'state'.tr()}: '),
                            ),
                            Expanded(
                              child: Text('${state.icDetails.data!.state}'
                                  .toUpperCase()),
                            ),
                          ],
                        ),
                        SizedBox(height: 5),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(15),
                          width: double.infinity,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: kPrimaryColor),
                            onPressed: () async {
                              if (!GlobalVariables.retakeEkyc!) {
                                GlobalVariables.retakeEkyc = true;
                                Navigator.of(context).pop();
                              } else {
                                Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                    builder: (context) => EKYCFailed(),
                                  ),
                                );
                              }
                            },
                            child: Text('ekyc_detail_retake'.tr()),
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(15),
                          width: double.infinity,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: kPrimaryColor),
                            onPressed: () async {
                              Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                  builder: (context) => EKYCLiveness(
                                    requestId: widget.requestId,
                                  ),
                                ),
                              );
                            },
                            child: Text('continue'.tr()),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          } else {
            return Center(
              child: SpinKitFadingCircle(
                color: kPrimaryColor,
                size: 30,
              ),
            );
          }
        },
      ),
    );
  }
}
