import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';
import 'package:selangkah_new/utils/constants.dart';

class EKYCWaitAccount extends StatefulWidget {
  final String requestId;

  const EKYCWaitAccount({Key? key, required this.requestId}) : super(key: key);

  @override
  _EKYCWaitAccountState createState() => _EKYCWaitAccountState();
}

class _EKYCWaitAccountState extends State<EKYCWaitAccount> {
  @override
  void initState() {
    context.read<CheckEkycCubit>().checkEkyc(widget.requestId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return EKYCScaffold(
      appBarTitle: 'Selangor Saring',
      child: BlocBuilder<CheckEkycCubit, CheckEkycState>(
        builder: (context, state) {
          if (state is CheckEkycComplete) {
            return EKYCPassed();
          } else {
            return Center(
              child: SpinKitFadingCircle(
                color: kPrimaryColor,
                size: 30,
              ),
            );
          }
        },
      ),
    );
  }
}

class EKYCPassed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top -
        AppBar().preferredSize.height;
    return Column(
      children: [
        Expanded(
          flex: 5,
          child: SingleChildScrollView(
            padding: EdgeInsets.all(15),
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 10),
                Center(
                  child: Image.asset(
                    'assets/images/Ekyc/ekyc_tick.png',
                    height: height * 0.35,
                  ),
                ),
                SizedBox(height: 10),
                Center(
                  child: Text(
                    'congratulation_account'.tr(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 22,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 40),
                Row(
                  children: [
                    DotInfo(
                      number: '1',
                      color: Colors.green,
                      finished: true,
                      progress: false,
                    ),
                    RowDot(),
                    DotInfo(
                      number: '2',
                      color: Colors.green,
                      finished: true,
                      progress: false,
                    ),
                    // RowDot(),
                    // DotInfo(
                    //   number: '3',
                    //   color: Colors.green,
                    //   finished: true,
                    //   progress: false,
                    // ),
                    RowDot(),
                    DotInfo(
                      number: '3',
                      color: Colors.green,
                      finished: true,
                      progress: false,
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Text(
                        'take_photo_nric'.tr(),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Spacer(),
                    Expanded(
                      child: Text(
                        'live_camera_interaction'.tr(),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    // Spacer(),
                    // Expanded(
                    //   child: Text(
                    //     'Take photo of yourself',
                    //     textAlign: TextAlign.center,
                    //   ),
                    // ),
                    Spacer(),
                    Expanded(
                      child: Text(
                        'wait_account_verified'.tr(),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 40),
                Text(
                  'disclaimer_ekyc'.tr(),
                  style: TextStyle(
                    color: Color(0xFF767674),
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
        Expanded(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: EdgeInsets.all(15),
              width: double.infinity,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(backgroundColor: kPrimaryColor),
                onPressed: () async {
                  // List<CameraDescription> camera = await availableCameras();

                  // Navigator.of(context).pushReplacement(
                  //   MaterialPageRoute(
                  //     builder: (context) => EKYCSelfieCamera(
                  //       camera: camera,
                  //     ),
                  //   ),
                  // );

                  // _pickImage(ImageSource.camera);
                  Navigator.of(context).pop();
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) => SaringMenu(
                        showRegistration: false,
                      ),
                    ),
                  );
                },
                child: Text('Okay'),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
