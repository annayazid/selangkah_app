import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';
import 'package:selangkah_new/utils/constants.dart';

class EKYCFailed extends StatefulWidget {
  const EKYCFailed({Key? key}) : super(key: key);

  @override
  _EKYCFailedState createState() => _EKYCFailedState();
}

class _EKYCFailedState extends State<EKYCFailed> {
  @override
  void initState() {
    EKYCRepositories.bypassEKYC();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        title: Text('KYC Failed'),
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).pop();
          },
          child: Container(
            child: Icon(
              Icons.arrow_back_ios,
            ),
          ),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/Ekyc/ekyc_failed.png',
          ),
          Center(
            child: Text('system_failed_ekyc'.tr()),
          ),
          SizedBox(height: 30),
          Column(
            children: [
              // Container(
              //   width: double.infinity,
              //   margin: EdgeInsets.symmetric(horizontal: 20),
              //   decoration: BoxDecoration(
              //     boxShadow: [
              //       BoxShadow(
              //         color: Colors.black.withOpacity(0.1),
              //         blurRadius: 10,
              //         offset: Offset(0, 5),
              //       ),
              //     ],
              //   ),
              //   child: ElevatedButton(
              //     style: ElevatedButton.styleFrom(
              //       primary: Colors.white,
              //       padding: EdgeInsets.all(10),
              //     ),
              //     onPressed: () async {
              //       Navigator.of(context).pop();
              //     },
              //     child: Text(
              //       'ekyc_detail_retake'.tr(),
              //       style: TextStyle(
              //         color: kPrimaryColor,
              //       ),
              //     ),
              //   ),
              // ),
              // SizedBox(height: 10),
              Container(
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.1),
                      blurRadius: 10,
                      offset: Offset(0, 5),
                    ),
                  ],
                ),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: kPrimaryColor,
                    padding: EdgeInsets.all(10),
                  ),
                  onPressed: () async {
                    await EKYCRepositories.bypassEKYC();
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) => SaringMenu(
                          showRegistration: false,
                        ),
                      ),
                    );
                  },
                  child: Text(
                    'proceed_without_verification'.tr(),
                    style: TextStyle(),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
          // Row(
          //   children: [
          //     Container(
          //       padding: EdgeInsets.all(15),
          //       child: ElevatedButton(
          //         style: ElevatedButton.styleFrom(
          //           primary: kPrimaryColor,
          //           padding: EdgeInsets.all(10),
          //         ),
          //         onPressed: () async {
          //           Navigator.of(context).pop();
          //         },
          //         child: Text('ekyc_detail_retake'.tr()),
          //       ),
          //     ),
          //     SizedBox(width: 20),
          //     Expanded(
          //       child: Container(
          //         padding: EdgeInsets.all(15),
          //         width: double.infinity,
          //         child: ElevatedButton(
          //           style: ElevatedButton.styleFrom(
          //             primary: kPrimaryColor,
          //             padding: EdgeInsets.all(10),
          //           ),
          //           onPressed: () async {
          //             await EKYCRepositories.bypassEKYC();
          //             Navigator.of(context).pushReplacement(
          //               MaterialPageRoute(
          //                 builder: (context) => BlocProvider(
          //                   create: (context) => UserTokenCubit(),
          //                   child: SaringPage(
          //                     showRegistration: false,
          //                   ),
          //                 ),
          //               ),
          //             );
          //           },
          //           child: Text(
          //             'proceed_without_verification'.tr(),
          //             style: TextStyle(),
          //             textAlign: TextAlign.center,
          //           ),
          //         ),
          //       ),
          //     ),
          //   ],
          // ),
        ],
      ),
    );
  }
}
