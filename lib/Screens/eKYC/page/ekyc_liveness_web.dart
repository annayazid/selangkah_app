import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';
import 'package:selangkah_new/utils/constants.dart';

import 'dart:developer' as dev;

import 'package:easy_localization/easy_localization.dart';

class EKYCLivenessWeb extends StatefulWidget {
  final String requestId;

  const EKYCLivenessWeb({Key? key, required this.requestId}) : super(key: key);

  @override
  _EKYCLivenessWebState createState() => _EKYCLivenessWebState();
}

class _EKYCLivenessWebState extends State<EKYCLivenessWeb> {
  InAppWebViewController? webView;
  // Timer timer;

  bool found = true;

  @override
  void dispose() {
    // timer.cancel();
    super.dispose();
  }

  String requestId = '';

  @override
  void initState() {
    requestId = widget.requestId;
    super.initState();
  }

  dynamic progress = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('EKYC Liveness'),
      ),
      body: Stack(
        children: [
          InAppWebView(
            onWebViewCreated: (controller) {
              webView = controller;
            },
            androidOnPermissionRequest: (controller, origin, resources) async {
              return PermissionRequestResponse(
                resources: resources,
                action: PermissionRequestResponseAction.GRANT,
              );
            },
            initialUrlRequest: URLRequest(
                url: Uri.parse(
                    'https://app.selangkah.my/ekyc/index.html?autoInit=true&apiKey=X81uMAg.U0EDnTlQzDYBe&requestId=$requestId')),
            initialOptions: InAppWebViewGroupOptions(
              crossPlatform: InAppWebViewOptions(
                useShouldOverrideUrlLoading: true,
                mediaPlaybackRequiresUserGesture: false,
                useOnDownloadStart: true,
              ),
              android: AndroidInAppWebViewOptions(
                hardwareAcceleration: true,
              ),
              ios: IOSInAppWebViewOptions(
                allowsInlineMediaPlayback: true,
              ),
            ),
            onConsoleMessage: (controller, consoleMessage) {
              dev.log('console is ${consoleMessage.message}');

              // if (consoleMessage.message.contains('rmEKYC') &&
              //     consoleMessage.message.contains('LIVENESS_TIMEOUT')) {
              //   // Navigator.of(context).pop();
              //   Navigator.of(context).pushReplacement(
              //     MaterialPageRoute(
              //       builder: (context) => EKYCSelfie(),
              //     ),
              //   );
              //   Fluttertoast.showToast(msg: 'success');
              // } else if (consoleMessage.message.contains('Verification fail')) {
              //   Navigator.of(context).pop();
              //   Fluttertoast.showToast(msg: 'fail');
              // } else if (consoleMessage.message.contains('Verification time out')) {
              //   Navigator.of(context).pop();
              //   Fluttertoast.showToast(msg: 'timeout');
              // }

              if (found) {
                if (consoleMessage.message.contains('Verification time out!')) {
                  found = !found;
                  EKYCRepositories.logEkyc(
                      'LIVENESS_VERIFICATION', 'FAILED', 'TIMEOUT');
                  Navigator.of(context).pop();
                  Fluttertoast.showToast(msg: 'fail');
                }

                if (consoleMessage.message.contains('rmEKYC') &&
                    !consoleMessage.message.contains('LIVENESS_TIMEOUT')) {
                  found = !found;
                  EkycStatus ekycStatus =
                      ekycStatusFromJson(consoleMessage.message);

                  if (ekycStatus.rmEkyc!.item!.isSamePerson == true) {
                    // Navigator.of(context).pushReplacement(
                    //   MaterialPageRoute(
                    //     builder: (context) => EKYCSelfie(),
                    //   ),
                    // );

                    EKYCRepositories.logEkyc(
                        'LIVENESS_VERIFICATION', 'PASS', '');

                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) => BlocProvider(
                          create: (context) => CheckEkycCubit(),
                          child: EKYCWaitAccount(
                            requestId: ekycStatus.rmEkyc!.item!.ekycId!,
                          ),
                        ),
                      ),
                    );
                  } else {
                    // Navigator.of(context).pop();
                    // Fluttertoast.showToast(msg: 'fail');

                    // EKYCRepositories.logFailedEkyc();
                    EKYCRepositories.logEkyc(
                        'LIVENESS_VERIFICATION', 'FAILED', 'DIFFERENT_PERSON');

                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) => EKYCFailed(),
                      ),
                    );
                  }
                }
              }
            },
            onProgressChanged:
                (InAppWebViewController controller, int progress) {
              setState(() {
                this.progress = progress / 100;
              });
            },
            onLoadStop: (controller, url) async {
              // print('current url is $url');

              // await Future.delayed(Duration(seconds: 3));
              // if (url.contains('.pdf')) {
              //   launch(url);
              // }

              // timer = Timer.periodic(Duration(seconds: 2), (Timer t) async {
              //   print('mencari');
              //   if (found) {
              //     await controller.findAllAsync(find: 'Verification');
              //   }
              // });
              // timerFailed = Timer.periodic(Duration(seconds: 2), (Timer t) async {
              //   print('mencari');
              //   await controller.findAllAsync(find: 'Verification fail');
              // });
              // timerTimeout = Timer.periodic(Duration(seconds: 2), (Timer t) async {
              //   print('mencari');
              //   await controller.findAllAsync(find: 'Verification time out');
              // });
            },
            onFindResultReceived: (controller, activeMatchOrdinal,
                numberOfMatches, isDoneCounting) async {
              // print('activeMatchOrdinal $activeMatchOrdinal');
              // print('numberOfMatches $numberOfMatches');
              // print('isDoneCounting $isDoneCounting');

              // // await controller.;

              // if (numberOfMatches > 0 && found) {
              //   // Navigator.of(context).pop();
              //   // controller.evaluateJavascript(
              //   //     source: 'alert("Hello! I am an alert box!!")');
              //   setState(() {
              //     found = false;
              //   });
              //   controller.evaluateJavascript(
              //       source:
              //           "var numDice = document.getElementById('instruction-text').innerHTML;console.log(numDice);");
              // }
            },
            // shouldOverrideUrlLoading: (controller, navigationAction) async {
            //   var uri = navigationAction.url;

            //   if (![
            //     "http",
            //     "https",
            //     "file",
            //     "chrome",
            //     "data",
            //     "javascript",
            //     "about",
            //   ].contains(uri)) {
            //     print('url override is $uri');
            //     if (await canLaunch(uri)) {
            //       // Launch the App
            //       launch(uri);
            //       // and cancel the request
            //       // return ShouldOverrideUrlLoadingAction.CANCEL;
            //     }
            //   } else {
            //     print('tak masuk override');
            //   }

            //   return ShouldOverrideUrlLoadingAction.ALLOW;
            // },
          ),
          Align(
            alignment: Alignment.center,
            child: _buildProgressBarIOS(),
          ),
        ],
      ),
    );
  }

  Widget _buildProgressBarIOS() {
    if (progress != 1.0) {
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SpinKitFadingCircle(
              color: kPrimaryColor,
              size: 35,
            ),
            SizedBox(height: 10),
            Text(
              'liveness_wait'.tr(),
            ),
          ],
        ),
      );
    }
    return Container();
  }
}
