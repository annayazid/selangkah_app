import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';
import 'package:selangkah_new/utils/constants.dart';

class EkycManual extends StatelessWidget {
  const EkycManual({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    return EKYCScaffold(
      appBarTitle: 'Selangor Saring',
      child: Column(
        children: [
          SizedBox(height: 25),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Text(
              'manual_verification'.tr(),
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 25),
          Image.asset(
            'assets/images/Ekyc/ekyc_manual.png',
            height: height * 0.35,
          ),
          SizedBox(height: 25),
          Row(
            children: [
              Spacer(),
              Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.black,
                  shape: BoxShape.circle,
                ),
                child: Text(
                  '1',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              DotDecoration(),
              DotDecoration(),
              DotDecoration(),
              DotDecoration(),
              DotDecoration(),
              DotDecoration(),
              DotDecoration(),
              DotDecoration(),
              DotDecoration(),
              DotDecoration(),
              DotDecoration(),
              DotDecoration(),
              DotDecoration(),
              DotDecoration(),
              DotDecoration(),
              Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.black,
                  shape: BoxShape.circle,
                ),
                child: Text(
                  '2',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              Spacer(),
            ],
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    'bring_ic_event_day'.tr(),
                    textAlign: TextAlign.center,
                  ),
                ),
                Expanded(
                  child: Text(
                    'officer_manually'.tr(),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            width: double.infinity,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(backgroundColor: kPrimaryColor),
              onPressed: () async {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => SaringMenu(
                      showRegistration: false,
                    ),
                  ),
                );
              },
              child: Text(
                'proceed_manual'.tr(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            width: double.infinity,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(backgroundColor: kPrimaryColor),
              onPressed: () async {
                Navigator.of(context).pop();
              },
              child: Text(
                'back'.tr(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          SizedBox(height: 25),
        ],
      ),
    );
  }
}

class DotDecoration extends StatelessWidget {
  const DotDecoration({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(1.5),
      margin: EdgeInsets.all(2.5),
      decoration: BoxDecoration(
        color: Colors.grey,
        shape: BoxShape.circle,
      ),
    );
  }
}
