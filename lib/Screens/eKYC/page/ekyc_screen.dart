import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/Saring/saring.dart';
import 'package:selangkah_new/Screens/VerifyProfile/verify_profile.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class EKYCScreen extends StatefulWidget {
  const EKYCScreen({Key? key}) : super(key: key);

  @override
  _EKYCScreenState createState() => _EKYCScreenState();
}

class _EKYCScreenState extends State<EKYCScreen> {
  @override
  void initState() {
    context.read<CheckProfileCubit>().checkProfile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top -
        AppBar().preferredSize.height;
    return EKYCScaffold(
      appBarTitle: 'Selangor Saring',
      child: BlocConsumer<CheckProfileCubit, CheckProfileState>(
        listener: (context, state) {
          print('state is {$state.toString()}');
          if (state is CheckProfileRedirectSaring) {
            //redirect here for ekyc live
            // Navigator.of(context).pushReplacement(
            //   MaterialPageRoute(
            //     builder: (context) => BlocProvider(
            //       create: (context) => UserTokenCubit(),
            //       child: SaringPage(showRegistration: state.showRegistration),
            //     ),
            //   ),
            // );

            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => SaringMenu(
                  showRegistration: state.showRegistration,
                ),
              ),
            );
          }

          if (state is CheckProfileIncomplete) {
            // Navigator.push(
            //   context,
            //   MaterialPageRoute(
            //     builder: (context) {
            //       return ProfilePage(
            //         message: "updateProfile".tr(),
            //         isFromRemedi: true,
            //       );
            //     },
            //     settings: RouteSettings(name: 'Edit profile page'),
            //   ),
            // ).then((value) {
            //   context.read<CheckProfileCubit>().checkProfile();
            // });

            Fluttertoast.showToast(
              msg: "updateProfile".tr(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
            );

            //redirect edit profile

            Navigator.of(context)
                .push(
              MaterialPageRoute(
                builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => GetUserDetailsCubit(),
                    ),
                    BlocProvider(
                      create: (context) => UpdateDetailCubit(),
                    ),
                  ],
                  child: VerifyProfile(),
                ),
              ),
            )
                .then((value) async {
              await Future.delayed(Duration(seconds: 1));
              String? email = await SecureStorage().readSecureData('email');

              print('email is $email');

              if (email == null || email == 'null' || email == '') {
                Navigator.of(context).pop();
              } else {
                context.read<CheckProfileCubit>().checkProfile();
              }
            });
          }

          if (state is CheckProfileRedirectQuestionnaire) {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => SaringMenu(
                  showRegistration: true,
                ),
              ),
            );

            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => SaringWebNew(
                  url: state.url,
                  appBarTitle: 'Selangor Saring',
                ),
              ),
            );
          }
        },
        builder: (context, state) {
          if (state is CheckProfileComplete) {
            return Column(
              children: [
                Expanded(
                  flex: 5,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Column(
                      children: [
                        SizedBox(height: 10),
                        Center(
                          child: Image.asset(
                            'assets/images/Ekyc/phone_ic.png',
                            height: height * 0.40,
                          ),
                        ),
                        SizedBox(height: 30),
                        Text(
                          'user_verification'.tr().toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                        SizedBox(height: 20),

                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Text.rich(
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: 'your_account'.tr() + ' ',
                                ),
                                TextSpan(
                                  text: 'must_be_verified'.tr(),
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: ' ' + 'claim_coupon'.tr(),
                                ),
                              ],
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                              ),
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),

                        SizedBox(height: 20),

                        Text(
                          'verification_module'.tr(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 20),
                        Spacer(),

                        // SizedBox(height: 30),
                        // Row(
                        //   children: [
                        //     DotInfo(
                        //       number: '1',
                        //       color: Color(0xFFF4AD08),
                        //       finished: false,
                        //       progress: false,
                        //     ),
                        //     RowDot(),
                        //     DotInfo(
                        //       number: '2',
                        //       color: Color(0xFFF4AD08),
                        //       finished: false,
                        //       progress: false,
                        //     ),
                        //     // RowDot(),
                        //     // DotInfo(
                        //     //   number: '3',
                        //     //   color: Color(0xFFF4AD08),
                        //     //   finished: false,
                        //     //   progress: false,
                        //     // ),
                        //     RowDot(),
                        //     DotInfo(
                        //       number: '3',
                        //       color: Color(0xFFF4AD08),
                        //       finished: false,
                        //       progress: false,
                        //     ),
                        //   ],
                        // ),
                        // SizedBox(height: 10),
                        // Row(
                        //   crossAxisAlignment: CrossAxisAlignment.start,
                        //   children: [
                        //     Expanded(
                        //       child: Text(
                        //         'take_photo_nric'.tr(),
                        //         textAlign: TextAlign.center,
                        //       ),
                        //     ),
                        //     Spacer(),
                        //     Expanded(
                        //       child: Text(
                        //         'live_camera_interaction'.tr(),
                        //         textAlign: TextAlign.center,
                        //       ),
                        //     ),
                        //     // Spacer(),
                        //     // Expanded(
                        //     //   child: Text(
                        //     //     'Take photo of yourself',
                        //     //     textAlign: TextAlign.center,
                        //     //   ),
                        //     // ),
                        //     Spacer(),
                        //     Expanded(
                        //       child: Text(
                        //         'wait_account_verified'.tr(),
                        //         textAlign: TextAlign.center,
                        //       ),
                        //     ),
                        //   ],
                        // ),
                        // SizedBox(height: 40),
                        // Text(
                        //   'disclaimer_ekyc'.tr(),
                        //   style: TextStyle(
                        //     color: Color(0xFF767674),
                        //     fontStyle: FontStyle.italic,
                        //     fontWeight: FontWeight.bold,
                        //   ),
                        //   textAlign: TextAlign.center,
                        // ),
                        // SizedBox(height: 20),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        width: double.infinity,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: kPrimaryColor),
                          onPressed: () async {
                            // int mykadCount =
                            //     await EKYCRepositories.getMykadCount();

                            // // int mykadCount = 0;

                            // if (mykadCount < 2) {
                            //   Navigator.of(context).push(
                            //     MaterialPageRoute(
                            //       builder: (context) => EKYCIC(),
                            //     ),
                            //   );
                            // } else {
                            //   Navigator.of(context).push(
                            //     MaterialPageRoute(
                            //       builder: (context) => EKYCFailed(),
                            //     ),
                            //   );
                            // }
                            showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15)),
                                  title: Text(
                                    'dialog_app_verification'
                                        .tr()
                                        .toUpperCase(),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                      fontSize: 20,
                                    ),
                                  ),
                                  content: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text('dialog_app_content'.tr()),
                                      SizedBox(height: 20),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: ElevatedButton(
                                              style: ElevatedButton.styleFrom(
                                                backgroundColor: kPrimaryColor,
                                              ),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: Text('no'.tr()),
                                            ),
                                          ),
                                          SizedBox(width: 20),
                                          Expanded(
                                            child: ElevatedButton(
                                              style: ElevatedButton.styleFrom(
                                                backgroundColor: kPrimaryColor,
                                              ),
                                              onPressed: () async {
                                                int mykadCount =
                                                    await EKYCRepositories
                                                        .getMykadCount();

                                                // int mykadCount = 0;

                                                print(
                                                    'mykad count $mykadCount');

                                                if (mykadCount < 2) {
                                                  // Navigator.of(context).pop();
                                                  Navigator.of(context)
                                                      .pushReplacement(
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          EKYCIC(),
                                                    ),
                                                  );
                                                } else {
                                                  Navigator.of(context)
                                                      .pushReplacement(
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          EKYCFailed(),
                                                    ),
                                                  );
                                                }
                                              },
                                              child: Text('yes'.tr()),
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                );
                              },
                            );
                          },
                          child: Text(
                            'verify_app'.tr(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        width: double.infinity,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: kPrimaryColor),
                          onPressed: () async {
                            Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                builder: (context) => EkycManual(),
                              ),
                            );
                          },
                          child: Text(
                            'verify_onsite'.tr(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 30),
              ],
            );
          } else {
            return Center(
              child: SpinKitFadingCircle(
                color: kPrimaryColor,
                size: 30,
              ),
            );
          }
        },
      ),
    );
  }
}

class DotInfo extends StatelessWidget {
  final String number;
  final Color color;
  final bool finished;
  final bool progress;

  const DotInfo({
    Key? key,
    required this.number,
    required this.color,
    required this.finished,
    required this.progress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: progress ? EdgeInsets.all(15) : EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: finished ? Colors.green : color,
          shape: BoxShape.circle,
        ),
        child: finished
            ? Center(
                child: Icon(
                  FontAwesomeIcons.check,
                  color: Colors.white,
                  size: 14,
                ),
              )
            : Center(
                child: Text(
                  number,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: progress ? 18 : 14,
                  ),
                ),
              ),
      ),
    );
  }
}

class RowDot extends StatelessWidget {
  const RowDot({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 3,
            height: 3,
            decoration: BoxDecoration(
              color: Color(0xFFFF4AD08),
              shape: BoxShape.circle,
            ),
          ),
          SizedBox(width: 2),
          Container(
            width: 3,
            height: 3,
            decoration: BoxDecoration(
              color: Color(0xFFFF4AD08),
              shape: BoxShape.circle,
            ),
          ),
          SizedBox(width: 2),
          Container(
            width: 3,
            height: 3,
            decoration: BoxDecoration(
              color: Color(0xFFFF4AD08),
              shape: BoxShape.circle,
            ),
          ),
          SizedBox(width: 2),
          Container(
            width: 3,
            height: 3,
            decoration: BoxDecoration(
              color: Color(0xFFFF4AD08),
              shape: BoxShape.circle,
            ),
          ),
          SizedBox(width: 2),
          Container(
            width: 3,
            height: 3,
            decoration: BoxDecoration(
              color: Color(0xFFFF4AD08),
              shape: BoxShape.circle,
            ),
          ),
        ],
      ),
    );
  }
}
