// import 'package:camera/camera.dart';
// import 'package:flutter/material.dart';
// import 'package:selangkah_new/Screens/eKYC/ekyc.dart';
// import 'package:selangkah_new/utils/constants.dart';

// class EKYCSelfie extends StatefulWidget {
//   const EKYCSelfie({Key? key}) : super(key: key);

//   @override
//   _EKYCSelfieState createState() => _EKYCSelfieState();
// }

// class _EKYCSelfieState extends State<EKYCSelfie> {
//   @override
//   Widget build(BuildContext context) {
//     double height = MediaQuery.of(context).size.height -
//         MediaQuery.of(context).padding.top -
//         AppBar().preferredSize.height;
//     return EKYCScaffold(
//       appBarTitle: 'Selangor Saring',
//       child: Column(
//         children: [
//           Expanded(
//             flex: 5,
//             child: SingleChildScrollView(
//               padding: EdgeInsets.all(15),
//               child: Column(
//                 // crossAxisAlignment: CrossAxisAlignment.center,
//                 children: [
//                   // Container(
//                   //   color: Colors.black,
//                   //   height: 1000,
//                   // ),

//                   Center(
//                     child: Image.asset(
//                       'assets/images/Ekyc/face_circle.png',
//                       height: height * 0.30,
//                     ),
//                   ),

//                   Text(
//                     'We need your selfie',
//                     style: TextStyle(
//                       fontWeight: FontWeight.bold,
//                       fontSize: 18,
//                     ),
//                   ),

//                   SizedBox(height: 40),

//                   Container(
//                     padding: EdgeInsets.symmetric(horizontal: 20),
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Row(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Text('•'),
//                             SizedBox(width: 10),
//                             Expanded(
//                               child: Text(
//                                 'Photo has to be clear, sharp and include your full face',
//                                 textAlign: TextAlign.left,
//                               ),
//                             ),
//                           ],
//                         ),
//                         Row(
//                           children: [
//                             Text('•'),
//                             SizedBox(width: 10),
//                             Expanded(
//                               child: Text(
//                                 'Avoid reflections or shadows on photo',
//                                 textAlign: TextAlign.left,
//                               ),
//                             ),
//                           ],
//                         ),
//                       ],
//                     ),
//                   ),

//                   SizedBox(height: 40),

//                   Row(
//                     children: [
//                       DotInfo(
//                         number: '1',
//                         color: Colors.green,
//                         finished: true,
//                         progress: false,
//                       ),
//                       RowDot(),
//                       DotInfo(
//                         number: '2',
//                         color: kPrimaryColor,
//                         finished: true,
//                         progress: false,
//                       ),
//                       RowDot(),
//                       DotInfo(
//                         number: '3',
//                         color: Color(0xFFF4AD08),
//                         finished: false,
//                         progress: false,
//                       ),
//                       RowDot(),
//                       DotInfo(
//                         number: '4',
//                         color: Color(0xFFF4AD08),
//                         finished: false,
//                         progress: false,
//                       ),
//                     ],
//                   ),
//                   SizedBox(height: 10),

//                   Row(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Expanded(
//                         child: Text(
//                           'Take photo of your NRIC',
//                           textAlign: TextAlign.center,
//                         ),
//                       ),
//                       Spacer(),
//                       Expanded(
//                         child: Text(
//                           'Live camera  interaction of yourself',
//                           textAlign: TextAlign.center,
//                         ),
//                       ),
//                       Spacer(),
//                       Expanded(
//                         child: Text(
//                           'Take photo of yourself',
//                           textAlign: TextAlign.center,
//                         ),
//                       ),
//                       Spacer(),
//                       Expanded(
//                         child: Text(
//                           'Wait for your account to be verified',
//                           textAlign: TextAlign.center,
//                         ),
//                       ),
//                     ],
//                   ),

//                   SizedBox(height: 40),

//                   Text(
//                     'Disclaimer: Your account data and information are kept secure and confidential',
//                     style: TextStyle(
//                       color: Color(0xFF767674),
//                       fontStyle: FontStyle.italic,
//                       fontWeight: FontWeight.bold,
//                     ),
//                     textAlign: TextAlign.center,
//                   ),

//                   SizedBox(height: 20),
//                 ],
//               ),
//             ),
//           ),
//           Expanded(
//             child: Align(
//               alignment: Alignment.bottomCenter,
//               child: Container(
//                 padding: EdgeInsets.all(15),
//                 width: double.infinity,
//                 child: ElevatedButton(
//                   style: ElevatedButton.styleFrom(primary: kPrimaryColor),
//                   onPressed: () async {
//                     List<CameraDescription> camera = await availableCameras();

//                     Navigator.of(context).pushReplacement(
//                       MaterialPageRoute(
//                         builder: (context) => EKYCSelfieCamera(
//                           camera: camera,
//                         ),
//                       ),
//                     );

//                     // _pickImage(ImageSource.camera);
//                   },
//                   child: Text('Take Selfie'),
//                 ),
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
