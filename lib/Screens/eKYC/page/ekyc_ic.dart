import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:selangkah_new/Screens/eKYC/ekyc.dart';
import 'package:selangkah_new/utils/constants.dart';

class EKYCIC extends StatelessWidget {
  const EKYCIC({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top -
        AppBar().preferredSize.height;
    return EKYCScaffold(
      appBarTitle: 'Selangor Saring',
      child: Column(
        children: [
          Expanded(
            flex: 5,
            child: SingleChildScrollView(
              padding: EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Container(
                  //   color: Colors.black,
                  //   height: 1000,
                  // ),
                  SizedBox(height: 10),

                  Center(
                    child: Image.asset(
                      'assets/images/Ekyc/ic_scan.png',
                      height: height * 0.275,
                    ),
                  ),
                  SizedBox(height: 35),
                  Center(
                    child: Text(
                      'take_front_ic'.tr(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                    ),
                  ),
                  SizedBox(height: 10),

                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('•'),
                      SizedBox(width: 5),
                      Expanded(
                        child: Text(
                          'ekyc_ic_info1'.tr(),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('•'),
                      SizedBox(width: 5),
                      Expanded(
                        child: Text(
                          'ekyc_ic_info2'.tr(),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('•'),
                      SizedBox(width: 5),
                      Expanded(
                        child: Text(
                          'ekyc_ic_info3'.tr(),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),

                  Row(
                    children: [
                      DotInfo(
                        number: '1',
                        color: Color(0xFFF4AD08),
                        finished: false,
                        progress: true,
                      ),
                      RowDot(),
                      DotInfo(
                        number: '2',
                        color: Colors.grey,
                        finished: false,
                        progress: false,
                      ),
                      // RowDot(),
                      // DotInfo(
                      //   number: '3',
                      //   color: Color(0xFFF4AD08),
                      //   finished: false,
                      //   progress: false,
                      // ),
                      RowDot(),
                      DotInfo(
                        number: '3',
                        color: Colors.grey,
                        finished: false,
                        progress: false,
                      ),
                    ],
                  ),
                  SizedBox(height: 10),

                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          'take_photo_nric'.tr(),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Spacer(),
                      Expanded(
                        child: Text(
                          'live_camera_interaction'.tr(),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      // Spacer(),
                      // Expanded(
                      //   child: Text(
                      //     'Take photo of yourself',
                      //     textAlign: TextAlign.center,
                      //   ),
                      // ),
                      Spacer(),
                      Expanded(
                        child: Text(
                          'wait_account_verified'.tr(),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),

                  SizedBox(height: 40),

                  Text(
                    'disclaimer_ekyc'.tr(),
                    style: TextStyle(
                      color: Color(0xFF767674),
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),

                  SizedBox(height: 20),
                ],
              ),
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.all(15),
                width: double.infinity,
                child: ElevatedButton(
                  style:
                      ElevatedButton.styleFrom(backgroundColor: kPrimaryColor),
                  onPressed: () async {
                    List<CameraDescription> camera = await availableCameras();

                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) => EKYCCamera(
                          camera: camera,
                        ),
                      ),
                    );
                  },
                  child: Text('take_photo'.tr()),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
