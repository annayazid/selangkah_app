// To parse this JSON data, do
//
//     final ekycStatus = ekycStatusFromJson(jsonString);

import 'dart:convert';

EkycStatus ekycStatusFromJson(String str) =>
    EkycStatus.fromJson(json.decode(str));

String ekycStatusToJson(EkycStatus data) => json.encode(data.toJson());

class EkycStatus {
  EkycStatus({
    this.rmEkyc,
  });

  RmEkyc? rmEkyc;

  factory EkycStatus.fromJson(Map<String, dynamic> json) => EkycStatus(
        rmEkyc: json["rmEKYC"] == null ? null : RmEkyc.fromJson(json["rmEKYC"]),
      );

  Map<String, dynamic> toJson() => {
        "rmEKYC": rmEkyc == null ? null : rmEkyc!.toJson(),
      };
}

class RmEkyc {
  RmEkyc({
    this.item,
  });

  Item? item;

  factory RmEkyc.fromJson(Map<String, dynamic> json) => RmEkyc(
        item: json["item"] == null ? null : Item.fromJson(json["item"]),
      );

  Map<String, dynamic> toJson() => {
        "item": item == null ? null : item!.toJson(),
      };
}

class Item {
  Item({
    this.id,
    this.ekycId,
    this.action,
    this.similarity,
    this.isSamePerson,
    this.status,
    this.updatedAt,
    this.queryImageContent,
  });

  String? id;
  String? ekycId;
  String? action;
  int? similarity;
  bool? isSamePerson;
  String? status;
  DateTime? updatedAt;
  String? queryImageContent;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["id"] == null ? null : json["id"],
        ekycId: json["ekycId"] == null ? null : json["ekycId"],
        action: json["action"] == null ? null : json["action"],
        similarity: json["similarity"] == null ? null : json["similarity"],
        isSamePerson:
            json["isSamePerson"] == null ? null : json["isSamePerson"],
        status: json["status"] == null ? null : json["status"],
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        queryImageContent: json["query_image_content"] == null
            ? null
            : json["query_image_content"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "ekycId": ekycId == null ? null : ekycId,
        "action": action == null ? null : action,
        "similarity": similarity == null ? null : similarity,
        "isSamePerson": isSamePerson == null ? null : isSamePerson,
        "status": status == null ? null : status,
        "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "query_image_content":
            queryImageContent == null ? null : queryImageContent,
      };
}
