// To parse this JSON data, do
//
//     final statusIc = statusIcFromJson(jsonString);

import 'dart:convert';

StatusIc statusIcFromJson(String str) => StatusIc.fromJson(json.decode(str));

String statusIcToJson(StatusIc data) => json.encode(data.toJson());

class StatusIc {
  StatusIc({
    this.code,
    this.requestId,
  });

  int? code;
  String? requestId;

  factory StatusIc.fromJson(Map<String, dynamic> json) => StatusIc(
        code: json["Code"] == null ? null : json["Code"],
        requestId: json["RequestID"] == null ? null : json["RequestID"],
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "RequestID": requestId == null ? null : requestId,
      };
}
