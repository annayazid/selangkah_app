// To parse this JSON data, do
//
//     final icDetails = icDetailsFromJson(jsonString);

import 'dart:convert';

IcDetails icDetailsFromJson(String str) => IcDetails.fromJson(json.decode(str));

String icDetailsToJson(IcDetails data) => json.encode(data.toJson());

class IcDetails {
  IcDetails({
    this.code,
    this.data,
  });

  int? code;
  Data? data;

  factory IcDetails.fromJson(Map<String, dynamic> json) => IcDetails(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null ? null : Data.fromJson(json["Data"]),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null ? null : data!.toJson(),
      };
}

class Data {
  Data({
    this.address1,
    this.address2,
    this.state,
    this.city,
    this.postcode,
    this.gender,
    this.ic,
    this.name,
  });

  String? address2;
  String? address1;
  String? state;
  String? city;
  String? postcode;
  String? gender;
  String? ic;
  String? name;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        address1: json["Address 1:"] == null ? null : json["Address 1:"],
        address2: json["Address 2:"] == null ? null : json["Address 2:"],
        state: json["State"] == null ? null : json["State"],
        city: json["City"] == null ? null : json["City"],
        postcode: json["Postcode"] == null ? null : json["Postcode"],
        gender: json["Gender"] == null ? null : json["Gender"],
        ic: json["IC"] == null ? null : json["IC"],
        name: json["Name"] == null ? null : json["Name"],
      );

  Map<String, dynamic> toJson() => {
        "Address 1:": address1 == null ? null : address1,
        "Address 2:": address2 == null ? null : address2,
        "State": state == null ? null : state,
        "City": city == null ? null : city,
        "Postcode": postcode == null ? null : postcode,
        "Gender": gender == null ? null : gender,
        "IC": ic == null ? null : ic,
        "Name": name == null ? null : name,
      };
}
