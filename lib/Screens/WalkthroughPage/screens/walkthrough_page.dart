import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/SignIn/sign_in.dart';
import 'package:selangkah_new/utils/constants.dart';

class WalkthroughPage extends StatefulWidget {
  const WalkthroughPage({super.key});

  @override
  State<WalkthroughPage> createState() => _WalkthroughPageState();
}

class _WalkthroughPageState extends State<WalkthroughPage> {
  double currentIndexPage = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: PageView(
              children: [
                WalkThrough(
                    heading: 'welcome'.tr(),
                    textContent: 'walkthrough1'.tr(),
                    bgImg: "assets/images/WalkthroughPage/walk_1.png",
                    walkImg:
                        "assets/images/WalkthroughPage/form_background.png"),
                WalkThrough(
                    heading: 'SELANGKAHID'.tr(),
                    textContent: 'walkthrough2'.tr(),
                    bgImg: "assets/images/WalkthroughPage/walk_2.png",
                    walkImg:
                        "assets/images/WalkthroughPage/selangor_flag_map.png"),
                WalkThrough(
                    heading: 'appfeatures'.tr(),
                    textContent: 'walkthrough3'.tr(),
                    bgImg: "assets/images/WalkthroughPage/walk_3.png",
                    walkImg:
                        "assets/images/WalkthroughPage/mobile_solution.png"),
                WalkThrough(
                    bgImg: '',
                    heading: 'SELANGKAHproducts'.tr(),
                    textContent: 'walkthrough4'.tr(),
                    walkImg:
                        "assets/images/WalkthroughPage/product_lineup.png"),
              ],
              onPageChanged: (value) {
                setState(() => currentIndexPage = value.toDouble());
              },
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: DotsIndicator(
                  dotsCount: 4,
                  position: currentIndexPage,
                  decorator: DotsDecorator(
                    size: Size.square(8.0),
                    activeSize: Size.square(12.0),
                    color: Colors.grey,
                    activeColor: kPrimaryColor,
                  )),
            ),
          )
        ],
      ),
    );
  }
}

class WalkThrough extends StatelessWidget {
  final String textContent;
  final String bgImg;
  final String walkImg;
  final String heading;

  WalkThrough({
    Key? key,
    required this.textContent,
    required this.bgImg,
    required this.walkImg,
    required this.heading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Stack(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: height * 0.05),
                height: height * 0.5,
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    bgImg != ''
                        ? Image.asset(
                            bgImg,
                            width: width,
                            height: height * 0.5,
                            fit: BoxFit.fill,
                          )
                        : Container(),
                    Image.asset(
                      walkImg,
                      width: width * 0.8,
                      height: height * 0.6,
                    )
                  ],
                ),
              ),
              SizedBox(height: height * 0.08),
              Text(
                heading,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
              SizedBox(height: height * 0.02),
              Padding(
                padding: EdgeInsets.only(left: 28.0, right: 28.0),
                child: Text(
                  textContent,
                  textAlign: TextAlign.center,
                  style: TextStyle(),
                ),
              )
            ],
          ),
        ),
        bgImg == ''
            ? Align(
                alignment: Alignment.bottomCenter,
                child: GestureDetector(
                  onTap: () {
                    //toast("Sign in clicked");
                    // SignInScreen().launch(context, isNewTask: true);

                    // Navigator.of(context).pushReplacement(
                    //   MaterialPageRoute(
                    //     builder: (context) => BlocProvider(
                    //       create: (context) => PrivacyTermCubit(),
                    //       child: SignInScreen(),
                    //     ),
                    //   ),
                    // );

                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) => MultiBlocProvider(
                          providers: [
                            BlocProvider(
                              create: (context) => PrivacyTermCubit(),
                            ),
                            BlocProvider(
                              create: (context) => CheckUserCubit(),
                            ),
                          ],
                          child: SignInScreen(),
                        ),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 50),
                    decoration: BoxDecoration(
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    alignment: Alignment.center,
                    height: width / 8,
                    child: Text(
                      'signinsignup'.tr(),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              )
            : Container()
      ],
    );
  }
}
