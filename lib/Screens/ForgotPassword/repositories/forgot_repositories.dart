import 'package:selangkah_new/utils/endpoint.dart';
import 'package:http/http.dart' as http;

class ForgotRepositories {
  static Future<void> resetCount(String phoneNum) async {
    final Map<String, String> map = {
      'token': TOKEN,
      'phonenumber': phoneNum,
    };

    await http.post(
      Uri.parse('$API_URL/reset_failed_count'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    print('Calling api reset failed count');
  }
}
