import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:selangkah_new/Screens/ForgotPassword/forgot_pass.dart';
import 'package:selangkah_new/Screens/SelangkahAppBarScaffold/scaffold.dart';
import 'package:selangkah_new/Screens/VerifyOTP/verifyOTP.dart';
import 'package:selangkah_new/utils/constants.dart';

class ForgotPasswordPage extends StatefulWidget {
  ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  final TextEditingController _phoneNumController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String? phonenumber;
  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    double height = MediaQuery.of(context).size.height -
        AppBar().preferredSize.height -
        MediaQuery.of(context).padding.top -
        MediaQuery.of(context).padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return TopBarScaffold(
        title: 'forgotPassword'.tr(),
        content: SingleChildScrollView(
          child: Center(
            child: Container(
              width: width * 0.9,
              padding: EdgeInsets.symmetric(vertical: 25),
              height: height,
              child: Form(
                key: _formKey,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'forgotPassword'.tr().toString(),
                      style:
                          TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 15),
                    Container(
                      // margin: EdgeInsets.symmetric(vertical: 10),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      // width: width * 0.8,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(width: 1, color: Colors.grey),
                      ),
                      child: TextFormField(
                        controller: _phoneNumController,
                        keyboardType: TextInputType.phone,
                        validator: validatePhoneNum,
                        cursorColor: kPrimaryColor,
                        decoration: InputDecoration(
                          icon: Icon(
                            Icons.phone,
                            color: kPrimaryColor,
                          ),
                          hintText: 'phonenumber'.tr(),
                          prefixText: "+6 ",
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                    // SizedBox(height: 15),
                    Spacer(),
                    Container(
                      width: width * 0.9,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          backgroundColor: kPrimaryColor,
                        ),
                        onPressed: () {
                          final isValid = _formKey.currentState!.validate();
                          if (!isValid) {
                            print(_phoneNumController);

                            return;
                          } else {
                            _formKey.currentState!.save();

                            context.read<ForgotAccCubit>().forgotAccount(
                                  _phoneNumController.text,
                                );
                          }
                        },
                        child: BlocConsumer<ForgotAccCubit, ForgotAccState>(
                          listener: (context, state) {
                            print(state.toString());

                            context.deviceLocale.languageCode;
                            if (state is ForgotAccVerifyOTP) {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => BlocProvider(
                                    create: (context) => OtpVerifyCubit(),
                                    child: VerifyOTP(
                                      fromScreen: 'forgotpass',
                                      phone: _phoneNumController.text,
                                    ),
                                  ),
                                ),
                              );
                            }
                          },
                          builder: (context, state) {
                            if (state is ForgotAccLoading) {
                              return Padding(
                                padding: EdgeInsets.all(18.0),
                                child: SpinKitFadingCircle(
                                  color: Colors.white,
                                  size: 18,
                                ),
                              );
                            } else {
                              return Container(
                                child: Center(
                                  child: Padding(
                                    padding: EdgeInsets.all(18.0),
                                    child: Text(
                                      'next_forgot_password'.tr(),
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16),
                                      // textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              );
                            }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  String? validatePhoneNum(String? value) {
    print(value);
    String patttern = r'(^[0-9]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value!.length == 0) {
      return 'phonerequired'.tr().toString();
    } else if (value.length < 10) {
      return 'phone10'.tr().toString();
    } else if (value.length >= 12) {
      return 'phone12'.tr().toString();
    } else if (!regExp.hasMatch(value)) {
      return 'phonedigit'.tr().toString();
    } else if (!value.startsWith('01')) {
      return 'phon01'.tr().toString();
    }
    return null;
  }
}
