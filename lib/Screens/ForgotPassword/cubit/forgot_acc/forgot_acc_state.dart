part of 'forgot_acc_cubit.dart';

@immutable
abstract class ForgotAccState {
  const ForgotAccState();
}

class ForgotAccInitial extends ForgotAccState {
  const ForgotAccInitial();
}

class ForgotAccLoading extends ForgotAccState {
  const ForgotAccLoading();
}

class ForgotAccNoInternet extends ForgotAccState {
  const ForgotAccNoInternet();
}

class ForgotAccNonExist extends ForgotAccState {
  const ForgotAccNonExist();
}

class ForgotAccVerifyOTP extends ForgotAccState {
  const ForgotAccVerifyOTP();
}
