import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/SignIn/sign_in.dart';

part 'forgot_acc_state.dart';

class ForgotAccCubit extends Cubit<ForgotAccState> {
  ForgotAccCubit() : super(ForgotAccInitial());

  Future<void> forgotAccount(String phoneNum) async {
    emit(ForgotAccLoading());

    bool userReg = await SignInRepositories.checkUserReg(phoneNum);

    print(userReg);

    if (userReg) {
      var connectivityResult = await (Connectivity().checkConnectivity());
      // I am connected to a mobile network.
      // I am connected to a wifi network.
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        emit(ForgotAccVerifyOTP());
      } else {
        Fluttertoast.showToast(msg: "nointernet".tr());
        emit(ForgotAccNoInternet());
      }
    } else {
      Fluttertoast.showToast(msg: "nouser".tr());
      emit(ForgotAccNonExist());
    }
  }
}
