import 'dart:convert';
import 'dart:developer';

import 'package:crypto/crypto.dart';
import 'package:http/http.dart' as http;
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/WavPay/wavpay.dart';
import 'package:selangkah_new/utils/endpoint.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class WavPayRepositories {
  static Future<bool> registerWallet() async {
    String name = await SecureStorage().readSecureData('userName');
    String idNumber = await SecureStorage().readSecureData('userIdNo');
    String mobile = await SecureStorage().readSecureData('userPhoneNo');

    String uniqueId = await SecureStorage().readSecureData('userSelId');

    idNumber =
        '${idNumber.substring(0, 6)}-${idNumber.substring(6, 8)}-${idNumber.substring(8, 12)}';

    print(idNumber);

    String combinedString =
        '$name|$idNumber|$mobile|$WAVPAY_PARTNER_ID|$uniqueId|$WAVPAY_PARTNER_SECRET_KEY';

    print(combinedString);

    String hashedString =
        sha256.convert(utf8.encode(combinedString)).toString();

    Map map = {
      "fullNameAsID": name,
      "idNumber": idNumber,
      "mobileNumber": mobile,
      "partnerId": WAVPAY_PARTNER_ID,
      "uniqueRefId": uniqueId,
      "secureHash": hashedString,
    };

    print(map.toString());

    String body = json.encode(map);

    final response = await http.post(
      Uri.parse('$API_WAVPAY/register'),
      // 'API_WAVPAY/register',

      headers: {'Content-Type': 'application/json'},
      body: body,
    );

    print(response.body);

    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  static Future<String> getToken() async {
    String uniqueId = await SecureStorage().readSecureData('userSelId');

    String combinedString =
        '$WAVPAY_PARTNER_ID|$uniqueId|$WAVPAY_PARTNER_SECRET_KEY';

    print(combinedString);

    String hashedString =
        sha256.convert(utf8.encode(combinedString)).toString();

    Map map = {
      "partnerId": WAVPAY_PARTNER_ID,
      "uniqueRefId": uniqueId,
      "secureHash": hashedString,
    };

    print(map.toString());

    String body = json.encode(map);

    final response = await http.post(
      Uri.parse('$API_WAVPAY/token/request'),
      // 'API_WAVPAY/token/request',

      headers: {'Content-Type': 'application/json'},
      body: body,
    );

    print(response.body);

    var data = jsonDecode(response.body);

    String accessToken = data['accessToken'];
    return accessToken;
  }

  static Future<WavpayBalance?> getBalance() async {
    String uniqueId = await SecureStorage().readSecureData('userSelId');

    String combinedString =
        '$WAVPAY_PARTNER_ID|$uniqueId|$WAVPAY_PARTNER_SECRET_KEY';

    print('combined string is $combinedString');

    print(combinedString);

    String hashedString =
        sha256.convert(utf8.encode(combinedString)).toString();

    Map map = {
      "partnerId": WAVPAY_PARTNER_ID,
      "uniqueRefId": uniqueId,
      "secureHash": hashedString,
    };

    print(map.toString());

    String body = json.encode(map);

    final response = await http.post(
      Uri.parse('$API_WAVPAY/user/balance'),
      // 'API_WAVPAY/token/request',

      headers: {'Content-Type': 'application/json'},
      body: body,
    );

    log(response.body);

    if (response.statusCode == 200) {
      try {
        WavpayBalance balance = wavpayBalanceFromJson(response.body);
        print('return dapat balance');
        return balance;
      } catch (e) {
        print(e.toString());
        print('return tak dpt balance');

        return null;
      }
    } else if (response.statusCode == 500) {
      print('return error');

      return WavpayBalance(
        balance: null,
        partnerId: null,
        totalBalance: null,
        uniqueRefId: null,
      );
    } else {
      return null;
    }
  }

  static Future<RawProfile> getProfile() async {
    String id = await SecureStorage().readSecureData('userId');

    final Map<String, String> map = {
      'id': id,
      'token': TOKEN,
    };

    //print('calling post GET_PROFILE');

    final response = await http.post(
      Uri.parse('$API_URL/check_user_profile'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      body: map,
    );

    RawProfile? profile = rawProfileFromJson(response.body);
    return profile!;
  }
}
