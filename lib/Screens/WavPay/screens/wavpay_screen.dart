import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selangkah_new/Screens/VerifyProfile/screens/verify_profile.dart';
import 'package:selangkah_new/Screens/WavPay/wavpay.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:url_launcher/url_launcher.dart';

class WavpayScreen extends StatefulWidget {
  final bool registered;
  final String? balance;

  const WavpayScreen({Key? key, required this.registered, this.balance})
      : super(key: key);

  @override
  _WavpayScreenState createState() => _WavpayScreenState();
}

class _WavpayScreenState extends State<WavpayScreen> {
  @override
  void initState() {
    context.read<RegisterWalletCubit>().registerWallet(widget.registered);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height -
    //     AppBar().preferredSize.height -
    //     MediaQuery.of(context).padding.top;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.asset(
          'assets/images/selangkah_logo.png',
          width: width * 0.45,
        ),
      ),
      body: Scaffold(
        appBar: AppBar(
          title: Text('BINGKAS'),
          centerTitle: true,
        ),
        body: BlocConsumer<RegisterWalletCubit, RegisterWalletState>(
          listener: (context, state) {
            if (state is RegisterWalletError) {
              Fluttertoast.showToast(msg: 'There is an error');
              Navigator.of(context).pop();
            }

            if (state is RegisterWalletComplete) {
              Navigator.of(context).pop();

              print('launching ${state.url}');
              // launch(state.url);
              launchUrl(
                Uri.parse(state.url),
                mode: LaunchMode.externalApplication,
              );
            }

            if (state is RegisterWalletProfileComplete) {
              context
                  .read<RegisterWalletCubit>()
                  .registerWallet(widget.registered);
            }

            if (state is RegisterWalletProfileIncomplete) {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return VerifyProfile();
                  },
                ),
              );
              // Navigator.of(context).pop();
            }
          },
          builder: (context, state) {
            // if (state is RegisterWalletComplete) {
            //   return SingleChildScrollView(
            //     child: Container(
            //       height: height - AppBar().preferredSize.height,
            //       child: Column(
            //         crossAxisAlignment: CrossAxisAlignment.center,
            //         children: [
            //           Container(
            //             margin: EdgeInsets.all(20),
            //             child: Stack(
            //               children: [
            //                 Image.asset(
            //                   'assets/images/bingkas/wavpay_balance.png',
            //                   // fit: BoxFit.fitHeight,
            //                 ),
            //                 Positioned(
            //                   bottom: 25,
            //                   child: Container(
            //                     padding: EdgeInsets.only(left: 10),
            //                     child: Column(
            //                       crossAxisAlignment: CrossAxisAlignment.start,
            //                       children: [
            //                         Text(
            //                           'BAKI AKAUN',
            //                           style: TextStyle(
            //                             fontWeight: FontWeight.bold,
            //                             fontSize: 10,
            //                             color: Colors.white,
            //                           ),
            //                         ),
            //                         Text(
            //                           widget.balance,
            //                           style: TextStyle(
            //                             fontWeight: FontWeight.bold,
            //                             color: Colors.white,
            //                           ),
            //                         ),
            //                       ],
            //                     ),
            //                   ),
            //                 ),
            //               ],
            //             ),
            //           ),
            //           SizedBox(height: 10),
            //           Container(
            //             decoration: BoxDecoration(
            //               color: Colors.white,
            //               borderRadius: BorderRadius.circular(5),
            //             ),
            //             padding: EdgeInsets.all(10),
            //             child: Column(
            //               mainAxisSize: MainAxisSize.min,
            //               children: [
            //                 BarcodeWidget(
            //                   barcode: Barcode.code128(),
            //                   data: state.selangkahId,
            //                   width: width * 0.8,
            //                   height: height * 0.15,
            //                   style: TextStyle(fontSize: 9),
            //                   drawText: false,
            //                 ),
            //                 SizedBox(height: 5),
            //                 Text(
            //                   state.selangkahId,
            //                   style: TextStyle(fontSize: 14),
            //                 ),
            //               ],
            //             ),
            //           ),
            //           SizedBox(height: 20),
            //           Spacer(),
            //           Container(
            //             padding: EdgeInsets.symmetric(horizontal: 20),
            //             width: double.infinity,
            //             child: ElevatedButton(
            //               style:
            //                   ElevatedButton.styleFrom(primary: kPrimaryColor),
            //               onPressed: () {
            //                 // print('launching ${state.url}');
            //                 launch(state.url);
            //               },
            //               child: Text('open_wavpay'.tr()),
            //             ),
            //           ),
            //           SizedBox(height: 20),
            //         ],
            //       ),
            //     ),
            //   );
            // }
            // else {

            //   return SpinKitFadingCircle(
            //     color: kPrimaryColor,
            //     size: 25,
            //   );
            // }
            return SpinKitFadingCircle(
              color: kPrimaryColor,
              size: 25,
            );
          },
        ),
      ),
    );
  }
}
