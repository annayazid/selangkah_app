part of 'register_wallet_cubit.dart';

@immutable
abstract class RegisterWalletState {
  const RegisterWalletState();
}

class RegisterWalletInitial extends RegisterWalletState {
  const RegisterWalletInitial();
}

class RegisterWalletLoading extends RegisterWalletState {
  const RegisterWalletLoading();
}

class RegisterWalletComplete extends RegisterWalletState {
  final String url;
  final String selangkahId;

  RegisterWalletComplete(this.url, this.selangkahId);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RegisterWalletComplete &&
        other.url == url &&
        other.selangkahId == selangkahId;
  }

  @override
  int get hashCode => url.hashCode ^ selangkahId.hashCode;
}

class RegisterWalletError extends RegisterWalletState {
  const RegisterWalletError();
}

class RegisterWalletProfileIncomplete extends RegisterWalletState {
  const RegisterWalletProfileIncomplete();
}

class RegisterWalletProfileComplete extends RegisterWalletState {
  const RegisterWalletProfileComplete();
}
