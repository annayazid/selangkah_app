import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Screens/WavPay/wavpay.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

part 'register_wallet_state.dart';

class RegisterWalletCubit extends Cubit<RegisterWalletState> {
  RegisterWalletCubit() : super(RegisterWalletInitial());

  Future<void> registerWallet(bool registered) async {
    emit(RegisterWalletLoading());

    RawProfile profile = await WavPayRepositories.getProfile();

    String? name = profile.data![2];
    String? idCard = profile.data![4];
    String? idType = profile.data![17];
    String? citizenship = profile.data![6];
    String? gender = profile.data![7];
    String? dob = profile.data![8];
    String? email = profile.data![15];
    String? phone = profile.data![19];
    String? address1 = profile.data![9];
    String? postcode = profile.data![11];
    String? city = profile.data![12];
    String? state = profile.data![13];
    String? country = profile.data![14];
    // String? status = profile.data![20];

    String id = await SecureStorage().readSecureData('userSelId');

    if (name == null ||
        idCard == null ||
        idType == null ||
        citizenship == null ||
        gender == null ||
        dob == null ||
        email == null ||
        phone == null ||
        address1 == null ||
        postcode == null ||
        city == null ||
        state == null ||
        country == null) {
      print('incomplete');
      emit(RegisterWalletProfileIncomplete());
    } else {
      if (!registered) {
        bool success = await WavPayRepositories.registerWallet();

        if (success) {
          String accessToken = await WavPayRepositories.getToken();

          // await Future.delayed(Duration(seconds: 1));

          emit(
            RegisterWalletComplete(
                'https://www.wavpay.net/app-links?token=$accessToken', id),
          );
        } else {
          // emit(RegisterWalletError());
          String accessToken = await WavPayRepositories.getToken();

          // await Future.delayed(Duration(seconds: 1));

          emit(RegisterWalletComplete(
              'https://www.wavpay.net/app-links?token=$accessToken', id));
        }
      } else {
        String accessToken = await WavPayRepositories.getToken();

        // await Future.delayed(Duration(seconds: 1));

        emit(RegisterWalletComplete(
            'https://www.wavpay.net/app-links?token=$accessToken', id));
      }
    }
    // print('complete');
  }
}
