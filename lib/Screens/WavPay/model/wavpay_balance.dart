// To parse this JSON data, do
//
//     final wavpayBalance = wavpayBalanceFromJson(jsonString);

import 'dart:convert';

WavpayBalance wavpayBalanceFromJson(String str) =>
    WavpayBalance.fromJson(json.decode(str));

String wavpayBalanceToJson(WavpayBalance data) => json.encode(data.toJson());

class WavpayBalance {
  WavpayBalance({
    this.uniqueRefId,
    this.partnerId,
    this.totalBalance,
    this.balance,
  });

  String? uniqueRefId;
  String? partnerId;
  String? totalBalance;
  List<Balance>? balance;

  factory WavpayBalance.fromJson(Map<String, dynamic> json) => WavpayBalance(
        uniqueRefId: json["uniqueRefId"] == null ? null : json["uniqueRefId"],
        partnerId: json["partnerId"] == null ? null : json["partnerId"],
        totalBalance:
            json["totalBalance"] == null ? null : json["totalBalance"],
        balance: json["balance"] == null
            ? null
            : List<Balance>.from(
                json["balance"].map((x) => Balance.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "uniqueRefId": uniqueRefId == null ? null : uniqueRefId,
        "partnerId": partnerId == null ? null : partnerId,
        "totalBalance": totalBalance == null ? null : totalBalance,
        "balance": balance == null
            ? null
            : List<dynamic>.from(balance!.map((x) => x.toJson())),
      };
}

class Balance {
  Balance({
    this.balanceType,
    this.balance,
  });

  String? balanceType;
  String? balance;

  factory Balance.fromJson(Map<String, dynamic> json) => Balance(
        balanceType: json["balanceType"] == null ? null : json["balanceType"],
        balance: json["balance"] == null ? null : json["balance"],
      );

  Map<String, dynamic> toJson() => {
        "balanceType": balanceType == null ? null : balanceType,
        "balance": balance == null ? null : balance,
      };
}
