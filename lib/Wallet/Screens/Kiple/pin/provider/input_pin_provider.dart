import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/otp/verify_phone_number.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/result_page.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

import '../input_pin.dart';

class InputPinProvider extends BaseProvider {
  String? lastPinCode;
  bool? needBackPinCode;

  InputPinProvider(BuildContext buildContext, PinType pinType, String currentTitle, String lastPinCode,
      {bool needBackPinCode = false})
      : super(buildContext) {
    this.pinType = pinType;
    this.currentTitle = currentTitle;
    this.lastPinCode = lastPinCode;
    this.needBackPinCode = needBackPinCode;
  }

  PinType? pinType;
  String? currentTitle;
  List<String> keyBordList = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '', '0', 'delete'];
  String inputPin = "";

  resetInputPin() {
    inputPin = "";
    notifyListeners();
  }

  inputPassword(String key) {
    if (key == 'delete') {
      if (inputPin.length > 0) {
        inputPin = inputPin.substring(0, inputPin.length - 1);
      }
    } else {
      if (inputPin.length < 6) {
        inputPin = inputPin + key;
      }
    }
    if (inputPin.length == 6) {
      inputComplete();
    }
    notifyListeners();
  }

  inputComplete() async {
    if (pinType == PinType.CreateWalletPin) {
      String? title;
      if (currentTitle == 'pin_create_wallet'.tr().toString()) {
        title = 'pin_confirm_wallet'.tr().toString();
      } else if (currentTitle == 'pin_enter_new_wallet'.tr().toString()) {
        title = 'pin_confirm_new_wallet'.tr().toString();
      }

      ResultModel confirmPin = await Navigator.push(
        currentContext,
        MaterialPageRoute(
          builder: (context) {
            return InputPinPage.createByTitle(
              PinType.ConfirmWalletPin,
              title!,
              lastPinCode: inputPin,
            );
          },
        ),
      );
      if (confirmPin != null) {
        Navigator.of(currentContext).pop(
          ResultModel(success: true),
        );
      }
    } else if (pinType == PinType.ChangePinByPin) {
      checkPinFunction(pinType!);
    } else if (pinType == PinType.ConfirmWalletPin) {
      if (lastPinCode == inputPin) {
        if (currentTitle == 'pin_confirm_wallet'.tr().toString()) {
          createPinFunction();
        } else if (currentTitle == 'pin_confirm_new_wallet'.tr().toString()) {
          resetPinFunction();
        }
      } else {
        resetInputPin();
        showMessage(
          'pin_match_fail'.tr().toString(),
        );
      }
    } else if (pinType == PinType.VerifyPin) {
      if (needBackPinCode != null && needBackPinCode!) {
        Navigator.of(currentContext).pop(
          ResultModel(success: true, data: inputPin),
        );
      } else {
        checkPinFunction(pinType!);
      }
    } else if (pinType == PinType.ForgetPin) {
      ResultModel changedResult = await Navigator.push(
        currentContext,
        MaterialPageRoute(
          builder: (context) {
            return InputPinPage.CreateWalletPin();
          },
        ),
      );
      Navigator.of(currentContext).pop();
    } else if (pinType == PinType.PrepaidCard) {
      Map map = Map<String, String>();
      map["pin"] = inputPin;
      requestNetwork<String>(Method.post, url: HttpApi.KipleBaseUrl + HttpApi.CheckPin, params: map, onSuccess: (date) {
        Timer.periodic(Duration(milliseconds: 100), (timer) async {
          timer.cancel();
          ResultModel confirmPin = await Navigator.push(
            currentContext,
            MaterialPageRoute(
              builder: (context) {
                return InputPinPage.createByTitle(
                  PinType.EnterNewCardPin,
                  "pin_new_card_pin".tr(),
                  lastPinCode: inputPin,
                );
              },
            ),
          );
          if (confirmPin != null) {
            Navigator.of(currentContext).pop(
              ResultModel(success: true),
            );
          }
        });
      }, onError: (code, msg) {
        showMessage(msg);
        resetInputPin();
      });
    } else if (pinType == PinType.EnterNewCardPin) {
      Timer.periodic(Duration(milliseconds: 100), (timer) async {
        timer.cancel();
        ResultModel confirmPin = await Navigator.push(
          currentContext,
          MaterialPageRoute(
            builder: (context) {
              return InputPinPage.createByTitle(
                PinType.ConfirmNewCardPin,
                "pin_confirm_card_pin".tr(),
                lastPinCode: inputPin,
              );
            },
          ),
        );
        if (confirmPin != null) {
          Navigator.of(currentContext).pop(
            ResultModel(success: true),
          );
        }
      });
    } else if (pinType == PinType.ConfirmNewCardPin) {
      if (lastPinCode == inputPin) {
        // ready to commit card pin
        Navigator.of(currentContext).pop(
          ResultModel(success: true),
        );
      } else {
        resetInputPin();
        showMessage(
          'pin_match_fail'.tr().toString(),
        );
      }
    } else if (pinType == PinType.VerifyCardPin) {
      if (needBackPinCode != null && needBackPinCode!) {
        Navigator.of(currentContext).pop(
          ResultModel(success: true, data: inputPin),
        );
      } else {
        Navigator.of(currentContext).pop(ResultModel(success: true));
      }
    }
  }

  forgetPassport() async {
    if (pinType == PinType.VerifyCardPin) {
      ResultModel resultModel = await Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
        return InputPinPage.ResetPrepaidCardPin();
      }));
      if (resultModel != null && resultModel.success!) {
        resetInputPin();
      }
    } else {
      ResultModel otpResullt = await Navigator.push(
        currentContext,
        MaterialPageRoute(
          builder: (context) {
            return VerifyPhoneNumberPage(OtpType.ResetPin);
          },
        ),
      );
      if (otpResullt != null) {
        if (otpResullt.success!) {
          ResultModel changedResult = await Navigator.push(
            currentContext,
            MaterialPageRoute(
              builder: (context) {
                return InputPinPage.createByTitle(
                  PinType.CreateWalletPin,
                  'pin_enter_new_wallet'.tr().toString(),
                  lastPinCode: inputPin,
                );
                ;
              },
            ),
          );
          Navigator.of(currentContext).pop();
        }
      } else {
        Navigator.of(currentContext).pop();
      }
    }
  }

  createPinFunction() {
    Map map = Map<String, String>();
    map["pin"] = inputPin;
    requestNetwork<String>(Method.post, url: HttpApi.KipleBaseUrl + HttpApi.CreatePin, params: map, onSuccess: (date) {
      Timer.periodic(Duration(milliseconds: 100), (timer) async {
        timer.cancel();
        ResultModel changedResult = await Navigator.push(
          currentContext,
          MaterialPageRoute(
            builder: (context) {
              return ResultPage(
                contentChild: successChild!,
                status: true,
              );
            },
          ),
        );
        Navigator.of(currentContext).pop(ResultModel(success: true));
      });
    }, onError: (code, msg) {
      showMessage(msg);
      resetInputPin();
    });
  }

  checkPinFunction(PinType pinType) {
    Map map = Map<String, String>();
    map["pin"] = inputPin;
    requestNetwork<String>(Method.post, url: HttpApi.KipleBaseUrl + HttpApi.CheckPin, params: map, onSuccess: (date) {
      Timer.periodic(Duration(milliseconds: 100), (timer) async {
        timer.cancel();
        if (pinType == PinType.ChangePinByPin) {
          ResultModel confirmPin = await Navigator.push(
            currentContext,
            MaterialPageRoute(
              builder: (context) {
                return InputPinPage.createByTitle(
                  PinType.CreateWalletPin,
                  'pin_enter_new_wallet'.tr().toString(),
                  lastPinCode: inputPin,
                );
              },
            ),
          );
        }
        Navigator.of(currentContext).pop(ResultModel(success: true));
      });
    }, onError: (code, msg) {
      showMessage(msg);
      resetInputPin();
    });
  }

  Widget? successChild;

  resetPinFunction() {
    Map map = Map<String, String>();
    map["pin"] = inputPin;
    requestNetwork<String>(Method.post, url: HttpApi.KipleBaseUrl + HttpApi.ResetPin, params: map, onSuccess: (date) {
      Timer.periodic(Duration(milliseconds: 100), (timer) async {
        timer.cancel();
        ResultModel changedResult = await Navigator.push(
          currentContext,
          MaterialPageRoute(
            builder: (context) {
              return ResultPage(
                contentChild: successChild!,
                status: true,
              );
            },
          ),
        );
        Navigator.of(currentContext).pop(ResultModel(success: true));
      });
    }, onError: (code, msg) {
      showMessage(msg);
      resetInputPin();
    });
  }
}
