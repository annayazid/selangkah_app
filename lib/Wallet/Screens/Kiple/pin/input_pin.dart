import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/pin/provider/input_pin_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/utils/AppColors.dart';

enum PinType {
  CreateWalletPin,
  ConfirmWalletPin,
  ChangePinByPin,
  VerifyPin,
  VerifyCardPin,
  ForgetPin,
  PrepaidCard,
  EnterNewCardPin,
  ConfirmNewCardPin
}

class InputPinPage extends StatefulWidget {
  PinType? pinType;
  String? title;
  String lastPinCode = "";
  bool needBackPinCode = true;

  InputPinPage.CreateWalletPin() {
    this.pinType = PinType.CreateWalletPin;
    this.title = 'pin_create_wallet'.tr().toString();
  }

  InputPinPage.VerifyPin({bool needBackPinCode = false}) {
    this.pinType = PinType.VerifyPin;
    this.needBackPinCode = needBackPinCode;
    this.title = 'pin_enter_wallet'.tr().toString();
  }

  InputPinPage.VerifyCardPin({bool needBackPinCode = false}) {
    this.pinType = PinType.VerifyCardPin;
    this.needBackPinCode = needBackPinCode;
    this.title = 'pin_verify_card_pin'.tr().toString();
  }

  InputPinPage.changePinByPin() {
    this.pinType = PinType.ChangePinByPin;
    this.title = 'pin_enter_current_wallet'.tr().toString();
  }

  InputPinPage.ResetPrepaidCardPin() {
    this.pinType = PinType.PrepaidCard;
    this.title = 'pin_enter_wallet'.tr().toString();
  }

  InputPinPage.createByTitle(PinType pinType, String title, {required this.lastPinCode}) {
    this.pinType = pinType;
    this.title = title;
    this.lastPinCode = lastPinCode;
  }

  @override
  State<StatefulWidget> createState() {
    return InputPinState();
  }
}

class InputPinState extends State<InputPinPage> with StateLifeCycle<InputPinPage, InputPinProvider> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    provider.successChild = successChild();
    return ChangeNotifierProvider(
      create: (BuildContext context) => provider,
      child: Consumer(
        builder: (BuildContext context, InputPinProvider provider, child) {
          return AppBarWhite(
            title: "",
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 25.h as double),
                      child: Text(
                        widget.title!,
                        style: primaryTextStyle(size: 20, color: appTextColorPrimary),
                      ),
                    ),
                    Offstage(
                      offstage:
                          !(widget.pinType == PinType.EnterNewCardPin || widget.pinType == PinType.ConfirmNewCardPin),
                      child: Container(
                        margin: EdgeInsets.only(top: 4.h as double),
                        child: Text(
                          "pin_notice_atm".tr(),
                          style: TextStyle(fontSize: 14, color: appTextColorGray),
                        ),
                      ),
                    ),
                    buildPassword(),
                    Container(
                      margin: EdgeInsets.only(top: 60.h as double, left: 30.w as double, right: 30.w as double),
                      child: getKeyBordWidget(),
                    ),
                    Offstage(
                      offstage: widget.pinType == PinType.CreateWalletPin ||
                          widget.pinType == PinType.ConfirmWalletPin ||
                          widget.pinType == PinType.EnterNewCardPin ||
                          widget.pinType == PinType.ConfirmNewCardPin ||
                          widget.pinType == PinType.ForgetPin,
                      child: InkWell(
                        child: Container(
                          margin: EdgeInsets.only(top: 20.h as double),
                          child: Text(
                            'pin_forgot'.tr().toString(),
                            style: primaryTextStyle(color: numberKeyBoardForget, size: 14),
                          ),
                        ),
                        onTap: () {
                          provider.forgetPassport();
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildPassword() {
    return Container(
      width: double.infinity,
      alignment: Alignment.center,
      margin: EdgeInsets.only(top: 15.h as double),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: getSubPinWidget(),
      ),
    );
  }

  List<Widget> getSubPinWidget() {
    List<Widget> subs = [];
    for (int index = 0; index < 6; index++) {
      if (index < provider.inputPin.length) {
        subs.add(buildPwdItem(true));
      } else {
        subs.add(buildPwdItem(false));
      }
    }
    return subs;
  }

  Widget buildPwdItem(bool selected) {
    return Container(
        width: 8,
        height: 8,
        margin: EdgeInsets.only(left: 3.w as double, right: 3.w as double),
        decoration: BoxDecoration(
            color: appTextColorPrimary.withOpacity(selected ? 1 : 0),
            border: Border.all(width: 1, color: appTextColorPrimary),
            borderRadius: BorderRadius.circular(5)));
  }

  Widget getKeyBordWidget() {
    return Container(
        width: double.infinity,
        child: GridView(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3, mainAxisSpacing: 20.0, crossAxisSpacing: 20.0, childAspectRatio: 1),
            shrinkWrap: true,
            padding: const EdgeInsets.all(0),
            physics: new NeverScrollableScrollPhysics(),
            children: provider.keyBordList.map((e) {
              return buildKeyItem(e);
            }).toList()));
  }

  Widget buildKeyItem(String keyValue) {
    return GestureDetector(
      onTap: () {
        provider.inputPassword(keyValue);
      },
      child: ClipOval(
        child: Container(
          alignment: Alignment.center,
          decoration:
              BoxDecoration(color: (keyValue != 'delete' && keyValue != '') ? numberKeyBoardBack : Colors.transparent),
          child: getSubWidget(keyValue),
        ),
      ),
    );
  }

  Widget getSubWidget(String keyValue) {
    if (keyValue == '') {
      return Container();
    } else if (keyValue == 'delete') {
      return Icon(
        Icons.arrow_back,
        color: appTextColorPrimary,
        size: 30,
      );
    } else {
      return Text(keyValue, style: primaryTextStyle(color: appTextColorPrimary, size: 30));
    }
  }

  late InputPinProvider provider;

  @override
  InputPinProvider createProvider(BuildContext buildContext) {
    provider = InputPinProvider(buildContext, widget.pinType!, widget.title!, widget.lastPinCode,
        needBackPinCode: widget.needBackPinCode);
    return provider;
  }

  Widget successChild() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 5.h as double),
          child: Text(
            'yeah_string'.tr().toString(),
            style: primaryTextStyle(size: 20, color: appTextColorPrimary),
          ),
        ),
        Text(
          'pin_changed_success_notice'.tr().toString(),
          style: primaryTextStyle(size: 14, color: appTextColorGray),
        ),
      ],
    );
  }
}
