import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/visa/delivery_details.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_item.dart';

class CreateVisaCard extends StatefulWidget {
  @override
  _CreateVisaCardState createState() => _CreateVisaCardState();
}

class _CreateVisaCardState extends State<CreateVisaCard> {
  @override
  Widget build(BuildContext context) {
    return AppBarWhite(title: 'Apply Visa card', backgroundColor: Colors.white, child: configBody());
  }

  Widget configBody() {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.all(15),
          width: double.infinity,
          height: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Visa Prepaid Card',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Color(0xFF212121)),
              ),
              SizedBox(height: 8),
              Text(
                'Apply now to own this card. A Secure and Convenient way to make payment in all VISA enabled merchant.',
                style: TextStyle(fontSize: 12, color: Color(0xFF212121), height: 1.5),
              ),
              Container(
                margin: EdgeInsets.only(left: 5, right: 5, top: 25, bottom: 25),
                width: double.infinity,
                height: 190,
                color: Colors.blue,
              ),
              RichText(
                text: TextSpan(
                    text: 'By clicking Apply Now,  you agree to our ',
                    style: TextStyle(color: Color(0xff757575), fontSize: 12),
                    children: [
                      TextSpan(
                        text: 'Terms and Conditions',
                        style: TextStyle(fontSize: 12, color: Color(0xff3391ff)),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            print('Terms and Conditions');
                          },
                      ),
                    ]),
              )
            ],
          ),
        ),
        Positioned(
            bottom: 30,
            left: 30,
            right: 30,
            child: NormalClickItem(
              content: 'Apply Now',
              clickFunction: () {
                Navigator.push(context, new MaterialPageRoute(builder: (_) {
                  return DeliveryDetails();
                }));
              },
            ))
      ],
    );
  }
}
