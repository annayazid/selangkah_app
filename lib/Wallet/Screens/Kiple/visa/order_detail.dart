import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/visa/provider/order_detail_provider.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';

class OrderDetail extends StatefulWidget {
  @override
  _OrderDetailState createState() => _OrderDetailState();
}

class _OrderDetailState extends State<OrderDetail> with StateLifeCycle<OrderDetail, OrderDetailProvider> {
  OrderDetailProvider? detailProvider;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'Delivery Details',
      backgroundColor: Colors.white,
      child: ChangeNotifierProvider(
        create: (BuildContext ctx) {
          return detailProvider;
        },
        child: Consumer(
          builder: (ctx, OrderDetailProvider provider, child) {
            return configBody();
          },
        ),
      ),
    );
  }

  Widget configBody() {
    return Container();
  }

  @override
  OrderDetailProvider createProvider(BuildContext buildContext) {
    detailProvider = OrderDetailProvider(buildContext);
    return detailProvider!;
  }
}
