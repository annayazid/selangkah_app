import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/visa/provider/visa_prepaid_card_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class VisaPrepaidCard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return VisaPrepaidCardState();
  }
}

class VisaPrepaidCardState extends State<VisaPrepaidCard>
    with StateLifeCycle<VisaPrepaidCard, VisaPrepaidCardProvider> {
  double? picWith;
  double? picHeight;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    picWith = MediaQuery.of(context).size.width - (24.w as double);
    picHeight = (MediaQuery.of(context).size.width - (24.w as double)) * 154 / 244;
    return AppBarWhite(
      title: "visa_card_title".tr(),
      child: ChangeNotifierProvider<VisaPrepaidCardProvider>(
        create: (BuildContext buildContext) => provider,
        child: Consumer<VisaPrepaidCardProvider>(
          builder: (BuildContext? buildContext, VisaPrepaidCardProvider? withDrawalProvider, Widget? child) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(
                      left: 7.w as double, right: 7.w as double, top: 8.h as double, bottom: 2.h as double),
                  child: Text(
                    "balance_string".tr(),
                    style: boldTextStyle(color: appTextColorGray, size: 12),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: 7.w as double,
                    right: 7.w as double,
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        "${"rm_string".tr()} ",
                        style: boldTextStyle(
                            color: provider.cardActive ? appTextColorPrimary : appTextColorPrimary.withAlpha(50),
                            size: 14),
                      ),
                      Text(
                        "200.00",
                        style: boldTextStyle(
                            color: provider.cardActive ? appTextColorPrimary : appTextColorPrimary.withAlpha(50),
                            size: 20),
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      Offstage(
                        offstage: provider.cardActive,
                        child: InkWell(
                          child: Text(
                            "visa_activate_card".tr(),
                            style: primaryTextStyle(color: appColorPrimary, size: 16),
                          ),
                          onTap: () {
                            provider.activateCard();
                          },
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: picWith,
                  height: picHeight,
                  margin: EdgeInsets.only(
                      left: 12.w as double, right: 12.w as double, top: 12.h as double, bottom: 5.h as double),
                  child: Stack(
                    children: [
                      Image.asset(
                        'assets/images/Wallet/vaccine_tq_english.png',
                      ),
                      Offstage(
                        offstage: provider.cardActive,
                        child: Container(
                          width: picWith,
                          height: picHeight,
                          color: Colors.white.withAlpha(80),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(7.w as double),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "visa_freeze_card".tr(),
                        style: TextStyle(
                            fontSize: 16,
                            color: provider.cardActive ? appTextColorPrimary : appTextColorPrimary.withAlpha(50)),
                      ),
                      Expanded(child: Container()),
                      Transform.scale(
                        scale: 0.7,
                        child: CupertinoSwitch(
                          value: provider.switchTurn,
                          activeColor: appColorPrimary,
                          trackColor: Color(0xFFbdbdbd),
                          onChanged: (value) {
                            if (provider.cardActive) {
                              provider.freezeCard();
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 0.8.h as double,
                  color: appLineColor,
                ),
                InkWell(
                  child: Container(
                    padding: EdgeInsets.all(7.w as double),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "visa_request_replacement".tr(),
                          style: TextStyle(
                              fontSize: 16,
                              color: provider.cardActive ? appTextColorPrimary : appTextColorPrimary.withAlpha(50)),
                        ),
                      ],
                    ),
                  ),
                  onTap: () {},
                ),
                Container(
                  height: 0.8.h as double,
                  color: appLineColor,
                ),
                InkWell(
                  child: Container(
                    padding: EdgeInsets.all(7.w as double),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "visa_top_up_wallet".tr(),
                          style: TextStyle(
                              fontSize: 16,
                              color: provider.cardActive ? appTextColorPrimary : appTextColorPrimary.withAlpha(50)),
                        ),
                      ],
                    ),
                  ),
                  onTap: () {},
                ),
                Container(
                  height: 0.8.h as double,
                  color: appLineColor,
                ),
                InkWell(
                  child: Container(
                    padding: EdgeInsets.all(7.w as double),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "visa_set_change_pin".tr(),
                          style: TextStyle(
                              fontSize: 16,
                              color: provider.cardActive ? appTextColorPrimary : appTextColorPrimary.withAlpha(50)),
                        ),
                      ],
                    ),
                  ),
                  onTap: () {
                    provider.setOrChangeCardPin();
                  },
                ),
                Container(
                  height: 0.8.h as double,
                  color: appLineColor,
                ),
                Offstage(
                  offstage: provider.cardActive,
                  child: Container(
                    margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double, top: 4.h as double),
                    child: Text(
                      "visa_card_freeze_notice".tr(),
                      style: TextStyle(fontSize: 14, color: Color(0xFFff0000)),
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  late VisaPrepaidCardProvider provider;

  @override
  VisaPrepaidCardProvider createProvider(BuildContext buildContext) {
    provider = VisaPrepaidCardProvider(buildContext);
    return provider;
  }
}
