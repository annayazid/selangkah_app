import 'package:flutter/material.dart';

enum VisaItemType {
  VisaItemTypeInputShowMax,
  VisaItemTypeInputHiddenMax,
  VisaItemTypeInputMobile,
  VisaItemTypeSelectAddress
}

class VisaDetailItem extends StatelessWidget {
  final TextEditingController? controller;
  final VisaItemType itemType;
  final String hitText;
  final int? maxLength;
  final bool enable;
  final String title;
  final Function? selectFunction;

  VisaDetailItem(
      {this.controller,
      this.itemType = VisaItemType.VisaItemTypeInputShowMax,
      this.hitText = '',
      this.maxLength,
      this.enable = true,
      this.title = '',
      this.selectFunction});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold)),
          Container(
            margin: EdgeInsets.only(top: 8),
            child: configInput(),
          )
        ],
      ),
    );
  }

  Widget configInput() {
    if (itemType == VisaItemType.VisaItemTypeInputShowMax) {
      return TextField(
        controller: controller,
        style: TextStyle(color: Color(0xFF212121), fontSize: 12),
        maxLength: maxLength,
        enabled: enable,
        decoration: InputDecoration(
            fillColor: Color(0xFFf4f4f4),
            filled: true,
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(3), borderSide: BorderSide.none),
            isCollapsed: true,
            hintText: hitText,
            hintStyle: TextStyle(color: Color(0xFF757575), fontSize: 12),
            contentPadding: EdgeInsets.all(10)),
      );
    } else if (itemType == VisaItemType.VisaItemTypeInputHiddenMax) {
      return TextField(
        controller: controller,
        enabled: enable,
        style: TextStyle(color: Color(0xFF212121), fontSize: 12),
        decoration: InputDecoration(
            fillColor: Color(0xFFf4f4f4),
            filled: true,
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(3), borderSide: BorderSide.none),
            isCollapsed: true,
            hintText: hitText,
            hintStyle: TextStyle(color: Color(0xFF757575), fontSize: 12),
            contentPadding: EdgeInsets.all(10)),
      );
    } else if (itemType == VisaItemType.VisaItemTypeInputMobile) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            margin: EdgeInsets.only(right: 10),
            child: Text('+60', style: TextStyle(color: Color(0xFF212121), fontSize: 12)),
          ),
          Expanded(
            child: TextField(
              controller: controller,
              enabled: enable,
              style: TextStyle(color: Color(0xFF212121), fontSize: 12),
              decoration: InputDecoration(
                  fillColor: Color(0xFFf4f4f4),
                  filled: true,
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(3), borderSide: BorderSide.none),
                  isCollapsed: true,
                  hintText: hitText,
                  hintStyle: TextStyle(color: Color(0xFF757575), fontSize: 12),
                  contentPadding: EdgeInsets.all(10)),
            ),
          )
        ],
      );
    } else {
      return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          if (selectFunction != null) selectFunction!();
        },
        child: Stack(
          alignment: Alignment.centerRight,
          children: [
            TextField(
              controller: controller,
              enabled: false,
              style: TextStyle(color: Color(0xFF212121), fontSize: 12),
              decoration: InputDecoration(
                  fillColor: Color(0xFFf4f4f4),
                  filled: true,
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(3), borderSide: BorderSide.none),
                  isCollapsed: true,
                  hintText: hitText,
                  hintStyle: TextStyle(color: Color(0xFF757575), fontSize: 12),
                  contentPadding: EdgeInsets.all(10)),
            ),
            Container(
              margin: EdgeInsets.only(right: 10),
              child: Image.asset(
                "assets/images/Wallet/icon_arrow_bottom.png",
                width: 20,
                height: 20,
              ),
            )
          ],
        ),
      );
    }
  }
}
