import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_button.dart';
import 'package:selangkah_new/Wallet/components/pin_code_input_textfield.dart';
import 'package:selangkah_new/utils/AppColors.dart';

import 'provider/activate_visa_card_provider.dart';

class ActivateVisaCard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ActivateVisaCardState();
  }
}

class ActivateVisaCardState extends State<ActivateVisaCard>
    with StateLifeCycle<ActivateVisaCard, ActivateVisaCardProvider> {
  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: "Activate Visa card",
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 7.h as double, left: 7.w as double, right: 7.w as double),
            child: Text(
              "Enter Card Number",
              style: TextStyle(fontSize: 16, color: appTextColorPrimary),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 4.h as double, left: 7.w as double, right: 7.w as double),
            child: Text(
              "To activate your card, kindly enter last 4-digit card number.",
              style: TextStyle(fontSize: 14, color: appTextColorGray),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 40.w as double, right: 40.w as double),
            height: 100,
            child: PinCodeInputTextField(
              codeLength: 4,
              controller: provider.cardNumberController,
              onTextChange: (text) {
                if (text.length == 4) {
                  provider.checkCardNumber(text);
                }
              },
              obscureText: true,
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double),
            child: Text(
              "You have exceeded maximum attempt for incorrect card number. Please contact our Customer Care at +603 8605 3357 for assistance.",
              style: TextStyle(fontSize: 14, color: Color(0xffff0000)),
            ),
          ),
          Expanded(child: Container()),
          Container(
            margin: EdgeInsets.only(left: 14.w as double, right: 14.w as double, bottom: 20.h as double),
            child: ClickButton(
              content: "Activate",
              canClick: true,
              clickFunction: () {},
            ),
          )
        ],
      ),
    );
  }

  late ActivateVisaCardProvider provider;

  @override
  ActivateVisaCardProvider createProvider(BuildContext buildContext) {
    provider = ActivateVisaCardProvider(buildContext);
    return provider;
  }
}
