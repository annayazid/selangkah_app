import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/ekyc_bottom_sheet.dart';
import 'package:selangkah_new/Wallet/models/occupation_list_model_entity.dart';
import 'package:selangkah_new/Wallet/models/select_item.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class DeliveryDetailsProvider extends BaseProvider {
  DeliveryDetailsProvider(BuildContext buildContext) : super(buildContext);

  TextEditingController address = TextEditingController();
  TextEditingController country = TextEditingController(text: 'Malaysia');
  TextEditingController state = TextEditingController();
  TextEditingController city = TextEditingController();
  TextEditingController postCode = TextEditingController();
  TextEditingController mobile = TextEditingController();
  bool nextEnable = true;
  List<SelectItem> statesData = [];

  requestData() {
    requestNetwork<OccupationListModelEntity>(Method.get,
        showDialog: true, url: HttpApi.KipleBaseUrl + HttpApi.OccupationList, onSuccess: (data) {
      if (data.states != null) {
        map2List(data.states!, statesData);
      }
    }, onError: (code, msg) {
      showMessage(msg);
    });
  }

  map2List(Map map, List<SelectItem> list, {bool needSort = false}) {
    list.clear();
    map.forEach((key, value) {
      list.add(SelectItem(key, value));
    });
    if (needSort) {
      list.sort((a, b) => a.value.compareTo(b.value));
    }
  }

  addControllerListen() {
    address.addListener(listenAction);
    state.addListener(listenAction);
    city.addListener(listenAction);
    postCode.addListener(listenAction);
    mobile.addListener(listenAction);
  }

  listenAction() {
    if (address.text.trim().length > 0 &&
        country.text.trim().length > 0 &&
        state.text.trim().length > 0 &&
        city.text.trim().length > 0 &&
        postCode.text.trim().length > 0 &&
        mobile.text.trim().length > 0) {
      nextEnable = true;
    } else {
      nextEnable = false;
    }
    notifyListeners();
  }

  selectState() {
    showPub('State', statesData, (SelectItem selectItem) {
      state.text = selectItem.value;
      notifyListeners();
    });
  }

  nextAction() {
    print('nextAction');
  }

  showPub(String title, List<dynamic> data, Function itemClick) {
    FocusScope.of(currentContext).requestFocus(FocusNode());
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        isDismissible: false,
        context: currentContext,
        builder: (BuildContext context) {
          return EkycBottomSheet(title, data, itemClick);
        });
  }
}
