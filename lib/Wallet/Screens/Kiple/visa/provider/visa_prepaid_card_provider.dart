import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/pin/input_pin.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/visa/activate_visa_card.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';

class VisaPrepaidCardProvider extends BaseProvider {
  VisaPrepaidCardProvider(BuildContext buildContext) : super(buildContext);

  bool _switchTurn = false;
  bool _cardActive = true;

  bool get switchTurn => _switchTurn;

  set switchTurn(bool value) {
    _switchTurn = value;
    notifyListeners();
  }

  bool get cardActive => _cardActive;

  set cardActive(bool value) {
    _cardActive = value;
  }

  activateCard() async {
    ResultModel resultModel = await Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
      return InputPinPage.VerifyPin();
    }));
    if (resultModel != null && resultModel.success!) {
      ResultModel acticateResult = await Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
        return ActivateVisaCard();
      }));
      if (acticateResult != null && acticateResult.success!) {
        notifyListeners();
      }
    }
  }

  freezeCard() async {
    if (!switchTurn) {
      ResultModel resultModel = await Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
        return InputPinPage.VerifyPin();
      }));
      if (resultModel != null && resultModel.success!) {
        switchTurn = true;
      }
    } else {
      ResultModel resultModel = await Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
        return InputPinPage.VerifyPin();
      }));
      if (resultModel != null && resultModel.success!) {
        ResultModel cardResult = await Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
          return InputPinPage.VerifyCardPin(
            needBackPinCode: true,
          );
        }));
        if (cardResult != null && cardResult.success!) {
          switchTurn = false;
        }
      }
    }
  }

  setOrChangeCardPin() async {
    ResultModel resultModel = await Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
      return InputPinPage.ResetPrepaidCardPin();
    }));
    if (resultModel != null && resultModel.success!) {
      showMessage("visa_card_pin_success_notice".tr());
    }
  }
}
