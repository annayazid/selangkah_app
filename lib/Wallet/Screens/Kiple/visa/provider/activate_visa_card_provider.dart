import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';

class ActivateVisaCardProvider extends BaseProvider {
  ActivateVisaCardProvider(BuildContext buildContext) : super(buildContext);

  TextEditingController cardNumberController = new TextEditingController();

  checkCardNumber(String cardNumber) {
    FocusScope.of(currentContext).requestFocus(FocusNode());
    Navigator.of(currentContext).pop(ResultModel(success: true));
  }
}
