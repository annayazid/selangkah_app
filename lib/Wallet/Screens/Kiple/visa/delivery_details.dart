import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/visa/components/visa_detail_item.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/visa/provider/delivery_details_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_item.dart';

class DeliveryDetails extends StatefulWidget {
  @override
  _DeliveryDetailsState createState() => _DeliveryDetailsState();
}

class _DeliveryDetailsState extends State<DeliveryDetails>
    with StateLifeCycle<DeliveryDetails, DeliveryDetailsProvider> {
  DeliveryDetailsProvider? detailsProvider;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);
    return AppBarWhite(
      title: 'Delivery Details',
      backgroundColor: Colors.white,
      child: ChangeNotifierProvider(
        create: (BuildContext ctx) {
          return detailsProvider;
        },
        child: Consumer(
          builder: (ctx, DeliveryDetailsProvider provider, child) {
            return configBody();
          },
        ),
      ),
    );
  }

  Widget configBody() {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.all(15),
            width: double.infinity,
            height: double.infinity,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Confirm Delivery Address',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Color(0xFF212121)),
                  ),
                  SizedBox(height: 8),
                  Text(
                    'Please enter your delivery address details.',
                    style: TextStyle(fontSize: 12, color: Color(0xFF212121), height: 1.5),
                  ),
                  SizedBox(height: 5),
                  configContent()
                ],
              ),
            ),
          ),
          Positioned(
              bottom: 30,
              left: 30,
              right: 30,
              child: NormalClickItem(
                content: 'Next',
                enable: detailsProvider!.nextEnable,
                clickFunction: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  detailsProvider?.nextAction();
                },
              ))
        ],
      ),
    );
  }

  Widget configContent() {
    return Container(
      margin: EdgeInsets.only(bottom: 100),
      child: Column(
        children: [
          VisaDetailItem(
            title: 'Address',
            hitText: 'Please enter address',
            controller: detailsProvider?.address,
            maxLength: 50,
            itemType: VisaItemType.VisaItemTypeInputShowMax,
          ),
          VisaDetailItem(
            title: 'Country',
            hitText: 'Country',
            controller: detailsProvider?.country,
            itemType: VisaItemType.VisaItemTypeInputHiddenMax,
            enable: false,
          ),
          VisaDetailItem(
            title: 'State',
            hitText: 'Please enter select state',
            controller: detailsProvider?.state,
            itemType: VisaItemType.VisaItemTypeSelectAddress,
            selectFunction: () {
              detailsProvider?.selectState();
            },
          ),
          VisaDetailItem(
            title: 'City',
            hitText: 'Please enter city',
            controller: detailsProvider?.city,
            maxLength: 50,
            itemType: VisaItemType.VisaItemTypeInputShowMax,
          ),
          VisaDetailItem(
            title: 'PostCode',
            hitText: 'Please enter postcode',
            controller: detailsProvider?.postCode,
            maxLength: 50,
            itemType: VisaItemType.VisaItemTypeInputShowMax,
          ),
          VisaDetailItem(
            title: 'Mobile Number',
            hitText: 'Please enter mobile number',
            controller: detailsProvider?.mobile,
            itemType: VisaItemType.VisaItemTypeInputMobile,
          ),
        ],
      ),
    );
  }

  @override
  DeliveryDetailsProvider createProvider(BuildContext buildContext) {
    detailsProvider = DeliveryDetailsProvider(buildContext);
    detailsProvider?.initState();
    detailsProvider?.requestData();
    return detailsProvider!;
  }
}
