import 'dart:convert';

JsBackModel jsBackModelFromJson(String str) => JsBackModel.fromJson(json.decode(str));

String jsBackModelToJson(JsBackModel data) => json.encode(data.toJson());

class JsBackModel {
  JsBackModel({
    required this.type,
    this.success,
    this.data,
    this.methodName,
    this.errorMsg,
    this.changeData,
  });

  String type;
  String? success;
  dynamic data;
  String? methodName;
  String? errorMsg;
  Function? changeData;

  factory JsBackModel.fromJson(Map<String, dynamic> json) => JsBackModel(
        type: json["type"],
        success: json["success"],
        data: json["data"],
        methodName: json["methodName"],
        errorMsg: json["errorMsg"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "success": success,
        "data": changeData == null ? data : changeData?.call(data),
        "methodName": methodName,
        "errorMsg": errorMsg,
      };
}
