import 'dart:convert';

JsOrder jsOrderFromJson(String str) => JsOrder.fromJson(json.decode(str));

String jsOrderToJson(JsOrder data) => json.encode(data.toJson());

class JsOrder {
  JsOrder({
    required this.type,
    this.nativeMethodName,
    this.needCallBack,
    this.params,
  });

  String type;
  String? nativeMethodName;
  bool? needCallBack;
  dynamic params;

  factory JsOrder.fromJson(Map<String, dynamic> json) => JsOrder(
        type: json["type"],
        nativeMethodName: json["nativeMethodName"],
        needCallBack: json["needCallBack"],
        params: json["params"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "nativeMethodName": nativeMethodName,
        "needCallBack": needCallBack,
        "params": params,
      };
}
