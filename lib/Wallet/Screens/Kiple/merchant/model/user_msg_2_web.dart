import 'dart:convert';

UserMsgWeb userMsgWebFromJson(String str) => UserMsgWeb.fromJson(json.decode(str));

String userMsgWebToJson(UserMsgWeb data) => json.encode(data.toJson());

class UserMsgWeb {
  UserMsgWeb({
    this.expires,
    this.accessToken,
    this.account,
  });

  String? expires;
  String? accessToken;
  AccountWeb? account;

  factory UserMsgWeb.fromJson(Map<String, dynamic> json) => UserMsgWeb(
        expires: json["Expires"],
        accessToken: json["AccessToken"],
        account: AccountWeb.fromJson(json["Account"]),
      );

  Map<String, dynamic> toJson() => {
        "Expires": expires,
        "AccessToken": accessToken,
        "Account": account?.toJson(),
      };
}

class AccountWeb {
  AccountWeb({this.userName, this.type, this.id, this.channel});

  String? userName;
  String? type;
  String? id;
  String? channel;

  factory AccountWeb.fromJson(Map<String, dynamic> json) => AccountWeb(
        userName: json["UserName"],
        type: json["Type"],
        id: json["Id"],
        channel: json["channel"],
      );

  Map<String, dynamic> toJson() => {
        "UserName": userName,
        "Type": type,
        "Id": id,
        "channel": channel,
      };
}
