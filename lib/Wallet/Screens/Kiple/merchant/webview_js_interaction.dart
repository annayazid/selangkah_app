import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/merchant/provider/base_js_provider.dart';
import 'package:selangkah_new/Wallet/Utils/log_utils.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewJsInteraction extends StatefulWidget {
  final BaseJsProvider provider;

  WebViewJsInteraction(this.provider);

  @override
  State<StatefulWidget> createState() {
    return WebViewJsInteractionState();
  }
}

class WebViewJsInteractionState extends State<WebViewJsInteraction>
    with StateLifeCycle<WebViewJsInteraction, BaseJsProvider> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (BuildContext context) => widget.provider,
      child: Consumer<BaseJsProvider>(
        builder: (context, value, child) {
          return AppBarWhite(
            title: widget.provider.webTitle == null ? '' : widget.provider.webTitle,
            child: WebView(
              debuggingEnabled: true,
              initialUrl: widget.provider.loadUrl,
              userAgent: "kipleFlutter",
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController controller) {
                widget.provider.webViewController = controller;
              },
              // ignore: prefer_collection_literals
              javascriptChannels: <JavascriptChannel>[
                _JavascriptChannel(context),
              ].toSet(),
              onPageStarted: (String url) async {
                value.pageLoading = true;
                print('Page started loading: $url');
              },
              onPageFinished: (String url) async {
                value.pageLoading = false;
                print('Page finished loading: $url');
                value.webTitle = (await widget.provider.webViewController.getTitle())!;
                widget.provider.webViewController.canGoBack().then((value) {
                  widget.provider.showBackBotton = value;
                });
              },
              gestureNavigationEnabled: true,
            ),
          );
        },
      ),
    );
  }

  @override
  BaseJsProvider createProvider(BuildContext buildContext) {
    return widget.provider;
  }

  Future<bool> _onBack() {
    widget.provider.webViewController.canGoBack().then((value) {
      if (value) {
        widget.provider.webViewController.goBack();
      } else {
        Navigator.of(context).pop();
      }
    });
    return Future.value(false);
  }

  JavascriptChannel _JavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'flutter',
        onMessageReceived: (JavascriptMessage message) async {
          // ignore: deprecated_member_use
          Log.d("=========jsorder===${message.message}====");
          if (message.message.contains("callResult")) {
            widget.provider.dealBack(message.message);
          } else if (message.message.contains("callMethod")) {
            widget.provider.dealOrder(message.message);
          } else {
            Log.d("=========no find order type=======");
            widget.provider.backJs(widget.provider.orderCallResult, "false", "", "", "NoFindOrderType");
          }
        });
  }
}
