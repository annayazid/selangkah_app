import 'dart:async';

import 'package:flutter_native_contact_picker/flutter_native_contact_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:selangkah_new/Wallet/Utils/log_utils.dart';

import 'base_js_provider.dart';

class MobileReloadProvider extends BaseJsProvider {
  MobileReloadProvider(String loadUrl) : super(loadUrl) {
    orderMap['selectPhoneNum'] = [selectPhoneNum];
    orderMap['getToken'] = [getToken];
    orderMap['goPay'] = [goPay];
  }

  Future<String?> selectPhoneNum() async {
    final status = await Permission.contacts.request();
    if (status.isGranted) {
      final FlutterContactPicker? _contactPicker = new FlutterContactPicker();
      Contact? contact = await _contactPicker!.selectContact();
      String phoneNumber = "";
      if (contact != null) {
        print("======== Phone = ${contact.phoneNumbers![0]} =");
        if (contact.phoneNumbers![0].contains(" ")) {
          phoneNumber = contact.phoneNumbers![0].replaceAll(" ", "");
        } else {
          phoneNumber = contact.phoneNumbers![0];
        }
      }
      return phoneNumber;
    } else {
      Log.d("=$status==");
    }
  }

  String getToken() {
    return "";
  }

  goPay(Map paramsBody) {
    return true;
  }

  String map2String(Map map) {
    String content = "{";
    map.forEach((key, value) {
      if ((value != null && '' != value)) {
        content = content + key;
        content = content + ":";
        content = content + ((value == null || '' == value) ? " " : value);
        content = content + ",";
      }
    });
    content = content.substring(0, content.length - 1) + "}";
    return content;
  }
}
