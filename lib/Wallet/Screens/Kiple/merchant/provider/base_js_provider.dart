import 'dart:async';
import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/merchant/model/js_back_model.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/merchant/model/js_order_model.dart';
import 'package:selangkah_new/Wallet/Utils/log_utils.dart';
import 'package:webview_flutter/webview_flutter.dart';

class BaseJsProvider extends BaseProvider {
  Completer<WebViewController> controller = Completer<WebViewController>();
  bool pageLoading = true;
  bool _showBackBotton = false;
  WebViewController? _webViewController;

  String? _webTitle;
  String? loadUrl;

  BuildContext? _buildContext;

  bool get showBackBotton => _showBackBotton;

  set showBackBotton(bool value) {
    _showBackBotton = value;
    notifyListeners();
  }

  BuildContext get buildContext => _buildContext!;

  set buildContext(BuildContext value) {
    _buildContext = value;
  }

  var orderMap = HashMap<String, List<Function>>();

  var jumpOrderMap = HashMap<String, Function>();

  BaseJsProvider(this.loadUrl) : super(null) {
    orderMap['closeWebView'] = [closeWebView];
    orderMap['changeTitle'] = [changeTitle];
    orderMap['jumpPage'] = [jumpPage];
  }

  WebViewController get webViewController => _webViewController!;

  set webViewController(WebViewController value) {
    _webViewController = value;
  }

  set webTitle(String value) {
    _webTitle = value;
    notifyListeners();
  }

  String get webTitle => _webTitle!;

  String getToken() {
    return "";
  }

  String orderCallMethod = "callMethod";
  String orderCallResult = "callResult";

  backJs(String orderType, String success, dynamic result, String methodName, String errorMsg,
      {Function? dataJsonFunction}) {
    JsBackModel jsBackModle = JsBackModel(
        success: success,
        type: orderType,
        data: result,
        methodName: methodName,
        errorMsg: errorMsg,
        changeData: dataJsonFunction);
    String backResponse = jsBackModelToJson(jsBackModle);
    backResponse = backResponse.replaceAll("\"", "'");

    if ("{" == backResponse.substring(backResponse.indexOf("data") + 7, backResponse.indexOf("data") + 8)) {
      String start = backResponse.substring(backResponse.indexOf("data") + 6, backResponse.indexOf("data") + 7);
      String end = backResponse.substring(backResponse.indexOf("}}") + 2, backResponse.indexOf("}}") + 3);
      if ("'" == start && "'" == end) {
        String first = backResponse.substring(0, backResponse.indexOf("data") + 6);
        String second = backResponse.substring(backResponse.indexOf("data") + 7, backResponse.indexOf("}}") + 2);
        String third = backResponse.substring(backResponse.indexOf("}}") + 3, backResponse.length);
        backResponse = first + second + third;
      }
    }
    Log.d("=========== backResponse ==== $backResponse ======");
    webViewController.evaluateJavascript('window._sdk.backResult("$backResponse")');
  }

  callMethod(String orderType, String nativeMethodName, bool needCallBack, Map params) {
    JsOrder jsOrder =
        JsOrder(nativeMethodName: nativeMethodName, type: orderType, needCallBack: needCallBack, params: params);
    String methodResponse = jsOrderToJson(jsOrder);
    methodResponse = methodResponse.replaceAll("\"", "'");

    Log.d("=========== methodResponse ==== $methodResponse ======");
    webViewController.evaluateJavascript('window._sdk.callMethod("$methodResponse")');
  }

  dealOrder(String message) async {
    try {
      JsOrder? jsOrder = jsOrderFromJson(message);
      if (jsOrder != null) {
        if (orderMap.containsKey(jsOrder.nativeMethodName)) {
          List functions = orderMap[jsOrder.nativeMethodName]!;
          if (functions.isNotEmpty && functions.length >= 1) {
            var result;
            if (jsOrder.params != null && jsOrder.params.isNotEmpty)
              result = await functions[0].call(jsOrder.params);
            else
              result = await functions[0].call();
            if (jsOrder.needCallBack != null && jsOrder.needCallBack!) {
              Log.d("===== method result == $result ====");
              backJs(orderCallResult, "true", result, jsOrder.nativeMethodName!, "",
                  dataJsonFunction: functions.length > 1 ? functions[1] : null);
            }
          }
        } else {
          backJs(orderCallResult, "false", "", jsOrder.nativeMethodName!, "NoSuchMethod");
        }
      }
    } catch (ex) {
      backJs(orderCallResult, "false", "", "", "JsonDataError");
    }
  }

  Future<String> dealBack(String message) async {
    return message;
  }

  closeWebView() {
    Navigator.pop(currentContext);
    return true;
  }

  changeTitle(Map paramsBody) {
    if (paramsBody['title'] != null) {
      webTitle = paramsBody['title'];
    }
  }

  jumpPage(dynamic paramsBody) {
    if (paramsBody is Map) {
      if (paramsBody['pageName'] != null) {
        if (jumpOrderMap.containsKey(paramsBody['pageName'])) {
          Function function = jumpOrderMap[paramsBody['pageName']]!;
          var result;
          if (paramsBody != null && paramsBody.isNotEmpty)
            result = function.call(paramsBody);
          else
            result = function.call();
          return result;
        }
      }
    } else if (paramsBody is String) {
      if (jumpOrderMap.containsKey(paramsBody)) {
        Function function = jumpOrderMap[paramsBody]!;
        var result;
        result = function.call();
        return result;
      }
    }

    return false;
  }
}
