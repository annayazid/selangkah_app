import 'dart:async';

import 'package:selangkah_new/Wallet/Screens/Kiple/merchant/model/user_msg_2_web.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

import 'base_js_provider.dart';

class JsNeedProvider extends BaseJsProvider {
  JsNeedProvider(String loadUrl) : super(loadUrl) {
    orderMap['getUserMsg'] = [getUserMsg, userMsgWebToJson];
    orderMap['offstageBottomBar'] = [offstageBottomBar];
    orderMap['getSelangkahId'] = [getSelangkahId];
    jumpOrderMap['topup'] = jump2TopUp;
    jumpOrderMap['resetPin'] = jump2ResetPin;
  }

  Future<UserMsgWeb> getUserMsg() async {
    AccountWeb accountWeb = AccountWeb(
        userName: await SecureStorage().readSecureData("userName"),
        id: await SecureStorage().readSecureData("userId"),
        channel: "selangkah");
    UserMsgWeb userMsgWeb =
        UserMsgWeb(account: accountWeb, accessToken: getToken());
    return userMsgWeb;
  }

  Future<String>? getSelangkahId() {}

  bool offstageBottomBar(Map paramsBody) {
    return false;
  }

  jump2TopUp() {}

  jump2ResetPin() {}
}
