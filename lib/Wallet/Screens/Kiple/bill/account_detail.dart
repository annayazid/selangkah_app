import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/bill/provider/account_detail_provider.dart';
import 'package:selangkah_new/Wallet/Utils/precision_limit_formatter.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_item.dart';
import 'package:selangkah_new/Wallet/models/operator_model_entity.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class AccountDetail extends StatefulWidget {
  final OperatorModelEntity? model;
  final bool fromMyBillers;

  AccountDetail({this.model, this.fromMyBillers = false});

  @override
  _AccountDetailState createState() => _AccountDetailState();
}

class _AccountDetailState extends State<AccountDetail> with StateLifeCycle<AccountDetail, AccountDetailProvider> {
  late AccountDetailProvider detailProvider;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'bill_detail_title'.tr(),
      backgroundColor: Colors.white,
      child: ChangeNotifierProvider(
        create: (_) {
          return detailProvider;
        },
        child: Consumer(
          builder: (ctx, AccountDetailProvider provider, child) {
            return configBody();
          },
        ),
      ),
    );
  }

  Widget configBody() {
    return Stack(
      children: [
        GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Container(
            width: double.infinity,
            height: double.infinity,
            child: SingleChildScrollView(
              child: Column(
                children: [configBaseInfo(), grayView(), configInputAmount(), configInputAmountNo(), configSave()],
              ),
            ),
          ),
        ),
        Positioned(
          child: NormalClickItem(
              content: 'ekyc_next'.tr(),
              enable: detailProvider.enable,
              clickFunction: () {
                detailProvider.nextAction();
              }),
          left: 30,
          right: 30,
          bottom: 30,
        )
      ],
    );
  }

  Widget grayView() {
    return Container(height: 5, width: double.infinity, color: Color(0xFFf5f5f5));
  }

  Widget configBaseInfo() {
    return Container(
      color: Colors.white,
      width: double.infinity,
      padding: EdgeInsets.all(15),
      child: Row(
        children: [
          Container(
            // margin: EdgeInsets.only(left: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${widget.model?.providerName}', style: TextStyle(color: Color(0xFF212121), fontSize: 16)),
                SizedBox(height: 6),
                Text(
                    '${"bill_select_code".tr()}: ${detailProvider.productModelList.length > 0 ? detailProvider.productModelList[0].productSubCode : ""}',
                    style: TextStyle(color: Color(0xFF757575), fontSize: 14)),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget configTitleView(String title) {
    return Text(title, style: TextStyle(color: Color(0xFF212121), fontSize: 14));
  }

  Widget configInputAmount() {
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          configTitleView('withdrawal_amount'.tr()),
          Container(
            margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(color: Color(0xFFf4f4f4), borderRadius: BorderRadius.circular(3)),
            child: TextField(
              textAlign: TextAlign.start,
              controller: TextEditingController.fromValue(TextEditingValue(
                  text: '${detailProvider.amount}',
                  selection: TextSelection.fromPosition(
                      TextPosition(affinity: TextAffinity.downstream, offset: '${detailProvider.amount}'.length)))),
              // keyboardType: TextInputType.number,
              inputFormatters: [
                PrecisionLimitFormatter(3),
                FilteringTextInputFormatter(RegExp("[0-9.]"), allow: true),
              ],
              style: TextStyle(color: Color(0xFF212121), fontSize: 12),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  isCollapsed: true,
                  contentPadding: EdgeInsets.only(top: 10, bottom: 10),
                  hintText: 'RM 0.00',
                  hintStyle: TextStyle(color: Color(0xFFbdbdbd), fontSize: 12)),
              onChanged: (text) {
                detailProvider.changeShowText(text);
              },
            ),
          )
        ],
      ),
    );
  }

  Widget configInputAmountNo() {
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          configTitleView('bill_select_account'.tr()),
          Container(
            margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(color: Color(0xFFf4f4f4), borderRadius: BorderRadius.circular(3)),
            child: TextField(
              textAlign: TextAlign.start,
              controller: detailProvider.amountNo,
              // keyboardType: TextInputType.number,
              // inputFormatters: [PrecisionLimitFormatter(2)],
              style: TextStyle(color: Color(0xFF212121), fontSize: 12),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  isCollapsed: true,
                  contentPadding: EdgeInsets.only(top: 10, bottom: 10),
                  hintText: 'bill_select_account_place'.tr(),
                  hintStyle: TextStyle(color: Color(0xFFbdbdbd), fontSize: 12)),
            ),
          )
        ],
      ),
    );
  }

  Widget configSave() {
    if (widget.fromMyBillers) {
      return Container();
    }
    return Container(
      padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(right: 10, top: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  configTitleView('bill_select_account_save'.tr()),
                  SizedBox(height: 10),
                  Text('bill_select_account_content'.tr(), style: TextStyle(color: Color(0xFF757575), fontSize: 12))
                ],
              ),
            ),
          ),
          IconButton(
              icon: Icon(
                detailProvider.save ? Icons.check_box_sharp : Icons.check_box_outline_blank_sharp,
                color: appColorPrimaryKiple,
              ),
              onPressed: () {
                detailProvider.save = !detailProvider.save;
                setState(() {});
              })
        ],
      ),
    );
  }

  @override
  AccountDetailProvider createProvider(BuildContext buildContext) {
    detailProvider = AccountDetailProvider(buildContext);
    detailProvider.model = widget.model;
    detailProvider.initData();
    detailProvider.getProductList();
    return detailProvider;
  }
}
