import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/bill/bill_payment.dart';
import 'package:selangkah_new/Wallet/Utils/bill_tool.dart';
import 'package:selangkah_new/Wallet/models/operator_model_entity.dart';
import 'package:selangkah_new/Wallet/models/product_model_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class AccountDetailProvider extends BaseProvider {
  AccountDetailProvider(BuildContext buildContext) : super(buildContext);

  String amount = '';
  TextEditingController amountNo = TextEditingController();
  bool save = false;
  bool enable = false;
  OperatorModelEntity? model;
  List<ProductModelEntity> productModelList = [];

  initData() {
    amountNo.addListener(listenerEdit);
    if (model?.amount != null) {
      amount = '${model?.amount}';
    }
    if (model?.account != null) {
      amountNo.text = '${model?.account}';
    }
  }

  getProductList() {
    Map map = Map();
    map["provider_name"] = model?.providerName;
    map["product_type"] = model?.providerType;
    requestNetwork<List<ProductModelEntity>>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.MobileProductList, params: map, onSuccess: (data) {
      if (data != null) {
        productModelList.addAll(data);
        listenerEdit();
        notifyListeners();
      }
    }, onError: (code, msg) {
      showMessage(msg);
    });
  }

  listenerEdit() {
    if (amount.trim().length > 0 && amountNo.text.trim().length > 0 && productModelList.length > 0) {
      enable = true;
    } else {
      enable = false;
    }
    notifyListeners();
  }

  nextAction() {
    if (save) {
      saveMyBillers();
    }
    Future.delayed(Duration(microseconds: 0), () {
      Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
        return BillPayment(
          amount: amount.trim(),
          account: amountNo.text.trim(),
          productId: productModelList[0].id,
          productName: model?.providerName,
        );
      }));
    });
  }

  saveMyBillers() async {
    model?.account = amountNo.text.trim();
    model?.amount = amount.trim();
    if (model != null) BillTool.saveBill(model!);
  }

  changeShowText(String text) {
    if (text.length > 0) {
      if (text.length == 1) {
        amount = (double.parse(text) / 100).toStringAsFixed(2);
      } else {
        double newValue = double.parse(text);
        if (newValue == 0) {
          amount = "";
        } else {
          if (text.substring(text.lastIndexOf(".") + 1, text.length).length == 2 &&
              amount.substring(amount.lastIndexOf(".") + 1, amount.length).length == 1) {
            amount = text;
          } else if (!text.endsWith(".") &&
              text.contains(".") &&
              (text.substring(text.lastIndexOf(".") + 1, text.length).length >= 1 &&
                  amount.substring(amount.lastIndexOf(".") + 1, amount.length).length >= 1) &&
              (double.parse(text.substring(0, text.length - 1)) == double.parse(amount) ||
                  double.parse(text) == double.parse(amount.substring(0, amount.length - 1)))) {
            if (text.length > amount.length) {
              amount = (newValue * 10).toStringAsFixed(2);
            } else if (text.length < amount.length) {
              amount = (newValue / 10).toStringAsFixed(2);
            }
          } else {
            amount = text;
          }
        }
      }
    } else {
      amount = '';
    }
    listenerEdit();
  }
}
