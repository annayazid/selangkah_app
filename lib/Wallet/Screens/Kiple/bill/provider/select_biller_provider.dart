import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/models/operator_model_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class SelectBillerProvider extends BaseProvider {
  SelectBillerProvider(BuildContext buildContext) : super(buildContext);

  TextEditingController search = TextEditingController();

  List<OperatorModelEntity> operatorList = [];

  getOperatorList({key = ''}) {
    Map map = Map();
    map["provider_type"] = 1;
    map["provider_name"] = key;
    requestNetwork<List<OperatorModelEntity>>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.Operator_List, params: map, onSuccess: (data) {
      if (data != null) {
        operatorList = data;
        notifyListeners();
      }
    }, onError: (code, msg) {
      showMessage(msg);
    });
  }
}
