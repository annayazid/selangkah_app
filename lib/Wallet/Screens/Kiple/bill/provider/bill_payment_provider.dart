import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/HomePage/cubit/get_homepage_data/get_homepage_data_cubit.dart';
import 'package:selangkah_new/Screens/HomePage/screens/homepage_main.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/bill/components/bill_success.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/mobileReload/redeem_points_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/pin/input_pin.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/result_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/components/failed_content.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/components/talk_to_us.dart';
import 'package:selangkah_new/Wallet/models/Integral_item_model_entity.dart';
import 'package:selangkah_new/Wallet/models/paymethod_model.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/models/scan_payment_result_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class BillPaymentProvider extends BaseProvider {
  BillPaymentProvider(BuildContext buildContext) : super(buildContext);

  var productId;
  var account;
  var amount;
  var productName;
  IntegralItemModelEntity? integral;
  List<PayWayModel> pays = [];

  jumpToPoints() {
    Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
      return RedeemPointsPage();
    })).then((value) {
      if (value != null && value.success) {
        integral = value.data;
        notifyListeners();
      }
    });
  }

  String handleAmount() {
    if (integral == null) {
      return '${double.parse(amount).toStringAsFixed(2)}';
    } else {
      double oldAmount = double.parse('$amount');
      double integralAmount = double.parse('${integral?.amount}');
      if (oldAmount > integralAmount) {
        return '${(oldAmount - integralAmount).toStringAsFixed(2)}';
      } else {
        return '0.00';
      }
    }
  }

  commitData(String pinCode) {
    Map map = Map();
    map["pay_type"] = 2;
    map["pin_code"] = pinCode;
    map["product_id"] = productId;
    map["pay_account"] = account;
    map["amount"] = int.parse(MyTools.dealNumber(double.parse(amount) * 100.toInt(), pointLength: 0));
    if (integral != null) {
      map["integral"] = integral?.integral;
    }

    requestNetwork<ScanPaymentResultEntity>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.Commit_MobileReload, params: map, onSuccess: (data) {
      Future.delayed(Duration(seconds: 0), () {
        goToSuccessPage(data);
      });
    }, onError: (code, msg) {
      showMessage(msg);
      Future.delayed(Duration(seconds: 0), () {
        gotoFailedPage(msg);
      });
    });
  }

  payAction() {
    if (pays == null || pays.length == 0) {
      showMessage('scan_qr_pay_method_alert'.tr());
      return;
    }

    Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
      return InputPinPage.VerifyPin(needBackPinCode: true);
    })).then((value) {
      if (value != null) {
        ResultModel model = value;
        if (model.success != null && model.success!) {
          String code = model.data;
          commitData(code);
        } else {
          if (model.message != null) showMessage(model.message!);
        }
      }
    });
  }

  gotoFailedPage(message) {
    Widget content = FailedContent(
        title: 'withdrawal_fail_title'.tr(),
        content: message.isNotEmpty && message.length > 0 ? message : 'bill_payment_failed_content'.tr());

    Future.delayed(Duration(microseconds: 0), () {
      Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
        return ResultPage(
          contentChild: content,
          bottomChild: Talk2UsWidget(
            contentString: "mobile_help_content".tr(),
            showButton: true,
          ),
          status: false,
        );
      })).then((value) {
        Navigator.popUntil(currentContext, (route) => route.isFirst);
      });
    });
  }

  goToSuccessPage(ScanPaymentResultEntity data) {
    Widget resultWidget = BillSuccessContent(
      result: data,
      integral: '${integral != null ? integral?.integral : 0}',
      account: account,
      biller: productName,
    );

    Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
      return ResultPage(
        contentChild: resultWidget,
        bottomChild: Talk2UsWidget(
          contentString: "mobile_help_content".tr(),
          showButton: true,
        ),
        status: true,
      );
    })).then((value) {
      gotoHomePage();
    });
  }

  gotoHomePage() {
    BlocProvider(
      create: (context) => GetHomepageDataCubit(),
      child: HomePageScreen(),
    ).launch(
      currentContext,
      isNewTask: true,
    );
  }
}
