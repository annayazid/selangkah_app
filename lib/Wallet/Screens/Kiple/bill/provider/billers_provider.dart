import 'dart:async';
import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Utils/bill_tool.dart';
import 'package:selangkah_new/Wallet/components/pop_up_dialog.dart';
import 'package:selangkah_new/Wallet/models/bill_config_entity.dart';
import 'package:selangkah_new/Wallet/models/operator_model_entity.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/net/common.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class BillersProvider extends BaseProvider {
  BillersProvider(BuildContext buildContext) : super(buildContext);

  bool isEdit = false;
  List<OperatorModelEntity> operatorModelEntity = [];

  getTurnConfig() {
    Map map = Map();
    map["provider_type"] = 1;
    requestNetwork<BillConfigEntity>(Method.post, url: HttpApi.KipleBaseUrl + HttpApi.BillConfig, params: map,
        onSuccess: (data) {
      if (data != null && data.productSwitch == 1) {
        getListData();
      } else {
        Timer.periodic(Duration(milliseconds: 100), (timer) {
          timer.cancel();
          showNotDialog(
              'transfer_no_search_result_title'.tr(), 'no_mobile_function'.tr(), 'ekyc_hard_reject_button'.tr(),
              confirmClickFunction: () {
            Navigator.of(currentContext).pop(ResultModel(success: false));
          });
        });
      }
    }, onError: (code, msg) {
      Navigator.of(currentContext).pop(ResultModel(success: false));
      showMessage(msg);
    });
  }

  getListData() async {
    String jsonStr = await SecureStorage().readSecureData(Constant.MyBillsKey);
    var saveList = jsonDecode(jsonStr);
    operatorModelEntity = BillTool.modelHandle(saveList);
    notifyListeners();
  }

  deleteAccount(int id) {
    BillTool.deleteBillers(id);
    operatorModelEntity.removeWhere((element) {
      return element.id == id;
    });
    notifyListeners();
  }

  showNotDialog(String title, String content, String confirm, {Function? confirmClickFunction}) {
    showDialog(
        context: currentContext,
        builder: (_) {
          return PopUpDialog(
            title: title,
            content: content,
            confirm: confirm,
            confirmFunction: confirmClickFunction,
          );
        });
  }
}
