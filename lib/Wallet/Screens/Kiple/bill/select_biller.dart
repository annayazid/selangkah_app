import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/bill/components/billers_item.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/bill/provider/select_biller_provider.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';

class SelectBiller extends StatefulWidget {
  @override
  _SelectBillerState createState() => _SelectBillerState();
}

class _SelectBillerState extends State<SelectBiller> with StateLifeCycle<SelectBiller, SelectBillerProvider> {
  late SelectBillerProvider selectProvider;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'bill_my_billers_select'.tr(),
      backgroundColor: Colors.white,
      child: ChangeNotifierProvider(
        create: (_) {
          return selectProvider;
        },
        child: Consumer(
          builder: (ctx, SelectBillerProvider provider, child) {
            return configBody();
          },
        ),
      ),
    );
  }

  Widget configBody() {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Column(
        children: [configSearchWidget(), grayView(), configListWidget()],
      ),
    );
  }

  Widget grayView() {
    return Container(height: 5, width: double.infinity, color: Color(0xFFf5f5f5));
  }

  Widget configSearchWidget() {
    return Container(
      margin: EdgeInsets.all(15),
      decoration: BoxDecoration(color: Color(0xFFf4f4f4), borderRadius: BorderRadius.circular(3)),
      width: double.infinity,
      child: TextField(
        textAlignVertical: TextAlignVertical.center,
        textAlign: TextAlign.left,
        controller: selectProvider.search,
        style: TextStyle(color: Color(0xFF212121), fontSize: 12),
        textInputAction: TextInputAction.search,
        onSubmitted: (text) {
          selectProvider.getOperatorList(key: text);
        },
        onChanged: (text) {
          if (text.length == 0) {
            selectProvider.getOperatorList();
            FocusScope.of(context).requestFocus(FocusNode());
          }
        },
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.search, color: Color(0xFF757575)),
          border: InputBorder.none,
          isCollapsed: true,
          contentPadding: EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
          hintText: 'bill_select_search'.tr(),
          hintStyle: TextStyle(
            color: Color(0xFFbdbdbd),
            fontSize: 12,
          ),
        ),
      ),
    );
  }

  Widget configListWidget() {
    return Expanded(
        child: ListView.builder(
            itemBuilder: (BuildContext context, int index) {
              return BillersItem(fromSelect: true, isSelect: true, model: selectProvider.operatorList[index]);
            },
            padding: EdgeInsets.only(bottom: 80),
            itemCount: selectProvider.operatorList.length));
  }

  @override
  SelectBillerProvider createProvider(BuildContext buildContext) {
    selectProvider = SelectBillerProvider(buildContext);
    selectProvider.getOperatorList();
    return selectProvider;
  }
}
