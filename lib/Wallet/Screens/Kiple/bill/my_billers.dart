import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/bill/components/billers_item.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/bill/provider/billers_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/bill/select_biller.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/cards/pop_up_alert.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_item.dart';
import 'package:selangkah_new/Wallet/models/operator_model_entity.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class MyBillersPage extends StatefulWidget {
  @override
  _MyBillersPageState createState() => _MyBillersPageState();
}

class _MyBillersPageState extends State<MyBillersPage> with StateLifeCycle<MyBillersPage, BillersProvider> {
  late BillersProvider billersProvider;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'bill_my_billers_title'.tr(),
      backgroundColor: Colors.white,
      actions: configActions(),
      child: ChangeNotifierProvider(
        create: (_) {
          return billersProvider;
        },
        child: Consumer(
          builder: (ctx, BillersProvider provider, child) {
            return configBody();
          },
        ),
      ),
    );
  }

  List<Widget> configActions() {
    List<Widget> list = [];
    TextButton button = TextButton(
        onPressed: () {
          billersProvider.isEdit = !billersProvider.isEdit;
          setState(() {});
        },
        child: Text(!billersProvider.isEdit ? 'bill_my_billers_edit'.tr() : 'bill_my_billers_done'.tr(),
            style: TextStyle(color: appColorPrimaryKiple, fontSize: 16)));
    list.add(button);
    return list;
  }

  Widget configBody() {
    return Stack(
      children: [
        // ignore: null_aware_before_operator
        billersProvider.operatorModelEntity.length > 0 ? configListWidget() : configNoDataWidget(),
        Positioned(
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(30, 5, 30, 30),
              child: NormalClickItem(
                content: 'bill_my_billers_select'.tr(),
                clickFunction: () {
                  Navigator.push(context, new MaterialPageRoute(builder: (_) {
                    return SelectBiller();
                  }));
                },
              ),
            ),
            left: 0,
            right: 0,
            bottom: 0)
      ],
    );
  }

  Widget configNoDataWidget() {
    return Container(
      margin: EdgeInsets.only(bottom: 100),
      width: double.infinity,
      height: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('assets/images/Wallet/empty_illustration.png', width: 300, fit: BoxFit.fitWidth),
          Container(
            margin: EdgeInsets.fromLTRB(40, 40, 40, 0),
            child: Column(
              children: [
                Text('bill_my_billers_no_data_title'.tr(), style: TextStyle(color: Color(0xFF212121), fontSize: 16)),
                SizedBox(height: 20),
                Text('bill_my_billers_no_data_content'.tr(),
                    style: TextStyle(color: Color(0xFF757575), fontSize: 14), textAlign: TextAlign.center),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget configListWidget() {
    return Container(
        width: double.infinity,
        height: double.infinity,
        child: ListView.builder(
            itemBuilder: (BuildContext context, int index) {
              OperatorModelEntity model = billersProvider.operatorModelEntity[index];
              return BillersItem(
                  isEdit: billersProvider.isEdit,
                  model: model,
                  deleteAction: () {
                    deleteAccount(model.id);
                  });
            },
            padding: EdgeInsets.only(bottom: 80),
            itemCount: billersProvider.operatorModelEntity.length));
  }

  deleteAccount(id) {
    showDialog(
        context: context,
        builder: (_) {
          return PopUpAlert(
              title: 'bill_my_billers_delete_title'.tr(),
              content: 'bill_my_billers_delete_content'.tr(),
              confirm: 'bill_my_billers_delete_confirm'.tr(),
              isCancel: true,
              confirmFunction: () {
                billersProvider.deleteAccount(id);
              });
        });
  }

  @override
  BillersProvider createProvider(BuildContext buildContext) {
    billersProvider = BillersProvider(buildContext);
    billersProvider.getTurnConfig();
    return billersProvider;
  }
}
