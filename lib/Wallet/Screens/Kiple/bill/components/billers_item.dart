import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/bill/account_detail.dart';
import 'package:selangkah_new/Wallet/models/operator_model_entity.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class BillersItem extends StatelessWidget {
  final bool isEdit;
  final Function? deleteAction;
  final bool isSelect;
  final OperatorModelEntity? model;
  final bool fromSelect;

  BillersItem({this.isEdit = false, this.deleteAction, this.isSelect = false, this.fromSelect = false, this.model});

  @override
  Widget build(BuildContext context) {
    if (model == null) {
      return Container();
    }
    return configItem(context);
  }

  Widget configItem(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, new MaterialPageRoute(builder: (_) {
          return AccountDetail(model: model, fromMyBillers: !fromSelect);
        }));
      },
      child: Container(
        padding: EdgeInsets.all(15),
        width: double.infinity,
        margin: EdgeInsets.only(top: 1),
        decoration:
            BoxDecoration(color: Colors.white, border: Border(bottom: BorderSide(width: 1, color: Color(0xFFf5f5f5)))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('${model?.providerName}', style: TextStyle(color: Color(0xFF212121), fontSize: 16)),
                      SizedBox(height: fromSelect ? 0 : 8),
                      fromSelect
                          ? Container()
                          : Text('${isSelect ? 'Biller code' : 'Account No.'}: ${model?.account}',
                              style: TextStyle(color: Color(0xFF757575), fontSize: 14))
                    ],
                  ),
                )
              ],
            ),
            isEdit
                ? IconButton(
                    icon: Icon(Icons.delete_outline, color: appColorPrimaryKiple),
                    onPressed: () {
                      if (deleteAction != null) deleteAction!();
                    },
                    padding: EdgeInsets.only(top: 0, bottom: 0))
                : Container()
          ],
        ),
      ),
    );
  }
}
