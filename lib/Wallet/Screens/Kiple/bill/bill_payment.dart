import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/bill/provider/bill_payment_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/components/payment_methods.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_item.dart';
import 'package:selangkah_new/Wallet/models/paymethod_model.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class BillPayment extends StatefulWidget {
  final productId;
  final account;
  final amount;
  final productName;

  BillPayment({@required this.productId, @required this.account, @required this.productName, @required this.amount});

  @override
  _BillPaymentState createState() => _BillPaymentState();
}

class _BillPaymentState extends State<BillPayment> with StateLifeCycle<BillPayment, BillPaymentProvider> {
  late BillPaymentProvider paymentProvider;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'scan_qr_pay_title'.tr(),
      backgroundColor: Colors.white,
      child: ChangeNotifierProvider(
        create: (_) {
          return paymentProvider;
        },
        child: Consumer(
          builder: (ctx, BillPaymentProvider provider, child) {
            return configBody();
          },
        ),
      ),
    );
  }

  Widget configBody() {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          child: SingleChildScrollView(
            child: Column(
              children: [
                configPaymentInfo(),
                PaymentMethodsWidget(
                  payMethodsFunction: (List<dynamic> methods) {
                    paymentProvider.pays = methods as List<PayWayModel>;
                    setState(() {});
                  },
                )
              ],
            ),
          ),
        ),
        Positioned(
          child: NormalClickItem(
            content: 'Pay',
            clickFunction: () {
              paymentProvider.payAction();
            },
            enable: paymentProvider.pays.length != 0,
          ),
          left: 30,
          right: 30,
          bottom: 30,
        )
      ],
    );
  }

  Widget configPaymentInfo() {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.fromLTRB(15, 10, 15, 20),
      padding: EdgeInsets.fromLTRB(15, 10, 15, 0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.15), blurRadius: 5)]),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 0),
                    child: Text('${widget.productName}', style: TextStyle(color: Color(0xFF212121), fontSize: 16)),
                  )
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Row(
                    children: [
                      Text('RM ', style: TextStyle(color: Color(0xFF212121), fontSize: 12)),
                      Text('${paymentProvider.handleAmount()}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 16, fontWeight: FontWeight.bold)),
                    ],
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      paymentProvider.jumpToPoints();
                    },
                    child: Text(
                      paymentProvider.integral == null
                          ? "${'pay_points'.tr()}"
                          : "${MyTools.dealNumber(double.parse(widget.amount))}",
                      style: paymentProvider.integral == null
                          ? TextStyle(
                              fontSize: 12,
                              color: appColorPrimary,
                            )
                          : TextStyle(
                              fontSize: 12,
                              color: appColorPrimary,
                              decoration: TextDecoration.lineThrough,
                              decorationColor: appColorPrimary),
                    ),
                  )
                ],
              )
            ],
          ),
          lineView(),
          Container(
            width: double.infinity,
            padding: EdgeInsets.only(top: 15, bottom: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('bill_select_account'.tr(), style: TextStyle(color: Color(0xFF757575), fontSize: 14)),
                SizedBox(height: 8),
                Text('${widget.account}', style: TextStyle(color: Color(0xFF757575), fontSize: 14))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget lineView() {
    return Container(margin: EdgeInsets.only(top: 20), width: double.infinity, height: 1, color: Color(0xFFf5f5f5));
  }

  @override
  BillPaymentProvider createProvider(BuildContext buildContext) {
    paymentProvider = BillPaymentProvider(buildContext);
    paymentProvider.amount = widget.amount;
    paymentProvider.account = widget.account;
    paymentProvider.productId = widget.productId;
    paymentProvider.productName = widget.productName;
    return paymentProvider;
  }
}
