import 'dart:async';

import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/provider/timer_provider.dart';
import 'package:selangkah_new/Wallet/Utils/app_helper.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class VerifyPhoneNumberProvider extends BaseProvider {
  int? otpType;
  bool selfCheckOtp;
  String userPhone = "";

  VerifyPhoneNumberProvider(
      BuildContext buildContext, int otpType, this.selfCheckOtp)
      : super(buildContext) {
    verifyOtpController.text = "";
    this.otpType = otpType;
    this.selfCheckOtp = selfCheckOtp;
    initData();
  }

  initData() async {
    userPhone = await SecureStorage().readSecureData("userPhoneNo");
    notifyListeners();
  }

  TimerProvider? _timerProvider;
  TextEditingController _verifyOtpController = new TextEditingController();

  TextEditingController get verifyOtpController => _verifyOtpController;

  set verifyOtpController(TextEditingController value) {
    _verifyOtpController = value;
  }

  TimerProvider get timerProvider => _timerProvider!;

  String deviceInfo = '';
  String getDeviceID = '';
  String ipInfo = '';
  String locationInfo = '';

  set timerProvider(TimerProvider value) {
    _timerProvider = value;
  }

  checkOTP() {
    FocusScope.of(currentContext).requestFocus(FocusNode());
    if (selfCheckOtp) {
      Navigator.of(currentContext).pop(
          ResultModel<String>(success: true, data: verifyOtpController.text));
    } else {
      verifyOtp();
    }
  }

  resendOtpCheck() {
    if (timerProvider.isResendOTP) {
      sendOtp();
    }
  }

  @override
  void initState() {
    super.initState();
    sendOtp();
  }

  sendOtp() async {
    showProgressDialog();

    Map map = Map<String, dynamic>();
    map["otp_type"] = otpType;
    Map deviceMsg = await AppHelper().getDeviceMap();
    if (deviceMsg != null && deviceMsg.length > 0) {
      map.addAll(deviceMsg);
    }

    requestNetwork<String>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.SendOTP,
        params: map, onSuccess: (date) {
      dismissProgressDialog();
      timerProvider.controlTimer();
    }, onError: (code, msg) {
      dismissProgressDialog();
      showMessage(msg);
    });
  }

  verifyOtp() async {
    showProgressDialog();
    Map map = Map<String, dynamic>();
    map["otp_code"] = verifyOtpController.text;
    map["otp_type"] = otpType;
    Map deviceMsg = await AppHelper().getDeviceMap();
    if (deviceMsg != null && deviceMsg.length > 0) {
      map.addAll(deviceMsg);
    }

    requestNetwork<String>(Method.post,
        showDialog: false,
        url: HttpApi.KipleBaseUrl + HttpApi.VerifyOTP,
        params: map, onSuccess: (date) {
      dismissProgressDialog();
      Timer.periodic(Duration(milliseconds: 100), (timer) {
        timer.cancel();
        Navigator.of(currentContext).pop(ResultModel(success: true));
      });
    }, onError: (code, msg) {
      dismissProgressDialog();
      showMessage(msg);
      verifyOtpController.text = "";
      notifyListeners();
    });
  }

  String handleInformation() {
    String info = '';
    if (deviceInfo.isNotEmpty) {
      info = 'DeviceInfo: $deviceInfo\n';
    }

    if (getDeviceID.isNotEmpty) {
      info = info + 'getDeviceID: $getDeviceID\n';
    }

    if (ipInfo.isNotEmpty) {
      info = info + 'ipInfo: $ipInfo\n';
    }

    if (locationInfo.isNotEmpty) {
      info = info + 'ipInfo: $locationInfo\n';
    }
    return info;
  }
}
