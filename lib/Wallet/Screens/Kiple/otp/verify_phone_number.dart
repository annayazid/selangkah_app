import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/provider/timer_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/otp/provider/verify_phone_number_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/pin_code_input_textfield.dart';
import 'package:selangkah_new/utils/AppColors.dart';

import '../ekyc/ekyc_top_content_widget.dart';

class OtpType {
  static const int Register = 1;
  static const int UpdateProfile = 2;
  static const int ResetPassword = 3;
  static const int ResetPin = 4;
  static const int CreatePin = 5;
}

class VerifyPhoneNumberPage extends StatefulWidget {
  final int otpType;
  final bool selfCheckOtp;

  VerifyPhoneNumberPage(this.otpType, {this.selfCheckOtp = false}) {}

  @override
  State<StatefulWidget> createState() {
    return VerifyPhoneNumberState();
  }
}

class VerifyPhoneNumberState extends State<VerifyPhoneNumberPage>
    with StateLifeCycle<VerifyPhoneNumberPage, VerifyPhoneNumberProvider> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    return AppBarWhite(
      title: 'verify_phone_number_title'.tr().toString(),
      child: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              TopContentWidget('verify_phone_number_bodytitle'.tr().toString(), [
                Text(
                  'verify_phone_number_numberdes'.tr().toString(),
                  style: primaryTextStyle(color: appTextColorGray, size: 14),
                ),
                ChangeNotifierProvider<VerifyPhoneNumberProvider>(
                  create: (BuildContext context) {
                    return provider;
                  },
                  child: Consumer<VerifyPhoneNumberProvider>(
                    builder: (BuildContext? context, VerifyPhoneNumberProvider? timeProvider, Widget? child) {
                      return Text(
                        "6${provider.userPhone}",
                        style: secondaryTextStyle(color: appTextColorPrimary, size: 14),
                      );
                    },
                  ),
                ),
              ]),
              SizedBox(
                height: 4.h as double,
              ),
              Container(
                margin: EdgeInsets.only(left: 4.w as double, right: 4.w as double),
                height: 100,
                child: PinCodeInputTextField(
                  controller: provider.verifyOtpController,
                  onTextChange: (text) {
                    if (text.length == 6) {
                      provider.checkOTP();
                    }
                  },
                  obscureText: true,
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: ChangeNotifierProvider<TimerProvider>(
                  create: (BuildContext context) {
                    TimerProvider timerNotifier = TimerProvider(context);
                    provider.timerProvider = timerNotifier;
                    return timerNotifier;
                  },
                  child: Consumer<TimerProvider>(
                    builder: (BuildContext? context, TimerProvider? timeProvider, Widget? child) {
                      return GestureDetector(
                        child: Container(
                          child: Text(
                            timeProvider!.codeStr,
                            style: primaryTextStyle(color: appColorPrimaryKiple, size: 14),
                          ),
                        ),
                        onTap: () {
                          provider.resendOtpCheck();
                        },
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  late VerifyPhoneNumberProvider provider;

  @override
  createProvider(BuildContext buildContext) {
    provider = VerifyPhoneNumberProvider(buildContext, widget.otpType, widget.selfCheckOtp);
    return provider;
  }

  @override
  void dispose() {
    super.dispose();
  }
}
