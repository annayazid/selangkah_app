import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/models/integral_model_entity.dart';
import 'package:selangkah_new/Wallet/models/loyalty_list_modle_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class LoyaltyListProvider extends BaseProvider {
  LoyaltyListProvider(BuildContext buildContext) : super(buildContext) {
    scrollController.addListener(() {
      if (scrollController.position.pixels == scrollController.position.maxScrollExtent && hasMore) {
        loadMore();
      }
    });
  }

  ScrollController scrollController = ScrollController();

  Future<void> onReference() async {
    loadFirstPage(showProgressDialog: false);
  }

  IntegralModelEntity? dataModel;
  bool integralInfoSuccess = false;

  getIntegralInfo() {
    requestNetwork<IntegralModelEntity>(Method.get,
        url: HttpApi.KipleBaseUrl + HttpApi.GetIntegralInfo, showDialog: false, onSuccess: (data) {
      integralInfoSuccess = true;
      checkSuccess();
      if (data != null) {
        dataModel = data;
        notifyListeners();
      }
    }, onError: (code, msg) {
      errorDismiss();
      showMessage(msg);
    });
  }

  bool hasMore = true;
  int currentPage = 1;
  int pageSize = 20;
  List<LoyaltyListModleList> dataList = [];
  bool firstLoadSuccess = false;

  void loadFirstPage({bool showProgressDialog = true}) {
    currentPage = 1;
    Map map = Map();
    map["page_no"] = currentPage;
    map["page_size"] = pageSize;
    requestNetwork<LoyaltyListModleEntity>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.IntegralHistory,
        params: map,
        showDialog: showProgressDialog, onSuccess: (data) {
      firstLoadSuccess = true;
      checkSuccess();
      if (data != null && data.xList != null) {
        dataList.clear();
        dataList.addAll(data.xList as Iterable<LoyaltyListModleList>);
        hasMore = data.total! > dataList.length;
        notifyListeners();
      }
    }, onError: (code, msg) {
      errorDismiss();
      showMessage(msg);
    });
  }

  Future<void> loadMore() async {
    currentPage++;
    Map map = Map();
    map["page_no"] = currentPage;
    map["page_size"] = pageSize;
    requestNetwork<LoyaltyListModleEntity>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.IntegralHistory, params: map, showDialog: false, onSuccess: (data) {
      if (data != null) {
        dataList.addAll(data.xList as Iterable<LoyaltyListModleList>);
        hasMore = data.total! > dataList.length;
        notifyListeners();
      }
    }, onError: (code, msg) {
      currentPage--;
      showMessage(msg);
    });
  }

  errorDismiss() {
    integralInfoSuccess = true;
    firstLoadSuccess = true;
    checkSuccess();
  }

  checkSuccess() {
    if (integralInfoSuccess && firstLoadSuccess) {
      dismissProgressDialog();
    }
  }

  @override
  void initState() {
    super.initState();
    integralInfoSuccess = false;
    firstLoadSuccess = false;
    showProgressDialog();
    getIntegralInfo();
    loadFirstPage(showProgressDialog: false);
  }

  bool isGreenNew(LoyaltyListModleList loyalty) {
    if (loyalty.integral.toString().startsWith("-")) {
      return false;
    } else {
      return true;
    }
  }
}
