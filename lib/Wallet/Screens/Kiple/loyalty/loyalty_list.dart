import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/loyalty/provider/loyalty_list_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class LoyaltyListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoyaltyListState();
  }
}

class LoyaltyListState extends State<LoyaltyListPage> with StateLifeCycle<LoyaltyListPage, LoyaltyListProvider> {
  String? currentLanguage = "";

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    currentLanguage = EasyLocalization.of(context)?.locale.languageCode;
    return AppBarWhite(
      title: "wallet_item_loyalty".tr(),
      child: Column(
        children: [
          ChangeNotifierProvider<LoyaltyListProvider>(
            create: (_) => provider,
            child: Consumer<LoyaltyListProvider>(
              builder: (BuildContext? buildContext, LoyaltyListProvider? notificationListProvider, Widget? child) {
                return Container(
                  margin: EdgeInsets.only(
                      left: 7.w as double, right: 7.w as double, top: 8.h as double, bottom: 8.h as double),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        provider.dataModel == null ? "0" : "${provider.dataModel?.validIntegral}",
                        style: primaryTextStyle(size: 20, color: appColorPrimaryKiple),
                      ),
                      Text(
                        " ${"mobile_points_available".tr()}",
                        style: primaryTextStyle(size: 16, color: appTextColorPrimary),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
          Container(
            height: 3.h as double,
            color: appLineColor,
          ),
          Expanded(
            child: Container(
              child: ChangeNotifierProvider<LoyaltyListProvider>(
                create: (_) => provider,
                child: Consumer<LoyaltyListProvider>(
                  builder: (BuildContext? buildContext, LoyaltyListProvider? notificationListProvider, Widget? child) {
                    return RefreshIndicator(
                      displacement: 10.0,
                      color: appColorPrimary,
                      child: Container(
                        child: ListView.builder(
                          itemBuilder: (context, position) => buildListItem(position),
                          controller: provider.dataList.length >= provider.pageSize ? provider.scrollController : null,
                          itemCount: provider.dataList.length + 1,
                        ),
                      ),
                      onRefresh: provider.onReference,
                    );
                  },
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  late LoyaltyListProvider provider;

  @override
  LoyaltyListProvider createProvider(BuildContext buildContext) {
    provider = LoyaltyListProvider(buildContext);
    return provider;
  }

  Widget buildListItem(int position) {
    if (position == provider.dataList.length) {
      return Offstage(
        offstage: !provider.hasMore,
        child: Container(
          height: 20.h as double,
          alignment: Alignment.center,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                margin: EdgeInsets.only(right: 3.w as double),
                width: 8.h as double,
                height: 8.h as double,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  valueColor: AlwaysStoppedAnimation<Color>(appColorPrimary),
                ),
              ),
              Text(
                "loading_more".tr(),
                style: primaryTextStyle(
                  size: 14,
                  color: appTextColorGray,
                ),
              ),
            ],
          ),
        ),
      );
    }
    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(7.w as double),
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    currentLanguage == "ms"
                        ? provider.dataList[position].description_malay!.isEmpty
                            ? provider.dataList[position].description!
                            : provider.dataList[position].description_malay!
                        : provider.dataList[position].description!,
                    style: primaryTextStyle(size: 14, color: appTextColorPrimary),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 2.h as double),
                    child: Text(
                      '${MyTools.formatDateInTransaction(provider.dataList[position].createTime)}',
                      style: primaryTextStyle(size: 12, color: appTextColorGray),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Container(),
              ),
              Text(
                "${provider.isGreenNew(provider.dataList[position]) ? "+" : ""}${provider.dataList[position].integral}",
                style: primaryTextStyle(
                    size: 16,
                    color: provider.isGreenNew(provider.dataList[position]) ? colorMoneyAdd : appTextColorPrimary),
              ),
            ],
          ),
        ),
        Container(
          height: 1.h as double,
          color: appLineColor,
        )
      ],
    );
  }
}
