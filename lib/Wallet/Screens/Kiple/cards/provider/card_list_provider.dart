import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/cards/card_online.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/cards/components/top_up_success.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/result_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/components/failed_content.dart';
import 'package:selangkah_new/Wallet/Utils/app_helper.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/components/talk_to_us.dart';
import 'package:selangkah_new/Wallet/models/card_list_entity.dart';
import 'package:selangkah_new/Wallet/models/card_manager_entity.dart';
import 'package:selangkah_new/Wallet/models/scan_payment_result_entity.dart';
import 'package:selangkah_new/Wallet/models/wallet_Info_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class CardListProvider extends BaseProvider {
  CardListProvider(BuildContext buildContext) : super(buildContext);
  CardListEntity? cardList;
  String? amount;

  getData() {
    requestNetwork(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.CardManager,
        onError: (code, msg) {
      showMessage(msg);
    }, onSuccess: (data) {});
  }

  getCardListData() {
    requestNetwork<CardListEntity>(Method.get,
        url: HttpApi.KipleBaseUrl + HttpApi.CardsList, onError: (code, msg) {
      showMessage(msg);
    }, onSuccess: (data) {
      cardList = data;
      notifyListeners();
    });
  }

  topUpData(CardListCard card) async {
    var para = {
      'amount': int.parse(MyTools.dealNumber(
          double.parse(amount!) * 100.toInt(),
          pointLength: 0)),
      'card_bin': card.maskedPan
    };

    showProgressDialog();
    Map<String, Object?> deviceMsg = await AppHelper().getDeviceMap();
    if (deviceMsg != null && deviceMsg.length > 0) {
      para.addAll(deviceMsg);
    }

    // requestNetwork<ScanPaymentResultEntity>(Method.post,
    //     params: para, showDialog: false, url: HttpApi.KipleBaseUrl + HttpApi.TopUp, onError: (code, msg) {
    //   dismissProgressDialog();
    //   gotoFailedPage(msg);
    // }, onSuccess: (data) {
    //   dismissProgressDialog();
    //   gotoSuccess(data);
    // });

    requestNetwork<CardManagerEntity>(Method.post,
        params: para,
        showDialog: false,
        url: HttpApi.KipleBaseUrl + HttpApi.TopUp, onError: (code, msg) {
      dismissProgressDialog();
      gotoFailedPage(msg);
    }, onSuccess: (data) {
      dismissProgressDialog();
      Future.delayed(Duration(microseconds: 0), () {
        Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
          return CardOnline(url: data.url);
        }));
      });
      // gotoSuccess(data);
    });
  }

  getCardManagerData() {
    requestNetwork<CardManagerEntity>(Method.get,
        url: HttpApi.KipleBaseUrl + HttpApi.CardManager, onError: (code, msg) {
      showMessage(msg);
    }, onSuccess: (data) {
      Future.delayed(Duration(microseconds: 0), () {
        Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
          return CardOnline(url: data.url!, addCard: true);
        })).then((value) {
          getCardListData();
        });
      });
    });
  }

  gotoFailedPage(String message) {
    String contentString = message;
    Widget content = FailedContent(
        title: 'withdrawal_fail_title'.tr(), content: contentString);

    Future.delayed(Duration(microseconds: 0), () {
      Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
        return ResultPage(
          contentChild: content,
          bottomChild: Talk2UsWidget(
            contentString: "mobile_help_content".tr(),
            showButton: true,
          ),
          status: false,
        );
      })).then((value) {
        gotoHomePage();
      });
    });
  }

  gotoSuccess(ScanPaymentResultEntity result) {
    requestNetwork<WalletInfoEntity>(Method.get,
        url: HttpApi.KipleBaseUrl + HttpApi.WalletInfo, onSuccess: (data) {
      Future.delayed(Duration(microseconds: 0), () {
        Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
          return ResultPage(
            contentChild:
                TopUpSuccess(result: result, balance: data.useableBalance),
            bottomChild: Talk2UsWidget(
              contentString: "mobile_help_content".tr(),
              showButton: true,
            ),
            status: true,
          );
        })).then((value) {
          gotoHomePage();
        });
      });
    }, onError: (code, message) {
      showMessage(message);
    });
  }

  gotoHomePage() {
    Navigator.popUntil(currentContext, (route) => route.isFirst);
  }
}
