import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/cards/provider/card_list_provider.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/models/card_list_entity.dart';

class ECardsList extends StatefulWidget {
  final String? amount;

  ECardsList({this.amount});

  @override
  _ECardsListState createState() => _ECardsListState();
}

class _ECardsListState extends State<ECardsList> with StateLifeCycle<ECardsList, CardListProvider> {
  late CardListProvider listProvider;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(title: 'top_up_bank_title'.tr(), backgroundColor: Colors.white, child: configBody());
  }

  Widget configBody() {
    return ChangeNotifierProvider(create: (_) {
      return listProvider;
    }, child: Consumer(
      builder: (ctx, CardListProvider provider, child) {
        return Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Column(
                  children: getSubList(),
                ),
                configAddCard()
              ],
            ),
          ),
        );
      },
    ));
  }

  List<Widget> getSubList() {
    List<Widget> widgets = [];
    if (listProvider.cardList != null &&
        listProvider.cardList?.cards != null &&
        listProvider.cardList!.cards!.length > 0) {
      widgets = listProvider.cardList!.cards!.map((CardListCard card) {
        return configCardItem(card);
      }).toList();
    }

    return widgets;
  }

  Widget configCardItem(CardListCard card) {
    String visaPath = 'assets/images/Wallet/visa_card.png';
    String masterPath = 'assets/images/Wallet/master_card.png';
    return GestureDetector(
      onTap: () {
        listProvider.topUpData(card);
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
        width: double.infinity,
        decoration:
            BoxDecoration(color: Colors.white, border: Border(bottom: BorderSide(width: 1, color: Color(0xFFf5f5f5)))),
        child: Row(
          children: [
            Image.asset(card.scheme == 'VISA' ? visaPath : masterPath, width: 50, fit: BoxFit.fitWidth),
            Text('${card.maskedPan}', style: TextStyle(color: Color(0xFF212121), fontSize: 16))
          ],
        ),
      ),
    );
  }

  Widget configAddCard() {
    return GestureDetector(
      onTap: () {
        listProvider.getCardManagerData();
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
        width: double.infinity,
        decoration:
            BoxDecoration(color: Colors.white, border: Border(bottom: BorderSide(width: 1, color: Color(0xFFf5f5f5)))),
        child: Row(
          children: [Text('top_up_bank_other'.tr(), style: TextStyle(color: Color(0xFF212121), fontSize: 16))],
        ),
      ),
    );
  }

  @override
  CardListProvider createProvider(BuildContext buildContext) {
    listProvider = CardListProvider(buildContext);
    listProvider.amount = widget.amount;
    listProvider.getCardListData();
    return listProvider;
  }
}
