import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/cards/pop_up_alert.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_item.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class ECardDetail extends StatefulWidget {
  @override
  _ECardDetailState createState() => _ECardDetailState();
}

class _ECardDetailState extends State<ECardDetail> {
  bool setAsPrimary = false;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(title: "eCard", backgroundColor: Colors.white, child: configBody());
  }

  Widget configBody() {
    String visaPath = 'assets/images/Wallet/visa_card.png';
    String masterPath = 'assets/images/Wallet/master_card.png';

    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(30, 15, 30, 15),
                    padding: EdgeInsets.fromLTRB(20, 25, 20, 25),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.15), blurRadius: 5)],
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(width: 1, color: Color(0xFFceced2)),
                    ),
                    child: Column(
                      children: [
                        Container(
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Card number', style: TextStyle(color: Color(0xFF747474), fontSize: 14)),
                              Text('******* 3454',
                                  style: TextStyle(color: Color(0xFF343434), fontSize: 16, height: 1.5)),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 45),
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Expiry date', style: TextStyle(color: Color(0xFF747474), fontSize: 14)),
                              Text('12 / 22', style: TextStyle(color: Color(0xFF343434), fontSize: 18, height: 1.5)),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Positioned(
                    top: 24,
                    right: 30,
                    child: Image.asset(masterPath, width: 70, fit: BoxFit.fitWidth),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 20, left: 30, right: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Set as primary', style: TextStyle(color: Color(0xFF212121), fontSize: 14)),
                    Switch(
                        value: setAsPrimary,
                        onChanged: (value) {
                          setAsPrimary = value;
                          setState(() {});
                        },
                        activeColor: appColorPrimaryKiple)
                  ],
                ),
              )
            ],
          ),
        ),
        Positioned(
            left: 30,
            right: 30,
            bottom: 35,
            child: NormalClickItem(
              content: 'Delete',
              clickFunction: () {
                showDialog(
                    context: context,
                    builder: (_) {
                      return PopUpAlert(
                          title: 'Delete This Payment Card?',
                          content: 'The payment card will be removed.',
                          confirm: 'Confirm',
                          isCancel: true,
                          confirmFunction: () {});
                    });
              },
            ))
      ],
    );
  }
}
