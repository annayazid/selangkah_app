import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_item.dart';

class TopUpCardDetail extends StatefulWidget {
  @override
  _TopUpCardDetailState createState() => _TopUpCardDetailState();
}

class _TopUpCardDetailState extends State<TopUpCardDetail> {
  @override
  Widget build(BuildContext context) {
    return AppBarWhite(title: 'Top Up', backgroundColor: Colors.white, child: configBody());
  }

  Widget configBody() {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            children: [configCardInfo(), configBottom()],
          ),
        ),
        Positioned(
            child: NormalClickItem(
              content: 'Add',
              clickFunction: () {},
            ),
            left: 30,
            right: 30,
            bottom: 40)
      ],
    );
  }

  Widget configCardInfo() {
    double width = (MediaQueryData.fromWindow(window).size.width - 120) * 0.5;
    return Container(
      width: double.infinity,
      margin: EdgeInsets.fromLTRB(30, 15, 30, 15),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.15), blurRadius: 5)]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Card number', style: TextStyle(color: Color(0xFF747474), fontSize: 14)),
          Container(
            margin: EdgeInsets.only(top: 10),
            padding: EdgeInsets.only(bottom: 10),
            width: double.infinity,
            decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Color(0xFFe2e2e2), width: 1))),
            child: Row(
              children: [
                Container(
                    margin: EdgeInsets.only(right: 15),
                    child: Image.asset('assets/images/Wallet/bank_card_front.png', height: 25, width: 36)),
                Text('1234 5678 9012 3456', style: TextStyle(color: Color(0xFFceced2), fontSize: 16))
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 40),
            padding: EdgeInsets.only(bottom: 10),
            child: IntrinsicHeight(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: width,
                    decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Color(0xFFe2e2e2), width: 1))),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Expiry date',
                          style: TextStyle(color: Color(0xFF747474), fontSize: 14, height: 1.5),
                        ),
                        SizedBox(height: 5),
                        Text(
                          '03/23',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 16, height: 1.5),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 10),
                    width: width,
                    decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Color(0xFFe2e2e2), width: 1))),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'CVV / CVC',
                          style: TextStyle(color: Color(0xFF747474), fontSize: 14, height: 1.5),
                        ),
                        SizedBox(height: 5),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '123',
                              style: TextStyle(color: Color(0xFF212121), fontSize: 16, height: 1.5),
                            ),
                            Image.asset('assets/images/Wallet/bank_card_front.png', height: 25, width: 36)
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget configBottom() {
    return Container(
      margin: EdgeInsets.fromLTRB(30, 5, 30, 5),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Save this card for future payments',
                style: TextStyle(color: Color(0xFF212121), fontSize: 14),
              ),
              Image.asset('assets/images/Wallet/unselectd.png', width: 18, height: 18)
            ],
          ),
          SizedBox(height: 5),
          Text(
            'By adding your debit / credit card,  you agree to our ',
            style: TextStyle(color: Color(0xFF757575), fontSize: 12, height: 1.5),
          ),
          Text(
            'Terms and Conditions',
            style: TextStyle(color: Color(0xFF3391ff), fontSize: 14, height: 1.5),
          ),
        ],
      ),
    );
  }
}
