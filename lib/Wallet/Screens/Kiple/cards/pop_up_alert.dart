import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/components/click_item.dart';

class PopUpAlert extends StatefulWidget {
  final String title;
  final String content;
  final String confirm;
  final Function? confirmFunction;
  final bool isCancel;

  PopUpAlert(
      {required this.title, required this.content, required this.confirm, this.confirmFunction, this.isCancel = false});

  @override
  _PopUpAlertState createState() => _PopUpAlertState();
}

class _PopUpAlertState extends State<PopUpAlert> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: Colors.transparent, body: configBody());
  }

  Widget configBody() {
    return Center(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 30),
        padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
        width: double.infinity,
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              widget.title,
              style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 15),
            Padding(
              padding: const EdgeInsets.fromLTRB(35, 0, 35, 0),
              child: Text(
                widget.content,
                style: TextStyle(color: Color(0xFF757575), fontSize: 12),
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 35),
              child: NormalClickItem(
                  content: widget.confirm,
                  height: 50,
                  clickFunction: () {
                    if (widget.confirmFunction != null) {
                      widget.confirmFunction!();
                    }
                    Navigator.pop(context);
                  }),
            ),
            !widget.isCancel
                ? Container(width: double.infinity, height: 20)
                : GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      color: Colors.white,
                      margin: EdgeInsets.only(top: 5),
                      width: double.infinity,
                      height: 50,
                      alignment: Alignment.center,
                      child: Text("cancel".tr(), style: TextStyle(color: Color(0xFF757575), fontSize: 14)),
                    ),
                  )
          ],
        ),
      ),
    );
  }
}
