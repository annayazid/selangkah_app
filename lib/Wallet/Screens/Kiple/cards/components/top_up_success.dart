import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/models/scan_payment_result_entity.dart';

class TopUpSuccess extends StatelessWidget {
  final ScanPaymentResultEntity? result;
  final balance;

  TopUpSuccess({this.result, this.balance});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('top_up_successful'.tr(),
              style: TextStyle(color: Color(0xFF212121), fontSize: 16, fontWeight: FontWeight.bold)),
          SizedBox(height: 15),
          Text('top_up_success_processed'.tr(), style: TextStyle(color: Color(0xFF757575), fontSize: 14)),
          SizedBox(height: 15),
          Text('scan_qr_pay_success_transaction'.tr(),
              style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5)),
          Text('${result?.orderNo}', style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5)),
          SizedBox(height: 15),
          Text('${MyTools.formatDateInTransaction(result?.bankTime)}',
              style: TextStyle(color: Color(0xFF212121), fontSize: 14)),
          SizedBox(height: 15),
          grayContainer(),
          configPaymentInfo(),
          grayContainer()
        ],
      ),
    );
  }

  Widget grayContainer() {
    return Container(width: double.infinity, height: 10, color: Color(0xFFf5f5f5));
  }

  Widget configPaymentInfo() {
    return Container(
      padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
      width: double.infinity,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('top_up_success_amount'.tr(), style: TextStyle(color: Color(0xFF757575), fontSize: 16)),
              Text('RM ${(double.parse(result!.amount!) / 100.0).toStringAsFixed(2)}',
                  style: TextStyle(color: Color(0xFF212121), fontSize: 16)),
            ],
          ),
          SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('top_up_balance'.tr(), style: TextStyle(color: Color(0xFF757575), fontSize: 16)),
              Text('RM ${(double.parse(balance) / 100.0).toStringAsFixed(2)}',
                  style: TextStyle(color: Color(0xFF212121), fontSize: 16)),
            ],
          )
        ],
      ),
    );
  }
}
