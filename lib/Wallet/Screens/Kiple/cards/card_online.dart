import 'dart:async';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Utils/event_bus_tool.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/utils/constants.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CardOnline extends StatefulWidget {
  final String? url;
  final bool fromPayment;
  final bool addCard;

  CardOnline({this.url, this.fromPayment = false, this.addCard = false});

  @override
  _CardOnlineState createState() => _CardOnlineState();
}

class _CardOnlineState extends State<CardOnline> {
  WebViewController? webViewCon;
  String? webUrl;
  bool showHome = false;
  String activeWebUrl = "";
  Timer? timer;

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    timer = Timer.periodic(Duration(seconds: 4), (Timer t) => getCurrentUrl());
  }

  getLanguageType() {
    String? languageValue;
    String? code = EasyLocalization.of(context)?.locale.languageCode;
    if (code == 'en') {
      languageValue = 'en-US';
    } else if (code == 'ms') {
      languageValue = 'ms-MS';
    }

    if (languageValue == null) {
      webUrl = widget.url;
    } else {
      webUrl = widget.url! + '&lang=' + languageValue;
    }
  }

  getCurrentUrl() async {
    var currentUrl = await webViewCon?.currentUrl();
    if (currentUrl != null) {
      try {
        activeWebUrl = currentUrl;
        if (activeWebUrl.contains("result/success") ||
            activeWebUrl.contains("result/error") ||
            activeWebUrl.contains("closewebview")) {
          if (!showHome) {
            showHome = true;
            setState(() {});
          }
        }
      } catch (e) {}
    }
  }

  @override
  Widget build(BuildContext context) {
    getLanguageType();
    return AppBarWhite(
      title: 'top_up_online'.tr(),
      actions: subActions(),
      child: Stack(children: [
        WebView(
          javascriptMode: JavascriptMode.unrestricted,
          initialUrl: webUrl,
          onWebViewCreated: (WebViewController controller) {
            webViewCon = controller;
          },
          onPageFinished: (String url) async {
            try {
              activeWebUrl = url;
              if (activeWebUrl.contains("result/success") ||
                  activeWebUrl.contains("result/error") ||
                  activeWebUrl.contains("closewebview")) {
                if (!showHome) {
                  showHome = true;
                  setState(() {});
                }
              }
            } catch (e) {}
          },
          onPageStarted: (String url) async {
            try {
              activeWebUrl = url;
              if (activeWebUrl.contains("result/success") ||
                  activeWebUrl.contains("result/error") ||
                  activeWebUrl.contains("closewebview")) {
                if (!showHome) {
                  showHome = true;
                  setState(() {});
                }
              }
            } catch (e) {}
          },
        ),
        showHome
            ? Align(
                alignment: Alignment.bottomCenter,
                child: GestureDetector(
                  onTap: () {
                    walletEventBus.fire('');
                    if (widget.fromPayment) {
                      Navigator.pop(context);
                    } else {
                      if (widget.addCard) {
                        Navigator.pop(context);
                      } else {
                        Navigator.popUntil(context, (route) => route.isFirst);
                      }
                    }
                  },
                  child: Container(
                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 50),
                    decoration: BoxDecoration(
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    alignment: Alignment.center,
                    height: MediaQuery.of(context).size.width / 8,
                    child: Text(
                      'backhome'.tr(),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              )
            : Container(),
      ]),
    );
  }

  List<Widget> subActions() {
    List<Widget> subs = [];
    Widget item = IconButton(
        icon: Icon(Icons.close),
        onPressed: () {
          walletEventBus.fire('');
          if (widget.fromPayment) {
            Navigator.pop(context);
          } else {
            if (widget.addCard) {
              Navigator.pop(context);
            } else {
              Navigator.popUntil(context, (route) => route.isFirst);
            }
          }
        });
    subs.add(item);
    return subs;
  }

  @override
  void dispose() {
    super.dispose();
    walletEventBus.fire('');
  }
}
