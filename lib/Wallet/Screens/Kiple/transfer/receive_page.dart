import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/transfer/provider/receive_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/components/appbar_white_result.dart';
import 'package:selangkah_new/Wallet/components/click_button.dart';
import 'package:selangkah_new/Wallet/components/visibility.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class ReceivePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ReceiveState();
  }
}

class ReceiveState extends State<ReceivePage> with StateLifeCycle<ReceivePage, ReceiveProvider> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    return AppBarWhiteResult(
      title: "receive_string".tr(),
      resultModel: ResultModel(success: true),
      child: Column(
        children: [
          Expanded(
            child: Container(),
            flex: 78,
          ),
          Text(
            "receive_my_qr".tr(),
            style: TextStyle(fontSize: 16, color: appTextColorPrimary),
          ),
          Expanded(
            child: Container(),
            flex: 7,
          ),
          Text(
            "receive_content_notice".tr(),
            style: TextStyle(fontSize: 14, color: appTextColorGray),
            textAlign: TextAlign.center,
          ),
          Expanded(
            child: Container(),
            flex: 33,
          ),
          ChangeNotifierProvider<ReceiveProvider>(
            create: (_) => provider,
            child: Consumer<ReceiveProvider>(
              builder: (BuildContext? buildContext, ReceiveProvider? receiveProvider, Widget? child) {
                return VisibilityView(
                  visibility: provider.qrData == null ? VisibilityFlag.invisible : VisibilityFlag.visible,
                  child: Container(
                    child: QrImage(
                        data: '${provider.qrData == null ? "" : provider.qrData?.qrCode}', size: 75.w as double),
                  ),
                );
              },
            ),
          ),
          Expanded(
            child: Container(),
            flex: 120,
          ),
          Container(
            margin: EdgeInsets.only(left: 14.w as double, right: 14.w as double),
            child: ClickButton(
              content: "receive_close_string".tr(),
              canClick: true,
              clickFunction: () {
                provider.dismiss();
              },
            ),
          ),
          Expanded(
            child: Container(),
            flex: 25,
          ),
        ],
      ),
    );
  }

  late ReceiveProvider provider;

  @override
  ReceiveProvider createProvider(BuildContext buildContext) {
    provider = ReceiveProvider(buildContext);
    return provider;
  }
}
