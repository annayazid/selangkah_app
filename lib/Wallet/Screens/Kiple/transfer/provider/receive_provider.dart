import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/models/my_qr_code_entity.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class ReceiveProvider extends BaseProvider {
  ReceiveProvider(BuildContext buildContext) : super(buildContext);

  MyQrCodeEntity? qrData;

  getQrCodeData() {
    requestNetwork<MyQrCodeEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.MyQrCode, onSuccess: (data) {
      qrData = data;
      notifyListeners();
    }, onError: (code, msg) {
      Navigator.of(currentContext).pop(ResultModel(success: false));
      showMessage(msg);
    });
  }

  @override
  void initState() {
    super.initState();
    getQrCodeData();
  }

  dismiss() {
    Navigator.of(currentContext).pop(ResultModel(success: true));
  }
}
