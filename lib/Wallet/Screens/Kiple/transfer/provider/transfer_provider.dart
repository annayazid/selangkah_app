import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_contact_picker/flutter_native_contact_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/pin/input_pin.dart';
import 'package:selangkah_new/Wallet/Utils/app_helper.dart';
import 'package:selangkah_new/Wallet/Utils/log_utils.dart';
import 'package:selangkah_new/Wallet/Utils/money_format.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/components/pop_up_dialog.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/models/transfer_result_entity.dart';
import 'package:selangkah_new/Wallet/models/user_profile_entity.dart';
import 'package:selangkah_new/Wallet/models/wallet_Info_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

import '../../result_page.dart';
import '../transfer_page.dart';

class TransferProvider extends BaseProvider {
  TransferState? transferState;

  TransferProvider(BuildContext buildContext, TransferState transferState, UserProfileEntity? userProfileEntity)
      : super(buildContext) {
    this.transferState = transferState;
    if (userProfileEntity != null) {
      searchResult = userProfileEntity;
    }
  }

  MoneyFormat? moneyFormat;

  TextEditingController mobileController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  selectContacter() async {
    String? phone = await selectPhoneNum();
    if (phone != null && phone.isNotEmpty) {
      searchUser(phone);
    }
  }

  bool canCommit = false;
  String lastMoney = "";

  initListener() {
    descriptionController.addListener(() {
      checkCanCommit();
    });
  }

  String amount = "";

  changeShowText(String text) {
    if (text.length > 0) {
      if (text.length == 1) {
        amount = (double.parse(text) / 100).toStringAsFixed(2);
      } else {
        double newValue = double.parse(text);
        if (newValue == 0) {
          amount = "";
        } else {
          if (text.substring(text.lastIndexOf(".") + 1, text.length).length == 2 &&
              amount.substring(amount.lastIndexOf(".") + 1, amount.length).length == 1) {
            amount = text;
          } else if (!text.endsWith(".") &&
              text.contains(".") &&
              (text.substring(text.lastIndexOf(".") + 1, text.length).length >= 1 &&
                  amount.substring(amount.lastIndexOf(".") + 1, amount.length).length >= 1) &&
              (double.parse(text.substring(0, text.length - 1)) == double.parse(amount) ||
                  double.parse(text) == double.parse(amount.substring(0, amount.length - 1)))) {
            if (text.length > amount.length) {
              amount = (newValue * 10).toStringAsFixed(2);
            } else if (text.length < amount.length) {
              amount = (newValue / 10).toStringAsFixed(2);
            }
          } else {
            amount = text;
          }
        }
        if (amount.isEmpty) {
          amount = "0.00";
        }
        if (double.parse(amount) > double.parse(wallet!.useableBalance!) / 100) {
          amount = amount.substring(0, amount.length - 1);
          amount = (double.parse(amount) / 10).toStringAsFixed(2);
        }
      }
    } else {
      amount = '';
    }
    notifyListeners();
    checkCanCommit();
  }

  checkCanCommit() {
    if (searchResult != null &&
        amount != null &&
        amount.isNotEmpty &&
        double.parse(amount) > 0 &&
        descriptionController.text.length > 0) {
      if (!canCommit) {
        canCommit = true;
        notifyListeners();
      }
    } else if (canCommit) {
      canCommit = false;
      notifyListeners();
    }
  }

  checkCommit() {
    if (searchResult == null) {
      showMessage("transfer_notice_one".tr());
      return;
    }
    if (double.parse(amount) <= 0) {
      showMessage("transfer_notice_two".tr());
      return;
    }
    if (descriptionController.text.length <= 0) {
      showMessage("transfer_notice_three".tr());
      return;
    }
  }

  Future<String?> selectPhoneNum() async {
    final status = await Permission.contacts.request();
    if (status.isGranted) {
      Log.d("Opening Contact picker");
      final FlutterContactPicker _contactPicker = new FlutterContactPicker();
      Contact? contact = await _contactPicker.selectContact();
      String phoneNumber = "";
      print(contact?.phoneNumbers?[0]);
      print(3232323232);
      if (contact != null) {
        phoneNumber = contact.phoneNumbers![0];
        if (phoneNumber.contains(" ")) {
          phoneNumber = phoneNumber.replaceAll(" ", "");
        }

        if (phoneNumber.contains("+")) {
          phoneNumber = phoneNumber.replaceAll("+", "");
        }

        if (phoneNumber.contains("-")) {
          phoneNumber = phoneNumber.replaceAll("-", "");
        }
        print(phoneNumber);
        return phoneNumber;
      }
    } else {
      showMessage("no_permission".tr());
    }
  }

  nextStep() async {
    if (!canCommit) {
      return;
    }

    FocusScope.of(currentContext).requestFocus(FocusNode());
    showProgressDialog();
    bool isNotOut = await isNotOutsideMalaysia();
    dismissProgressDialog();
    if (isNotOut == null) {
      Timer.periodic(Duration(milliseconds: 100), (timer) {
        timer.cancel();
        showNotDialog('ekyc_oops_title'.tr(), 'not_location_permission'.tr(), 'ekyc_hard_reject_button'.tr());
      });
    }
    if (isNotOut) {
      Timer.periodic(Duration(milliseconds: 100), (timer) async {
        timer.cancel();
        ResultModel pinResult = await Navigator.push(
          currentContext,
          MaterialPageRoute(
            builder: (context) {
              return InputPinPage.VerifyPin(
                needBackPinCode: true,
              );
            },
          ),
        );
        if (pinResult != null && pinResult.success!) {
          commitData(pinResult.data);
        }
      });
    } else {
      Timer.periodic(Duration(milliseconds: 100), (timer) {
        timer.cancel();
        showNotDialog('ekyc_oops_title'.tr(), 'out_malaysia_transfer'.tr(), 'ekyc_hard_reject_button'.tr(),
            confirmClickFunction: () {
          Navigator.of(currentContext).pop(ResultModel(success: false));
        });
      });
    }
  }

  commitData(String pinCode) async {
    showProgressDialog();
    Map map = Map();
    map["pin"] = pinCode;
    map["amount"] = int.parse(MyTools.dealNumber(double.parse(amount) * 100.toInt(), pointLength: 0));
    map["target_account"] = searchResult?.mobile;
    map["remark"] = descriptionController.text;
    Map deviceMsg = await AppHelper().getDeviceMap();
    if (deviceMsg != null && deviceMsg.length > 0) {
      map.addAll(deviceMsg);
    }
    requestNetwork<TransferResultEntity>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.CommitTransfer, showDialog: false, params: map, onSuccess: (data) async {
      dismissProgressDialog();
      Timer.periodic(Duration(milliseconds: 100), (timer) async {
        timer.cancel();
        ResultModel resultModel = await Navigator.push(
          currentContext,
          MaterialPageRoute(
            builder: (context) {
              return ResultPage(
                contentChild: transferState!.successChild(data),
                bottomChild: transferState!.bottomChild(),
                status: true,
              );
            },
          ),
        );
        Navigator.of(currentContext).pop(ResultModel(success: true));
      });
    }, onError: (code, message) {
      dismissProgressDialog();
      Timer.periodic(Duration(milliseconds: 100), (timer) async {
        timer.cancel();
        ResultModel resultModel = await Navigator.push(
          currentContext,
          MaterialPageRoute(
            builder: (context) {
              return ResultPage(
                contentChild: transferState!.failChild(failMsg: message),
                bottomChild: transferState!.bottomChild(),
                status: false,
              );
            },
          ),
        );
      });
    });
  }

  UserProfileEntity? searchResult;

  searchUser(String account) {
    if (account == null || account.isEmpty) {
      return;
    }
    if (account.startsWith("0")) {
      account = "6$account";
    }

    FocusScope.of(currentContext).requestFocus(FocusNode());
    Map map = Map();
    map["account"] = account;
    requestNetwork<UserProfileEntity>(Method.post, url: HttpApi.KipleBaseUrl + HttpApi.TransferCheckUser, params: map,
        onSuccess: (data) {
      if (data != null) {
        searchResult = data;
        notifyListeners();
        checkCanCommit();
      }
    }, onError: (code, msg) {
      if (code == 2900002) {
        Timer.periodic(Duration(milliseconds: 100), (timer) {
          timer.cancel();
          showNotDialog('transfer_no_search_result_title'.tr(), 'transfer_no_search_result_content'.tr(),
              'ekyc_hard_reject_button'.tr());
        });
      } else
        showMessage(msg);
    });
  }

  showNotDialog(String title, String content, String confirm, {Function? confirmClickFunction}) {
    showDialog(
        context: currentContext,
        builder: (_) {
          return PopUpDialog(
            title: title,
            content: content,
            confirm: confirm,
            confirmFunction: confirmClickFunction,
          );
        });
  }

  WalletInfoEntity? wallet;

  getBalance() {
    requestNetwork<WalletInfoEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.WalletInfo, onSuccess: (data) {
      wallet = data;
      notifyListeners();
    }, onError: (code, message) {
      Navigator.of(currentContext).pop(ResultModel(success: false));
      showMessage(message);
    });
  }

  @override
  void initState() {
    super.initState();
    getBalance();
  }
}
