import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/personal_details_item_widget.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/transfer/provider/transfer_provider.dart';
import 'package:selangkah_new/Wallet/Services/navigation_service.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/Utils/precision_limit_formatter.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_button.dart';
import 'package:selangkah_new/Wallet/components/talk_to_us.dart';
import 'package:selangkah_new/Wallet/components/visibility.dart';
import 'package:selangkah_new/Wallet/models/merchant_info_entity.dart';
import 'package:selangkah_new/Wallet/models/transfer_result_entity.dart';
import 'package:selangkah_new/Wallet/models/user_profile_entity.dart';
import 'package:selangkah_new/utils/AppColors.dart';
import 'package:selangkah_new/utils/global.dart';

class TransferPage extends StatefulWidget {
  MerchantInfoEntity? merchantInfo;
  UserProfileEntity? userProfile;

  TransferPage({this.merchantInfo}) {
    if (merchantInfo != null) {
      userProfile = UserProfileEntity();
      userProfile?.fullName = merchantInfo?.name;
      userProfile?.mobile = merchantInfo?.mobile;
    }
  }

  @override
  State<StatefulWidget> createState() {
    return TransferState();
  }
}

class TransferState extends State<TransferPage>
    with StateLifeCycle<TransferPage, TransferProvider> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    return AppBarWhite(
      title: "transfer_string".tr(),
      child: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height - (60.h as double),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              VisibilityView(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5.w as double),
                        child: Text(
                          "+6",
                          style: TextStyle(fontSize: 14, color: colorBlack),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(
                              left: 5.w as double,
                              right: 5.w as double,
                              top: 5.h as double),
                          height: 20.h as double,
                          // padding: EdgeInsets.only(left: 4.w, right: 4.w),
                          decoration: BoxDecoration(
                            color: appTextColorGrayBack,
                            borderRadius: BorderRadius.circular(
                              (4.0),
                            ),
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              InkWell(
                                child: IconButton(
                                  padding: EdgeInsets.only(
                                      left: 4.w as double,
                                      right: 12.w as double),
                                  icon: Icon(
                                    Icons.search,
                                    color: appColorPrimary,
                                    size: 20,
                                  ),
                                  onPressed: () {},
                                ),
                                onTap: () {
                                  provider.searchUser(
                                      provider.mobileController.text);
                                },
                              ),
                              Expanded(
                                child: Container(
                                  alignment: Alignment.center,
                                  child: TextField(
                                    cursorColor: appColorPrimaryKiple,
                                    controller: provider.mobileController,
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(0),
                                      hintText: "transfer_phone_number".tr(),
                                      hintStyle: primaryTextStyle(
                                          color: appTextColorGrayhint,
                                          size: 12),
                                      border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.transparent,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.transparent,
                                        ),
                                      ),
                                      disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.transparent,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.transparent,
                                        ),
                                      ),
                                    ),
                                    style: primaryTextStyle(
                                        color: appTextColorPrimary,
                                        size: 12,
                                        textBaseline: TextBaseline.alphabetic),
                                    inputFormatters: [
                                      FilteringTextInputFormatter.digitsOnly
                                    ],
                                    textInputAction: TextInputAction.search,
                                    onSubmitted: (value) {
                                      provider.searchUser(value);
                                    },
                                  ),
                                ),
                              ),
                              InkWell(
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: 6.w as double,
                                      right: 6.w as double),
                                  child: Image.asset(
                                    "assets/images/Wallet/icon_select_container.png",
                                    width: 12.w as double,
                                    height: 12.w as double,
                                  ),
                                ),
                                onTap: () {
                                  GlobalFunction.displayDisclosure(
                                    icon: Icon(
                                      Icons.contacts_outlined,
                                      color: Colors.blue,
                                      size: 30,
                                    ),
                                    title: 'contactPrepaidTitle'.tr(),
                                    description: 'contactPrepaid'.tr(),
                                    key: 'contactPrepaid',
                                  )?.then((value) async {
                                    if (value) {
                                      //function here
                                      if (await Permission.contacts
                                          .request()
                                          .isGranted) {
                                        if (value) {
                                          provider.selectContacter();
                                        }
                                      } else {
                                        return Flushbar(
                                          backgroundColor: Colors.white,
                                          // message: LoremText,
                                          // title: 'Permission needed to continue',
                                          titleText: Text(
                                            'permission_continue'.tr(),
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 15,
                                            ),
                                          ),
                                          messageText: Text(
                                            'continue_permission'.tr(),
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                          mainButton: TextButton(
                                            onPressed:
                                                AppSettings.openAppSettings,
                                            child: Text(
                                              'App Settings',
                                              style:
                                                  TextStyle(color: Colors.blue),
                                            ),
                                          ),
                                          duration: Duration(seconds: 7),
                                        ).show(NavigationService.navigatorKey
                                            .currentState!.overlay!.context);
                                      }
                                    }
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  visibility: widget.userProfile == null
                      ? VisibilityFlag.visible
                      : VisibilityFlag.gone),
              Container(
                height: 9.h as double,
              ),
              ChangeNotifierProvider<TransferProvider>(
                create: (_) => provider,
                child: Consumer<TransferProvider>(
                  builder: (BuildContext? buildContext,
                      TransferProvider? transferProvider, Widget? child) {
                    return VisibilityView(
                      visibility: provider.searchResult == null
                          ? VisibilityFlag.invisible
                          : VisibilityFlag.visible,
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(
                                    left: 5.w as double, right: 9.w as double),
                                child: Image.asset(
                                  'assets/images/Wallet/icon_contactor_default.png',
                                  width: 20.w as double,
                                  height: 20.w as double,
                                ),
                              ),
                              Text(
                                provider.searchResult == null ||
                                        provider.searchResult!.fullName ==
                                            null ||
                                        provider.searchResult!.fullName!.isEmpty
                                    ? ""
                                    : provider.searchResult!.fullName!,
                                style: primaryTextStyle(
                                    size: 16, color: appTextColorPrimary),
                              ),
                            ],
                          ),
                          Container(
                            height: 5.h as double,
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
              Container(
                height: 3.h as double,
                color: appLineColor,
              ),
              Container(
                margin: EdgeInsets.only(
                    left: 5.w as double,
                    top: 6.h as double,
                    bottom: 7.h as double),
                child: Text(
                  "withdrawal_mr".tr(),
                  style: boldTextStyle(
                    size: 16,
                    color: appTextColorGray,
                  ),
                ),
              ),
              Container(
                width: 80.w as double,
                height: 29.h as double,
                child: ChangeNotifierProvider<TransferProvider>(
                  create: (_) => provider,
                  child: Consumer<TransferProvider>(
                    builder: (BuildContext? buildContext,
                        TransferProvider? transferProvider, Widget? child) {
                      return Container(
                        width: 80.w as double,
                        height: 29.h as double,
                        margin: EdgeInsets.only(left: 5.w as double),
                        decoration: BoxDecoration(
                          color: appTextColorGrayBack,
                          borderRadius: BorderRadius.circular(
                            (4.0),
                          ),
                        ),
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(
                            left: 3.w as double, right: 3.w as double),
                        child: TextField(
                          textAlign: TextAlign.center,
                          cursorColor: appColorPrimaryKiple,
                          controller: TextEditingController.fromValue(
                            TextEditingValue(
                              text: '${provider.amount}',
                              selection: TextSelection.fromPosition(
                                TextPosition(
                                    affinity: TextAffinity.downstream,
                                    offset: '${provider.amount}'.length),
                              ),
                            ),
                          ),
                          decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.all(0),
                            hintText: "00.00",
                            hintStyle: boldTextStyle(
                                color: appTextColorGrayhint, size: 25),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                          ),
                          style: boldTextStyle(
                              color: appTextColorPrimary, size: 25),
                          inputFormatters: [
                            PrecisionLimitFormatter(3),
                            FilteringTextInputFormatter(RegExp("[0-9.]"),
                                allow: true),
                          ],
                          onChanged: (string) {
                            provider.changeShowText(string);
                          },
                        ),
                      );
                    },
                  ),
                ),
              ),
              ChangeNotifierProvider<TransferProvider>(
                create: (_) => provider,
                child: Consumer<TransferProvider>(
                  builder: (BuildContext? buildContext,
                      TransferProvider? transferProvider, Widget? child) {
                    return Container(
                      margin: EdgeInsets.only(
                          left: 5.w as double,
                          top: 7.h as double,
                          bottom: 14.h as double),
                      child: Text(
                        "${'balance_string'.tr()}: ${'withdrawal_mr'.tr()} ${provider.wallet == null ? "0.00" : MyTools.dealNumber(double.parse(provider.wallet!.useableBalance!) / 100)}",
                        style: boldTextStyle(
                          size: 14,
                          color: appTextColorGray,
                        ),
                      ),
                    );
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 5.w as double),
                child: Text(
                  "transfer_reason_string".tr(),
                  style: primaryTextStyle(size: 14, color: appTextColorPrimary),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    left: 5.w as double,
                    top: 2.h as double,
                    bottom: 2.h as double),
                child: Text(
                  "transfer_description_string".tr(),
                  style: primaryTextStyle(size: 12, color: appTextColorGray),
                ),
              ),
              Container(
                margin:
                    EdgeInsets.only(left: 5.w as double, right: 5.w as double),
                child: PersonalDetailsItem(
                  titleString: "",
                  hintString: "transfer_type_something".tr(),
                  controller: provider.descriptionController,
                  textColor: appTextColorPrimary,
                  filedEnabled: true,
                  hideRightImage: true,
                  hideLeftWidget: true,
                  spacing: 2.h as double,
                  inputMaxLines: 2,
                  isLins: true,
                  showNoticeTitle: false,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.go,
                  onSubmit: (value) {
                    provider.nextStep();
                  },
                ),
              ),
              Expanded(
                child: Container(),
              ),
              ChangeNotifierProvider<TransferProvider>(
                create: (BuildContext buildContext) => provider,
                child: Consumer<TransferProvider>(
                  builder: (BuildContext? buildContext,
                      TransferProvider? transferProvider, Widget? child) {
                    return Container(
                      margin: EdgeInsets.only(
                        left: 9.w as double,
                        right: 9.w as double,
                        bottom: 25.h as double,
                      ),
                      child: ClickButton(
                        content: "transfer_string".tr(),
                        canClick: provider.canCommit,
                        clickFunction: () {
                          provider.nextStep();
                        },
                        checkFunction: () {
                          provider.checkCommit();
                        },
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  late TransferProvider provider;

  @override
  TransferProvider createProvider(BuildContext buildContext) {
    provider = TransferProvider(buildContext, this, widget.userProfile);
    provider.initListener();
    return provider;
  }

  Widget successChild(TransferResultEntity data) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 5.h as double),
          child: Text(
            "transfer_success_title".tr(),
            style: primaryTextStyle(size: 16, color: appTextColorPrimary),
          ),
        ),
        Text(
          "transfer_success_content".tr(),
          style: primaryTextStyle(size: 14, color: appTextColorGray),
        ),
        Container(
          margin: EdgeInsets.only(top: 7.h as double, bottom: 2.h as double),
          child: Text(
            'withdrawal_receipt_id'.tr().toString(),
            style: primaryTextStyle(size: 14, color: appTextColorGray),
          ),
        ),
        Text(
          data.orderNumber!,
          style: primaryTextStyle(size: 14, color: appTextColorGray),
        ),
        Container(
          margin: EdgeInsets.only(top: 7.h as double, bottom: 7.h as double),
          child: Text(
            MyTools.formatDate(
              DateTime.parse(data.tradeTime!),
            ),
            style: primaryTextStyle(size: 14, color: appTextColorPrimary),
          ),
        ),
        Container(
          height: 3.h as double,
          color: appLineColor,
        ),
        Container(
          padding: EdgeInsets.only(
              top: 6.h as double,
              bottom: 6.h as double,
              left: 15.w as double,
              right: 15.w as double),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    'withdrawal_amount'.tr().toString(),
                    style: primaryTextStyle(size: 16, color: appTextColorGray),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  Text(
                    MyTools.dealNumber(data.amount! / 100),
                    style:
                        primaryTextStyle(size: 16, color: appTextColorPrimary),
                  ),
                ],
              ),
            ],
          ),
        ),
        Container(
          height: 3.h as double,
          color: appLineColor,
        ),
      ],
    );
  }

  Widget failChild({String? failMsg}) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 5.h as double),
          child: Text(
            'withdrawal_fail_title'.tr().toString(),
            style: primaryTextStyle(size: 16, color: appTextColorPrimary),
          ),
        ),
        Text(
          failMsg == null || failMsg.isEmpty
              ? 'withdrawal_fail_content'.tr().toString()
              : failMsg,
          style: primaryTextStyle(size: 14, color: appTextColorGray),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  Widget bottomChild() {
    return Talk2UsWidget(contentString: 'withdrawal_need_help'.tr().toString());
  }
}
