import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Utils/event_bus_tool.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/components/click_button.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';

class ResultPage extends StatelessWidget {
  Widget contentChild;
  Widget? bottomChild;
  bool status;
  String? buttonStr;

  ResultPage({required this.contentChild, this.bottomChild, required this.status, this.buttonStr});

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    return WillPopScope(
        child: Scaffold(
          body: Container(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 40.h as double),
                  child: Image.asset(
                    status
                        ? "assets/images/Wallet/icon_pin_changed_success.png"
                        : "assets/images/WalletEkyc/negative-illustration.png",
                    width: status ? 114.w as double : 120.w as double,
                    height: status ? 69.h as double : 63.h as double,
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.only(top: 14.h as double),
                      child: contentChild,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 11.w as double, right: 11.w as double, top: 10.h as double),
                  child: ClickButton(
                    content: buttonStr == null ? 'ok_string'.tr().toString() : buttonStr!,
                    clickFunction: () {
                      dismiss(context);
                      if (status) {
                        walletEventBus.fire('');
                      }
                    },
                  ),
                ),
                bottomChild == null
                    ? Container(
                        height: 35.h as double,
                      )
                    : Container(
                        height: 35.h as double,
                        margin: EdgeInsets.only(top: 10.h as double),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 10,
                              spreadRadius: 1,
                              color: Colors.black.withOpacity(0.15),
                            ),
                          ],
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10.0),
                            topRight: Radius.circular(10.0),
                          ),
                        ),
                        child: bottomChild,
                      ),
              ],
            ),
          ),
        ),
        onWillPop: () {
          return dismiss(context);
        });
  }

  dismiss(BuildContext buildContext) {
    Navigator.of(buildContext).pop(ResultModel(success: true));
  }
}
