import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/transaction/provider/transaction_list_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/transaction/transaction_detail.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/models/transaction_list_new_entity.dart';

class TransactionHistory extends StatefulWidget {
  @override
  _TransactionHistoryState createState() => _TransactionHistoryState();
}

class _TransactionHistoryState extends State<TransactionHistory>
    with StateLifeCycle<TransactionHistory, TransactionListProvider> {
  TransactionListProvider? transaction;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'transaction_list_title'.tr(),
      child: ChangeNotifierProvider(
        create: (_) {
          return transaction;
        },
        child: Consumer(
          builder: (ctx, TransactionListProvider provider, child) {
            return configBody();
          },
        ),
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget configBody() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        children: [
          configFilter(),
          Expanded(
            child: ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  TransactionListNewList model = transaction!.historyList[index];
                  return configListItem(model);
                },
                itemCount: transaction?.historyList.length),
          )
        ],
      ),
    );
  }

  Widget configFilter() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 15, right: 15),
      height: 40,
      margin: EdgeInsets.only(bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [configFilterItem(0), SizedBox(height: 40, width: 25), configFilterItem(1)],
      ),
    );
  }

  Widget configFilterItem(int type) {
    List<String> titles = [transaction!.currentType, transaction!.currentDate];
    return Expanded(
      child: GestureDetector(
        onTap: () {
          transaction?.clickFilter(type);
        },
        child: Container(
          decoration: BoxDecoration(color: Color(0xffe5e5e5), borderRadius: BorderRadius.circular(5)),
          alignment: Alignment.center,
          padding: EdgeInsets.only(left: 10, right: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                titles[type],
                style: TextStyle(color: Colors.black, fontSize: 12, fontWeight: FontWeight.w400),
              ),
              IconButton(
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: Color(0xFF212121),
                  size: 20,
                ),
                onPressed: () {},
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget configListItem(TransactionListNewList model) {
    String type = '';
    String prefix = '+';
    String userName = model.title ?? '';
    if (userName == '') userName = 'Wallet';
    Color? color;
    if (model.type == 'PAY') {
      type = 'transaction_list_type_payment'.tr();
      color = Color(0xFF212121);
      prefix = '-';
    } else if (model.type == 'TFT') {
      type = 'transaction_list_type_receive'.tr();
      color = Color(0xFF00c717);
    } else if (model.type == 'TP') {
      type = 'transaction_list_type_top_up'.tr();
      color = Color(0xFF00c717);
    } else if (model.type == 'TFF') {
      type = 'transaction_list_type_send'.tr();
      color = Color(0xFF212121);
      prefix = '-';
    } else if (model.type == 'PRE') {
      type = 'transaction_type_prefund'.tr();
      color = Color(0xFF00c717);
    } else if (model.type == 'WD') {
      type = 'transaction_withdrawal'.tr();
      prefix = '-';
      color = Color(0xFF212121);
    } else if (model.type == 'RF') {
      type = 'transaction_refund'.tr();
      color = Color(0xFF00c717);
    } else if (model.type == 'TPAD') {
      type = 'balance_adjustment'.tr();
      color = Color(0xFF212121);
      prefix = '-';
    } else if (model.type == 'TPADOTS') {
      type = 'outstanding_transaction'.tr();
      color = Color(0xFF212121);
      prefix = '';
    }

    return GestureDetector(
      onTap: () {
        Navigator.push(context, new MaterialPageRoute(builder: (_) {
          return TransactionDetail(transactionId: model.orderNo, type: model.type);
        }));
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(25, 15, 25, 15),
        width: double.infinity,
        decoration:
            BoxDecoration(color: Colors.white, border: Border(bottom: BorderSide(width: 1, color: Color(0xFFf5f5f5)))),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '$userName',
                  style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold),
                ),
                Text(
                  '${prefix}RM ${((model.type == 'WD' ? int.parse(model.actualAmount!) + int.parse(model.fee!) : int.parse(model.actualAmount!)) / 100).toStringAsFixed(2)}',
                  style: TextStyle(color: color, fontSize: 14, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  type,
                  style: TextStyle(color: Color(0xFF757575), fontSize: 12),
                ),
                Text(
                  '${MyTools.formatDateInTransaction(model.createTime)}',
                  style: TextStyle(color: Color(0xFF757575), fontSize: 12),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  TransactionListProvider createProvider(BuildContext buildContext) {
    transaction = TransactionListProvider(buildContext);
    transaction?.getListData(true);
    return transaction!;
  }
}
