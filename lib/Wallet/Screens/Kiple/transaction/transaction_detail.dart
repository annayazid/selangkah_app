import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/transaction/provider/transaction_detail_provider.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';

const StatusList = ['', 'In-Progress', 'Successful', 'Failed', 'Cancel'];

class TransactionDetail extends StatefulWidget {
  final transactionId;
  final type;

  TransactionDetail({this.transactionId, this.type});

  @override
  _TransactionDetailState createState() => _TransactionDetailState();
}

class _TransactionDetailState extends State<TransactionDetail>
    with StateLifeCycle<TransactionDetail, TransactionDetailProvider> {
  late TransactionDetailProvider detail;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'transaction_detail_title'.tr(),
      child: ChangeNotifierProvider(
        create: (_) {
          return detail;
        },
        child: Consumer(
          builder: (ctx, TransactionDetailProvider provider, child) {
            return configBody();
          },
        ),
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget configBody() {
    return SingleChildScrollView(
      child: Column(
        children: [configTransactionType(), grayView(), getContent()],
      ),
    );
  }

  Widget getContent() {
    Widget subWidget = Container();
    if (widget.type == "PAY") {
      subWidget = configPayment();
    } else if (widget.type == "TFT") {
      subWidget = configReceive();
    } else if (widget.type == "TP") {
      subWidget = configTopUp();
    } else if (widget.type == "TFF") {
      subWidget = configSend();
    } else if (widget.type == "PRE") {
      subWidget = configPrefund();
    } else if (widget.type == "WD") {
      subWidget = configWithdrawal();
    } else if (widget.type == "RF") {
      subWidget = configRefund();
    } else if (widget.type == "TPAD") {
      subWidget = configBalanceAdjust();
    } else if (widget.type == "TPADOTS") {
      subWidget = configOutstanding();
    }
    return subWidget;
  }

  Widget grayView() {
    return Container(
      width: double.infinity,
      color: Color(0xFFf4f4f4),
      height: 5.0,
    );
  }

  Widget configTransactionType() {
    String type = '';
    Color color = Color(0xFF212121);
    String prefix = '+';
    if (widget.type == "PAY") {
      type = 'transaction_list_type_payment'.tr();
      color = Color(0xFF212121);
      prefix = '-';
    } else if (widget.type == "TFT") {
      type = 'transaction_list_type_receive'.tr();
      color = Color(0xFF00c717);
    } else if (widget.type == "TP") {
      type = 'transaction_list_type_top_up'.tr();
      color = Color(0xFF00c717);
    } else if (widget.type == "TFF") {
      type = 'transaction_list_type_send'.tr();
      color = Color(0xFF212121);
      prefix = '-';
    } else if (widget.type == "PRE") {
      type = 'transaction_type_prefund'.tr();
      color = Color(0xFF00c717);
      prefix = '';
    } else if (widget.type == "WD") {
      type = 'transaction_withdrawal'.tr();
      color = Color(0xFF212121);
      prefix = '-';
    } else if (widget.type == 'RF') {
      type = 'transaction_refund'.tr();
      color = Color(0xFF00c717);
    } else if (widget.type == 'TPAD') {
      type = 'balance_adjustment'.tr();
      color = Color(0xFF212121);
      prefix = '-';
    } else if (widget.type == 'TPADOTS') {
      type = 'outstanding_transaction'.tr();
      color = Color(0xFF212121);
      prefix = '';
    }

    String amount = detail.detailInfo?.actualAmount ?? '0';
    if (detail.detailInfo != null && detail.detailInfo!.actualAmount!.length == 0) {
      amount = detail.detailInfo!.amount!;
    }

    return Container(
      padding: EdgeInsets.fromLTRB(25, 15, 25, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            type,
            style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold),
          ),
          Text(
            '${prefix}RM ${(int.parse(amount) / 100).toStringAsFixed(2)}',
            style: TextStyle(color: color, fontSize: 14, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  Widget configPayment() {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(25, 20, 25, 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_merchant'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${detail.detailInfo?.merchantName ?? ''}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_paid_using'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${detail.detailInfo?.payType ?? ''}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_requested_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.payTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                 Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_completed_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 2) Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.completedTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_status'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 1)
                      Text(
                        'transaction_pending'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 2)
                      Text(
                        'transaction_completed'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 3)
                      Text(
                        'transaction_rejected'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 4)
                      Text(
                        'transaction_cancelled'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'mobile_transacation_id'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          '${detail.detailInfo?.orderNo ?? ''}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_points_used'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          '${detail.detailInfo?.pointUsed == null ? 0 : detail.detailInfo!.pointUsed!.abs()}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_points_earned'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          '${detail.detailInfo?.pointEarned == null ? 0 : detail.detailInfo!.pointEarned!.abs()}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          grayView(),
        ],
      ),
    );
  }

  Widget configReceive() {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(25, 20, 25, 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_receive'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${detail.detailInfo?.transferFullName ?? ''}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_requested_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.payTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                 Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_completed_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 2) Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.completedTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_status'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 1)
                      Text(
                        'transaction_pending'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 2)
                      Text(
                        'transaction_completed'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 3)
                      Text(
                        'transaction_rejected'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 4)
                      Text(
                        'transaction_cancelled'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'mobile_transacation_id'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 15),
                        child: Text(
                          '${detail.detailInfo?.orderNo ?? ''}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          grayView()
        ],
      ),
    );
  }

  Widget configSend() {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(25, 20, 25, 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_send'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${detail.detailInfo?.transferFullName ?? ''}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_requested_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.payTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                 Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_completed_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 2) Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.completedTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_status'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 1)
                      Text(
                        'transaction_pending'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 2)
                      Text(
                        'transaction_completed'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 3)
                      Text(
                        'transaction_rejected'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 4)
                      Text(
                        'transaction_cancelled'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'mobile_transacation_id'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          '${detail.detailInfo?.orderNo ?? ''}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          grayView()
        ],
      ),
    );
  }

  Widget configTopUp() {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(25, 20, 25, 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'auto_top_up_using'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Row(
                      children: [
                        // Image.asset(visaPath, width: 50, fit: BoxFit.fitWidth),
                        Text(
                          '${detail.detailInfo?.payType ?? ""}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        )
                      ],
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_requested_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.payTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                 Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_completed_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 2) Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.completedTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_status'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 1)
                      Text(
                        'transaction_pending'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 2)
                      Text(
                        'transaction_completed'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 3)
                      Text(
                        'transaction_rejected'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 4)
                      Text(
                        'transaction_cancelled'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'mobile_transacation_id'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          '${detail.detailInfo?.orderNo ?? ''}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          grayView()
        ],
      ),
    );
  }

  Widget configPrefund() {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(25, 20, 25, 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_receive'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      'Selangkah',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_requested_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.payTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_completed_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 2) Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.completedTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_status'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 1)
                      Text(
                        'transaction_pending'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 2)
                      Text(
                        'transaction_completed'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 3)
                      Text(
                        'transaction_rejected'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 4)
                      Text(
                        'transaction_cancelled'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'mobile_transacation_id'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          '${detail.detailInfo?.orderNo ?? ''}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          grayView()
        ],
      ),
    );
  }

  Widget configWithdrawal() {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(25, 20, 25, 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Fee".tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      detail.detailInfo?.fee == null
                          ? ''
                          : '- RM ${((int.parse(detail.detailInfo!.fee!)) / 100).toStringAsFixed(2)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_requested_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.payTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                 Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_completed_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 2) Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.completedTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_status'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 1)
                      Text(
                        'transaction_pending'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 2)
                      Text(
                        'transaction_completed'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 3)
                      Text(
                        'transaction_rejected'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 4)
                      Text(
                        'transaction_cancelled'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'mobile_transacation_id'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          '${detail.detailInfo?.orderNo ?? ''}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          grayView()
        ],
      ),
    );
  }

  Widget configRefund() {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(25, 20, 25, 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "transaction_detail_receive".tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      "${detail.detailInfo?.merchantName}",
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_requested_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.payTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                 Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_completed_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 3) Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.completedTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_status'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 0)
                      Text(
                        'transaction_refund_unknown'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 1)
                      Text(
                        'transaction_refund_doing'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 2)
                      Text(
                        'transaction_refund_pass'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 3)
                      Text(
                        'transaction_refund_completed'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 4)
                      Text(
                        'transaction_refund_refuse'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 5)
                      Text(
                        'transaction_refund_cancel'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 6)
                      Text(
                        'transaction_refund_unusual'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'mobile_transacation_id'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          '${detail.detailInfo?.orderNo ?? ''}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_origin_order_no'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          '${detail.detailInfo?.originOrderNo ?? ''}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_refund_points_used'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          '${detail.detailInfo?.pointUsed == null ? 0 : detail.detailInfo!.pointUsed!.abs()}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_refund_points_earned'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          '${detail.detailInfo?.pointEarned == null ? 0 : detail.detailInfo!.pointEarned!.abs()}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          grayView()
        ],
      ),
    );
  }

  Widget configBalanceAdjust() {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(25, 20, 25, 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_merchant'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${detail.detailInfo?.merchantName ?? ''}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_requested_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.payTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                 Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_completed_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 2) Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.completedTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_status'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    if (detail.detailInfo?.status == 1)
                      Text(
                        'transaction_pending'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 2)
                      Text(
                        'transaction_completed'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 3)
                      Text(
                        'transaction_rejected'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                    if (detail.detailInfo?.status == 4)
                      Text(
                        'transaction_cancelled'.tr(),
                        style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                      ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'mobile_transacation_id'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          '${detail.detailInfo?.orderNo ?? ''}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          grayView(),
        ],
      ),
    );
  }

  Widget configOutstanding() {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(25, 20, 25, 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_detail_merchant'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${detail.detailInfo?.merchantName ?? ''}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'transaction_requested_date'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Text(
                      '${MyTools.formatDateInTransaction(detail.detailInfo?.payTime)}',
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'mobile_transacation_id'.tr(),
                      style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          '${detail.detailInfo?.orderNo ?? ''}',
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          grayView(),
        ],
      ),
    );
  }

  @override
  TransactionDetailProvider createProvider(BuildContext buildContext) {
    detail = TransactionDetailProvider(buildContext);
    detail.transactionId = widget.transactionId;
    detail.type = widget.type;
    detail.getListData();
    return detail;
  }
}
