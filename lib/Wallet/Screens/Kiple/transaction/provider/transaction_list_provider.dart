import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/ekyc_bottom_sheet.dart';
import 'package:selangkah_new/Wallet/models/transaction_list_new_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class TransactionListProvider extends BaseProvider {
  TransactionListProvider(BuildContext buildContext) : super(buildContext);

  TransactionListNewEntity? history;
  List<TransactionListNewList> historyList = [];
  int pageNumber = 1;
  List<String> typeList = [
    'transaction_type_all'.tr(),
    'transaction_type_payment'.tr(),
    'transaction_type_topup'.tr(),
    'transaction_type_receive'.tr(),
    'transaction_type_send'.tr(),
    'transaction_type_withdrawal'.tr(),
    'transaction_type_refund'.tr(),
    'transaction_type_prefund'.tr(),
    'transaction_type_balance'.tr(),
    'transaction_type_outstanding'.tr()
  ];

  List<String> typeValue = ['', 'PAY', 'TP', 'TFT', 'TFF', 'WD', 'RF', 'PRE', 'TPAD', 'TPADOTS'];

  List<String> dateList = [
    'transaction_type_today'.tr(),
    'transaction_type_week'.tr(),
    'transaction_type_month'.tr(),
    'transaction_type_three_month'.tr()
  ];

  String currentType = "transaction_type_all".tr();
  String currentDate = "transaction_type_date_title".tr();

  getListData(bool refresh) {
    var para = {'page_no': pageNumber, 'type': typeValue[typeList.indexOf(currentType)]};

    if (handleDateRang()[0].isNotEmpty) {
      para['start_time'] = handleDateRang()[0];
    }

    if (handleDateRang()[1].isNotEmpty) {
      para['end_time'] = handleDateRang()[1];
    }

    requestNetwork<TransactionListNewEntity>(Method.post,
        params: para, url: HttpApi.KipleBaseUrl + HttpApi.TransactionList, onError: (code, msg) {
      showMessage(msg);
    }, onSuccess: (data) {
      history = data;
      if (refresh) {
        historyList.clear();
      }
      historyList.addAll(history?.xList as Iterable<TransactionListNewList>);
      notifyListeners();
    });
  }

  onRefresh() {
    pageNumber = 1;
    getListData(true);
  }

  onLoading() {
    pageNumber++;
    getListData(false);
  }

  clickFilter(int type) {
    List<List<String>> lists = [typeList, dateList];
    List<String> titleList = ['transaction_type_type'.tr(), 'transaction_type_date_title'.tr()];

    showPub(titleList[type], lists[type], (String data) {
      if (type == 0) {
        currentType = data;
      } else if (type == 1) {
        currentDate = data;
      }
      getListData(true);
    });
  }

  showPub(String title, List<dynamic> data, Function itemClick) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        isDismissible: true,
        context: currentContext,
        builder: (BuildContext context) {
          return EkycBottomSheet(title, data, itemClick);
        });
  }

  List<String> handleDateRang() {
    List<String> values = ['', ''];
    if (currentDate == "transaction_type_date_title".tr()) {
      values = ['', ''];
    } else if (currentDate == dateList[0]) {
      DateTime endDate = DateTime.now();
      int year = endDate.year;
      int month = endDate.month;
      int day = endDate.day;
      DateTime startTime = DateTime(year, month, day);
      values = [translateDate(startTime), translateDate(endDate)];
    } else if (currentDate == dateList[1]) {
      DateTime endDate = DateTime.now();
      int week = weekNumber(endDate);
      List<DateTime> range = weekToDayFormat(endDate.year, week);
      values = [translateDate(range[0]), translateDate(endDate)];
    } else if (currentDate == dateList[2]) {
      DateTime endDate = DateTime.now();
      DateTime startDate = getCurrentMonthStartDate();
      values = [translateDate(startDate), translateDate(endDate)];
    } else if (currentDate == dateList[3]) {
      DateTime endDate = DateTime.now();
      DateTime startDate = endDate.subtract(Duration(days: 90));
      values = [translateDate(startDate), translateDate(endDate)];
    }
    return values;
  }

  String translateDate(DateTime date) {
    String formatStr = "yyyy-MM-dd'T'HH:mm:ss";
    DateFormat formatter = DateFormat(formatStr);
    String dateStr = formatter.format(date);
    return dateStr + '+08:00';
  }

  int weekNumber(DateTime date) {
    int dayOfYear = int.parse(DateFormat("D").format(date));
    int woy = ((dayOfYear - date.weekday + 10) / 7).floor();
    if (woy < 1) {
      woy = numOfWeeks(date.year - 1);
    } else if (woy > numOfWeeks(date.year)) {
      woy = 1;
    }
    return woy;
  }

  int numOfWeeks(int year) {
    DateTime dec28 = DateTime(year, 12, 28);
    int dayOfDec28 = int.parse(DateFormat("D").format(dec28));
    return ((dayOfDec28 - dec28.weekday + 10) / 7).floor();
  }

  List<DateTime> weekToDayFormat(int year, int week) {
    int dayMills = 24 * 60 * 60 * 1000;
    int weekMills = 7 * dayMills;
    DateTime dateTime = DateTime(year, 1, 4);
    int targetTimeStamp = dateTime.millisecondsSinceEpoch + (week * 7 - dateTime.weekday) * dayMills;
    DateTime startDate = DateTime.fromMillisecondsSinceEpoch(targetTimeStamp - weekMills + dayMills);
    DateTime endDate = DateTime.fromMillisecondsSinceEpoch(targetTimeStamp + dayMills - 1);
    return [startDate, endDate];
  }

  DateTime getCurrentMonthStartDate() {
    DateTime endDate = DateTime.now();
    int year = endDate.year;
    int month = endDate.month;
    DateTime starTime = DateTime(year, month, 1);
    return starTime;
  }
}
