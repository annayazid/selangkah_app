import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/models/transaction_list_new_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class TransactionDetailProvider extends BaseProvider {
  TransactionDetailProvider(BuildContext buildContext) : super(buildContext);

  var transactionId;
  var type;
  TransactionListNewList? detailInfo;

  getListData() {
    var para = {'order_sn': transactionId, "type": type};
    requestNetwork<TransactionListNewList>(Method.post,
        params: para, url: HttpApi.KipleBaseUrl + HttpApi.TransactionDetail, onError: (code, msg) {
      showMessage(msg);
    }, onSuccess: (data) {
      detailInfo = data;
      notifyListeners();
    });
  }
}
