import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/notification/provider/notification_detail_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/components/appbar_white_result.dart';
import 'package:selangkah_new/Wallet/components/click_button.dart';
import 'package:selangkah_new/Wallet/models/message_model_entity.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class NotificationDetail extends StatefulWidget {
  final MessageModelList data;

  NotificationDetail(this.data);

  @override
  State<StatefulWidget> createState() {
    return NotificationDetailState();
  }
}

class NotificationDetailState extends State<NotificationDetail>
    with StateLifeCycle<NotificationDetail, NotificationDetailProvider> {
  String? currentLanguage = "";

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    currentLanguage = EasyLocalization.of(context)?.locale.languageCode;
    return AppBarWhiteResult(
      title: "notifications_details".tr(),
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.delete_outline,
            color: appColorPrimary,
            size: 25,
          ),
          onPressed: () {
            provider.deleteMessage();
          },
        ),
      ],
      resultModel: ResultModel<String>(
          success: provider.isMarkVisiable,
          data: provider.isMarkVisiable ? widget.data.orderNo : "",
          orderType: "reference"),
      child: Container(
        margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(top: 8.h as double),
              child: Text(
                currentLanguage == "ms"
                    ? provider.data.message_type_malay!.isEmpty
                        ? provider.data.messageType!
                        : provider.data.message_type_malay!
                    : provider.data.messageType!,
                style: boldTextStyle(size: 20, color: appTextColorPrimary),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 4.h as double),
              child: Text(
                MyTools.formatDate(
                  DateTime.parse(provider.data.createTime!),
                ),
                style: primaryTextStyle(size: 12, color: appTextColorGray),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 12.h as double),
              child: Text(
                provider.getShowContent(currentLanguage!)!,
                style: primaryTextStyle(size: 12, color: appTextColorPrimary),
              ),
            ),
            Expanded(
              child: Container(),
            ),
            ChangeNotifierProvider<NotificationDetailProvider>(
              create: (_) => provider,
              child: Consumer<NotificationDetailProvider>(
                builder:
                    (BuildContext? buildContext, NotificationDetailProvider? notificationListProvider, Widget? child) {
                  return Offstage(
                    offstage: provider.hideNextButton(),
                    child: Container(
                      margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double, bottom: 25.h as double),
                      child: ClickButton(
                        content: "notification_view_transaction".tr(),
                        canClick: true,
                        clickFunction: () {
                          provider.viewTransaction();
                        },
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  late NotificationDetailProvider provider;

  @override
  NotificationDetailProvider createProvider(BuildContext buildContext) {
    provider = NotificationDetailProvider(buildContext, widget.data);
    return provider;
  }
}
