import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/notification/provider/notification_list_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class NotificationListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NotificationListState();
  }
}

class NotificationListState extends State<NotificationListPage>
    with StateLifeCycle<NotificationListPage, NotificationListProvider> {
  String? currentLanguage;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    currentLanguage = EasyLocalization.of(context)?.locale.languageCode;
    return AppBarWhite(
      title: "notifications_string".tr(),
      child: ChangeNotifierProvider<NotificationListProvider>(
        create: (_) => provider,
        child: Consumer<NotificationListProvider>(
          builder: (BuildContext? buildContext, NotificationListProvider? notificationListProvider, Widget? child) {
            return RefreshIndicator(
              displacement: 10.0,
              color: appColorPrimary,
              child: ChangeNotifierProvider(
                create: (_) => provider,
                child: Consumer<NotificationListProvider>(
                  builder:
                      (BuildContext? buildContext, NotificationListProvider? notificationListProvider, Widget? child) {
                    return Container(
                      child: ListView.builder(
                        itemBuilder: (context, position) => buildListItem(position),
                        controller: provider.dataList.length >= provider.pageSize ? provider.scrollController : null,
                        itemCount: provider.dataList.length + 1,
                      ),
                    );
                  },
                ),
              ),
              onRefresh: provider.onReference,
            );
          },
        ),
      ),
    );
  }

  late NotificationListProvider provider;

  @override
  NotificationListProvider createProvider(BuildContext buildContext) {
    provider = NotificationListProvider(buildContext);
    return provider;
  }

  Widget buildListItem(int position) {
    if (position == provider.dataList.length) {
      return Offstage(
        offstage: !provider.hasMore,
        child: Container(
          height: 20.h as double,
          alignment: Alignment.center,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                margin: EdgeInsets.only(right: 3.w as double),
                width: 8.h as double,
                height: 8.h as double,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  valueColor: AlwaysStoppedAnimation<Color>(appColorPrimary),
                ),
              ),
              Text(
                "loading_more".tr(),
                style: TextStyle(fontSize: 14, color: appTextColorGray),
              ),
            ],
          ),
        ),
      );
    }
    return Column(
      children: [
        InkWell(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double, top: 7.h as double),
                      child: Text(
                        currentLanguage == "ms"
                            ? provider.dataList[position].message_type_malay!.isEmpty
                                ? provider.dataList[position].messageType!
                                : provider.dataList[position].message_type_malay!
                            : provider.dataList[position].messageType!,
                        style: TextStyle(fontSize: 14, color: appTextColorPrimary),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 7.w as double, right: 7.w as double, bottom: 7.h as double, top: 2.h as double),
                      child: Text(
                        currentLanguage == "ms"
                            ? provider.dataList[position].message_content_malay!.isEmpty
                                ? provider.dataList[position].messageContent!
                                : provider.dataList[position].message_content_malay!
                            : provider.dataList[position].messageContent!,
                        style: TextStyle(fontSize: 14, color: appTextColorPrimary),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
              Offstage(
                offstage: provider.dataList[position].is_read == 1,
                child: Container(
                  margin: EdgeInsets.only(top: 7.h as double, right: 7.w as double),
                  width: 4.h as double,
                  height: 4.h as double,
                  decoration: BoxDecoration(
                    color: appColorPrimary,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
              ),
            ],
          ),
          onTap: () {
            provider.onItemClick(provider.dataList[position]);
          },
        ),
        Container(
          height: 0.8.h as double,
          color: appLineColor,
        ),
      ],
    );
  }
}
