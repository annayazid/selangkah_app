import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/transaction/transaction_detail.dart';
import 'package:selangkah_new/Wallet/models/message_model_entity.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class NotificationDetailProvider extends BaseProvider {
  MessageModelList data;

  NotificationDetailProvider(BuildContext buildContext, this.data) : super(buildContext);

  deleteMessage() {
    Map map = Map();
    map["order_no"] = data.orderNo;
    requestNetwork(Method.post, url: HttpApi.KipleBaseUrl + HttpApi.DeleteMessage, params: map, onSuccess: (back) {
      showMessage("delete_message_notice".tr());
      Timer.periodic(Duration(milliseconds: 100), (timer) {
        timer.cancel();
        Navigator.of(currentContext).pop(ResultModel<String>(success: true, data: data.orderNo, orderType: "delete"));
      });
    }, onError: (code, msg) {
      showMessage(msg);
    });
  }

  viewTransaction() {
    if (data.trade_type != null && data.trade_type!.isNotEmpty) {
      Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
        return TransactionDetail(transactionId: data.orderNo, type: data.trade_type == 'POS' ? 'PAY' : data.trade_type);
      }));
    }
  }

  String? getShowContent(String currentLanguage) {
    if ("WD" == data.trade_type && data.notifyData != null) {
      if (!data.notifyData!.isSuccess())
        return data.notifyData?.reason == null
            ? currentLanguage == "ms"
                ? data.message_content_malay!.isEmpty
                    ? data.messageContent
                    : data.message_content_malay
                : data.messageContent
            : data.notifyData?.reason;
      else
        return currentLanguage == "ms"
            ? data.message_content_malay!.isEmpty
                ? data.messageContent
                : data.message_content_malay
            : data.messageContent;
    } else {
      return currentLanguage == "ms"
          ? data.message_content_malay!.isEmpty
              ? data.messageContent
              : data.message_content_malay
          : data.messageContent;
    }
  }

  bool hideNextButton() {
    if ("WD" == data.trade_type &&
        data.notifyData != null &&
        data.notifyData?.status != null &&
        !data.notifyData!.isSuccess()) {
      return true;
    }
    return data.trade_type == null || data.trade_type!.isEmpty;
  }

  finish() {
    Navigator.of(currentContext).pop(
        ResultModel<String>(success: isMarkVisiable, data: isMarkVisiable ? data.orderNo : "", orderType: "reference"));
  }

  bool isMarkVisiable = false;

  void markVisiable() {
    Map map = Map();
    map["order_no"] = data.orderNo;
    requestNetwork(Method.post, url: HttpApi.KipleBaseUrl + HttpApi.MarkMessageVisiable, params: map,
        onSuccess: (data) {
      isMarkVisiable = true;
    }, onError: (code, msg) {});
  }

  @override
  void initState() {
    super.initState();
    if (data.is_read == 2) {
      markVisiable();
    }
  }
}
