import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/notification/notification_detail.dart';
import 'package:selangkah_new/Wallet/models/message_model_entity.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class NotificationListProvider extends BaseProvider {
  NotificationListProvider(BuildContext buildContext) : super(buildContext) {
    scrollController.addListener(() {
      if (scrollController.position.pixels == scrollController.position.maxScrollExtent && hasMore) {
        loadMore();
      }
    });
  }

  ScrollController scrollController = ScrollController();

  Future<void> onReference() async {
    loadFirstPage(showProgressDialog: false);
  }

  onItemClick(MessageModelList data) async {
    ResultModel result = await Navigator.push(
      currentContext,
      MaterialPageRoute(
        builder: (context) {
          return NotificationDetail(data);
        },
      ),
    );
    if (result != null && result.success!) {
      if ("reference" == result.orderType) {
        markVisiable(result.data);
      } else if ("delete" == result.orderType) {
        deleteMessgae(result.data);
      }
    }
  }

  markVisiable(String orderNumber) {
    dataList.forEach((element) {
      if (element.orderNo == orderNumber) {
        element.is_read = 1;
        notifyListeners();
        return;
      }
    });
  }

  deleteMessgae(String orderNumber) {
    for (int i = 0; i < dataList.length; i++) {
      if (dataList[i].orderNo == orderNumber) {
        dataList.removeAt(i);
        notifyListeners();
        return;
      }
    }
  }

  bool hasMore = true;
  int currentPage = 1;
  int pageSize = 20;
  List<MessageModelList> dataList = [];

  void loadFirstPage({bool showProgressDialog = true}) {
    currentPage = 1;
    Map map = Map();
    map["page_no"] = currentPage;
    map["page_size"] = pageSize;
    requestNetwork<MessageModelEntity>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.MessageList,
        params: map,
        showDialog: showProgressDialog, onSuccess: (data) {
      if (data != null && data.xList != null) {
        dataList.clear();
        dataList.addAll(data.xList as Iterable<MessageModelList>);
        hasMore = (data.total! > dataList.length);
        notifyListeners();
      }
    }, onError: (code, msg) {
      showMessage(msg);
    });
  }

  Future<void> loadMore() async {
    currentPage++;
    Map map = Map();
    map["page_no"] = currentPage;
    map["page_size"] = pageSize;
    requestNetwork<MessageModelEntity>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.MessageList, params: map, showDialog: false, onSuccess: (data) {
      if (data != null) {
        dataList.addAll(data.xList as Iterable<MessageModelList>);
        hasMore = (data.total! > dataList.length);
        notifyListeners();
      }
    }, onError: (code, msg) {
      currentPage--;
      showMessage(msg);
    });
  }

  @override
  void initState() {
    super.initState();
    loadFirstPage();
  }
}
