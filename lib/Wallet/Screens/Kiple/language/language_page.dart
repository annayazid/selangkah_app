import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/language/provider/language_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Main/provider/wallet_home_main_page_provider.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';

class LanguagePage extends StatefulWidget {
  final WalletHomeMainPageProvider? walletHomeMainPageProvider;

  LanguagePage({this.walletHomeMainPageProvider});

  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> with StateLifeCycle<LanguagePage, LanguageProvider> {
  late LanguageProvider languageProvider;

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(microseconds: 0), () {
      String? code = EasyLocalization.of(context)?.locale.languageCode;
      if (code == 'en') {
        languageProvider.languageIndex = 0;
      } else if (code == 'ms') {
        languageProvider.languageIndex = 1;
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'language_page_title'.tr(),
      backgroundColor: Colors.white,
      child: ChangeNotifierProvider(
        create: (BuildContext ctx) {
          return languageProvider;
        },
        child: Consumer(
          builder: (ctx, LanguageProvider provider, child) {
            return configBody();
          },
        ),
      ),
    );
  }

  Widget configBody() {
    return Container(
      padding: EdgeInsets.fromLTRB(15, 40, 15, 0),
      width: double.infinity,
      height: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'language_select_title'.tr(),
            style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold),
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            width: double.infinity,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.15), blurRadius: 5)]),
            child: Column(
              children: [
                configLanguageItem(0),
                Container(
                  width: double.infinity,
                  height: 1,
                  color: Color(0xFFf5f5f5),
                ),
                configLanguageItem(1)
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget configLanguageItem(int index) {
    String selectedImg = 'assets/images/WalletEkyc/select_item.png';
    String unSelectedImg = 'assets/images/WalletEkyc/unselect-item.png';
    String imgPath = '';
    if (languageProvider.languageIndex == index) {
      imgPath = selectedImg;
    } else {
      imgPath = unSelectedImg;
    }

    String languageName = '';
    if (index == 0) {
      languageName = 'English';
    } else {
      languageName = 'Bahasa Melayu';
    }

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        if (languageProvider.languageIndex != index) {
          languageProvider.languageIndex = index;
          resetLanguage();
          setState(() {});
        }
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
        width: double.infinity,
        child: Row(
          children: [
            Image.asset(imgPath, width: 15, height: 15),
            Container(
              margin: EdgeInsets.only(left: 10),
              child: Text(
                languageName,
                style: TextStyle(color: Color(0xFF212121), fontSize: 14),
              ),
            )
          ],
        ),
      ),
    );
  }

  resetLanguage() {
    if (languageProvider.languageIndex == 0) {
      EasyLocalization.of(context)!.setLocale(Locale('en', 'US'));
      languageProvider.sendLanguageData(2);
    } else if (languageProvider.languageIndex == 1) {
      EasyLocalization.of(context)!..setLocale(Locale('ms', 'MY'));
      languageProvider.sendLanguageData(1);
    }
    widget.walletHomeMainPageProvider?.refreshPage();
  }

  @override
  LanguageProvider createProvider(BuildContext buildContext) {
    languageProvider = LanguageProvider(buildContext);
    return languageProvider;
  }
}
