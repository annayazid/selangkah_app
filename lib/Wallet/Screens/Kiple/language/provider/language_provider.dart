import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class LanguageProvider extends BaseProvider {
  LanguageProvider(BuildContext buildContext) : super(buildContext);
  int? languageIndex;

  sendLanguageData(language, {bool showDialog = true}) {
    Map map = Map();
    map["lg"] = language;
    requestNetwork(Method.get,
        showDialog: showDialog,
        url: HttpApi.KipleBaseUrl + HttpApi.languageSwitch + '?lg=$language', onSuccess: (data) {
      if (data != null) {}
    }, onError: (code, msg) {
      showMessage(msg);
    });
  }
}
