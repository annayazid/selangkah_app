import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/models/auto_top_up_config_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class AutoTopUpEditProvider extends BaseProvider {
  AutoTopUpEditProvider(BuildContext buildContext) : super(buildContext);
  AutoTopUpConfigEntity? config;

  editConfig() {
    var para = {
      'trigger_button': config?.triggerButton,
      'card_number': config?.cardNumber,
      'topup_min_limit': config?.topupMinLimit,
      'balance_limit': config?.balanceLimit,
      'card_type': config?.type
    };
    requestNetwork(Method.post, params: para, url: HttpApi.KipleBaseUrl + HttpApi.AutoTopUpEdit, onError: (code, msg) {
      showMessage(msg);
    }, onSuccess: (data) {
      showMessage('auto_top_up_edit_alert'.tr());
    });
  }
}
