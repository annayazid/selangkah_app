import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/cards/card_online.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/cards/e_cards_list.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/cards/pop_up_alert.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/topup/auto_top_up.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/topup/auto_topup_edit.dart';
import 'package:selangkah_new/Wallet/models/auto_top_up_config_entity.dart';
import 'package:selangkah_new/Wallet/models/card_manager_entity.dart';
import 'package:selangkah_new/Wallet/models/wallet_Info_entity.dart';
import 'package:selangkah_new/Wallet/net/net.dart';

class TopUpProvider extends BaseProvider {
  TopUpProvider(BuildContext buildContext) : super(buildContext);

  int? selectMoney;
  String amount = '';
  AutoTopUpConfigEntity? config;
  WalletInfoEntity? walletInfo;
  bool? fromPayment;

  getWalletData() {
    requestNetwork<WalletInfoEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.WalletInfo, onSuccess: (data) {
      walletInfo = data;
      notifyListeners();
    }, onError: (code, message) {
      showMessage(message);
    });
  }

  getAutoTopUpInfo() {
    requestNetwork<AutoTopUpConfigEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.AutoTopUpConfig,
        onError: (code, msg) {
      // showMessage(msg);
      gotoCreateConfig();
    }, onSuccess: (data) {
      config = data;
      gotoEditPage();
    });
  }

  gotoCreateConfig() {
    Future.delayed(Duration(microseconds: 0), () {
      Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
        return AutoTopUp(isCreate: true);
      }));
    });
  }

  gotoEditPage() {
    Future.delayed(Duration(microseconds: 0), () {
      Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
        return AutoTopUpEdit(config: config!);
      }));
    });
  }

  getCardManagerData() {
    if (!checkAmount()) {
      return;
    }

    var para = {'amount': '${(double.parse(amount) * 100).toInt()}'};
    requestNetwork<CardManagerEntity>(Method.post, params: para, url: HttpApi.KipleBaseUrl + HttpApi.CardPayment,
        onError: (code, msg) {
      showMessage(msg);
    }, onSuccess: (data) {
      Future.delayed(Duration(microseconds: 0), () {
        Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
          return CardOnline(url: data.url, fromPayment: fromPayment!);
        }));
      });
    });
  }

  methodClick(int index) {
    if (index == 0) {
      getCardManagerData();
    } else if (index == 1) {
      if (!checkAmount()) {
        return;
      }
      Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
        return ECardsList(amount: amount);
      }));
    }
  }

  bool checkAmount() {
    if (amount.length == 0) {
      showMessage('top_up_alert'.tr());
      return false;
    }

    if (double.parse(amount) < 20) {
      showMessage('${'top_up_amount_desc'.tr()} 20.00');
      return false;
    }

    if (walletInfo == null) {
      return false;
    }

    if (int.parse(walletInfo!.maxBalanceLimit!) < int.parse(walletInfo!.useableBalance!) + double.parse(amount) * 100) {
      showDialog(
          context: currentContext,
          builder: (_) {
            return PopUpAlert(
                title: 'top_up_limit_title'.tr(),
                content: "top_up_limit_alert".tr(),
                confirm: "bill_my_billers_delete_confirm".tr(),
                isCancel: false);
          });
      return false;
    }
    return true;
  }

  onChangeAmount(String text) {
    if (text.length > 0) {
      if (text.length == 1) {
        amount = (double.parse(text) / 100).toStringAsFixed(2);
      } else {
        double newValue = double.parse(text);
        if (newValue == 0) {
          amount = "";
        } else {
          if (text.substring(text.lastIndexOf(".") + 1, text.length).length == 2 &&
              amount.substring(amount.lastIndexOf(".") + 1, amount.length).length == 1) {
            amount = text;
          } else if (!text.endsWith(".") &&
              text.contains(".") &&
              (text.substring(text.lastIndexOf(".") + 1, text.length).length >= 1 &&
                  amount.substring(amount.lastIndexOf(".") + 1, amount.length).length >= 1) &&
              (double.parse(text.substring(0, text.length - 1)) == double.parse(amount) ||
                  double.parse(text) == double.parse(amount.substring(0, amount.length - 1)))) {
            if (text.length > amount.length) {
              amount = (newValue * 10).toStringAsFixed(2);
            } else if (text.length < amount.length) {
              amount = (newValue / 10).toStringAsFixed(2);
            }
          } else {
            amount = text;
          }
        }
      }
    } else {
      amount = '';
    }
    notifyListeners();
  }
}
