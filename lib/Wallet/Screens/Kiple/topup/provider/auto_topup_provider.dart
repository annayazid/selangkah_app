import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/cards/card_online.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/models/auto_top_up_config_entity.dart';
import 'package:selangkah_new/Wallet/models/card_list_entity.dart';
import 'package:selangkah_new/Wallet/models/card_manager_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class AutoTopUpProvider extends BaseProvider {
  AutoTopUpProvider(BuildContext buildContext) : super(buildContext);

  int? selectMoney;
  String? amount = '';
  String? belowAmount = '';
  CardListEntity? cardList;
  int? cardIndex;
  AutoTopUpConfigEntity? config;
  bool? isCreate;

  initEditData() {
    amount = '${config!.topupMinLimit! / 100}';
    belowAmount = '${config!.balanceLimit! / 100}';
    List<int> moneys = [20, 50, 100];
    selectMoney = moneys.indexOf(double.parse('${config!.balanceLimit! / 100}').toInt());
  }

  getCardListData() {
    requestNetwork<CardListEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.CardsList, onError: (code, msg) {
      showMessage(msg);
    }, onSuccess: (data) {
      cardList = data;
      if (!isCreate!) {
        cardList?.cards?.forEach((element) {
          if (element.maskedPan == config?.cardNumber) {
            cardIndex = cardList?.cards?.indexOf(element);
          }
        });
      }
      notifyListeners();
    });
  }

  bool checkIsEnable() {
    if (amount!.trim().length > 0 && belowAmount!.trim().length > 0 && cardIndex != null) {
      return true;
    } else {
      return false;
    }
  }

  getCardManagerData() {
    requestNetwork<CardManagerEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.CardManager,
        onError: (code, msg) {
      showMessage(msg);
    }, onSuccess: (data) {
      Future.delayed(Duration(microseconds: 0), () {
        Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
          return CardOnline(url: data.url, addCard: true);
        })).then((value) {
          getCardListData();
        });
      });
    });
  }

  createConfig() {
    if (amount?.length == 0) {
      showMessage('top_up_alert'.tr());
      return;
    }

    if (double.parse(amount!) < 20) {
      showMessage('${'top_up_amount_desc'.tr()} 20.00');
      return;
    }

    if (double.parse(amount!) < double.parse(belowAmount!)) {
      showMessage('The top up amount must be equal or greater than the threshold amount.');
      return;
    }

    var para = {
      'trigger_button': 1,
      'card_number': cardList!.cards![cardIndex!].maskedPan!,
      'topup_min_limit': int.parse(MyTools.dealNumber(double.parse(amount!) * 100.toInt(), pointLength: 0)),
      'balance_limit': int.parse(MyTools.dealNumber(double.parse(belowAmount!) * 100.toInt(), pointLength: 0)),
      'card_type': cardList!.cards![cardIndex!].scheme
    };
    requestNetwork(Method.post, params: para, url: HttpApi.KipleBaseUrl + HttpApi.AutoTopUpCreate,
        onError: (code, msg) {
      showMessage(msg);
    }, onSuccess: (data) {
      showMessage('auto_top_up_save_alert'.tr());
      Navigator.pop(currentContext);
    });
  }

  editConfig() {
    if (amount?.length == 0) {
      showMessage('top_up_alert'.tr());
      return;
    }

    if (double.parse(amount!) < 20) {
      showMessage('${'top_up_amount_desc'.tr()} 20.00');
      return;
    }

    if (double.parse(amount!) < double.parse(belowAmount!)) {
      showMessage('auto_top_up_amount_desc'.tr());
      return;
    }

    var para = {
      'trigger_button': config?.triggerButton,
      'card_number': cardList?.cards![cardIndex!].maskedPan,
      'topup_min_limit': (double.parse(amount!) * 100).toInt(),
      'balance_limit': (double.parse(belowAmount!) * 100).toInt(),
      'card_type': cardList?.cards![cardIndex!].scheme
    };
    requestNetwork(Method.post, params: para, url: HttpApi.KipleBaseUrl + HttpApi.AutoTopUpEdit, onError: (code, msg) {
      showMessage(msg);
    }, onSuccess: (data) {
      showMessage('auto_top_up_edit_alert'.tr());
      Navigator.pop(currentContext);
      Navigator.pop(currentContext);
    });
  }

  onChangeAmount(String text) {
    if (text.length > 0) {
      if (text.length == 1) {
        amount = (double.parse(text) / 100).toStringAsFixed(2);
      } else {
        double newValue = double.parse(text);
        if (newValue == 0) {
          amount = "";
        } else {
          if (text.substring(text.lastIndexOf(".") + 1, text.length).length == 2 &&
              amount!.substring(amount!.lastIndexOf(".") + 1, amount?.length).length == 1) {
            amount = text;
          } else if (!text.endsWith(".") &&
              text.contains(".") &&
              (text.substring(text.lastIndexOf(".") + 1, text.length).length >= 1 &&
                  amount!.substring(amount!.lastIndexOf(".") + 1, amount!.length).length >= 1) &&
              (double.parse(text.substring(0, text.length - 1)) == double.parse(amount!) ||
                  double.parse(text) == double.parse(amount!.substring(0, amount!.length - 1)))) {
            if (text.length > amount!.length) {
              amount = (newValue * 10).toStringAsFixed(2);
            } else if (text.length < amount!.length) {
              amount = (newValue / 10).toStringAsFixed(2);
            }
          } else {
            amount = text;
          }
        }
      }
    } else {
      amount = '';
    }
    notifyListeners();
  }

  onChangeBelowAmount(String text) {
    if (text.length > 0) {
      if (text.length == 1) {
        belowAmount = (double.parse(text) / 100).toStringAsFixed(2);
      } else {
        double newValue = double.parse(text);
        if (newValue == 0) {
          belowAmount = "";
        } else {
          if (text.substring(text.lastIndexOf(".") + 1, text.length).length == 2 &&
              belowAmount!.substring(belowAmount!.lastIndexOf(".") + 1, belowAmount!.length).length == 1) {
            belowAmount = text;
          } else if (!text.endsWith(".") &&
              text.contains(".") &&
              (text.substring(text.lastIndexOf(".") + 1, text.length).length >= 1 &&
                  belowAmount!.substring(belowAmount!.lastIndexOf(".") + 1, belowAmount!.length).length >= 1) &&
              (double.parse(text.substring(0, text.length - 1)) == double.parse(belowAmount!) ||
                  double.parse(text) == double.parse(belowAmount!.substring(0, belowAmount!.length - 1)))) {
            if (text.length > belowAmount!.length) {
              belowAmount = (newValue * 10).toStringAsFixed(2);
            } else if (text.length < belowAmount!.length) {
              belowAmount = (newValue / 10).toStringAsFixed(2);
            }
          } else {
            belowAmount = text;
          }
        }
      }
    } else {
      belowAmount = '';
    }
    notifyListeners();
  }
}
