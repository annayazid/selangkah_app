import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/topup/auto_top_up.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/topup/provider/auto_edit_provider.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_item.dart';
import 'package:selangkah_new/Wallet/models/auto_top_up_config_entity.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class AutoTopUpEdit extends StatefulWidget {
  final AutoTopUpConfigEntity config;

  AutoTopUpEdit({required this.config});

  @override
  _AutoTopUpEditState createState() => _AutoTopUpEditState();
}

class _AutoTopUpEditState extends State<AutoTopUpEdit> with StateLifeCycle<AutoTopUpEdit, AutoTopUpEditProvider> {
  bool activate_ = true;
  AutoTopUpEditProvider? editProvider;

  @override
  void initState() {
    super.initState();
    activate_ = widget.config.triggerButton == 1;
  }

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'top_up_title'.tr(),
      child: configBody(),
      backgroundColor: Colors.white,
    );
  }

  Widget configBody() {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            children: [configActivate(), grayView(), configInfo()],
          ),
        ),
        Positioned(
            child: NormalClickItem(
              content: 'auto_top_up_button_edit'.tr(),
              clickFunction: () {
                Navigator.push(context, new MaterialPageRoute(builder: (_) {
                  return AutoTopUp(isCreate: false, config: widget.config);
                }));
              },
            ),
            left: 30,
            right: 30,
            bottom: 30)
      ],
    );
  }

  Widget grayView({height = 5.0}) {
    return Container(width: double.infinity, height: height, color: Color(0xFFf4f4f4));
  }

  Widget configActivate() {
    return Container(
      padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'auto_top_up_activate'.tr(),
            style: TextStyle(color: Color(0xFF212121), fontWeight: FontWeight.bold, fontSize: 14),
          ),
          Switch(
              value: activate_,
              onChanged: (value) {
                activate_ = value;
                widget.config.triggerButton = activate_ ? 1 : 2;
                editProvider?.editConfig();
                setState(() {});
              },
              activeColor: appColorPrimaryKiple)
        ],
      ),
    );
  }

  Widget configInfo() {
    String visaPath = 'assets/images/Wallet/visa_card.png';
    String masterPath = 'assets/images/Wallet/master_card.png';

    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'auto_top_up_amount'.tr(),
            style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
          ),
          Text(
            'RM ${(widget.config.topupMinLimit!.toInt() / 100).toStringAsFixed(2)}',
            style: TextStyle(color: Color(0xFF212121), fontSize: 16, height: 1.5),
          ),
          SizedBox(height: 15),
          Text(
            'auto_top_up_below'.tr(),
            style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
          ),
          Text(
            'RM ${(widget.config.balanceLimit!.toInt() / 100).toStringAsFixed(2)}',
            style: TextStyle(color: Color(0xFF212121), fontSize: 16, height: 1.5),
          ),
          SizedBox(height: 15),
          Text(
            'auto_top_up_using'.tr(),
            style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5),
          ),
          SizedBox(height: 10),
          Row(
            children: [
              Image.asset(widget.config.type == 'VISA' ? visaPath : masterPath, width: 50, fit: BoxFit.fitWidth),
              Container(
                margin: EdgeInsets.only(right: 10),
                child: Text(
                  '${widget.config.cardNumber}',
                  style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  AutoTopUpEditProvider createProvider(BuildContext buildContext) {
    editProvider = AutoTopUpEditProvider(buildContext);
    editProvider?.config = widget.config;
    return editProvider!;
  }
}
