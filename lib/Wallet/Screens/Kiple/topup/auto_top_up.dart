import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/topup/provider/auto_topup_provider.dart';
import 'package:selangkah_new/Wallet/Utils/precision_limit_formatter.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_item.dart';
import 'package:selangkah_new/Wallet/models/auto_top_up_config_entity.dart';
import 'package:selangkah_new/Wallet/models/card_list_entity.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class AutoTopUp extends StatefulWidget {
  final bool isCreate;
  final AutoTopUpConfigEntity? config;

  AutoTopUp({required this.isCreate, this.config});

  @override
  _AutoTopUpState createState() => _AutoTopUpState();
}

class _AutoTopUpState extends State<AutoTopUp> with StateLifeCycle<AutoTopUp, AutoTopUpProvider> {
  AutoTopUpProvider? auto;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'auto_top_up_title'.tr(),
      child: ChangeNotifierProvider(
        create: (_) {
          return auto;
        },
        child: Consumer(
          builder: (ctx, AutoTopUpProvider provider, child) {
            return configBody();
          },
        ),
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget configBody() {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            child: SingleChildScrollView(
              child: Column(
                children: [configTopView(), configMoneySelect(), configAmount(), configBelowAmount(), configUseCard()],
              ),
            ),
          ),
          Positioned(
              child: NormalClickItem(
                content: 'auto_top_up_button_save'.tr(),
                enable: auto!.checkIsEnable(),
                clickFunction: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  if (widget.isCreate) {
                    auto?.createConfig();
                  } else {
                    auto?.editConfig();
                  }
                },
              ),
              left: 30,
              right: 30,
              bottom: 30)
        ],
      ),
    );
  }

  Widget configTopView() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(right: 90),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'auto_top_up_desc_title'.tr(),
                    style: TextStyle(color: Color(0xFF212121), fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 8),
                  Text(
                    'auto_top_up_desc_content'.tr(),
                    style: TextStyle(color: Color(0xFF757575), fontSize: 12, height: 1.5),
                  ),
                ],
              ),
            ),
          ),
          Image.asset('assets/images/Wallet/auto_topup_llustration.png', width: 60, height: 60)
        ],
      ),
    );
  }

  Widget configMoneySelect() {
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'auto_top_up_amount_title'.tr(),
            style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [configMoneyItem(0), configMoneyItem(1), configMoneyItem(2)],
          )
        ],
      ),
    );
  }

  Widget configMoneyItem(int index) {
    bool selected = auto?.selectMoney == index;
    List<String> items = ['RM 20', 'RM 50', 'RM 100'];
    List<String> moneys = ['20', '50', '100'];

    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        auto?.selectMoney = index;
        auto?.amount = moneys[index];
        setState(() {});
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        height: 40,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: !selected ? Color(0xFF757575) : appColorPrimaryKiple, width: 1),
        ),
        child: Text(
          '${items[index]}',
          style: TextStyle(color: !selected ? Color(0xFF757575) : appColorPrimaryKiple, fontSize: 16),
        ),
      ),
    );
  }

  Widget configAmount() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'top_up_amount'.tr(),
            style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold),
          ),
          Text(
            '${'top_up_amount_desc'.tr()} 20',
            style: TextStyle(color: Color(0xFF757575), fontSize: 12, height: 1.5),
          ),
          configInputAmount()
        ],
      ),
    );
  }

  Widget configInputAmount() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      padding: EdgeInsets.only(left: 10, right: 10),
      decoration: BoxDecoration(
        color: Color(0xFFf4f4f4),
        borderRadius: BorderRadius.circular(3),
      ),
      child: TextField(
        textAlign: TextAlign.start,
        controller: TextEditingController.fromValue(
          TextEditingValue(
            text: '${auto?.amount}',
            selection: TextSelection.fromPosition(
              TextPosition(affinity: TextAffinity.downstream, offset: '${auto?.amount}'.length),
            ),
          ),
        ),
        // keyboardType: TextInputType.number,
        inputFormatters: [
          PrecisionLimitFormatter(3),
          FilteringTextInputFormatter(RegExp("[0-9.]"), allow: true),
        ],
        style: TextStyle(color: Color(0xFF212121), fontSize: 12),
        decoration: InputDecoration(
          border: InputBorder.none,
          isCollapsed: true,
          contentPadding: EdgeInsets.only(top: 10, bottom: 10),
          hintText: 'RM 0.00',
          hintStyle: TextStyle(color: Color(0xFF999999), fontSize: 12),
        ),
        onChanged: (String text) {
          auto?.onChangeAmount(text);
        },
      ),
    );
  }

  Widget configBelowAmount() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.fromLTRB(15, 25, 15, 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'auto_top_up_below_title'.tr(),
            style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold),
          ),
          configBelowInputAmount()
        ],
      ),
    );
  }

  Widget configBelowInputAmount() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      padding: EdgeInsets.only(left: 10, right: 10),
      decoration: BoxDecoration(
        color: Color(0xFFf4f4f4),
        borderRadius: BorderRadius.circular(3),
      ),
      child: TextField(
        textAlign: TextAlign.start,
        controller: TextEditingController.fromValue(
          TextEditingValue(
            text: '${auto?.belowAmount}',
            selection: TextSelection.fromPosition(
              TextPosition(affinity: TextAffinity.downstream, offset: '${auto?.belowAmount}'.length),
            ),
          ),
        ),
        // keyboardType: TextInputType.number,
        inputFormatters: [PrecisionLimitFormatter(3), FilteringTextInputFormatter(RegExp("[0-9.]"), allow: true)],
        style: TextStyle(color: Color(0xFF212121), fontSize: 12),
        decoration: InputDecoration(
          border: InputBorder.none,
          isCollapsed: true,
          contentPadding: EdgeInsets.only(top: 10, bottom: 10),
          hintText: 'RM 0.00',
          hintStyle: TextStyle(color: Color(0xFF999999), fontSize: 12),
        ),
        onChanged: (String text) {
          auto?.onChangeBelowAmount(text);
        },
      ),
    );
  }

  Widget configUseCard() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
      margin: EdgeInsets.only(bottom: 100),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'auto_top_up_using_title'.tr(),
            style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold),
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.15), blurRadius: 5)]),
            child: Column(
              children: getSubList(),
            ),
          ),
          configAddCard()
        ],
      ),
    );
  }

  List<Widget> getSubList() {
    List<Widget> widgets = [];
    if (auto?.cardList != null && auto?.cardList?.cards != null && auto!.cardList!.cards!.length > 0) {
      widgets = auto!.cardList!.cards!.map((CardListCard card) {
        return configCardItem(card);
      }).toList();
    }
    return widgets;
  }

  Widget configCardItem(CardListCard card) {
    String visaPath = 'assets/images/Wallet/visa_card.png';
    String masterPath = 'assets/images/Wallet/master_card.png';
    String selectedImg = 'assets/images/WalletEkyc/select_item.png';
    String unSelectedImg = 'assets/images/WalletEkyc/unselect-item.png';
    int index = auto!.cardList!.cards!.indexOf(card);

    return GestureDetector(
      onTap: () {
        if (auto?.cardIndex != index) {
          auto?.cardIndex = index;
          setState(() {});
        }
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            bottom: BorderSide(
              width: 1,
              color: Color(0xFFf5f5f5),
            ),
          ),
        ),
        child: Row(
          children: [
            Image.asset(auto?.cardIndex == index ? selectedImg : unSelectedImg, width: 15, height: 15),
            Image.asset(card.scheme == 'VISA' ? visaPath : masterPath, width: 50, fit: BoxFit.fitWidth),
            Text(
              '${card.maskedPan}',
              style: TextStyle(color: Color(0xFF212121), fontSize: 16),
            )
          ],
        ),
      ),
    );
  }

  Widget configAddCard() {
    return GestureDetector(
      onTap: () {
        auto?.getCardManagerData();
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
        width: double.infinity,
        decoration: BoxDecoration(color: Colors.white),
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(right: 5),
              child: Icon(
                Icons.add,
                color: appColorPrimaryKiple,
                size: 30,
              ),
            ),
            Text(
              'auto_top_up_add_card'.tr(),
              style: TextStyle(color: Color(0xFF212121), fontSize: 16),
            )
          ],
        ),
      ),
    );
  }

  @override
  AutoTopUpProvider createProvider(BuildContext buildContext) {
    auto = AutoTopUpProvider(buildContext);
    auto?.isCreate = widget.isCreate;
    if (!widget.isCreate) {
      auto?.config = widget.config;
      auto?.initEditData();
    }
    auto?.getCardListData();
    return auto!;
  }
}
