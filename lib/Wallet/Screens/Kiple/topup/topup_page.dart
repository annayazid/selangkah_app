import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/topup/provider/topup_provider.dart';
import 'package:selangkah_new/Wallet/Utils/precision_limit_formatter.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class TopUpPage extends StatefulWidget {
  final bool fromPayment;

  TopUpPage({this.fromPayment = false});

  @override
  _TopUpPageState createState() => _TopUpPageState();
}

class _TopUpPageState extends State<TopUpPage> with StateLifeCycle<TopUpPage, TopUpProvider> {
  TopUpProvider? topUp;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'top_up_title'.tr(),
      child: ChangeNotifierProvider(
        create: (context) {
          return topUp;
        },
        child: Consumer(
          builder: (ctx, TopUpProvider provider, child) {
            return configBody();
          },
        ),
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget configBody() {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Container(
        child: Column(
          children: [configAutoTopUp(), configGrayView(), configMoneySelect(), configAmount(), configMethod()],
        ),
      ),
    );
  }

  Widget configGrayView({double height = 5.0}) {
    return Container(width: double.infinity, height: height, color: Color(0xFFf4f4f4));
  }

  Widget configAutoTopUp() {
    return GestureDetector(
      onTap: () {
        log('auto top up');
        topUp?.getAutoTopUpInfo();
      },
      child: Container(
        padding: EdgeInsets.all(15),
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'top_up_auto'.tr(),
              style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold),
            ),
            Icon(
              Icons.chevron_right,
              color: appColorPrimaryKiple,
            )
          ],
        ),
      ),
    );
  }

  Widget configMoneySelect() {
    return Container(
      padding: EdgeInsets.all(15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          configMoneyItem(0),
          configMoneyItem(1),
          configMoneyItem(2),
        ],
      ),
    );
  }

  Widget configMoneyItem(int index) {
    bool selected = topUp?.selectMoney == index;
    List<String> items = ['RM 20', 'RM 50', 'RM 100'];
    List<String> moneys = ['20', '50', '100'];

    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        topUp?.selectMoney = index;
        topUp?.amount = double.parse(moneys[index]).toStringAsFixed(2);
        setState(() {});
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        height: 40,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: !selected ? Color(0xFF757575) : appColorPrimaryKiple, width: 1),
        ),
        child: Text(
          '${items[index]}',
          style: TextStyle(color: !selected ? Color(0xFF757575) : appColorPrimaryKiple, fontSize: 16),
        ),
      ),
    );
  }

  Widget configAmount() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'top_up_amount'.tr(),
            style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold),
          ),
          Text(
            '${'top_up_amount_desc'.tr()} 20.00',
            style: TextStyle(color: Color(0xFF757575), fontSize: 12, height: 1.5),
          ),
          configInputAmount()
        ],
      ),
    );
  }

  Widget configInputAmount() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      padding: EdgeInsets.only(left: 10, right: 10),
      decoration: BoxDecoration(
        color: Color(0xFFf4f4f4),
        borderRadius: BorderRadius.circular(3),
      ),
      child: TextField(
        textAlign: TextAlign.start,
        controller: TextEditingController.fromValue(
          TextEditingValue(
            text: '${topUp?.amount}',
            selection: TextSelection.fromPosition(
              TextPosition(affinity: TextAffinity.downstream, offset: '${topUp?.amount}'.length),
            ),
          ),
        ),
        // keyboardType: TextInputType.number,
        inputFormatters: [
          PrecisionLimitFormatter(3),
          FilteringTextInputFormatter(RegExp("[0-9.]"), allow: true),
        ],
        style: TextStyle(color: Color(0xFF212121), fontSize: 12),
        decoration: InputDecoration(
            border: InputBorder.none,
            isCollapsed: true,
            contentPadding: EdgeInsets.only(top: 8, bottom: 8),
            hintText: 'top_up_amount_place'.tr(),
            hintStyle: TextStyle(color: Color(0xFF999999), fontSize: 12)),
        onChanged: (String text) {
          topUp?.onChangeAmount(text);
        },
      ),
    );
  }

  Widget configMethod() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.fromLTRB(15, 15, 15, 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('top_up_method'.tr(),
              style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold)),
          SizedBox(height: 10),
          configMethodItem(0),
          configGrayView(height: 1.0),
          configMethodItem(1)
        ],
      ),
    );
  }

  Widget configMethodItem(int index) {
    List<String> titles = ['top_up_online'.tr(), 'top_up_credit'.tr()];

    return GestureDetector(
      onTap: () {
        log(index);
        topUp?.methodClick(index);
      },
      child: Container(
        padding: EdgeInsets.only(top: 10, bottom: 10),
        width: double.infinity,
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(titles[index], style: TextStyle(color: Color(0xFF212121), fontSize: 14)),
            Icon(
              Icons.chevron_right,
              color: appColorPrimaryKiple,
            )
          ],
        ),
      ),
    );
  }

  @override
  TopUpProvider createProvider(BuildContext buildContext) {
    topUp = TopUpProvider(buildContext);
    topUp?.fromPayment = widget.fromPayment;
    topUp?.getWalletData();
    return topUp!;
  }
}
