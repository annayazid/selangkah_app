import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/language/language_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/loyalty/loyalty_list.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/notification/notification_list.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/pin/input_pin.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/topup/topup_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/withdrawal/withdrawal_page.dart';
import 'package:selangkah_new/Wallet/Screens/Main/provider/wallet_home_main_page_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/wallet/provider/my_wallet_page_provider.dart';
import 'package:selangkah_new/Wallet/Utils/event_bus_tool.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class MyWalletPage extends StatefulWidget {
  final WalletHomeMainPageProvider? walletHomeProvider;

  MyWalletPage({this.walletHomeProvider});

  @override
  _MyWalletPageState createState() => _MyWalletPageState();
}

class _MyWalletPageState extends State<MyWalletPage> with StateLifeCycle<MyWalletPage, MyWalletProvider> {
  List<String>? itemTitles;
  late MyWalletProvider provider;
  StreamSubscription? walletSubscription;

  @override
  void initState() {
    super.initState();
    configData();
    walletSubscription = walletEventBus.on().listen((event) {
      provider.initData(showDialog: false);
    });
  }

  configData() {
    itemTitles = [
      "wallet_item_notifications".tr().toString(),
      "wallet_item_card".tr().toString(),
      "wallet_item_loyalty".tr().toString(),
      // "wallet_item_prepaid".tr().toString(),
      "wallet_item_pin".tr().toString(),
      // "wallet_item_language".tr().toString(),
      "wallet_item_withdrawal".tr().toString()
    ];
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);
    configData();
    return AppBarWhite(
      title: 'wallet_my_title'.tr().toString(),
      backgroundColor: Colors.white,
      child: ChangeNotifierProvider(
        create: (_) {
          return provider;
        },
        child: Consumer(
          builder: (ctx, MyWalletProvider provider, child) {
            return configBody();
          },
        ),
      ),
    );
  }

  Widget configBody() {
    return SingleChildScrollView(
      child: Column(
        children: [configWalletInfo(), configFunction()],
      ),
    );
  }

  Widget configWalletInfo() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 15, 20, 35),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.05), blurRadius: 15)]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(15, 20, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('wallet_my_balance'.tr().toString(),
                    style: TextStyle(color: Color(0xFF343434), fontWeight: FontWeight.bold, fontSize: 13)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Container(
                        child: AutoSizeText(
                          'RM${MyTools.dealShowMoneyString('${(double.parse(provider.walletInfo?.useableBalance ?? '0') / 100).toStringAsFixed(2)}')}',
                          style: TextStyle(
                              color: Color(0xFF343434), fontWeight: FontWeight.bold, fontSize: 30, height: 1.5),
                          // overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                    Text(
                        ' / RM ${MyTools.dealShowMoneyString('${(double.parse(provider.walletInfo?.maxBalanceLimit ?? '0') / 100).toStringAsFixed(2)}')}',
                        style: TextStyle(color: Color(0xFF343434), fontWeight: FontWeight.bold, fontSize: 13)),
                  ],
                ),
                Text(
                    '${"wallet_my_desc".tr().toString()} ${MyTools.formatDateInTransaction(provider.walletInfo?.createTime, showTime: false)}',
                    style: TextStyle(color: Color(0xFF343434), fontSize: 10)),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            height: 1,
            color: Color(0xFFcbcadb),
          ),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              topUp();
            },
            child: Container(
              padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('wallet_top_up'.tr().toString(), style: TextStyle(color: Color(0xFF343434), fontSize: 12)),
                  Icon(Icons.chevron_right, color: Color(0xFF343434), size: 20)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget configFunction() {
    return Container(
      decoration: BoxDecoration(border: Border(top: BorderSide(width: 1, color: Color(0xFFf5f5f5)))),
      child: Column(
        children: itemTitles!.map((String title) {
          return GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              clickItem(title);
            },
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white, border: Border(bottom: BorderSide(width: 1, color: Color(0xFFf5f5f5)))),
              padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(title, style: TextStyle(color: Color(0xFF212121), fontSize: 16)),
                  Icon(Icons.chevron_right, color: appColorPrimaryKiple, size: 34)
                ],
              ),
            ),
          );
        }).toList(),
      ),
    );
  }

  void topUp() {
    log('top up');
    Navigator.push(context, new MaterialPageRoute(builder: (_) {
      return TopUpPage();
    }));
  }

  /*
  "wallet_item_notifications": "Notifications",
  "wallet_item_card": "Saved Card(s)",
  "wallet_item_loyalty": "Loyalty Points",
  "wallet_item_prepaid": "Visa Prepaid Card",
  "wallet_item_pin": "Change PIN/Forgot PIN",
  "wallet_item_language": "Language",
  "wallet_item_withdrawal": "Withdrawal"
  */
  void clickItem(String title) {
    if (title == "wallet_item_notifications".tr()) {
      Navigator.push(context, new MaterialPageRoute(builder: (_) {
        return NotificationListPage();
      }));
    } else if (title == "wallet_item_card".tr()) {
      provider.getCardManagerData();
    } else if (title == "wallet_item_loyalty".tr()) {
      Navigator.push(context, new MaterialPageRoute(builder: (_) {
        return LoyaltyListPage();
      }));
    } else if (title == "wallet_item_prepaid".tr()) {
    } else if (title == "wallet_item_pin".tr()) {
      Navigator.push(context, new MaterialPageRoute(builder: (_) {
        return InputPinPage.changePinByPin();
      }));
    } else if (title == "wallet_item_language".tr()) {
      Navigator.push(context, new MaterialPageRoute(builder: (_) {
        return LanguagePage(walletHomeMainPageProvider: widget.walletHomeProvider);
      }));
    } else if (title == "wallet_item_withdrawal".tr()) {
      Navigator.push(context, new MaterialPageRoute(builder: (_) {
        return WithDrawalPage();
      }));
    }
  }

  @override
  MyWalletProvider createProvider(BuildContext buildContext) {
    provider = MyWalletProvider(buildContext);
    provider.initData();
    return provider;
  }

  @override
  void dispose() {
    super.dispose();
    walletSubscription?.cancel();
  }
}
