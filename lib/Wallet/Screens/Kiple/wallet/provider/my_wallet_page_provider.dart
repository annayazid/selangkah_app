import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/cards/card_online.dart';
import 'package:selangkah_new/Wallet/models/card_manager_entity.dart';
import 'package:selangkah_new/Wallet/models/wallet_Info_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class MyWalletProvider extends BaseProvider {
  MyWalletProvider(BuildContext buildContext) : super(buildContext);
  WalletInfoEntity? walletInfo;

  initData({bool showDialog = true}) {
    requestNetwork<WalletInfoEntity>(Method.get, showDialog: showDialog, url: HttpApi.KipleBaseUrl + HttpApi.WalletInfo,
        onSuccess: (data) {
      walletInfo = data;
      notifyListeners();
    }, onError: (code, message) {
      showMessage(message);
    });
  }

  getCardManagerData() {
    requestNetwork<CardManagerEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.CardManager,
        onError: (code, msg) {
      showMessage(msg);
    }, onSuccess: (data) {
      Future.delayed(Duration(microseconds: 0), () {
        Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
          return CardOnline(url: data.url, addCard: true);
        }));
      });
    });
  }
}
