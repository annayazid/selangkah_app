import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/pin/input_pin.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/my_qr_code.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/provider/scan_qr_provider.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class ScanQrPage extends StatefulWidget {
  @override
  _ScanQrPageState createState() => _ScanQrPageState();
}

class _ScanQrPageState extends State<ScanQrPage>
    with StateLifeCycle<ScanQrPage, ScanQrProvider> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  ScanQrProvider? scanProvider;
  MediaQueryData? media;
  double coverHeight = 0.0;
  double scanHeight = 270.0;
  double spaceWidth = 0.0;
  double coverAlpha = 0.5;
  bool showQrButton = true;

  @override
  void initState() {
    super.initState();
    media = MediaQueryData.fromWindow(window);
    coverHeight = (media!.size.height - scanHeight) * 0.5;
    spaceWidth = (media!.size.width - scanHeight) * 0.5;
  }

  @override
  void reassemble() {
    super.reassemble();
    checkPermission();
    if (Platform.isAndroid) {
      scanProvider?.qrController?.pauseCamera();
    } else if (Platform.isIOS) {
      scanProvider?.qrController?.resumeCamera();
    }
  }

  Future<String?> checkPermission() async {
    final status = await Permission.camera.request();
    if (status.isGranted) {
    } else {
      toast("no_permission".tr(), length: Toast.LENGTH_LONG);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: Colors.white, body: configBody());
  }

  Widget configBody() {
    return Stack(
      children: [
        QRView(
          key: qrKey,
          onQRViewCreated: onQRViewCreated,
        ),
        configCover()
      ],
    );
  }

  Widget configCover() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        children: [
          Stack(
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 15),
                alignment: Alignment.bottomCenter,
                width: double.infinity,
                height: coverHeight,
                decoration:
                    BoxDecoration(color: Colors.black.withOpacity(coverAlpha)),
                child: Text('scan_qr_title'.tr(),
                    style: TextStyle(color: Colors.white, fontSize: 16)),
              ),
              Positioned(
                  top: 35,
                  left: 10,
                  child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(Icons.arrow_back,
                          color: Colors.white, size: 30)))
            ],
          ),
          Container(
            width: double.infinity,
            height: scanHeight,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: double.infinity,
                  width: spaceWidth,
                  decoration: BoxDecoration(
                      color: Colors.black.withOpacity(coverAlpha)),
                ),
                Container(
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: appColorPrimaryKiple, width: 1)),
                    width: scanHeight,
                    height: double.infinity),
                Container(
                  height: double.infinity,
                  width: spaceWidth,
                  decoration: BoxDecoration(
                      color: Colors.black.withOpacity(coverAlpha)),
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            height: coverHeight,
            decoration:
                BoxDecoration(color: Colors.black.withOpacity(coverAlpha)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    if (scanProvider?.qrController != null) {
                      scanProvider?.qrController?.toggleFlash();
                    }
                  },
                  child: Image.asset('assets/images/Wallet/turn_on.png',
                      width: 45, height: 45),
                ),
                GestureDetector(
                  onTap: () {
                    log('My QR');
                    showMyQrCode();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.fromLTRB(30, 35, 30, 50),
                    width: double.infinity,
                    height: 45,
                    decoration: BoxDecoration(
                        color: appColorPrimaryKiple
                            .withOpacity(showQrButton ? 1 : 0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Text(showQrButton ? 'scan_qr_button_qr'.tr() : '',
                        style: TextStyle(fontSize: 16, color: Colors.white)),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  void showMyQrCode() {
    Navigator.push(context, new MaterialPageRoute(builder: (_) {
      return InputPinPage.VerifyPin();
    })).then((value) {
      if (value != null && value.success) {
        showQrButton = false;
        setState(() {});
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return MyQrCodePage();
            }).then((value) {
          showQrButton = true;
          setState(() {});
        });
      }
    });
  }

  void onQRViewCreated(QRViewController controller) {
    scanProvider?.qrController = controller;
    controller.scannedDataStream.listen((scanData) {
      scanProvider?.result = scanData;
      scanProvider?.handleResult();
    });

    controller.pauseCamera();
    controller.resumeCamera();
  }

  @override
  ScanQrProvider createProvider(BuildContext buildContext) {
    scanProvider = ScanQrProvider(buildContext);
    return scanProvider!;
  }

  @override
  void dispose() {
    super.dispose();
    scanProvider?.dispose();
    scanProvider?.qrController?.dispose();
  }
}
