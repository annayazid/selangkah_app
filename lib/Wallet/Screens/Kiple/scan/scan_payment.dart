import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/components/payment_methods.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/provider/payment_summary_provider.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_item.dart';
import 'package:selangkah_new/Wallet/models/merchant_info_entity.dart';
import 'package:selangkah_new/Wallet/models/paymethod_model.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class PayParameter {
  String? amount;
  MerchantInfoEntity? merchantInfo;
}

class ScanPaymentSummary extends StatefulWidget {
  final PayParameter parameter;

  ScanPaymentSummary({required this.parameter});

  @override
  _ScanPaymentSummaryState createState() => _ScanPaymentSummaryState();
}

class _ScanPaymentSummaryState extends State<ScanPaymentSummary>
    with StateLifeCycle<ScanPaymentSummary, PaymentSummaryProvider> {
  PaymentSummaryProvider? payProvider;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(title: 'scan_qr_pay_title'.tr(), backgroundColor: Colors.white, child: configBody());
  }

  Widget configBody() {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            children: [
              configChangeNotifierWidget(),
              PaymentMethodsWidget(
                payMethodsFunction: (List<PayWayModel> methods) {
                  payProvider?.payWays = methods;
                },
              )
            ],
          ),
        ),
        Positioned(
            left: 30,
            right: 30,
            bottom: 35,
            child: NormalClickItem(
                content: 'scan_qr_pay_button'.tr(),
                clickFunction: () {
                  payProvider?.payAction();
                }))
      ],
    );
  }

  Widget configChangeNotifierWidget() {
    return ChangeNotifierProvider(
      create: (_) {
        return payProvider;
      },
      child: Consumer(
        builder: (ctx, PaymentSummaryProvider provider, child) {
          return configMerchantInfo();
        },
      ),
    );
  }

  Widget configMerchantInfo() {
    return Container(
      margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.15), blurRadius: 5)]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(right: 10),
              child: Row(
                children: [
                  Expanded(
                    child: Text('${widget.parameter.merchantInfo?.name}',
                        style: TextStyle(color: Color(0xFF212121), fontWeight: FontWeight.bold, fontSize: 16)),
                  )
                ],
              ),
            ),
          ),
          Column(
            children: [
              Row(
                children: [
                  Text('RM  ', style: TextStyle(fontSize: 12, color: Color(0xFF212121))),
                  Text('${payProvider?.handleAmount()}',
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Color(0xFF212121)))
                ],
              ),
              GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  payProvider?.jumpToPoints();
                },
                child: Text(
                  payProvider?.integral == null
                      ? "${'pay_points'.tr()}"
                      : "${MyTools.dealNumber(double.parse(widget.parameter.amount!))}",
                  style: payProvider?.integral == null
                      ? TextStyle(
                          fontSize: 12,
                          color: appColorPrimary,
                        )
                      : TextStyle(
                          fontSize: 12,
                          color: appColorPrimary,
                          decoration: TextDecoration.lineThrough,
                          decorationColor: appColorPrimary),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  PaymentSummaryProvider createProvider(BuildContext buildContext) {
    payProvider = PaymentSummaryProvider(buildContext);
    payProvider?.parameter = widget.parameter;
    return payProvider!;
  }
}
