import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/provider/my_qr_provider.dart';
import 'package:selangkah_new/Wallet/Utils/event_bus_tool.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class MyQrCodePage extends StatefulWidget {
  @override
  _MyQrCodePageState createState() => _MyQrCodePageState();
}

class _MyQrCodePageState extends State<MyQrCodePage> with StateLifeCycle<MyQrCodePage, MyQrCodeProvider> {
  MyQrCodeProvider? qrProvider;

  StreamSubscription? posSubscription;

  @override
  void initState() {
    super.initState();
    posSubscription = posNotificationEventBus.on().listen((event) {
      qrProvider?.goToPosResultPage(event);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: ChangeNotifierProvider(
        create: (_) {
          return qrProvider;
        },
        child: Consumer(
          builder: (ctx, MyQrCodeProvider provider, child) {
            return configBody();
          },
        ),
      ),
    );
  }

  Widget configBody() {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          child: Center(
            child: Container(
              margin: EdgeInsets.only(left: 25, right: 25),
              padding: EdgeInsets.all(15),
              width: double.infinity,
              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('my_qr_title'.tr(), style: TextStyle(color: Color(0xFF212121), fontSize: 16)),
                  qrProvider?.codeInfo == null
                      ? Container(height: 210)
                      : Container(
                          margin: EdgeInsets.only(top: 30, bottom: 30),
                          child: QrImage(data: '${qrProvider?.codeInfo?.qrCode}', size: 150),
                        ),
                  Text('${qrProvider?.second}s', style: TextStyle(color: Color(0xFF757575), fontSize: 14)),
                ],
              ),
            ),
          ),
        ),
        Positioned(
          left: 30,
          right: 30,
          bottom: 50,
          child: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              alignment: Alignment.center,
              width: double.infinity,
              height: 45,
              decoration: BoxDecoration(color: appColorPrimaryKiple, borderRadius: BorderRadius.circular(10)),
              child: Text(
                'my_qr_button_back'.tr(),
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  @override
  MyQrCodeProvider createProvider(BuildContext buildContext) {
    qrProvider = MyQrCodeProvider(buildContext);
    qrProvider?.getMyCodeDat();
    return qrProvider!;
  }

  @override
  void dispose() {
    super.dispose();
    qrProvider?.timer?.cancel();
    posSubscription?.cancel();
  }
}
