import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/models/scan_payment_result_entity.dart';

class SuccessContent extends StatelessWidget {
  final ScanPaymentResultEntity? result;

  SuccessContent({this.result});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('scan_qr_pay_success_title'.tr(),
              style: TextStyle(color: Color(0xFF212121), fontSize: 16, fontWeight: FontWeight.bold)),
          SizedBox(height: 15),
          Text('scan_qr_pay_success_processed'.tr(), style: TextStyle(color: Color(0xFF757575), fontSize: 14)),
          SizedBox(height: 15),
          Text('scan_qr_pay_success_transaction'.tr(),
              style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5)),
          Text('${result?.orderNo}', style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5)),
          SizedBox(height: 15),
          Text('${MyTools.formatDateInTransaction(result?.bankPayTime)}',
              style: TextStyle(color: Color(0xFF212121), fontSize: 14)),
          SizedBox(height: 15),
          grayContainer(),
          configPaymentInfo(),
          grayContainer()
        ],
      ),
    );
  }

  Widget grayContainer() {
    return Container(
      width: double.infinity,
      height: 10,
      color: Color(0xFFf5f5f5),
    );
  }

  Widget configPaymentInfo() {
    return Container(
      padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
      width: double.infinity,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('payment_string'.tr(), style: TextStyle(color: Color(0xFF757575), fontSize: 16)),
              Text('RM ${(double.parse(result!.amount!) / 100).toStringAsFixed(2)}',
                  style: TextStyle(color: Color(0xFF212121), fontSize: 16)),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('bill_payment_success_point_used'.tr(), style: TextStyle(color: Color(0xFF757575), fontSize: 16)),
              Text('${result?.useIntegral}', style: TextStyle(color: Color(0xFF212121), fontSize: 16)),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('bill_payment_success_point_earned'.tr(), style: TextStyle(color: Color(0xFF757575), fontSize: 16)),
              Text('${result?.gainIntegral}', style: TextStyle(color: Color(0xFF212121), fontSize: 16)),
            ],
          )
        ],
      ),
    );
  }
}
