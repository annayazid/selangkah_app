import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/provider/payment_methods_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/topup/topup_page.dart';
import 'package:selangkah_new/Wallet/models/paymethod_model.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class PaymentMethodsWidget extends StatefulWidget {
  final ValueChanged<List<PayWayModel>>? payMethodsFunction;
  final double space;

  PaymentMethodsWidget({this.payMethodsFunction, this.space = 15.0});

  @override
  _PaymentMethodsState createState() => _PaymentMethodsState();
}

class _PaymentMethodsState extends State<PaymentMethodsWidget>
    with StateLifeCycle<PaymentMethodsWidget, PaymentMethodsProvider> {
  PaymentMethodsProvider? payMethods;
  List<PayWayModel> methods = [];
  bool selectWallet = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (BuildContext context) {
        return payMethods;
      },
      child: Consumer(
        builder: (ctx, PaymentMethodsProvider provider, child) {
          return Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(widget.space, 10, widget.space, 10),
                  child: Text('scan_qr_pay_choose_method'.tr(),
                      style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold)),
                ),
                Container(
                  width: double.infinity,
                  margin: EdgeInsets.fromLTRB(widget.space, 5, widget.space, 5),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.15), blurRadius: 5)]),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [configWalletMethod()],
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  Widget configWalletMethod() {
    String imgPath = '';
    if (selectWallet) {
      imgPath = 'assets/images/WalletEkyc/select_item.png';
    } else {
      imgPath = 'assets/images/WalletEkyc/unselect-item.png';
    }

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        selectWallet = !selectWallet;
        if (selectWallet) {
          PayWayModel model = PayWayModel();
          model.type = PayType.wallet;
          model.balance = double.parse(payMethods?.walletInfo?.useableBalance ?? '0.00');
          methods.add(model);
        } else {
          methods.clear();
        }
        setState(() {});
        if (widget.payMethodsFunction != null) widget.payMethodsFunction!(methods);
      },
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.all(15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Image.asset(
                  imgPath,
                  width: 15,
                  height: 15,
                ),
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('utama_selangkah_wallet'.tr(),
                          style: TextStyle(color: Color(0xFF212121), fontSize: 14, height: 1.5)),
                      Text(
                          '${'scan_qr_pay_balance'.tr()}  RM ${(double.parse(payMethods?.walletInfo?.useableBalance ?? '0.00') / 100).toStringAsFixed(2)}',
                          style: TextStyle(color: Color(0xFF757575), fontSize: 12, height: 1.5)),
                    ],
                  ),
                )
              ],
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context, new MaterialPageRoute(builder: (_) {
                  return TopUpPage(fromPayment: true);
                })).then((value) {
                  payMethods?.getWalletInfo(false);
                });
              },
              child: Container(
                padding: EdgeInsets.fromLTRB(12, 5, 12, 5),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: appColorPrimaryKiple, width: 1)),
                child: Text('wallet_topup'.tr(),
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: appColorPrimaryKiple)),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  PaymentMethodsProvider createProvider(BuildContext buildContext) {
    payMethods = PaymentMethodsProvider(buildContext);
    payMethods?.getWalletInfo(true);
    return payMethods!;
  }

  @override
  void dispose() {
    super.dispose();
    payMethods?.dispose();
  }
}
