import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class FailedContent extends StatelessWidget {
  final String title;
  final String? content;

  FailedContent({required this.title, this.content});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 30, right: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(title, style: TextStyle(color: Color(0xFF212121), fontSize: 16, fontWeight: FontWeight.bold)),
          SizedBox(height: 30),
          Text(content ?? 'scan_qr_pay_failed'.tr(),
              style: TextStyle(color: Color(0xFF757575), fontSize: 14), textAlign: TextAlign.center),
        ],
      ),
    );
  }
}
