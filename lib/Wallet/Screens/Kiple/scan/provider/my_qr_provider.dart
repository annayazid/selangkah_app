import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/HomePage/cubit/get_homepage_data/get_homepage_data_cubit.dart';
import 'package:selangkah_new/Screens/HomePage/screens/homepage_main.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/result_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/components/pos_success.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';
import 'package:selangkah_new/Wallet/components/talk_to_us.dart';
import 'package:selangkah_new/Wallet/models/my_qr_code_entity.dart';
import 'package:selangkah_new/Wallet/models/pos_model_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class MyQrCodeProvider extends BaseProvider {
  MyQrCodeProvider(BuildContext buildContext) : super(buildContext);
  MyQrCodeEntity? codeInfo;
  final int timerSecond = 60;
  int second = 60;
  Timer? timer;

  getMyCodeDat() {
    requestNetwork<MyQrCodeEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.MyQrCode, onError: (code, msg) {
      showMessage(msg);
      Navigator.pop(currentContext);
    }, onSuccess: (data) {
      codeInfo = data;
      initTimer();
      notifyListeners();
    });
  }

  initTimer() {
    second = 60;
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      second--;
      notifyListeners();
      if (second == 0) {
        timer.cancel();
        getMyCodeDat();
      }
    });
  }

  goToPosResultPage(event) {
    PosModelEntity? posModel = JsonConvert.fromJsonAsT<PosModelEntity>(transferValue(event));
    Navigator.pushReplacement(currentContext, new MaterialPageRoute(builder: (_) {
      return ResultPage(
        contentChild: PosSuccessContent(result: posModel),
        bottomChild: Talk2UsWidget(
          contentString: "mobile_help_content".tr(),
          showButton: true,
        ),
        status: true,
      );
    })).then((value) {
      gotoHomePage();
    });
  }

  Map<String, dynamic> transferValue(value) {
    Map<String, dynamic> theValue = {
      'amount': value['amount'],
      'time': value['time'],
      'type_code': value['type_code'],
      'point_earned': value['point_earned'],
      'transaction_no': value['transaction_no'],
      'trade_type': value['trade_type'],
      'merchant_name': value['merchant_name'],
    };
    return theValue;
  }

  gotoHomePage() {
    BlocProvider(
      create: (context) => GetHomepageDataCubit(),
      child: HomePageScreen(),
    ).launch(
      currentContext,
      isNewTask: true,
    );
  }
}
