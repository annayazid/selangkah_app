import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/scan_payment.dart';
import 'package:selangkah_new/Wallet/models/merchant_info_entity.dart';

class ScanAmountProvider extends BaseProvider {
  ScanAmountProvider(BuildContext buildContext) : super(buildContext);

  String amount = '';
  String codeResult = '';
  MerchantInfoEntity? merchantInfo;

  initData() {}

  nextAction() {
    if (amount.trim().length == 0) {
      showMessage('top_up_alert'.tr());
      return;
    }

    PayParameter para = PayParameter();
    para.amount = amount;
    para.merchantInfo = merchantInfo;
    Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
      return ScanPaymentSummary(parameter: para);
    }));
  }

  onChangeAmount(String text) {
    if (text.length > 0) {
      if (text.length == 1) {
        amount = (double.parse(text) / 100).toStringAsFixed(2);
      } else {
        double newValue = double.parse(text);
        if (newValue == 0) {
          amount = "";
        } else {
          if (text.substring(text.lastIndexOf(".") + 1, text.length).length == 2 &&
              amount.substring(amount.lastIndexOf(".") + 1, amount.length).length == 1) {
            amount = text;
          } else if (!text.endsWith(".") &&
              text.contains(".") &&
              (text.substring(text.lastIndexOf(".") + 1, text.length).length >= 1 &&
                  amount.substring(amount.lastIndexOf(".") + 1, amount.length).length >= 1) &&
              (double.parse(text.substring(0, text.length - 1)) == double.parse(amount) ||
                  double.parse(text) == double.parse(amount.substring(0, amount.length - 1)))) {
            if (text.length > amount.length) {
              amount = (newValue * 10).toStringAsFixed(2);
            } else if (text.length < amount.length) {
              amount = (newValue / 10).toStringAsFixed(2);
            }
          } else {
            amount = text;
          }
        }
      }
    } else {
      amount = '';
    }
    notifyListeners();
  }

  bool enableStatus() {
    if (amount.toString().length > 0 && double.parse(amount) > 0) {
      return true;
    } else {
      return false;
    }
  }
}
