import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/models/wallet_Info_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class PaymentMethodsProvider extends BaseProvider {
  PaymentMethodsProvider(BuildContext buildContext) : super(buildContext);
  WalletInfoEntity? walletInfo;

  getWalletInfo(bool showHud) {
    requestNetwork<WalletInfoEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.WalletInfo, showDialog: showHud,
        onSuccess: (data) {
      walletInfo = data;
      notifyListeners();
    }, onError: (code, message) {
      showMessage(message);
    });
  }
}
