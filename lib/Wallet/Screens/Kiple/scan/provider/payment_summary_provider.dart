import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/HomePage/cubit/get_homepage_data/get_homepage_data_cubit.dart';
import 'package:selangkah_new/Screens/HomePage/screens/homepage_main.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/mobileReload/redeem_points_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/pin/input_pin.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/result_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/components/failed_content.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/components/success_content.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/scan_payment.dart';
import 'package:selangkah_new/Wallet/Utils/app_helper.dart';
import 'package:selangkah_new/Wallet/components/pop_up_dialog.dart';
import 'package:selangkah_new/Wallet/components/talk_to_us.dart';
import 'package:selangkah_new/Wallet/models/Integral_item_model_entity.dart';
import 'package:selangkah_new/Wallet/models/paymethod_model.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/models/scan_payment_result_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class PaymentSummaryProvider extends BaseProvider {
  PaymentSummaryProvider(BuildContext buildContext) : super(buildContext);
  List<PayWayModel>? payWays;
  PayParameter? parameter;
  ScanPaymentResultEntity? result;
  IntegralItemModelEntity? integral;

  payAction() {
    if (payWays == null || payWays?.length == 0) {
      showMessage('scan_qr_pay_method_alert'.tr());
      return;
    }

    Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
      return InputPinPage.VerifyPin(needBackPinCode: true);
    })).then((value) {
      if (value != null) {
        ResultModel model = value;
        if (model.success!) {
          String code = model.data;
          payMoney(code);
        } else {
          showMessage(model.message!);
        }
      }
    });
  }

  payMoney(String pinCode) async {
    showProgressDialog();
    bool isNotOut = await isNotOutsideMalaysia();
    dismissProgressDialog();
    if (isNotOut == null) {
      Timer.periodic(Duration(milliseconds: 100), (timer) {
        timer.cancel();
        showNotDialog('ekyc_oops_title'.tr(), 'not_location_permission'.tr(), 'ekyc_hard_reject_button'.tr());
      });
    }
    if (isNotOut) {
      var para = {
        'pin': '$pinCode',
        'merchant_id': parameter?.merchantInfo?.subMerchantId,
        'amount': (double.parse(parameter!.amount!) * 100).toStringAsFixed(0),
        'integral': integral?.integral ?? 0
      };
      showProgressDialog();
      Map<String, dynamic> deviceMsg = await AppHelper().getDeviceMap();
      if (deviceMsg != null && deviceMsg.length > 0) {
        para.addAll(deviceMsg);
      }
      requestNetwork<ScanPaymentResultEntity>(Method.post,
          url: HttpApi.KipleBaseUrl + HttpApi.WalletPayment, showDialog: false, params: para, onError: (code, msg) {
        dismissProgressDialog();
        showMessage(msg);
        gotoFailedPage(code, msg);
      }, onSuccess: (data) {
        dismissProgressDialog();
        result = data;
        Future.delayed(Duration(microseconds: 0), () {
          gotoSuccess();
        });
      });
    } else {
      Timer.periodic(Duration(milliseconds: 100), (timer) {
        timer.cancel();
        showNotDialog('ekyc_oops_title'.tr(), 'out_malaysia_payment'.tr(), 'ekyc_hard_reject_button'.tr(),
            confirmClickFunction: () {
          Navigator.of(currentContext).pop(ResultModel(success: false));
        });
      });
    }
  }

  showNotDialog(String title, String content, String confirm, {Function? confirmClickFunction}) {
    showDialog(
        context: currentContext,
        builder: (_) {
          return PopUpDialog(
            title: title,
            content: content,
            confirm: confirm,
            confirmFunction: confirmClickFunction,
          );
        });
  }

  gotoFailedPage(int code, String msg) {
    Widget content;
    if (code == 1700031) {
      content = FailedContent(title: 'scan_qr_pay_insufficient_balance'.tr(), content: 'scan_qr_pay_failed'.tr());
    } else {
      content = FailedContent(
          title: 'withdrawal_fail_title'.tr(),
          content: msg.isNotEmpty && msg.length > 0 ? msg : 'scan_qr_pay_failed_normal'.tr());
    }

    Future.delayed(Duration(microseconds: 0), () {
      Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
        return ResultPage(
          contentChild: content,
          bottomChild: Talk2UsWidget(
            contentString: "mobile_help_content".tr(),
            showButton: true,
          ),
          status: false,
        );
      })).then((value) {
        gotoHomePage();
      });
    });
  }

  gotoSuccess() {
    Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
      return ResultPage(
        contentChild: SuccessContent(result: result),
        bottomChild: Talk2UsWidget(
          contentString: "mobile_help_content".tr(),
          showButton: true,
        ),
        status: true,
      );
    })).then((value) {
      gotoHomePage();
    });
  }

  gotoHomePage() {
    BlocProvider(
      create: (context) => GetHomepageDataCubit(),
      child: HomePageScreen(),
    ).launch(
      currentContext,
      isNewTask: true,
    );
  }

  String handleAmount() {
    if (integral == null) {
      return '${double.parse(parameter!.amount!).toStringAsFixed(2)}';
    } else {
      double oldAmount = double.parse('${parameter!.amount}');
      double integralAmount = double.parse('${integral!.amount}');
      if (oldAmount > integralAmount) {
        return '${(oldAmount - integralAmount).toStringAsFixed(2)}';
      } else {
        return '0.00';
      }
    }
  }

  jumpToPoints() {
    Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
      return RedeemPointsPage();
    })).then((value) {
      if (value != null && value.success) {
        integral = value.data;
        notifyListeners();
      }
    });
  }
}
