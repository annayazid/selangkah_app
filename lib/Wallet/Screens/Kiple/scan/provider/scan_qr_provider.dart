import 'dart:async';

import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/scan_amount.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/transfer/transfer_page.dart';
import 'package:selangkah_new/Wallet/models/merchant_info_entity.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class ScanQrProvider extends BaseProvider {
  ScanQrProvider(BuildContext buildContext) : super(buildContext);

  String? codeResult;
  MerchantInfoEntity? info;
  QRViewController? qrController;
  Barcode? result;

  handleResult() async {
    await qrController?.pauseCamera();
    codeResult = result?.code;
    decodingQrCode();
  }

  decodingQrCode() {
    requestNetwork<MerchantInfoEntity>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.DecodingCode,
        params: {'qr_code': codeResult}, onSuccess: (data) async {
      info = data;
      if (info?.typeCode == 10003) {
        Timer.periodic(Duration(milliseconds: 100), (timer) async {
          timer.cancel();
          ResultModel resultModel = await Navigator.pushReplacement(
              currentContext, new MaterialPageRoute(builder: (_) {
            return TransferPage(
              merchantInfo: info,
            );
          }));
          if (resultModel != null && resultModel.success!) {
            Navigator.of(currentContext).pop(ResultModel(success: true));
          }
        });
      } else if (info?.typeCode == 10001) {
        if (info?.amount?.length == 0) {
          info?.amount = '0';
        }
        Future.delayed(Duration(microseconds: 0), () {
          Navigator.pushReplacement(currentContext,
              new MaterialPageRoute(builder: (_) {
            return ScanAmountPage(info: info!);
          }));
        });
      } else {
        showMessage('${info?.typeCode}');
      }
    }, onError: (code, msg) {
      showMessage(msg);
    });
  }
}
