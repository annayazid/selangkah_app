import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/provider/scan_amount_provider.dart';
import 'package:selangkah_new/Wallet/Utils/precision_limit_formatter.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_item.dart';
import 'package:selangkah_new/Wallet/models/merchant_info_entity.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class ScanAmountPage extends StatefulWidget {
  final MerchantInfoEntity info;

  ScanAmountPage({required this.info});

  @override
  _ScanAmountPageState createState() => _ScanAmountPageState();
}

class _ScanAmountPageState extends State<ScanAmountPage> with StateLifeCycle<ScanAmountPage, ScanAmountProvider> {
  ScanAmountProvider? amountProvider;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'scan_qr_amount_title'.tr(),
      backgroundColor: Colors.white,
      child: ChangeNotifierProvider(
        create: (_) {
          return amountProvider;
        },
        child: Consumer(
          builder: (ctx, ScanAmountProvider provider, child) {
            return configBody();
          },
        ),
      ),
    );
  }

  Widget configBody() {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.only(top: 65),
            width: double.infinity,
            height: double.infinity,
            child: Column(
              children: [
                Text('${widget.info.name ?? ""}', style: TextStyle(color: Color(0xFF757575), fontSize: 14)),
                SizedBox(height: 15),
                Text('RM', style: TextStyle(color: Color(0xFF757575), fontSize: 16)),
                configInputAmount(),
                Text('scan_qr_amount_alert'.tr(), style: TextStyle(color: Color(0xFF757575), fontSize: 14)),
              ],
            ),
          ),
          Positioned(
            bottom: 30,
            left: 30,
            right: 30,
            child: NormalClickItem(
              content: 'button_next_string'.tr().toString(),
              enable: amountProvider!.enableStatus(),
              clickFunction: () {
                FocusScope.of(context).requestFocus(FocusNode());
                amountProvider?.nextAction();
              },
            ),
          )
        ],
      ),
    );
  }

  Widget configInputAmount() {
    return Container(
      width: 180,
      margin: EdgeInsets.only(top: 20, bottom: 20),
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      decoration: BoxDecoration(
        color: Color(0xFFf4f4f4),
        borderRadius: BorderRadius.circular(3),
      ),
      child: TextField(
        textAlign: TextAlign.center,
        controller: TextEditingController.fromValue(TextEditingValue(
            text: '${amountProvider?.amount}',
            selection: TextSelection.fromPosition(
                TextPosition(affinity: TextAffinity.downstream, offset: '${amountProvider?.amount}'.length)))),
        // keyboardType: TextInputType.number,
        inputFormatters: [
          PrecisionLimitFormatter(3),
          FilteringTextInputFormatter(RegExp("[0-9.]"), allow: true),
        ],
        style: TextStyle(color: Color(0xFF212121), fontSize: 40, fontWeight: FontWeight.bold),
        decoration: InputDecoration(
            border: InputBorder.none,
            isCollapsed: true,
            contentPadding: EdgeInsets.only(top: 8, bottom: 8),
            hintText: '0.00',
            hintStyle: TextStyle(color: appTextColorGrayhint, fontSize: 40, fontWeight: FontWeight.bold)),
        onChanged: (String text) {
          amountProvider?.onChangeAmount(text);
        },
      ),
    );
  }

  @override
  ScanAmountProvider createProvider(BuildContext buildContext) {
    amountProvider = ScanAmountProvider(buildContext);
    amountProvider?.merchantInfo = widget.info;
    amountProvider?.initData();
    return amountProvider!;
  }

  @override
  void dispose() {
    super.dispose();
    amountProvider?.dispose();
  }
}
