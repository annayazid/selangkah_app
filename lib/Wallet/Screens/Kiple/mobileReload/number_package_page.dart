import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/mobileReload/provider/number_package_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/mobileReload/select_operator_item.dart';
import 'package:selangkah_new/Wallet/Services/navigation_service.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_button.dart';
import 'package:selangkah_new/Wallet/models/operator_model_entity.dart';
import 'package:selangkah_new/utils/AppColors.dart';
import 'package:selangkah_new/utils/global.dart';

class NumberPackagePage extends StatefulWidget {
  final OperatorModelEntity data;

  NumberPackagePage(this.data);

  @override
  State<StatefulWidget> createState() {
    return NumberPackageState();
  }
}

class NumberPackageState extends State<NumberPackagePage>
    with StateLifeCycle<NumberPackagePage, NumberPackageProvider> {
  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'mobile_number_package'.tr(),
      child: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height - (60.h as double),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              SelectOperatorItemPage(widget.data),
              Container(
                height: 3.h as double,
                color: appLineColor,
              ),
              Container(
                margin: EdgeInsets.only(
                    left: 7.w as double,
                    right: 7.w as double,
                    top: 5.h as double,
                    bottom: 5.h as double),
                child: Text(
                  'mobile_number_string'.tr(),
                  style: boldTextStyle(size: 14, color: appTextColorPrimary),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 7.h as double,
                  bottom: 11.h as double,
                  left: 7.w as double,
                  right: 7.w as double,
                ),
                child: Row(
                  children: [
                    Text(
                      "+6",
                      style: primaryTextStyle(
                          size: 12, color: appTextColorPrimary),
                    ),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(
                            left: 4.w as double, right: 4.w as double),
                        height: 16.h as double,
                        padding: EdgeInsets.only(
                            left: 4.w as double, right: 4.w as double),
                        decoration: BoxDecoration(
                          color: appTextColorGrayBack,
                          borderRadius: BorderRadius.circular(
                            (4.0),
                          ),
                        ),
                        child: TextField(
                          cursorColor: appColorPrimaryKiple,
                          controller: provider.mobileController,
                          // keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(0),
                            hintText: 'personal_details_mobile_hint'.tr(),
                            // isCollapsed:true,
                            hintStyle: primaryTextStyle(
                                color: appTextColorGrayhint, size: 12),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                          ),
                          style: primaryTextStyle(
                              color: appTextColorPrimary, size: 12),
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      child: Image.asset(
                        "assets/images/Wallet/icon_select_container.png",
                        width: 12.w as double,
                        height: 12.w as double,
                      ),
                      onTap: () {
                        GlobalFunction.displayDisclosure(
                          icon: Icon(
                            Icons.contacts_outlined,
                            color: Colors.blue,
                            size: 30,
                          ),
                          title: 'contactPrepaidTitle'.tr(),
                          description: 'contactPrepaid'.tr(),
                          key: 'contactPrepaid',
                        )?.then((value) async {
                          if (value) {
                            //function here
                            if (await Permission.contacts.request().isGranted) {
                              if (value) {
                                provider.selectContacter();
                              }
                            } else {
                              return Flushbar(
                                backgroundColor: Colors.white,
                                // message: LoremText,
                                // title: 'Permission needed to continue',
                                titleText: Text(
                                  'permission_continue'.tr(),
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                  ),
                                ),
                                messageText: Text(
                                  'continue_permission'.tr(),
                                  style: TextStyle(color: Colors.black),
                                ),
                                mainButton: TextButton(
                                  onPressed: AppSettings.openAppSettings,
                                  child: Text(
                                    'App Settings',
                                    style: TextStyle(color: Colors.blue),
                                  ),
                                ),
                                duration: Duration(seconds: 7),
                              ).show(NavigationService
                                  .navigatorKey.currentState!.overlay!.context);
                            }
                          }
                        });
                      },
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  left: 7.w as double,
                  right: 7.w as double,
                ),
                child: Text(
                  'mobile_select_package'.tr(),
                  style: boldTextStyle(
                    size: 14,
                    color: appTextColorPrimary,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  left: 7.w as double,
                  right: 7.w as double,
                ),
                child: ChangeNotifierProvider<NumberPackageProvider>(
                  create: (BuildContext buildContext) => provider,
                  child: Consumer<NumberPackageProvider>(
                    builder: (BuildContext? buildContext,
                        NumberPackageProvider? numberChangeProvider,
                        Widget? child) {
                      return Container(
                        constraints: BoxConstraints(maxHeight: 160.h as double),
                        margin: EdgeInsets.only(top: 8.h as double),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 10,
                              spreadRadius: 1,
                              color: Colors.black.withOpacity(0.15),
                            ),
                          ],
                          borderRadius: BorderRadius.all(
                            Radius.circular(10.0),
                          ),
                        ),
                        child: ListView.builder(
                          itemBuilder: (context, position) =>
                              buildListItem(position),
                          controller: provider.scrollController,
                          itemCount: provider.productModelList.length,
                          shrinkWrap: true,
                        ),
                      );
                    },
                  ),
                ),
              ),
              Expanded(
                child: Container(),
              ),
              Container(
                margin: EdgeInsets.only(
                    left: 7.w as double,
                    right: 7.w as double,
                    top: 10.h as double,
                    bottom: 25.h as double),
                child: ChangeNotifierProvider<NumberPackageProvider>(
                  create: (BuildContext buildContext) => provider,
                  child: Consumer<NumberPackageProvider>(
                    builder: (BuildContext? buildContext,
                        NumberPackageProvider? numberPackageProvider,
                        Widget? child) {
                      return ClickButton(
                        content: 'ekyc_next'.tr(),
                        canClick: provider.canCommit,
                        clickFunction: () {
                          provider.jump2Next();
                        },
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  late NumberPackageProvider provider;

  @override
  NumberPackageProvider createProvider(BuildContext buildContext) {
    provider = NumberPackageProvider(buildContext, widget.data);
    return provider;
  }

  Widget buildListItem(int position) {
    return InkWell(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(
                left: 9.w as double,
                right: 9.w as double,
                top: 7.h as double,
                bottom: 7.h as double),
            child: Row(
              children: [
                Image.asset(
                  provider.selectItem.id ==
                          provider.productModelList[position].id
                      ? "assets/images/WalletEkyc/select_item.png"
                      : "assets/images/WalletEkyc/unselect-item.png",
                  width: 7.w as double,
                  height: 7.w as double,
                ),
                Container(
                  margin: EdgeInsets.only(left: 3.w as double),
                  child: Text(
                    "${'withdrawal_mr'.tr()} ${MyTools.dealNumber(provider.productModelList[position].amount! / 100)}",
                    style: TextStyle(color: appTextColorPrimary, fontSize: 14),
                  ),
                ),
              ],
            ),
          ),
          Offstage(
            offstage: position == provider.productModelList.length - 1,
            child: Container(
              height: 0.8.h as double,
              color: appLineColor,
            ),
          ),
        ],
      ),
      onTap: () {
        provider.changeSelectItem(provider.productModelList[position]);
      },
    );
  }
}
