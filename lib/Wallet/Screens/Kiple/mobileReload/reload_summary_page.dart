import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/mobileReload/provider/reload_summary_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/components/payment_methods.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_button.dart';
import 'package:selangkah_new/Wallet/components/talk_to_us.dart';
import 'package:selangkah_new/Wallet/models/mobile_result_entity.dart';
import 'package:selangkah_new/Wallet/models/number_selector.dart';
import 'package:selangkah_new/Wallet/models/paymethod_model.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class ReloadSummaryPage extends StatefulWidget {
  NumberSelector numberSelector;

  ReloadSummaryPage(this.numberSelector);

  @override
  State<StatefulWidget> createState() {
    return ReloadSummaryState();
  }
}

class ReloadSummaryState extends State<ReloadSummaryPage>
    with StateLifeCycle<ReloadSummaryPage, ReloadSummaryProvider> {
  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: 'mobile_reload_summary'.tr(),
      child: Container(
        margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double, top: 5.h as double),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    blurRadius: 10,
                    spreadRadius: 1,
                    color: Colors.black.withOpacity(0.15),
                  ),
                ],
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 6.h as double, bottom: 6.h as double, left: 7.w as double),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.numberSelector.operatorModelEntity!.providerName!,
                          style: primaryTextStyle(size: 16, color: appTextColorPrimary),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 4.h as double),
                          child: Text(
                            "+6${widget.numberSelector.contact!.phoneNumbers![0].replaceAll(" ", "")}",
                            style: primaryTextStyle(size: 14, color: appTextColorGray),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  ChangeNotifierProvider<ReloadSummaryProvider>(
                    create: (BuildContext buildContext) => provider,
                    child: Consumer<ReloadSummaryProvider>(
                      builder:
                          (BuildContext? buildContext, ReloadSummaryProvider? reloadSummaryProvider, Widget? child) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Row(
                              children: [
                                Text(
                                  'rm_string'.tr(),
                                  style: primaryTextStyle(color: appTextColorPrimary, size: 12),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 3.w as double, right: 7.w as double),
                                  child: Text(
                                    provider.selectIntegral == null
                                        ? "${MyTools.dealNumber(widget.numberSelector.productSelector!.amount! / 100)}"
                                        : "${MyTools.dealNumber(widget.numberSelector.productSelector!.amount! / 100 - provider.selectIntegral!.amount! > 0 ? widget.numberSelector.productSelector!.amount! / 100 - provider.selectIntegral!.amount! : 0.00)}",
                                    style: boldTextStyle(color: appTextColorPrimary, size: 16),
                                  ),
                                )
                              ],
                            ),
                            InkWell(
                              child: Container(
                                margin: EdgeInsets.only(top: 2.h as double, right: 7.w as double),
                                child: Text(
                                  provider.selectIntegral == null
                                      ? "${'pay_points'.tr()}"
                                      : "${MyTools.dealNumber(widget.numberSelector.productSelector!.amount! / 100)}",
                                  style: provider.selectIntegral == null
                                      ? TextStyle(
                                          fontSize: 12,
                                          color: appColorPrimary,
                                        )
                                      : TextStyle(
                                          fontSize: 12,
                                          color: appColorPrimary,
                                          decoration: TextDecoration.lineThrough,
                                          decorationColor: appColorPrimary),
                                ),
                              ),
                              onTap: () {
                                provider.selectRedeem();
                              },
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10.h as double),
              child: PaymentMethodsWidget(
                payMethodsFunction: (List<dynamic> methods) {
                  provider.changeSelectPayWay(methods as List<PayWayModel>);
                },
                space: 0,
              ),
            ),
            Expanded(
              child: Container(),
            ),
            ChangeNotifierProvider<ReloadSummaryProvider>(
              create: (BuildContext buildContext) => provider,
              child: Consumer<ReloadSummaryProvider>(
                builder: (BuildContext? buildContext, ReloadSummaryProvider? reloadSummaryProvider, Widget? child) {
                  return Container(
                    margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double, bottom: 25.h as double),
                    child: ClickButton(
                      content: 'wallet_topup'.tr(),
                      canClick: provider.canCommit,
                      clickFunction: () {
                        provider.jump2Next();
                      },
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  late ReloadSummaryProvider provider;

  @override
  ReloadSummaryProvider createProvider(BuildContext buildContext) {
    provider = ReloadSummaryProvider(buildContext, widget.numberSelector, this);
    return provider;
  }

  Widget successChild(MobileResultEntity data) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 5.h as double),
          child: Text(
            'mobile_success_title'.tr(),
            style: primaryTextStyle(size: 16, color: appTextColorPrimary),
          ),
        ),
        Text(
          'mobile_success_content'.tr(),
          style: primaryTextStyle(size: 14, color: appTextColorGray),
        ),
        Container(
          margin: EdgeInsets.only(top: 7.h as double, bottom: 2.h as double),
          child: Text(
            'mobile_transacation_id'.tr(),
            style: primaryTextStyle(size: 14, color: appTextColorGray),
          ),
        ),
        Text(
          data.orderNumber == null ? "" : data.orderNumber!,
          style: primaryTextStyle(size: 14, color: appTextColorGray),
        ),
        Container(
          margin: EdgeInsets.only(top: 7.h as double, bottom: 7.h as double),
          child: Text(
            MyTools.formatDate(
              DateTime.parse(data.tradeTime!),
            ),
            style: primaryTextStyle(size: 14, color: appTextColorPrimary),
          ),
        ),
        Container(
          height: 3.h as double,
          color: appLineColor,
        ),
        Container(
          padding:
              EdgeInsets.only(top: 6.h as double, bottom: 6.h as double, left: 15.w as double, right: 15.w as double),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    'payment_string'.tr(),
                    style: primaryTextStyle(size: 16, color: appTextColorGray),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  Text(
                    "${'withdrawal_mr'.tr().toString()} ${MyTools.dealNumber(data.amount! / 100)}",
                    style: primaryTextStyle(size: 16, color: appTextColorPrimary),
                  ),
                ],
              ),
              Offstage(
                offstage: data.use_integral! <= 0,
                child: Container(
                  margin: EdgeInsets.only(top: 4.h as double),
                  child: Row(
                    children: [
                      Text(
                        'bill_payment_success_point_used'.tr(),
                        style: primaryTextStyle(size: 16, color: appTextColorGray),
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      Text(
                        '${data.use_integral}',
                        style: primaryTextStyle(size: 16, color: appTextColorPrimary),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 4.h as double),
                child: Row(
                  children: [
                    Text(
                      'operator_string'.tr(),
                      style: primaryTextStyle(size: 16, color: appTextColorGray),
                    ),
                    Expanded(
                      child: Container(),
                    ),
                    Text(
                      data.operatorName == null ? "" : data.operatorName!,
                      style: primaryTextStyle(size: 16, color: appTextColorPrimary),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 4.h as double),
                child: Row(
                  children: [
                    Text(
                      'bill_payment_success_point_earned'.tr(),
                      style: primaryTextStyle(size: 16, color: appTextColorGray),
                    ),
                    Expanded(
                      child: Container(),
                    ),
                    Text(
                      '${data.gain_integral}',
                      style: primaryTextStyle(size: 16, color: appTextColorPrimary),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        Container(
          height: 3.h as double,
          color: appLineColor,
        ),
      ],
    );
  }

  Widget failChild(String message) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 5.h as double),
          child: Text(
            'withdrawal_fail_title'.tr().toString(),
            style: primaryTextStyle(size: 16, color: appTextColorPrimary),
          ),
        ),
        Text(
          message,
          style: primaryTextStyle(size: 14, color: appTextColorGray),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  Widget bottomChild() {
    return Talk2UsWidget(
      contentString: 'mobile_help_content'.tr(),
      showButton: true,
    );
  }
}
