import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_contact_picker/flutter_native_contact_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/mobileReload/reload_summary_page.dart';
import 'package:selangkah_new/Wallet/models/number_selector.dart';
import 'package:selangkah_new/Wallet/models/operator_model_entity.dart';
import 'package:selangkah_new/Wallet/models/product_model_entity.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class NumberPackageProvider extends BaseProvider {
  OperatorModelEntity? operatorModelEntity;
  NumberSelector? numberSelector;

  NumberPackageProvider(BuildContext buildContext, OperatorModelEntity? operatorModelEntity) : super(buildContext) {
    numberSelector = NumberSelector();
    numberSelector?.operatorModelEntity = operatorModelEntity;
    this.operatorModelEntity = operatorModelEntity;
    mobileController.addListener(() {
      checkCommit();
    });
  }

  TextEditingController mobileController = TextEditingController();
  ScrollController scrollController = ScrollController();

  ProductModelEntity selectItem = ProductModelEntity();
  List<ProductModelEntity> productModelList = [];

  changeSelectItem(ProductModelEntity newSelect) {
    if (selectItem.id != newSelect.id) {
      numberSelector?.productSelector = newSelect;
      selectItem = newSelect;
      checkCommit();
      notifyListeners();
    }
  }

  selectContacter() async {
    mobileController.text = (await selectPhoneNum())!;
    notifyListeners();
  }

  Future<String?> selectPhoneNum() async {
    final status = await Permission.contacts.request();
    if (status.isGranted) {
      final FlutterContactPicker _contactPicker = new FlutterContactPicker();
      Contact? contact = await _contactPicker.selectContact();
      String phoneNumber = "";
      if (contact != null) {
        numberSelector!.contact = contact;
        if (contact.phoneNumbers![0].contains(" ")) {
          phoneNumber = contact.phoneNumbers![0].replaceAll(" ", "");
        } else {
          phoneNumber = contact.phoneNumbers![0];
        }
      }

      return phoneNumber;
    } else {
      showMessage("no_permission".tr());
    }
  }

  checkCommit() {
    if (selectItem != null && selectItem.id != null && selectItem.id != 0 && mobileController.text.length > 0) {
      if (!canCommit) {
        canCommit = true;
        notifyListeners();
      }
    } else {
      if (canCommit) {
        canCommit = false;
        notifyListeners();
      }
    }
  }

  bool canCommit = false;

  jump2Next() async {
    if (numberSelector?.contact == null ||
        numberSelector!.contact!.phoneNumbers![0].replaceAll(" ", "") != mobileController.text.replaceAll(" ", "")) {}
    ResultModel resultModel = await Navigator.push(
      currentContext,
      MaterialPageRoute(
        builder: (context) {
          return ReloadSummaryPage(numberSelector!);
        },
      ),
    );
    if (resultModel != null && resultModel.success!) {
      Navigator.of(currentContext).pop(ResultModel(success: resultModel != null ? resultModel.success : false));
    }
  }

  getProductList() {
    Map map = Map();
    map["provider_name"] = numberSelector?.operatorModelEntity?.providerName;
    map["product_type"] = numberSelector?.operatorModelEntity?.providerType;
    requestNetwork<List<ProductModelEntity>>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.MobileProductList, params: map, onSuccess: (data) {
      if (data != null) {
        productModelList.addAll(data);
        notifyListeners();
      }
    }, onError: (code, msg) {
      Navigator.of(currentContext).pop(ResultModel(success: false));
      showMessage(msg);
    });
  }

  @override
  void initState() {
    super.initState();
    getProductList();
  }
}
