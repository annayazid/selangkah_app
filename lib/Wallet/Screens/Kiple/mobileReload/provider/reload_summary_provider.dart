import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/mobileReload/redeem_points_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/pin/input_pin.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/result_page.dart';
import 'package:selangkah_new/Wallet/components/pop_up_dialog.dart';
import 'package:selangkah_new/Wallet/models/Integral_item_model_entity.dart';
import 'package:selangkah_new/Wallet/models/mobile_result_entity.dart';
import 'package:selangkah_new/Wallet/models/number_selector.dart';
import 'package:selangkah_new/Wallet/models/paymethod_model.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

import '../reload_summary_page.dart';

class ReloadSummaryProvider extends BaseProvider {
  NumberSelector? numberSelector;

  ReloadSummaryState? reloadSummaryState;

  ReloadSummaryProvider(BuildContext buildContext, NumberSelector numberSelector, ReloadSummaryState reloadSummaryState)
      : super(buildContext) {
    this.numberSelector = numberSelector;
    this.reloadSummaryState = reloadSummaryState;
  }

  bool canCommit = false;

  changeSelectPayWay(List<PayWayModel> methods) {
    numberSelector?.payWays = methods;
    if (methods != null && methods.length > 0) {
      if (!canCommit) {
        canCommit = true;
        notifyListeners();
      }
    } else if (canCommit) {
      canCommit = false;
      notifyListeners();
    }
  }

  IntegralItemModelEntity? selectIntegral;

  selectRedeem() async {
    ResultModel selectData = await Navigator.push(currentContext, MaterialPageRoute(builder: (_) {
      return RedeemPointsPage();
    }));
    if (selectData != null && selectData.success!) {
      selectIntegral = selectData.data;
      notifyListeners();
    }
  }

  jump2Next() async {
    showProgressDialog();
    bool? isNotOut = await isNotOutsideMalaysia();
    dismissProgressDialog();
    if (isNotOut == null) {
      Timer.periodic(Duration(milliseconds: 100), (timer) {
        timer.cancel();
        showNotDialog('ekyc_oops_title'.tr(), 'not_location_permission'.tr(), 'ekyc_hard_reject_button'.tr());
      });
    }
    if (isNotOut) {
      Timer.periodic(Duration(milliseconds: 100), (timer) async {
        timer.cancel();
        ResultModel? pinResult = await Navigator.push(currentContext, MaterialPageRoute(builder: (_) {
          return InputPinPage.VerifyPin(
            needBackPinCode: true,
          );
        }));
        if (pinResult != null && pinResult.success!) {
          commitData(pinResult.data);
        }
      });
    } else {
      Timer.periodic(Duration(milliseconds: 100), (timer) {
        timer.cancel();
        showNotDialog('ekyc_oops_title'.tr(), 'out_malaysia_mobile_reload'.tr(), 'ekyc_hard_reject_button'.tr(),
            confirmClickFunction: () {
          Navigator.of(currentContext).pop(ResultModel(success: false));
        });
      });
    }
  }

  showNotDialog(String title, String content, String confirm, {Function? confirmClickFunction}) {
    showDialog(
        context: currentContext,
        builder: (_) {
          return PopUpDialog(
            title: title,
            content: content,
            confirm: confirm,
            confirmFunction: confirmClickFunction,
          );
        });
  }

  commitData(String pinCode) {
    Map map = Map();
    map["pay_type"] = 1;
    map["pin_code"] = pinCode;
    map["product_id"] = numberSelector?.productSelector?.id;
    map["pay_account"] = numberSelector?.contact?.phoneNumbers![0].replaceAll(" ", "");
    map["amount"] = numberSelector?.productSelector?.amount;
    if (selectIntegral != null) {
      map["integral"] = selectIntegral?.integral;
    }

    requestNetwork<MobileResultEntity>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.Commit_MobileReload, params: map, onSuccess: (data) {
      if (data != null) {
        Timer.periodic(Duration(milliseconds: 100), (timer) async {
          timer.cancel();
          ResultModel result = await Navigator.push(currentContext, MaterialPageRoute(builder: (_) {
            return ResultPage(
              contentChild: reloadSummaryState!.successChild(data),
              status: true,
              bottomChild: reloadSummaryState!.bottomChild(),
            );
          }));
          Navigator.of(currentContext).pop(ResultModel(success: true));
        });
      }
    }, onError: (code, msg) {
      Timer.periodic(Duration(milliseconds: 100), (timer) async {
        timer.cancel();
        ResultModel result = await Navigator.push(currentContext, MaterialPageRoute(builder: (_) {
          return ResultPage(
            contentChild: reloadSummaryState!.failChild(msg),
            status: false,
            bottomChild: reloadSummaryState!.bottomChild(),
          );
        }));
        Navigator.of(currentContext).pop(ResultModel(success: true));
      });
    });
  }
}
