import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/mobileReload/number_package_page.dart';
import 'package:selangkah_new/Wallet/components/pop_up_dialog.dart';
import 'package:selangkah_new/Wallet/models/bill_config_entity.dart';
import 'package:selangkah_new/Wallet/models/operator_model_entity.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class SelectOperatorProvider extends BaseProvider {
  SelectOperatorProvider(BuildContext buildContext) : super(buildContext);

  ScrollController scrollController = ScrollController();

  onItemClick(OperatorModelEntity data) async {
    ResultModel? resultModel = await Navigator.push(
      currentContext,
      MaterialPageRoute(
        builder: (context) {
          return NumberPackagePage(data);
        },
      ),
    );
    if (resultModel != null) {
      Navigator.of(currentContext).pop(ResultModel(success: resultModel != null ? resultModel.success : false));
    }
  }

  List<OperatorModelEntity> operatorList = [];

  getOperatorList() {
    Map map = Map();
    map["provider_type"] = 2;
    requestNetwork<List<OperatorModelEntity>>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.Operator_List, params: map, onSuccess: (data) {
      if (data != null) {
        operatorList.addAll(data);
        notifyListeners();
      }
    }, onError: (code, msg) {
      Navigator.of(currentContext).pop(ResultModel(success: false));
      showMessage(msg);
    });
  }

  @override
  void initState() {
    super.initState();
    getTurnConfig();
  }

  getTurnConfig() {
    Map map = Map();
    map["provider_type"] = 2;
    requestNetwork<BillConfigEntity>(Method.post, url: HttpApi.KipleBaseUrl + HttpApi.BillConfig, params: map,
        onSuccess: (data) {
      if (data != null && data.productSwitch == 1) {
        getOperatorList();
      } else {
        Timer.periodic(Duration(milliseconds: 100), (timer) {
          timer.cancel();
          showNotDialog(
              'transfer_no_search_result_title'.tr(), 'no_mobile_function'.tr(), 'ekyc_hard_reject_button'.tr(),
              confirmClickFunction: () {
            Navigator.of(currentContext).pop(ResultModel(success: false));
          });
        });
      }
    }, onError: (code, msg) {
      Navigator.of(currentContext).pop(ResultModel(success: false));
      showMessage(msg);
    });
  }

  showNotDialog(String title, String content, String confirm, {Function? confirmClickFunction}) {
    showDialog(
        context: currentContext,
        builder: (_) {
          return PopUpDialog(
            title: title,
            content: content,
            confirm: confirm,
            confirmFunction: confirmClickFunction,
          );
        });
  }
}
