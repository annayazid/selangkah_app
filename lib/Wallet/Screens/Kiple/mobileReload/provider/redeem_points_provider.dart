import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/models/Integral_item_model_entity.dart';
import 'package:selangkah_new/Wallet/models/integral_model_entity.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class RedeemPointsProvider extends BaseProvider {
  RedeemPointsProvider(BuildContext buildContext) : super(buildContext);

  ScrollController? scrollController = ScrollController();

  IntegralItemModelEntity? selectItem;

  changeSelect(IntegralItemModelEntity newSelect) {
    canCommit = true;
    if (selectItem == null || selectItem?.id != newSelect.id) {
      selectItem = newSelect;
      notifyListeners();
    }
  }

  bool canCommit = false;

  nextStep() {
    Navigator.of(currentContext).pop(ResultModel<IntegralItemModelEntity>(success: true, data: selectItem));
  }

  IntegralModelEntity? dataModel;
  bool integralInfoSuccess = false;

  getIntegralInfo() {
    requestNetwork<IntegralModelEntity>(Method.get,
        url: HttpApi.KipleBaseUrl + HttpApi.GetIntegralInfo, showDialog: false, onSuccess: (data) {
      integralInfoSuccess = true;
      checkSuccess();
      if (data != null) {
        dataModel = data;
        notifyListeners();
      }
    }, onError: (code, msg) {
      errorDismiss();
      showMessage(msg);
    });
  }

  List<IntegralItemModelEntity> dataList = [];
  bool integralListSuccess = false;

  getIntegralList() {
    requestNetwork<List<IntegralItemModelEntity>>(Method.get,
        url: HttpApi.KipleBaseUrl + HttpApi.IntegralSpendList, showDialog: false, onSuccess: (data) {
      integralListSuccess = true;
      checkSuccess();
      if (data != null) {
        dataList.clear();
        dataList.addAll(data);
        notifyListeners();
      }
    }, onError: (code, msg) {
      errorDismiss();
      showMessage(msg);
    });
  }

  errorDismiss() {
    integralInfoSuccess = true;
    integralListSuccess = true;
    checkSuccess();
  }

  checkSuccess() {
    if (integralInfoSuccess && integralListSuccess) {
      dismissProgressDialog();
    }
  }

  @override
  void initState() {
    super.initState();
    showProgressDialog();
    getIntegralInfo();
    getIntegralList();
  }
}
