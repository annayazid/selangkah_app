import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/mobileReload/provider/select_operator_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/mobileReload/select_operator_item.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class SelectOperatorPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SelectOperatorState();
  }
}

class SelectOperatorState extends State<SelectOperatorPage>
    with StateLifeCycle<SelectOperatorPage, SelectOperatorProvider> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    return AppBarWhite(
      title: 'mobile_select_operator'.tr(),
      child: ChangeNotifierProvider<SelectOperatorProvider>(
        create: (BuildContext buildContext) => provider,
        child: Consumer<SelectOperatorProvider>(
          builder: (BuildContext? buildContext, SelectOperatorProvider? selectOperatorProvider, Widget? child) {
            return ListView.builder(
              itemBuilder: (context, position) => buildListItem(position),
              controller: provider.scrollController,
              itemCount: provider.operatorList.length,
            );
          },
        ),
      ),
    );
  }

  late SelectOperatorProvider provider;

  @override
  SelectOperatorProvider createProvider(BuildContext buildContext) {
    provider = SelectOperatorProvider(buildContext);
    return provider;
  }

  Widget buildListItem(int position) {
    return Column(
      children: [
        InkWell(
          child: SelectOperatorItemPage(provider.operatorList[position]),
          onTap: () {
            provider.onItemClick(provider.operatorList[position]);
          },
        ),
        Container(
          height: 0.8.h as double,
          color: appLineColor,
        )
      ],
    );
  }
}
