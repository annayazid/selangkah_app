import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/mobileReload/provider/redeem_points_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_button.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class RedeemPointsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RedeemPointsState();
  }
}

class RedeemPointsState extends State<RedeemPointsPage> with StateLifeCycle<RedeemPointsPage, RedeemPointsProvider> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    return AppBarWhite(
      title: 'mobile_redeem_points'.tr(),
      child: Container(
        margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(top: 7.h as double, bottom: 4.h as double),
              child: Text(
                'mobile_save_using_points'.tr(),
                style: boldTextStyle(
                  color: appTextColorPrimary,
                  size: 16,
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ChangeNotifierProvider<RedeemPointsProvider>(
                  create: (BuildContext buildContext) => provider,
                  child: Consumer<RedeemPointsProvider>(
                    builder: (BuildContext? buildContext, RedeemPointsProvider? redeemPointsProvider, Widget? child) {
                      return Text(
                        provider.dataModel == null ? "0" : "${provider.dataModel?.validIntegral}",
                        style: boldTextStyle(
                          color: appColorPrimaryKiple,
                          size: 16,
                        ),
                      );
                    },
                  ),
                ),
                Text(
                  'mobile_points_available'.tr(),
                  style: primaryTextStyle(
                    color: appTextColorGray,
                    size: 12,
                  ),
                ),
              ],
            ),
            ChangeNotifierProvider<RedeemPointsProvider>(
              create: (BuildContext buildContext) => provider,
              child: Consumer<RedeemPointsProvider>(
                builder: (BuildContext? buildContext, RedeemPointsProvider? redeemPointsProvider, Widget? child) {
                  return Container(
                    child: ListView.builder(
                      itemBuilder: (context, position) => buildListItem(position),
                      controller: provider.scrollController,
                      itemCount: provider.dataList.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                    ),
                  );
                },
              ),
            ),
            Expanded(
              child: Container(),
            ),
            ChangeNotifierProvider<RedeemPointsProvider>(
              create: (BuildContext buildContext) => provider,
              child: Consumer<RedeemPointsProvider>(
                builder: (BuildContext? buildContext, RedeemPointsProvider? redeemPointsProvider, Widget? child) {
                  return Container(
                    margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double, bottom: 25.h as double),
                    child: ClickButton(
                      content: 'redeem_string'.tr(),
                      canClick: provider.canCommit,
                      clickFunction: () {
                        provider.nextStep();
                      },
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  late RedeemPointsProvider provider;

  @override
  RedeemPointsProvider createProvider(BuildContext buildContext) {
    provider = RedeemPointsProvider(buildContext);
    return provider;
  }

  Widget buildListItem(int position) {
    return Container(
      margin: EdgeInsets.only(top: 7.h as double),
      decoration: new BoxDecoration(
        border: new Border.all(
            color: provider.selectItem != null && provider.selectItem?.id == provider.dataList[position].id
                ? appColorPrimaryKiple
                : appTextColorGray,
            width: 0.5),
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(5.0),
        ),
      ),
      child: InkWell(
        child: Container(
          margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double, top: 9.h as double, bottom: 9.h as double),
          child: Row(
            children: [
              Text(
                "${'withdrawal_mr'.tr()} ${provider.dataList[position].amount} off",
                style: primaryTextStyle(
                    size: 16,
                    color: provider.selectItem != null && provider.selectItem?.id == provider.dataList[position].id
                        ? appColorPrimaryKiple
                        : appTextColorGray),
              ),
              Expanded(
                child: Container(),
              ),
              Text(
                "${provider.dataList[position].integral} points",
                style: primaryTextStyle(
                    size: 14,
                    color: provider.selectItem != null && provider.selectItem?.id == provider.dataList[position].id
                        ? appColorPrimaryKiple
                        : appTextColorGray),
              ),
            ],
          ),
        ),
        onTap: () {
          provider.changeSelect(provider.dataList[position]);
        },
      ),
    );
  }
}
