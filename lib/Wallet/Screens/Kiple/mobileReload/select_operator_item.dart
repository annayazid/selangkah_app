import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/models/operator_model_entity.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class SelectOperatorItemPage extends StatelessWidget {
  final OperatorModelEntity operatorModelEntity;

  SelectOperatorItemPage(this.operatorModelEntity);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double, top: 11.h as double, bottom: 11.h as double),
      child: Row(
        children: [
          Container(
            child: Text(
              operatorModelEntity.providerName!,
              style: primaryTextStyle(size: 16, color: appTextColorPrimary),
            ),
          )
        ],
      ),
    );
  }
}
