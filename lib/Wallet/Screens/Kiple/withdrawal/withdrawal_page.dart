import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/personal_details_item_widget.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/withdrawal/provider/withdrawal_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/Utils/passport_format.dart';
import 'package:selangkah_new/Wallet/Utils/precision_limit_formatter.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_button.dart';
import 'package:selangkah_new/Wallet/components/talk_to_us.dart';
import 'package:selangkah_new/Wallet/models/withdrawal_result_model_entity.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class WithDrawalPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return WithDrawalState();
  }
}

class WithDrawalState extends State<WithDrawalPage> with StateLifeCycle<WithDrawalPage, WithDrawalProvider> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    return AppBarWhite(
        title: 'withdrawal_title'.tr().toString(),
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(left: 7.w as double, right: 7.w as double),
            child: Column(
              children: [
                PersonalDetailsItem(
                  titleString: 'withdrawal_recipient_name'.tr().toString(),
                  hintString: 'withdrawal_recipient_name_hint'.tr().toString(),
                  controller: provider.nameController,
                  textColor: appTextColorGray,
                  filedEnabled: false,
                  hideRightImage: true,
                  spacing: 2.h as double,
                  keyboardType: TextInputType.text,
                ),
                PersonalDetailsItem(
                  titleString: 'withdrawal_ic_passport'.tr().toString(),
                  hintString: 'withdrawal_ic_passport_hint'.tr().toString(),
                  controller: provider.passportController,
                  textColor: appTextColorGray,
                  filedEnabled: provider.userProfileEntity == null ||
                      provider.userProfileEntity?.idNumber == null ||
                      provider.userProfileEntity!.idNumber!.isEmpty,
                  hideRightImage: true,
                  spacing: 2.h as double,
                  keyboardType: TextInputType.number,
                  inputFormatters: [PassportFormat()],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 7.h as double,
                    ),
                    Text(
                      'withdrawal_amount'.tr().toString(),
                      style: TextStyle(color: appTextColorPrimary, fontSize: 14),
                    ),
                    SizedBox(
                      height: 2.h as double,
                    ),
                    ChangeNotifierProvider<WithDrawalProvider>(
                      create: (BuildContext buildContext) => provider,
                      child: Consumer<WithDrawalProvider>(
                        builder: (BuildContext? buildContext, WithDrawalProvider? withDrawalProvider, Widget? child) {
                          return Text(
                            provider.freeConfigEntity == null
                                ? 'withdrawal_amount_notice'.tr().toString()
                                : 'withdrawal_amount_notice'.tr().toString().replaceFirst('0.00',
                                    '${MyTools.dealNumber(provider.freeConfigEntity?.withdrawValue == null ? 0 : double.parse(provider.freeConfigEntity!.withdrawValue!) / 100)}'),
                            style: TextStyle(color: appColorPrimaryKiple, fontSize: 12),
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      height: 2.h as double,
                    ),
                    Container(
                      height: 21.h as double,
                      decoration:
                          BoxDecoration(color: appTextColorGrayBack, borderRadius: BorderRadius.circular((4.0))),
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                              left: 4.w as double,
                            ),
                            child: Text(
                              "${'withdrawal_mr'.tr().toString()}",
                              style: TextStyle(fontSize: 12, color: appTextColorGrayhint),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(left: 2.w as double),
                              child: ChangeNotifierProvider<WithDrawalProvider>(
                                create: (_) => provider,
                                child: Consumer<WithDrawalProvider>(
                                  builder: (BuildContext? buildContext, WithDrawalProvider? withdrawProvider,
                                      Widget? child) {
                                    return Container(
                                      alignment: Alignment.center,
                                      child: TextField(
                                        textAlign: TextAlign.left,
                                        cursorColor: appColorPrimaryKiple,
                                        controller: TextEditingController.fromValue(TextEditingValue(
                                            text: '${provider.amount}',
                                            selection: TextSelection.fromPosition(TextPosition(
                                                affinity: TextAffinity.downstream,
                                                offset: '${provider.amount}'.length)))),
                                        keyboardType: TextInputType.number,
                                        decoration: InputDecoration(
                                          isDense: true,
                                          contentPadding: EdgeInsets.all(0),
                                          hintText: "00.00",
                                          hintStyle: primaryTextStyle(color: appTextColorGrayhint, size: 12),
                                          border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.transparent,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.transparent,
                                            ),
                                          ),
                                          disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.transparent,
                                            ),
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.transparent,
                                            ),
                                          ),
                                        ),
                                        style: primaryTextStyle(color: appTextColorPrimary, size: 14),
                                        inputFormatters: [
                                          PrecisionLimitFormatter(3),
                                          FilteringTextInputFormatter(RegExp("[0-9.]"), allow: true),
                                        ],
                                        onChanged: (content) {
                                          provider.changeShowText(content);
                                        },
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                // PersonalDetailsItem(
                //   titleString: 'withdrawal_amount'.tr().toString(),
                //   hintString: "0.00",
                //   controller: provider.amountController,
                //   textColor: appTextColorPrimary,
                //   filedEnabled: true,
                //   hideRightImage: true,
                //   hideNotice: false,
                //   noticeText: 'withdrawal_amount_notice'.tr().toString(),
                //   spacing: 2.h,
                //   hideLfetText: false,
                //   leftTextStr: 'withdrawal_mr'.tr().toString(),
                //   leftTextColor: provider.amountController.text.length > 0
                //       ? appTextColorGray
                //       : appTextColorGrayhint,
                //   keyboardType: TextInputType.number,
                //   inputFormatters: [MoneyFormat()],
                // ),
                PersonalDetailsItem(
                  titleString: 'withdrawal_bank_name'.tr().toString(),
                  hintString: 'withdrawal_bank_name_hint'.tr().toString(),
                  controller: provider.bankController,
                  textColor: appTextColorPrimary,
                  filedEnabled: false,
                  spacing: 2.h as double,
                  clickFunction: () {
                    if (provider.bankList != null) {
                      provider.showPub('withdrawal_bank_name_hint'.tr(), provider.bankList!, provider.clickItem);
                    }
                  },
                ),
                PersonalDetailsItem(
                  titleString: 'withdrawal_bank_account_number'.tr().toString(),
                  hintString: 'withdrawal_enter_account_number'.tr().toString(),
                  controller: provider.bankNumberController,
                  textColor: appTextColorPrimary,
                  filedEnabled: true,
                  hideRightImage: true,
                  spacing: 2.h as double,
                  keyboardType: TextInputType.number,
                ),
                PersonalDetailsItem(
                  titleString: 'withdrawal_purpose_of'.tr().toString(),
                  hintString: 'withdrawal_purpose_of_hint'.tr().toString(),
                  controller: provider.purposeController,
                  textColor: appTextColorPrimary,
                  filedEnabled: true,
                  hideRightImage: true,
                  hideLeftWidget: true,
                  spacing: 2.h as double,
                  inputMaxLines: 3,
                  isLins: true,
                  contentCountProvider: provider.purposeContentCountProvider,
                  keyboardType: TextInputType.text,
                ),

                Container(
                  margin: EdgeInsets.only(top: 3.h as double, bottom: 3.h as double),
                  child: Text(
                    "withdraw_notice_string".tr(),
                    style: TextStyle(fontSize: 13, color: appTextColorPrimary),
                  ),
                ),

                ChangeNotifierProvider<WithDrawalProvider>(
                  create: (BuildContext buildContext) => provider,
                  child: Consumer<WithDrawalProvider>(
                    builder: (BuildContext? buildContext, WithDrawalProvider? withDrawalProvider, Widget? child) {
                      return Container(
                        margin: EdgeInsets.only(
                            top: 50.h as double, bottom: 25.h as double, left: 7.w as double, right: 7.w as double),
                        child: ClickButton(
                          content: 'submit'.tr().toString(),
                          canClick: provider.canCommit,
                          clickFunction: () {
                            provider.submit();
                          },
                          checkFunction: () {
                            provider.checkCommit();
                          },
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  late WithDrawalProvider provider;

  @override
  WithDrawalProvider createProvider(BuildContext buildContext) {
    provider = WithDrawalProvider(buildContext, this);
    provider.initListener();
    return provider;
  }

  Widget successChild(WithdrawalResultModelEntity data) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 5.h as double),
          child: Text(
            'withdrawal_submitted'.tr().toString(),
            style: primaryTextStyle(size: 16, color: appTextColorPrimary),
          ),
        ),
        Text(
          'withdrawal_submitted_content'.tr().toString(),
          style: primaryTextStyle(size: 14, color: appTextColorGray),
        ),
        Container(
          margin: EdgeInsets.only(top: 7.h as double, bottom: 2.h as double),
          child: Text(
            'withdrawal_receipt_id'.tr().toString(),
            style: primaryTextStyle(size: 14, color: appTextColorGray),
          ),
        ),
        Text(
          data.order_number!,
          style: primaryTextStyle(size: 14, color: appTextColorGray),
        ),
        Container(
          margin: EdgeInsets.only(top: 7.h as double, bottom: 7.h as double),
          child: Text(
            MyTools.formatDate(DateTime.parse(data.trade_time!)),
            style: primaryTextStyle(size: 14, color: appTextColorPrimary),
          ),
        ),
        Container(
          height: 3.h as double,
          color: appLineColor,
        ),
        Container(
          padding:
              EdgeInsets.only(top: 6.h as double, bottom: 6.h as double, left: 15.w as double, right: 15.w as double),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    'bank_string'.tr().toString(),
                    style: primaryTextStyle(size: 16, color: appTextColorGray),
                  ),
                  Expanded(child: Container()),
                  Text(
                    data.bankName!,
                    style: primaryTextStyle(size: 16, color: appTextColorPrimary),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 4.h as double),
                child: Row(
                  children: [
                    Text(
                      'amount_string'.tr().toString(),
                      style: primaryTextStyle(size: 16, color: appTextColorGray),
                    ),
                    Expanded(child: Container()),
                    Text(
                      "${'withdrawal_mr'.tr().toString()} ${MyTools.dealNumber(data.amount! / 100)}",
                      style: primaryTextStyle(size: 16, color: appTextColorPrimary),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 4.h as double),
                child: Row(
                  children: [
                    Text(
                      'withdrawal_fee'.tr().toString(),
                      style: primaryTextStyle(size: 16, color: appTextColorGray),
                    ),
                    Expanded(child: Container()),
                    Text(
                      "${'withdrawal_mr'.tr().toString()} ${MyTools.dealNumber(int.parse(data.fee!) / 100)}",
                      style: primaryTextStyle(size: 16, color: appTextColorPrimary),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 7.h as double, bottom: 7.h as double),
                color: appLineColor,
                height: 1.h as double,
              ),
              Container(
                child: Row(
                  children: [
                    Text(
                      'balance_string'.tr().toString(),
                      style: primaryTextStyle(size: 16, color: appTextColorGray),
                    ),
                    Expanded(child: Container()),
                    Text(
                      "${'withdrawal_mr'.tr().toString()} ${MyTools.dealNumber(int.parse(data.balance!) / 100)}",
                      style: primaryTextStyle(size: 16, color: appTextColorPrimary),
                    ),
                  ],
                ),
              ),
              Text(
                'withdrawal_after'.tr().toString(),
                style: primaryTextStyle(size: 10, color: appTextColorGray),
              ),
            ],
          ),
        ),
        Container(
          height: 3.h as double,
          color: appLineColor,
        ),
      ],
    );
  }

  Widget failChild({String? failMsg}) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 5.h as double),
          child: Text(
            'withdrawal_fail_title'.tr().toString(),
            style: primaryTextStyle(size: 16, color: appTextColorPrimary),
          ),
        ),
        Text(
          failMsg == null || failMsg.isEmpty ? 'withdrawal_fail_content'.tr().toString() : failMsg,
          style: primaryTextStyle(size: 14, color: appTextColorGray),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  Widget bottomChild() {
    return Talk2UsWidget(contentString: 'withdrawal_need_help'.tr().toString());
  }
}
