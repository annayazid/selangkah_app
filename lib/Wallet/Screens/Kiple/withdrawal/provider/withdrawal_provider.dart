import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/ekyc_bottom_sheet.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/provider/content_count_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/pin/input_pin.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/result_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/withdrawal/withdrawal_page.dart';
import 'package:selangkah_new/Wallet/Utils/app_helper.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/models/free_config_entity.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/Wallet/models/user_profile_entity.dart';
import 'package:selangkah_new/Wallet/models/withdrawal_result_model_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class WithDrawalProvider extends BaseProvider {
  WithDrawalState? withDrawalState;

  WithDrawalProvider(BuildContext buildContext, WithDrawalState withDrawalState)
      : super(buildContext) {
    purposeContentCountProvider = ContentCountProvider(buildContext);
    this.withDrawalState = withDrawalState;
  }

  TextEditingController nameController = TextEditingController();
  TextEditingController passportController = TextEditingController();
  TextEditingController bankController = TextEditingController();
  TextEditingController bankNumberController = TextEditingController();
  TextEditingController purposeController = TextEditingController();

  ContentCountProvider? purposeContentCountProvider;
  String inputPassportStr = "";

  initListener() {
    nameController.addListener(() {
      checkCanCommit();
    });
    passportController.addListener(() {
      String content = passportController.text.replaceAll("-", "");
      if (content.length > inputPassportStr.length) {
        inputPassportStr = content;
      }
    });
    bankController.addListener(() {
      checkCanCommit();
    });
    bankNumberController.addListener(() {
      checkCanCommit();
    });
    purposeController.addListener(() {
      controllerCounter(purposeController, 50);
      purposeContentCountProvider?.changeContent(purposeController.text);
      checkCanCommit();
    });
  }

  String amount = "";

  changeShowText(String text) {
    if (text.length > 0) {
      if (text.length == 1) {
        amount = (double.parse(text) / 100).toStringAsFixed(2);
      } else {
        double newValue = double.parse(text);
        if (newValue == 0) {
          amount = "";
        } else {
          if (text.substring(text.lastIndexOf(".") + 1, text.length).length ==
                  2 &&
              amount
                      .substring(amount.lastIndexOf(".") + 1, amount.length)
                      .length ==
                  1) {
            amount = text;
          } else if (!text.endsWith(".") &&
              text.contains(".") &&
              (text.substring(text.lastIndexOf(".") + 1, text.length).length >=
                      1 &&
                  amount
                          .substring(amount.lastIndexOf(".") + 1, amount.length)
                          .length >=
                      1) &&
              (double.parse(
                        text.substring(0, text.length - 1),
                      ) ==
                      double.parse(amount) ||
                  double.parse(text) ==
                      double.parse(amount.substring(0, amount.length - 1)))) {
            if (text.length > amount.length) {
              amount = (newValue * 10).toStringAsFixed(2);
            } else if (text.length < amount.length) {
              amount = (newValue / 10).toStringAsFixed(2);
            }
          } else {
            amount = text;
          }
        }
      }
    } else {
      amount = '';
    }
    notifyListeners();
    checkCanCommit();
  }

  bool canCommit = false;

  checkCanCommit() {
    if (nameController.text.length > 0 &&
        inputPassportStr.length > 0 &&
        amount.length > 0 &&
        bankController.text.length > 0 &&
        bankNumberController.text.length > 0 &&
        purposeController.text.length > 0) {
      if (!canCommit) {
        canCommit = true;
        notifyListeners();
      }
    } else {
      if (canCommit) {
        canCommit = false;
        notifyListeners();
      }
    }
  }

  controllerCounter(TextEditingController controller, int maxLength) {
    if (controller.text.length > maxLength) {
      controller.text = controller.text.substring(0, maxLength);
      controller.selection = TextSelection.fromPosition(
          TextPosition(offset: controller.text.length));
    }
  }

  clickItem(String name) {
    bankController.text = name;
  }

  checkCommit() {
    if (nameController.text.length <= 0) {
      showMessage(
          "${'please_enter_string'.tr()} ${'withdrawal_recipient_name'.tr().toString()}");
      return;
    }
    if (inputPassportStr.length <= 0) {
      showMessage(
          "${'please_enter_string'.tr()} ${'withdrawal_ic_passport'.tr().toString()}");
      return;
    }
    if (int.parse(amount) <= 0) {
      showMessage(
          "${'please_enter_string'.tr()} ${'withdrawal_amount'.tr().toString()}");
      return;
    }
    if (bankController.text.length <= 0) {
      showMessage(
          "${'please_enter_string'.tr()} ${'withdrawal_bank_name'.tr().toString()}");
      return;
    }
    if (bankNumberController.text.length <= 0) {
      showMessage(
          "${'please_enter_string'.tr()} ${'withdrawal_bank_account_number'.tr().toString()}");
      return;
    }
    if (purposeController.text.length <= 0) {
      showMessage(
          "${'please_enter_string'.tr()} ${'withdrawal_purpose_of'.tr().toString()}");
      return;
    }
  }

  showPub(String title, List<String> data, Function itemClick) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        isDismissible: false,
        context: currentContext,
        builder: (BuildContext context) {
          return EkycBottomSheet(title, data, itemClick);
        });
  }

  submit() async {
    ///close keyboard
    FocusScope.of(currentContext).requestFocus(FocusNode());
    ResultModel changedResult = await Navigator.push(
      currentContext,
      MaterialPageRoute(
        builder: (context) {
          return InputPinPage.VerifyPin();
        },
      ),
    );
    if (changedResult != null && changedResult.success!) {
      commitData();
    }
  }

  @override
  Future<void> initState() async {
    super.initState();
    loadData();
  }

  loadData() async {
    showProgressDialog();
    Future.wait([getBankList(), getUserProfile(), getFreeConfig()])
        .then((value) => () {
              dismissProgressDialog();
            })
        .catchError(() {
      dismissProgressDialog();
    });
  }

  commitData() async {
    showProgressDialog();
    Map map = Map();
    map["payee_name"] = nameController.text;
    map["mobile"] = await SecureStorage().readSecureData("userPhoneNo");
    map["payee_bank_account_no"] = bankNumberController.text;
    map["payee_bank_name"] = bankController.text;
    map["payee_identification_id"] = inputPassportStr;
    map["reason"] = purposeController.text;
    map["amount"] = int.parse(
        MyTools.dealNumber(double.parse(amount) * 100.toInt(), pointLength: 0));
    Map deviceMsg = await AppHelper().getDeviceMap();
    if (deviceMsg != null && deviceMsg.length > 0) {
      map.addAll(deviceMsg);
    }
    requestNetwork<WithdrawalResultModelEntity>(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.Post_Withdrawal,
        showDialog: false,
        params: map, onSuccess: (data) async {
      dismissProgressDialog();
      if (data != null) {
        Timer.periodic(Duration(milliseconds: 100), (timer) async {
          timer.cancel();
          ResultModel resultModel = await Navigator.push(
            currentContext,
            MaterialPageRoute(
              builder: (context) {
                return ResultPage(
                  contentChild: withDrawalState!.successChild(data),
                  bottomChild: withDrawalState!.bottomChild(),
                  status: true,
                );
              },
            ),
          );
          Navigator.of(currentContext).pop(ResultModel(success: true));
        });
      }
    }, onError: (code, msg) {
      dismissProgressDialog();
      Timer.periodic(Duration(milliseconds: 100), (timer) async {
        timer.cancel();
        Navigator.push(
          currentContext,
          MaterialPageRoute(
            builder: (context) {
              return ResultPage(
                contentChild: withDrawalState!.failChild(failMsg: msg),
                bottomChild: withDrawalState!.bottomChild(),
                status: false,
              );
            },
          ),
        );
      });
      // showMessage(msg);
    });
  }

  List<String>? bankList;

  Future<String> getBankList() {
    asyncRequestNetwork<List<String>>(Method.get,
        showDialog: false,
        url: HttpApi.KipleBaseUrl + HttpApi.Bank_List, onSuccess: (data) {
      if (data != null) {
        bankList = data;
        dismissDialog();
      }
    }, onError: (code, msg) {
      dismissProgressDialog();
      Navigator.of(currentContext).pop(ResultModel(success: false));
      showMessage(msg);
    });
    return Future.value("");
  }

  UserProfileEntity? userProfileEntity;

  Future getUserProfile() {
    asyncRequestNetwork<UserProfileEntity>(Method.get,
        url: HttpApi.KipleBaseUrl + HttpApi.GetUserProfile,
        showDialog: false, onSuccess: (data) {
      if (data != null) {
        userProfileEntity = data;
        nameController.text = userProfileEntity!.fullName!;
        passportController.text = userProfileEntity!.idNumber!;
        dismissDialog();
      }
    }, onError: (code, msg) {
      dismissProgressDialog();
      Navigator.of(currentContext).pop(ResultModel(success: false));
      showMessage(msg);
    });
    return Future.value("");
  }

  FreeConfigEntity? freeConfigEntity;

  Future getFreeConfig() {
    asyncRequestNetwork<FreeConfigEntity>(Method.get,
        url: HttpApi.KipleBaseUrl + HttpApi.GetFreeConfig,
        showDialog: false, onSuccess: (data) {
      if (data != null) {
        freeConfigEntity = data;
        notifyListeners();
        dismissDialog();
      }
    }, onError: (code, msg) {
      dismissProgressDialog();
      Navigator.of(currentContext).pop(ResultModel(success: false));
      showMessage(msg);
    });
    return Future.value("");
  }

  dismissDialog() {
    if (bankList != null &&
        userProfileEntity != null &&
        freeConfigEntity != null) {
      dismissProgressDialog();
    }
  }
}
