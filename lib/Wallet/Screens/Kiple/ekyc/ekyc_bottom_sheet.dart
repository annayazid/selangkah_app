import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class EkycBottomSheet extends StatefulWidget {
  final List<dynamic> data;
  final String title;
  final Function itemClick;

  EkycBottomSheet(this.title, this.data, this.itemClick) {}

  @override
  State<StatefulWidget> createState() {
    return EkycBottomSheetState();
  }
}

class EkycBottomSheetState extends State<EkycBottomSheet> {
  ScrollController? scrollController;

  EkycBottomSheetState() {
    scrollController = ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0))),
      height: 162.h as double,
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.all(7.w as double),
                  child: Text(
                    widget.title,
                    style: TextStyle(color: appTextColorPrimary, fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              InkWell(
                child: Container(
                  child: IconButton(
                    icon: Icon(
                      Icons.clear,
                      color: appTextColorGray,
                      size: 20,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                onTap: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemBuilder: (context, position) => buildListItem(position),
              controller: scrollController,
              itemCount: widget.data.length,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildListItem(int position) {
    return Column(
      children: [
        InkWell(
          child: Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(7.w as double),
            child: Text(
              widget.data[position] is String ? widget.data[position] : widget.data[position].value,
              style: TextStyle(color: appTextColorPrimary, fontSize: 16),
            ),
          ),
          onTap: () {
            widget.itemClick.call(widget.data[position]);
            Navigator.of(context).pop();
          },
        ),
        Container(
          height: 0.8.h as double,
          color: appLineColor,
        )
      ],
    );
  }
}
