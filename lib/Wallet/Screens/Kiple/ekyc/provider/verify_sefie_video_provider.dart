import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/ekyc_verify_detail.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/oops_page.dart';
import 'package:selangkah_new/Wallet/Utils/channel_tool.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/models/scan_back_model_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class VerifySelfieVideoProvider extends BaseProvider {
  VerifyModel? para;
  var images;

  VerifySelfieVideoProvider(BuildContext buildContext) : super(buildContext);

  getFaceImage() {
    ChannelPlatformTool.startScanIdCard(ChannelPlatformTool.EKYC_GET_FACE_IMAGES).then((value) {
      if (value != null) {
        ScanBackModelEntity callBack;
        if (value is String) {
          value = json.decode(value);
        }
        callBack = JsonConvert.fromJsonAsT<ScanBackModelEntity>(MyTools.transferValue(value))!;
        if (callBack.success != null && callBack.success! && callBack.status == 'image_face') {
          if (callBack.data is String) {
            images = changeListFromJson(callBack.data);
          } else {
            images = callBack.data;
          }
        }
        notifyListeners();
      }
    });
  }

  List<String> changeListFromJson(String str) => List<String>.from(json.decode(str).map((x) => x));

  String changeListToJson(List<String> data) => json.encode(List<dynamic>.from(data.map((x) => x)));

  submitVideoAction() {
    Map para = {"onboarding_id": "${this.para?.result['mOnBoardingID']}"};

    requestNetwork(Method.post, params: para, url: HttpApi.KipleBaseUrl + HttpApi.EkycComplete, onSuccess: (data) {
      Future.delayed(Duration(microseconds: 0), () {
        Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
          return OopsPage(ekycSuccess: true);
        }));
      });
    }, onError: (code, message) {
      showMessage(message);
    });
  }

  String getFaceImagePath() {
    if (images != null && images.length > 0) {
      return images[0];
    }
    getFaceImage();
    return '';
  }

  retakeVideo() {
    Navigator.pop(currentContext, {'toScan': true});
  }
}
