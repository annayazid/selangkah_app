import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/ekyc_verify_detail.dart';
import 'package:selangkah_new/Wallet/Utils/channel_tool.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/models/scan_back_model_entity.dart';

class LandingProvider extends BaseProvider {
  LandingProvider(BuildContext buildContext) : super(buildContext);

  int? scanType;

  startVerifyEkyc() {
    showProgressDialog();
    ChannelPlatformTool.startScanIdCard(ChannelPlatformTool.INIT_EKYC, context: currentContext).then((value) {
      dismissProgressDialog();
      if (value != null) {
        ScanBackModelEntity model;
        if (value is String) {
          value = json.decode(value);
        }
        model = JsonConvert.fromJsonAsT<ScanBackModelEntity>(MyTools.transferValue(value))!;
        if (model.success != null && model.success!) {
          goToVerifyEkyc();
        } else {
          if (model.message != null) showMessage(model.message!);
        }
      }
    });
  }

  goToVerifyEkyc() {
    if (scanType == 0) {
      ChannelPlatformTool.startScanIdCard(ChannelPlatformTool.EKYC_TYPE_ID).then((value) {
        if (value != null) {
          ScanBackModelEntity callBack;
          if (value is String) {
            value = json.decode(value);
          }
          callBack = JsonConvert.fromJsonAsT<ScanBackModelEntity>(MyTools.transferValue(value))!;

          if (callBack.status == 'close_page') {
          } else {
            if (callBack.success != null && callBack.success! && callBack.status == 'ic') {
              VerifyModel model = VerifyModel(0, result: callBack.data);
              gotoVerifyPage(model);
            } else {
              log('${callBack.message}');
              showMessage('${callBack.message}');
            }
          }
        }
      });
    } else if (scanType == 1) {
      ChannelPlatformTool.startScanIdCard(ChannelPlatformTool.EKYC_TYPE_PASSPORT).then((value) {
        if (value != null) {
          ScanBackModelEntity callBack;
          if (value is String) {
            value = json.decode(value);
          }
          callBack = JsonConvert.fromJsonAsT<ScanBackModelEntity>(MyTools.transferValue(value))!;
          if (callBack.status == 'close_page') {
          } else {
            if (callBack.success != null && callBack.success! && callBack.status == 'passport') {
              VerifyModel model = VerifyModel(1, result: callBack.data);
              gotoVerifyPage(model);
            } else {}
          }
        }
      });
    }
  }

  gotoVerifyPage(VerifyModel model) {
    Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
      return VerifyDetailNew(model);
    })).then((value) {
      if (value != null) {
        if (value['toScan']) {
          goToVerifyEkyc();
        } else if (value['changeIdType']) {
          scanType = 0;
          notifyListeners();
        }
      }
    });
  }
}
