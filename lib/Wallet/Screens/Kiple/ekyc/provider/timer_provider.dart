import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';

class TimerProvider extends BaseProvider {
  TimerProvider(BuildContext buildContext) : super(buildContext);

  Timer? _timer;
  bool isResendOTP = true;
  String _codeStr = 'verify_phone_number_resend'.tr().toString();

  String get codeStr => _codeStr;

  set codeStr(String value) {
    _codeStr = value;
  }

  Timer get timer => _timer!;

  set timer(Timer value) {
    _timer = value;
  }

  void controlTimer() {
    int _seconds = 60;
    isResendOTP = false;
    if (_timer == null || !_timer!.isActive) {
      _timer = Timer.periodic(Duration(seconds: 1), (timer) {
        _seconds--;
        _codeStr =
            '${'verify_phone_number_resend'.tr().toString()}(' + '${_seconds > 9 ? _seconds : "0${_seconds}"}' + 's)';
        if (_seconds <= 0) {
          _timer?.cancel();
          isResendOTP = true;
          _codeStr = 'verify_phone_number_resend'.tr().toString();
        }
        notifyListeners();
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }
}
