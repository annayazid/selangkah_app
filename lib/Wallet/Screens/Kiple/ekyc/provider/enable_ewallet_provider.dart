import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/personal_details.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';

import '../../otp/verify_phone_number.dart';

class EnableEWalletProvider extends BaseProvider {
  EnableEWalletProvider(BuildContext buildContext) : super(buildContext);

  jump2Next() async {
    ResultModel result = await Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
      return VerifyPhoneNumberPage(OtpType.UpdateProfile);
    }));
    if (result != null && result.success!) {
      Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
        return PersonalDetailsPage();
      }));
    }
  }
}
