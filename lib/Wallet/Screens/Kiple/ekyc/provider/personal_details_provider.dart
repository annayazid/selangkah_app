import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/landing_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/provider/content_count_provider.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/models/occupation_list_model_entity.dart';
import 'package:selangkah_new/Wallet/models/select_item.dart';
import 'package:selangkah_new/Wallet/models/sso_user_information_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

import '../ekyc_bottom_sheet.dart';

class PersonalDetailsProvider extends BaseProvider {
  PersonalDetailsProvider(BuildContext buildContext) : super(buildContext) {
    address1ContentCountProvider = ContentCountProvider(buildContext);
    address2ContentCountProvider = ContentCountProvider(buildContext);
    cityContentCountProvider = ContentCountProvider(buildContext);
    postcodeContentCountProvider =
        ContentCountProvider(buildContext, maxLength: 5);
    initData();
  }

  initData() async {
    String? email = await SecureStorage().readSecureData("email");
    if (email == null || email == "null") {
      email = '';
    }
    emailController.text = email;
    countryController.text = await SecureStorage().readSecureData("country");
    if (countryController.text == null || "" == countryController.text) {
      countryController.text = "Malaysia";
    }
    mobileController.text = await SecureStorage().readSecureData("userPhoneNo");
  }

  TextEditingController occupationController = TextEditingController();
  TextEditingController positionController = TextEditingController();
  TextEditingController businessTypeController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController address1Controller = TextEditingController();
  TextEditingController address2Controller = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController stateController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController postcodeController = TextEditingController();

  ContentCountProvider? address1ContentCountProvider;
  ContentCountProvider? address2ContentCountProvider;
  ContentCountProvider? cityContentCountProvider;
  ContentCountProvider? postcodeContentCountProvider;

  initListener() {
    occupationController.addListener(() {
      checkCanCommit();
    });
    positionController.addListener(() {
      checkCanCommit();
    });
    businessTypeController.addListener(() {
      checkCanCommit();
    });
    emailController.addListener(() {
      checkCanCommit();
    });
    mobileController.addListener(() {
      checkCanCommit();
    });

    address1Controller.addListener(() {
      controllerCounter(address1Controller, 50);
      address1ContentCountProvider?.changeContent(address1Controller.text);
      checkCanCommit();
    });
    address2Controller.addListener(() {
      controllerCounter(address2Controller, 50);
      address2ContentCountProvider?.changeContent(address2Controller.text);
      checkCanCommit();
    });
    countryController.addListener(() {
      controllerCounter(countryController, 50);
      checkCanCommit();
    });
    stateController.addListener(() {
      checkCanCommit();
    });
    cityController.addListener(() {
      controllerCounter(cityController, 50);
      cityContentCountProvider?.changeContent(cityController.text);
      checkCanCommit();
    });
    postcodeController.addListener(() {
      controllerCounter(postcodeController, 5);
      postcodeContentCountProvider?.changeContent(postcodeController.text);
      checkCanCommit();
    });
  }

  bool canCommit = false;

  checkCanCommit() {
    replaceSpace();
    if (selectItemOccupation != null &&
        selectItemPosition != null &&
        selectItemBusiness != null &&
        mobileController.text.length > 5 &&
        address1Controller.text.length > 0 &&
        selectItemStates != null &&
        cityController.text.length > 0 &&
        postcodeController.text.length > 4 &&
        countryController.text.length > 0 &&
        MyTools.checkEmailFormat(emailController)) {
      if (!canCommit) {
        canCommit = true;
        notifyListeners();
      }
    } else {
      if (canCommit) {
        canCommit = false;
        notifyListeners();
      }
    }
  }

  notCommitShow() {
    replaceSpace();
    if (!MyTools.checkEmailFormat(emailController)) {
      showMessage("not_format_email".tr());
    }
    if (postcodeController.text.length < 5) {
      showMessage("postcode_format_notice_string".tr());
    }
  }

  replaceSpace() {
    if (emailController.text.contains(" ")) {
      emailController.text = emailController.text.replaceAll(" ", "");
      emailController.selection = TextSelection.fromPosition(
          TextPosition(offset: emailController.text.length));
    }
  }

  controllerCounter(TextEditingController controller, int maxLength) {
    if (controller.text.length > maxLength) {
      controller.text = controller.text.substring(0, maxLength);
      controller.selection = TextSelection.fromPosition(
          TextPosition(offset: controller.text.length));
    }
  }

  SelectItem? selectItemOccupation;

  clickOccupationItem(SelectItem selectItem) {
    selectItemOccupation = selectItem;
    occupationController.text = selectItem.value;
  }

  String? selectItemPosition;

  clickPositionItem(String selectItem) {
    selectItemPosition = selectItem;
    positionController.text = selectItem;
  }

  SelectItem? selectCountry;

  clickCountryItem(SelectItem selectItem) {
    selectCountry = selectItem;
    countryController.text = selectItem.value;
  }

  SelectItem? selectItemBusiness;

  clickBusinessItem(SelectItem selectItem) {
    selectItemBusiness = selectItem;
    businessTypeController.text = selectItem.value;
  }

  SelectItem? selectItemStates;

  clickStatesItem(SelectItem selectItem) {
    selectItemStates = selectItem;
    stateController.text = selectItem.value;
  }

  showPub(String title, List<dynamic> data, Function itemClick) {
    ///close keyboard
    FocusScope.of(currentContext).requestFocus(FocusNode());
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        isDismissible: false,
        context: currentContext,
        builder: (BuildContext context) {
          return EkycBottomSheet(title, data, itemClick);
        });
  }

  @override
  void initState() {
    super.initState();
    showProgressDialog();
    requestData();
    requestPositionList();
    requestSSOInformation();
  }

  List<SelectItem> nationalitiesData = [];
  List<SelectItem> natureBusinessesData = [];
  List<SelectItem> occupationsData = [];
  List<SelectItem> statesData = [];
  List<String> positionData = [];
  SsoUserInformationEntity? ssoUserInfo;

  bool requestDataSuccess = false;

  requestData() {
    requestNetwork<OccupationListModelEntity>(Method.get,
        showDialog: false,
        url: HttpApi.KipleBaseUrl + HttpApi.OccupationList, onSuccess: (data) {
      if (data != null) {
        requestDataSuccess = true;
        checkDismissDialog();
        map2List(data.nationalities!, nationalitiesData);
        map2List(data.natureBusinesses!, natureBusinessesData);
        map2List(data.occupations!, occupationsData, needSort: true);
        map2List(data.states!, statesData);
      }
    }, onError: (code, msg) {
      showMessage(msg);
    });
  }

  bool requestPositionSuccess = false;

  requestPositionList() {
    requestNetwork<List<String>>(Method.get,
        showDialog: false,
        url: HttpApi.KipleBaseUrl + HttpApi.PositionList, onSuccess: (data) {
      if (data != null) {
        requestPositionSuccess = true;
        checkDismissDialog();
        positionData.addAll(data);
      }
    }, onError: (code, msg) {
      showMessage(msg);
    });
  }

  requestSSOInformation() {
    requestNetwork<SsoUserInformationEntity>(Method.post,
        showDialog: false,
        url: HttpApi.KipleBaseUrl + HttpApi.ssoInformation, onSuccess: (data) {
      if (data != null) {
        ssoUserInfo = data;
        address1Controller.text = ssoUserInfo?.address1 ?? '';
        address2Controller.text = ssoUserInfo?.address2 ?? '';
        countryController.text = ssoUserInfo?.country ?? '';
        stateController.text = ssoUserInfo?.state ?? '';
        cityController.text = ssoUserInfo?.city ?? '';
        postcodeController.text = ssoUserInfo?.postcode ?? '';
        checkDismissDialog();
      }
    }, onError: (code, msg) {
      showMessage(msg);
    });
  }

  checkDismissDialog() {
    if (requestDataSuccess && requestPositionSuccess && ssoUserInfo != null) {
      dismissProgressDialog();
      for (SelectItem item in statesData) {
        if (item.value == ssoUserInfo?.state) {
          selectItemStates = item;
          break;
        }
      }
    }
  }

  map2List(Map map, List<SelectItem> list, {bool needSort = false}) {
    list.clear();
    map.forEach((key, value) {
      list.add(SelectItem(key, value));
    });
    if (needSort) {
      list.sort((a, b) => a.value.compareTo(b.value));
    }
  }

  commitData() {
    ///close keyboard
    FocusScope.of(currentContext).requestFocus(FocusNode());
    Map map = Map();
    map["position"] = selectItemPosition;
    map["email"] = emailController.text.isEmpty ? "" : emailController.text;
    map["first_address"] = address1Controller.text;
    map["second_address"] = address2Controller.text;
    map["country"] = countryController.text;
    map["state"] = selectItemStates?.key;
    map["city"] = cityController.text.isEmpty ? "" : cityController.text;
    map["business_type"] = selectItemBusiness?.key;
    map["phone_number"] = mobileController.text;
    map["occupation"] = selectItemOccupation?.key;
    map["post_code"] = postcodeController.text;
    requestNetwork(Method.post,
        url: HttpApi.KipleBaseUrl + HttpApi.SubmitPersonalInfo,
        params: map, onSuccess: (data) {
      Timer.periodic(Duration(milliseconds: 100), (timer) {
        timer.cancel();
        Navigator.push(
          currentContext,
          MaterialPageRoute(
            builder: (context) {
              return LandingPage();
            },
          ),
        );
      });
    }, onError: (code, msg) {
      showMessage(msg);
    });
  }
}
