import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';

class ContentCountProvider extends BaseProvider {
  int maxLength = 50;

  ContentCountProvider(BuildContext buildContext, {int maxLength = 50}) : super(buildContext) {
    this.maxLength = maxLength;
  }

  TextEditingController contentController = TextEditingController();

  changeContent(String content) {
    contentController.text = content;
    notifyListeners();
  }
}
