import 'dart:convert';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/cards/pop_up_alert.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/ekyc_verify_detail.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/verify_selfie_new.dart';
import 'package:selangkah_new/Wallet/Utils/channel_tool.dart';
import 'package:selangkah_new/Wallet/Utils/country_file.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/models/scan_back_model_entity.dart';
import 'package:selangkah_new/Wallet/net/net.dart';

class VerifyDetailProvider extends BaseProvider {
  VerifyDetailProvider(BuildContext buildContext) : super(buildContext);

  VerifyModel? para;
  String? fullName;
  String? icAndPassport;
  String? nationality;
  String? dateOfBirth;
  String? gender;
  String? address;
  String? expireDate;
  TextEditingController? passportAddress;
  var images;

  initCardOrPassportData() {
    fullName = '${para?.result['mFrontName']}';
    icAndPassport = '${para?.result['mDocumentNumber']}';
    nationality = para?.type == 0 ? 'Malaysia' : '${para?.result['mNationality']}';
    dateOfBirth = '${para?.result['mDateOfBirth']}';
    gender = '${para?.result['mGender'] == 'M' ? 'Male' : 'Female'}';
    address = para?.result['mAddress'] == null ? '' : '${para?.result['mAddress']}';
    expireDate = '${para?.result['mFrontExpiry']}';
    passportAddress = TextEditingController();
  }

  getIcOrPassportImage() {
    ChannelPlatformTool.startScanIdCard(
      ChannelPlatformTool.EKYC_GET_SCAN_IMAGES,
    ).then((value) {
      if (value != null) {
        ScanBackModelEntity callBack;
        if (value is String) {
          value = json.decode(value);
        }
        callBack = JsonConvert.fromJsonAsT<ScanBackModelEntity>(MyTools.transferValue(value))!;

        if (callBack.success != null && callBack.success! && callBack.status == 'image_list') {
          images = callBack.data;
          notifyListeners();
        } else {
          showMessage('${callBack.message}');
        }
      }
    });
  }

  reTackAction() {
    FocusScope.of(currentContext).requestFocus(FocusNode());
    Navigator.pop(currentContext, {'toScan': true});
  }

  submitIdAction() {
    FocusScope.of(currentContext).requestFocus(FocusNode());
    if (this.para?.type == 1 && passportAddress?.text.trim().length == 0) {
      showMessage('ekyc_address_alert'.tr());
      return;
    }
    checkInOcr();
  }

  checkInOcr() async {
    showProgressDialog();
    FirebaseMessaging? firebaseMessaging = FirebaseMessaging.instance;
    String? token;
    try {
      token = await firebaseMessaging.getToken().timeout(Duration(seconds: 15), onTimeout: () {
        return '';
      });
    } catch(e) {

    }

    if (token == null) token = '';
    dismissProgressDialog();

    Map para = {
      'onboarding_id': this.para?.result['mOnBoardingID'],
      'ref_id': '${this.para?.result['mReferenceID']}',
      'card_type': this.para?.type == 0 ? 1 : 2,
      'client_type': Platform.isIOS ? 2 : 1,
      'id_card_no': '${this.para?.result['mDocumentNumber']}',
      'token': token
    };

    requestNetwork(Method.post, params: para, url: HttpApi.KipleBaseUrl + HttpApi.CheckOcr, onSuccess: (data) {
      Future.delayed(Duration(microseconds: 0), () {
        Navigator.pushReplacement(currentContext, new MaterialPageRoute(builder: (_) {
          return VerifySelfie(this.para!);
        }));
      });
    }, onError: (code, message) {
      log(message);
      showMessage(message);
    });
  }

  verifySanctionCountry() {
    String? code = CountryTool.getCountryCode(para?.result['mNationality']);
    if (code == null) {
    } else {}
  }

  goToStartVerifySelfie() {
    Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
      return VerifySelfie(VerifyModel(0));
    }));
  }

  String getFrontImagePath() {
    if (images != null && images.length > 0) {
      return images[0];
    }
    return '';
  }

  String getBackImagePath() {
    if (images != null && images.length > 1) {
      return images[1];
    }
    return '';
  }

  alertCountry() {
    showDialog(
        context: currentContext,
        builder: (_) {
          return PopUpAlert(
              title: 'transfer_no_search_result_title'.tr(),
              content: 'ekyc_use_card'.tr(),
              confirm: 'ekyc_change_type'.tr(),
              isCancel: false,
              confirmFunction: () {
                Navigator.pop(currentContext, {"changeIdType": true});
              });
        });
  }
}
