import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/HomePage/cubit/get_homepage_data/get_homepage_data_cubit.dart';
import 'package:selangkah_new/Screens/HomePage/screens/homepage_main.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class OopsPage extends StatefulWidget {
  final bool ekycSuccess;

  OopsPage({this.ekycSuccess = false});

  @override
  _OopsPageState createState() => _OopsPageState();
}

class _OopsPageState extends State<OopsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: Colors.white, body: body());
  }

  Widget body() {
    String title = '';
    String content = '';
    String imagePath = '';

    if (widget.ekycSuccess) {
      title = 'ekyc_success_title'.tr();
      content = 'ekyc_success_decs'.tr();
      imagePath = 'assets/images/WalletEkyc/ekyc_success.png';
    } else {
      title = 'ekyc_oops_title'.tr();
      content = 'ekyc_oops_desc'.tr();
      imagePath = 'assets/images/WalletEkyc/ekyc_success.png';
    }

    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          alignment: Alignment.center,
          margin: EdgeInsets.only(bottom: 50),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Image.asset(
                  imagePath,
                  width: 200,
                  fit: BoxFit.fitWidth,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 15),
                child:
                    Text(title, style: TextStyle(color: Color(0xFF212121), fontWeight: FontWeight.bold, fontSize: 16)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30, right: 30),
                child: Text(content,
                    style: TextStyle(color: Color(0xFF757575), fontSize: 14, height: 1.5), textAlign: TextAlign.center),
              ),
              widget.ekycSuccess
                  ? Container()
                  : Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('Customer Support : +603 8605 3357 ',
                              style: TextStyle(color: appColorPrimaryKiple, fontSize: 14))
                        ],
                      ),
                    )
            ],
          ),
        ),
        buildNextButton()
      ],
    );
  }

  Widget buildNextButton() {
    return Positioned(
      left: 30,
      right: 30,
      bottom: 50,
      child: GestureDetector(
        onTap: () {
          if (widget.ekycSuccess) {
            gotoHomePage();
          } else {
            Navigator.pop(context);
          }
        },
        child: Container(
          height: 45,
          decoration: BoxDecoration(color: appColorPrimaryKiple, borderRadius: BorderRadius.circular(10)),
          alignment: Alignment.center,
          child: Text(
            'ok_string'.tr().toString(),
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
      ),
    );
  }

  gotoHomePage() {
    BlocProvider(
      create: (context) => GetHomepageDataCubit(),
      child: HomePageScreen(),
    ).launch(
      context,
      isNewTask: true,
    );
  }
}
