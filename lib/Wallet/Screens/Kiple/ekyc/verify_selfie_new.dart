import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/ekyc_verify_detail.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/verify_sefie_video.dart';
import 'package:selangkah_new/Wallet/Utils/channel_tool.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/models/scan_back_model_entity.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class VerifySelfie extends StatefulWidget {
  final VerifyModel para;

  VerifySelfie(this.para);

  @override
  _VerifySelfieState createState() => _VerifySelfieState();
}

class _VerifySelfieState extends State<VerifySelfie> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(hasLeading: false, title: 'ekyc_Selfie_title'.tr().toString(), child: body());
  }

  Widget body() {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,
          margin: EdgeInsets.only(left: 20, right: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Text('ekyc_Selfie_verification'.tr().toString(),
                      style: TextStyle(color: Color(0xFF212121), fontSize: 16, fontWeight: FontWeight.bold)),
                ),
                Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Text('ekyc_Selfie_verification_desc'.tr().toString(),
                        style: TextStyle(color: Color(0xFF757575), fontSize: 12))),
                buildContent()
              ],
            ),
          ),
        ),
        buildNextButton()
      ],
    );
  }

  Widget buildContent() {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 30),
      width: double.infinity,
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(top: 20, bottom: 20),
            width: 158,
            height: 158,
            child: Image.asset('assets/images/WalletEkyc/selfie-illustration.png'),
          ),
          Container(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('ekyc_Selfie_content_one'.tr().toString(),
                    style: TextStyle(color: Color(0xFF343434), fontWeight: FontWeight.bold, fontSize: 12)),
                SizedBox(height: 10),
                Text('ekyc_Selfie_content_two'.tr().toString(),
                    style: TextStyle(color: Color(0xFF757575), fontSize: 12, height: 1.5)),
                Text('ekyc_Selfie_content_three'.tr().toString(),
                    style: TextStyle(color: Color(0xFF757575), fontSize: 12, height: 1.5)),
                Text('ekyc_Selfie_content_four'.tr().toString(),
                    style: TextStyle(color: Color(0xFF757575), fontSize: 12, height: 1.5)),
                Text('ekyc_Selfie_content_five'.tr().toString(),
                    style: TextStyle(color: Color(0xFF757575), fontSize: 12, height: 1.5))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildNextButton() {
    return Positioned(
      left: 30,
      right: 30,
      bottom: 50,
      child: GestureDetector(
        onTap: () {
          startFaceVideo();
        },
        child: Container(
          height: 45,
          decoration: BoxDecoration(color: appColorPrimaryKiple, borderRadius: BorderRadius.circular(10)),
          alignment: Alignment.center,
          child: Text(
            'ekyc_next'.tr().toString(),
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
      ),
    );
  }

  void startFaceVideo() {
    ChannelPlatformTool.startScanIdCard(ChannelPlatformTool.EKYC_TYPE_FACE).then((value) {
      if (value != null) {
        ScanBackModelEntity callBack;
        if (value is String) {
          value = json.decode(value);
        }
        callBack = JsonConvert.fromJsonAsT<ScanBackModelEntity>(MyTools.transferValue(value))!;

        if (callBack.status == 'face_record') {
          Navigator.push(context, new MaterialPageRoute(builder: (_) {
            return VerifySelfieVideo(widget.para);
          })).then((value) {
            if (value != null && value['toScan']) {
              startFaceVideo();
            }
          });
          toast('Match score: ${callBack.data['percentMatched']}');
          if (callBack.success != null && callBack.success!) {
          } else {}
        } else {
          toast('Face matching failed. Match score: ${callBack.data['percentMatched']}');
        }
      }
    });
  }
}
