import 'package:flutter/material.dart';

class VerifyDetailItem extends StatelessWidget {
  final String? title;
  final String? value;

  VerifyDetailItem({this.title = '', this.value = ''});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title!,
            style: TextStyle(color: Color(0xFF212121), fontWeight: FontWeight.bold, fontSize: 14),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 5, bottom: 5),
            padding: EdgeInsets.fromLTRB(8, 10, 8, 10),
            decoration: BoxDecoration(
              color: Color(0xFFf4f4f4),
              borderRadius: BorderRadius.circular(3),
            ),
            child: value == null
                ? Container()
                : Text(
                    value!,
                    style: TextStyle(color: Color(0xFF757575), fontSize: 12),
                  ),
          )
        ],
      ),
    );
  }
}
