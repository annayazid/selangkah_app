import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/provider/landing_provider.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> with StateLifeCycle<LandingPage, LandingProvider> {
  late LandingProvider landingProvider;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: "ekyc_landing_title".tr().toString(),
      child: buildBody(),
      backgroundColor: Colors.white,
    );
  }

  Widget buildBody() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Stack(
        children: [
          Container(
              width: double.infinity,
              height: double.infinity,
              margin: EdgeInsets.only(top: 15, left: 15, right: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'ekyc_verification'.tr().toString(),
                    style: TextStyle(
                      color: Color(0xFF212121),
                      fontSize: 20.00,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    'ekyc_verification_desc'.tr().toString(),
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Color(0xFF757575), fontSize: 12),
                  ),
                  SizedBox(height: 30),
                  Row(
                    children: [
                      Container(
                        width: 42,
                        height: 42,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(21),
                            color: Colors.white,
                            boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.1), blurRadius: 5)]),
                        child: Image.asset(
                          'assets/images/WalletEkyc/verification_id.png',
                          fit: BoxFit.fill,
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('ekyc_id_verification'.tr(),
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12,
                                  )),
                              Padding(
                                padding: const EdgeInsets.only(top: 5.0),
                                child: Text('ekyc_id_desc'.tr().toString(),
                                    style: TextStyle(color: Color(0xFF757575), fontSize: 12)),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 15),
                  Row(
                    children: [
                      Container(
                        width: 42,
                        height: 42,
                        padding: EdgeInsets.fromLTRB(8, 5, 8, 5),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(21),
                            color: Colors.white,
                            boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.1), blurRadius: 5)]),
                        child: Image.asset(
                          'assets/images/WalletEkyc/selfies.png',
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('ekyc_take_self'.tr().toString(),
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12,
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis),
                              Padding(
                                padding: const EdgeInsets.only(top: 5.0),
                                child: Text('ekyc_take_self_desc'.tr().toString(),
                                    style: TextStyle(color: Color(0xFF757575), fontSize: 12)),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  buildDocumentType()
                ],
              )),
          Positioned(
            left: 30,
            right: 30,
            bottom: 50,
            child: GestureDetector(
              onTap: () {
                log('Next');
                if (landingProvider.scanType != null) {
                  landingProvider.startVerifyEkyc();
                }

                // VerifyModel model = VerifyModel(1, result: '');
                // Navigator.push(context, new MaterialPageRoute(builder: (_) {
                //   return VerifySelfie(model);
                // }));
              },
              child: Container(
                height: 45,
                decoration: BoxDecoration(
                    color: appColorPrimaryKiple.withOpacity(landingProvider.scanType == null ? 0.5 : 1),
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Text(
                  'ekyc_next'.tr().toString(),
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildDocumentType() {
    String selectedImg = 'assets/images/WalletEkyc/select_item.png';
    String unSelectedImg = 'assets/images/WalletEkyc/unselect-item.png';

    return Container(
      margin: EdgeInsets.fromLTRB(15, 25, 15, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('ekyc_document'.tr().toString(), style: TextStyle(color: Color(0xFF212121), fontSize: 14)),
          Container(
            margin: EdgeInsets.only(top: 15),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                boxShadow: [BoxShadow(color: Color(0xFF42414B26), blurRadius: 5)]),
            child: Column(
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    if (landingProvider.scanType != 0) {
                      landingProvider.scanType = 0;
                      setState(() {});
                    }
                  },
                  child: Container(
                    width: double.infinity,
                    height: 60,
                    padding: EdgeInsets.only(left: 15, right: 15),
                    decoration: BoxDecoration(border: Border(bottom: BorderSide(width: 1, color: Color(0xFFDEDEDE)))),
                    child: Row(
                      children: [
                        Container(
                          width: 15,
                          height: 15,
                          margin: EdgeInsets.only(right: 10),
                          child: Image.asset(landingProvider.scanType == 0 ? selectedImg : unSelectedImg),
                        ),
                        Expanded(
                          child: Text('ekyc_document_card'.tr().toString(),
                              style: TextStyle(color: Color(0xFF212121), fontSize: 14)),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    if (landingProvider.scanType != 1) {
                      landingProvider.scanType = 1;
                      setState(() {});
                    }
                  },
                  child: Container(
                    width: double.infinity,
                    height: 60,
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: Row(
                      children: [
                        Container(
                            width: 18,
                            height: 18,
                            margin: EdgeInsets.only(right: 10),
                            child: Image.asset(landingProvider.scanType == 1 ? selectedImg : unSelectedImg)),
                        Expanded(
                          child: Text('ekyc_document_passport'.tr().toString(),
                              style: TextStyle(color: Color(0xFF212121), fontSize: 14)),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  LandingProvider createProvider(BuildContext buildContext) {
    landingProvider = LandingProvider(buildContext);
    return landingProvider;
  }
}
