import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/provider/content_count_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class PersonalDetailsItem extends StatefulWidget {
  final String? titleString;
  final String? hintString;
  final TextEditingController? controller;
  final Function? clickFunction;
  final bool? filedEnabled;
  final Color? textColor;
  final bool? hideRightImage;
  final double? spacing;
  final bool? hideLeftWidget;
  final String? leftWidgetString;
  final TextInputType? keyboardType;
  final ContentCountProvider? contentCountProvider;
  final int? inputMaxLength;
  final int? inputMaxLines;
  final bool? isLins;
  final bool? hideNotice;
  final Color? noticeTextColor;
  final String? noticeText;
  final List<TextInputFormatter>? inputFormatters;

  final bool? hideLfetText;
  final String? leftTextStr;
  final Color? leftTextColor;
  final bool? showNoticeTitle;
  final ValueChanged<String>? onSubmit;
  final TextInputAction? textInputAction;
  final bool? hideMustTag;

  PersonalDetailsItem(
      {required this.titleString,
      required this.hintString,
      required this.controller,
      required this.spacing,
      this.clickFunction,
      this.filedEnabled = true,
      this.textColor,
      this.hideRightImage = false,
      this.hideLeftWidget = true,
      this.leftWidgetString = "",
      this.keyboardType,
      this.inputMaxLength,
      this.inputMaxLines,
      this.isLins = false,
      this.hideNotice = true,
      this.noticeTextColor = appColorPrimaryKiple,
      this.noticeText = "",
      this.hideLfetText = true,
      this.leftTextStr = "",
      this.leftTextColor,
      this.showNoticeTitle = true,
      this.inputFormatters,
      this.onSubmit,
      this.textInputAction,
      this.contentCountProvider,
      this.hideMustTag = true});

  @override
  State<StatefulWidget> createState() {
    return PersonalDetailsItemSate();
  }
}

class PersonalDetailsItemSate extends State<PersonalDetailsItem> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Offstage(
          offstage: widget.showNoticeTitle != null ? !widget.showNoticeTitle! : false,
          child: Column(
            children: [
              SizedBox(
                height: 7.h as double,
              ),
              Container(
                alignment: Alignment.topLeft,
                child: Row(
                  children: [
                    Offstage(
                      offstage: widget.hideMustTag != null ? widget.hideMustTag! : true,
                      child: Text(
                        "*",
                        style: TextStyle(color: appColorPrimary, fontSize: 14),
                      ),
                    ),
                    Text(
                      widget.titleString!,
                      style: TextStyle(color: appTextColorPrimary, fontSize: 14),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        Offstage(
          offstage: widget.hideNotice != null ? widget.hideNotice! : true,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: widget.spacing,
              ),
              Text(
                widget.noticeText != null ? widget.noticeText! : "",
                style: TextStyle(color: widget.noticeTextColor, fontSize: 12),
              )
            ],
          ),
        ),
        SizedBox(
          height: widget.spacing,
        ),
        Row(
          children: [
            Offstage(
              offstage: widget.hideLeftWidget != null ? widget.hideLeftWidget! : true,
              child: Container(
                margin: EdgeInsets.only(right: 4.w as double),
                child: Text(
                  widget.leftWidgetString != null ? widget.leftWidgetString! : "",
                  style: primaryTextStyle(color: appTextColorPrimary, size: 12),
                ),
              ),
            ),
            Expanded(
              child: InkWell(
                child: widget.isLins != null && widget.isLins!
                    ? Container(
                        constraints: BoxConstraints(
                          maxHeight: 34.h as double,
                          minHeight: 14.h as double,
                        ),
                        padding: EdgeInsets.only(
                          left: 4.w as double,
                          right: 4.w as double,
                        ),
                        decoration:
                            BoxDecoration(color: appTextColorGrayBack, borderRadius: BorderRadius.circular((4.0))),
                        child: getTextFiled(),
                      )
                    : Container(
                        height: 21.h as double,
                        padding: EdgeInsets.only(left: 4.w as double, right: 4.w as double),
                        alignment: Alignment.center,
                        decoration:
                            BoxDecoration(color: appTextColorGrayBack, borderRadius: BorderRadius.circular((4.0))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Offstage(
                              offstage: widget.hideLfetText != null ? widget.hideLfetText! : true,
                              child: Container(
                                margin: EdgeInsets.only(right: 2.w as double),
                                child: Text(
                                  widget.leftTextStr != null ? widget.leftTextStr! : "",
                                  style: primaryTextStyle(
                                      color: widget.leftTextColor == null ? widget.textColor : widget.leftTextColor,
                                      size: 12),
                                ),
                              ),
                            ),
                            Expanded(child: getTextFiled()),
                            Offstage(
                              offstage: widget.hideRightImage != null ? widget.hideRightImage! : true,
                              child: Image.asset(
                                "assets/images/Wallet/icon_arrow_bottom.png",
                                width: 9.h as double,
                                height: 9.h as double,
                              ),
                            )
                          ],
                        ),
                      ),
                onTap: () {
                  if (widget.clickFunction != null) {
                    widget.clickFunction!();
                  }
                },
              ),
            ),
          ],
        ),
        Offstage(
          offstage: widget.contentCountProvider == null,
          child: widget.contentCountProvider == null
              ? Container()
              : Container(
                  margin: EdgeInsets.only(top: 2.h as double),
                  alignment: Alignment.topRight,
                  child: ChangeNotifierProvider<ContentCountProvider?>(
                    create: (BuildContext context) {
                      return widget.contentCountProvider;
                    },
                    child: Consumer<ContentCountProvider?>(
                      builder: (BuildContext context, ContentCountProvider? timeProvider, Widget? child) {
                        return Text(
                          "${widget.contentCountProvider?.contentController.text.length}/${widget.contentCountProvider?.maxLength}",
                          style: TextStyle(color: appTextColorGrayhint, fontSize: 10),
                        );
                      },
                    ),
                  ),
                ),
        ),
      ],
    );
  }

  Widget getTextFiled() {
    return TextField(
        cursorColor: appColorPrimaryKiple,
        controller: widget.controller,
        enabled: widget.filedEnabled,
        textAlign: TextAlign.left,
        maxLength: widget.inputMaxLength,
        maxLines: widget.inputMaxLines != null ? widget.inputMaxLines : 1,
        keyboardType: widget.keyboardType == null ? TextInputType.multiline : widget.keyboardType,
        inputFormatters: widget.inputFormatters,
        textInputAction: widget.textInputAction,
        onSubmitted: widget.onSubmit,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(0),
          hintText: widget.hintString,
          hintStyle: primaryTextStyle(color: appTextColorGrayhint, size: 12),
          border: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
            ),
          ),
        ),
        style: primaryTextStyle(color: widget.textColor, size: 14));
  }
}
