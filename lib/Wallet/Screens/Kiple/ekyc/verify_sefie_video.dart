import 'dart:io';
import 'dart:ui';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/ekyc_verify_detail.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/provider/verify_sefie_video_provider.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class VerifySelfieVideo extends StatefulWidget {
  final VerifyModel para;

  VerifySelfieVideo(this.para);

  @override
  _VerifySelfieVideoState createState() => _VerifySelfieVideoState();
}

class _VerifySelfieVideoState extends State<VerifySelfieVideo>
    with StateLifeCycle<VerifySelfieVideo, VerifySelfieVideoProvider> {
  late VerifySelfieVideoProvider videoProvider;

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(title: 'ekyc_Selfie_title'.tr().toString(), hasLeading: false, child: body());
  }

  Widget body() {
    return ChangeNotifierProvider(create: (BuildContext context) {
      return videoProvider;
    }, child: Consumer(
      builder: (ctx, VerifySelfieVideoProvider provider, child) {
        videoProvider = provider;
        return Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.white,
              padding: EdgeInsets.only(left: 20, right: 20),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      child: Text('ekyc_Selfie_video'.tr().toString(),
                          style: TextStyle(color: Color(0xFF212121), fontSize: 16, fontWeight: FontWeight.bold)),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text('ekyc_Selfie_video_desc'.tr().toString(),
                            style: TextStyle(color: Color(0xFF757575), fontSize: 12))),
                    buildContent()
                  ],
                ),
              ),
            ),
            buildBottomButton()
          ],
        );
      },
    ));
  }

  buildContent() {
    return Container(
      margin: EdgeInsets.only(top: 30),
      width: double.infinity,
      alignment: Alignment.center,
      child: ClipOval(
        child: Container(
          color: Color(0xFFf5f5f5),
          width: 200,
          height: 200,
          child: videoProvider.getFaceImagePath() != ''
              ? Image.file(File(videoProvider.getFaceImagePath()), fit: BoxFit.cover)
              : Container(),
        ),
      ),
    );
  }

  Widget buildBottomButton() {
    double paddingBottom = MediaQueryData.fromWindow(window).padding.bottom;
    return Container(
      padding: EdgeInsets.fromLTRB(15, 10, 15, 15 + paddingBottom),
      width: double.infinity,
      decoration: BoxDecoration(color: Colors.white),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              videoProvider.retakeVideo();
            },
            child: Container(
              width: 140,
              height: 50,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15), border: Border.all(width: 1, color: appColorPrimaryKiple)),
              child: Text('ekyc_video_retake'.tr().toString(),
                  style: TextStyle(color: appColorPrimaryKiple, fontSize: 16, fontWeight: FontWeight.bold)),
            ),
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              videoProvider.submitVideoAction();
            },
            child: Container(
              width: 140,
              height: 50,
              alignment: Alignment.center,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: appColorPrimaryKiple),
              child: Text('ekyc_video_submit'.tr().toString(),
                  style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold)),
            ),
          )
        ],
      ),
    );
  }

  @override
  VerifySelfieVideoProvider createProvider(BuildContext buildContext) {
    videoProvider = VerifySelfieVideoProvider(buildContext);
    videoProvider.para = widget.para;
    return videoProvider;
  }
}
