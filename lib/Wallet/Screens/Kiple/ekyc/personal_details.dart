import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/personal_details_item_widget.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/provider/personal_details_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_button.dart';
import 'package:selangkah_new/utils/AppColors.dart';

import 'ekyc_top_content_widget.dart';

class PersonalDetailsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PersonalDetailsState();
  }
}

class PersonalDetailsState extends State<PersonalDetailsPage>
    with StateLifeCycle<PersonalDetailsPage, PersonalDetailsProvider> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    return AppBarWhite(
      title: 'personal_details_title'.tr().toString(),
      child: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double),
          child: Column(
            children: [
              TopContentWidget('personal_details_bodytitle'.tr().toString(), [
                Text(
                  'personal_details_bodydes'.tr().toString(),
                  style: primaryTextStyle(color: appTextColorGray, size: 14),
                ),
              ]),
              SizedBox(
                height: 4.h as double,
              ),
              PersonalDetailsItem(
                titleString: 'personal_details_occupation'.tr().toString(),
                hintString: 'personal_details_occupation_hint'.tr().toString(),
                controller: provider.occupationController,
                textColor: appTextColorPrimary,
                filedEnabled: false,
                spacing: 2.h as double,
                hideMustTag: false,
                clickFunction: () {
                  provider.showPub('personal_details_occupation'.tr().toString(), provider.occupationsData,
                      provider.clickOccupationItem);
                },
              ),
              PersonalDetailsItem(
                titleString: 'personal_details_position'.tr().toString(),
                hintString: 'personal_details_occupation_hint'.tr().toString(),
                controller: provider.positionController,
                textColor: appTextColorPrimary,
                filedEnabled: false,
                spacing: 2.h as double,
                hideMustTag: false,
                clickFunction: () {
                  provider.showPub('personal_details_occupation_hint'.tr().toString(), provider.positionData,
                      provider.clickPositionItem);
                },
              ),
              PersonalDetailsItem(
                titleString: 'personal_details_business_type'.tr().toString(),
                hintString: 'personal_details_occupation_hint'.tr().toString(),
                controller: provider.businessTypeController,
                textColor: appTextColorPrimary,
                filedEnabled: false,
                spacing: 2.h as double,
                hideMustTag: false,
                clickFunction: () {
                  provider.showPub('personal_details_occupation_hint'.tr().toString(), provider.natureBusinessesData,
                      provider.clickBusinessItem);
                },
              ),
              PersonalDetailsItem(
                titleString: 'personal_details_email'.tr().toString(),
                hintString: 'personal_details_email_hint'.tr().toString(),
                controller: provider.emailController,
                textColor: appTextColorPrimary,
                filedEnabled: true,
                hideRightImage: true,
                spacing: 2.h as double,
                hideMustTag: false,
              ),
              PersonalDetailsItem(
                titleString: 'personal_details_mobile_number'.tr().toString(),
                hintString: 'personal_details_mobile_hint'.tr().toString(),
                controller: provider.mobileController,
                textColor: appTextColorPrimary,
                filedEnabled: false,
                hideRightImage: true,
                hideLeftWidget: false,
                leftWidgetString: "+6",
                spacing: 8.h as double,
                keyboardType: TextInputType.phone,
                hideMustTag: false,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              ),
              Container(
                alignment: Alignment.topLeft,
                margin: EdgeInsets.only(top: 20.h as double, bottom: 4.h as double),
                child: Text(
                  'personal_details_mailing_address'.tr().toString(),
                  style: boldTextStyle(color: appTextColorPrimary, size: 16),
                ),
              ),
              PersonalDetailsItem(
                titleString: 'personal_details_address_line1'.tr().toString(),
                hintString: 'personal_details_address_hint'.tr().toString(),
                controller: provider.address1Controller,
                textColor: appTextColorPrimary,
                filedEnabled: true,
                hideRightImage: true,
                hideLeftWidget: true,
                spacing: 2.h as double,
                inputMaxLines: 2,
                isLins: true,
                hideMustTag: false,
                contentCountProvider: provider?.address1ContentCountProvider,
              ),
              PersonalDetailsItem(
                titleString: 'personal_details_address_line2'.tr().toString(),
                hintString: 'personal_details_address_hint'.tr().toString(),
                controller: provider.address2Controller,
                textColor: appTextColorPrimary,
                filedEnabled: true,
                hideRightImage: true,
                hideLeftWidget: true,
                spacing: 2.h as double,
                inputMaxLines: 2,
                isLins: true,
                contentCountProvider: provider.address2ContentCountProvider,
              ),
              PersonalDetailsItem(
                titleString: 'personal_details_country'.tr().toString(),
                hintString: 'personal_details_occupation_hint'.tr().toString(),
                controller: provider.countryController,
                textColor: appTextColorPrimary,
                filedEnabled: false,
                spacing: 2.h as double,
                hideMustTag: false,
                clickFunction: () {
                  provider.showPub('personal_details_occupation'.tr().toString(), provider.nationalitiesData,
                      provider.clickCountryItem);
                },
              ),
              PersonalDetailsItem(
                titleString: 'personal_details_state'.tr().toString(),
                hintString: 'personal_details_state_hint'.tr().toString(),
                controller: provider.stateController,
                textColor: appTextColorPrimary,
                filedEnabled: false,
                spacing: 2.h as double,
                hideMustTag: false,
                clickFunction: () {
                  provider.showPub('personal_details_occupation_hint'.tr().toString(), provider.statesData,
                      provider.clickStatesItem);
                },
              ),
              PersonalDetailsItem(
                titleString: 'personal_details_city'.tr().toString(),
                hintString: 'personal_details_city_hint'.tr().toString(),
                controller: provider.cityController,
                textColor: appTextColorPrimary,
                filedEnabled: true,
                hideRightImage: true,
                hideLeftWidget: true,
                spacing: 2.h as double,
                isLins: true,
                hideMustTag: false,
                keyboardType: TextInputType.text,
                contentCountProvider: provider.cityContentCountProvider,
              ),
              PersonalDetailsItem(
                titleString: 'personal_details_postcode'.tr().toString(),
                hintString: 'personal_details_postcode_hint'.tr().toString(),
                controller: provider.postcodeController,
                textColor: appTextColorPrimary,
                filedEnabled: true,
                hideRightImage: true,
                hideLeftWidget: true,
                spacing: 2.h as double,
                isLins: true,
                hideMustTag: false,
                keyboardType: TextInputType.number,
                contentCountProvider: provider.postcodeContentCountProvider,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              ),
              ChangeNotifierProvider<PersonalDetailsProvider?>(
                create: (BuildContext build) => provider,
                child: Consumer<PersonalDetailsProvider>(
                  builder: (BuildContext? build, PersonalDetailsProvider? pro, Widget? child) {
                    return Container(
                      margin: EdgeInsets.only(
                          top: 14.h as double, bottom: 16.h as double, left: 7.w as double, right: 7.w as double),
                      child: ClickButton(
                        content: 'button_next_string'.tr().toString(),
                        canClick: provider.canCommit,
                        clickFunction: () {
                          provider.commitData();
                        },
                        checkFunction: () {
                          provider.notCommitShow();
                        },
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  late PersonalDetailsProvider provider;

  @override
  createProvider(BuildContext buildContext) {
    provider = PersonalDetailsProvider(buildContext);
    provider.initListener();
    return provider;
  }
}
