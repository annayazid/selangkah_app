import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class TopContentWidget extends StatefulWidget {
  final String titleContent;
  final List<Widget> bodyChild;

  TopContentWidget(this.titleContent, this.bodyChild) {}

  @override
  State<StatefulWidget> createState() {
    return TopContentState();
  }
}

class TopContentState extends State<TopContentWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 14.h as double,
        ),
        Container(
          alignment: Alignment.topLeft,
          child: Text(
            widget.titleContent,
            style: boldTextStyle(color: appTextColorPrimary, size: 20),
          ),
        ),
        SizedBox(
          height: 4.h as double,
        ),
        Row(
          children: widget.bodyChild,
        ),
      ],
    );
  }
}
