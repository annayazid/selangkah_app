import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/provider/enable_ewallet_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/click_button.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class EnableEWalletPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return EnableEWalletState();
  }
}

class EnableEWalletState extends State<EnableEWalletPage>
    with StateLifeCycle<EnableEWalletPage, EnableEWalletProvider> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    return AppBarWhite(
      title: 'enable_e_wallet_title'.tr().toString(),
      child: Container(
        margin: EdgeInsets.only(left: 14.w as double, right: 14.w as double),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 74.h as double,
            ),
            Image.asset(
              "assets/images/Wallet/icon_enable_e_wallet.png",
              width: 63.h as double,
              height: 63.h as double,
            ),
            SizedBox(
              height: 22.h as double,
            ),
            Text(
              'enable_e_wallet_body_1'.tr().toString(),
              style: boldTextStyle(color: appTextColorPrimary, size: 20),
            ),
            SizedBox(
              height: 8.h as double,
            ),
            Text(
              'enable_e_wallet_body_2'.tr().toString(),
              style: primaryTextStyle(color: appTextColorGray, size: 16),
              textAlign: TextAlign.center,
            ),
            Expanded(child: Container()),
            ClickButton(
              content: 'button_next_string'.tr().toString(),
              clickFunction: () {
                provider.jump2Next();
              },
            ),
            SizedBox(
              height: 24.h as double,
            )
          ],
        ),
      ),
    );
  }

  late EnableEWalletProvider provider;

  @override
  EnableEWalletProvider createProvider(BuildContext buildContext) {
    provider = EnableEWalletProvider(buildContext);
    return provider;
  }

  @override
  void dispose() {
    super.dispose();

    provider.dispose();
  }
}
