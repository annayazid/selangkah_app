import 'dart:io';
import 'dart:ui';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/personal_details_item_widget.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/provider/ekyc_verify_detail_provider.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/utils/AppColors.dart';

import 'components/verify_detail_item.dart';

class VerifyModel {
  VerifyModel(this.type, {this.result});

  int type;
  dynamic result;
}

class VerifyDetailNew extends StatefulWidget {
  final VerifyModel para;

  VerifyDetailNew(this.para);

  @override
  _VerifyDetailNewState createState() => _VerifyDetailNewState();
}

class _VerifyDetailNewState extends State<VerifyDetailNew> with StateLifeCycle<VerifyDetailNew, VerifyDetailProvider> {
  late VerifyDetailProvider verifyProvider;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      if (widget.para.type == 1 && widget.para.result['mNationality'] == "Malaysia") {
        verifyProvider.alertCountry();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    return AppBarWhite(
      hasLeading: false,
      title: "ekyc_landing_title".tr().toString(),
      child: buildBody(),
      backgroundColor: Colors.white,
    );
  }

  Widget buildBody() {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: ChangeNotifierProvider<VerifyDetailProvider?>(
        create: (BuildContext context) {
          return verifyProvider;
        },
        child: Consumer(
          builder: (ctx, VerifyDetailProvider provider, child) {
            return Stack(
              alignment: Alignment.bottomCenter,
              children: [buildBodyContent(), buildBottom()],
            );
          },
        ),
      ),
    );
  }

  Widget buildBodyContent() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.white,
      padding: EdgeInsets.fromLTRB(20, 00, 20, 0),
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 20),
                child: Text(
                  'ekyc_detail_title'.tr().toString(),
                  style: TextStyle(color: Color(0xFF212121), fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                child: Text(
                  'ekyc_detail_desc'.tr().toString(),
                  style: TextStyle(color: Color(0xFF757575), fontSize: 12),
                ),
              ),
              buildFrontAndBackImage(),
              buildContent()
            ],
          ),
        ),
      ),
    );
  }

  Widget buildFrontAndBackImage() {
    double imgWidth = (MediaQueryData.fromWindow(window).size.width - 55) * 0.5;

    if (widget.para.type == 0) {
      return Container(
        margin: EdgeInsets.only(top: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: imgWidth,
              height: imgWidth * 2 / 3.0,
              decoration: BoxDecoration(
                color: Color(0xFFf5f5f5),
                borderRadius: BorderRadius.circular(5),
              ),
              child: verifyProvider.getFrontImagePath() != ''
                  ? Image.file(
                      File(
                        verifyProvider.getFrontImagePath(),
                      ),
                      fit: BoxFit.cover)
                  : Container(),
            ),
            Container(
              width: imgWidth,
              height: imgWidth * 2 / 3.0,
              decoration: BoxDecoration(
                color: Color(0xFFf5f5f5),
                borderRadius: BorderRadius.circular(5),
              ),
              child: verifyProvider.getBackImagePath() != ''
                  ? Image.file(
                      File(
                        verifyProvider.getBackImagePath(),
                      ),
                      fit: BoxFit.cover)
                  : Container(),
            ),
          ],
        ),
      );
    } else {
      return Container(
        margin: EdgeInsets.only(top: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: imgWidth,
              height: imgWidth * 2 / 3.0,
              decoration: BoxDecoration(
                color: Color(0xFFf5f5f5),
                borderRadius: BorderRadius.circular(5),
              ),
              child: verifyProvider.getFrontImagePath() != ''
                  ? Image.file(
                      File(
                        verifyProvider.getFrontImagePath(),
                      ),
                      fit: BoxFit.cover)
                  : Container(),
            )
          ],
        ),
      );
    }
  }

  Widget buildContent() {
    return Container(
      margin: EdgeInsets.only(bottom: 100, top: 20),
      child: Column(
        children: [
          VerifyDetailItem(title: 'ekyc_detail_full_name'.tr().toString(), value: verifyProvider.fullName),
          VerifyDetailItem(title: 'ekyc_detail_passport'.tr().toString(), value: verifyProvider.icAndPassport),
          widget.para.type == 0
              ? Container()
              : VerifyDetailItem(title: 'ekyc_detail_expiry'.tr().toString(), value: verifyProvider.expireDate),
          VerifyDetailItem(title: 'ekyc_detail_nationality'.tr().toString(), value: verifyProvider.nationality),
          VerifyDetailItem(title: 'ekyc_detail_birth'.tr().toString(), value: verifyProvider.dateOfBirth),
          VerifyDetailItem(title: 'ekyc_detail_gender'.tr().toString(), value: verifyProvider.gender),
          widget.para.type == 0
              ? VerifyDetailItem(title: 'ekyc_detail_address'.tr().toString(), value: verifyProvider.address)
              : PersonalDetailsItem(
                  titleString: 'ekyc_detail_address'.tr().toString(),
                  hintString: 'personal_details_address_hint'.tr().toString(),
                  controller: provider.passportAddress,
                  textColor: Color(0xFF757575),
                  filedEnabled: true,
                  hideRightImage: true,
                  hideLeftWidget: true,
                  spacing: 2.h as double,
                  inputMaxLines: 1,
                  isLins: true,
                ),
        ],
      ),
    );
  }

  Widget buildBottom() {
    double paddingBottom = MediaQueryData.fromWindow(window).padding.bottom;
    return Container(
      padding: EdgeInsets.fromLTRB(15, 10, 15, 15 + paddingBottom),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          top: BorderSide(
            width: 1,
            color: Color(0xFFf5f5f5),
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              verifyProvider.reTackAction();
            },
            child: Container(
              width: 140,
              height: 50,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                border: Border.all(width: 1, color: appColorPrimaryKiple),
              ),
              child: Text(
                'ekyc_detail_retake'.tr().toString(),
                style: TextStyle(color: appColorPrimaryKiple, fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              // Get.toNamed(ROUTE_EKYC_NEW_SELFIE);
              verifyProvider.submitIdAction();
            },
            child: Container(
              width: 140,
              height: 50,
              alignment: Alignment.center,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: appColorPrimaryKiple),
              child: Text(
                'ekyc_detail_submit'.tr().toString(),
                style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  VerifyDetailProvider createProvider(BuildContext buildContext) {
    verifyProvider = VerifyDetailProvider(buildContext);
    verifyProvider.para = widget.para;
    verifyProvider.initCardOrPassportData();
    verifyProvider.getIcOrPassportImage();
    return verifyProvider;
  }
}
