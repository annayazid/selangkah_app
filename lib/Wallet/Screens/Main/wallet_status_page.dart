import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/HomePage/cubit/get_homepage_data/get_homepage_data_cubit.dart';
import 'package:selangkah_new/Screens/HomePage/screens/homepage_main.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/ekyc/personal_details.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/otp/verify_phone_number.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/pin/input_pin.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/Wallet/models/result_model.dart';
import 'package:selangkah_new/utils/AppColors.dart';
import 'package:url_launcher/url_launcher.dart';

enum EKYCSTATUS { EKYCSTATUS_NOT_START, EKYCSTATUS_IN_PROGRESS, EKYCSTATUS_SUCCESS, EKYCSTATUS_SOFT, EKYCSTATUS_HARD }

class WalletStatusPage extends StatefulWidget {
  final EKYCSTATUS status;

  WalletStatusPage({Key? key, required this.status}) : super(key: key);

  @override
  _WalletStatusPageState createState() => _WalletStatusPageState();
}

class _WalletStatusPageState extends State<WalletStatusPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    return Scaffold(backgroundColor: Colors.white, body: configBody());
  }

  Widget configBody() {
    Widget body = Container();
    switch (widget.status) {
      case EKYCSTATUS.EKYCSTATUS_NOT_START:
        body = noEkycBody();
        break;
      case EKYCSTATUS.EKYCSTATUS_IN_PROGRESS:
        body = configEkycInProgress();
        break;
      case EKYCSTATUS.EKYCSTATUS_SUCCESS:
        body = configEkycSuccess();
        break;
      case EKYCSTATUS.EKYCSTATUS_SOFT:
        body = configEkycSoftReject();
        break;
      case EKYCSTATUS.EKYCSTATUS_HARD:
        body = configHardReject();
        break;
    }
    return body;
  }

  Widget configEkycSuccess() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('assets/images/WalletEkyc/ekyc_success.png', width: 125, fit: BoxFit.fitWidth),
          Container(
            margin: EdgeInsets.only(top: 40, bottom: 15),
            child: Text(
              "ekyc_ekyc_success_title".tr().toString(),
              style: TextStyle(
                  color: Color(0xFF212121), fontSize: 20, fontWeight: FontWeight.bold, fontFamily: 'Helvetica Neue'),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 30, right: 30, top: 35),
            child: Text(
              'ekyc_ekyc_success_des'.tr().toString(),
              style: TextStyle(color: Color(0xFF757575), fontSize: 16, fontFamily: 'Helvetica Neue'),
              textAlign: TextAlign.center,
            ),
          ),
          GestureDetector(
            onTap: () async {
              ResultModel? result = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return InputPinPage.CreateWalletPin();
                  },
                ),
              );
              if (result != null && result.success!) {
                gotoHomePage();
              }
            },
            child: Container(
              margin: EdgeInsets.only(left: 30, right: 30, top: 150),
              width: double.infinity,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: appColorPrimaryKiple,
                borderRadius: BorderRadius.circular(10),
              ),
              height: 45,
              child: Text(
                'ekyc_ekyc_success_button'.tr().toString(),
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget configEkycSoftReject() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('assets/images/WalletEkyc/negative-illustration.png', width: 125, fit: BoxFit.fitWidth),
          Container(
            margin: EdgeInsets.only(top: 40, bottom: 15),
            child: Text(
              "ekyc_soft_reject_title".tr().toString(),
              style: TextStyle(
                  color: Color(0xFF212121), fontSize: 20, fontWeight: FontWeight.bold, fontFamily: 'Helvetica Neue'),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 30, right: 30),
            child: Text(
              'ekyc_soft_reject_des_one'.tr().toString(),
              style: TextStyle(color: Color(0xFF757575), fontSize: 16, fontFamily: 'Helvetica Neue'),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 30, right: 30),
            child: Text(
              'ekyc_soft_reject_des_two'.tr().toString(),
              style: TextStyle(color: Color(0xFF757575), fontSize: 12, fontFamily: 'Helvetica Neue'),
              textAlign: TextAlign.left,
            ),
          ),
          GestureDetector(
            onTap: () async {
              ResultModel? result = await Navigator.push(context, new MaterialPageRoute(builder: (_) {
                return VerifyPhoneNumberPage(OtpType.UpdateProfile);
              }));
              if (result != null && result.success!) {
                Navigator.push(context, new MaterialPageRoute(builder: (_) {
                  return PersonalDetailsPage();
                }));
              }
            },
            child: Container(
              margin: EdgeInsets.only(left: 30, right: 30, top: 30),
              width: double.infinity,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: appColorPrimaryKiple,
                borderRadius: BorderRadius.circular(10),
              ),
              height: 45,
              child: Text(
                'ekyc_soft_reject_button'.tr().toString(),
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget configHardReject() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('assets/images/WalletEkyc/negative-illustration.png', width: 125, fit: BoxFit.fitWidth),
          Container(
            margin: EdgeInsets.only(top: 40, bottom: 15),
            child: Text(
              "ekyc_hard_reject_title".tr().toString(),
              style: TextStyle(
                  color: Color(0xFF212121), fontSize: 20, fontWeight: FontWeight.bold, fontFamily: 'Helvetica Neue'),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 30, right: 30),
            child: Text(
              'ekyc_hard_reject_desc'.tr().toString(),
              style: TextStyle(color: Color(0xFF757575), fontSize: 16, fontFamily: 'Helvetica Neue'),
              textAlign: TextAlign.center,
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              margin: EdgeInsets.only(left: 30, right: 30, top: 20),
              width: double.infinity,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: appColorPrimaryKiple,
                borderRadius: BorderRadius.circular(10),
              ),
              height: 45,
              child: Text(
                'ekyc_hard_reject_button'.tr().toString(),
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget configEkycInProgress() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('assets/images/WalletEkyc/in_progress_icon.png', width: 125, fit: BoxFit.fitWidth),
          Container(
            margin: EdgeInsets.only(top: 40, bottom: 15),
            child: Text(
              "ekyc_in_progress_title".tr().toString(),
              style: TextStyle(
                  color: Color(0xFF212121), fontSize: 20, fontWeight: FontWeight.bold, fontFamily: 'Helvetica Neue'),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 30, right: 30),
            child: Text(
              'ekyc_in_progress_desc'.tr().toString(),
              style: TextStyle(color: Color(0xFF757575), fontSize: 16, fontFamily: 'Helvetica Neue'),
              textAlign: TextAlign.center,
            ),
          ),
          GestureDetector(
            onTap: () {
              // in progress
              Navigator.pop(context);
            },
            child: Container(
              margin: EdgeInsets.only(left: 30, right: 30, top: 90),
              width: double.infinity,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: appColorPrimaryKiple,
                borderRadius: BorderRadius.circular(10),
              ),
              height: 45,
              child: Text(
                'ekyc_hard_reject_button'.tr().toString(),
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget noEkycBody() {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          margin: EdgeInsets.only(bottom: 130),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset('assets/images/Wallet/wallet.png', width: 100, fit: BoxFit.fitWidth),
              SizedBox(height: 5),
              Image.asset('assets/images/Wallet/KipleInLanding.png', width: 65, fit: BoxFit.fitWidth),
              Container(
                margin: EdgeInsets.only(top: 40, bottom: 15),
                child: Text(
                  "utama_let_start".tr().toString(),
                  style: TextStyle(
                      color: Color(0xFF212121),
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Helvetica Neue'),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 40, right: 40),
                child: Text(
                  'utama_let_start_des'.tr().toString(),
                  style: TextStyle(color: Color(0xFF757575), fontSize: 16, fontFamily: 'Helvetica Neue'),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: 0,
          bottom: 40,
          right: 0,
          child: Container(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 15, right: 15),
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(children: <InlineSpan>[
                      TextSpan(
                        text: 'ekyc_click_desc'.tr().toString() + ' ',
                        style: TextStyle(color: Color(0xFF757575), fontSize: 12),
                      ),
                      TextSpan(
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            String url = 'https://selangkah.my/wp-content/uploads/2021/02/PrivacyStatement.pdf';
                            launchPrivacyUrl(url);
                          },
                        text: 'ekyc_click_privacy'.tr(),
                        style: TextStyle(color: Color(0xFF757575), fontSize: 12, decoration: TextDecoration.underline),
                      ),
                      TextSpan(
                        text: ' ',
                        style: TextStyle(color: Color(0xFF757575), fontSize: 12),
                      ),
                      TextSpan(
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            String url =
                                'https://selangkah.my/wp-content/uploads/2021/08/Selangkah-Terms-Of-Use-updated-Including-Selangkah-Wallet.pdf';
                            launchPrivacyUrl(url);
                          },
                        text: 'ekyc_click_terms'.tr(),
                        style: TextStyle(color: Color(0xFF757575), fontSize: 12, decoration: TextDecoration.underline),
                      ),
                    ]),
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    ResultModel? result = await Navigator.push(context, new MaterialPageRoute(builder: (_) {
                      return VerifyPhoneNumberPage(OtpType.UpdateProfile);
                    }));
                    if (result != null && result.success!) {
                      Navigator.push(context, new MaterialPageRoute(builder: (_) {
                        return PersonalDetailsPage();
                      }));
                    }
                  },
                  child: Container(
                    margin: EdgeInsets.only(left: 30, right: 30, top: 20),
                    width: double.infinity,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: appColorPrimaryKiple,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    height: 45,
                    child: Text(
                      'utama_activate'.tr().toString(),
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  gotoHomePage() {
    BlocProvider(
      create: (context) => GetHomepageDataCubit(),
      child: HomePageScreen(),
    ).launch(
      context,
      isNewTask: true,
    );
  }

  launchPrivacyUrl(String url) async {
    await launch(url, forceSafariVC: false);
  }
}
