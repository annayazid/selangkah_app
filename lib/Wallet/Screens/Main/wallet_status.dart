import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:selangkah_new/Screens/HomePage/homepage.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/cards/card_online.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/loyalty/loyalty_list.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/notification/notification_list.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/scan_qr_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/withdrawal/withdrawal_page.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_main_page.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_screen.dart';
import 'package:selangkah_new/Wallet/Screens/Main/wallet_status_page.dart';
import 'package:selangkah_new/Wallet/Services/navigation_service.dart';
import 'package:selangkah_new/utils/global.dart';

class WalletStatusClass {
  Widget configWalletScan(
    BuildContext context, {
    String? balance,
    Function? walletAction,
    String? selangkahId,
  }) {
    HomepageRepositoriesGlobalVar.status = '6';
    return Stack(
      children: [
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) => WalletHomeMainPage(),
              ),
            );
          },
          child: Image.asset(
            EasyLocalization.of(context)?.locale.languageCode == 'en'
                ? 'assets/images/WalletCard/status6.png'
                : 'assets/images/WalletCard/status6_bm.png',
            fit: BoxFit.fitWidth,
          ),
        ),
        Positioned(
          left: 20,
          bottom: 20,
          child: Align(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'RM ${(double.parse('$balance') / 100).toStringAsFixed(2)}',
                      style: TextStyle(
                        color: Color(0xFFF6EB0F),
                        fontSize: 30,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                    SizedBox(height: 5),
                    Text(
                      selangkahId!,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Positioned(
          right: 20,
          bottom: 25,
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              GlobalFunction.displayDisclosure(
                icon: Icon(
                  Icons.camera_alt_outlined,
                  color: Colors.blue,
                  size: 30,
                ),
                title: 'cameraQRTitle'.tr(),
                description: 'cameraQR'.tr(),
                key: 'cameraQR',
              )?.then((value) async {
                if (value) {
                  if (await Permission.camera.request().isGranted) {
                    if (value) {
                      Navigator.of(context, rootNavigator: true).push(
                        MaterialPageRoute(
                          builder: (context) => ScanQrPage(),
                        ),
                      );
                    }
                  } else {
                    return Flushbar(
                      backgroundColor: Colors.white,
                      titleText: Text(
                        'permission_continue'.tr(),
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      messageText: Text(
                        'continue_permission'.tr(),
                        style: TextStyle(color: Colors.black),
                      ),
                      mainButton: TextButton(
                        onPressed: AppSettings.openAppSettings,
                        child: Text(
                          'App Settings',
                          style: TextStyle(color: Colors.blue),
                        ),
                      ),
                      duration: Duration(seconds: 7),
                    ).show(NavigationService
                        .navigatorKey.currentState!.overlay!.context);
                  }
                }
              });
            },
            child: Image.asset(
              'assets/images/WalletCard/scan.png',
              width: 30,
              height: 30,
            ),
          ),
        ),
      ],
    );
  }

  Widget configWalletScanHomepage(
    BuildContext context, {
    String? balance,
    Function? walletAction,
    String? selangkahId,
  }) {
    double sizeIcon = MediaQuery.of(context).size.width * 0.1;

    HomepageRepositoriesGlobalVar.status = '6';
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Text(
              'E-Wallet',
              style: TextStyle(
                color: Color(0xff505660),
                fontSize: 17,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        SizedBox(height: 10),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          padding: EdgeInsets.symmetric(vertical: 15),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(color: Colors.black.withOpacity(0.15), blurRadius: 5)
              ]),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 3,
                    child: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        Navigator.of(context, rootNavigator: true).push(
                          MaterialPageRoute(
                            builder: (context) => WalletHomeMainPage(),
                          ),
                        );
                      },
                      child: Row(
                        children: [
                          SizedBox(width: 27),
                          Image.asset(
                            'assets/images/Wallet/wallet.png',
                            width: 30,
                            height: 30,
                          ),
                          SizedBox(width: 5),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'selangkah_pay'.tr(),
                                style: TextStyle(
                                    color: Color(0xFF212121),
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 5),
                              Text(
                                'RM ${(double.parse('$balance') / 100).toStringAsFixed(2)}',
                                style: TextStyle(
                                  color: Color(0xFF212121),
                                  fontSize: 16,
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Spacer(),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      Navigator.of(context, rootNavigator: true).push(
                        MaterialPageRoute(
                          builder: (context) => WalletHomeMainPage(),
                        ),
                      );
                    },
                    child: Column(
                      children: [
                        Image.asset(
                          'assets/images/Homepage/more.png',
                          width: sizeIcon,
                          height: sizeIcon,
                        ),
                        SizedBox(height: 5),
                        Text(
                          'More',
                          style: TextStyle(fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 20),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      GlobalFunction.displayDisclosure(
                        icon: Icon(
                          Icons.camera_alt_outlined,
                          color: Colors.blue,
                          size: 30,
                        ),
                        title: 'cameraQRTitle'.tr(),
                        description: 'cameraQR'.tr(),
                        key: 'cameraQR',
                      ).then((value) async {
                        if (value) {
                          if (await Permission.camera.request().isGranted) {
                            if (value) {
                              Navigator.of(context, rootNavigator: true).push(
                                MaterialPageRoute(
                                  builder: (context) => ScanQrPage(),
                                ),
                              );
                            }
                          } else {
                            return Flushbar(
                              backgroundColor: Colors.white,
                              titleText: Text(
                                'permission_continue'.tr(),
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                ),
                              ),
                              messageText: Text(
                                'continue_permission'.tr(),
                                style: TextStyle(color: Colors.black),
                              ),
                              mainButton: TextButton(
                                onPressed: AppSettings.openAppSettings,
                                child: Text(
                                  'App Settings',
                                  style: TextStyle(color: Colors.blue),
                                ),
                              ),
                              duration: Duration(seconds: 7),
                            ).show(NavigationService
                                .navigatorKey.currentState!.overlay!.context);
                          }
                        }
                      });
                    },
                    child: Column(
                      children: [
                        Image.asset(
                          'assets/images/Homepage/pay.png',
                          width: sizeIcon,
                          height: sizeIcon,
                        ),
                        SizedBox(height: 5),
                        Text(
                          'pay_homeicon'.tr(),
                          style: TextStyle(fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 20),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget configActivateWallet(BuildContext context) {
    HomepageRepositoriesGlobalVar.status = '1';
    return GestureDetector(
      onTap: () {
        log('Activate Now');
        Navigator.of(context, rootNavigator: true).push(
          MaterialPageRoute(
            builder: (context) =>
                WalletStatusPage(status: EKYCSTATUS.EKYCSTATUS_NOT_START),
          ),
        );
      },
      child: Image.asset(
        EasyLocalization.of(context)?.locale.languageCode == 'en'
            ? 'assets/images/WalletCard/status1.png'
            : 'assets/images/WalletCard/status1_bm.png',
        fit: BoxFit.fitWidth,
      ),
    );
  }

  Widget configEkycInProgress(BuildContext context) {
    HomepageRepositoriesGlobalVar.status = '2';
    return Image.asset(
      EasyLocalization.of(context)?.locale.languageCode == 'en'
          ? 'assets/images/WalletCard/status2.png'
          : 'assets/images/WalletCard/status2_bm.png',
      fit: BoxFit.fitWidth,
    );
  }

  Widget configHasEkycNoWallet(BuildContext context) {
    HomepageRepositoriesGlobalVar.status = '5';
    return GestureDetector(
      onTap: () {
        log('SET PIN');
        Navigator.of(context, rootNavigator: true).push(
          MaterialPageRoute(
            builder: (context) =>
                WalletStatusPage(status: EKYCSTATUS.EKYCSTATUS_SUCCESS),
          ),
        );
      },
      child: Image.asset(
        EasyLocalization.of(context)?.locale.languageCode == 'en'
            ? 'assets/images/WalletCard/status5.png'
            : 'assets/images/WalletCard/status5_bm.png',
        fit: BoxFit.fitWidth,
      ),
    );
  }

  Widget configEkycFailed(BuildContext context, {bool softReject = true}) {
    if (softReject) {
      HomepageRepositoriesGlobalVar.status = '3';
      return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          log('SET PIN');
          Navigator.of(context, rootNavigator: true).push(
            MaterialPageRoute(
              builder: (context) =>
                  WalletStatusPage(status: EKYCSTATUS.EKYCSTATUS_SOFT),
            ),
          );
        },
        child: Image.asset(
          EasyLocalization.of(context)?.locale.languageCode == 'en'
              ? 'assets/images/WalletCard/status3.png'
              : 'assets/images/WalletCard/status3_bm.png',
          fit: BoxFit.fitWidth,
        ),
      );
    } else {
      HomepageRepositoriesGlobalVar.status = '4';
      return Image.asset(
        EasyLocalization.of(context)?.locale.languageCode == 'en'
            ? 'assets/images/WalletCard/status4.png'
            : 'assets/images/WalletCard/status4_bm.png',
        fit: BoxFit.fitWidth,
      );
    }
  }
}

class HomepageRepositoriesGlobalVar {
  static String status = '0';
  static bool popUp = false;
}
