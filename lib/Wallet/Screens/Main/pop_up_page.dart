import 'dart:ui';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class PopUpPage extends StatefulWidget {
  final List<Widget>? children;

  PopUpPage({@required this.children});

  @override
  _PopUpPageState createState() => _PopUpPageState();
}

class _PopUpPageState extends State<PopUpPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: Colors.transparent, body: configBody());
  }

  Widget configBody() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      alignment: Alignment.bottomCenter,
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5))),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('wallet_all_services'.tr().toString(),
                    style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold)),
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.clear),
                ),
              ],
            ),
            buildChildren()
          ],
        ),
      ),
    );
  }

  Widget buildChildren() {
    double width = (MediaQueryData.fromWindow(window).size.width - 20) * 0.25;
    int line = 0;

    if (widget.children!.length % 4 == 0) {
      line = widget.children!.length ~/ 4;
    } else {
      line = (widget.children!.length ~/ 4) + 1;
    }

    List<Widget> firstChildren = widget.children!.sublist(0, 4);
    List<Widget> secondChildren = widget.children!.sublist(4, widget.children!.length);

    return Container(
      width: double.infinity,
      child: Column(
        children: [
          Row(
            children: firstChildren.map((e) {
              return Container(
                alignment: Alignment.center,
                width: width,
                child: e,
              );
            }).toList(),
          ),
          SizedBox(height: 15),
          Row(
            children: secondChildren.map((e) {
              return Container(
                alignment: Alignment.center,
                width: width,
                child: e,
              );
            }).toList(),
          )
        ],
      ),
    );
  }
}
