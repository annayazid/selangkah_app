import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/language/provider/language_provider.dart';
import 'package:selangkah_new/Wallet/models/ekyc_info_entity.dart';
import 'package:selangkah_new/Wallet/models/wallet_Info_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';
import 'package:selangkah_new/utils/secure_storage.dart';
import 'package:sp_util/sp_util.dart';

class WalletHomeProvider extends BaseProvider {
  WalletHomeProvider(BuildContext buildContext) : super(buildContext);
  EkycInfoEntity? ekycInfo;
  bool? hasWallet;
  WalletInfoEntity? walletInfo;
  LanguageProvider? languageProvider;

  switchLanguage() {
    languageProvider = LanguageProvider(currentContext);
    String? code = EasyLocalization.of(currentContext)?.locale.languageCode;
    languageProvider?.sendLanguageData(code == 'en' ? 2 : 1, showDialog: false);
  }

  getEkycInfo() {
    requestNetwork<EkycInfoEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.EkycInfo,
        onSuccess: (EkycInfoEntity data) {
      Map<String, dynamic> jsonInfo = data.toJson();
      SecureStorage().writeSecureData('EKYC_INFO', jsonEncode(jsonInfo));
      ekycInfo = data;
      if (ekycInfo?.status == '1') {
        registerToken();
      } else {
        notifyListeners();
      }
    }, onError: (code, message) {
      SecureStorage().deleteSecureData("EKYC_INFO");
      showMessage(message);
    }, showDialog: false);
  }

  registerToken() {
    requestNetwork<WalletInfoEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.refreshToken, onSuccess: (data) {
      getWalletInfo();
    }, onError: (code, message) {
      getWalletInfo();
    });
  }

  getWalletInfo({bool showDialog = true}) {
    requestNetwork<WalletInfoEntity>(Method.get, showDialog: showDialog, url: HttpApi.KipleBaseUrl + HttpApi.WalletInfo,
        onSuccess: (data) {
      hasWallet = true;
      SpUtil.putBool('HAS_WALLET', hasWallet!);
      walletInfo = data;
      notifyListeners();
    }, onError: (code, message) {
      if (code == 1700003) {
        hasWallet = false;
        SpUtil.putBool('HAS_WALLET', hasWallet!);
        notifyListeners();
      } else {
        SpUtil.remove("HAS_WALLET");
        showMessage(message);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}
