import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/cards/card_online.dart';
import 'package:selangkah_new/Wallet/models/card_manager_entity.dart';
import 'package:selangkah_new/Wallet/models/transaction_list_new_entity.dart';
import 'package:selangkah_new/Wallet/models/wallet_Info_entity.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';

class WalletHomeMainPageProvider extends BaseProvider {
  WalletHomeMainPageProvider(BuildContext buildContext) : super(buildContext);

  WalletInfoEntity? walletInfo;
  TransactionListNewEntity? history;

  refreshPage() {
    notifyListeners();
  }

  initData() {
    showProgressDialog();
    Future.wait([
      requestNetwork<WalletInfoEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.WalletInfo, showDialog: true,
          onSuccess: (data) {
        walletInfo = data;
      }, onError: (code, message) {
        showMessage(message);
      }),
      requestNetwork<TransactionListNewEntity>(Method.post,
          params: {'page_no': 1, 'page_size': 3},
          url: HttpApi.KipleBaseUrl + HttpApi.TransactionList,
          showDialog: false, onError: (code, msg) {
        showMessage(msg);
      }, onSuccess: (data) {
        history = data;
      })
    ]).then((value) {
      dismissProgressDialog();
      notifyListeners();
    });
  }

  refreshWalletInfo() {
    Future.wait([
      requestNetwork<WalletInfoEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.WalletInfo, showDialog: false,
          onSuccess: (data) {
        walletInfo = data;
      }, onError: (code, message) {
        showMessage(message);
      }),
      requestNetwork<TransactionListNewEntity>(Method.post,
          params: {'page_no': 1, 'page_size': 3},
          url: HttpApi.KipleBaseUrl + HttpApi.TransactionList,
          showDialog: false, onError: (code, msg) {
        showMessage(msg);
      }, onSuccess: (data) {
        history = data;
      })
    ]).then((value) {
      notifyListeners();
    });
  }

  getCardManagerData() {
    requestNetwork<CardManagerEntity>(Method.get, url: HttpApi.KipleBaseUrl + HttpApi.CardManager,
        onError: (code, msg) {
      showMessage(msg);
    }, onSuccess: (data) {
      Future.delayed(Duration(microseconds: 0), () {
        Navigator.push(currentContext, new MaterialPageRoute(builder: (_) {
          return CardOnline(url: data.url, addCard: true);
        }));
      });
    });
  }
}
