import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_menu.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_widget.dart';

class WalletHomeBanner extends StatefulWidget {
  @override
  _WalletHomeBannerState createState() => new _WalletHomeBannerState();
}

class _WalletHomeBannerState extends State<WalletHomeBanner> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return walletModuleInit();
  }

  Widget walletModuleInit() {
    // if (disableEwallet == '0') {
    return WalletHomeWidget(slider: true, selid: '$guestSelId');
    // }
    // return Container();
  }
}
