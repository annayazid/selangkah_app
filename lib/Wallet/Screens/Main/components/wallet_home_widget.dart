import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_menu.dart';
import 'package:selangkah_new/Wallet/Screens/Main/provider/wallet_home_provider.dart';
import 'package:selangkah_new/Wallet/Screens/Main/wallet_status.dart';
import 'package:selangkah_new/Wallet/Utils/event_bus_tool.dart';
import 'package:selangkah_new/Wallet/Utils/global_wallet.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class WalletHomeWidget extends StatefulWidget {
  final Function? walletAction;
  final bool? slider;
  final String selid;

  WalletHomeWidget(
      {Key? key, this.walletAction, this.slider, required this.selid})
      : super(key: key);

  @override
  _WalletHomeWidgetState createState() => _WalletHomeWidgetState();
}

class _WalletHomeWidgetState extends State<WalletHomeWidget>
    with StateLifeCycle<WalletHomeWidget, WalletHomeProvider> {
  WalletHomeProvider? homeEkycProvider;
  StreamSubscription? walletSubscription;

  String selangkahId = '';

  @override
  void initState() {
    super.initState();

    walletSubscription = walletEventBus.on().listen((event) {
      homeEkycProvider?.getWalletInfo(showDialog: false);
    });

    Future.delayed(Duration(seconds: 0), () {
      homeEkycProvider?.switchLanguage();
    });

    getSelangkahId();
  }

  void getSelangkahId() async {
    // await GlobalFunction.screenJourney('46');
    selangkahId = await SecureStorage().readSecureData('userSelId');
    selangkahId = selangkahId.substring(0, 2) +
        ' ' +
        selangkahId.substring(2, 6) +
        ' ' +
        selangkahId.substring(6, 11);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(create: (_) {
      return homeEkycProvider;
    }, child: Consumer(
      builder: (ctx, WalletHomeProvider provider, child) {
        return buildEkycStatus();
      },
    ));
  }

  /*
   * 0.no(not start)
   * 1.pass(success)
   * 2 manual not pass(soft reject)
   * 3 not pass (failed)
   * 4 black list (hard reject)
   * 5 pending(in-progress)
   * 6 or else (set pin)
  */

  Widget buildEkycStatus() {
    walletActive = false;
    Widget ekycWidget = Container();
    if (homeEkycProvider?.ekycInfo == null) {
      ekycWidget = Container();
    } else {
      if (homeEkycProvider?.ekycInfo?.status == '0') {
        if (widget.slider != null && widget.slider!) {
          ekycWidget = WalletStatusClass().configActivateWallet(context);
        } else {
          ekycWidget = Container();
        }
      } else if (homeEkycProvider?.ekycInfo?.status == '1') {
        if (homeEkycProvider?.hasWallet == null) {
          ekycWidget = Container();
        } else if (homeEkycProvider?.hasWallet == true) {
          walletActive = true;
          if (widget.slider != null && widget.slider!) {
            ekycWidget = WalletStatusClass().configWalletScan(
              context,
              balance: '${homeEkycProvider?.walletInfo?.useableBalance ?? '0'}',
              walletAction: walletAction(),
              selangkahId: selangkahId,
            );
          } else {
            ekycWidget = WalletStatusClass().configWalletScanHomepage(
              context,
              balance: '${homeEkycProvider?.walletInfo?.useableBalance ?? '0'}',
              walletAction: walletAction(),
              selangkahId: selangkahId,
            );
          }
        } else {
          if (widget.slider != null && widget.slider!) {
            ekycWidget = WalletStatusClass().configHasEkycNoWallet(context);
          } else {
            ekycWidget = Container();
          }
        }
      } else if (homeEkycProvider?.ekycInfo?.status == '2') {
        if (widget.slider != null && widget.slider!) {
          ekycWidget =
              WalletStatusClass().configEkycFailed(context, softReject: true);
        } else {
          ekycWidget = Container();
        }
      } else if (homeEkycProvider?.ekycInfo?.status == '3') {
        if (widget.slider != null && widget.slider!) {
          ekycWidget =
              WalletStatusClass().configEkycFailed(context, softReject: true);
        } else {
          ekycWidget = Container();
        }
      } else if (homeEkycProvider?.ekycInfo?.status == '4') {
        if (widget.slider != null && widget.slider!) {
          ekycWidget =
              WalletStatusClass().configEkycFailed(context, softReject: false);
        } else {
          ekycWidget = Container();
        }
      } else if (homeEkycProvider?.ekycInfo?.status == '5') {
        if (widget.slider != null && widget.slider!) {
          ekycWidget = WalletStatusClass().configEkycInProgress(context);
        } else {
          ekycWidget = Container();
        }
      }
    }
    return ekycWidget;
  }

  @override
  WalletHomeProvider createProvider(BuildContext buildContext) {
    homeEkycProvider = WalletHomeProvider(buildContext);
    homeEkycProvider?.getEkycInfo();
    return homeEkycProvider!;
  }

  @override
  void dispose() {
    super.dispose();
    homeEkycProvider?.dispose();
    walletSubscription?.cancel();
  }

  walletAction() {
    if (widget.walletAction != null)
      return widget.walletAction;
    else
      return;
  }
}
