import 'dart:async';

import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_state/state_lifecycle.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/bill/my_billers.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/loyalty/loyalty_list.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/mobileReload/select_operator_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/notification/notification_list.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/scan_qr_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/topup/topup_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/transaction/transaction_detail.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/transaction/transaction_history.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/transfer/receive_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/transfer/transfer_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/visa/create_visa_card.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/wallet/my_wallet_page.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/withdrawal/withdrawal_page.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_menu.dart';
import 'package:selangkah_new/Wallet/Screens/Main/pop_up_page.dart';
import 'package:selangkah_new/Wallet/Screens/Main/provider/wallet_home_main_page_provider.dart';
import 'package:selangkah_new/Wallet/Services/navigation_service.dart';
import 'package:selangkah_new/Wallet/Utils/event_bus_tool.dart';
import 'package:selangkah_new/Wallet/Utils/my_tools.dart';
import 'package:selangkah_new/Wallet/Utils/upload_notification_token.dart';
import 'package:selangkah_new/Wallet/components/appbar_white.dart';
import 'package:selangkah_new/Wallet/components/pop_up_dialog.dart';
import 'package:selangkah_new/Wallet/models/transaction_list_new_entity.dart';
import 'package:selangkah_new/utils/AppColors.dart';
import 'package:selangkah_new/utils/global.dart';

class WalletHomeMainPage extends StatefulWidget {
  final bool? card;

  const WalletHomeMainPage({Key? key, this.card}) : super(key: key);

  @override
  _WalletHomeMainPageState createState() => new _WalletHomeMainPageState();
}

class _WalletHomeMainPageState extends State<WalletHomeMainPage>
    with StateLifeCycle<WalletHomeMainPage, WalletHomeMainPageProvider>, RouteAware {
  late WalletHomeMainPageProvider soonProvider;
  StreamSubscription? walletSubscription;

  @override
  void initState() {
    super.initState();
    walletSubscription = walletEventBus.on().listen((event) {
      soonProvider.refreshWalletInfo();
    });

    try {
      // firebase config
      Future.delayed(Duration(seconds: 1), () {
        UploadNotificationProvider(context);
      });
    } catch (e) {}

    if (widget.card ?? false) {
      soonProvider.getCardManagerData();
    }
  }

  @override
  Widget build(BuildContext context) {
    return AppBarWhite(
      title: '',
      backgroundColor: Colors.white,
      child: configBody(),
    );
  }

  Widget configBody() {
    return ChangeNotifierProvider(
      create: (_) {
        return soonProvider;
      },
      child: Consumer(
        builder: (ctx, WalletHomeMainPageProvider provider, child) {
          return SingleChildScrollView(
            child: Column(
              children: [
                configWallet(),
                configBaseItem(),
                configService(),
                configPartners(),
                configComingSoon(),
                configRecentTransactions(),
                configBottomIcon()
              ],
            ),
          );
        },
      ),
    );
  }

  Widget configWallet() {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {},
      child: Container(
        margin: EdgeInsets.only(left: 10, top: 10, bottom: 15, right: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  margin: EdgeInsets.only(right: 5),
                  child: Image.asset('assets/images/Wallet/wallet.png', width: 30, height: 30),
                ),
                Text(
                  'selangkah_pay'.tr().toString(),
                  style: TextStyle(
                      color: Color(0xFF212121),
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Helvetica Neue'),
                )
              ],
            ),
            Row(
              children: [
                Text(
                  soonProvider.walletInfo?.useableBalance != null && soonProvider.walletInfo?.useableBalance != ""
                      ? 'RM ${(double.parse(soonProvider.walletInfo!.useableBalance!) / 100).toStringAsFixed(2)}'
                      : "",
                  style: TextStyle(
                      color: Color(0xFF212121),
                      fontSize: 20,
                      fontFamily: 'Helvetica Neue',
                      fontWeight: FontWeight.bold),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget configVisaCard() {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, new MaterialPageRoute(builder: (_) {
          return CreateVisaCard();
        }));
      },
      child: Container(
        height: 100,
        width: double.infinity,
        margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
        decoration: BoxDecoration(
            color: Color(0xFFffb600),
            borderRadius: BorderRadius.circular(5),
            boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.15), blurRadius: 5)]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image.asset('assets/images/Homepage/SELANGKAH App Banner Red.jpg'),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                height: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Cash withdrawals at\nany ATMs nationwide',
                      style: TextStyle(color: Colors.white, fontSize: 14),
                    ),
                    SizedBox(height: 15),
                    Text(
                      'APPLY NOW!',
                      style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget configBaseItem() {
    return Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.only(top: 15, bottom: 15),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.15), blurRadius: 5)]),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [baseItemWidget(0), baseItemWidget(1), baseItemWidget(2), baseItemWidget(3)],
          ),
          SizedBox(height: 20),
          IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [baseItemWidget(4), baseItemWidget(5), baseItemWidget(6), baseItemWidget(7)],
            ),
          ),
          SizedBox(height: 15),
          GestureDetector(
            onTap: () {
              Navigator.push(context, new MaterialPageRoute(builder: (_) {
                return MyWalletPage(walletHomeProvider: soonProvider);
              }));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  'wallet_home_more'.tr(),
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14, color: Colors.red),
                ),
                Image.asset(
                  'assets/images/Wallet/ChevronRightRed.png',
                  width: 30,
                  height: 30,
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget baseItemWidget(int index) {
    double width = (MediaQuery.of(context).size.width - 20) * 0.25;
    List<String> imagePath = [
      'assets/images/Wallet/Scan.png',
      'assets/images/Wallet/Send.png',
      'assets/images/Wallet/Receive.png',
      'assets/images/Wallet/TopUp.png',
      'assets/images/Wallet/home_withdrawal.png',
      'assets/images/Wallet/home_link_card.png',
      'assets/images/Wallet/home_loyalty.png',
      'assets/images/Wallet/home_notification.png'
    ];

    List<String> itemTitle = [
      'wallet_scan'.tr(),
      'wallet_send'.tr(),
      'wallet_receive'.tr(),
      'wallet_topup'.tr(),
      'wallet_home_withdrawal'.tr(),
      'wallet_home_linked_card'.tr(),
      'wallet_home_loyalty'.tr(),
      'wallet_home_notifications'.tr()
    ];

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        clickBaseItem(index);
      },
      child: Container(
        width: width,
        alignment: Alignment.topCenter,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(margin: EdgeInsets.only(bottom: 10), child: Image.asset(imagePath[index], width: 35, height: 35)),
            Text(
              itemTitle[index],
              textAlign: TextAlign.center,
              style: TextStyle(color: Color(0xFF212121), fontSize: 12),
            )
          ],
        ),
      ),
    );
  }

  Widget configService() {
    double width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          sectionHeader('wallet_services'.tr().toString(), showAll: false),
          Container(
            width: width * 0.5,
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                serviceItemWidget(0),
                serviceItemWidget(1),
                // serviceItemWidget(2),
                // serviceItemWidget(3)
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget configPartners() {
    double width = MediaQuery.of(context).size.width;
    List<String> paths = [
      'assets/images/WalletHomeIcon/a_speed_mart.png',
      'assets/images/WalletHomeIcon/a_selcare.png',
      'assets/images/WalletHomeIcon/a_pusat.png',
      'assets/images/WalletHomeIcon/a_my_din.png'
    ];

    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          sectionHeader('wallet_home_partners'.tr(), showAll: false),
          Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            width: width,
            padding: EdgeInsets.all(0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                partnersItem(paths[0]),
                partnersItem(paths[1]),
                partnersItem(paths[2]),
                partnersItem(paths[3]),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget configComingSoon() {
    double width = MediaQuery.of(context).size.width;
    List<String> paths = [
      'assets/images/WalletHomeIcon/a_kk_mart_new.png',
      'assets/images/WalletHomeIcon/a_burger_king.png'
    ];

    return Container(
      margin: EdgeInsets.only(top: 20, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          sectionHeader('wallet_home_coming_soon'.tr(), showAll: false),
          SizedBox(height: 10),
          Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            width: width * 0.5,
            padding: EdgeInsets.all(0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                partnersItem(paths[0]),
                partnersItem(paths[1]),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget partnersItem(String imagePath) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {},
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
                margin: EdgeInsets.only(bottom: 0),
                padding: EdgeInsets.only(right: 10, left: 10),
                child: Image.asset(imagePath, width: 60, height: 60)),
          ],
        ),
      ),
    );
  }

  Widget serviceItemWidget(int index) {
    List<String> imagePath = [
      'assets/images/Wallet/Prepaid.png',
      'assets/images/Wallet/UtilityBills.png',
    ];

    List<String> itemTitle = [
      'wallet_prepaid'.tr().toString(),
      'wallet_utility_bills'.tr().toString(),
    ];

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        log(index);
        serviceAction(itemTitle[index]);
      },
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(margin: EdgeInsets.only(bottom: 10), child: Image.asset(imagePath[index], width: 45, height: 45)),
            Text(
              itemTitle[index],
              style: TextStyle(color: Color(0xFF212121), fontSize: 12),
            )
          ],
        ),
      ),
    );
  }

  Widget configPromotions() {
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: Column(
        children: [
          sectionHeader('wallet_promotions'.tr().toString(), showAll: false),
          Container(
            height: 130,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  promotionsItem(0),
                  promotionsItem(1),
                  promotionsItem(2),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget promotionsItem(int index) {
    String url = 'assets/images/Wallet/banner_placeholder.png';
    return Container(
        margin: EdgeInsets.only(left: 10, right: 10),
        child: ClipRRect(borderRadius: BorderRadius.circular(10), child: Image.asset(url)));
  }

  Widget configRecentTransactions() {
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: Column(
        children: [
          sectionHeader('wallet_recent_transactions'.tr().toString(), showAll: true),
          (soonProvider.history == null || soonProvider.history?.xList?.length == 0)
              ? Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  padding: EdgeInsets.only(top: 40, bottom: 40),
                  child: Text('transaction_no_record'.tr(), style: TextStyle(color: Color(0xFF999999), fontSize: 15)),
                )
              : Container(
                  child: Column(
                    children: soonProvider.history!.xList!.map((e) {
                      return transactionItem(e);
                    }).toList(),
                  ),
                )
        ],
      ),
    );
  }

  Widget transactionItem(TransactionListNewList model) {
    String type = '';
    String prefix = '+';
    String userName = model.title ?? '';
    if (userName == '') userName = 'Wallet';
    if (model.type == 'PAY') {
      type = 'transaction_list_type_payment'.tr();
      prefix = '-';
    } else if (model.type == 'TFT') {
      type = 'transaction_list_type_receive'.tr();
    } else if (model.type == 'TP') {
      type = 'transaction_list_type_top_up'.tr();
    } else if (model.type == 'TFF') {
      type = 'transaction_list_type_send'.tr();
      prefix = '-';
    } else if (model.type == 'PRE') {
      type = 'transaction_type_prefund'.tr();
      // userName = 'Selangkah';
    } else if (model.type == 'WD') {
      type = 'transaction_withdrawal'.tr();
      prefix = '-';
    } else if (model.type == 'RF') {
      type = 'transaction_refund'.tr();
    } else if (model.type == 'TPAD') {
      type = 'balance_adjustment'.tr();
      prefix = '-';
    } else if (model.type == 'TPADOTS') {
      type = 'outstanding_transaction'.tr();
      prefix = '';
    }
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        Navigator.push(context, new MaterialPageRoute(builder: (_) {
          return TransactionDetail(transactionId: model.orderNo, type: model.type);
        }));
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(25, 5, 25, 25),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '$userName',
                  style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold),
                ),
                Text(
                  '${prefix}RM ${((model.type == 'WD' ? int.parse(model.actualAmount!) + int.parse(model.fee!) : int.parse(model.actualAmount!)) / 100).toStringAsFixed(2)}',
                  style: TextStyle(color: Color(0xFF212121), fontSize: 14, fontWeight: FontWeight.bold),
                )
              ],
            ),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  type,
                  style: TextStyle(color: Color(0xFF757575), fontSize: 12),
                ),
                Text(
                  '${MyTools.formatDateInTransaction(model.createTime)}',
                  style: TextStyle(color: Color(0xFF757575), fontSize: 12),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget sectionHeader(String title, {bool showAll = false}) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: TextStyle(color: Color(0xFF212121), fontSize: 20, fontWeight: FontWeight.w500),
          ),
          showAll
              ? GestureDetector(
                  onTap: () {
                    if (title == 'wallet_services'.tr().toString()) {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return PopUpPage(children: configAllService());
                          });
                    } else if (title == 'wallet_recent_transactions'.tr().toString()) {
                      Navigator.push(context, new MaterialPageRoute(builder: (_) {
                        return TransactionHistory();
                      }));
                    }
                  },
                  child: Text(
                    'wallet_see_all'.tr().toString(),
                    style: TextStyle(color: appColorPrimaryKiple, fontSize: 14),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  Widget configBottomIcon() {
    return Container(
      margin: EdgeInsets.only(top: 20, bottom: 30),
      width: double.infinity,
      alignment: Alignment.center,
      child: Image.asset(
        'assets/images/Wallet/PwbyKipleHome.png',
        width: 120,
        fit: BoxFit.fitWidth,
      ),
    );
  }

  List<Widget> configAllService() {
    List<Widget> children = [];
    List<String> imagePath = [
      'assets/images/Wallet/Prepaid.png',
      'assets/images/Wallet/Parking.png',
      'assets/images/Wallet/UtilityBills.png',
      'assets/images/Wallet/Summons.png',
      'assets/images/Wallet/StateLicense.png',
      'assets/images/Wallet/LandTax.png'
    ];

    List<String> itemTitle = [
      'wallet_prepaid'.tr().toString(),
      'wallet_parking'.tr().toString(),
      'wallet_utility_bills'.tr().toString(),
      'wallet_summons'.tr().toString(),
      'wallet_licenses'.tr().toString(),
      'wallet_land_taxes'.tr().toString()
    ];

    for (int index = 0; index < imagePath.length; index++) {
      Widget item = GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          serviceAction(itemTitle[index]);
        },
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  margin: EdgeInsets.only(bottom: 10), child: Image.asset(imagePath[index], width: 45, height: 45)),
              Text(
                itemTitle[index],
                style: TextStyle(color: Color(0xFF212121), fontSize: 12),
              )
            ],
          ),
        ),
      );
      children.add(item);
    }
    return children;
  }

  serviceAction(String title) {
    // List<String> itemTitle = [
    //   'wallet_prepaid'.tr(),
    //   'wallet_parking'.tr(),
    //   'wallet_utility_bills'.tr(),
    //   'wallet_summons'.tr(),
    //   'wallet_licenses'.tr(),
    //   'wallet_land_taxes'.tr()
    // ];

    if (title == 'wallet_prepaid'.tr()) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            return SelectOperatorPage();
          },
        ),
      );
    } else if (title == 'wallet_parking'.tr()) {
      showNotDialog();
    } else if (title == 'wallet_utility_bills'.tr()) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            return MyBillersPage();
          },
        ),
      );
    } else if (title == 'wallet_summons'.tr()) {
      showNotDialog();
    } else if (title == 'wallet_licenses'.tr()) {
      showNotDialog();
    } else if (title == 'wallet_land_taxes'.tr()) {
      showNotDialog();
    }
  }

  clickBaseItem(int index) {
    if (index == 0) {
      GlobalFunction.displayDisclosure(
        icon: Icon(
          Icons.camera_alt_outlined,
          color: Colors.blue,
          size: 30,
        ),
        title: 'cameraQRTitle'.tr(),
        description: 'cameraQR'.tr(),
        key: 'cameraQR',
      )?.then((value) async {
        if (value) {
          if (await Permission.camera.request().isGranted) {
            if (value) {
              Navigator.push(context, new MaterialPageRoute(builder: (_) {
                return ScanQrPage();
              }));
            }
          } else {
            return Flushbar(
              backgroundColor: Colors.white,
              titleText: Text(
                'permission_continue'.tr(),
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              messageText: Text(
                'continue_permission'.tr(),
                style: TextStyle(color: Colors.black),
              ),
              mainButton: TextButton(
                onPressed: AppSettings.openAppSettings,
                child: Text(
                  'App Settings',
                  style: TextStyle(color: Colors.blue),
                ),
              ),
              duration: Duration(seconds: 7),
            ).show(NavigationService.navigatorKey.currentState!.overlay!.context);
          }
        }
      });
    } else if (index == 1) {
      Navigator.push(context, new MaterialPageRoute(builder: (_) {
        return TransferPage();
      }));
    } else if (index == 2) {
      Navigator.push(context, new MaterialPageRoute(builder: (_) {
        // return VisaPrepaidCard();
        return ReceivePage();
      }));
    } else if (index == 3) {
      Navigator.push(context, new MaterialPageRoute(builder: (_) {
        return TopUpPage();
      }));
    } else if (index == 4) {
      Navigator.push(context, new MaterialPageRoute(builder: (_) {
        return WithDrawalPage();
      }));
    } else if (index == 5) {
      soonProvider.getCardManagerData();
    } else if (index == 6) {
      Navigator.push(context, new MaterialPageRoute(builder: (_) {
        return LoyaltyListPage();
      }));
    } else if (index == 7) {
      Navigator.push(context, new MaterialPageRoute(builder: (_) {
        return NotificationListPage();
      }));
    }
  }

  void showNotDialog() {
    showDialog(
        context: context,
        builder: (_) {
          return PopUpDialog(
            title: "wallet_no_services_title".tr(),
            content: "wallet_no_services_content".tr(),
            confirm: "wallet_no_services_button".tr(),
            confirmFunction: () {},
          );
        });
  }

  @override
  WalletHomeMainPageProvider createProvider(BuildContext buildContext) {
    soonProvider = WalletHomeMainPageProvider(buildContext);
    soonProvider.initData();
    return soonProvider;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, (ModalRoute.of(context) as PageRoute<dynamic>));
  }

  @override
  void didPopNext() {
    super.didPopNext();
  }

  @override
  void didPush() {
    super.didPush();
  }

  @override
  void didPop() {
    super.didPop();
  }

  @override
  void didPushNext() {
    super.didPushNext();
  }

  @override
  void dispose() {
    super.dispose();
    routeObserver.unsubscribe(this);
    walletSubscription?.cancel();
  }
}
