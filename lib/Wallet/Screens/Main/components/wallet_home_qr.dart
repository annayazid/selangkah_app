import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:selangkah_new/Screens/HomePage/screens/homepage_main.dart';
import 'package:selangkah_new/Wallet/Screens/Kiple/scan/scan_qr_page.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_menu.dart';
import 'package:selangkah_new/Wallet/Services/navigation_service.dart';
import 'package:selangkah_new/Wallet/Utils/log_utils.dart';
import 'package:selangkah_new/utils/global.dart';

class WalletQRWidget extends StatefulWidget {
  WalletQRWidget({Key? key}) : super(key: key);

  @override
  _WalletQRWidgetState createState() => new _WalletQRWidgetState();
}

class _WalletQRWidgetState extends State<WalletQRWidget> {
  @override
  void initState() {
    super.initState();

    persistentTabController?.addListener(() {
      eWalletActive();
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      eWalletActive();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  eWalletActive() {
    if (disableEwallet == '0') {
      if (persistentTabController != null &&
          persistentTabController!.index == 3) {
        checkWalletData();
      }
    }
  }

  void checkWalletData() {
    persistentTabController?.jumpToTab(2);
    if (!walletActive) {
      Fluttertoast.showToast(
        msg: 'activate_ewallet'.tr(),
        gravity: ToastGravity.CENTER,
        toastLength: Toast.LENGTH_LONG,
      );
    } else
      openQrPay();
  }

  openQrPay() {
    GlobalFunction.displayDisclosure(
      icon: Icon(
        Icons.camera_alt_outlined,
        color: Colors.blue,
        size: 30,
      ),
      title: 'cameraQRTitle'.tr(),
      description: 'cameraQR'.tr(),
      key: 'cameraQR',
    ).then((value) async {
      if (value) {
        if (await Permission.camera.request().isGranted) {
          if (value) {
            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) => ScanQrPage(),
              ),
            );
          }
        } else {
          Log.d("Camera Permission denied");
          return Flushbar(
            backgroundColor: Colors.white,
            titleText: Text(
              'permission_continue'.tr(),
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
            messageText: Text(
              'continue_permission'.tr(),
              style: TextStyle(color: Colors.black),
            ),
            mainButton: TextButton(
              onPressed: AppSettings.openAppSettings,
              child: Text(
                'App Settings',
                style: TextStyle(color: Colors.blue),
              ),
            ),
            duration: Duration(seconds: 7),
          ).show(NavigationService.navigatorKey.currentState!.overlay!.context);
        }
      }
    });
  }
}
