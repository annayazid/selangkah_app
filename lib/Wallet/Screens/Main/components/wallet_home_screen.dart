import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Screens/HomePage/screens/homepage_main.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_menu.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_main_page.dart';
import 'package:selangkah_new/utils/global.dart';

class WalletHomeWidget extends StatefulWidget {
  WalletHomeWidget({Key? key}) : super(key: key);

  @override
  _WalletHomeWidgetState createState() => new _WalletHomeWidgetState();
}

class _WalletHomeWidgetState extends State<WalletHomeWidget> {
  @override
  void initState() {
    GlobalFunction.screenJourney('46');
    super.initState();

    persistentTabController?.addListener(() {
      eWalletActive();
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      eWalletActive();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  eWalletActive() {
    if (disableEwallet == '0') {
      if (persistentTabController != null &&
          persistentTabController!.index == 1) {
        checkWalletData();
      }
    }
  }

  void checkWalletData() {
    persistentTabController?.jumpToTab(2);
    if (!walletActive) {
      Fluttertoast.showToast(
        msg: 'activate_ewallet'.tr(),
        gravity: ToastGravity.CENTER,
        toastLength: Toast.LENGTH_LONG,
      );
    } else
      openWalletHome();
  }

  openWalletHome() {
    Navigator.of(context, rootNavigator: true).push(
      MaterialPageRoute(
        builder: (context) => WalletHomeMainPage(),
      ),
    );
  }
}
