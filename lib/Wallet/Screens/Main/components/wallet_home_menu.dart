import 'package:flutter/material.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/model/user_profile.dart';
import 'package:selangkah_new/Screens/AppSplashScreen/repositories/app_splash_repositories.dart';
import 'package:selangkah_new/Screens/HomePage/repositories/homepage_repositories.dart';
import 'package:selangkah_new/Wallet/Screens/Main/components/wallet_home_widget.dart';
import 'package:selangkah_new/Wallet/Services/navigation_service.dart';
import 'package:selangkah_new/Wallet/Utils/log_utils.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

import '../../../../Screens/HomePage/model/config.dart';

class WalletHomeMenu extends StatefulWidget {
  @override
  _WalletHomeMenuState createState() => new _WalletHomeMenuState();
}

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

String? guestId = '';
String? guestSelId = '';
String? disableEwallet = '';
bool walletActive = false;

class _WalletHomeMenuState extends State<WalletHomeMenu> {
  @override
  void initState() {
    getWalletInitData();
    super.initState();
    // guestId = '';
    // guestSelId = '';
    // disableEwallet = '';
  }

  @override
  Widget build(BuildContext context) {
    return walletModuleInit();
  }

  Widget walletModuleInit() {
    if (disableEwallet == '0') {
      return WalletHomeWidget(slider: false, selid: '$guestSelId');
    }
    return Container();
  }

  void getWalletInitData() async {
    Config config = await HomepageRepositories.getConfig();
    UserProfile? userProfile = await AppSplashRepositories.getUserProfile();
    SecureStorage().writeSecureData('guestId', "${userProfile?.id}");
    SecureStorage().writeSecureData('guestSelId', "${userProfile?.selId}");
    SecureStorage().writeSecureData('guestName', "${userProfile?.name}");
    SecureStorage().writeSecureData('guestPhoneNum', "${userProfile?.phoneNo}");
    SecureStorage().writeSecureData('guestEmail', "${userProfile?.email}");
    SecureStorage().writeSecureData('guestCountry', "${userProfile?.country}");
    SecureStorage()
        .writeSecureData('disableEwallet', "${config.data?[0].disableEwallet}");

    Log.d("================================");
    Log.d("========= WALLET START =========");
    Log.d("================================");

    if (NavigationService.navigatorKey.currentContext != null)
      Log.d("CONTEXT = OK");
    Log.d("ID = ${userProfile?.id}");
    Log.d("SELANGKAH_ID = ${userProfile?.selId}");
    Log.d("NAME = ${userProfile?.name}");
    Log.d("EMAIL = ${userProfile?.email}");
    Log.d("PHONE_NUMBER = ${userProfile?.phoneNo}");
    Log.d("EKYC_STATUS = ${userProfile?.ekycStatus}");

    guestId = userProfile?.id;
    guestSelId = userProfile?.selId;
    disableEwallet = config.data?[0].disableEwallet;
    setState(() {});
  }
}
