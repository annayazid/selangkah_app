import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/Utils/log_utils.dart';

mixin StateLifeCycle<T extends StatefulWidget, BP extends BaseProvider> on State<T> {
  late BP provider;

  BP createProvider(BuildContext buildContext);

  @override
  void initState() {
    provider = createProvider(context);
    if (mounted) {
      Log.d("========= $T == initState =========");
      provider.initState();
    }
    super.initState();
  }

  @override
  void dispose() {
    if (mounted) {
      provider.dispose();
    }
    Log.d("========= $T == dispose ========= ");
    super.dispose();
  }
}
