abstract class ILifecycle {
  void initState();

  void dispose();
}
