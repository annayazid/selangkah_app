import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Wallet/Services/navigation_service.dart';
import 'package:selangkah_new/Wallet/components/progress_dialog.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';

import 'i_lifecycle.dart';

class BaseProvider extends ChangeNotifier with ILifecycle {
  late CancelToken _cancelToken;
  late BuildContext currentContext;

  BaseProvider(BuildContext? buildContext) {
    _cancelToken = CancelToken();
    if (buildContext != null) currentContext = buildContext;
  }

  @override
  void initState() {}

  @override
  void dispose() {
    if (!_cancelToken.isCancelled) {
      _cancelToken.cancel();
    }
  }

  showMessage(String message) {
    if (message == "Cancel the request" || message == "Batalkan permintaan") {
      return;
    }
    toast(message, length: Toast.LENGTH_LONG);
  }

  Future requestNetwork<T>(
    Method method, {
    required String url,
    bool showDialog = true,
    NetSuccessCallback<T>? onSuccess,
    NetErrorCallback? onError,
    dynamic params,
    Map<String, dynamic>? queryParameters,
    CancelToken? cancelToken,
    Options? options,
  }) {
    if (showDialog) showProgressDialog();

    return DioUtils.instance.requestNetwork<T>(
      method,
      url,
      params: params,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken ?? _cancelToken,
      onSuccess: (data) {
        if (showDialog) dismissProgressDialog();
        if (onSuccess != null) {
          onSuccess(data);
        }
      },
      onError: (code, msg) {
        if (showDialog) dismissProgressDialog();
        if (onError != null) {
          onError(code, msg);
        }
      },
    );
  }

  void asyncRequestNetwork<T>(
    Method method, {
    required String url,
    bool showDialog = true,
    NetSuccessCallback<T>? onSuccess,
    NetErrorCallback? onError,
    dynamic params,
    Map<String, dynamic>? queryParameters,
    CancelToken? cancelToken,
    Options? options,
  }) {
    if (showDialog) showProgressDialog();
    DioUtils.instance.asyncRequestNetwork<T>(
      method,
      url,
      params: params,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken ?? _cancelToken,
      onSuccess: (data) {
        if (showDialog) dismissProgressDialog();
        if (onSuccess != null) {
          onSuccess(data);
        }
      },
      onError: (code, msg) {
        if (showDialog) dismissProgressDialog();
        if (onError != null) {
          onError(code, msg);
        }
      },
    );
  }

  bool dialogShowing = false;

  void showProgressDialog() {
    Future.delayed(const Duration(microseconds: 0), () {
      if (currentContext != null && !dialogShowing) {
        // GlobalVariables.dialog = true;
        dialogShowing = true;

        try {
          showDialog(
            context:
                NavigationService.navigatorKey.currentState!.overlay!.context,
            barrierDismissible: false,
            builder: (BuildContext context) => const ProgressDialog(),
          );
        } catch (e) {}
      }
    });
  }

  void dismissProgressDialog() {
    Future.delayed(const Duration(microseconds: 0), () {
      if (currentContext != null && dialogShowing) {
        dialogShowing = false;
        // GlobalVariables.dialog = false;
        Navigator.pop(
            NavigationService.navigatorKey.currentState!.overlay!.context);
      }
    });
  }

  Future<bool> isNotOutsideMalaysia() async {
    return Future.value(true);
  }
}
