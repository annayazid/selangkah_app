import 'package:flutter/foundation.dart';

class Constant {
  static bool inProduction = kReleaseMode;

  static bool isDriverTest = false;
  static bool isUnitTest = false;

  static const String data = 'data';
  static const String message = 'msg';
  static const String code = 'code';
  static const String time = 'time';

  static const String keyGuide = 'keyGuide';
  static const String phone = 'phone';
  static const String accessToken = 'accessToken';
  static const String refreshToken = 'refreshToken';

  static const String theme = 'AppTheme';
  static const String locale = 'locale';
  static const MALAYSIA = 'Malaysia';
  static const int SuccessCode = 0;
  static const String encryptKey = 'aa5dfeb4db8344cf86ec3eb52b6911ad'; // 32 length
  static const String MyBillsKey = 'MY-BILLS';
}
