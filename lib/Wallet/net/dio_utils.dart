import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:selangkah_new/Wallet/Utils/log_utils.dart';

import 'base_entity.dart';
import 'common.dart';
import 'error_handle.dart';

int _connectTimeout = 30000;
int _receiveTimeout = 30000;
int _sendTimeout = 10000;
String _baseUrl = "";
List<Interceptor>? _interceptors = [];

void configDio({
  int? connectTimeout,
  int? receiveTimeout,
  int? sendTimeout,
  String baseUrl = "",
  List<Interceptor>? interceptors,
}) {
  _connectTimeout = connectTimeout ?? _connectTimeout;
  _receiveTimeout = receiveTimeout ?? _receiveTimeout;
  _sendTimeout = sendTimeout ?? _sendTimeout;
  _baseUrl = baseUrl;
  _interceptors = interceptors ?? _interceptors;
}

typedef NetSuccessCallback<T> = Function(T data);
typedef NetSuccessListCallback<T> = Function(List<T> data);
typedef NetErrorCallback = Function(int code, String msg);

class DioUtils {
  factory DioUtils() => _singleton;

  DioUtils._() {
    final BaseOptions _options = BaseOptions(
      connectTimeout: _connectTimeout,
      receiveTimeout: _receiveTimeout,
      sendTimeout: _sendTimeout,
      responseType: ResponseType.plain,
      validateStatus: (_) {
        return true;
      },
      baseUrl: _baseUrl,
    );
    _dio = Dio(_options);

    /// add interceptor
    _interceptors?.forEach((interceptor) {
      _dio?.interceptors.add(interceptor);
    });
  }

  static final DioUtils _singleton = DioUtils._();

  static DioUtils get instance => DioUtils();

  static Dio? _dio;

  Dio? get dio => _dio;

  Future<BaseEntity<T>> _request<T>(
    String method,
    String url, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
    CancelToken? cancelToken,
    Options? options,
  }) async {
    final Response<String> response = await _dio!.request<String>(
      url,
      data: data,
      queryParameters: queryParameters,
      options: _checkOptions(method, options),
      cancelToken: cancelToken,
    );
    try {
      final String data = response.data.toString();
      final bool isCompute = !Constant.isDriverTest && data.length > 10 * 1024;
      final Map<String, dynamic> _map = isCompute ? await compute(parseData, data) : parseData(data);
      try {
        return BaseEntity<T>.fromJson(_map);
      } catch (e) {
        return BaseEntity<T>(ExceptionHandle.parse_error, 'net_parsing_error'.tr(), null, null);
      }
    } catch (e) {
      debugPrint(e.toString());
      return BaseEntity<T>(ExceptionHandle.parse_error, 'net_server_error'.tr(), null, null);
    }
  }

  Options? _checkOptions(String method, Options? options) {
    options ??= Options();
    options.method = method;
    return options;
  }

  Future requestNetwork<T>(
    Method method,
    String url, {
    NetSuccessCallback<T>? onSuccess,
    NetErrorCallback? onError,
    dynamic params,
    Map<String, dynamic>? queryParameters,
    CancelToken? cancelToken,
    Options? options,
  }) {
    return _request<T>(
      method.value,
      url,
      data: params,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
    ).then<void>((BaseEntity<T> result) {
      if (result.code == Constant.SuccessCode) {
        if (onSuccess != null) {
          if (result.data != null)
            onSuccess(result.data as T);
          else
            onSuccess(result.msg as T);
        }
      } else {
        _onError(result.code, result.msg, onError);
      }
    }, onError: (dynamic e) {
      _cancelLogPrint(e, url);
      final NetError? error = ExceptionHandle.handleException(e);
      if (error != null) _onError(error.code, error.msg, onError);
    });
  }

  void asyncRequestNetwork<T>(
    Method method,
    String url, {
    NetSuccessCallback<T>? onSuccess,
    NetErrorCallback? onError,
    dynamic params,
    Map<String, dynamic>? queryParameters,
    CancelToken? cancelToken,
    Options? options,
  }) {
    Stream.fromFuture(_request<T>(
      method.value,
      url,
      data: params,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
    )).asBroadcastStream().listen((result) {
      if (result.code == Constant.SuccessCode) {
        if (onSuccess != null) {
          onSuccess(result.data as T);
        }
      } else {
        _onError(result.code, result.msg, onError);
      }
    }, onError: (dynamic e) {
      _cancelLogPrint(e, url);
      final NetError? error = ExceptionHandle.handleException(e);
      if (error != null) _onError(error.code, error.msg, onError);
    });
  }

  void _cancelLogPrint(dynamic e, String url) {
    if (e is DioError && CancelToken.isCancel(e)) {
      Log.e('Cancel request interface： $url');
    }
  }

  void _onError(int? code, String? msg, NetErrorCallback? onError) {
    if (code == null) {
      code = ExceptionHandle.unknown_error;
      msg = 'net_unkonwn'.tr();
    }
    Log.e('Interface request exception： code: $code, mag: $msg');
    if (onError != null && msg != null) {
      onError(code, msg);
    }
  }
}

Map<String, dynamic> parseData(String data) {
  return json.decode(data) as Map<String, dynamic>;
}

enum Method { get, post, put, patch, delete, head }

extension MethodExtension on Method {
  String get value => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD'][index];
}
