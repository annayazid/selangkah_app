class HttpApi {
  // static const String KipleBaseUrl = "http://47.254.245.66:10891";//sandbox
  // static const String KipleBaseUrl = "https://util-staging.kiplepay.com:8080"; // instaging
  // static const String KipleBaseUrl = "https://util-staging.kiplepay.com";// staging
  static const String KipleBaseUrl =
      "https://selangkah.kiplepay.com"; // release

  static const String SendOTP = '/sm/api/v1.0/user-register/otp-send';
  static const String VerifyOTP = '/sm/api/v1.0/user-register/otp-verify';
  static const String CreatePin = "/sm/api/v1.0/ekyc/create-pin";
  static const String CheckPin = "/sm/api/v1.0/ekyc/check-pin";
  static const String ResetPin = "/sm/api/v1.0/ekyc/reset-pin";
  static const String OccupationList =
      "/sm/api/v1.0/user-register/occupation-list";
  static const String PositionList = "/sm/api/v1.0/user-register/position-list";
  static const String SubmitPersonalInfo =
      "/sm/api/v1.0/user-register/submit-info";
  static const String CheckOcr = '/sm/api/v1.0/ekyc/ocr';
  static const String EkycComplete = '/sm/api/v1.0/ekyc/complete';
  static const String EkycInfo = '/sm/api/v1.0/ekyc/info';
  static const String WalletInfo = '/sm/api/v1.0/wallet/info';
  static const String DecodingCode = '/sm/api/v1.0/wallet/get-qr-info';

  static const String Operator_List = "/sm/api/v1.0/recharge/operator-lists";
  static const String Bank_List = "/sm/api/v1.0/wallet/bank-list";
  static const String Post_Withdrawal = "/sm/api/v1.0/wallet/withdrawal-apply";
  static const String WalletPayment = '/sm/api/v1.0/wallet/payment';
  static const String CardManager = '/sm/api/v1.0/card/card-manager';
  static const String MyQrCode = '/sm/api/v1.0/wallet/create-transfer-qr';
  static const String Commit_MobileReload =
      "/sm/api/v1.0/recharge/mobile-reload";
  static const String GetUserProfile = "/sm/api/v1.0/user/profile";
  static const String CommitTransfer = "/sm/api/v1.0/wallet/transfer";
  static const String MobileProductList = "/sm/api/v1.0/recharge/product-lists";
  static const String TransferCheckUser = "/sm/api/v1.0/wallet/check-user";
  static const String CardsList = '/sm/api/v1.0/card/card-list';
  // static const String TopUp = '/sm/api/v1.0/recharge/topup';
  static const String TopUp = '/sm/api/v1.0/recharge/token-topup';
  static const String AutoTopUpConfig =
      '/sm/api/v1.0/recharge/topup-config-get';
  static const String AutoTopUpCreate =
      '/sm/api/v1.0/recharge/topup-config-create';
  static const String AutoTopUpEdit =
      '/sm/api/v1.0/recharge/topup-config-update';
  static const String TransactionList = '/sm/api/v1.0/wallet/trans-list';
  static const String TransactionDetail = '/sm/api/v1.0/wallet/trans-detail';
  static const String CardPayment = '/sm/api/v1.0/card/card-pay';
  static const String NotificationTokenRegister =
      "/sm/api/v1.0/notify/token-register";
  static const String MessageList = "/sm/api/v1.0/notify/message-list";
  static const String MarkMessageVisiable =
      "/sm/api/v1.0/notify/message-update";
  static const String DeleteMessage = "/sm/api/v1.0/notify/message-delete";
  static const String GetIntegralInfo = "/sm/api/v1.0/integral/info";
  static const String IntegralSpendList = "/sm/api/v1.0/integral/spend-list";
  static const String ProductList = "/sm/api/v1.0/recharge/product-lists";
  static const String IntegralHistory = "/sm/api/v1.0/integral/history";
  static const String BillConfig = "/sm/api/v1.0/recharge/bill-config";
  static const String languageSwitch = "/sm/api/v1.0/user/switch-language";
  static const String refreshToken = "/sm/api/v1.0/user/refresh-login";
  static const String GetFreeConfig = "/sm/api/v1.0/wallet/withdraw-fee";
  static const String ssoInformation = "/sm/api/v1.0/ekyc/get-address";
}
