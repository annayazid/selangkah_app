import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:selangkah_new/Wallet/Utils/log_utils.dart';
import 'package:selangkah_new/utils/secure_storage.dart';
import 'package:sprintf/sprintf.dart';

import 'common.dart';
import 'dio_utils.dart';
import 'error_handle.dart';

class AuthInterceptor extends Interceptor {
  @override
  Future<void> onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    final String? selangkahId = await SecureStorage().readSecureData("userId");
    if (selangkahId != null && selangkahId.isNotEmpty) {
      options.headers['selangkah_id'] = selangkahId;
    }
    options.headers['User-Agent'] = 'Mozilla/5.0';
    super.onRequest(options, handler);
  }
}

class TokenInterceptor extends Interceptor {
  Future<String?> getToken() async {
    final Map<String, String> params = <String, String>{};
    params['refresh_token'] =
        await SecureStorage().readSecureData(Constant.refreshToken);
    try {
      _tokenDio.options = DioUtils.instance.dio!.options;
      final Response response =
          await _tokenDio.post<dynamic>('lgn/refreshToken', data: params);
      if (response.statusCode == ExceptionHandle.success) {
        return json.decode(response.data.toString())['access_token'] as String;
      }
    } catch (e) {
      Log.e('Failed to refresh Token！');
    }
    return null;
  }

  final Dio _tokenDio = Dio();

  @override
  Future<void> onResponse(
      Response response, ResponseInterceptorHandler handler) async {
    if (response != null &&
        response.statusCode == ExceptionHandle.unauthorized) {
      Log.d('-----------refresh Token------------');
      final String? accessToken = await getToken();
      if (accessToken != null) {
        SecureStorage().writeSecureData(Constant.accessToken, accessToken);
        final RequestOptions? request = response.requestOptions;
        request?.headers['Authorization'] = 'Bearer $accessToken';
      }
    }
    super.onResponse(response, handler);
  }
}

class LoggingInterceptor extends Interceptor {
  DateTime? _startTime;
  DateTime? _endTime;

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    _startTime = DateTime.now();
    Log.d('----------Start----------');
    if (options.queryParameters.isEmpty) {
      Log.d('RequestUrl: ' + options.baseUrl + options.path);
    } else {
      Log.d('RequestUrl: ' + options.baseUrl + options.path + '?');
      Log.d(
          'RequestQuery: ' + Transformer.urlEncodeMap(options.queryParameters));
    }
    Log.d('RequestMethod: ' + options.method);
    Log.d('RequestHeaders:' + options.headers.toString());
    Log.d('RequestContentType: ${options.contentType}');
    Log.d('RequestData: ${options.data.toString()}');
    super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    _endTime = DateTime.now();
    final int? duration = _endTime?.difference(_startTime!).inMilliseconds;
    if (response.statusCode == ExceptionHandle.success) {
      Log.d('ResponseCode: ${response.statusCode}');
    } else {
      Log.e('ResponseCode: ${response.statusCode}');
    }
    Log.json(response.data.toString());
    Log.d('---------- End: $duration ms ----------');
    super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    Log.d('----------Error-----------');
    super.onError(err, handler);
  }
}

class AdapterInterceptor extends Interceptor {
  static const String _kMsg = 'msg';
  static const String _kSlash = '\'';
  static const String _kMessage = 'message';

  static const String _kDefaultText = '"No return message"';
  static const String _kNotFound = 'Query information not found';

  static const String _kFailureFormat = '{"code":%d,"message":"%s"}';
  static const String _kSuccessFormat = '{"code":0,"data":%s,"message":""}';

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    final Response? r = adapterData(response);
    if (r == null)
      super.onResponse(response, handler);
    else
      super.onResponse(r, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    if (err.response != null) {
      adapterData(err.response);
    }
    super.onError(err, handler);
  }

  Response? adapterData(Response? response) {
    String result;
    String? content = response?.data?.toString() ?? '';

    if (response?.statusCode == ExceptionHandle.success ||
        response?.statusCode == ExceptionHandle.success_not_content) {
      if (content.isEmpty) {
        content = _kDefaultText;
      }
      result = sprintf(_kSuccessFormat, [content]);
      response?.statusCode = ExceptionHandle.success;
    } else {
      if (response?.statusCode == ExceptionHandle.not_found) {
        result = sprintf(_kFailureFormat, [response?.statusCode, _kNotFound]);
        response?.statusCode = ExceptionHandle.success;
      } else {
        if (content.isEmpty) {
          result = content;
        } else {
          String msg;
          try {
            content = content.replaceAll(r'\', '');
            if (_kSlash == content.substring(0, 1)) {
              content = content.substring(1, content.length - 1);
            }
            final Map<String, dynamic> map =
                json.decode(content) as Map<String, dynamic>;
            if (map.containsKey(_kMessage)) {
              msg = map[_kMessage] as String;
            } else if (map.containsKey(_kMsg)) {
              msg = map[_kMsg] as String;
            } else {
              msg = 'Unknown abnormal';
            }
            result = sprintf(_kFailureFormat, [response?.statusCode, msg]);
            if (response?.statusCode == ExceptionHandle.unauthorized) {
              response?.statusCode = ExceptionHandle.unauthorized;
            } else {
              response?.statusCode = ExceptionHandle.success;
            }
          } catch (e) {
            result = sprintf(_kFailureFormat, [
              response?.statusCode,
              'Server exception(${response?.statusCode})'
            ]);
          }
        }
      }
    }
    response?.data = result;
    return response;
  }
}
