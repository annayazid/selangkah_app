import 'package:selangkah_new/Wallet/net/common.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class BaseEntity<T> {
  BaseEntity(this.code, this.msg, this.data, this.time);

  BaseEntity.fromJson(Map<String, dynamic> json) {
    code = json[Constant.code] as int;
    time = json[Constant.time] as int;
    msg = json[Constant.message] as String;
    if (json.containsKey(Constant.data) && json[Constant.data] != null) {
      if (code == Constant.SuccessCode) {
        data = _generateOBJ<T>(json[Constant.data]);
      }
    }
  }

  int? code;
  String? msg;
  T? data;
  int? time;

  T? _generateOBJ<T>(Object json) {
    if (T.toString() == 'String') {
      return json.toString() as T;
    } else if (T.toString() == 'Map<dynamic, dynamic>') {
      return json as T;
    } else {
      return JsonConvert.fromJsonAsT<T>(json);
    }
  }
}
