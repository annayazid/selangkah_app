import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class MyQrCodeEntity with JsonConvert<MyQrCodeEntity> {
  String? name;
  @JSONField(name: "qr_code")
  String? qrCode;
}
