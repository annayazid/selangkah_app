import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class NotificationDataModleEntity with JsonConvert<NotificationDataModleEntity> {
  String? reason;
  int? status;
  @JSONField(name: "trade_type")
  String? tradeType;

  bool isSuccess() {
    return status == 1;
  }
}
