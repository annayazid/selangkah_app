import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class SsoUserInformationEntity with JsonConvert<SsoUserInformationEntity> {
  @JSONField(name: "user_no")
  String? userNo;
  @JSONField(name: "acc_name")
  String? accName;
  String? phonenumber;
  String? address1;
  String? address2;
  String? country;
  String? state;
  String? city;
  String? postcode;
}
