import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class ScanBackModelEntity with JsonConvert<ScanBackModelEntity> {
  String? message;
  String? status;
  bool? success;
  dynamic data;
}
