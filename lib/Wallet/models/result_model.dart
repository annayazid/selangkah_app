class ResultModel<T> {
  ResultModel({this.success, this.message, this.data, this.orderType});

  bool? success;
  String? message;
  T? data;
  String? orderType;
}
