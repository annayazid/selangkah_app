import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class ProductModelEntity with JsonConvert<ProductModelEntity> {
  int? amount;
  @JSONField(name: "commission_type")
  int? commissionType;
  @JSONField(name: "commission_value")
  int? commissionValue;
  @JSONField(name: "create_time")
  String? createTime;
  int? id;
  @JSONField(name: "product_code")
  String? productCode;
  @JSONField(name: "product_sub_code")
  String? productSubCode;
  @JSONField(name: "product_type")
  int? productType;
  @JSONField(name: "provider_id")
  int? providerId;
  @JSONField(name: "provider_name")
  String? providerName;
  @JSONField(name: "third_party")
  int? thirdParty;
  @JSONField(name: "update_time")
  String? updateTime;
}
