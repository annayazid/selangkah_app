import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class FreeConfigEntity with JsonConvert<FreeConfigEntity> {
  int? id;
  @JSONField(name: "refund_type")
  int? refundType;
  @JSONField(name: "refund_value")
  String? refundValue;
  @JSONField(name: "serve_charges")
  String? serveCharges;
  int? status;
  @JSONField(name: "withdraw_type")
  String? withdrawType;
  @JSONField(name: "withdraw_value")
  String? withdrawValue;
}
