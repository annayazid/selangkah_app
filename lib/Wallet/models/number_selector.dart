import 'package:flutter_native_contact_picker/flutter_native_contact_picker.dart';
import 'package:selangkah_new/Wallet/models/operator_model_entity.dart';
import 'package:selangkah_new/Wallet/models/paymethod_model.dart';
import 'package:selangkah_new/Wallet/models/product_model_entity.dart';

class NumberSelector {
  NumberSelector({this.operatorModelEntity, this.contact, this.productSelector});

  OperatorModelEntity? operatorModelEntity;
  Contact? contact;
  ProductModelEntity? productSelector;
  List<PayWayModel>? payWays;
}
