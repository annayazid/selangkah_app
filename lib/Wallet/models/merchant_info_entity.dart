import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class MerchantInfoEntity with JsonConvert<MerchantInfoEntity> {
  String? amount;
  @JSONField(name: "sub_merchant_id")
  int? subMerchantId;
  String? type;
  String? name;
  String? mobile;
  @JSONField(name: "type_code")
  int? typeCode;
  String? token;
}
