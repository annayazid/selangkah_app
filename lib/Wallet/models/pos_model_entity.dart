import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class PosModelEntity with JsonConvert<PosModelEntity> {
  String? amount;
  String? time;
  @JSONField(name: "point_earned")
  int? pointEarned;
  @JSONField(name: "merchant_name")
  String? merchantName;
  @JSONField(name: "transaction_no")
  String? transactionNo;
  @JSONField(name: "trade_type")
  String? tradeType;
}
