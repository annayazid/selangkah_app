import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class UserProfileEntity with JsonConvert<UserProfileEntity> {
  String? address;
  String? birthdate;
  String? city;
  String? email;
  @JSONField(name: "full_name")
  String? fullName;
  int? gender;
  @JSONField(name: "id_number")
  String? idNumber;
  @JSONField(name: "id_type")
  int? idType;
  String? mobile;
  String? nationality;
  @JSONField(name: "nature_business")
  String? natureBusiness;
  String? occupation;
  @JSONField(name: "source_merchant_id")
  int? sourceMerchantId;
  String? state;
  int? status;
  String? username;
}
