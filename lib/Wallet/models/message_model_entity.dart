import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

import 'notification_data_modle_entity.dart';

class MessageModelEntity with JsonConvert<MessageModelEntity> {
  @JSONField(name: "list")
  List<MessageModelList>? xList;
  int? total;
}

class MessageModelList with JsonConvert<MessageModelList> {
  @JSONField(name: "order_no")
  String? orderNo;
  int? amount;
  @JSONField(name: "message_type")
  String? messageType;
  @JSONField(name: "message_content")
  String? messageContent;
  @JSONField(name: "receiver_token")
  String? receiverToken;
  @JSONField(name: "create_time")
  String? createTime;
  int? is_read;
  String? trade_type;
  @JSONField(name: "notify_data")
  NotificationDataModleEntity? notifyData;
  String? message_type_malay;
  String? message_content_malay;
}
