import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class BillConfigEntity with JsonConvert<BillConfigEntity> {
  int? id;
  @JSONField(name: "merchant_id")
  int? merchantId;
  @JSONField(name: "product_type")
  int? productType;
  @JSONField(name: "product_switch")
  int? productSwitch;
  @JSONField(name: "commission_type")
  int? commissionType;
  @JSONField(name: "partner_percent")
  int? partnerPercent;
  @JSONField(name: "kiple_percent")
  int? kiplePercent;
  @JSONField(name: "partner_amount")
  int? partnerAmount;
  @JSONField(name: "kiple_amount")
  int? kipleAmount;
  int? frequency;
  int? provider;
  @JSONField(name: "create_time")
  String? createTime;
  @JSONField(name: "update_time")
  String? updateTime;
}
