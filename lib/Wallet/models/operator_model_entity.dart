import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class OperatorModelEntity with JsonConvert<OperatorModelEntity> {
  @JSONField(name: "create_time")
  String? createTime;
  int? id;
  @JSONField(name: "provider_name")
  String? providerName;
  @JSONField(name: "provider_type")
  int? providerType;
  @JSONField(name: "update_time")
  String? updateTime;
  String? account;
  String? amount;
}
