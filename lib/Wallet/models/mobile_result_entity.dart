import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class MobileResultEntity with JsonConvert<MobileResultEntity> {
  int? amount;
  @JSONField(name: "operator_name")
  String? operatorName;
  @JSONField(name: "order_number")
  String? orderNumber;
  @JSONField(name: "trade_time")
  String? tradeTime;
  int? use_integral;
  int? gain_integral;
}
