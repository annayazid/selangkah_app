import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class IntegralItemModelEntity with JsonConvert<IntegralItemModelEntity> {
  int? amount;
  @JSONField(name: "create_time")
  String? createTime;
  int? flag;
  int? id;
  int? integral;
  @JSONField(name: "merchant_id")
  int? merchantId;
  @JSONField(name: "update_time")
  String? updateTime;
}
