import 'dart:convert';

Config configFromJson(String str) => Config.fromJson(json.decode(str));

String configToJson(Config data) => json.encode(data.toJson());

class Config {
  Config({
    this.code,
    this.data,
  });

  int? code;
  List<Datum>? data;

  factory Config.fromJson(Map<String, dynamic> json) => Config(
        code: json["Code"] == null ? null : json["Code"],
        data: json["Data"] == null ? null : List<Datum>.from(json["Data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Code": code == null ? null : code,
        "Data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.welcomeBanner,
    this.announcementBanner,
    this.discoverBanner,
    this.disableOtp,
    this.telegramLink,
    this.disableVax,
    this.redirectVax,
    this.announcementTotal,
    this.disableAppUpdateAlert,
    this.disableEwallet,
    this.disableSplash,
    this.disableSelidik,
    this.disableSaring,
    this.disableWavpay,
    this.disableZakat,
  });

  String? welcomeBanner;
  String? announcementBanner;
  String? discoverBanner;
  String? disableOtp;
  String? telegramLink;
  String? disableVax;
  String? redirectVax;
  String? announcementTotal;
  String? disableAppUpdateAlert;
  String? disableEwallet;
  String? disableSplash;
  String? disableSelidik;
  String? disableSaring;
  String? disableWavpay;
  String? disableZakat;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        welcomeBanner: json["welcome_banner"] == null ? null : json["welcome_banner"],
        announcementBanner: json["announcement_banner"] == null ? null : json["announcement_banner"],
        discoverBanner: json["discover_banner"] == null ? null : json["discover_banner"],
        disableOtp: json["disable_otp"] == null ? null : json["disable_otp"],
        telegramLink: json["telegram_link"] == null ? null : json["telegram_link"],
        disableVax: json["disable_vax"] == null ? null : json["disable_vax"],
        redirectVax: json["redirect_vax"] == null ? null : json["redirect_vax"],
        announcementTotal: json["announcement_total"] == null ? null : json["announcement_total"],
        disableAppUpdateAlert: json["disable_app_update_alert"] == null ? null : json["disable_app_update_alert"],
        disableEwallet: json["disable_ewallet"] == null ? null : json["disable_ewallet"],
        disableSplash: json["disable_splash"] == null ? null : json["disable_splash"],
        disableSelidik: json["disable_selidik"] == null ? null : json["disable_selidik"],
        disableSaring: json["disable_saring"] == null ? null : json["disable_saring"],
        disableWavpay: json["disable_wavpay"] == null ? null : json["disable_wavpay"],
        disableZakat: json["disable_zakat"] == null ? null : json["disable_zakat"],
      );

  Map<String, dynamic> toJson() => {
        "welcome_banner": welcomeBanner == null ? null : welcomeBanner,
        "announcement_banner": announcementBanner == null ? null : announcementBanner,
        "discover_banner": discoverBanner == null ? null : discoverBanner,
        "disable_otp": disableOtp == null ? null : disableOtp,
        "telegram_link": telegramLink == null ? null : telegramLink,
        "disable_vax": disableVax == null ? null : disableVax,
        "redirect_vax": redirectVax == null ? null : redirectVax,
        "announcement_total": announcementTotal == null ? null : announcementTotal,
        "disable_app_update_alert": disableAppUpdateAlert == null ? null : disableAppUpdateAlert,
        "disable_ewallet": disableEwallet == null ? null : disableEwallet,
        "disable_splash": disableSplash == null ? null : disableSplash,
        "disable_selidik": disableSelidik == null ? null : disableSelidik,
        "disable_saring": disableSaring == null ? null : disableSaring,
        "disable_wavpay": disableWavpay == null ? null : disableWavpay,
        "disable_zakat": disableZakat == null ? null : disableZakat,
      };
}
