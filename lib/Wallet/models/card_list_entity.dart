import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class CardListEntity with JsonConvert<CardListEntity> {
  @JSONField(name: "auto_topup_enabled")
  int? autoTopupEnabled;
  List<CardListCard>? cards;
  int? threshold;
  @JSONField(name: "topup_amount")
  int? topupAmount;
  @JSONField(name: "wallet_id")
  int? walletId;
}

class CardListCard with JsonConvert<CardListCard> {
  @JSONField(name: "expiry_month")
  String? expiryMonth;
  @JSONField(name: "expiry_year")
  String? expiryYear;
  @JSONField(name: "holder_name")
  String? holderName;
  @JSONField(name: "last_topup")
  String? lastTopup;
  @JSONField(name: "masked_pan")
  String? maskedPan;
  String? primary;
  String? reference;
  @JSONField(name: "saved_date")
  String? savedDate;
  String? scheme;
  @JSONField(name: "topup_enabled")
  String? topupEnabled;
}
