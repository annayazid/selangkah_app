import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class TransactionHistoryEntity with JsonConvert<TransactionHistoryEntity> {
  @JSONField(name: "list")
  List<TransactionHistoryList>? xList;
  int? total;
}

class TransactionHistoryList with JsonConvert<TransactionHistoryList> {
  @JSONField(name: "after_balance")
  String? afterBalance;
  @JSONField(name: "before_balance")
  String? beforeBalance;
  @JSONField(name: "create_time")
  String? createTime;
  String? description;
  int? id;
  String? money;
  dynamic omitempty;
  @JSONField(name: "order_sn")
  String? orderSn;
  @JSONField(name: "origin_transaction_no")
  String? originTransactionNo;
  @JSONField(name: "transaction_no")
  String? transactionNo;
  int? type;
  @JSONField(name: "type_desc")
  String? typeDesc;
  @JSONField(name: "user_id")
  int? userId;
  @JSONField(name: "void_time")
  int? voidTime;
  @JSONField(name: "void_type")
  int? voidType;
  @JSONField(name: "wallet_id")
  int? walletId;
}
