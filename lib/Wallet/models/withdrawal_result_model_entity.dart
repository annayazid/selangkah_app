import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class WithdrawalResultModelEntity with JsonConvert<WithdrawalResultModelEntity> {
  int? amount;
  String? balance;
  @JSONField(name: "bank_name")
  String? bankName;
  String? fee;
  String? order_number;
  String? trade_time;
}
