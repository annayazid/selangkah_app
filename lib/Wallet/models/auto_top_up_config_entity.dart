import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class AutoTopUpConfigEntity with JsonConvert<AutoTopUpConfigEntity> {
  @JSONField(name: "balance_limit")
  int? balanceLimit;
  @JSONField(name: "topup_min_limit")
  int? topupMinLimit;
  @JSONField(name: "card_number")
  String? cardNumber;
  @JSONField(name: "trigger_button")
  int? triggerButton;
  String? type;
}
