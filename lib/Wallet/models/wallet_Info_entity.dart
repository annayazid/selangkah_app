import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class WalletInfoEntity with JsonConvert<WalletInfoEntity> {
  @JSONField(name: "max_balance_limit")
  String? maxBalanceLimit;
  @JSONField(name: "max_withdraw_limit")
  String? maxWithdrawLimit;
  @JSONField(name: "total_balance")
  String? totalBalance;
  @JSONField(name: "useable_balance")
  String? useableBalance;
  @JSONField(name: "wallet_sn")
  int? walletSn;
  @JSONField(name: "create_time")
  String? createTime;
}
