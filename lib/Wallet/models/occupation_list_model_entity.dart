import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class OccupationListModelEntity with JsonConvert<OccupationListModelEntity> {
  Map<dynamic, dynamic>? nationalities;
  @JSONField(name: "nature_businesses")
  Map<dynamic, dynamic>? natureBusinesses;
  Map<dynamic, dynamic>? occupations;
  Map<dynamic, dynamic>? states;
}
