import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class ScanPaymentResultEntity with JsonConvert<ScanPaymentResultEntity> {
  String? amount;
  @JSONField(name: "merchant_id")
  int? merchantId;
  @JSONField(name: "merchant_order_no")
  String? merchantOrderNo;
  @JSONField(name: "order_no")
  String? orderNo;
  @JSONField(name: "bank_pay_time")
  String? bankPayTime;
  @JSONField(name: "operator_name")
  String? operatorName;
  @JSONField(name: "trade_time")
  String? tradeTime;
  @JSONField(name: "order_number")
  String? orderNumber;
  @JSONField(name: "gain_integral")
  String? gainIntegral;
  @JSONField(name: "use_integral")
  String? useIntegral;
  @JSONField(name: "bank_time")
  String? bankTime;
}
