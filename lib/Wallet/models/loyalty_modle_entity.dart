import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class LoyaltyModleEntity with JsonConvert<LoyaltyModleEntity> {
  int? amount;
  @JSONField(name: "config_by_type")
  String? configByType;
  @JSONField(name: "create_time")
  String? createTime;
  String? description;
  int? id;
  int? integral;
  @JSONField(name: "integral_type")
  int? integralType;
  @JSONField(name: "merchant_id")
  int? merchantId;
  @JSONField(name: "order_no")
  String? orderNo;
  @JSONField(name: "phone_number")
  String? phoneNumber;
  @JSONField(name: "shopper_name")
  String? shopperName;
  @JSONField(name: "update_time")
  String? updateTime;
  @JSONField(name: "user_id")
  int? userId;
  @JSONField(name: "user_name")
  String? userName;
}
