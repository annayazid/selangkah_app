import 'package:selangkah_new/Wallet/generated/json/base/json_field.dart';
import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';

class IntegralModelEntity with JsonConvert<IntegralModelEntity> {
  @JSONField(name: "create_time")
  String? createTime;
  @JSONField(name: "expired_integral")
  int? expiredIntegral;
  @JSONField(name: "gain_integral")
  int? gainIntegral;
  int? id;
  @JSONField(name: "merchant_id")
  int? merchantId;
  @JSONField(name: "merchant_name")
  String? merchantName;
  @JSONField(name: "phone_number")
  String? phoneNumber;
  @JSONField(name: "spend_integral")
  int? spendIntegral;
  @JSONField(name: "update_time")
  String? updateTime;
  @JSONField(name: "user_id")
  int? userId;
  @JSONField(name: "user_name")
  String? userName;
  @JSONField(name: "valid_integral")
  int? validIntegral;
}
