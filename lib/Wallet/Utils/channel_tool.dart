import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class ChannelPlatformTool {
  static const String EKYC_SCAN_ID_CARD = 'kiplePay.flutter.ekyc';
  static const String INIT_EKYC = 'initEkyc';
  static const String EKYC_TYPE_ID = 'ekycScanId';
  static const String EKYC_TYPE_PASSPORT = 'ekycScanPassport';
  static const String EKYC_TYPE_FACE = 'ekycFaceRecord';
  static const String EKYC_TRY_AGAIN = 'ekycTryAgain';
  static const String EKYC_GET_SCAN_IMAGES = 'ekycGetScanImages';
  static const String EKYC_GET_FACE_IMAGES = 'ekycGetFaceImages';
  static const String EKYC_ALERT_DIALOG = 'ekycAlert';
  static const String EKYC_ALERT_DIALOG_OVERTIMR = 'ekycOvertime';
  static const String EKYC_ALERT_DIALOG_TIMEMAX = 'ekycTimeMax';
  static const String EKYC_TO_NEED_HELP = 'goToNeedHelp';

  static Future startScanIdCard(String methodChannel, {String type = "", BuildContext? context}) async {
    final platform = MethodChannel(EKYC_SCAN_ID_CARD);
    try {
      var result;
      if (methodChannel == INIT_EKYC) {
        String? code;
        if (context != null) code = EasyLocalization.of(context)?.locale.languageCode;
        result = await platform.invokeMethod(methodChannel, {'type': type, 'language': code});
      } else {
        result = await platform.invokeMethod(methodChannel, {'type': type});
      }
      return result;
    } on PlatformException catch (e) {
      print(e);
    }
  }

  static const String COMMON_CHANNEL_PLATFORM = 'kiplePay.flutter.platform';
  static const String TOP_UP_UUID = 'autoTopUpGetUuid';

  static Future commonChannelPlatform(String methodChannel) async {
    final platform = MethodChannel(COMMON_CHANNEL_PLATFORM);
    try {
      var result = await platform.invokeMethod(methodChannel);
      return result;
    } on PlatformException catch (e) {
      print(e);
      return null;
    }
  }
}
