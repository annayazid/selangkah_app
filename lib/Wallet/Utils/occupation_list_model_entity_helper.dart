import 'package:selangkah_new/Wallet/models/occupation_list_model_entity.dart';

occupationListModelEntityFromJson(OccupationListModelEntity data, Map<String, dynamic> json) {
  if (json['nationalities'] != null) {
    data.nationalities = Map<dynamic, dynamic>.from(json['nationalities']);
  }
  if (json['nature_businesses'] != null) {
    data.natureBusinesses = Map<dynamic, dynamic>.from(json['nature_businesses']);
  }
  if (json['occupations'] != null) {
    data.occupations = Map<dynamic, dynamic>.from(json['occupations']);
  }
  if (json['states'] != null) {
    data.states = Map<dynamic, dynamic>.from(json['states']);
  }
  return data;
}

Map<String, dynamic> occupationListModelEntityToJson(OccupationListModelEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['nationalities'] = entity.nationalities;
  data['nature_businesses'] = entity.natureBusinesses;
  data['occupations'] = entity.occupations;
  data['states'] = entity.states;
  return data;
}
