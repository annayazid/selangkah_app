import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Wallet/Screens/Base/base_notifier/base_provider.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/http_api.dart';
import 'package:selangkah_new/utils/secure_storage.dart';
import 'package:sp_util/sp_util.dart';

import 'app_helper.dart';
import 'log_utils.dart';

class UploadNotificationProvider extends BaseProvider {
  UploadNotificationProvider(BuildContext buildContext) : super(buildContext) {
    startNotificationRequest();
  }

  startNotificationRequest() {
    SecureStorage().readSecureData("userId").then((selangkahId) {
      if (selangkahId != null && selangkahId.isNotEmpty) {
        commitNotificationToken();
      }
    });
  }

  int circleTimes = 0;

  Future<void> commitNotificationToken() async {
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
    _firebaseMessaging.getToken().then((value) async {
      Log.d("==========token=${value}======");
      if (value != null && value.isNotEmpty) {
        Map map = Map();
        map["device_id"] = await AppHelper().getDeviceID();
        map["os_name"] = AppHelper().getDevicePlatform();
        map["os_version"] = await AppHelper().getDeviceVersion();
        map["app_name"] = await AppHelper().getAppName();
        map["app_version"] = await AppHelper().getVersionNumber();
        map["token"] = value;
        DioUtils.instance.requestNetwork(Method.post,
            HttpApi.KipleBaseUrl + HttpApi.NotificationTokenRegister,
            params: map, onSuccess: (data) {
          circleTimes = 0;
        }, onError: (code, message) {
          circleTimes++;
          if (circleTimes < 3) {
            startNotificationRequest();
          } else {
            circleTimes = 0;
          }
        });
      }
    });
    _firebaseMessaging.subscribeToTopic("selangkah_announcement");
  }
}
