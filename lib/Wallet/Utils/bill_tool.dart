import 'dart:convert';

import 'package:selangkah_new/Wallet/Utils/json_convert_content.dart';
import 'package:selangkah_new/Wallet/models/operator_model_entity.dart';
import 'package:selangkah_new/Wallet/net/common.dart';
import 'package:selangkah_new/utils/secure_storage.dart';

class BillTool {
  static saveBill(OperatorModelEntity modelEntity) async {
    Map mapModel = modelEntity.toJson();
    List<Map> newJsonList = [];
    var jsonStr = await SecureStorage().readSecureData(Constant.MyBillsKey);
    if (jsonStr != null) {
      var saveList = jsonDecode(jsonStr);
      List<OperatorModelEntity> modelList = modelHandle(saveList);
      modelList.forEach((element) {
        if (modelEntity.id != element.id) {
          newJsonList.add(element.toJson());
        }
      });
    }
    newJsonList.add(mapModel);
    SecureStorage().writeSecureData(Constant.MyBillsKey, jsonEncode(newJsonList));
  }

  static List<OperatorModelEntity> modelHandle(list) {
    List<OperatorModelEntity> modelList = [];
    list.forEach((element) {
      OperatorModelEntity? model = JsonConvert.fromJsonAsT<OperatorModelEntity>(element);
      if (model != null) modelList.add(model);
    });
    return modelList;
  }

  static deleteBillers(id) async {
    String jsonStr = await SecureStorage().readSecureData(Constant.MyBillsKey);
    var saveList = jsonDecode(jsonStr);
    saveList.removeWhere((element) {
      return element['id'] == id;
    });
    SecureStorage().writeSecureData(Constant.MyBillsKey, jsonEncode(saveList));
  }
}
