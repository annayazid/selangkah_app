import 'dart:math' as math;

import 'package:flutter/services.dart';

class MoneyFormat extends TextInputFormatter {
  MoneyFormat({this.decimalRange = 2, this.maxMoney}) : assert(decimalRange == null || decimalRange > 0);

  final int decimalRange;
  double? maxMoney;

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    String nValue = newValue.text;
    print("==11====$nValue====");
    TextSelection nSelection = newValue.selection;

    Pattern p = RegExp(r'(\d+\.?)|(\.?\d+)|(\.?)');
    nValue = p.allMatches(nValue).map<String>((Match match) => match.group(0)!).join();

    if (nValue.startsWith('.')) {
      nValue = '0.';
    } else if (maxMoney != null && nValue != null && nValue.isNotEmpty && double.parse(nValue) > this.maxMoney!) {
      nValue = oldValue.text;
    } else if (nValue.contains('.')) {
      if (nValue.substring(nValue.indexOf('.') + 1).length > decimalRange) {
        nValue = oldValue.text;
      } else {
        if (nValue.split('.').length > 2) {
          List<String> split = nValue.split('.');
          nValue = split[0] + '.' + split[1];
        }
      }
    }

    nSelection = newValue.selection.copyWith(
      baseOffset: math.min(nValue.length, nValue.length + 1),
      extentOffset: math.min(nValue.length, nValue.length + 1),
    );

    return TextEditingValue(text: nValue, selection: nSelection, composing: TextRange.empty);
  }
}
