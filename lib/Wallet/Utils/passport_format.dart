import 'dart:math' as math;

import 'package:flutter/services.dart';

class PassportFormat extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    String nValue = newValue.text;
    TextSelection nSelection = newValue.selection;
    Pattern p = RegExp(r"^[ZA-ZZa-z0-9_\-]+$");
    nValue = p.allMatches(nValue).map<String>((Match match) => match.group(0)!).join();

    nSelection = newValue.selection.copyWith(
      baseOffset: math.min(nValue.length, nValue.length + 1),
      extentOffset: math.min(nValue.length, nValue.length + 1),
    );

    return TextEditingValue(text: nValue, selection: nSelection, composing: TextRange.empty);
  }
}
