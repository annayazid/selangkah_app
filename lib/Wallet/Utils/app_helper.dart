import 'dart:io';

import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:device_info/device_info.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:selangkah_new/Wallet/Services/navigation_service.dart';
import 'package:selangkah_new/Wallet/Utils/log_utils.dart';
import 'package:selangkah_new/utils/global.dart';

class AppHelper {
  static AppHelper? _instance;
  static AndroidDeviceInfo? _androidInfo;
  static IosDeviceInfo? _iosInfo;
  static PackageInfo? packageInfo;

  static Future<AppHelper?> getInstance() async {
    if (_instance == null) {
      _instance = AppHelper();
    }
    if (Platform.isAndroid) {
      _androidInfo = await DeviceInfoPlugin().androidInfo;
    } else if (Platform.isIOS) {
      _iosInfo = await DeviceInfoPlugin().iosInfo;
    }
    packageInfo = await PackageInfo.fromPlatform();
    return _instance;
  }

  Future<Map<String, dynamic>> getDeviceMap() async {
    Map<String, dynamic> map = Map();
    map["device_model"] = await getDeviceName();
    map["device_id"] = await getDeviceID();

    Position? position;

    await GlobalFunction.displayDisclosure(
      icon: Icon(
        Icons.location_on_outlined,
        color: Colors.blue,
        size: 30,
      ),
      title: 'locationAccessTitle'.tr(),
      description: 'locationAccess'.tr(),
      key: 'locationAccess',
    )?.then((value) async {
      if (value) {
        if (await Permission.camera.request().isGranted) {
          if (value) {
            position = await getPosition().timeout(Duration(seconds: 10),
                onTimeout: () {
              return null;
            });
          }
        } else {
          Log.d("Camera Permission denied");
          return Flushbar(
            backgroundColor: Colors.white,
            titleText: Text(
              'permission_continue'.tr(),
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
            messageText: Text(
              'continue_permission'.tr(),
              style: TextStyle(color: Colors.black),
            ),
            mainButton: TextButton(
              onPressed: AppSettings.openAppSettings,
              child: Text(
                'App Settings',
                style: TextStyle(color: Colors.blue),
              ),
            ),
            duration: Duration(seconds: 7),
          ).show(NavigationService.navigatorKey.currentState!.overlay!.context);
        }
      }
    });

    if (position != null) {
      map["latitude"] = position?.latitude.toString();
      map["longitude"] = position?.longitude.toString();
    }
    return Future.value(map);
  }

  String getDevicePlatform() {
    if (Platform.isAndroid) {
      return 'Android';
    } else if (Platform.isIOS) {
      return 'iOS';
    }
    return '';
  }

  String getDeviceModel() {
    if (Platform.isAndroid && _androidInfo != null) {
      final String? manufacturer = _androidInfo?.manufacturer;
      final String? model = _androidInfo?.model;
      print('$manufacturer $model');
      return '$manufacturer $model';
    } else if (Platform.isIOS && _iosInfo != null) {
      final String? name = _iosInfo?.utsname.machine;
      print('$name');

      return '$name';
    }
    return '';
  }

  Future<String> getDeviceID() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor;
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId;
    }
  }

  DeviceInfoPlugin _deviceInfo = DeviceInfoPlugin();

  Future<String> getDeviceName() async {
    if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await _deviceInfo.iosInfo;
      String? iosModel = iosModelTable[iosInfo.utsname.machine];
      return iosModel ?? iosInfo.name;
    } else {
      AndroidDeviceInfo androidInfo = await _deviceInfo.androidInfo;
      return androidInfo.model;
    }
  }

  Future<String> getDeviceVersion() async {
    if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await _deviceInfo.iosInfo;
      String iosModel = iosModelTable[iosInfo.utsname.machine];
      return iosModel;
    } else {
      AndroidDeviceInfo androidInfo = await _deviceInfo.androidInfo;
      return androidInfo.display;
    }
  }

  Map iosModelTable = {
    "iPhone5,1": "iPhone 5 (GSM)",
    "iPhone5,2": "iPhone 5 (GSM+CDMA)",
    "iPhone5,3": "iPhone 5C (GSM)",
    "iPhone5,4": "iPhone 5C (Global)",
    "iPhone6,1": "iPhone 5S (GSM)",
    "iPhone6,2": "iPhone 5S (Global)",
    "iPhone7,1": "iPhone 6 Plus",
    "iPhone7,2": "iPhone 6",
    "iPhone8,1": "iPhone 6s",
    "iPhone8,2": "iPhone 6s Plus",
    "iPhone8,3": "iPhone SE (GSM+CDMA)",
    "iPhone8,4": "iPhone SE (GSM)",
    "iPhone9,1": "iPhone 7",
    "iPhone9,2": "iPhone 7 Plus",
    "iPhone9,3": "iPhone 7",
    "iPhone9,4": "iPhone 7 Plus",
    "iPhone10,1": "iPhone 8",
    "iPhone10,2": "iPhone 8 Plus",
    "iPhone10,3": "iPhone X Global",
    "iPhone10,4": "iPhone 8",
    "iPhone10,5": "iPhone 8 Plus",
    "iPhone10,6": "iPhone X GSM",
    "iPhone11,2": "iPhone XS",
    "iPhone11,4": "iPhone XS Max",
    "iPhone11,6": "iPhone XS Max Global",
    "iPhone11,8": "iPhone XR",
    "iPhone12,1": "iPhone 11",
    "iPhone12,3": "iPhone 11 Pro",
    "iPhone12,5": "iPhone 11 Pro Max",
    "iPhone13,1": "iPhone 12 mini",
    "iPhone13,2": "iPhone 12",
    "iPhone13,3": "iPhone 12 Pro",
    "iPhone13,4": "iPhone 12 Pro Max",
  };

  Position? positionCurrent;

  Future<Position?> getPosition() async {
    try {
      final permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        final permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          return null;
        }
        positionCurrent = await Geolocator.getCurrentPosition();
      } else
        positionCurrent = await Geolocator.getCurrentPosition();
    } catch (e) {}

    return positionCurrent;
  }

  Future<String?> getVersionNumber() async {
    if (packageInfo == null) {
      packageInfo = await PackageInfo.fromPlatform();
    }
    return packageInfo?.version;
  }

  Future<String?> getBuildNumber() async {
    if (packageInfo == null) {
      packageInfo = await PackageInfo.fromPlatform();
    }
    return packageInfo?.buildNumber;
  }

  Future<String?> getAppName() async {
    if (packageInfo == null) {
      packageInfo = await PackageInfo.fromPlatform();
    }
    return packageInfo?.appName;
  }

  bool isTablet(BuildContext context) {
    if (Platform.isIOS) {
      return _iosInfo?.model.toLowerCase() == "ipad";
    } else {
      var shortestSide = MediaQuery.of(context).size.shortestSide;
      return shortestSide > 600;
    }
  }
}
