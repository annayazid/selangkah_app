// import 'dart:convert';
// import 'dart:io';

// import 'package:app_settings/app_settings.dart';
// import 'package:device_info/device_info.dart';
// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;
// import 'package:selangkah_new/Wallet/Services/navigation_service.dart';
// import 'package:selangkah_new/Wallet/Utils/globar_var.dart';
// import 'package:selangkah_new/utils/endpoint.dart';
// import 'package:selangkah_new/utils/secure_storage.dart';

// class GlobalFunction {
//   static Future<String> getDeviceId() async {
//     var deviceInfo = DeviceInfoPlugin();
//     if (Platform.isIOS) {
//       var iosDeviceInfo = await deviceInfo.iosInfo;
//       return iosDeviceInfo.identifierForVendor;
//     } else {
//       var androidDeviceInfo = await deviceInfo.androidInfo;
//       return androidDeviceInfo.androidId;
//     }
//   }

//   static int convertDateToSeconds(DateTime date) {
//     return (date.millisecondsSinceEpoch / 1000).round();
//   }

//   static DateTime convertStrToDateTime(String str) {
//     DateTime parseDate = new DateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
//     return parseDate;
//   }

//   static Future<void> startSession() async {
//     SecureStorage secureStorage = SecureStorage();
//     String id = await secureStorage.readSecureData('guestId');

//     final Map<String, String> map = {
//       "id_selangkah_user": id,
//       "token": TOKEN,
//     };

//     final response = await http.post(
//       Uri.parse('$API_URL/set_start_session'),
//       headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//       body: map,
//     );

//     print('calling start session');
//     print(response.body);

//     var decoded = json.decode(response.body);
//     String sessionId = decoded['Session'];
//     GlobalVariables.sessionId = sessionId;
//     print('session id is ${GlobalVariables.sessionId}');
//   }

//   static Future<void> screenJourney(String screenId) async {
//     SecureStorage secureStorage = SecureStorage();
//     String id = await secureStorage.readSecureData('guestId');

//     String? sessionId = GlobalVariables.sessionId;

//     final Map<String, String> map = {
//       "id_selangkah_user": id,
//       "screen_id": screenId,
//       "session_id": "$sessionId",
//       "token": TOKEN,
//     };

//     final response = await http.post(
//       Uri.parse('$API_URL/app_screen_journey'),
//       headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//       body: map,
//     );

//     print('screen id $screenId');
//     print(response.body);
//   }

//   static Future<bool>? displayDisclosure({
//     Widget? icon,
//     String? title,
//     String? description,
//     required String key,
//   }) async {
//     bool proceed = false;

//     if (Platform.isAndroid) {
//       String? condition = await SecureStorage().readSecureData(key);

//       print('condition is $condition');

//       if (condition == null || condition == 'false') {
//         await showDialog(
//           context: NavigationService.navigatorKey.currentState!.overlay!.context,
//           builder: (context) {
//             return AlertDialog(
//               backgroundColor: Colors.white,
//               content: SingleChildScrollView(
//                 child: Column(
//                   mainAxisSize: MainAxisSize.min,
//                   children: [
//                     icon!,
//                     SizedBox(height: 10),
//                     Text(
//                       title!,
//                       textAlign: TextAlign.center,
//                       style: TextStyle(
//                         fontWeight: FontWeight.bold,
//                         fontSize: 18,
//                       ),
//                     ),
//                     SizedBox(height: 20),
//                     Text(
//                       description!,
//                       textAlign: TextAlign.center,
//                     ),
//                     SizedBox(height: 10),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceAround,
//                       children: [
//                         TextButton(
//                           child: Text(
//                             "deny".tr(),
//                             style: TextStyle(color: Colors.blue),
//                           ),
//                           onPressed: () {
//                             proceed = false;
//                             SecureStorage().writeSecureData(key, 'false');
//                             Navigator.of(context).pop();
//                           },
//                         ),
//                         TextButton(
//                           child: Text(
//                             "agree".tr(),
//                             style: TextStyle(color: Colors.blue),
//                           ),
//                           onPressed: () {
//                             proceed = true;
//                             SecureStorage().writeSecureData(key, 'true');
//                             Navigator.of(context).pop();
//                           },
//                         ),
//                       ],
//                     )
//                   ],
//                 ),
//               ),
//             );
//           },
//         );
//       } else if (condition == 'true') {
//         proceed = true;
//       } else {}
//     } else {
//       proceed = true;
//     }

//     return proceed;
//   }

//   static Future showDenyDialog(Widget icon, String key) {
//     return showDialog(
//       context: NavigationService.navigatorKey.currentState!.overlay!.context,
//       builder: (context) {
//         return AlertDialog(
//           content: SingleChildScrollView(
//             child: Column(
//               mainAxisSize: MainAxisSize.min,
//               children: [
//                 icon,
//                 SizedBox(height: 30),
//                 Text(
//                   'thank_you_permission'.tr(),
//                   textAlign: TextAlign.center,
//                 ),
//                 SizedBox(height: 30),
//                 Row(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   mainAxisAlignment: MainAxisAlignment.spaceAround,
//                   children: [
//                     Expanded(
//                       child: TextButton(
//                         child: Text(
//                           'open_app_settings'.tr(),
//                           style: TextStyle(color: Colors.blue, fontSize: 16),
//                           textAlign: TextAlign.center,
//                         ),
//                         onPressed: AppSettings.openAppSettings,
//                       ),
//                     ),
//                     Expanded(
//                       child: TextButton(
//                         child: Text(
//                           "Okay",
//                           style: TextStyle(color: Colors.blue, fontSize: 16),
//                         ),
//                         onPressed: () {
//                           Navigator.of(context).pop();
//                         },
//                       ),
//                     ),
//                   ],
//                 ),
//               ],
//             ),
//           ),
//         );
//       },
//     );
//   }
// }
