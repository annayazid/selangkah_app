import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:selangkah_new/Wallet/Utils/common_utils/common_utils.dart';

class MyTools {
  static String formatTime(int timeSecond) {
    String result = '';
    int minutes = timeSecond ~/ 60;
    int second = timeSecond % 60;
    String minutesString;
    if (minutes < 10) {
      minutesString = '0$minutes';
    } else {
      minutesString = '$minutes';
    }

    String secondString;
    if (second < 10) {
      secondString = '0$second';
    } else {
      secondString = '$second';
    }

    result = '$minutesString : $secondString';
    return result;
  }

  static String formatDateTime(DateTime time) {
    String day = time.day > 9 ? '${time.day}' : '0${time.day}';
    String month = MyTools.monthNumberToEnglish(time.month);
    String year = '${time.year}';
    String hour = '${time.hour + 8}';
    String minute = '${time.minute}';
    return '$day $month $year  $hour:$minute';
  }

  static String formatDateTimeEndDay(DateTime time) {
    String day = time.day > 9 ? '${time.day}' : '0${time.day}';
    String month = time.month > 9 ? '${time.month}' : '0${time.month}';
    String year = '${time.year}';
    return '$year-$month-$day';
  }

  static String monthNumberToEnglish(int month) {
    List<String> monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    return monthList[month - 1];
  }

  static String formatOrderDate(DateTime time) {
    String day = time.day > 9 ? '${time.day}' : '0${time.day}';
    String month = MyTools.monthNumberToEnglish(time.month);
    String year = '${time.year}';
    String hour = '${time.hour + 8}';
    String minute = '${time.minute}';
    return '$day $month $year';
  }

  static String formatOrderTimeInString(String time) {
    List<String> times = time.split(' ').first.split('-');
    String month = MyTools.monthNumberToEnglish(int.parse(times[1]));
    return '${times[2]} $month ${times[0]}';
  }

  static String formatOrderTime(DateTime time) {
    String day = time.day > 9 ? '${time.day}' : '0${time.day}';
    String month = MyTools.monthNumberToEnglish(time.month);
    String year = '${time.year}';
    int hour = time.hour;
    String minute = time.minute > 9 ? '${time.minute}' : '0${time.minute}';

    String hourStr = '';
    String timeEnd = '';
    if (hour >= 12) {
      int newHour = hour - 12;
      if (newHour == 0) newHour = 12;
      hourStr = newHour > 9 ? '$newHour' : '0$newHour';
      timeEnd = 'PM';
    } else {
      hourStr = hour > 9 ? '$hour' : '0$hour';
      timeEnd = 'AM';
    }

    return '$hourStr:$minute $timeEnd';
  }

  static bool checkEmailFormat(TextEditingController controller) {
    Pattern pattern = r'^[a-zA-Z0-9_-]+[\.]?[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$';
    RegExp emailCheck = new RegExp(pattern.toString());
    if (emailCheck.hasMatch(controller.text.toString())) {
      return true;
    } else {
      return false;
    }
  }

  static Map<String, dynamic> transferValue(value) {
    Map<String, dynamic> theValue = {
      'status': value['status'],
      'success': value['success'],
      'data': value['data'],
      'message': value['message'],
    };
    return theValue;
  }

  static String dealNumber(dynamic price, {int pointLength = 2}) {
    return (NumUtil.getNumByValueDouble(price, pointLength))!.toStringAsFixed(pointLength);
  }

  static String formatDate(DateTime time) {
    String day = time.day > 9 ? '${time.day}' : '0${time.day}';
    String month = MyTools.monthNumberToEnglish(time.month);
    String year = '${time.year}';
    int hour = time.hour + 8;
    String minute = '${time.minute > 9 ? time.minute : "0${time.minute}"}';

    String hourStr = '';
    String timeEnd = '';
    if (hour >= 12) {
      int newHour = hour - 12;
      if (newHour == 0) newHour = 12;
      hourStr = newHour > 9 ? '$newHour' : '0$newHour';
      timeEnd = 'pm';
    } else {
      hourStr = hour > 9 ? '$hour' : '0$hour';
      timeEnd = 'am';
    }
    return '$day $month $year,$hourStr:$minute$timeEnd';
  }

  static String formatDateInTransaction(String? dateTime, {bool showTime = true}) {
    if (dateTime == null || dateTime.length == 0) {
      return '';
    }
    DateTime time = DateTime.parse(dateTime);
    String day = time.day > 9 ? '${time.day}' : '0${time.day}';
    String month = MyTools.monthNumberToEnglish(time.month);
    String year = '${time.year}';
    int hour = time.hour + 8;
    String minute = '${time.minute > 9 ? time.minute : "0${time.minute}"}';

    String hourStr = '';
    String timeEnd = '';
    if (hour >= 12) {
      int newHour = hour - 12;
      if (newHour == 0) newHour = 12;
      hourStr = newHour > 9 ? '$newHour' : '0$newHour';
      timeEnd = 'PM';
    } else {
      hourStr = hour > 9 ? '$hour' : '0$hour';
      timeEnd = 'AM';
    }

    if (showTime) {
      return '$day $month $year $hourStr:$minute $timeEnd';
    } else {
      return '$day $month $year';
    }
  }

  static String dealShowMoneyString(String content) {
    content = content.replaceAll(",", "");
    if (content.contains(".")) {
      List<String> data = content.split(".");
      return '${addPoint(data[0])}.${data[1]}';
    } else {
      return '${addPoint(content)}';
    }
  }

  static String addPoint(String body) {
    String newString = "";
    if (body.length > 3) {
      List<String> spData = [];
      String numberStr = '${double.parse('${body.length / 3}')}';
      if (numberStr.contains(".")) {
        numberStr = numberStr.split('.')[0];
      }
      int forNumber = 0;
      if (body.length % 3 != 0) {
        forNumber = int.parse(numberStr) + 1;
      } else {
        forNumber = int.parse(numberStr);
      }
      for (int i = 0; i < forNumber; i++) {
        if (body.length % 3 == 0) {
          spData.add(body.substring((i) * 3, (i + 1) * 3));
        } else {
          if (i == 0) {
            spData.add(body.substring(0, (i) * 3 + body.length % 3));
          } else {
            if ((i) * 3 + (body.length % 3) > body.length) {
              spData.add(body.substring((i - 1) * 3 + (body.length % 3), body.length));
            } else {
              spData.add(body.substring((i - 1) * 3 + (body.length % 3), (i) * 3 + (body.length % 3)));
            }
          }
        }
      }
      spData.forEach((element) {
        newString = '$newString,$element';
      });
      if (newString.startsWith(",")) {
        newString = newString.substring(1, newString.length);
      }
    } else {
      newString = body;
    }
    return newString;
  }
}
