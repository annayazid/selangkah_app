import 'package:selangkah_new/Wallet/models/scan_back_model_entity.dart';

scanBackModelEntityFromJson(
    ScanBackModelEntity data, Map<String, dynamic> json) {
  if (json['message'] != null) {
    data.message = json['message'].toString();
  }
  if (json['status'] != null) {
    data.status = json['status'].toString();
  }
  if (json['success'] != null) {
    data.success = json['success'];
  }
  if (json['data'] != null) {
    data.data = json['data'];
  }
  return data;
}

Map<String, dynamic> scanBackModelEntityToJson(ScanBackModelEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['message'] = entity.message;
  data['status'] = entity.status;
  data['success'] = entity.success;
  data['data'] = entity.data;
  return data;
}
