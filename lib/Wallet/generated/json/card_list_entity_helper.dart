import 'package:selangkah_new/Wallet/models/card_list_entity.dart';

cardListEntityFromJson(CardListEntity data, Map<String, dynamic> json) {
  if (json['auto_topup_enabled'] != null) {
    data.autoTopupEnabled = json['auto_topup_enabled'] is String
        ? int.tryParse(json['auto_topup_enabled'])
        : json['auto_topup_enabled'].toInt();
  }
  if (json['cards'] != null) {
    data.cards = <CardListCard>[];
    (json['cards'] as List).forEach((v) {
      data.cards?.add(new CardListCard().fromJson(v));
    });
  }
  if (json['threshold'] != null) {
    data.threshold = json['threshold'] is String
        ? int.tryParse(json['threshold'])
        : json['threshold'].toInt();
  }
  if (json['topup_amount'] != null) {
    data.topupAmount = json['topup_amount'] is String
        ? int.tryParse(json['topup_amount'])
        : json['topup_amount'].toInt();
  }
  if (json['wallet_id'] != null) {
    data.walletId = json['wallet_id'] is String
        ? int.tryParse(json['wallet_id'])
        : json['wallet_id'].toInt();
  }
  return data;
}

Map<String, dynamic> cardListEntityToJson(CardListEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['auto_topup_enabled'] = entity.autoTopupEnabled;
  if (entity.cards != null) {
    data['cards'] = entity.cards?.map((v) => v.toJson()).toList();
  }
  data['threshold'] = entity.threshold;
  data['topup_amount'] = entity.topupAmount;
  data['wallet_id'] = entity.walletId;
  return data;
}

cardListCardFromJson(CardListCard data, Map<String, dynamic> json) {
  if (json['expiry_month'] != null) {
    data.expiryMonth = json['expiry_month'].toString();
  }
  if (json['expiry_year'] != null) {
    data.expiryYear = json['expiry_year'].toString();
  }
  if (json['holder_name'] != null) {
    data.holderName = json['holder_name'].toString();
  }
  if (json['last_topup'] != null) {
    data.lastTopup = json['last_topup'].toString();
  }
  if (json['masked_pan'] != null) {
    data.maskedPan = json['masked_pan'].toString();
  }
  if (json['primary'] != null) {
    data.primary = json['primary'].toString();
  }
  if (json['reference'] != null) {
    data.reference = json['reference'].toString();
  }
  if (json['saved_date'] != null) {
    data.savedDate = json['saved_date'].toString();
  }
  if (json['scheme'] != null) {
    data.scheme = json['scheme'].toString();
  }
  if (json['topup_enabled'] != null) {
    data.topupEnabled = json['topup_enabled'].toString();
  }
  return data;
}

Map<String, dynamic> cardListCardToJson(CardListCard entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['expiry_month'] = entity.expiryMonth;
  data['expiry_year'] = entity.expiryYear;
  data['holder_name'] = entity.holderName;
  data['last_topup'] = entity.lastTopup;
  data['masked_pan'] = entity.maskedPan;
  data['primary'] = entity.primary;
  data['reference'] = entity.reference;
  data['saved_date'] = entity.savedDate;
  data['scheme'] = entity.scheme;
  data['topup_enabled'] = entity.topupEnabled;
  return data;
}
