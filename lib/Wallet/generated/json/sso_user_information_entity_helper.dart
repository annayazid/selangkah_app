import 'package:selangkah_new/Wallet/models/sso_user_information_entity.dart';

ssoUserInformationEntityFromJson(
    SsoUserInformationEntity data, Map<String, dynamic> json) {
  if (json['user_no'] != null) {
    data.userNo = json['user_no'].toString();
  }
  if (json['acc_name'] != null) {
    data.accName = json['acc_name'].toString();
  }
  if (json['phonenumber'] != null) {
    data.phonenumber = json['phonenumber'].toString();
  }
  if (json['address1'] != null) {
    data.address1 = json['address1'].toString();
  }
  if (json['address2'] != null) {
    data.address2 = json['address2'].toString();
  }
  if (json['country'] != null) {
    data.country = json['country'].toString();
  }
  if (json['state'] != null) {
    data.state = json['state'].toString();
  }
  if (json['city'] != null) {
    data.city = json['city'].toString();
  }
  if (json['postcode'] != null) {
    data.postcode = json['postcode'].toString();
  }
  return data;
}

Map<String, dynamic> ssoUserInformationEntityToJson(
    SsoUserInformationEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['user_no'] = entity.userNo;
  data['acc_name'] = entity.accName;
  data['phonenumber'] = entity.phonenumber;
  data['address1'] = entity.address1;
  data['address2'] = entity.address2;
  return data;
}
