import 'package:selangkah_new/Wallet/models/card_manager_entity.dart';

cardManagerEntityFromJson(CardManagerEntity data, Map<String, dynamic> json) {
  if (json['url'] != null) {
    data.url = json['url'].toString();
  }
  return data;
}

Map<String, dynamic> cardManagerEntityToJson(CardManagerEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['url'] = entity.url;
  return data;
}
