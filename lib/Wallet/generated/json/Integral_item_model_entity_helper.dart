import 'package:selangkah_new/Wallet/models/Integral_item_model_entity.dart';

integralItemModelEntityFromJson(
    IntegralItemModelEntity data, Map<String, dynamic> json) {
  if (json['amount'] != null) {
    data.amount = json['amount'] is String
        ? int.tryParse(json['amount'])
        : json['amount'].toInt();
  }
  if (json['create_time'] != null) {
    data.createTime = json['create_time'].toString();
  }
  if (json['flag'] != null) {
    data.flag = json['flag'] is String
        ? int.tryParse(json['flag'])
        : json['flag'].toInt();
  }
  if (json['id'] != null) {
    data.id =
        json['id'] is String ? int.tryParse(json['id']) : json['id'].toInt();
  }
  if (json['integral'] != null) {
    data.integral = json['integral'] is String
        ? int.tryParse(json['integral'])
        : json['integral'].toInt();
  }
  if (json['merchant_id'] != null) {
    data.merchantId = json['merchant_id'] is String
        ? int.tryParse(json['merchant_id'])
        : json['merchant_id'].toInt();
  }
  if (json['update_time'] != null) {
    data.updateTime = json['update_time'].toString();
  }
  return data;
}

Map<String, dynamic> integralItemModelEntityToJson(
    IntegralItemModelEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['amount'] = entity.amount;
  data['create_time'] = entity.createTime;
  data['flag'] = entity.flag;
  data['id'] = entity.id;
  data['integral'] = entity.integral;
  data['merchant_id'] = entity.merchantId;
  data['update_time'] = entity.updateTime;
  return data;
}
