import 'package:selangkah_new/Wallet/models/withdrawal_result_model_entity.dart';

withdrawalResultModelEntityFromJson(
    WithdrawalResultModelEntity data, Map<String, dynamic> json) {
  if (json['amount'] != null) {
    data.amount = json['amount'] is String
        ? int.tryParse(json['amount'])
        : json['amount'].toInt();
  }
  if (json['balance'] != null) {
    data.balance = json['balance'].toString();
  }
  if (json['bank_name'] != null) {
    data.bankName = json['bank_name'].toString();
  }
  if (json['fee'] != null) {
    data.fee = json['fee'].toString();
  }
  if (json['order_number'] != null) {
    data.order_number = json['order_number'].toString();
  }
  if (json['trade_time'] != null) {
    data.trade_time = json['trade_time'].toString();
  }
  return data;
}

Map<String, dynamic> withdrawalResultModelEntityToJson(
    WithdrawalResultModelEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['amount'] = entity.amount;
  data['balance'] = entity.balance;
  data['bank_name'] = entity.bankName;
  data['fee'] = entity.fee;
  data['order_number'] = entity.order_number;
  data['trade_time'] = entity.trade_time;
  return data;
}
