import 'package:selangkah_new/Wallet/models/transaction_history_entity.dart';

transactionHistoryEntityFromJson(
    TransactionHistoryEntity data, Map<String, dynamic> json) {
  if (json['list'] != null) {
    data.xList = <TransactionHistoryList>[];
    (json['list'] as List).forEach((v) {
      data.xList?.add(new TransactionHistoryList().fromJson(v));
    });
  }
  if (json['total'] != null) {
    data.total = json['total'] is String
        ? int.tryParse(json['total'])
        : json['total'].toInt();
  }
  return data;
}

Map<String, dynamic> transactionHistoryEntityToJson(
    TransactionHistoryEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  if (entity.xList != null) {
    data['list'] = entity.xList?.map((v) => v.toJson()).toList();
  }
  data['total'] = entity.total;
  return data;
}

transactionHistoryListFromJson(
    TransactionHistoryList data, Map<String, dynamic> json) {
  if (json['after_balance'] != null) {
    data.afterBalance = json['after_balance'].toString();
  }
  if (json['before_balance'] != null) {
    data.beforeBalance = json['before_balance'].toString();
  }
  if (json['create_time'] != null) {
    data.createTime = json['create_time'].toString();
  }
  if (json['description'] != null) {
    data.description = json['description'].toString();
  }
  if (json['id'] != null) {
    data.id =
        json['id'] is String ? int.tryParse(json['id']) : json['id'].toInt();
  }
  if (json['money'] != null) {
    data.money = json['money'].toString();
  }
  if (json['omitempty'] != null) {
    data.omitempty = json['omitempty'];
  }
  if (json['order_sn'] != null) {
    data.orderSn = json['order_sn'].toString();
  }
  if (json['origin_transaction_no'] != null) {
    data.originTransactionNo = json['origin_transaction_no'].toString();
  }
  if (json['transaction_no'] != null) {
    data.transactionNo = json['transaction_no'].toString();
  }
  if (json['type'] != null) {
    data.type = json['type'] is String
        ? int.tryParse(json['type'])
        : json['type'].toInt();
  }
  if (json['type_desc'] != null) {
    data.typeDesc = json['type_desc'].toString();
  }
  if (json['user_id'] != null) {
    data.userId = json['user_id'] is String
        ? int.tryParse(json['user_id'])
        : json['user_id'].toInt();
  }
  if (json['void_time'] != null) {
    data.voidTime = json['void_time'] is String
        ? int.tryParse(json['void_time'])
        : json['void_time'].toInt();
  }
  if (json['void_type'] != null) {
    data.voidType = json['void_type'] is String
        ? int.tryParse(json['void_type'])
        : json['void_type'].toInt();
  }
  if (json['wallet_id'] != null) {
    data.walletId = json['wallet_id'] is String
        ? int.tryParse(json['wallet_id'])
        : json['wallet_id'].toInt();
  }
  return data;
}

Map<String, dynamic> transactionHistoryListToJson(
    TransactionHistoryList entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['after_balance'] = entity.afterBalance;
  data['before_balance'] = entity.beforeBalance;
  data['create_time'] = entity.createTime;
  data['description'] = entity.description;
  data['id'] = entity.id;
  data['money'] = entity.money;
  data['omitempty'] = entity.omitempty;
  data['order_sn'] = entity.orderSn;
  data['origin_transaction_no'] = entity.originTransactionNo;
  data['transaction_no'] = entity.transactionNo;
  data['type'] = entity.type;
  data['type_desc'] = entity.typeDesc;
  data['user_id'] = entity.userId;
  data['void_time'] = entity.voidTime;
  data['void_type'] = entity.voidType;
  data['wallet_id'] = entity.walletId;
  return data;
}
