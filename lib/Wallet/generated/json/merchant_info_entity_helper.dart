import 'package:selangkah_new/Wallet/models/merchant_info_entity.dart';

merchantInfoEntityFromJson(MerchantInfoEntity data, Map<String, dynamic> json) {
  if (json['amount'] != null) {
    data.amount = json['amount'].toString();
  }
  if (json['sub_merchant_id'] != null) {
    data.subMerchantId = json['sub_merchant_id'] is String
        ? int.tryParse(json['sub_merchant_id'])
        : json['sub_merchant_id'].toInt();
  }
  if (json['type'] != null) {
    data.type = json['type'].toString();
  }
  if (json['name'] != null) {
    data.name = json['name'].toString();
  }
  if (json['mobile'] != null) {
    data.mobile = json['mobile'].toString();
  }
  if (json['type_code'] != null) {
    data.typeCode = json['type_code'] is String
        ? int.tryParse(json['type_code'])
        : json['type_code'].toInt();
  }
  if (json['token'] != null) {
    data.token = json['token'].toString();
  }
  return data;
}

Map<String, dynamic> merchantInfoEntityToJson(MerchantInfoEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['amount'] = entity.amount;
  data['sub_merchant_id'] = entity.subMerchantId;
  data['type'] = entity.type;
  data['name'] = entity.name;
  data['mobile'] = entity.mobile;
  data['type_code'] = entity.typeCode;
  data['token'] = entity.token;
  return data;
}
