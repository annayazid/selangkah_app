import 'package:selangkah_new/Wallet/models/test_modle_entity.dart';

testModleEntityFromJson(TestModleEntity data, Map<String, dynamic> json) {
  if (json['age'] != null) {
    data.age = json['age'].toString();
  }
  if (json['name'] != null) {
    data.name = json['name'].toString();
  }
  return data;
}

Map<String, dynamic> testModleEntityToJson(TestModleEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['age'] = entity.age;
  data['name'] = entity.name;
  return data;
}
