import 'package:selangkah_new/Wallet/models/ekyc_info_entity.dart';

ekycInfoEntityFromJson(EkycInfoEntity data, Map<String, dynamic> json) {
  if (json['ekycinfo'] != null) {
    data.ekycinfo = json['ekycinfo'].toString();
  }
  if (json['status'] != null) {
    data.status = json['status'].toString();
  }
  return data;
}

Map<String, dynamic> ekycInfoEntityToJson(EkycInfoEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['ekycinfo'] = entity.ekycinfo;
  data['status'] = entity.status;
  return data;
}
