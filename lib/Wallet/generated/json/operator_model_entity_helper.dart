import 'package:selangkah_new/Wallet/models/operator_model_entity.dart';

operatorModelEntityFromJson(
    OperatorModelEntity data, Map<String, dynamic> json) {
  if (json['create_time'] != null) {
    data.createTime = json['create_time'].toString();
  }
  if (json['id'] != null) {
    data.id =
        json['id'] is String ? int.tryParse(json['id']) : json['id'].toInt();
  }
  if (json['provider_name'] != null) {
    data.providerName = json['provider_name'].toString();
  }
  if (json['provider_type'] != null) {
    data.providerType = json['provider_type'] is String
        ? int.tryParse(json['provider_type'])
        : json['provider_type'].toInt();
  }
  if (json['update_time'] != null) {
    data.updateTime = json['update_time'].toString();
  }
  if (json['account'] != null) {
    data.account = json['account'].toString();
  }
  if (json['amount'] != null) {
    data.amount = json['amount'].toString();
  }
  return data;
}

Map<String, dynamic> operatorModelEntityToJson(OperatorModelEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['create_time'] = entity.createTime;
  data['id'] = entity.id;
  data['provider_name'] = entity.providerName;
  data['provider_type'] = entity.providerType;
  data['update_time'] = entity.updateTime;
  data['account'] = entity.account;
  data['amount'] = entity.amount;
  return data;
}
