import 'package:selangkah_new/Wallet/models/notification_data_modle_entity.dart';

notificationDataModleEntityFromJson(
    NotificationDataModleEntity data, Map<String, dynamic> json) {
  if (json['reason'] != null) {
    data.reason = json['reason'].toString();
  }
  if (json['status'] != null) {
    data.status = json['status'] is String
        ? int.tryParse(json['status'])
        : json['status'].toInt();
  }
  if (json['trade_type'] != null) {
    data.tradeType = json['trade_type'].toString();
  }
  return data;
}

Map<String, dynamic> notificationDataModleEntityToJson(
    NotificationDataModleEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['reason'] = entity.reason;
  data['status'] = entity.status;
  data['trade_type'] = entity.tradeType;
  return data;
}
