import 'package:selangkah_new/Wallet/models/free_config_entity.dart';

freeConfigEntityFromJson(FreeConfigEntity data, Map<String, dynamic> json) {
  if (json['id'] != null) {
    data.id =
        json['id'] is String ? int.tryParse(json['id']) : json['id'].toInt();
  }
  if (json['refund_type'] != null) {
    data.refundType = json['refund_type'] is String
        ? int.tryParse(json['refund_type'])
        : json['refund_type'].toInt();
  }
  if (json['refund_value'] != null) {
    data.refundValue = json['refund_value'].toString();
  }
  if (json['serve_charges'] != null) {
    data.serveCharges = json['serve_charges'].toString();
  }
  if (json['status'] != null) {
    data.status = json['status'] is String
        ? int.tryParse(json['status'])
        : json['status'].toInt();
  }
  if (json['withdraw_type'] != null) {
    data.withdrawType = json['withdraw_type'].toString();
  }
  if (json['withdraw_value'] != null) {
    data.withdrawValue = json['withdraw_value'].toString();
  }
  return data;
}

Map<String, dynamic> freeConfigEntityToJson(FreeConfigEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['id'] = entity.id;
  data['refund_type'] = entity.refundType;
  data['refund_value'] = entity.refundValue;
  data['serve_charges'] = entity.serveCharges;
  data['status'] = entity.status;
  data['withdraw_type'] = entity.withdrawType;
  data['withdraw_value'] = entity.withdrawValue;
  return data;
}
