import 'package:selangkah_new/Wallet/models/user_profile_entity.dart';

userProfileEntityFromJson(UserProfileEntity data, Map<String, dynamic> json) {
  if (json['address'] != null) {
    data.address = json['address'].toString();
  }
  if (json['birthdate'] != null) {
    data.birthdate = json['birthdate'].toString();
  }
  if (json['city'] != null) {
    data.city = json['city'].toString();
  }
  if (json['email'] != null) {
    data.email = json['email'].toString();
  }
  if (json['full_name'] != null) {
    data.fullName = json['full_name'].toString();
  }
  if (json['gender'] != null) {
    data.gender = json['gender'] is String
        ? int.tryParse(json['gender'])
        : json['gender'].toInt();
  }
  if (json['id_number'] != null) {
    data.idNumber = json['id_number'].toString();
  }
  if (json['id_type'] != null) {
    data.idType = json['id_type'] is String
        ? int.tryParse(json['id_type'])
        : json['id_type'].toInt();
  }
  if (json['mobile'] != null) {
    data.mobile = json['mobile'].toString();
  }
  if (json['nationality'] != null) {
    data.nationality = json['nationality'].toString();
  }
  if (json['nature_business'] != null) {
    data.natureBusiness = json['nature_business'].toString();
  }
  if (json['occupation'] != null) {
    data.occupation = json['occupation'].toString();
  }
  if (json['source_merchant_id'] != null) {
    data.sourceMerchantId = json['source_merchant_id'] is String
        ? int.tryParse(json['source_merchant_id'])
        : json['source_merchant_id'].toInt();
  }
  if (json['state'] != null) {
    data.state = json['state'].toString();
  }
  if (json['status'] != null) {
    data.status = json['status'] is String
        ? int.tryParse(json['status'])
        : json['status'].toInt();
  }
  if (json['username'] != null) {
    data.username = json['username'].toString();
  }
  return data;
}

Map<String, dynamic> userProfileEntityToJson(UserProfileEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['address'] = entity.address;
  data['birthdate'] = entity.birthdate;
  data['city'] = entity.city;
  data['email'] = entity.email;
  data['full_name'] = entity.fullName;
  data['gender'] = entity.gender;
  data['id_number'] = entity.idNumber;
  data['id_type'] = entity.idType;
  data['mobile'] = entity.mobile;
  data['nationality'] = entity.nationality;
  data['nature_business'] = entity.natureBusiness;
  data['occupation'] = entity.occupation;
  data['source_merchant_id'] = entity.sourceMerchantId;
  data['state'] = entity.state;
  data['status'] = entity.status;
  data['username'] = entity.username;
  return data;
}
