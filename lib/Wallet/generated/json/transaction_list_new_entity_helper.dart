import 'package:selangkah_new/Wallet/models/transaction_list_new_entity.dart';

transactionListNewEntityFromJson(
    TransactionListNewEntity data, Map<String, dynamic> json) {
  if (json['list'] != null) {
    data.xList = <TransactionListNewList>[];
    (json['list'] as List).forEach((v) {
      data.xList?.add(new TransactionListNewList().fromJson(v));
    });
  }
  if (json['total'] != null) {
    data.total = json['total'] is String
        ? int.tryParse(json['total'])
        : json['total'].toInt();
  }
  return data;
}

Map<String, dynamic> transactionListNewEntityToJson(
    TransactionListNewEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  if (entity.xList != null) {
    data['list'] = entity.xList?.map((v) => v.toJson()).toList();
  }
  data['total'] = entity.total;
  return data;
}

transactionListNewListFromJson(
    TransactionListNewList data, Map<String, dynamic> json) {
  if (json['actual_amount'] != null) {
    data.actualAmount = json['actual_amount'].toString();
  }
  if (json['amount'] != null) {
    data.amount = json['amount'].toString();
  }
  if (json['bank_amount'] != null) {
    data.bankAmount = json['bank_amount'].toString();
  }
  if (json['bank_code'] != null) {
    data.bankCode = json['bank_code'].toString();
  }
  if (json['bank_order_no'] != null) {
    data.bankOrderNo = json['bank_order_no'].toString();
  }
  if (json['bank_time'] != null) {
    data.bankTime = json['bank_time'].toString();
  }
  if (json['client_id'] != null) {
    data.clientId = json['client_id'] is String
        ? int.tryParse(json['client_id'])
        : json['client_id'].toInt();
  }
  if (json['create_time'] != null) {
    data.createTime = json['create_time'].toString();
  }
  if (json['fee'] != null) {
    data.fee = json['fee'].toString();
  }
  if (json['id'] != null) {
    data.id =
    json['id'] is String ? int.tryParse(json['id']) : json['id'].toInt();
  }
  if (json['merchant_id'] != null) {
    data.merchantId = json['merchant_id'] is String
        ? int.tryParse(json['merchant_id'])
        : json['merchant_id'].toInt();
  }
  if (json['merchant_name'] != null) {
    data.merchantName = json['merchant_name'].toString();
  }
  if (json['merchant_order_no'] != null) {
    data.merchantOrderNo = json['merchant_order_no'].toString();
  }
  if (json['mode_of_pay'] != null) {
    data.modeOfPay = json['mode_of_pay'].toString();
  }
  if (json['order_no'] != null) {
    data.orderNo = json['order_no'].toString();
  }
  if (json['serve_charges'] != null) {
    data.serveCharges = json['serve_charges'].toString();
  }
  if (json['sub_pay_type'] != null) {
    data.subPayType = json['sub_pay_type'].toString();
  }
  if (json['trade_no'] != null) {
    data.tradeNo = json['trade_no'].toString();
  }
  if (json['trade_time'] != null) {
    data.tradeTime = json['trade_time'].toString();
  }
  if (json['type'] != null) {
    data.type = json['type'].toString();
  }
  if (json['user_id'] != null) {
    data.userId = json['user_id'] is String
        ? int.tryParse(json['user_id'])
        : json['user_id'].toInt();
  }
  if (json['user_name'] != null) {
    data.userName = json['user_name'].toString();
  }
  if (json['wallet_type'] != null) {
    data.walletType = json['wallet_type'].toString();
  }
  if (json['pay_time'] != null) {
    data.payTime = json['pay_time'].toString();
  }
  if (json['pay_type'] != null) {
    data.payType = json['pay_type'].toString();
  }
  if (json['status'] != null) {
    data.status = json['status'] is String
        ? int.tryParse(json['status'])
        : json['status'].toInt();
  }
  if (json['transfer_user_name'] != null) {
    data.transferUserName = json['transfer_user_name'].toString();
  }
  if (json['transfer_full_name'] != null) {
    data.transferFullName = json['transfer_full_name'].toString();
  }
  if (json['origin_order_no'] != null) {
    data.originOrderNo = json['origin_order_no'].toString();
  }
  if (json['title'] != null) {
    data.title = json['title'].toString();
  }
  if (json['point_earned'] != null) {
    data.pointEarned = json['point_earned'] is String
        ? int.tryParse(json['point_earned'])
        : json['point_earned'].toInt();
  }
  if (json['point_used'] != null) {
    data.pointUsed = json['point_used'] is String
        ? int.tryParse(json['point_used'])
        : json['point_used'].toInt();
  }
  if (json['completed_time'] != null) {
    data.completedTime = json['completed_time'].toString();
  }
  return data;
}

Map<String, dynamic> transactionListNewListToJson(
    TransactionListNewList entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['actual_amount'] = entity.actualAmount;
  data['amount'] = entity.amount;
  data['bank_amount'] = entity.bankAmount;
  data['bank_code'] = entity.bankCode;
  data['bank_order_no'] = entity.bankOrderNo;
  data['bank_time'] = entity.bankTime;
  data['client_id'] = entity.clientId;
  data['create_time'] = entity.createTime;
  data['fee'] = entity.fee;
  data['id'] = entity.id;
  data['merchant_id'] = entity.merchantId;
  data['merchant_name'] = entity.merchantName;
  data['merchant_order_no'] = entity.merchantOrderNo;
  data['mode_of_pay'] = entity.modeOfPay;
  data['order_no'] = entity.orderNo;
  data['serve_charges'] = entity.serveCharges;
  data['sub_pay_type'] = entity.subPayType;
  data['trade_no'] = entity.tradeNo;
  data['trade_time'] = entity.tradeTime;
  data['type'] = entity.type;
  data['user_id'] = entity.userId;
  data['user_name'] = entity.userName;
  data['wallet_type'] = entity.walletType;
  data['pay_time'] = entity.payTime;
  data['pay_type'] = entity.payType;
  data['status'] = entity.status;
  data['transfer_user_name'] = entity.transferUserName;
  data['transfer_full_name'] = entity.transferFullName;
  data['origin_order_no'] = entity.originOrderNo;
  data['title'] = entity.title;
  data['point_earned'] = entity.pointEarned;
  data['point_used'] = entity.pointUsed;
  return data;
}
