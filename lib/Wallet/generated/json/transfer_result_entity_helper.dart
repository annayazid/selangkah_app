import 'package:selangkah_new/Wallet/models/transfer_result_entity.dart';

transferResultEntityFromJson(
    TransferResultEntity data, Map<String, dynamic> json) {
  if (json['amount'] != null) {
    data.amount = json['amount'] is String
        ? int.tryParse(json['amount'])
        : json['amount'].toInt();
  }
  if (json['order_number'] != null) {
    data.orderNumber = json['order_number'].toString();
  }
  if (json['trade_time'] != null) {
    data.tradeTime = json['trade_time'].toString();
  }
  return data;
}

Map<String, dynamic> transferResultEntityToJson(TransferResultEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['amount'] = entity.amount;
  data['order_number'] = entity.orderNumber;
  data['trade_time'] = entity.tradeTime;
  return data;
}
