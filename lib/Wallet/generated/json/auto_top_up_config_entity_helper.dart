import 'package:selangkah_new/Wallet/models/auto_top_up_config_entity.dart';

autoTopUpConfigEntityFromJson(
    AutoTopUpConfigEntity data, Map<String, dynamic> json) {
  if (json['balance_limit'] != null) {
    data.balanceLimit = json['balance_limit'] is String
        ? int.tryParse(json['balance_limit'])
        : json['balance_limit'].toInt();
  }
  if (json['topup_min_limit'] != null) {
    data.topupMinLimit = json['topup_min_limit'] is String
        ? int.tryParse(json['topup_min_limit'])
        : json['topup_min_limit'].toInt();
  }
  if (json['card_number'] != null) {
    data.cardNumber = json['card_number'].toString();
  }
  if (json['trigger_button'] != null) {
    data.triggerButton = json['trigger_button'] is String
        ? int.tryParse(json['trigger_button'])
        : json['trigger_button'].toInt();
  }
  if (json['type'] != null) {
    data.type = json['type'].toString();
  }
  return data;
}

Map<String, dynamic> autoTopUpConfigEntityToJson(AutoTopUpConfigEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['balance_limit'] = entity.balanceLimit;
  data['topup_min_limit'] = entity.topupMinLimit;
  data['card_number'] = entity.cardNumber;
  data['trigger_button'] = entity.triggerButton;
  data['type'] = entity.type;
  return data;
}
