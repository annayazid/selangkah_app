import 'package:selangkah_new/Wallet/models/scan_payment_result_entity.dart';

scanPaymentResultEntityFromJson(
    ScanPaymentResultEntity data, Map<String, dynamic> json) {
  if (json['amount'] != null) {
    data.amount = json['amount'].toString();
  }
  if (json['merchant_id'] != null) {
    data.merchantId = json['merchant_id'] is String
        ? int.tryParse(json['merchant_id'])
        : json['merchant_id'].toInt();
  }
  if (json['merchant_order_no'] != null) {
    data.merchantOrderNo = json['merchant_order_no'].toString();
  }
  if (json['order_no'] != null) {
    data.orderNo = json['order_no'].toString();
  }
  if (json['bank_pay_time'] != null) {
    data.bankPayTime = json['bank_pay_time'].toString();
  }
  if (json['operator_name'] != null) {
    data.operatorName = json['operator_name'].toString();
  }
  if (json['trade_time'] != null) {
    data.tradeTime = json['trade_time'].toString();
  }
  if (json['order_number'] != null) {
    data.orderNumber = json['order_number'].toString();
  }
  if (json['gain_integral'] != null) {
    data.gainIntegral = json['gain_integral'].toString();
  }
  if (json['use_integral'] != null) {
    data.useIntegral = json['use_integral'].toString();
  }
  if (json['bank_time'] != null) {
    data.bankTime = json['bank_time'].toString();
  }
  return data;
}

Map<String, dynamic> scanPaymentResultEntityToJson(
    ScanPaymentResultEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['amount'] = entity.amount;
  data['merchant_id'] = entity.merchantId;
  data['merchant_order_no'] = entity.merchantOrderNo;
  data['order_no'] = entity.orderNo;
  data['bank_pay_time'] = entity.bankPayTime;
  data['operator_name'] = entity.operatorName;
  data['trade_time'] = entity.tradeTime;
  data['order_number'] = entity.orderNumber;
  data['gain_integral'] = entity.gainIntegral;
  data['use_integral'] = entity.useIntegral;
  data['bank_time'] = entity.bankTime;
  return data;
}
