import 'package:selangkah_new/Wallet/models/product_model_entity.dart';

productModelEntityFromJson(ProductModelEntity data, Map<String, dynamic> json) {
  if (json['amount'] != null) {
    data.amount = json['amount'] is String
        ? int.tryParse(json['amount'])
        : json['amount'].toInt();
  }
  if (json['commission_type'] != null) {
    data.commissionType = json['commission_type'] is String
        ? int.tryParse(json['commission_type'])
        : json['commission_type'].toInt();
  }
  if (json['commission_value'] != null) {
    data.commissionValue = json['commission_value'] is String
        ? int.tryParse(json['commission_value'])
        : json['commission_value'].toInt();
  }
  if (json['create_time'] != null) {
    data.createTime = json['create_time'].toString();
  }
  if (json['id'] != null) {
    data.id =
        json['id'] is String ? int.tryParse(json['id']) : json['id'].toInt();
  }
  if (json['product_code'] != null) {
    data.productCode = json['product_code'].toString();
  }
  if (json['product_sub_code'] != null) {
    data.productSubCode = json['product_sub_code'].toString();
  }
  if (json['product_type'] != null) {
    data.productType = json['product_type'] is String
        ? int.tryParse(json['product_type'])
        : json['product_type'].toInt();
  }
  if (json['provider_id'] != null) {
    data.providerId = json['provider_id'] is String
        ? int.tryParse(json['provider_id'])
        : json['provider_id'].toInt();
  }
  if (json['provider_name'] != null) {
    data.providerName = json['provider_name'].toString();
  }
  if (json['third_party'] != null) {
    data.thirdParty = json['third_party'] is String
        ? int.tryParse(json['third_party'])
        : json['third_party'].toInt();
  }
  if (json['update_time'] != null) {
    data.updateTime = json['update_time'].toString();
  }
  return data;
}

Map<String, dynamic> productModelEntityToJson(ProductModelEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['amount'] = entity.amount;
  data['commission_type'] = entity.commissionType;
  data['commission_value'] = entity.commissionValue;
  data['create_time'] = entity.createTime;
  data['id'] = entity.id;
  data['product_code'] = entity.productCode;
  data['product_sub_code'] = entity.productSubCode;
  data['product_type'] = entity.productType;
  data['provider_id'] = entity.providerId;
  data['provider_name'] = entity.providerName;
  data['third_party'] = entity.thirdParty;
  data['update_time'] = entity.updateTime;
  return data;
}
