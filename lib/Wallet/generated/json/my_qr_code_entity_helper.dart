import 'package:selangkah_new/Wallet/models/my_qr_code_entity.dart';

myQrCodeEntityFromJson(MyQrCodeEntity data, Map<String, dynamic> json) {
  if (json['name'] != null) {
    data.name = json['name'].toString();
  }
  if (json['qr_code'] != null) {
    data.qrCode = json['qr_code'].toString();
  }
  return data;
}

Map<String, dynamic> myQrCodeEntityToJson(MyQrCodeEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['name'] = entity.name;
  data['qr_code'] = entity.qrCode;
  return data;
}
