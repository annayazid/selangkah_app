import 'package:selangkah_new/Wallet/models/loyalty_list_modle_entity.dart';

loyaltyListModleEntityFromJson(
    LoyaltyListModleEntity data, Map<String, dynamic> json) {
  if (json['list'] != null) {
    data.xList = <LoyaltyListModleList>[];
    (json['list'] as List).forEach((v) {
      data.xList?.add(new LoyaltyListModleList().fromJson(v));
    });
  }
  if (json['page_size'] != null) {
    data.pageSize = json['page_size'] is String
        ? int.tryParse(json['page_size'])
        : json['page_size'].toInt();
  }
  if (json['total'] != null) {
    data.total = json['total'] is String
        ? int.tryParse(json['total'])
        : json['total'].toInt();
  }
  return data;
}

Map<String, dynamic> loyaltyListModleEntityToJson(
    LoyaltyListModleEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  if (entity.xList != null) {
    data['list'] = entity.xList?.map((v) => v.toJson()).toList();
  }
  data['page_size'] = entity.pageSize;
  data['total'] = entity.total;
  return data;
}

loyaltyListModleListFromJson(
    LoyaltyListModleList data, Map<String, dynamic> json) {
  if (json['amount'] != null) {
    data.amount = json['amount'] is String
        ? int.tryParse(json['amount'])
        : json['amount'].toInt();
  }
  if (json['config_by_type'] != null) {
    data.configByType = json['config_by_type'].toString();
  }
  if (json['create_time'] != null) {
    data.createTime = json['create_time'].toString();
  }
  if (json['description'] != null) {
    data.description = json['description'].toString();
  }
  if (json['description_malay'] != null) {
    data.description_malay = json['description_malay'].toString();
  }
  if (json['id'] != null) {
    data.id =
        json['id'] is String ? int.tryParse(json['id']) : json['id'].toInt();
  }
  if (json['integral'] != null) {
    data.integral = json['integral'] is String
        ? int.tryParse(json['integral'])
        : json['integral'].toInt();
  }
  if (json['integral_type'] != null) {
    data.integralType = json['integral_type'] is String
        ? int.tryParse(json['integral_type'])
        : json['integral_type'].toInt();
  }
  if (json['merchant_id'] != null) {
    data.merchantId = json['merchant_id'] is String
        ? int.tryParse(json['merchant_id'])
        : json['merchant_id'].toInt();
  }
  if (json['order_no'] != null) {
    data.orderNo = json['order_no'].toString();
  }
  if (json['phone_number'] != null) {
    data.phoneNumber = json['phone_number'].toString();
  }
  if (json['shopper_name'] != null) {
    data.shopperName = json['shopper_name'].toString();
  }
  if (json['update_time'] != null) {
    data.updateTime = json['update_time'].toString();
  }
  if (json['user_id'] != null) {
    data.userId = json['user_id'] is String
        ? int.tryParse(json['user_id'])
        : json['user_id'].toInt();
  }
  if (json['user_name'] != null) {
    data.userName = json['user_name'].toString();
  }
  return data;
}

Map<String, dynamic> loyaltyListModleListToJson(LoyaltyListModleList entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['amount'] = entity.amount;
  data['config_by_type'] = entity.configByType;
  data['create_time'] = entity.createTime;
  data['description'] = entity.description;
  data['description_malay'] = entity.description_malay;
  data['id'] = entity.id;
  data['integral'] = entity.integral;
  data['integral_type'] = entity.integralType;
  data['merchant_id'] = entity.merchantId;
  data['order_no'] = entity.orderNo;
  data['phone_number'] = entity.phoneNumber;
  data['shopper_name'] = entity.shopperName;
  data['update_time'] = entity.updateTime;
  data['user_id'] = entity.userId;
  data['user_name'] = entity.userName;
  return data;
}
