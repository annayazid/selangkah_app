import 'package:selangkah_new/Wallet/models/pos_model_entity.dart';

posModelEntityFromJson(PosModelEntity data, Map<String, dynamic> json) {
  if (json['amount'] != null) {
    data.amount = json['amount'].toString();
  }
  if (json['time'] != null) {
    data.time = json['time'].toString();
  }
  if (json['point_earned'] != null) {
    data.pointEarned = json['point_earned'] is String
        ? int.tryParse(json['point_earned'])
        : json['point_earned'].toInt();
  }
  if (json['merchant_name'] != null) {
    data.merchantName = json['merchant_name'].toString();
  }
  if (json['transaction_no'] != null) {
    data.transactionNo = json['transaction_no'].toString();
  }
  if (json['trade_type'] != null) {
    data.tradeType = json['trade_type'].toString();
  }
  return data;
}

Map<String, dynamic> posModelEntityToJson(PosModelEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['amount'] = entity.amount;
  data['time'] = entity.time;
  data['point_earned'] = entity.pointEarned;
  data['merchant_name'] = entity.merchantName;
  data['transaction_no'] = entity.transactionNo;
  data['trade_type'] = entity.tradeType;
  return data;
}
