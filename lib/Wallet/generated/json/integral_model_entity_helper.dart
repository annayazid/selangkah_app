import 'package:selangkah_new/Wallet/models/integral_model_entity.dart';

integralModelEntityFromJson(
    IntegralModelEntity data, Map<String, dynamic> json) {
  if (json['create_time'] != null) {
    data.createTime = json['create_time'].toString();
  }
  if (json['expired_integral'] != null) {
    data.expiredIntegral = json['expired_integral'] is String
        ? int.tryParse(json['expired_integral'])
        : json['expired_integral'].toInt();
  }
  if (json['gain_integral'] != null) {
    data.gainIntegral = json['gain_integral'] is String
        ? int.tryParse(json['gain_integral'])
        : json['gain_integral'].toInt();
  }
  if (json['id'] != null) {
    data.id =
        json['id'] is String ? int.tryParse(json['id']) : json['id'].toInt();
  }
  if (json['merchant_id'] != null) {
    data.merchantId = json['merchant_id'] is String
        ? int.tryParse(json['merchant_id'])
        : json['merchant_id'].toInt();
  }
  if (json['merchant_name'] != null) {
    data.merchantName = json['merchant_name'].toString();
  }
  if (json['phone_number'] != null) {
    data.phoneNumber = json['phone_number'].toString();
  }
  if (json['spend_integral'] != null) {
    data.spendIntegral = json['spend_integral'] is String
        ? int.tryParse(json['spend_integral'])
        : json['spend_integral'].toInt();
  }
  if (json['update_time'] != null) {
    data.updateTime = json['update_time'].toString();
  }
  if (json['user_id'] != null) {
    data.userId = json['user_id'] is String
        ? int.tryParse(json['user_id'])
        : json['user_id'].toInt();
  }
  if (json['user_name'] != null) {
    data.userName = json['user_name'].toString();
  }
  if (json['valid_integral'] != null) {
    data.validIntegral = json['valid_integral'] is String
        ? int.tryParse(json['valid_integral'])
        : json['valid_integral'].toInt();
  }
  return data;
}

Map<String, dynamic> integralModelEntityToJson(IntegralModelEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['create_time'] = entity.createTime;
  data['expired_integral'] = entity.expiredIntegral;
  data['gain_integral'] = entity.gainIntegral;
  data['id'] = entity.id;
  data['merchant_id'] = entity.merchantId;
  data['merchant_name'] = entity.merchantName;
  data['phone_number'] = entity.phoneNumber;
  data['spend_integral'] = entity.spendIntegral;
  data['update_time'] = entity.updateTime;
  data['user_id'] = entity.userId;
  data['user_name'] = entity.userName;
  data['valid_integral'] = entity.validIntegral;
  return data;
}
