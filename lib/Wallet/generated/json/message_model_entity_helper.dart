import 'package:selangkah_new/Wallet/models/message_model_entity.dart';
import 'package:selangkah_new/Wallet/models/notification_data_modle_entity.dart';

messageModelEntityFromJson(MessageModelEntity data, Map<String, dynamic> json) {
  if (json['list'] != null) {
    data.xList = <MessageModelList>[];
    (json['list'] as List).forEach((v) {
      data.xList?.add(new MessageModelList().fromJson(v));
    });
  }
  if (json['total'] != null) {
    data.total = json['total'] is String
        ? int.tryParse(json['total'])
        : json['total'].toInt();
  }
  return data;
}

Map<String, dynamic> messageModelEntityToJson(MessageModelEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  if (entity.xList != null) {
    data['list'] = entity.xList?.map((v) => v.toJson()).toList();
  }
  data['total'] = entity.total;
  return data;
}

messageModelListFromJson(MessageModelList data, Map<String, dynamic> json) {
  if (json['order_no'] != null) {
    data.orderNo = json['order_no'].toString();
  }
  if (json['amount'] != null) {
    data.amount = json['amount'] is String
        ? int.tryParse(json['amount'])
        : json['amount'].toInt();
  }
  if (json['message_type'] != null) {
    data.messageType = json['message_type'].toString();
  }
  if (json['message_content'] != null) {
    data.messageContent = json['message_content'].toString();
  }
  if (json['receiver_token'] != null) {
    data.receiverToken = json['receiver_token'].toString();
  }
  if (json['create_time'] != null) {
    data.createTime = json['create_time'].toString();
  }
  if (json['is_read'] != null) {
    data.is_read = json['is_read'] is String
        ? int.tryParse(json['is_read'])
        : json['is_read'].toInt();
  }
  if (json['trade_type'] != null) {
    data.trade_type = json['trade_type'].toString();
  }
  if (json['notify_data'] != null) {
    data.notifyData =
        new NotificationDataModleEntity().fromJson(json['notify_data']);
  }
  if (json['message_type_malay'] != null) {
    data.message_type_malay = json['message_type_malay'].toString();
  }
  if (json['message_content_malay'] != null) {
    data.message_content_malay = json['message_content_malay'].toString();
  }
  return data;
}

Map<String, dynamic> messageModelListToJson(MessageModelList entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['order_no'] = entity.orderNo;
  data['amount'] = entity.amount;
  data['message_type'] = entity.messageType;
  data['message_content'] = entity.messageContent;
  data['receiver_token'] = entity.receiverToken;
  data['create_time'] = entity.createTime;
  data['is_read'] = entity.is_read;
  data['trade_type'] = entity.trade_type;
  if (entity.notifyData != null) {
    data['notify_data'] = entity.notifyData?.toJson();
  }
  data['message_type_malay'] = entity.message_type_malay;
  data['message_content_malay'] = entity.message_content_malay;
  return data;
}
