import 'package:selangkah_new/Wallet/models/wallet_Info_entity.dart';

walletInfoEntityFromJson(WalletInfoEntity data, Map<String, dynamic> json) {
  if (json['max_balance_limit'] != null) {
    data.maxBalanceLimit = json['max_balance_limit'].toString();
  }
  if (json['max_withdraw_limit'] != null) {
    data.maxWithdrawLimit = json['max_withdraw_limit'].toString();
  }
  if (json['total_balance'] != null) {
    data.totalBalance = json['total_balance'].toString();
  }
  if (json['useable_balance'] != null) {
    data.useableBalance = json['useable_balance'].toString();
  }
  if (json['wallet_sn'] != null) {
    data.walletSn = json['wallet_sn'] is String
        ? int.tryParse(json['wallet_sn'])
        : json['wallet_sn'].toInt();
  }
  if (json['create_time'] != null) {
    data.createTime = json['create_time'].toString();
  }
  return data;
}

Map<String, dynamic> walletInfoEntityToJson(WalletInfoEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['max_balance_limit'] = entity.maxBalanceLimit;
  data['max_withdraw_limit'] = entity.maxWithdrawLimit;
  data['total_balance'] = entity.totalBalance;
  data['useable_balance'] = entity.useableBalance;
  data['wallet_sn'] = entity.walletSn;
  data['create_time'] = entity.createTime;
  return data;
}
