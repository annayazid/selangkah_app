import 'package:selangkah_new/Wallet/models/mobile_result_entity.dart';

mobileResultEntityFromJson(MobileResultEntity data, Map<String, dynamic> json) {
  if (json['amount'] != null) {
    data.amount = json['amount'] is String
        ? int.tryParse(json['amount'])
        : json['amount'].toInt();
  }
  if (json['operator_name'] != null) {
    data.operatorName = json['operator_name'].toString();
  }
  if (json['order_number'] != null) {
    data.orderNumber = json['order_number'].toString();
  }
  if (json['trade_time'] != null) {
    data.tradeTime = json['trade_time'].toString();
  }
  if (json['use_integral'] != null) {
    data.use_integral = json['use_integral'] is String
        ? int.tryParse(json['use_integral'])
        : json['use_integral'].toInt();
  }
  if (json['gain_integral'] != null) {
    data.gain_integral = json['gain_integral'] is String
        ? int.tryParse(json['gain_integral'])
        : json['gain_integral'].toInt();
  }
  return data;
}

Map<String, dynamic> mobileResultEntityToJson(MobileResultEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['amount'] = entity.amount;
  data['operator_name'] = entity.operatorName;
  data['order_number'] = entity.orderNumber;
  data['trade_time'] = entity.tradeTime;
  data['use_integral'] = entity.use_integral;
  data['gain_integral'] = entity.gain_integral;
  return data;
}
