import 'package:selangkah_new/Wallet/models/bill_config_entity.dart';

billConfigEntityFromJson(BillConfigEntity data, Map<String, dynamic> json) {
  if (json['id'] != null) {
    data.id =
        json['id'] is String ? int.tryParse(json['id']) : json['id'].toInt();
  }
  if (json['merchant_id'] != null) {
    data.merchantId = json['merchant_id'] is String
        ? int.tryParse(json['merchant_id'])
        : json['merchant_id'].toInt();
  }
  if (json['product_type'] != null) {
    data.productType = json['product_type'] is String
        ? int.tryParse(json['product_type'])
        : json['product_type'].toInt();
  }
  if (json['product_switch'] != null) {
    data.productSwitch = json['product_switch'] is String
        ? int.tryParse(json['product_switch'])
        : json['product_switch'].toInt();
  }
  if (json['commission_type'] != null) {
    data.commissionType = json['commission_type'] is String
        ? int.tryParse(json['commission_type'])
        : json['commission_type'].toInt();
  }
  if (json['partner_percent'] != null) {
    data.partnerPercent = json['partner_percent'] is String
        ? int.tryParse(json['partner_percent'])
        : json['partner_percent'].toInt();
  }
  if (json['kiple_percent'] != null) {
    data.kiplePercent = json['kiple_percent'] is String
        ? int.tryParse(json['kiple_percent'])
        : json['kiple_percent'].toInt();
  }
  if (json['partner_amount'] != null) {
    data.partnerAmount = json['partner_amount'] is String
        ? int.tryParse(json['partner_amount'])
        : json['partner_amount'].toInt();
  }
  if (json['kiple_amount'] != null) {
    data.kipleAmount = json['kiple_amount'] is String
        ? int.tryParse(json['kiple_amount'])
        : json['kiple_amount'].toInt();
  }
  if (json['frequency'] != null) {
    data.frequency = json['frequency'] is String
        ? int.tryParse(json['frequency'])
        : json['frequency'].toInt();
  }
  if (json['provider'] != null) {
    data.provider = json['provider'] is String
        ? int.tryParse(json['provider'])
        : json['provider'].toInt();
  }
  if (json['create_time'] != null) {
    data.createTime = json['create_time'].toString();
  }
  if (json['update_time'] != null) {
    data.updateTime = json['update_time'].toString();
  }
  return data;
}

Map<String, dynamic> billConfigEntityToJson(BillConfigEntity entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['id'] = entity.id;
  data['merchant_id'] = entity.merchantId;
  data['product_type'] = entity.productType;
  data['product_switch'] = entity.productSwitch;
  data['commission_type'] = entity.commissionType;
  data['partner_percent'] = entity.partnerPercent;
  data['kiple_percent'] = entity.kiplePercent;
  data['partner_amount'] = entity.partnerAmount;
  data['kiple_amount'] = entity.kipleAmount;
  data['frequency'] = entity.frequency;
  data['provider'] = entity.provider;
  data['create_time'] = entity.createTime;
  data['update_time'] = entity.updateTime;
  return data;
}
