import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class ClickButton extends StatelessWidget {
  final Function? clickFunction;
  final String content;
  final double? height;
  final double? width;
  final bool canClick;
  final int fontSize;
  final Function? checkFunction;

  ClickButton(
      {required this.content,
      this.height,
      this.width,
      this.canClick = true,
      this.clickFunction,
      this.fontSize = 16,
      this.checkFunction});

  @override
  Widget build(BuildContext context) {
    double? getHeight = height;
    double? getWidth = width;
    if (getHeight == null) {
      getHeight = 21.h as double?;
    }
    if (getWidth == null) {
      getWidth = MediaQuery.of(context).size.width;
    }
    return InkWell(
      child: Container(
        alignment: Alignment.center,
        width: getWidth,
        height: getHeight,
        decoration: BoxDecoration(
          color: canClick ? appColorPrimaryKiple : appLightRed,
          borderRadius: BorderRadius.circular(
            (8.0),
          ),
        ),
        child: Text(
          content,
          style: primaryTextStyle(color: canClick ? iconColorPrimary : appNotClickText, size: fontSize),
          textAlign: TextAlign.center,
        ),
      ),
      onTap: () {
        if (canClick) {
          clickFunction?.call();
        } else {
          checkFunction?.call();
        }
      },
    );
  }
}
