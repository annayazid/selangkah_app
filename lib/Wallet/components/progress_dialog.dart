import 'package:flutter/material.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class ProgressDialog extends Dialog {
  const ProgressDialog({
    Key? key,
    this.hintText = '',
  }) : super(key: key);

  final String hintText;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return Future.value(false);
      },
      child: Container(
        color: Colors.transparent,
        width: 100,
        height: 200,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            new CircularProgressIndicator(
              backgroundColor: Colors.red,
              valueColor: new AlwaysStoppedAnimation<Color>(appColorPrimaryKiple),
            )
          ],
        ),
      ),
    );
  }
}
