import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/utils/AppColors.dart';
import 'package:url_launcher/url_launcher.dart';

import 'click_button.dart';

class Talk2UsWidget extends StatelessWidget {
  final String contentString;
  final bool showButton;

  Talk2UsWidget({required this.contentString, this.showButton = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.only(top: 5.h as double, left: 7.w as double, right: 7.w as double),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            "assets/images/Wallet/icon_help_person.png",
            width: 13.w as double,
            height: 15.h as double,
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 4.w as double),
              child: Text(
                contentString,
                style: primaryTextStyle(size: 12, color: colorBlack),
              ),
            ),
          ),
          Offstage(
            offstage: !showButton,
            child: Container(
              child: ClickButton(
                content: 'talk_to_us'.tr().toString(),
                canClick: true,
                fontSize: 10,
                width: 35.w as double,
                height: 12.h as double,
                clickFunction: () {
                  showPub(context);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  callPhone() async {
    String url = 'tel:1-800-22-6600';
    await launch(url);
  }

  showPub(BuildContext buildContext) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        isDismissible: false,
        context: buildContext,
        builder: (BuildContext context) {
          return Column(
            children: [
              Expanded(
                child: Container(),
              ),
              InkWell(
                child: Container(
                  margin: EdgeInsets.only(left: 7.w as double, right: 7.w as double),
                  padding: EdgeInsets.only(
                    top: 7.h as double,
                    bottom: 7.h as double,
                  ),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(
                      (8.0),
                    ),
                  ),
                  child: Text(
                    "${"call_string".tr()} 1-800-22-6600",
                    style: primaryTextStyle(color: appTextColorPrimary, size: 16),
                    textAlign: TextAlign.center,
                  ),
                ),
                onTap: () {
                  callPhone();
                  Navigator.of(context).pop();
                },
              ),
              InkWell(
                child: Container(
                  margin: EdgeInsets.only(
                      top: 6.h as double, bottom: 3.h as double, left: 7.w as double, right: 7.w as double),
                  padding: EdgeInsets.only(
                    top: 7.h as double,
                    bottom: 7.h as double,
                  ),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(
                      (8.0),
                    ),
                  ),
                  child: Text(
                    "cancel".tr(),
                    style: primaryTextStyle(color: appTextColorPrimary, size: 16),
                    textAlign: TextAlign.center,
                  ),
                ),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }
}
