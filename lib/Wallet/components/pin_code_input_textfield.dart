import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:selangkah_new/utils/AppColors.dart';

typedef GetSmsCode = Function();

TextStyle defaultStyle = TextStyle(color: appTextColorPrimary, fontSize: 30, fontWeight: FontWeight.bold);

TextStyle defaultStyleObscure = TextStyle(
  color: appTextColorPrimary,
  fontSize: 30,
);

abstract class CodeDecoration {
  final TextStyle? textStyle;
  final ObscureStyle? obscureStyle;

  const CodeDecoration({
    this.textStyle,
    this.obscureStyle,
  });
}

class ObscureStyle {
  final bool isTextObscure;
  final String obscureText;

  const ObscureStyle({
    this.isTextObscure = false,
    this.obscureText = '*',
  }) : assert(obscureText.length == 1);
}

class UnderlineDecoration extends CodeDecoration {
  final double gapSpace;
  final Color color;
  final double lineHeight;
  final Color enteredColor;

  const UnderlineDecoration({
    TextStyle? textStyle,
    ObscureStyle? obscureStyle,
    this.enteredColor = appTextColorGray,
    this.gapSpace = 20.0,
    this.color = appTextColorGray,
    this.lineHeight = 1.0,
  }) : super(
          textStyle: textStyle,
          obscureStyle: obscureStyle,
        );
}

class PinCodeInputTextField extends StatefulWidget {
  final int codeLength;
  final bool? obscureText;
  final ValueChanged<String>? onSubmit;
  final CodeDecoration decoration;
  final List<TextInputFormatter> inputFormatters;
  final TextInputType keyboardType;
  final bool autoFocus;
  final FocusNode? focusNode;
  final TextInputAction textInputAction;
  final TextEditingController? controller;
  final Function(dynamic)? onTextChange;

  PinCodeInputTextField({
    this.codeLength = 6,
    this.onSubmit,
    this.controller,
    this.decoration = const UnderlineDecoration(),
    List<TextInputFormatter>? inputFormatter,
    this.keyboardType = TextInputType.number,
    this.focusNode,
    this.autoFocus = false,
    this.textInputAction = TextInputAction.done,
    this.obscureText = false,
    this.onTextChange,
  }) : inputFormatters = inputFormatter ?? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly];

  @override
  State createState() {
    return _PinCodeInputTextFieldState();
  }
}

class _PinCodeInputTextFieldState extends State<PinCodeInputTextField> with SingleTickerProviderStateMixin {
  String? _text;
  AnimationController? _animationController;
  Animation<double>? _animation;

  @override
  void initState() {
    widget.controller?.addListener(() {
      setState(() {
        _text = widget.controller?.text;
      });
      if (widget.controller?.text != null && widget.controller!.text.length >= widget.codeLength) {
        widget.controller?.text.substring(0, widget.codeLength);
      }
    });

    _animationController = AnimationController(duration: Duration(milliseconds: 500), vsync: this);

    _animation = Tween(begin: 0.0, end: 255.0).animate(_animationController!)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _animationController?.reverse();
        } else if (status == AnimationStatus.dismissed) {
          _animationController?.forward();
        }
      })
      ..addListener(() {
        setState(() {});
      });

    _animationController?.forward();

    super.initState();
  }

  @override
  void dispose() {
    _animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      foregroundPainter: _CodePaint(
          text: _text != null ? _text! : "",
          codeLength: widget.codeLength,
          decoration: widget.decoration,
          cursorAlpha: _animation!.value.toInt(),
          obscureText: widget.obscureText == null ? false : widget.obscureText,
          cursorAnimationListener: (bool isStart) {
            if (_animationController != null && !_animationController!.isAnimating && isStart) {
              _animationController?.addListener(() {
                setState(() {});
              });
            } else {
              _animationController?.removeListener(() {});
            }
          }),
      child: RepaintBoundary(
        child: Container(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                TextField(
                  controller: widget.controller,
                  style: TextStyle(
                    color: Colors.transparent,
                  ),
                  cursorColor: Colors.transparent,
                  cursorWidth: 0.0,
                  autocorrect: false,
                  textAlign: TextAlign.center,
                  enableInteractiveSelection: false,
                  maxLength: widget.codeLength,
                  onSubmitted: widget.onSubmit,
                  keyboardType: widget.keyboardType,
                  inputFormatters: widget.inputFormatters,
                  focusNode: widget.focusNode,
                  autofocus: widget.autoFocus,
                  textInputAction: widget.textInputAction,
                  obscureText: true,
                  decoration: InputDecoration(
                    counterText: '',
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                    ),
                  ),
                  onChanged: onTextChange(),
                ),
              ]),
        ),
      ),
    );
  }

  onTextChange() {
    if (widget.onTextChange != null) {
      return widget.onTextChange!;
    } else
      return;
  }
}

typedef CursorAnimationListener = Function(bool isStart);

class _CodePaint extends CustomPainter {
  String? text;
  final int codeLength;
  final double space;
  CodeDecoration? decoration;
  int? cursorAlpha;
  CursorAnimationListener? cursorAnimationListener;
  bool? isPrint;
  bool? obscureText;

  _CodePaint(
      {required String text,
      required this.codeLength,
      this.decoration,
      this.space = 4.0,
      this.cursorAlpha,
      this.cursorAnimationListener,
      this.obscureText}) {
    text ??= "";
    this.text = text.trim();
    isPrint = true;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => !(oldDelegate is _CodePaint && oldDelegate.text == this.text);

  _drawUnderLine(Canvas canvas, Size size) {
    var dr = decoration as UnderlineDecoration;
    Paint underlinePaint = Paint()
      ..color = dr.color
      ..strokeWidth = dr.lineHeight
      ..style = PaintingStyle.stroke
      ..isAntiAlias = true;

    var startX = 0.0;
    var startY = size.height - 28.0;
    double singleWidth = (size.width - (codeLength - 1) * dr.gapSpace) / codeLength;

    for (int i = 0; i < codeLength; i++) {
      if (text != null && i < text!.length) {
        underlinePaint.color = dr.enteredColor;
      } else {
        underlinePaint.color = dr.color;
      }
      canvas.drawLine(Offset(startX, startY), Offset(startX + singleWidth, startY), underlinePaint);
      startX += singleWidth + dr.gapSpace;
    }

    var index = 0;
    startX = 0.0;
    startY = 28.0;

    TextStyle? textStyle;
    if (decoration?.textStyle == null) {
      if (!obscureText!) {
        textStyle = defaultStyleObscure;
      } else {
        textStyle = defaultStyle;
      }
    } else {
      textStyle = decoration?.textStyle;
    }
    text!.runes.forEach((rune) {
      String code;
      if (obscureText != null && !obscureText!) {
        code = "•";
      } else {
        code = String.fromCharCode(rune);
      }
      TextPainter textPainter = TextPainter(
        text: TextSpan(
          style: textStyle,
          text: code,
        ),
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr,
      );

      textPainter.layout();

      startX = singleWidth * index + singleWidth / 2 - textPainter.width / 2 + dr.gapSpace * index;
      textPainter.paint(
        canvas,
        Offset(startX, startY),
      );
      index++;
    });

    if (text != null && text!.length < codeLength) {
      Color cursorColor = appColorPrimaryKiple;
      if (cursorAlpha != null) cursorColor = cursorColor.withAlpha(cursorAlpha!);

      Paint cursorPaint = Paint()
        ..color = cursorColor
        ..strokeWidth = 2.0
        ..style = PaintingStyle.stroke
        ..isAntiAlias = true;

      startX = text!.length * (singleWidth + dr.gapSpace) + singleWidth / 2;
      var endY = size.height - 36.0;
      canvas.drawLine(Offset(startX, startY), Offset(startX, endY), cursorPaint);
      if (cursorAnimationListener != null) {
        cursorAnimationListener!(true);
      }
    } else {
      if (cursorAnimationListener != null) {
        cursorAnimationListener!(false);
      }
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    _drawUnderLine(canvas, size);
  }
}
