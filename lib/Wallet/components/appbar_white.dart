import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class AppBarWhite extends StatefulWidget {
  final String title;
  final Function? bottomBack;
  final Widget child;
  final Color? backgroundColor;
  final Function? appBarBack;
  final bool hasLeading;
  final List<Widget>? actions;

  AppBarWhite(
      {required this.title,
      required this.child,
      this.bottomBack,
      this.backgroundColor,
      this.hasLeading = true,
      this.appBarBack,
      this.actions});

  @override
  State<StatefulWidget> createState() {
    return AppBarState();
  }
}

class AppBarState extends State<AppBarWhite> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            widget.title,
            style: primaryTextStyle(size: 17, color: colorBlack),
          ),
          centerTitle: true,
          brightness: Brightness.light,
          backgroundColor: iconColorPrimary,
          iconTheme: IconThemeData(color: colorBlack),
          elevation: 0,
          leading: widget.hasLeading
              ? Container(
                  child: IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    color: appTextColorPrimary,
                    size: 25,
                  ),
                  onPressed: () {
                    return onPressed();
                  },
                ))
              : Container(),
          actions: widget.actions,
        ),
        body: widget.child,
        backgroundColor: widget.backgroundColor == null ? iconColorPrimary : widget.backgroundColor,
      ),
      onWillPop: () {
        return onWillPop();
      },
    );
  }

  onPressed() {
    if (widget.appBarBack == null) {
      return Navigator.of(context).pop();
    } else {
      return widget.appBarBack;
    }
  }

  onWillPop() {
    if (widget.bottomBack == null) {
      return Navigator.of(context).pop();
    } else {
      return widget.bottomBack;
    }
  }
}
