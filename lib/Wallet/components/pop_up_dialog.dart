import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/Wallet/components/click_item.dart';
import 'package:selangkah_new/Wallet/Utils/flutter_screen_utils/flutter_screenutil.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class PopUpDialog extends StatefulWidget {
  final String title;
  final String content;
  final String confirm;
  final Function? confirmFunction;
  final bool hideCancelButton;

  PopUpDialog(
      {required this.title,
      required this.content,
      required this.confirm,
      this.confirmFunction,
      this.hideCancelButton = true});

  @override
  State<StatefulWidget> createState() {
    return _PopUpDialogState();
  }
}

class _PopUpDialogState extends State<PopUpDialog> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 188, height: 406);
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: configBody());
  }

  Widget configBody() {
    return Center(
      child: Container(
        margin: EdgeInsets.only(left: 15.w as double, right: 15.w as double),
        padding: EdgeInsets.fromLTRB(10.w as double, 10.h as double, 10.w as double, 10.h as double),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              widget.title,
              style: boldTextStyle(
                color: appTextColorPrimary,
                size: 14,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 7.h as double, bottom: 10.h as double),
              child: Text(
                widget.content,
                style: primaryTextStyle(color: appTextColorGray, size: 12),
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              child: NormalClickItem(
                  content: widget.confirm,
                  height: 24.h as double,
                  clickFunction: () {
                    if (widget.confirmFunction != null) {
                      widget.confirmFunction!();
                    }
                    Navigator.pop(context);
                  }),
            ),
            Offstage(
              offstage: widget.hideCancelButton,
              child: InkWell(
                child: Container(
                  color: Colors.white,
                  margin: EdgeInsets.only(top: 5.h as double),
                  width: double.infinity,
                  height: 24.h as double,
                  alignment: Alignment.center,
                  child: Text(
                    'cancel'.tr(),
                    style: primaryTextStyle(color: appTextColorGray, size: 14),
                  ),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
