import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:selangkah_new/utils/AppColors.dart';

class NormalClickItem extends StatelessWidget {
  final Function? clickFunction;
  final String content;
  final double height;
  final bool enable;

  NormalClickItem({this.content = 'OK', this.height = 45, this.enable = true, this.clickFunction});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        alignment: Alignment.center,
        height: height,
        decoration: BoxDecoration(
          color: enable ? appColorPrimaryKiple : appLightRed,
          borderRadius: BorderRadius.circular(
            (10.0),
          ),
        ),
        child: Text(
          content,
          style: primaryTextStyle(color: enable ? iconColorPrimary : appNotClickText, size: 16),
          textAlign: TextAlign.center,
        ),
      ),
      onTap: () {
        if (enable) clickFunction?.call();
      },
    );
  }
}
