import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:selangkah_new/Screens/Inbox/inbox.dart';
import 'package:selangkah_new/Screens/LHR/lhr_pages.dart';
import 'package:selangkah_new/Screens/ProfilePage/profile_page.dart';
import 'package:selangkah_new/Screens/Sehat/screens/content/content_screen.dart';
import 'package:selangkah_new/Screens/Sehat/sehat.dart';
import 'package:selangkah_new/utils/constants.dart';

import 'package:selangkah_new/Wallet/net/common.dart';
import 'package:selangkah_new/Wallet/net/dio_utils.dart';
import 'package:selangkah_new/Wallet/net/intercept.dart';
import 'package:selangkah_new/Wallet/Services/navigation_service.dart';

import 'package:sp_util/sp_util.dart';

import 'Screens/AppSplashScreen/app_splash.dart';
import 'package:selangkah_new/Wallet/Utils/log_utils.dart';

import 'package:http/http.dart' as http;

import 'Screens/Sehat/screens/module/module_pathway.dart';
import 'Screens/Sehat/screens/module/module_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  await FlutterDownloader.initialize(debug: true, ignoreSsl: true);
  // FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);

  FlutterDownloader.registerCallback(TestClass.callback);
  await Firebase.initializeApp();

  //local
  // SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then(
  //   (value) => runApp(
  //     EasyLocalization(
  //       supportedLocales: [Locale('ms', 'MY'), Locale('en', 'US')],
  //       path: 'assets/translations',
  //       fallbackLocale: Locale('en', 'US'),
  //       child: MyApp(),
  //     ),
  //   ),
  // );

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  //cloud
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then(
    (value) => runApp(
      EasyLocalization(
        supportedLocales: [Locale('en', 'US'), Locale('ms', 'MY')],
        path: 'https://app.selangkah.my/app_library/',
        assetLoader: HttpAssetLoader(),
        // assetLoader: FileAssetLoader(),
        fallbackLocale: Locale('en', 'US'),
        child: MyApp(),
      ),
    ),
  );

  // SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then(
  //   (value) => runApp(
  //     DevicePreview(
  //       enabled: true,
  //       builder: (context) => EasyLocalization(
  //         supportedLocales: [Locale('ms', 'MY'), Locale('en', 'US')],
  //         path: 'assets/translations',
  //         fallbackLocale: Locale('ms', 'MY'),
  //         child: MyApp(),
  //       ),
  //     ),
  //   ),
  // );
}

class TestClass {
  @pragma('vm:entry-point')
  static void callback(String id, int status, int progress) {}
}

class HttpAssetLoader extends AssetLoader {
  @override
  Future<Map<String, dynamic>?> load(String path, Locale locale) async {
    try {
      String fileName = '';
      print('language is ${locale.languageCode}');
      if (locale.languageCode == 'en') {
        fileName = 'en-US.json';
      } else {
        fileName = 'ms-MY.json';
      }
      return http
          .get(Uri.parse('$path$fileName'))
          .then((response) => json.decode(response.body.toString()));
    } catch (e) {
      //Catch network exceptions
      // return Future.value();
      print(e.toString());
      return null;
    }
  }
}

// final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

class MyApp extends StatefulWidget {
  MyApp() {
    initDio();
    Log.init();
  }

  @override
  State<MyApp> createState() => _MyAppState();

  Future<void> initDio() async {
    await SpUtil.getInstance();
    final List<Interceptor> interceptors = <Interceptor>[];
    Constant.inProduction = false;
    interceptors.add(AuthInterceptor());
    if (!Constant.inProduction) {
      interceptors.add(LoggingInterceptor());
    }
    configDio(
      interceptors: interceptors,
    );
  }
}

class _MyAppState extends State<MyApp> {
  // late final FirebaseMessaging _firebaseMessaging;
  final messagingService = MessagingService();

  @override
  void initState() {
    init();
    super.initState();
  }

  void init() async {
    await messagingService.init();
  }

  //real apps uncomment
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: NavigationService.navigatorKey,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      debugShowCheckedModeBanner: false,
      title: 'SELANGKAH APP',
      builder: (context, child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: 0.95),
          child: child!,
        );
      },
      // home: WalkthroughPage(),
      home: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => PrivacyTermCubit(),
          ),
          BlocProvider(
            create: (context) => UserProfileCubit(),
          ),
        ],
        child: AppSplashScreen(),
      ),
      theme: ThemeData(
        appBarTheme: AppBarTheme(backgroundColor: kPrimaryColor),
        primaryColor: kPrimaryColor,
        scaffoldBackgroundColor: Colors.white,
      ),
    );
  }
}

class MessagingService {
  final FirebaseMessaging _fcm = FirebaseMessaging.instance;

  Future<void> init() async {
    _fcm.requestPermission();

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print("onMessage: ${message.data.toString()}");

      redirectHandler(message);
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print("onMessageOpenedApp: ${message.data.toString()}");

      redirectHandler(message);
    });

    // FirebaseMessaging.onBackgroundMessage((message) async {});
  }
}

void redirectHandler(RemoteMessage remoteMessage) async {
  if (remoteMessage.data.toString() != '{}') {
    Map<String, dynamic> message = remoteMessage.data;

    String event = '';

    if (Platform.isIOS) {
      event = message["event"];
    } else {
      event = message["event"];
    }

    event = event.replaceAll('\'', '"');
    print('event on resume is $event');

    if (event == 'DASS' || event == 'dass') {
      //redirect dass
      print('redirect dass');

      await SehatRepositories.getDassFollowupId();

      Navigator.of(
              NavigationService.navigatorKey.currentState!.overlay!.context,
              rootNavigator: true)
          .push(
        MaterialPageRoute(
          builder: (context) => BlocProvider(
            create: (context) => GetContentCubit(),
            child: ContentScreen(
              title: 'DEPRESSION ANXIETY STRESS SCALE (DASS-21)',
              idCategory: '19',
              isQuestionnaire: true,
            ),
          ),
        ),
      );
    } else if (event == 'SEHAT') {
      //redirect sehat

      SehatRedirect sehatRedirect = sehatRedirectFromJson(event);

      Navigator.of(
              NavigationService.navigatorKey.currentState!.overlay!.context,
              rootNavigator: true)
          .push(
        MaterialPageRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => GetHomepageCubit(),
              ),
              BlocProvider(
                create: (context) => CommentCubit(),
              ),
            ],
            child: SehatHomepage(),
          ),
        ),
      );

      Navigator.of(
              NavigationService.navigatorKey.currentState!.overlay!.context)
          .push(
        MaterialPageRoute(
          builder: (context) => BlocProvider(
            create: (context) => GetModuleCubit(),
            child: ModuleScreen(),
          ),
        ),
      );

      Navigator.of(
              NavigationService.navigatorKey.currentState!.overlay!.context)
          .push(
        MaterialPageRoute(
          builder: (context) => BlocProvider(
            create: (context) => GetPathwayCubit(),
            child: ModulePathway(
              title: sehatRedirect.moduleName!,
              idModule: sehatRedirect.idModule!,
            ),
          ),
        ),
      );

      Navigator.of(
              NavigationService.navigatorKey.currentState!.overlay!.context)
          .push(
        MaterialPageRoute(
          builder: (context) => BlocProvider(
            create: (context) => GetContentCubit(),
            child: ContentScreen(
              title: sehatRedirect.activityName!,
              idCategory: sehatRedirect.idActivity!,
              isQuestionnaire: false,
            ),
          ),
        ),
      );
    } else if (event == 'HAYAT' || event == 'hayat') {
      //redirect hayat
      Navigator.of(
              NavigationService.navigatorKey.currentState!.overlay!.context,
              rootNavigator: true)
          .push(
        MaterialPageRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => SliderCardCubit(),
              ),
              BlocProvider(
                create: (context) => ProfilePageCubit(),
              ),
              BlocProvider(
                create: (context) => DropdownMeasurementCubit(),
              ),
              BlocProvider(
                create: (context) => HealthRecordCubit(),
              ),
              BlocProvider(
                create: (context) => BackgroundIllnessCubit(),
              ),
              BlocProvider(
                create: (context) => AppointmentCubit(),
              ),
              BlocProvider(
                create: (context) => TestResultCubit(),
              ),
              BlocProvider(
                create: (context) => MedicalCertificatesCubit(),
              ),
              BlocProvider(
                create: (context) => PrescriptionsCubit(),
              ),
              BlocProvider(
                create: (context) => VaccineCertificatesCubit(),
              ),
              BlocProvider(
                create: (context) => ArchivedRecordCubit(),
              ),
              BlocProvider(
                create: (context) => InvoiceCubit(),
              ),
              BlocProvider(
                create: (context) => ReferralLetterCubit(),
              ),
            ],
            child: ProfilePage(
              page: 'HAYAT',
            ),
          ),
        ),
      );
    } else {
      //redirect inbox

      Navigator.of(
              NavigationService.navigatorKey.currentState!.overlay!.context,
              rootNavigator: true)
          .push(
        MaterialPageRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => GetInboxCubit(),
              ),
              BlocProvider(
                create: (context) => DeleteInboxCubit(),
              ),
            ],
            child: InboxMain(),
          ),
        ),
      );
    }
  } else {
    Navigator.of(NavigationService.navigatorKey.currentState!.overlay!.context,
            rootNavigator: true)
        .push(
      MaterialPageRoute(
        builder: (context) => MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => GetInboxCubit(),
            ),
            BlocProvider(
              create: (context) => DeleteInboxCubit(),
            ),
          ],
          child: InboxMain(),
        ),
      ),
    );
  }
}

Future _firebaseMessagingBackgroundHandler(RemoteMessage message) async {}
