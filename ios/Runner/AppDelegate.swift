import UIKit
import Flutter
import GoogleMaps
import flutter_downloader

let platformChannel = "kiplePay.flutter.ekyc"
let platformChannelCommon = "kiplePay.flutter.platform"
let methodChannelekycInit = "initEkyc"
let methodChannelekycIdCard = "ekycScanId"
let methodChannelekycPassport = "ekycScanPassport"
let methodChannelekycFace = "ekycFaceRecord"
let methodChannelekycGetImage = "ekycGetScanImages"
let methodChannelekycAlert = "ekycAlert"
let methodChannelekycOverTime = "ekycOvertime"
let methodChannelekycTimeMax = "ekycTimeMax"
let methodChannelekycTryAgain = "ekycTryAgain"
let methodChannelekycGetFaceImage = "ekycGetFaceImages"
let methodChannelekycNeedHelp = "goToNeedHelp"

let notificationCenterAlertTimeOut = "notificationCenterAlertTimeOut"
let notificationCenterAlertLimitMax = "notificationCenterAlertLimitMax"
let notificationCenterAlertTryAgain = "notificationCenterAlertTryAgain"
let topUpUuid = "autoTopUpGetUuid";

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {

    
//    if #available(iOS 10.0, *) {
//    }
    UNUserNotificationCenter.current().delegate = self
    

    GMSServices.provideAPIKey("AIzaSyDYRS1DEQvOr74ScS7jBXo1mbdamzvx_8g")

    GeneratedPluginRegistrant.register(with: self)
    FlutterDownloaderPlugin.setPluginRegistrantCallback(registerPlugins)
    UIViewController.swizzlePresent()
    let controller: FlutterViewController = window.rootViewController as! FlutterViewController
    EkycTool.ekycScan.inputViewController = controller
    let scanChannel = FlutterMethodChannel.init(name: platformChannel, binaryMessenger: controller as! FlutterBinaryMessenger)
    scanChannel.setMethodCallHandler { (call:FlutterMethodCall, result: @escaping FlutterResult) in
        if call.method == methodChannelekycInit {
            let para = call.arguments as! [String : String]
            EkycTool.ekycScan.flutterResultInit = result
            EkycTool.ekycScan.initXenditySDK(language: para["language"] ?? "en")
        }else if call.method == methodChannelekycIdCard {
            EkycTool.ekycScan.flutterResultIc = result
            EkycTool.ekycScan.scanIdCardAndPassport(isPassport: false)
        }else if call.method == methodChannelekycPassport {
            EkycTool.ekycScan.flutterResultPassport = result
            EkycTool.ekycScan.scanIdCardAndPassport(isPassport: true)
        }else if call.method == methodChannelekycFace {
            EkycTool.ekycScan.flutterResultFace = result
            EkycTool.ekycScan.verifyFaceVideo()
        }else if call.method == methodChannelekycGetImage {
            EkycTool.ekycScan.flutterResultGetImage = result
            EkycTool.ekycScan.getFrontOrBackImage()
        }else if call.method == methodChannelekycAlert {
            EkycTool.ekycScan.flutterResultAlert = result
            let para = call.arguments as! [String : String]
            EkycTool.ekycScan.alertPop(type: para["type"]!)
        }else if call.method == methodChannelekycTryAgain {
            EkycTool.ekycScan.flutterResultTryAgain = result
            EkycTool.ekycScan.tryAgain()
        }else if call.method == methodChannelekycGetFaceImage {
            EkycTool.ekycScan.flutterResultTryFaceImage = result
            EkycTool.ekycScan.getFaceImage()
        }else if (call.method == methodChannelekycNeedHelp) {
            EkycTool.ekycScan.goToNeedHelp()
        }
    }

    let platformChannel = FlutterMethodChannel.init(name: platformChannelCommon, binaryMessenger: controller as! FlutterBinaryMessenger)
    platformChannel.setMethodCallHandler { (call: FlutterMethodCall, result: @escaping FlutterResult) in
        if (call.method == topUpUuid) {
            result(UUID().uuidString)
        }
    }
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}

private func registerPlugins(registry: FlutterPluginRegistry) {
    if (!registry.hasPlugin("FlutterDownloaderPlugin")) {
       FlutterDownloaderPlugin.register(with: registry.registrar(forPlugin: "FlutterDownloaderPlugin")!)
    }
}
