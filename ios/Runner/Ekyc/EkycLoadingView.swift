//
//  EkycLoadingView.swift
//  Runner
//
//  Created by marvin on 2021/3/15.
//

import UIKit

class EkycLoadingView: UIView {

    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: ConstantFile.screenWidth, height: ConstantFile.screenHeight))
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        configAnimation()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configAnimation() {
        
//        let indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
//        indicator.startAnimating()
//        indicator.center = CGPoint(x: ConstantFile.screenWidth * 0.5, y: ConstantFile.screenHeight * 0.5)
//        self.addSubview(indicator)
//        indicator.color = UIColor.blue
        
        let images:[UIImage] = [
            UIImage(named: "loading_1")!,
            UIImage(named: "loading_2")!,
            UIImage(named: "loading_3")!,
            UIImage(named: "loading_4")!
        ]

        let animationImageView = UIImageView()
        self.addSubview(animationImageView)
        animationImageView.animationImages = images
        animationImageView.animationDuration = 1
        animationImageView.animationRepeatCount = 0
        animationImageView.startAnimating()
        animationImageView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalTo(self.snp.centerY).offset(-30)
            make.width.height.equalTo(45)
        }

        let loadingLabel = UILabel()
        self.addSubview(loadingLabel)
        loadingLabel.text = "Please wait while system is retrieving your details"
        loadingLabel.textAlignment = .center
        loadingLabel.font = UIFont.systemFont(ofSize: 15)
        loadingLabel.textColor = UIColor(hex: "#29374B")
        loadingLabel.numberOfLines = 0
        loadingLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(animationImageView.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(FitWidth(width: 50))
            make.right.equalToSuperview().offset(FitWidth(width: -50))
        }
    }
    
}
