//
//  CameraViewController.swift
//  Runner
//
//  Created by marvin on 2020/12/21.
//

import UIKit
import XenchainSDK

class CameraViewController: XScannerViewController {
    override func viewDidLoad() {
      super.viewDidLoad()
    }

    // This function is called when the ID Scanner is about to Start Scan Hologram.
    override func startScanHologram() {
      print("Start Scan Hologram")
    }

    // This function is used to check the progress of Hologram Scanning.
    override func setHologramProgress(_ progress: Double) {
      print("Hologram Progress")
    }

    // This function will be called when the Scanner has successfully pass the hologram threshold of the IC Check.
    override func passHologramICCheck() {
      print("Pass Hologram IC Check")
    }

    // This function will be called when the Scanner has successfully pass the hologram threshold of the Name Check.
    override func passHologramNameCheck() {
      print("Pass Hologram Name Check")
    }

    // This function will be called when the Scanner has successfully pass the hologram threshold of the Ghost Check.
    override func passHologramGhostCheck() {
      print("Has Scan Hologram")
    }

    // This function is called to represents that the Hologram Scanning has been completed or bypassed.
    override func hasScanHologram() {
      print("Has Scan Hologram")
    }

    // This function is called when the ID Scanner is about to Check Font Size.
    override func startScanFontSize() {
      print("Start Scan Font Size")
    }

    // This function is called represents that the Font Size Checking has been completed.
    override func hasScanFontSize() {
      print("Has Scan Font Size")
    }

    // This function is called when the ID Scanner is about to Scan the Front Side of the ID Document.
    override func startScanFront() {
      print("Start Scan Front")
    }

    // This function is called represents that the Front ID Document has been scanned.
    override func hasScanFront(_ frontImage: UIImage) {
      print("Has Scan Front")
    }

    // This function is called when the ID Scanner is about to Scan the Back Side of the ID Document.
    override func startScanBack() {
      print("Start Scan Back")
    }

    // This function is called represents that the Back ID Document has been scanned.
    override func hasScanBack(_ backImage: UIImage) {
      print("Has Scan Back")
    }
}
