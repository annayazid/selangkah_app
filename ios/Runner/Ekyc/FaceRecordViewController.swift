//
//  FaceRecordViewController.swift
//  Runner
//
//  Created by marvin on 2020/11/27.
//

import UIKit
import XenchainSDK
import SnapKit

class FaceRecordViewController: XenchainFaceRecordViewController {
    
    var recordingButton: UIButton!
    var timer: Timer?
    var totalTimeLength: Int = 60
    var secondTime: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(onNotifitionClickTimeOut), name: Notification.Name(notificationCenterAlertTimeOut), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onNotifitionClickMax), name: Notification.Name(notificationCenterAlertLimitMax), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onNotifitionTryagain), name: Notification.Name(notificationCenterAlertTryAgain), object: nil)
    }
    
    @objc func onNotifitionClickTimeOut() {
        ConstantFile.alertTryAgainView(callBack: { [self]tag in
            if (tag == 1) { // can
                self.dismiss(animated: true) {}
            }else if (tag == 0) { // try again
                var backData = [String : Any]()
                backData["status"] = "try_again"
                EkycTool.ekycScan.flutterResultAlert(backData)
                self.dismiss(animated: true) {}
            }
        })
    }
    
    @objc func onNotifitionClickMax() {
        ConstantFile.alertAttemptView(callBack: { [self]tag in
            if (tag == 0) {
                self.dismiss(animated: true) {}
            }
        })
    }
    
    @objc func onNotifitionTryagain() {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cover: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: ConstantFile.screenWidth, height: ConstantFile.screenHeight))
        cover.image = UIImage(named: "cover.png")
        self.view.addSubview(cover)
        configUi()
    }
    
    override func startRecording() {
        
    }
    
    override func processingRecording() {
        
    }
    
    override func completingProcess() {
        
    }
    
//    override func processImageFailure(_ errorMessage: String?) {
//
//    }
    
    func configUi() {
        let backButton: UIButton = UIButton(frame: CGRect(x: 15, y: ConstantFile.statusHeight + 15, width: 30, height: 30))
        backButton.setImage(UIImage(named: "back_icon"), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        self.view.addSubview(backButton)
        
        let titleView: UIView = UIView()
        titleView.backgroundColor = UIColor.init(hex: "#ce0e2d")
        titleView.layer.cornerRadius = 13
        self.view.addSubview(titleView)
        titleView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.height.equalTo(26)
            make.centerY.equalTo(backButton.snp.bottom).offset(FitHeight(height: 30))
        }
        
        let titleLabel = UILabel()
        titleView.addSubview(titleLabel)
        titleLabel.text = LocalizationTool.shareInstance.valueWithKey(key: "face_desc")
        titleLabel.textColor = UIColor.init(hex: "#FFFFFF")
        titleLabel.font = UIFont.systemFont(ofSize: 12)
        titleLabel.textAlignment = .center
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(26)
        }
        
        recordingButton = UIButton()
        recordingButton.layer.cornerRadius = 15
        recordingButton.backgroundColor = UIColor.init(hex: "#ce0e2d")
        recordingButton.setTitle(LocalizationTool.shareInstance.valueWithKey(key: "face_start_recording"), for: .normal)
        recordingButton.setTitleColor(UIColor.white, for: .normal)
        recordingButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        recordingButton.addTarget(self, action: #selector(startVideoRecordAction), for: .touchUpInside)
        self.view.addSubview(recordingButton)
        recordingButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-20)
            make.height.equalTo(FitHeight(height: 50.0))
        }
        
        secondTime = UILabel()
        secondTime.text = "\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_start"))60\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_end"))"
        secondTime.textColor = UIColor.init(hex: "#B4B4B4")
        secondTime.font = UIFont.systemFont(ofSize: 12)
        self.view.addSubview(secondTime)
        secondTime.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(recordingButton.snp.top).offset(-FitHeight(height: 25.0))
        }
        
        let faceDes = UILabel()
        faceDes.text = LocalizationTool.shareInstance.valueWithKey(key: "face_clean_desc")
        faceDes.textColor = UIColor.init(hex: "#FFFFFF")
        faceDes.font = UIFont.systemFont(ofSize: 12)
        self.view.addSubview(faceDes)
        faceDes.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(secondTime.snp.top).offset(-FitHeight(height: 20.0))
        }
        
        let faceTitle = UILabel()
        faceTitle.text = LocalizationTool.shareInstance.valueWithKey(key: "face_capture")
        faceTitle.textColor = UIColor.init(hex: "#FFFFFF")
        faceTitle.font = UIFont.boldSystemFont(ofSize: 14)
        self.view.addSubview(faceTitle)
        faceTitle.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(faceDes.snp.top).offset(-FitHeight(height: 6.0))
        }
    }
    
    @objc func startVideoRecordAction() {
        self.startVideoRecord()
        recordingButton.isHidden = true
    }
    
    @objc func backAction() {
        self.dismiss(animated: true) {
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func initTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc func timerAction() {
        totalTimeLength -= 1
        
        if (totalTimeLength == 0) {
            timeCancel()
        }else{
            DispatchQueue.main.async { [self] in
                secondTime.text = "\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_start"))\(totalTimeLength)\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_end"))"
            }
        }
    }
    
    func timeCancel() {
        if timer != nil {
            timer?.invalidate()
        }
    }

}
