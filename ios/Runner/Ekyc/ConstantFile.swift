//
//  ConstantFile.swift
//  Runner
//
//  Created by marvin on 2020/11/30.
//

import UIKit

func FitHeight(height: Double) -> Double{
    return Double(ConstantFile.screenHeight) * height / 592.0;
}

func FitWidth(width: Double) -> Double{
    return Double(ConstantFile.screenWidth) * width / 360.0;
}

class ConstantFile {
    static let screenWidth = UIScreen.main.bounds.size.width
    static let screenHeight = UIScreen.main.bounds.size.height
    static let statusHeight = UIApplication.shared.statusBarFrame.height
    
    static func alertTryAgainView(callBack: @escaping CallBackBlock) {
        let aletView = MyAlertView(frame: CGRect(x: 0, y: 0, width: ConstantFile.screenWidth, height: ConstantFile.screenHeight), message: "Oops. You have exceeded the time limit. Please do your verification again", buttons: ["Try again", "Cancel"]) { (tag) in
            callBack(tag)
        }
        aletView.show()
    }
    
    static func alertCountinueView(callBack: @escaping CallBackBlock) {
        let aletView = MyAlertView(frame: CGRect(x: 0, y: 0, width: ConstantFile.screenWidth, height: ConstantFile.screenHeight), message: "Timeout expired. Do you want to continue?", buttons: ["Countinue"]) { (tag) in
            callBack(tag)
        }
        aletView.show()
    }
    
    static func alertAttemptView(callBack: @escaping CallBackBlock) {
        let aletView = MyAlertView(frame: CGRect(x: 0, y: 0, width: ConstantFile.screenWidth, height: ConstantFile.screenHeight), message: "You have reached the max. attempts. Please try again in xx hour.", buttons: ["OK"]) { (tag) in
            callBack(tag)
        }
        aletView.show()
    }
    
    static func saveImage(image: UIImage, path: String) -> String {
        let document_path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        if document_path.count > 0 {
            let save_image_path = document_path[0] + "/\(path).jpg"
            let save_image_path_url = URL(fileURLWithPath: save_image_path)
            
            do {
                try image.jpegData(compressionQuality: 1)?.write(to: save_image_path_url)
                return save_image_path
            } catch {
                return ""
            }
        }
        return ""
    }
}

extension UIColor {

    public convenience init(hex: String) {

        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        var hex:   String = hex

        if hex.hasPrefix("#") {
            let index = hex.index(hex.startIndex, offsetBy: 1)
            hex = String(hex[index...])
        }

        let scanner = Scanner(string: hex)
        var hexValue: CUnsignedLongLong = 0
        if scanner.scanHexInt64(&hexValue) {
            switch (hex.count) {
            case 3:
                red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
                green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
                blue  = CGFloat(hexValue & 0x00F)              / 15.0
            case 4:
                red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
                green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
                blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
                alpha = CGFloat(hexValue & 0x000F)             / 15.0
            case 6:
                red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
                green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
                blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
            case 8:
                red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
            default:
                print("Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8", terminator: "")
            }
        } else {
//            print("Scan hex error")
        }
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }

}
