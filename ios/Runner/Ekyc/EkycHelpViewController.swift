//
//  EkycHelpViewController.swift
//  Runner
//
//  Created by marvin on 2021/3/10.
//

import UIKit
import AVKit

class EkycHelpViewController: UIViewController {
    
    var cameraVideoPlayer: AVPlayer!
    var cameraVideoPlayerLayer: AVPlayerLayer!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        initConfigUIView()
    }
    
    func initConfigUIView() {
        
        let topView: UIView = UIView()
        self.view.addSubview(topView)
        topView.backgroundColor = .white
        topView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(44 + ConstantFile.statusHeight)
        }
        
        let backButton: UIButton = UIButton()
        backButton.setImage(UIImage(named: "back_black"), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        topView.addSubview(backButton)
        backButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(ConstantFile.statusHeight+7)
            make.width.equalTo(30)
            make.height.equalTo(30)
        }
        
        let titlePage = UILabel()
        topView.addSubview(titlePage)
        titlePage.text = "How To Scan ID Card"
        titlePage.font = UIFont.systemFont(ofSize: 18)
        titlePage.textColor = UIColor(hex: "#0D79F2")
        titlePage.snp.makeConstraints { (make) in
            make.centerY.equalTo(backButton.snp.centerY)
            make.centerX.equalTo(topView.snp.centerX)
        }
        
        let contentView = UIView()
        self.view.addSubview(contentView)
        contentView.backgroundColor = .white
        contentView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.top.equalTo(topView.snp.bottom)
            make.left.equalToSuperview().offset(15)
            make.right.equalTo(-15)
        }
        
        let idVerification = UILabel()
        contentView.addSubview(idVerification)
        idVerification.text = "ID Verification"
        idVerification.textColor = .black
        idVerification.font = UIFont.boldSystemFont(ofSize: 16)
        idVerification.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalToSuperview().offset(20)
        }
        
        let videoView = UIView()
        contentView.addSubview(videoView)
//        videoView.backgroundColor = .gray
        videoView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(idVerification.snp.bottom).offset(15)
            make.height.equalTo(FitHeight(height: 140.0))
        }
        
        let path = Bundle.main.path(forResource: "Hologram_tutorial", ofType: ".mp4")
        let videoUrl = NSURL.fileURL(withPath: path!)
        
        let playerItem: AVPlayerItem = AVPlayerItem(url: videoUrl)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidPlayToEndTime), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        
        let videoWidth: Double = 320.0 * FitHeight(height: 140) / 210.0
        cameraVideoPlayer = AVPlayer(playerItem: playerItem)
        if (cameraVideoPlayerLayer != nil) {
            cameraVideoPlayerLayer.removeFromSuperlayer()
        }
        cameraVideoPlayerLayer = AVPlayerLayer(player: cameraVideoPlayer)
        videoView.layer.addSublayer(cameraVideoPlayerLayer)
        cameraVideoPlayerLayer.frame = CGRect(x: (Double(ConstantFile.screenWidth) - videoWidth) * 0.5, y: 0, width: videoWidth, height: FitHeight(height: 140.0))
        cameraVideoPlayer.play()
        
        let hologram = UILabel()
        contentView.addSubview(hologram)
        hologram.text = "Hologram checking:"
        hologram.textColor = UIColor(hex: "#343434")
        hologram.font = UIFont.boldSystemFont(ofSize: 14)
        hologram.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalTo(videoView.snp.bottom).offset(18)
        }
        
        let hologramContent = UILabel()
        hologramContent.numberOfLines = 0
        contentView.addSubview(hologramContent)
        hologramContent.text = "1. Tilt your ID card right and left within the frame.\n2. Camera will auto-scan the hologram on your ID card.\n3. Do not remove your ID card from frame until scanning is 100% complete."
        hologramContent.textColor = UIColor(hex: "#343434")
        hologramContent.font = UIFont.systemFont(ofSize: 14)
        hologramContent.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(hologram.snp.bottom).offset(5)
        }
        
        let scanning = UILabel()
        contentView.addSubview(scanning)
        scanning.text = "ID scanning:"
        scanning.textColor = UIColor(hex: "#343434")
        scanning.font = UIFont.boldSystemFont(ofSize: 14)
        scanning.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalTo(hologramContent.snp.bottom).offset(15)
        }
        
        let scanningContent = UILabel()
        contentView.addSubview(scanningContent)
        scanningContent.text = "1. Place front page of your ID card within the frame.\n2. Camera will auto-scan your ID card.\n3. Do not remove your ID card from frame until scanning is 100% complete.\n4. Turn your ID card to scan the back page and wait till scanning is 100% complete."
        scanningContent.textColor = UIColor(hex: "#343434")
        scanningContent.numberOfLines = 0
        scanningContent.font = UIFont.systemFont(ofSize: 14)
        scanningContent.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(scanning.snp.bottom).offset(5)
        }
    }
    
    @objc func backAction() {
        self.dismiss(animated: true) {
        }
    }
    
    @objc func playerItemDidPlayToEndTime() {
        if (cameraVideoPlayer != nil) {
            cameraVideoPlayer.seek(to: CMTime.zero)
            cameraVideoPlayer.play()
        }
    }
    
}
