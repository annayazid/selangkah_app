//
//  EkycTool.swift
//  Runner
//
//  Created by marvin on 2020/11/27.
//

import UIKit
import XenchainSDK
import Flutter
import Microblink

// staging （sel.selangkah.app.stag）
// let appKey = "B7yRrL8yRPouTGr497Z5gyZKMMSJDe-x"
// let appUrl = "https://uat-kiple-ekyca.xendity.com"

// production （sel.selangkah.app）
let appKey = "fWQP6BAGwWJkWdDI8FjI6kuDjNGDaj8p"
let appUrl = "https://kiple-ekyca.xendity.com"

class EkycTool: NSObject {
    
    static let ekycScan = EkycTool()
    
    var inputViewController: FlutterViewController!
    var canScan: Bool = false
    var mOCRPreview: Bool = true
    var mCardResult: XCardResult!
    var mReferenceID: String = ""
    var mOnBoardingID: String = ""
    var mCardMetaResults: [AnyHashable : Any]?
    var mFaceImageRef: String = ""
    var frontImagePath: String?
    var backImagePath: String?
    var passport: Bool?
//    var getImage: Bool = false
    var faceImagePath: String?
    
    var flutterResultInit: FlutterResult!
    var flutterResultIc: FlutterResult!
    var flutterResultPassport: FlutterResult!
    var flutterResultFace: FlutterResult!
    var flutterResultGetImage: FlutterResult!
    var flutterResultAlert: FlutterResult!
    var flutterResultTryAgain: FlutterResult!
    var flutterResultTryFaceImage: FlutterResult!
    
    
    func initXenditySDK(language: String) {
        var nativeLanguage: String = "en"
        if language == "en" {
            nativeLanguage = "en"
        }else if language == "ms" {
            nativeLanguage = "ms-MY"
        }
        LocalizationTool.shareInstance.setLanguage(langeuage: nativeLanguage)
        
        
        XenchainSDK.initSDK(appKey, apiURL: appUrl, onPremSaveData: true, completionHandler: self)
        XenchainSDK.setFullScreeenDeployment(true)
        let loading = EkycLoadingView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        XenchainSDK.setLoading(loading)
    }
    
    func goToNeedHelp() {
        inputViewController.present(EkycHelpViewController(), animated: true) {
            
        }
    }
    
    func getFrontOrBackImage() {
        var paths = [String]()
        if passport == true {
            if let path = frontImagePath {
                paths.append(path)
                var backData = [String : Any]()
                backData["status"] = "image_list"
                backData["data"] = paths
                backData["success"] = true
                backData["message"] = ""
                if (flutterResultGetImage != nil) {
                    flutterResultGetImage(backData)
                }
            }
        }else{
            if frontImagePath == nil || backImagePath == nil {
                return
            }else {
                paths.append(frontImagePath!)
                paths.append(backImagePath!)
                var backData = [String : Any]()
                backData["status"] = "image_list"
                backData["data"] = paths
                backData["success"] = true
                backData["message"] = ""
                if (flutterResultGetImage != nil) {
                    flutterResultGetImage(backData)
                }
            }
            
        }
    }
    
    func alertPop(type: String) {
        if (type == methodChannelekycOverTime) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationCenterAlertTimeOut), object: nil)
        }else if (type == methodChannelekycTimeMax) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationCenterAlertLimitMax), object: nil)
        }
    }
    
    func tryAgain() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationCenterAlertTryAgain), object: nil)
    }
    
    func getFaceImage() {
        var paths = [String]()
        if let path = faceImagePath {
            paths.append(path)
            var backData = [String : Any]()
            backData["status"] = "image_face"
            backData["data"] = paths
            backData["success"] = true
            backData["message"] = ""
            if (flutterResultTryFaceImage != nil) {
                flutterResultTryFaceImage(backData)
            }
        }
    }
    
    func scanIdCardAndPassport(isPassport: Bool) {
        passport = isPassport
        if canScan {
//            if mReferenceID.count > 0 {
//                XenchainSDK.setReferenceID(mReferenceID)
//            }
//
//            if mCardResult != nil {
//                XenchainSDK.completeScanDeployment(mOnBoardingID, metaCardResult: mCardMetaResults as! [AnyHashable : Any], cardResult: mCardResult, scanConfig: myKadConfig, completionHandler: self)
//                return
//            }
            
            let scan = ScanIdCardViewController()
            scan.scanType = isPassport ? 1 : 0
            XenchainSDK.deployScanner(getConfig(), inputController: inputViewController, extendedController: scan, completionHandler: self)
        }
    }
    
    func verifyFaceVideo() {
        if canScan {
            let face = FaceRecordViewController()
            face.modalPresentationStyle = .fullScreen
            XenchainSDK.deployFaceRecord(mOnBoardingID, inputController: inputViewController, extendedController: face, completionHandler: self)
        }
    }
    
}

extension EkycTool: XenchainSDKCallback, XenchainScannerCallback, XenchainFaceCallback{
    
    // XenchainScannerCallback
    func scanResults(_ cardResult: XCardResult!, referenceID: String!, onBoardingID: String!, errorMessage: String!) {
        var backData = [String: Any]()
        
        if (errorMessage.count > 0) {
            backData["status"] = passport! ? "passport" : "ic"
            backData["data"] = nil
            backData["success"] = false
            backData["message"] = errorMessage
            passport! ? flutterResultPassport(backData) : flutterResultIc(backData)
            return
        }else{
            mCardResult = cardResult;
            mReferenceID = referenceID;
            mOnBoardingID = onBoardingID;
            var callBack: [AnyHashable : Any] = mCardResult.toDictionary()
            callBack["mReferenceID"] = mReferenceID
            callBack["mOnBoardingID"] = mOnBoardingID
            
            backData["status"] = passport! ? "passport" : "ic"
            backData["data"] = callBack
            backData["success"] = true
            backData["message"] = ""
            passport! ? flutterResultPassport(backData) : flutterResultIc(backData)
            XenchainSDK.setReferenceID(referenceID)
            // after scan, need use function 'completeScanDeployment'
            
            XenchainSDK.completeScanDeployment(mOnBoardingID, metaCardResult: mCardMetaResults!, cardResult: mCardResult, scanConfig: getConfig(), completionHandler: self)
        }
    }
    
    func hologramScanResults(_ cardResult: XCardResult?, referenceID: String, onBoardingID: String, errorMessage: String) {
        mReferenceID = referenceID;
        mOnBoardingID = onBoardingID;
        XenchainSDK.setReferenceID(referenceID)
        let scan = ScanIdCardViewController()
        scan.scanType = passport! ? 1 : 0
        
        XenchainSDK.deployScanner(getConfig(), inputController: inputViewController, extendedController: scan, completionHandler: self)
    }
    
    func scanImageResults(_ imageResults: [AnyHashable : Any]!) {
        mCardMetaResults = imageResults
        
        if (imageResults["idFrontImage"] != nil) {
            let frontImage = (imageResults["idFrontImage"] as! UIImage)
            frontImagePath = ConstantFile.saveImage(image: frontImage, path: "frontImage")
            if (flutterResultGetImage != nil) {
                getFrontOrBackImage()
            }
        }
        
        if (imageResults["idBackImage"] != nil) {
            let backImage = (imageResults["idBackImage"] as! UIImage)
            backImagePath = ConstantFile.saveImage(image: backImage, path: "backImage")
            if (flutterResultGetImage != nil) {
                getFrontOrBackImage()
            }
        }
    }
    
    func scanMetaResults(_ metaRefResults: [AnyHashable : Any]!) {
        mCardMetaResults = metaRefResults
    }
    
    func scanMetaResults(_ idFaceImage: UIImage!, refFace: String!, idFrontImage: UIImage!, refFrontID: String!, idBack idBackImage: UIImage!, refBackID: String!, errorMessage: String!) {
        if mCardMetaResults == nil {
            mCardMetaResults = [AnyHashable : Any]()
        }
        mCardMetaResults!["idFrontImage"] = idFrontImage
        mCardMetaResults!["refFrontID"] = refFrontID
        mCardMetaResults!["idBackImage"] = idBackImage
        mCardMetaResults!["refBackID"] = refBackID
        mCardMetaResults!["idFaceImage"] = idFaceImage
        mCardMetaResults!["refFace"] = refFace
        
        mFaceImageRef = refFace
    }
    
    func scanLandmarkInformation(_ landmarkScores: [AnyHashable : Any]!) {
    }
    
    func scanCompleteDeployment(_ status: Bool, errorMessage: String!) {
        if (status) {
            mCardResult = nil;
            mCardMetaResults = nil;
        }
    }
    
    func scanSimilarityResults(_ nameIsSimilar: Bool, documentNoIsSimilar: Bool, frontBackIsSimilar: Bool) {
        
    }
    
    func completePostDeploymentResults(_ nameIsSimilarScore: Double, nameIsSimilar: Bool, documentNoIsSimilarScore: Double, documentNoIsSimilar: Bool, frontBackIsSimilarScore: Double, frontBackIsSimilar: Bool, errorMessage: String) {
        
    }
    
    // XenchainFaceCallback
    func faceMatchResult(_ isMatched: Bool, percentMatched: Double, error: String!) {
        
        var backData = [String: Any]()
        backData["status"] = "face_record"
        if (isMatched) {
            backData["success"] = true
            backData["data"] = ["percentMatched" : percentMatched, "isMatched": isMatched]
            backData["message"] = ""
        }else{
            backData["success"] = false
            backData["data"] = ["percentMatched" : percentMatched, "isMatched": isMatched]
            backData["message"] = error
        }
        flutterResultFace(backData)
    }
    
    func faceMatchMetaResult(_ outputImage: UIImage!, outputRef: String!) {
        if (outputImage != nil) {
            faceImagePath = ConstantFile.saveImage(image: outputImage, path: "faceImage")
            getFaceImage()
        }
    }
    
    // XenchainSDKCallback
    func initSDKStatus(_ status: Bool, message: String!) {
        var backData = [String : Any]()
        if status {
            canScan = true
            XenchainSDK.setPreviewOCR(mOCRPreview)
            backData["status"] = "init_sdk"
            backData["data"] = true
            backData["success"] = true
            backData["message"] = ""
        }else{
            backData["status"] = "init_sdk"
            backData["data"] = false
            backData["success"] = false
            backData["message"] = message
        }
        flutterResultInit(backData)
    }
    
    func getConfig() -> CardConfig {
        var config:CardConfig? = nil
        
        if (passport!) {
            let passportConfig = PassportConfig.init()
            config = passportConfig
        }else{
            let myKadConfig = MyKadConfig.init()
            myKadConfig.scanDefaultConfig = true
            myKadConfig.checkLandmark = true
            myKadConfig.checkHologram = true
            myKadConfig.checkHoloSwap = false
            myKadConfig.checkHoloOCRSwap = false
            myKadConfig.checkFontSize = true
            myKadConfig.scanFrontBack = true
            myKadConfig.scanCitizen = true
            myKadConfig.scanReligion = true
            myKadConfig.scanGender = true
            myKadConfig.scanOldICNumber = true
            myKadConfig.isRejectOldIC = false
            myKadConfig.stableFrame = 1
            myKadConfig.checkHoloMaterial = false
            config = myKadConfig
        }
        return config!
    }
    
}
