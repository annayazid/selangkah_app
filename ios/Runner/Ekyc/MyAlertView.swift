//
//  MyAlertView.swift
//  Runner
//
//  Created by marvin on 2020/12/8.
//

import UIKit
import Spring

typealias CallBackBlock = (Int) -> Void

class MyAlertView: UIView {

    let bgView = SpringView()
    let contentLabel = UILabel() //显示内容
    var message = ""
    var buttons:Array<String> = []
    var rightButton = UIButton()
    var callBack:CallBackBlock!
    
    init(frame: CGRect, message: String, buttons:Array<String>, callBack:@escaping CallBackBlock) {
        super.init(frame: frame)
        self.message = message
        self.callBack = callBack
        self.buttons = buttons
        configUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configUI() {
        self.backgroundColor = UIColor.darkGray.withAlphaComponent(0.5)
        self.addSubview(self.bgView)
        bgView.backgroundColor = UIColor.white//背景色
        bgView.layer.cornerRadius = 9
        bgView.clipsToBounds = true

        bgView.animation = "pop"
        bgView.curve = "Spring"
        bgView.duration = 1.0
        bgView.damping = 0.7
        bgView.velocity = 0.7
        bgView.force = 0.3
        bgView.animate()
        
        bgView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(30)
            make.right.equalToSuperview().offset(-30)
            make.center.equalToSuperview()
        }
        
        contentLabel.numberOfLines = 0
        contentLabel.textColor = UIColor.black
        contentLabel.text = self.message
        contentLabel.font = UIFont.systemFont(ofSize: 16)
        bgView.addSubview(contentLabel)
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(30)
            make.right.equalToSuperview().offset(-30)
            make.top.equalToSuperview().offset(30)
        }
        
        bgView.addSubview(rightButton)
        rightButton.setTitle(buttons[0], for: .normal)
        rightButton.setTitleColor(UIColor.init(hex: "#0D79F2"), for: .normal)
        rightButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        rightButton.tag = 0
        rightButton.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
        rightButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(50)
            make.width.equalTo(80)
            make.top.equalTo(contentLabel.snp.bottom).offset(30)
            make.bottom.equalToSuperview().offset(-20)
        }
        
        if (buttons.count > 1) {
            let leftButton = UIButton()
            bgView.addSubview(leftButton)
            leftButton.setTitle(buttons[1], for: .normal)
            leftButton.setTitleColor(UIColor.init(hex: "#0D79F2"), for: .normal)
            leftButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            leftButton.tag = 1
            leftButton.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
            leftButton.snp.makeConstraints { (make) in
                make.right.equalTo(rightButton.snp.left).offset(-10)
                make.height.equalTo(50)
                make.width.equalTo(80)
                make.top.equalTo(contentLabel.snp.bottom).offset(30)
            }
        }
        
    }
    
    @objc func clickButton(button: UIButton) {
        dismiss()
        callBack(button.tag)
    }
    
    //MARK:消失
    @objc func dismiss() {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.bgView.alpha = 0
            self.alpha = 0
        }, completion: { (finish) -> Void in
            if finish {
                self.removeFromSuperview()
            }
        })
    }
    /** 指定视图实现方法 */
    func show() {
        let wind = UIApplication.shared.keyWindow
        self.alpha = 0

        wind?.addSubview(self)
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.alpha = 1
        })
    }

}
