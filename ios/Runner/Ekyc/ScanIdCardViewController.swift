//
//  ScanIdCardViewController.swift
//  Runner
//
//  Created by marvin on 2020/11/30.
//

import UIKit
import XenchainSDK
import SnapKit
import Microblink

class ScanIdCardViewController: XScannerViewController {
    var scanType: Int! // type: 0. scan id card     1. scan passport
    var topCoverView: UIView!
    var scanSuper: UIView!
    var bottomCoverView: UIView!
    var firstBootomVersin: UIView!
    var secondBootomVersin: UIView!
    var frontImage: UIImageView!
    var backImage: UIImageView!
    var progressSuper: UIView!
    var progressView: UIProgressView!
    var progressValue: UILabel!
    var timer: Timer?
    var totalTimeLength: Int = 120
    var firstTimeLabel: UILabel!
    var secondTimeLabel: UILabel!
    var scanTitle: UILabel!
    var titleLabel: UILabel!
    
    // video
    var cameraVideoPlayer: AVPlayer!
    var cameraVideoPlayerLayer: AVPlayerLayer!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(onNotifitionClickTimeOut), name: Notification.Name(notificationCenterAlertTimeOut), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onNotifitionClickMax), name: Notification.Name(notificationCenterAlertLimitMax), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onNotifitionTryagain), name: Notification.Name(notificationCenterAlertTryAgain), object: nil)
    }
    
    @objc func onNotifitionClickTimeOut() {
        ConstantFile.alertTryAgainView(callBack: { [self]tag in
            if (tag == 1) { // cancel
                timeCancel()
                self.dismiss(animated: true) {}
            }else if (tag == 0) { // try again
                var backData = [String : Any]()
                backData["status"] = "try_again"
                EkycTool.ekycScan.flutterResultAlert(backData)
                self.dismiss(animated: true) {}
            }
        })
    }
    
    @objc func onNotifitionClickMax() {
        ConstantFile.alertAttemptView(callBack: { [self]tag in
            if (tag == 0) {
                timeCancel()
                self.dismiss(animated: true) {}
            }
        })
    }
    
    @objc func onNotifitionTryagain() {
        initTimer()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let loading = EkycLoadingView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
//        self.view.addSubview(loading)
        
        if (scanType == 0) {
            setUpCardInitView()
        }else {
            setUpPassportInitView()
        }
        initTimer()
    }
    
    func setUpCardInitView() {
        topCoverView = UIView()
        topCoverView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.view.addSubview(topCoverView)
        topCoverView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(scanType == 1 ? FitHeight(height: 150.0) : FitHeight(height: 115.0))
        }
        
        let backButton: UIButton = UIButton()
        backButton.setImage(UIImage(named: "back_icon"), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        topCoverView.addSubview(backButton)
        backButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(ConstantFile.statusHeight)
            make.width.equalTo(30)
            make.height.equalTo(30)
        }
        
//        let helpView: UIView = UIView()
//        topCoverView.addSubview(helpView)
//        helpView.snp.makeConstraints { (make) in
//            make.height.equalTo(30)
//            make.width.equalTo(90)
//            make.right.equalToSuperview().offset(-15)
//            make.top.equalToSuperview().offset(ConstantFile.statusHeight)
//        }
//
//        let question: UIImageView = UIImageView()
//        question.image = UIImage(named: "question")
//        helpView.addSubview(question)
//        question.snp.makeConstraints { (make) in
//            make.right.equalToSuperview()
//            make.centerY.equalToSuperview()
//            make.width.height.equalTo(18)
//        }
//
//        let helpLabel: UILabel = UILabel()
//        helpView.addSubview(helpLabel)
//        helpLabel.text = "Need Help"
//        helpLabel.textColor = .white
//        helpLabel.font = UIFont.systemFont(ofSize: 14)
//        helpLabel.snp.makeConstraints { (make) in
//            make.right.equalTo(question.snp.left).offset(-5)
//            make.centerY.equalToSuperview()
//        }
//
//        let whiteView = UIView()
//        helpView.addSubview(whiteView)
//        whiteView.backgroundColor = .white
//        whiteView.snp.makeConstraints { (make) in
//            make.top.equalTo(helpLabel.snp.bottom)
//            make.height.equalTo(1)
//            make.left.equalTo(helpLabel.snp.left)
//            make.right.equalTo(helpLabel.snp.right)
//        }
//
//        let tapButtopn = UIButton()
//        tapButtopn.addTarget(self, action: #selector(tapHelpAction), for: .touchUpInside)
//        helpView.addSubview(tapButtopn)
//        tapButtopn.snp.makeConstraints { (make) in
//            make.left.top.right.left.equalToSuperview()
//        }
        
        // scan view
        buildScanView()
        
        // bottom view
        bottomCoverView = UIView()
        bottomCoverView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.view.addSubview(bottomCoverView)
        bottomCoverView.snp.makeConstraints { (make) in
            make.top.equalTo(scanSuper.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        buildBottomCover()
        buildSecondView()
        firstBootomVersin.isHidden = false
        secondBootomVersin.isHidden = true
    }
    
    @objc func tapHelpAction() {
        self.present(EkycHelpViewController(), animated: true) {
            
        }
    }
    
    func setUpPassportInitView() {
        topCoverView = UIView()
        topCoverView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.view.addSubview(topCoverView)
        topCoverView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(FitHeight(height: 150.0))
        }
        
        let backButton: UIButton = UIButton()
        backButton.setImage(UIImage(named: "back_icon"), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        topCoverView.addSubview(backButton)
        backButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(ConstantFile.statusHeight)
            make.width.equalTo(30)
            make.height.equalTo(30)
        }
        
        let titleView: UIView = UIView()
        titleView.backgroundColor = UIColor.init(hex: "#ce0e2d")
        titleView.layer.cornerRadius = 13
        topCoverView.addSubview(titleView)
        titleView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.height.equalTo(26)
            make.centerY.equalTo(backButton.snp.bottom).offset(FitHeight(height: 50.0))
        }
        
        titleLabel = UILabel()
        titleView.addSubview(titleLabel)
        titleLabel.text = LocalizationTool.shareInstance.valueWithKey(key: "passport_title")
        titleLabel.textColor = UIColor.init(hex: "#FFFFFF")
        titleLabel.font = UIFont.systemFont(ofSize: 12)
        titleLabel.textAlignment = .center
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(26)
        }
        
        // scan view
        buildScanView()
        
        // bottom view
        bottomCoverView = UIView()
        bottomCoverView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.view.addSubview(bottomCoverView)
        bottomCoverView.snp.makeConstraints { (make) in
            make.top.equalTo(scanSuper.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        buildPassportBottomView()
    }
    
    @objc func backAction() {
        timeCancel()
        pageBack()
        self.dismiss(animated: true) {
        }
    }
    
    func buildScanView() {
        let scanHeight: Double = FitHeight(height: 230.0)
        let lineWidth = 25
        
        scanSuper = UIView()
        self.view.addSubview(scanSuper)
        scanSuper.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalTo(topCoverView.snp.bottom)
            make.height.equalTo(scanHeight)
        }
        
        let leftCover = UIView()
        leftCover.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        scanSuper.addSubview(leftCover)
        leftCover.snp.makeConstraints { (make) in
            make.width.equalTo(20)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
        }
        
        let rightCover = UIView()
        rightCover.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        scanSuper.addSubview(rightCover)
        rightCover.snp.makeConstraints { (make) in
            make.width.equalTo(20)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.right.equalToSuperview()
        }
        
        let scanView = UIView()
        scanSuper.addSubview(scanView)
        scanView.snp.makeConstraints { (make) in
            make.left.equalTo(leftCover.snp.right)
            make.right.equalTo(rightCover.snp.left)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        let topLeft = UIView()
        scanView.addSubview(topLeft)
        topLeft.backgroundColor = UIColor.init(hex: "#ce0e2d")
        topLeft.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview()
            make.height.equalTo(2)
            make.width.equalTo(lineWidth)
        }
        
        let letTop = UIView()
        scanView.addSubview(letTop)
        letTop.backgroundColor = UIColor.init(hex: "#ce0e2d")
        letTop.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview()
            make.height.equalTo(lineWidth)
            make.width.equalTo(2)
        }
        
        let topRight = UIView()
        scanView.addSubview(topRight)
        topRight.backgroundColor = UIColor.init(hex: "#ce0e2d")
        topRight.snp.makeConstraints { (make) in
            make.right.top.equalToSuperview()
            make.height.equalTo(2)
            make.width.equalTo(lineWidth)
        }
        
        let rightTop = UIView()
        scanView.addSubview(rightTop)
        rightTop.backgroundColor = UIColor.init(hex: "#ce0e2d")
        rightTop.snp.makeConstraints { (make) in
            make.right.top.equalToSuperview()
            make.height.equalTo(lineWidth)
            make.width.equalTo(2)
        }
        
        let bottomLeft = UIView()
        scanView.addSubview(bottomLeft)
        bottomLeft.backgroundColor = UIColor.init(hex: "#ce0e2d")
        bottomLeft.snp.makeConstraints { (make) in
            make.left.bottom.equalToSuperview()
            make.height.equalTo(2)
            make.width.equalTo(lineWidth)
        }
        
        let leftBottom = UIView()
        scanView.addSubview(leftBottom)
        leftBottom.backgroundColor = UIColor.init(hex: "#ce0e2d")
        leftBottom.snp.makeConstraints { (make) in
            make.left.bottom.equalToSuperview()
            make.width.equalTo(2)
            make.height.equalTo(lineWidth)
        }
        
        let bottomRight = UIView()
        scanView.addSubview(bottomRight)
        bottomRight.backgroundColor = UIColor.init(hex: "#ce0e2d")
        bottomRight.snp.makeConstraints { (make) in
            make.right.bottom.equalToSuperview()
            make.height.equalTo(2)
            make.width.equalTo(lineWidth)
        }
        
        let rightBottom = UIView()
        scanView.addSubview(rightBottom)
        rightBottom.backgroundColor = UIColor.init(hex: "#ce0e2d")
        rightBottom.snp.makeConstraints { (make) in
            make.right.bottom.equalToSuperview()
            make.width.equalTo(2)
            make.height.equalTo(lineWidth)
        }
        
        if (scanType == 0) {
            progressSuper = UIView()
            scanView.addSubview(progressSuper)
            progressSuper.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(FitWidth(width: 40))
                make.right.equalToSuperview().offset(FitHeight(height: -40))
                make.bottom.equalTo(scanView.snp.bottom).offset(FitHeight(height: -65))
            }
            
            progressValue = UILabel()
            progressValue.text = "0% \(LocalizationTool.shareInstance.valueWithKey(key: "scan_completed"))"
            progressValue.textColor = .white
            progressValue.font = UIFont.systemFont(ofSize: 12)
            progressSuper.addSubview(progressValue)
            progressValue.snp.makeConstraints { (make) in
                make.right.equalToSuperview()
                make.centerY.equalToSuperview()
            }

            progressView = UIProgressView()
            progressSuper.addSubview(progressView)
            progressView.progress = 0.0
            progressView.layer.cornerRadius = 3
            progressView.layer.masksToBounds = true
            progressView.progressTintColor = UIColor.init(hex: "#ce0e2d")
            progressView.trackTintColor = UIColor.init(hex: "#1D1D1D")
            progressView.snp.makeConstraints { (make) in
                make.height.equalTo(6)
                make.left.equalToSuperview()
                make.centerY.equalToSuperview()
                make.right.equalTo(progressValue.snp.left).offset(FitHeight(height: -10.0))
            }
            
            let titleView: UIView = UIView()
            titleView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            titleView.layer.cornerRadius = 13
            scanView.addSubview(titleView)
            titleView.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.height.equalTo(30)
                make.left.greaterThanOrEqualTo(leftCover.snp.right).offset(15)
                make.right.greaterThanOrEqualTo(rightCover.snp.left).offset(-15)
                make.bottom.equalTo(progressSuper.snp.bottom).offset(FitHeight(height: -30.0))
            }

            titleLabel = UILabel()
            titleView.addSubview(titleLabel)
            titleLabel.text = LocalizationTool.shareInstance.valueWithKey(key: "scan_tilt")
            titleLabel.textColor = .white
            titleLabel.font = UIFont.systemFont(ofSize: 12)
            titleLabel.textAlignment = .center
            titleLabel.numberOfLines = 0
            titleLabel.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(30)
                make.right.equalToSuperview().offset(-30)
                make.centerY.equalToSuperview()
            }
        }
    }
    
    func buildBottomCover(){
        firstBootomVersin = UIView()
        bottomCoverView.addSubview(firstBootomVersin)
        firstBootomVersin.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
        
        let descLabel = UILabel()
        descLabel.text = LocalizationTool.shareInstance.valueWithKey(key: "scan_desc_sure")
        descLabel.textColor = .white
        descLabel.numberOfLines = 0
        descLabel.textAlignment = .center
        descLabel.font = UIFont.systemFont(ofSize: 14)
        firstBootomVersin.addSubview(descLabel)
        descLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.left.equalToSuperview().offset(30)
            make.right.equalToSuperview().offset(-30)
            make.top.equalToSuperview().offset(FitHeight(height: 15.0))
        }
        
        let videoView = UIView()
        firstBootomVersin.addSubview(videoView)
        videoView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(descLabel.snp.bottom).offset(FitHeight(height: 20))
            make.width.equalTo(FitWidth(width: 144))
            make.height.equalTo(FitHeight(height: 94))
        }
        
        let path = Bundle.main.path(forResource: "hologram_animation", ofType: ".mp4")
        let videoUrl = NSURL.fileURL(withPath: path!)
        
        let playerItem: AVPlayerItem = AVPlayerItem(url: videoUrl)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidPlayToEndTime), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        
        cameraVideoPlayer = AVPlayer(playerItem: playerItem)
        if (cameraVideoPlayerLayer != nil) {
            cameraVideoPlayerLayer.removeFromSuperlayer()
        }
        cameraVideoPlayerLayer = AVPlayerLayer(player: cameraVideoPlayer)
        videoView.layer.addSublayer(cameraVideoPlayerLayer)
        cameraVideoPlayerLayer.frame = CGRect(x: 0, y: 0, width: FitWidth(width: 140), height: FitHeight(height: 60))
        cameraVideoPlayer.play()
        

        firstTimeLabel = UILabel()
        firstTimeLabel.text = "\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_start"))120\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_end"))"
        firstTimeLabel.textColor = UIColor(hex: "#B4B4B4")
        firstTimeLabel.font = UIFont.systemFont(ofSize: 12)
        firstBootomVersin.addSubview(firstTimeLabel)
        firstTimeLabel.snp.makeConstraints { (make) in
            make.top.equalTo(videoView.snp.bottom).offset(FitHeight(height: 40))
            make.centerX.equalToSuperview()
        }
    }
    
    func buildSecondView() {
        secondBootomVersin = UIView()
        bottomCoverView.addSubview(secondBootomVersin)
        secondBootomVersin.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
        
        scanTitle = UILabel()
        scanTitle.text = LocalizationTool.shareInstance.valueWithKey(key: "scan_card_desc")
        scanTitle.numberOfLines = 0
        scanTitle.textColor = .white
        scanTitle.font = UIFont.systemFont(ofSize: 14)
        scanTitle.textAlignment = .left
        secondBootomVersin.addSubview(scanTitle)
        scanTitle.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(FitHeight(height: 15.0))
            make.left.equalToSuperview().offset(FitWidth(width:  20))
            make.right.equalToSuperview().offset(FitWidth(width: -20))
        }
        
        let pictureView = UIView()
        secondBootomVersin.addSubview(pictureView)
        pictureView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.width.equalTo(FitWidth(width: 270.0))
            make.height.equalTo(FitHeight(height: 75.0))
            make.top.equalTo(scanTitle.snp.bottom).offset(FitHeight(height: 30.0))
        }
        
        let frontView = UIView()
        pictureView.addSubview(frontView)
        frontView.layer.cornerRadius = 5
        frontView.layer.borderWidth = 1
        frontView.layer.borderColor = UIColor.init(hex: "#919191").cgColor
        frontView.snp.makeConstraints { (make) in
            make.left.top.bottom.equalToSuperview()
            make.width.equalTo(FitWidth(width: 125.0))
        }
        
        let frontLabel = UILabel()
        frontLabel.text = LocalizationTool.shareInstance.valueWithKey(key: "scan_front")
        frontLabel.textColor = UIColor.init(hex: "#919191")
        frontView.addSubview(frontLabel)
        frontLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        frontImage = UIImageView()
        frontImage.layer.cornerRadius = 5
        frontView.addSubview(frontImage)
        frontImage.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        let backView = UIView()
        pictureView.addSubview(backView)
        backView.layer.cornerRadius = 5
        backView.layer.borderWidth = 1
        backView.layer.borderColor = UIColor.init(hex: "#919191").cgColor
        backView.snp.makeConstraints { (make) in
            make.right.top.bottom.equalToSuperview()
            make.width.equalTo(FitWidth(width: 125.0))
        }
        
        let backLabel = UILabel()
        backLabel.text = LocalizationTool.shareInstance.valueWithKey(key: "scan_back")
        backLabel.textColor = UIColor.init(hex: "#919191")
        backView.addSubview(backLabel)
        backLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        backImage = UIImageView()
        backView.addSubview(backImage)
        backImage.layer.cornerRadius = 5
        backImage.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        secondTimeLabel = UILabel()
        secondTimeLabel.text = "\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_start"))60\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_end"))"
        secondTimeLabel.textColor = UIColor.init(hex: "#B4B4B4")
        secondTimeLabel.font = UIFont.systemFont(ofSize: 12)
        secondBootomVersin.addSubview(secondTimeLabel)
        secondTimeLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(pictureView.snp.bottom).offset(FitHeight(height: 15.0))
        }
    }
    
    func buildPassportBottomView() {
        bottomCoverView = UIView()
        bottomCoverView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.view.addSubview(bottomCoverView)
        bottomCoverView.snp.makeConstraints { (make) in
            make.top.equalTo(scanSuper.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        secondBootomVersin = UIView()
        bottomCoverView.addSubview(secondBootomVersin)
        secondBootomVersin.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
        
        let descLabel = UILabel()
        descLabel.text = LocalizationTool.shareInstance.valueWithKey(key: "scan_passport_desc")
        descLabel.textColor = .white
        secondBootomVersin.addSubview(descLabel)
        descLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(FitHeight(height: 30.0))
        }
        
        
        let frontView = UIView()
        secondBootomVersin.addSubview(frontView)
        frontView.layer.cornerRadius = 5
        frontView.layer.borderWidth = 1
        frontView.layer.borderColor = UIColor.init(hex: "#919191").cgColor
        frontView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.width.equalTo(150)
            make.height.equalTo(FitHeight(height: 80.0))
            make.top.equalTo(descLabel.snp.bottom).offset(FitHeight(height: 30.0))
        }
        
        let frontLabel = UILabel()
        frontLabel.text = LocalizationTool.shareInstance.valueWithKey(key: "scan_card_first")
        frontLabel.textColor = UIColor.init(hex: "#919191")
        frontView.addSubview(frontLabel)
        frontLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        frontImage = UIImageView()
        frontImage.layer.cornerRadius = 5
        frontView.addSubview(frontImage)
        frontImage.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        firstTimeLabel = UILabel()
        firstTimeLabel.text = "\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_start"))60\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_end"))"
        firstTimeLabel.textColor = UIColor.init(hex: "#B4B4B4")
        firstTimeLabel.font = UIFont.systemFont(ofSize: 12)
        secondBootomVersin.addSubview(firstTimeLabel)
        firstTimeLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(frontView.snp.bottom).offset(FitHeight(height: 40.0))
        }
    }
    
    func initTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc func timerAction() {
        totalTimeLength -= 1
        
        if (totalTimeLength == 0) {
            timeCancel()
            ConstantFile.alertTryAgainView(callBack: { [self]tag in
                if (tag == 1) { // cancel
                    self.dismiss(animated: true) {}
                }else if (tag == 0) { // try again
//                    totalTimeLength = 120
//                    initTimer()
                    timeCancel()
                    self.dismiss(animated: true) {}
                }
            })
        }else{
            DispatchQueue.main.async { [self] in
                if scanType == 0 {
                    firstTimeLabel.text = "\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_start"))\(totalTimeLength)\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_end"))"
                    secondTimeLabel.text = "\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_start"))\(totalTimeLength)\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_end"))"
                }else{
                    firstTimeLabel.text = "\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_start"))\(totalTimeLength)\(LocalizationTool.shareInstance.valueWithKey(key: "scan_timer_end"))"
                }
            }
        }
    }
    
    func timeCancel() {
        if timer != nil {
            timer?.invalidate()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timeCancel()
//        pageBack()
        NotificationCenter.default.removeObserver(self)
    }
    
    func pageBack() {
        var backData = [String: Any]()
        backData["status"] = "close_page"
        backData["data"] = nil
        backData["success"] = false
        backData["message"] = ""
        EkycTool.ekycScan.passport! ? EkycTool.ekycScan.flutterResultPassport(backData) : EkycTool.ekycScan.flutterResultIc(backData)
    }
    
}

extension ScanIdCardViewController {
    
    @objc func playerItemDidPlayToEndTime() {
        if (cameraVideoPlayer != nil) {
            cameraVideoPlayer.seek(to: CMTime.zero)
            cameraVideoPlayer.play()
        }
    }
    
    
    override func startScanHologram() {
    }
    
    override func setHologramProgress(_ progress: Double) {
        print(progress)
        DispatchQueue.main.async { [self] in
            progressView.progress = Float(progress)
            progressValue.text = "\(String(format: "%.2f", progress * 100))% \(LocalizationTool.shareInstance.valueWithKey(key: "scan_completed"))"
        }
    }
    
    override func passHologramICCheck() {
    }
    
    override func passHologramNameCheck() {
    }
    
    override func passHologramGhostCheck() {
    }
    
    override func hasScanHologram() {
    }
    
    override func startScanFontSize() {
    }
    
    override func startScanFront() {
        if (scanType == 0) {
            progressSuper.isHidden = true
            firstBootomVersin.isHidden = true
            secondBootomVersin.isHidden = false
//            scanTitle.text = "Position your ID within the frame and avoid any reflection on your ID card."
            titleLabel.text = LocalizationTool.shareInstance.valueWithKey(key: "scan_card_front")
        }
    }
    
    override func hasScanFront(_ frontImage: UIImage) {
        if (scanType == 0) {
//            scanTitle.text = "Do not cover the card corners and avoid any reflection of your ID card."
            titleLabel.text = LocalizationTool.shareInstance.valueWithKey(key: "scan_card_back")
        }
        self.frontImage.image = frontImage
    }
    
    override func startScanBack() {
    }
    
    override func hasScanBack(_ backImage: UIImage) {
        self.backImage.image = backImage
    }
}
