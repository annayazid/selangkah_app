//
//  LocalizationTool.swift
//  Runner
//
//  Created by marvin on 2021/5/14.
//  Copyright © 2021 The Chromium Authors. All rights reserved.
//

import Foundation

// en  ms-MY
class LocalizationTool: NSObject {
     static let shareInstance = LocalizationTool()
 
     let def = UserDefaults.standard
     var bundle : Bundle?
     
     func valueWithKey(key: String!) -> String {
          let bundle = LocalizationTool.shareInstance.bundle
          let str = bundle?.localizedString(forKey: key, value: nil, table: "Selangkah")
          return str!
     }
     
     func setLanguage(langeuage:String) {
        let str = langeuage
        UserDefaults.standard.set(str, forKey: "langeuage")
        UserDefaults.standard.synchronize()
        let path = Bundle.main.path(forResource:str , ofType: "lproj")
        bundle = Bundle(path: path!)
     }
}
