//
//  JordanIDConfig.h
//  XenchainSDK
//
//  Created by Jovial on 05/11/2020.
//  Copyright © 2020 Xenchain. All rights reserved.
//

#import <XenchainSDK/XenchainSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface JordanIDConfig : CardConfig

@end

NS_ASSUME_NONNULL_END
