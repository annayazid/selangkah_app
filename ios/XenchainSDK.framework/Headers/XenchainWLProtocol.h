//
//  XenchainWLProtocol.h
//  XenchainSDK
//
//  Created by Jovial  on 12/9/2019.
//  Copyright © 2019 Xenchain. All rights reserved.
//

#ifndef XenchainWLProtocol_h
#define XenchainWLProtocol_h

@protocol XenchainWhitelabelCallback

-(void) ServiceOrderSequences:(NSString *)accessToken serviceOrder:(NSArray<NSString *> *)serviceOrder;
-(void) MaxRetryReached;

@end

#endif /* XenchainWLProtocol_h */
