//
//  XenchainFaceRecordViewController.h
//  XenditySDK
//
//  Created by Jovial  on 2/9/2019.
//  Copyright © 2019 Xenchain. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XenchainProtocol.h"

NS_ASSUME_NONNULL_BEGIN

/**
 @brief Overlay ViewController that will be used as overlay for the Face Match Camera UI.
 
 @discussion This class is used to create an UI Overlay for the Camera. In addition, this class also includes callback methods which will be called by the SDK during the Face Match process.
 
 @warning This class needs to be extended as it is used as an overlay for the Camera UI. Failure to do so will results in Camera UI having pure Camera Screen with no User Interaction at all.
 */
@interface XenchainFaceRecordViewController : UIViewController

@property (nonatomic, assign) id<XenchainFaceCallback> delegate;

/**
 @brief Function used to start record video.
 
 @discussion This function is used to record video for processing. Processing includes Liveness of the Video & Face Match.
 
 @warning Use `isRecording` function to check whether the the Camere is recording/processing or available to record/processing.
 */
- (void)startVideoRecord;

/**
 @brief Function used to determine the readiness to record video.
 
 @discussion This function is used to determine whether the Camera is ready for record & processing.
 */
- (bool)isRecording;

/**
@brief Function called when the Camera has start recording.

@discussion This function is called when the Camera has started to record video.
*/
- (void)startRecording;

/**
@brief Function called when processing recording.

@discussion This function is called when the Camera has successfully capture the video and start processing the video input.
*/
- (void)processingRecording;

/**
@brief Function called when Camera completed the Video Process.

@discussion This function is called when the Camera has successfully process the video.
*/
- (void)completingProcess;

@end

NS_ASSUME_NONNULL_END
