//
//  XHoloScannerViewController.h
//  XenchainSDK
//
//  Created by Jovial on 21/09/2020.
//  Copyright © 2020 Xenchain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Microblink/Microblink.h>

#import "CardConfig.h"
#import "XenchainProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface XHoloScannerViewController : MBCustomOverlayViewController

-(void) pauseCamera;

-(void) resumeCamera;

-(CardConfig *) getCardConfig;

-(void) closeScannerAsFail;

/**
 @brief Delegate function for Start Hologram Scanning.
 
 @discussion This function is called when the ID Scanner is about to Start Scan Hologram.
 
 @warning This function is called if the `CheckHologram` variable is set to `true`. `CheckHologram` can be setup in `CardConfig` class and it's respective child class.
 */
-(void) startScanHologram;

/**
 @brief Delegate function for Hologram Scanning Process
 
 @discussion This function is used to check the progress of Hologram Scanning.
 
 @param progress Refers to the progress of Hologram Scanning. It's value is from 0 to 1 in Double Data Type.
 
 @warning This function is called if the `CheckHologram` variable is set to `true`. `CheckHologram` can be setup in `CardConfig` class and it's respective child class.
 */
-(void) setHologramProgress:(double)progress;

/**
@brief Delegate function for Hologram Scanning Process

@discussion This function will be called when the Scanner has successfully pass the hologram threshold of the IC Check.

@warning This function is called if the `CheckHologram` variable is set to `true`. `CheckHologram` can be setup in `CardConfig` class and it's respective child class.
*/
-(void) passHologramICCheck;

/**
@brief Delegate function for Hologram Scanning Process

@discussion This function will be called when the Scanner has successfully pass the hologram threshold of the Name Check.

@warning This function is called if the `CheckHologram` variable is set to `true`. `CheckHologram` can be setup in `CardConfig` class and it's respective child class.
*/
-(void) passHologramNameCheck;

/**
@brief Delegate function for Hologram Scanning Process

@discussion This function will be called when the Scanner has successfully pass the hologram threshold of the Ghost Check.

@warning This function is called if the `CheckHologram` variable is set to `true`. `CheckHologram` can be setup in `CardConfig` class and it's respective child class.
*/
-(void) passHologramGhostCheck;

/**
 @brief Delegate function for Hologram Scanning Complete Process
 
 @discussion This function is called to represents that the Hologram Scanning has been completed or bypassed.
 
 @warning This function is called if the `CheckHologram` variable is set to `true`. `CheckHologram` can be setup in `CardConfig` class and it's respective child class.
 */
-(void) hasScanHologram;

@end

NS_ASSUME_NONNULL_END
