//
//  XHoloScannerV2ViewController.h
//  XenchainSDK
//
//  Created by Jovial on 13/09/2021.
//  Copyright © 2021 Xenchain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Microblink/Microblink.h>

#import "CardConfig.h"
#import "XenchainProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface XHoloScannerV2ViewController : MBCustomOverlayViewController

-(void) pauseCamera;

-(void) resumeCamera;

-(CardConfig *) getCardConfig;

-(void) closeScannerByUser;

-(void) closeScannerByTimeout;

/**
 @brief Delegate function for Start Hologram Scanning.
 
 @discussion This function is called when the ID Scanner is about to Start Scan Hologram.
 
 @warning This function is called if the `CheckHologram` variable is set to `true`. `CheckHologram` can be setup in `CardConfig` class and it's respective child class.
 */
-(void) startScanHologram;

/**
 @brief Delegate function for Hologram Scanning Process
 
 @discussion This function is used to check the progress of Hologram Scanning.
 
 @param progress Refers to the progress of Hologram Scanning. It's value is from 0 to 1 in Double Data Type.
 
 @warning This function is called if the `CheckHologram` variable is set to `true`. `CheckHologram` can be setup in `CardConfig` class and it's respective child class.
 */
-(void) setHologramProgress:(double)progress;

/**
 @brief Delegate function for Hologram Scanning Complete Process
 
 @discussion This function is called to represents that the Hologram Scanning has been completed or bypassed.
 
 @warning This function is called if the `CheckHologram` variable is set to `true`. `CheckHologram` can be setup in `CardConfig` class and it's respective child class.
 */
-(void) hasScannedHologram;

@end

NS_ASSUME_NONNULL_END
