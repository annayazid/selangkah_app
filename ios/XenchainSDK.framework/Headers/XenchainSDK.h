//
//  XenchainSDK.h
//  XenchainSDK
//
//  Created by Jovial  on 10/4/2019.
//  Copyright © 2019 Xenchain. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for XenchainSDK.
FOUNDATION_EXPORT double XenchainSDKVersionNumber;

//! Project version string for XenchainSDK_V1_1.
FOUNDATION_EXPORT const unsigned char XenchainSDKVersionString[];

#import "AnswerKBA.h"
#import "BillingStatus.h"
#import "CardConfig.h"
#import "EKTPConfig.h"
#import "ErrorCode.h"
#import "JordanIDConfig.h"
#import "MyDLConfig.h"
#import "MyKadConfig.h"
#import "MyTenteraConfig.h"
#import "OCRType.h"
#import "PassportConfig.h"
#import "ServiceType.h"
#import "XCardResult.h"

#import "XenchainProtocol.h"
#import "XenchainWLProtocol.h"
#import "XenchainScanner.h"
#import "XBarcodeViewController.h"
#import "XenchainFaceRecordViewController.h"
#import "XFaceViewController.h"
#import "XHoloScannerViewController.h"
#import "XHoloScannerV2ViewController.h"
#import "XScannerViewController.h"
#import "XScannerV2ViewController.h"
#import "XScannerV2TestViewController.h"
#import "XScannerV3ViewController.h"
#import "XSignatureViewController.h"
#import "XPassportCoverViewController.h"



