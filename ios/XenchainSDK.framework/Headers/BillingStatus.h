//
//  BillingStatus.h
//  XenchainSDK
//
//  Created by Jovial  on 12/9/2019.
//  Copyright © 2019 Xenchain. All rights reserved.
//

#ifndef BillingStatus_h
#define BillingStatus_h

typedef enum BillingStatus: int {
    PENDING = 0,
    COMPLETE = 1,
    FAIL = 2,
    TIMEOUT = 3,
    CRASH = 4
} BillingStatus;

#endif /* BillingStatus_h */
