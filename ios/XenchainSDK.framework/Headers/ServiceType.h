//
//  ServiceType.h
//  XenchainSDK
//
//  Created by Jovial  on 11/9/2019.
//  Copyright © 2019 Xenchain. All rights reserved.
//

#ifndef ServiceType_h
#define ServiceType_h

typedef enum ServiceType: int {
    HOLOGRAM = 1,
    LANDMARK = 2,
    OCR_SCANING = 3,
    FACE_MATCH = 4,
    ID_VERIFICATION = 5,
    KBA_VERIFICATION = 6
} ServiceType;

#endif /* ServiceType_h */
