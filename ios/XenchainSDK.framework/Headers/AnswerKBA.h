//
//  AnswerKBA.h
//  XenchainSDK
//
//  Created by Jovial  on 18/8/2019.
//  Copyright © 2019 Xenchain. All rights reserved.
//

#ifndef AnswerKBA_h
#define AnswerKBA_h

#import <Foundation/Foundation.h>

@interface AnswerKBA : NSObject

- (instancetype _Nullable)initWithDictionary:(NSDictionary * _Nonnull)inputDictionary;
- (void)setQuestionID:(NSString * _Nonnull)value;
- (NSString * _Nullable)getQuestionID;
- (void)setAnswer:(bool)value;
- (NSString * _Nullable)getAnswer;
- (NSMutableDictionary * _Nonnull)dictionaryValues;

@end

#endif /* AnswerKBA_h */
