//
//  OCRType.h
//  XenchainSDK
//
//  Created by Jovial  on 11/4/2019.
//  Copyright © 2019 Xenchain. All rights reserved.
//

#ifndef OCRType_h
#define OCRType_h

typedef enum OCRType: int {
    ID_PSP_MRTD = 0,
    ID_MYS_MYKAD = 1,
    ID_MYS_MYTENTERA = 2,
    ID_MYS_IKAD = 3,
    ID_MYS_MYDL = 4,
    ID_MYS_MYPR = 5,
    ID_SGP_NRIC = 6,
    ID_SGP_WPC = 7,
    ID_SGP_VPC = 8,
    ID_SGP_DLC = 9,
    ID_IDN_EKTP = 10,
    ID_THA_NIDC = 11,
    ID_THA_DLC_65 = 12,
    ID_THA_DLC_35 = 13,
    ID_JOR_NRIC = 14
} OCRType;

#endif /* OCRType_h */
