//
//  EKTPConfig.h
//  XenchainSDK
//
//  Created by Jovial  on 15/10/2019.
//  Copyright © 2019 Xenchain. All rights reserved.
//

#ifndef EKTPConfig_h
#define EKTPConfig_h

#import "CardConfig.h"

/**
 @brief Configuration for EKTP Scanning.
 
 @discussion This class is designed to allowed developers with the freedom to determine which part of the card must be scan or not.
 
 @warning Every ID Card configuration will have at least one and/or two (in case of front and back scanning) mandatory field scanning. Those mandatory field scanning will not be listed in the corresponding class.
 
 @note For this class, the Frontside OCR Scanning is mandatory. Front & Back Scanning is not applicable.
 */
@interface EKTPConfig: CardConfig

@end

#endif /* EKTPConfig_h */
