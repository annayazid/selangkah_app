//
//  NSObject+XenchainSDK.h
//  XenchainCustomSDK
//
//  Created by Jovial  on 10/2/2019.
//  Copyright © 2019 Xenchain. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AnswerKBA.h"
#import "BillingStatus.h"
#import "CardConfig.h"
#import "ErrorCode.h"
#import "ServiceType.h"
#import "XenchainProtocol.h"
#import "XenchainWLProtocol.h"
#import "XBarcodeViewController.h"
#import "XenchainFaceRecordViewController.h"
#import "XFaceViewController.h"
#import "XHoloScannerViewController.h"
#import "XHoloScannerV2ViewController.h"
#import "XPassportCoverViewController.h"
#import "XScannerViewController.h"
#import "XScannerV2ViewController.h"
#import "XScannerV2TestViewController.h"
#import "XScannerV3ViewController.h"
#import "XSignatureViewController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 @brief Main Class of the XenchainSDK.
 
 @discussion Responsible for providing client project access to Xenchain Features.
 
 @note For Debug Information during the execution of the SDK, please refer to the printed log on XCode Debug Window. The Debug Information will usually opened with *Xenchain Log:* text followed with the description of the Debug.
 */
@interface XenchainSDK: NSObject

+(NSString * _Nonnull) SDKVersion;

+(void) setCOLLECT_IMAGE:(bool)value;

+(void) setAPI_VERSION:(int)value;

+(NSString * _Nullable) CertNameWithType;
+(void) setCertNameWithType:(NSString * _Nonnull)value;

/**
@brief Checks whether the deployed Extended ViewController is full-screen.

@discussion If set 'true', any deployed Extended ViewController will be set to full-screen as per pre-iOS 13 design. Otherwise, it will follow default card presentation.
*/
+(bool) isFullScreenDeployment;

/**
 @brief Set whether the deployed Extended ViewController is full-screen.
 
 @discussion If set 'true', any deployed Extended ViewController will be set to full-screen as per pre-iOS 13 design. Otherwise, it will follow default card presentation.
 */
+(void) setFullScreeenDeployment:(bool)value;

/**
 @brief Refers to the Scanning Sound for ID Scanner.
 
 @discussion Gets the Scanning Sound that will be used for ID scanning.
 
 @note Default value will be used if no Scanning Sound is being set.
 */
+(NSString * _Nullable) scanningSound;
/**
 @brief Refers to the Scanning Sound for ID Scanner.
 
 @discussion Sets the Scanning Sound that will be used for ID scanning.
 
 @note Default value will be used if no Scanning Sound is being set.
 */
+(void) setScanningSound:(NSString * _Nonnull)value;

/**
 @brief Refers to the Loading logo of the Loading Screen during the Scanner/Face Match Processing.
 
 @discussion Gets the loading Logo image for the Loading screen during the scanning and face match process.
 
 @note Default value will be used if no Loading Image is being set.
 */
+(UIImage * _Nullable) loadingLogo;
/**
 @brief Refers to the Loading logo of the Loading Screen during the Scanner/Face Match Processing.
 
 @discussion Sets the Loading Logo image for the Loading screen during the scanning and face match process.
 
 @note Default value will be used if no Loading Image is being set.
 */
+(void) setLoadingLogo:(UIImage * _Nonnull)value;

/**
 @brief Refers to the  Loading Screen during the Scanner/Face Match Processing.
 
 @discussion Gets the view for the Loading screen during the scanning and face match process.
 
 @note Default value will be used if no Loading View is being set.
 */
+(UIView * _Nullable) loadingView;
/**
 @brief Refers to the  Loading Screen during the Scanner/Face Match Processing.
 
 @discussion Sets the view for the Loading screen during the scanning and face match process.
 
 @note Default value will be used if no Loading View is being set.
 */
+(void) setLoadingView:(UIView * _Nonnull)value;

+(void) setWhiteLabelCallback:(id<XenchainWhitelabelCallback> _Nonnull)callback;
+(void) setDebugCallback:(id<XenchainDebugCallback> _Nonnull)callback;
+(int) getScanningLeftOver;
+(int) getFaceMatchLeftOver;

/**
 @brief Initialise the XenchainSDK. This include checking the validity of API Key and what features can be accessed by the API Key.
 
 @param apiKey Variable used for Initialize the XenchainSDK. Depending on the Access Granted, other features will be initialized as well.
 @param apiURL The API URL that will be used to access the XenchainSDK features. Depending on the requirement, this could be Xenchain cloud server or on-premise hosted server. Default value is `false`.
 @param completionHandler Completion Block or Completion Handler used for returning the results of Initialization. Refer to `XenchainSDKCallback` Protocol for further information.
 
 @discussion Please ensure that this function is executed and the response from completion handler is returned before proceeding to other features of the SDK.
 */
+(void) InitSDK:(NSString * _Nonnull)apiKey apiURL:(NSString *)apiURL onPremSaveData:(bool)onPremSaveData completionHandler:(id<XenchainSDKCallback> _Nonnull)completionHandler;

/**
 @brief Setup Reference ID as the Parent Transaction of the OnBoardingID.
 
 @param referenceID The param value refers to every onboarding or transactions done under this value.
 
 @discussion This function are required to be used if the app needs to categorise the whole onboarding process under the param value. This is usually useful if the app needs to track each user's onboarding journey and how many onboarding has been done. In this case, the `referenceID` will tracked all the details of each onboaring that has been done by the user.
 
 @warning Should the app need to use this feature, please ensure that the function is executed before proceeding to other features of this SDK.

 */
+(void) setReferenceID:(NSString * _Nonnull)referenceID;

+(void) setOnBoardingID:(NSString * _Nonnull)onBoardingID;

/**
@brief Setup and provide ID Card Scanner with initial information.

@param userID Applicable only if the particular onboarding needs to be tied to certain user, otherwise the value is empty string.
@param inputDetails A JSON Dictionary that contains the full name and IC Number of the user. Param key for full name is `fullName`. Param key for IC Number is `documentNumber`.
@param completionHandler Completion Block or Completion Handler used for returning the results of this function. In this case, `onBoardingID` refers the Overall Transaction ID for the whole Onboarding Process. The value contained in this variable will be used to track User's onboarding journeys.

@pre Requires `InitSDK` function to be executed with callbacks return with success value.
*/
+(void) setPreDeployment:(NSString * _Nonnull)userID inputDetails:(NSDictionary * _Nonnull)inputDetails completionHandler:(void (^)(NSString * _Nullable referenceID, NSString * _Nullable onBoardingID, NSString * _Nullable errorMessage))completionHandler;

/**
@brief Update name and modify ID Card Scanner with user entry.


@param inputDetails A JSON Dictionary that contains the full name and IC Number of the user. Param key for full name is `fullName`. Param key for IC Number is `documentNumber`.
@param completionHandler Completion Block or Completion Handler used for returning the results of this function. In this case, `nameIsSimilar` refers to name similarity between the edited name and scan card name, while `documentNoIsSimilar` refers the document number similarity between edited card ic number and edited ic number, while `frontBackIsSimilar` refers the document number similarity between front and back card scan card number with edited ic number

@pre Requires `InitSDK` and ''DeployScanner" function to be executed with callbacks return with success value.
*/
+ (void)setPostDeployment:(NSDictionary * _Nonnull)inputDetails onBoardingID:(NSString * _Nonnull)onBoardingID referenceID:(NSString *)referenceID completionHandler:(id<XenchainScannerCallback> _Nonnull)completionHandler;

+(void) ConfigureOnBoardingID:(NSString * _Nonnull)userID onBoardingID:(NSString * _Nonnull)onBoardingID;

+(void) configureAPIDelay:(int)numberOfRetry delayTime:(double)delayTime;

/**
@brief Setup and provide ID Card Scanner with OCR Results first.

@discussion This function determines whether the Scanner return the OCR Results first before commiting for Landmark Checks and Text Similarity Checks. Please note that Hologram Checks, if enabled, still applicable for the ID Scanner.
 
@param usePreviewMode Enables OCR Preview Mode before submitting the results for landmark checks. Please note that hologram checks are also required before OCR Scanning if it is enabled..

@pre Requires `InitSDK` function to be executed with callbacks return with success value.
*/
+(void) setPreviewOCR:(bool)usePreviewMode;

/**
 @brief Setup and provide View Controller of the ID Card Scanner.

 @param scanConfig This variable carries the configuration of the Card/Passport that will be scanned. Refer to the `ScanConfig` class type for more information.
 @param inputController The UIViewController that is used to present the extended class of the `XScannerViewController`.
 @param extendedController The extended class of `XScannerViewController` that is used as UI Overlay for the Camera.
 @param completionHandler Completion Block or Completion Handler used for returning the results of ID Scanning. Refer to `XenchainScannerCallback` Protocol for further information.
 
 @pre Requires `InitSDK` function to be executed with callbacks return with success value.
 
 @post Requires `CompleteScanDeployment` to be executed. Otherwise, the SDK will charged for every scanning.
 
 @warning Developers are required to call the `CompleteScanDeployment` function. Otherwise for every scanning that is being done, the SDK will charged the scan.
 */
+ (void)DeployScanner:(CardConfig * _Nonnull)scanConfig inputController:(UIViewController * _Nonnull)inputController extendedController:(XScannerViewController * _Nonnull)extendedController completionHandler:(id<XenchainScannerCallback> _Nonnull)completionHandler;

+ (void)DeployHologramScanner:(CardConfig * _Nonnull)scanConfig inputController:(UIViewController * _Nonnull)inputController extendedController:(XHoloScannerViewController * _Nonnull)extendedController completionHandler:(id<XenchainScannerCallback> _Nonnull)completionHandler;

+ (void)deployImageCaptureWithInputController:(UIViewController * _Nonnull)inputController extendedController:(XPassportCoverViewController * _Nonnull)extendedController completionHandler:(id<XenchainImageCaptureCallback> _Nonnull)completionHandler;

/**
 @brief Delegate function that must be called when scanning is completed.
 
 @param onBoardingID Refers to the onBoardingID passed from the `XenchainScannerCallback` protocol. The `onBoardingID` is to ensure that the SDK consider this particular transaction as fully consume for ID Scanning.
 @param metaCardResult Refers to the `metaCardResult` passed from the `XenchainScannerCallback` protocol.
 @param cardResult Refers to the `cardResult` passed from the `XenchainScannerCallback` protocol.
 @param scanConfig Refers to the existing `scanConfig` used in the `deployScanner` function
 @param completionHandler Completion Block or Completion Handler. Refer to `XenchainScannerCallback` Protocol for further information.
 
 @pre Requires `DeployScanner` to be executed with callbacks return.
 
 @warning Developers are required to call the `CompleteScanDeployment` function. Otherwise for every scanning that is being done, the SDK will charged the scan.
 */
+(void) CompleteScanDeployment:(NSString * _Nonnull)onBoardingID metaCardResult:(NSDictionary * _Nonnull)metaCardResult cardResult:(XCardResult * _Nonnull)cardResult scanConfig:(CardConfig * _Nonnull)scanConfig completionHandler:(id<XenchainScannerCallback> _Nonnull)completionHandler;

/**
 @brief Delegate function that must be called when scanning is completed.
 
 @param onBoardingID Refers to the onBoardingID passed from the `XenchainScannerCallback` protocol. The `onBoardingID` is to ensure that the SDK consider this particular transaction as fully consume for ID Scanning.
 @param cardFrontReferenceID Refers to the `refFrontID` passed from the `XenchainScannerCallback` protocol.
 @param faceReferenceID Refers to the `refFace` passed from the `XenchainScannerCallback` protocol.
 @param cardBackReferenceID Refers to the `refBackID` passed from the `XenchainScannerCallback` protocol.
 @param cardResult Refers to the `cardResult` passed from the `XenchainScannerCallback` protocol.
 @param completionHandler Completion Block or Completion Handler. Refer to `XenchainScannerCallback` Protocol for further information.
 
 @pre Requires `DeployScanner` to be executed with callbacks return.
 
 @warning Developers are required to call the `CompleteScanDeployment` function. Otherwise for every scanning that is being done, the SDK will charged the scan.
 */
+(void) CompleteScanDeployment:(NSString * _Nonnull)onBoardingID cardFrontReferenceID:(NSString * _Nonnull)cardFrontReferenceID faceReferenceID:(NSString * _Nonnull)faceReferenceID cardBackReferenceID:(NSString * _Nonnull)cardBackReferenceID cardResult:(XCardResult * _Nonnull)cardResult completionHandler:(id<XenchainScannerCallback> _Nonnull)completionHandler __deprecated_msg("Use CompleteScanDeployment:metaCardResult:cardResult:scanConfig:completionHandler: function instead");

/**
 @brief Setup and provide View Controller of the ID Card Scanner.

 @param scanConfig This variable carries the configuration of the Card/Passport that will be scanned. Refer to the `ScanConfig` class type for more information.
 @param inputController The UIViewController that is used to present the extended class of the `XScannerV2ViewController`.
 @param extendedController The extended class of `XScannerV2ViewController` that is used as UI Overlay for the Camera.
 @param completionHandler Completion Block or Completion Handler used for returning the results of ID Scanning. Refer to `XenchainScannerCallback` Protocol for further information.
 
 @pre Requires `InitSDK` function to be executed with callbacks return with success value.
 
 @post Requires `completeScanDeploymentV2WithOnBoardingID` to be executed. Otherwise, the SDK will charged for every scanning.
 
 @warning Developers are required to call the `completeScanDeploymentV2WithOnBoardingID` function. Otherwise for every scanning that is being done, the SDK will charged the scan.
 */
+ (void)deployScannerV2:(CardConfig * _Nonnull)scanConfig inputController:(UIViewController * _Nonnull)inputController extendedController:(XScannerV2ViewController * _Nonnull)extendedController completionHandler:(id<XenchainScannerCallback> _Nonnull)completionHandler;

+ (void)deployScannerV2Test:(CardConfig * _Nonnull)scanConfig inputController:(UIViewController * _Nonnull)inputController extendedController:(XScannerV2TestViewController * _Nonnull)extendedController completionHandler:(id<XenchainScannerCallback> _Nonnull)completionHandler;

/**
 @brief Delegate function that must be called when scanning is completed.
 
 @param onBoardingID Refers to the onBoardingID passed from the `XenchainScannerCallback` protocol. The `onBoardingID` is to ensure that the SDK consider this particular transaction as fully consume for ID Scanning.
 @param metaCardResult Refers to the `metaCardResult` passed from the `XenchainScannerCallback` protocol.
 @param cardResult Refers to the `cardResult` passed from the `XenchainScannerCallback` protocol.
 @param scanConfig Refers to the existing `scanConfig` used in the `deployScanner` function
 @param completionHandler Completion Block or Completion Handler. Refer to `XenchainScannerCallback` Protocol for further information.
 
 @pre Requires `DeployScanner` to be executed with callbacks return.
 
 @warning Developers are required to call the `completeScanDeploymentV2` function. Otherwise for every scanning that is being done, the SDK will charged the scan.
 */
+ (void)completeScanDeploymentV2WithOnBoardingID:(NSString * _Nonnull)onBoardingID metaCardResult:(NSDictionary * _Nonnull)metaCardResult cardResult:(XCardResult * _Nonnull)cardResult scanConfig:(CardConfig * _Nonnull)scanConfig completionHandler:(id<XenchainScannerCallback> _Nonnull)completionHandler;

/**
 @brief Setup and provide View Controller of the Stand-Alone Hologram Scanner.

 @param scanConfig This variable carries the configuration of the Card/Passport that will be scanned. Refer to the `ScanConfig` class type for more information.
 @param inputController The UIViewController that is used to present the extended class of the `XHoloScannerV2ViewController`.
 @param extendedController The extended class of `XHoloScannerV2ViewController` that is used as UI Overlay for the Camera.
 @param completionHandler Completion Block or Completion Handler used for returning the results of ID Scanning. Refer to `XenchainScannerCallback` Protocol for further information.
 
 @pre Requires `InitSDK` function to be executed with callbacks return with success value.
 
 @warning Developers are required to call the `deployScannerV2` function. Otherwise, the OCR results provided will not be accurate.
 */
+ (void)deployHoloScannerV2:(CardConfig * _Nonnull)scanConfig inputController:(UIViewController * _Nonnull)inputController extendedController:(XHoloScannerV2ViewController * _Nonnull)extendedController completionHandler:(id<XenchainScannerV2Callback> _Nonnull)completionHandler;

+ (void)deployScannerV3:(CardConfig * _Nonnull)scanConfig inputController:(UIViewController * _Nonnull)inputController extendedController:(XScannerV3ViewController * _Nonnull)extendedController completionHandler:(id<XenchainScannerV2Callback> _Nonnull)completionHandler;

+ (void)completeScanDeploymentV3WithMetaCardResult:(NSDictionary * _Nonnull)metaCardResult scanResultDetails:(NSDictionary * _Nonnull)scanResultDetails scanConfig:(CardConfig * _Nonnull)scanConfig completionHandler:(id<XenchainScannerV2Callback> _Nonnull)completionHandler;

+ (void)uploadDebugImage:(NSString * _Nonnull)onBoardingID debugImage:(UIImage * _Nonnull)debugImage completionHandler:(void (^ _Nonnull)(NSString * _Nonnull imageRefID, NSString * _Nonnull error))completionHandler;

/**
 @brief Setup and provide Users with Face Match Features.
 
 @param onBoardingID Refers to the onBoardingID passed from the `XenchainScannerCallback` protocol. The `onBoardingID` is to ensure that the Face Match refers to this particular **Onboarding Transaction ID**.
 @param imageRef Refers to the `refFace` param value taken from `XenchainScannerCallback` protocol. This variable will be used for Face Matching purpose with the Face Image captured during the `DeployFaceMatch` process.
 @param inputController the UIViewController that is used to present the `extendedController` View Controller.
 @param extendedController The extended class of `XFaceViewController` that is used as UI Overlay for the Camera.
 @param completionHandler Completion Block or Completion Handler. Refer to `XenchainFaceCallback` Protocol for further information.
 
 @discussion This function includes Face Motion Checking, in which, the user will be asked to perform 2 motion, Blink Eyes & Smilling. For each motion, the camera will capture the Face. In this case, each captured Face will be compared against the `imageRef`.
 
 @pre Requires `DeployScanner` to be executed with callbacks return.
 
 @warning Please ensure that the `viewController` param is passed using current presented View Controller. Otherwise, the Camera View Controller will not shown.
 */
+ (void)DeployFaceMatch:(NSString * _Nonnull)onBoardingID imageRef:(NSString * _Nonnull)imageRef inputController:(UIViewController * _Nonnull)inputController extendedController:(XFaceViewController * _Nonnull)extendedController completionHandler:(id<XenchainFaceCallback> _Nonnull)completionHandler;

/**
 @brief Setup and provide Users with Face Match Record Features.
 
 @param onBoardingID Refers to the onBoardingID passed from the `XendityScannerCallback` protocol. The `onBoardingID` is to ensure that the Face Match refers to this particular **Onboarding Transaction ID**.
 @param inputController The UIViewController that is used to present the extended class of the `XenchainFaceRecordViewController`.
 @param extendedController The extended class of `XenchainFaceRecordViewController` that is used as UI Overlay for the Camera.
 @param completionHandler Completion Block or Completion Handler. Refer to `XendityFaceCallback` Protocol for further information.
 
 @discussion This function includes Face Motion Checking, in which, the user will be asked to record their own face. The camera will record a video that will be used to determine the liveness of the user as well as face comparison against the ID Card captured during the `DeployScanner` process.
 
 @pre Requires `DeployScanner` to be executed with callbacks return.
 
 @warning Please ensure that the `viewController` param is passed using current presented View Controller. Otherwise, the Camera View Controller will not shown.
 */
+(void) DeployFaceRecord:(NSString * _Nonnull)onBoardingID inputController:(UIViewController * _Nonnull)inputController extendedController:(XenchainFaceRecordViewController * _Nullable)extendedController completionHandler:(id<XenchainFaceCallback> _Nonnull)completionHandler;

/**
 @brief Setup and provide Users with Face Match Features.
 
 @param onBoardingID Refers to the onBoardingID passed from the `XenchainScannerCallback` protocol. The `onBoardingID` is to ensure that the Face Match refers to this particular **Onboarding Transaction ID**.
 @param cropImageRef Refers to the `refFace` param value taken from `XenchainScannerCallback` protocol. This variable will be used for Face Matching purpose with the `faceImage` variable.
 @param faceImage The image that will be used for comparison against `cropImageRef`.
 @param completionHandler Completion Block or Completion Handler. Refer to `XenchainFaceCallback` Protocol for further information.
 
 @discussion This function is used in situation where Face Motion is not required. It will compare the `faceImage` against the `cropImageRef`.
 
 @pre Requires `DeployScanner` to be executed with callbacks return.
 */
+(void) ProcessFaceMatch:(NSString * _Nonnull)onBoardingID cropImageRef:(NSString * _Nonnull)cropImageRef faceImage:(UIImage * _Nonnull)faceImage completionHandler:(id<XenchainFaceCallback> _Nonnull)completionHandler;

/**
 @brief Setup and provide Users with Face Match Features for Advance AI.
 
 @param onBoardingID Refers to the onBoardingID passed from the `XenchainScannerCallback` protocol. The `onBoardingID` is to ensure that the Face Match refers to this particular **Onboarding Transaction ID**.
 @param cropImageRef Refers to the `refFace` param value taken from `XenchainScannerCallback` protocol. This variable will be used for Face Matching purpose with the `faceImage` variable.
 @param faceImage The image that will be used for comparison against `cropImageRef`.
 @param completionHandler Completion Block or Completion Handler. Refer to `XenchainFaceCallback` Protocol for further information.
 
 @discussion This function is used in situation where Face Motion is not required. It will compare the `faceImage` against the `cropImageRef`.
 
 @pre Requires `DeployScanner` to be executed with callbacks return.
 */

+(void) ProcessFaceMatch2:(NSString * _Nonnull)onBoardingID cropImageRef:(NSString * _Nonnull)cropImageRef faceImage:(UIImage * _Nonnull)faceImage completionHandler:(id<XenchainFaceCallback> _Nonnull)completionHandler;

+(void) CommitFaceMatch:(NSString * _Nonnull)onBoardingID selfieImage:(UIImage * _Nonnull)selfieImage cropImageRef:(NSString * _Nonnull)cropImageRef isMatched:(bool)isMatched percentMatched:(double)percentMatched completionHandler:(id<XenchainFaceCallback> _Nonnull)completionHandler;

+(void) CommitGhostMatch:(NSString * _Nonnull)onBoardingID isMatched:(bool)isMatched percentMatched:(double)percentMatched completionHandler:(id<XenchainGhostCallback> _Nonnull)completionHandler;

/**
 @brief Setup and provide View Controller of the Barcode Scanner.
 
 @param onBoardingID Refers to the onBoardingID passed from the `XenchainScannerCallback` protocol. The `onBoardingID` is to ensure that the SDK consider this particular transaction as fully consume for ID Scanning.
 @param inputController The UIViewController that is used to present the extended class of the `XBarcodeViewController`.
 @param extendedController The extended class of `XBarcodeViewController` that is used as UI Overlay for the Camera.
 @param completionHandler Completion Block or Completion Handler used for returning the results of Barcode Scanning. Refer to `XenchainBarcodeCallback` Protocol for further information.
 
 @pre Requires `DeployScanner` to be executed with callbacks return.
 
 @warning Please ensure that the `viewController` param is passed using current presented View Controller. Otherwise, the Camera View Controller will not shown.
 */
+(void) DeployBarcodeScanner:(NSString * _Nonnull)onBoardingID inputController:(UIViewController * _Nonnull)inputController extendedController:(XBarcodeViewController * _Nonnull)extendedController completionHandler:(id<XenchainBarcodeCallback> _Nonnull)completionHandler;

/**
 @brief Setup and provide View Controller of the Signature Capture Feature.
 
 @param onBoardingID Refers to the onBoardingID passed from the `XenchainScannerCallback` protocol. The `onBoardingID` is to ensure that the SDK consider this particular transaction as fully consume for ID Scanning.
 @param inputController The UIViewController that is used to present the extended class of the `XSignatureViewController`.
 @param extendedController The extended class of `XSignatureViewController` that is used as UI Overlay for the Signature Capture ViewController.
 @param completionHandler Completion Block or Completion Handler used for returning the results of processing the Signature. Refer to `XSignatureViewController` Protocol for further information.
 
 @pre Requires `DeployScanner` to be executed with callbacks return.
 
 @warning Please ensure that the `viewController` param is passed using current presented View Controller. Otherwise, the Signature View Controller will not shown.
 */
+(void) DeploySignatureProcess:(NSString * _Nonnull)onBoardingID inputController:(UIViewController * _Nonnull)inputController extendedController:(XSignatureViewController * _Nonnull)extendedController completionHandler:(id<XenchainSignatureCallback> _Nonnull)completionHandler;

/**
 @brief Provide ID Verification for User's ID Checking & Verification.
 
 @param onBoardingID Refers to the onBoardingID passed from the `XenchainScannerCallback` protocol. The `onBoardingID` is to ensure that the SDK consider this particular transaction as fully consume for ID Scanning.
 @param completionHandler Completion Block or Completion Handler used for returning the results of processing the User's ID. Refer to `XenchainKBACallback` Protocol for further information.
 
 @pre Requires `DeployScanner` & either `DeployFaceMatch` or `ProcessFaceMatch` to be executed with callbacks return.
 */
+(void) IDVerificationForOnBoarding:(NSString * _Nonnull)onBoardingID completionHandler:(id<XenchainKBACallback> _Nonnull)completionHandler;

/**
@brief Provide ID Verification for User's ID Checking & Verification, including Phone No.

@param onBoardingID Refers to the onBoardingID passed from the `XenchainScannerCallback` protocol. The `onBoardingID` is to ensure that the SDK consider this particular transaction as fully consume for ID Scanning.
@param phoneNo Refers to the Phone No that will be used for Checking & Verification.
@param completionHandler Completion Block or Completion Handler used for returning the results of processing the User's ID. Refer to `XenchainKBACallback` Protocol for further information.

@pre Requires `DeployScanner` & either `DeployFaceMatch` or `ProcessFaceMatch` to be executed with callbacks return.
*/
+(void) IDVerificationForOnBoarding:(NSString * _Nonnull)onBoardingID phoneNo:(NSString * _Nonnull)phoneNo completionHandler:(id<XenchainKBACallback> _Nonnull)completionHandler;

/**
 @brief Request KBA Questions for knowledge based authentication of the user.
 
 @param onBoardingID Refers to the onBoardingID passed from the `XenchainScannerCallback` protocol. The `onBoardingID` is to ensure that the SDK consider this particular transaction as fully consume for ID Scanning.
 @param referenceNo Refers to the `ref_no` of the `profileObject` returned from `XenchainKBACallback` protocol.
 @param completionHandler Completion Block or Completion Handler used for returning the results of KBA request. Refer to `XenchainKBACallback` Protocol for further information.
 
 @pre Requires `IDVerificationForOnBoarding` to be executed with callbacks return.
 */
+(void) RequestKBAForOnBoarding:(NSString * _Nonnull)onBoardingID referenceNo:(NSString * _Nonnull)referenceNo completionHandler:(id<XenchainKBACallback> _Nonnull)completionHandler;

/**
 @brief Verify the KBA answers submitted by the user.
 
 @param onBoardingID Refers to the onBoardingID passed from the `XenchainScannerCallback` protocol. The `onBoardingID` is to ensure that the SDK consider this particular transaction as fully consume for ID Scanning.
 @param referenceNo Refers to the `ref_no` of the `profileObject` returned from `XenchainKBACallback` protocol.
 @param answers The answers that user submitted for KBA.
 @param completionHandler Completion Block or Completion Handler used for returning the results of KBA verification. Refer to `XenchainKBACallback` Protocol for further information.
 
 @pre Requires `RequestKBAForOnBoarding` to be executed with callbacks return.
 */
+(void) VerifyKBAForOnBoarding:(NSString * _Nonnull)onBoardingID referenceNo:(NSString * _Nonnull)referenceNo answers:(NSArray<AnswerKBA *> * _Nonnull)answers completionHandler:(id<XenchainKBACallback> _Nonnull)completionHandler;

@end

NS_ASSUME_NONNULL_END
