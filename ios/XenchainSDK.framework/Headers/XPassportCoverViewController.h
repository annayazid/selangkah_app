//
//  XPassportCoverViewController.h
//  XPassportCoverViewController
//
//  Created by Chan Tak Zee on 17/08/2021.
//  Copyright © 2021 Xenchain. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XPreviewViewController, ImageCaptureListener;

NS_ASSUME_NONNULL_BEGIN
@interface XPassportCoverViewController : UIViewController  {
    BOOL safeToTakePicture;
}

/**
 @brief Function used for initiating Image Capture Process.
 
 @discussion Call this function to initiate Image Capture Process.
 
 @pre `setupCameraSession` function is required before calling this function.
 
 @post Once the image is captured, the Captured Image will be passed to `showPreview` function. If there is an issue during the capture, `processImageFailure` function will be called instead.
 */
- (void)capturePicture;

/**
 @brief Function used for passing issues during Image Capture Process.
 
 @discussion This function will be called if the Camera encounter any issues. Refer to the Debug Log for further information.
 */
- (void)processImageFailure:(NSString * _Nullable)errorMessage;

/**
 @brief Function used for passing the captured image obtained from the camera.
 
 @discussion The App Developer may consider to preview the image captured to be approved by the User before submit it for processing.
 */
- (void)showPreview:(UIImage * _Nonnull)previewImage;

/**
 @brief Function used to process the Captured Image.
 
 @discussion This function will processed the captured image and pass it to backend server.
 */
- (void)submitImageCapture;

@end

NS_ASSUME_NONNULL_END
