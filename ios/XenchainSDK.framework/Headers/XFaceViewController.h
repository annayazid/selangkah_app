//
//  XFaceViewController.h
//  XenchainCustomSDK
//
//  Created by Jovial  on 11/3/2019.
//  Copyright © 2019 Xenchain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "XenchainProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface XFaceViewController : UIViewController

@property (nonatomic, assign) id<XenchainFaceCallback> delegate;

-(void) setupDefaultUI;

-(void) startScanFaceFeature;
-(void) processingBlink:(float)progress;
-(void) hasSuccessBlink;
-(void) processingSmile:(float)progress;
-(void) hasSuccessSmile;
-(void) hasFailFaceMatch;

@end

NS_ASSUME_NONNULL_END
