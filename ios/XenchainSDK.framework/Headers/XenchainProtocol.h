//
//  XenchainProtocol.h
//  XenchainCustomSDK
//
//  Created by Jovial  on 8/3/2019.
//  Copyright © 2019 Xenchain. All rights reserved.
//

#ifndef XenchainProtocol_h
#define XenchainProtocol_h

#import <UIKit/UIKit.h>
#import "XCardResult.h"

/**
 @brief Scan Delegate for SDK Initialization.
 */
@protocol XenchainSDKCallback

/**
 @brief Delegate function to be called for SDK Initialization.
 
 @param status Represents whether the SDK is successfully initialize or not.
 @param message If the SDK was not able to initialize successfully, this variable will provide the general description of the issue.
 */
-(void) InitSDKStatus:(bool)status message:(NSString * _Nonnull)message;

@end

/**
 @brief Scan Delegate for ID Document Scanning.
 */
@protocol XenchainScannerCallback

/**
 @brief Delegate function to be called when the Scanner has process the ID Document.
 
 @param cardResult Any important attributes of the ID Document that is found during the scanning process will be returned in this parameter.
 @param referenceID The Parent transaction for every onboarding that is processed or created under this value.
 @param onBoardingID The Overal Transaction ID for the whole Onboarding Process. The value contained in this variable will be used to track User's onboarding journeys.
 @param errorMessage If no important attributes of the ID Document was found or there is issue during the processing of ID Document attributes, this variable will provide the general description of the issue.
 
 @warning Call the `CompleteScanDeployment` function if the `deployScanner` function once the User confirm that this function returns the correct information to the App or User. Otherwise for every scanning that is being done, the SDK will charged the scan.
 
 @note It is recommended that the App stores `onBoardingID` value in the event that the App will access the `deployFaceMatch` function on later date.
 */
-(void) ScanResults:(XCardResult * _Nullable)cardResult referenceID:(NSString * _Nonnull)referenceID onBoardingID:(NSString * _Nonnull)onBoardingID errorMessage:(NSString * _Nonnull)errorMessage;


/**
 @brief Delegate function to be called when the Scanner has determine that the ID Document passed hologram checks.
 
 @param cardResult Any important attributes of the ID Document that is found during the scanning process will be returned in this parameter.
 @param referenceID The Parent transaction for every onboarding that is processed or created under this value.
 @param onBoardingID The Overal Transaction ID for the whole Onboarding Process. The value contained in this variable will be used to track User's onboarding journeys.
 @param errorMessage If no important attributes of the ID Document was found or there is issue during the processing of ID Document attributes, this variable will provide the general description of the issue.
 
 @warning Call the `CompleteScanDeployment` function if the `deployScanner` function once the User confirm that this function returns the correct information to the App or User. Otherwise for every scanning that is being done, the SDK will charged the scan.
 
 @note It is recommended that the App stores `referenceID` & `onBoardingID` value in the event that the App will access the `deployFaceMatch` function on later date. In addition, the OCR Values obtained from this callback is not accurate. It is recommended to take the updated results from `ScanResults` callback instead.
 */
-(void) HologramScanResults:(XCardResult * _Nullable)cardResult referenceID:(NSString * _Nonnull)referenceID onBoardingID:(NSString * _Nonnull)onBoardingID errorMessage:(NSString * _Nonnull)errorMessage;

/**
 @brief Delegate function to be called when the Scanner has process the ID Document.
 
 @param imageResults Contains all the image results obtained during the scanning of ID. This includes the ID Image and/or Face Image.
 */
-(void) ScanImageResults:(NSDictionary * _Nullable)imageResults;

/**
 @brief Delegate function to be called when the Scanner has process the ID Document.
 
 @param metaRefResults Contains all the meta results obtained during the scanning of ID. This includes the reference ID of ID Image and/or Face Image.
 
 @note It is recommended that the App stores `refFace` value contained in the `metaRefResults` in the event that the App will access the `deployFaceMatch` function on later date.
 */
-(void) ScanMetaResults:(NSDictionary * _Nullable)metaRefResults;

/**
 @brief Delegate function to be called when the Scanner has process the ID Document.
 
 @param idFaceImage If the Scanner was able to grab the Face of the ID Document, this variable will returned the Face Image. Otherwise, it will returned nil.
 @param refFace The reference ID of the Face Image which must be passed to `deployFaceMatch` function for Face Comparison.
 @param idFrontImage If the Scanner was able to grab the whole image of ID Document, this variable will returned the ID Image. Otherwise, it will returned nil.
 @param refFrontID The reference ID of the ID Document Image.
 @param idBackImage Refers to the MyKad Logo of the Back ID Image. This is for Landmark check purpose and only available to MyKad Scanning.
 @param refBackID The reference ID of the Back ID Document Image.
 @param errorMessage If the Scanner was not able to grab even the ID Document Image, this variable will provide the general description of the issue.
 
 @note It is recommended that the App stores `refFace` value in the event that the App will access the `deployFaceMatch` function on later date.
 */
-(void) ScanMetaResults:(UIImage * _Nullable)idFaceImage refFace:(NSString * _Nonnull)refFace idFrontImage:(UIImage * _Nonnull)idFrontImage refFrontID:(NSString * _Nonnull)refFrontID idBackImage:(UIImage * _Nonnull)idBackImage refBackID:(NSString * _Nonnull)refBackID errorMessage:(NSString * _Nonnull)errorMessage;

/**
@brief Delegate function to be called when the Scanner has process the ID Document.

@param nameIsSimilar If the name is similar to the input of the `setPreDeployment`, then it will show as `true`.
@param documentNoIsSimilar If the document no is similar to the input of the `setPreDeployment`, then it will show as `true`.
@param frontBackIsSimilar If the document no of the Front ID card is similar to the document no for the Back ID Card, then it will show as `true`.
 
@warning This function will be called regardless  if `setPreDeployment` function is executed with callbacks return with success value. Developer may consider to ignore it if no initial values are submitted whether through `setPreDeployment` or Web-API call.
 
@note This function, at the moment, is only supported for MyKad scanning.
*/
-(void) ScanSimilarityResults:(bool)nameIsSimilar documentNoIsSimilar:(bool)documentNoIsSimilar frontBackIsSimilar:(bool)frontBackIsSimilar;

-(void) CompletePostDeploymentResults:(double)nameIsSimilarScore nameIsSimilar:(bool)nameIsSimilar documentNoIsSimilarScore:(double)documentNoIsSimilarScore documentNoIsSimilar:(bool)documentNoIsSimilar frontBackIsSimilarScore:(double)frontBackIsSimilarScore frontBackIsSimilar:(bool)frontBackIsSimilar errorMessage:(NSString * _Nonnull)errorMessage;

/**
 @brief Delegate function to be called when the Scanner has process the Landmark of ID Document.
 
 @param landmarkScores Shows the percentage score for each landmark check.
 
 @warning Kindly note that this delegate function will only be called if `CheckLandmark` options is set to `true`. `CheckLandmark` can be setup in `CardConfig` class and it's respective child class.
 */
-(void) ScanLandmarkInformation:(NSDictionary * _Nullable)landmarkScores;

/**
 @brief Delegate function to be called when the App called `CompleteScanDeployment`.
 
 @param status Represents whether the `CompleteScanDeployment` is successfully called or not.
 @param errorMessage If the SDK was not able to process the `CompleteScanDeployment`, this variable will provide the general description of the issue.
 */
-(void) ScanCompleteDeployment:(bool)status errorMessage:(NSString * _Nonnull)errorMessage;

@end

/**
 @brief Scan Delegate for ID Document Scanning.
 */
@protocol XenchainScannerV2Callback

/**
 @brief Delegate function to be called when the Scanner has process the ID Document.
 
 @param scanResultDetails Any important attributes of the ID Document that is found during the scanning process will be returned in this parameter. The `ReferenceID` & `OnBoardingID` will be returned inside this param as well
 @param errorMessage If no important attributes of the ID Document was found or there is issue during the processing of ID Document attributes, this variable will provide the general description of the issue.
 
 @warning Call the `CompleteScanDeployment` function if the `deployScanner` function once the User confirm that this function returns the correct information to the App or User. Otherwise for every scanning that is being done, the SDK will charged the scan.
 
 @note It is recommended that the App stores `ReferenceID` & `OnBoardingID` value in the event that the App will access the `deployFaceMatch` function on later date.
 */
-(void) ScanResults:(NSDictionary * _Nullable)scanResultDetails errorMessage:(NSString * _Nonnull)errorMessage;


/**
 @brief Delegate function to be called when the Scanner has determine that the ID Document passed hologram checks.
 
 @param holoResultDetails Any important attributes of the ID Document that is found during the scanning process will be returned in this parameter. The `ReferenceID` & `OnBoardingID` will be returned inside this param as well
 @param errorMessage If no important attributes of the ID Document was found or there is issue during the processing of ID Document attributes, this variable will provide the general description of the issue.

 @warning Call the `CompleteScanDeployment` function if the `deployScanner` function once the User confirm that this function returns the correct information to the App or User. Otherwise for every scanning that is being done, the SDK will charged the scan.
 
 @note It is recommended that the App stores `ReferenceID` & `OnBoardingID` value in the event that the App will access the `deployFaceMatch` function on later date. In addition, the OCR Values obtained from this callback is not accurate. It is recommended to take the updated results from `ScanResults` callback instead.
 */
-(void) HologramScanResults:(NSDictionary * _Nullable)holoResultDetails errorMessage:(NSString * _Nonnull)errorMessage;

/**
 @brief Delegate function to be called when the Scanner has process the ID Document.
 
 @param imageResults Contains all the image results obtained during the scanning of ID. This includes the ID Image and/or Face Image.
 */
-(void) ScanImageResults:(NSDictionary * _Nullable)imageResults;

/**
 @brief Delegate function to be called when the Scanner has process the ID Document.
 
 @param metaRefResults Contains all the meta results obtained during the scanning of ID. This includes the reference ID of ID Image and/or Face Image.
 
 @note It is recommended that the App stores `refFace` value contained in the `metaRefResults` in the event that the App will access the `deployFaceMatch` function on later date.
 */
-(void) ScanMetaResults:(NSDictionary * _Nullable)metaRefResults;

-(void) ScanIndicatorResults:(NSDictionary * _Nullable)indicatorResults;

/**
@brief Delegate function to be called when the Scanner has process the ID Document.

@param similarityResults // TO BE FILLED LATER
 
@warning This function will be called regardless  if `setPreDeployment` function is executed with callbacks return with success value. Developer may consider to ignore it if no initial values are submitted whether through `setPreDeployment` or Web-API call.
 
@note This function, at the moment, is only supported for MyKad scanning.
*/
-(void) ScanSimilarityResults:(NSDictionary * _Nullable)similarityResults;

/**
 @brief Delegate function to be called when the Scanner has process the Landmark of ID Document.
 
 @param landmarkScores Shows the percentage score for each landmark check.
 
 @warning Kindly note that this delegate function will only be called if `CheckLandmark` options is set to `true`. `CheckLandmark` can be setup in `CardConfig` class and it's respective child class.
 */
-(void) ScanLandmarkInformation:(NSDictionary * _Nullable)landmarkScores;

/**
 @brief Delegate function to be called when the App called `CompleteScanDeployment`.
 
 @param status Represents whether the `CompleteScanDeployment` is successfully called or not.
 @param errorMessage If the SDK was not able to process the `CompleteScanDeployment`, this variable will provide the general description of the issue.
 */
-(void) ScanCompleteDeployment:(bool)status errorMessage:(NSString * _Nonnull)errorMessage;

@end

@protocol XenchainPostDeployCallback

-(void) CompletePostDeploymentResults:(double)nameIsSimilarScore nameIsSimilar:(bool)nameIsSimilar documentNoIsSimilarScore:(double)documentNoIsSimilarScore documentNoIsSimilar:(bool)documentNoIsSimilar frontBackIsSimilarScore:(double)frontBackIsSimilarScore frontBackIsSimilar:(bool)frontBackIsSimilar errorMessage:(NSString * _Nonnull)errorMessage;

@end

@protocol XenchainGhostCallback

-(void) ScanLandmarkInformation:(NSDictionary * _Nullable)landmarkScores errorMessage:(NSString * _Nonnull)errorMessage;

@end

@protocol XenchainDebugCallback

-(void) DebugInformation:(UIImage * _Nullable)lastCapturedImage detailedErrorMessage:(NSString * _Nonnull)detailedErrorMessage;

@end

@protocol XenchainDetectionCallback

-(void) FaceDetectionResult:(UIImage * _Nullable)outputImage outputRef:(NSString * _Nonnull)outputRef error:(NSString * _Nonnull)error;

@end

/**
 @brief Scan Delegate for Face Matching.
 */
@protocol XenchainFaceCallback

/**
 @brief Delegate function to be called when the Scanner has processed the Selfie Image and matched it with ID Card.
 
 @param isMatched True for Matched Face, otherwise False.
 @param percentMatched Show the percentage of how matched is the ID Card face and the Uploaded Image Face.
 @param error Any error during the Face Match result will be passed in this variable.
 */
-(void) FaceMatchResult:(bool)isMatched percentMatched:(double)percentMatched error:(NSString * _Nonnull)error;

/**
 @brief Delegate function to be called when the Scanner has successfully processed the Selfie Image.
 
 @param outputImage Show the image captured for Selfie.
 @param outputRef The reference ID of the image captured for Selfie.
 */
-(void) FaceMatchMetaResult:(UIImage * _Nullable)outputImage outputRef:(NSString * _Nonnull)outputRef;

@end


/**
 @brief Scan delegate for Passport Cover Scanning.
 **/
@protocol XenchainImageCaptureCallback

/**
 @brief Delegate function to be called when the scanner has successfully processed captured image.
 
 @param onBoardingID The Overal Transaction ID for the whole Onboarding Process. The value contained in this variable will be used to track User's onboarding journeys.
 @param imageRefID The reference ID of the image captured for the image.
 @param image The image captured.
 @param errorMessage Error message returned from the server.
 */
- (void)captureResultWithOnBoardingID:(NSString * _Nonnull)onBoardingID imageRefID:(NSString * _Nonnull)imageRefID image:(UIImage * _Nullable)image errorMessage:(NSString * _Nonnull)errorMessage;
@end

/**
 @brief Scan Delegate for Barcode Scanning.
 */
@protocol XenchainBarcodeCallback

/**
 @brief Delegate function to be called when the Scanner has successfully processed Barcode Image.
 
 @param barcodeValue The value contained in the Barcode Image.
 @param barcodeImage The image of the barcode captured during the Barcode scanning.
 @param barcodeImageRef The reference ID of the image captured for Barcode scanning.
 @param error Any error during the Barcode scanning will be passed in this variable.
 */
-(void) BarcodeResult:(NSString * _Nonnull)barcodeValue barcodeImage:(UIImage * _Nullable)barcodeImage barcodeImageRef:(NSString * _Nonnull)barcodeImageRef error:(NSString * _Nonnull)error;

@end

/**
 @brief Delegate for Signature Capture.
 */

@protocol XenchainSignatureCallback

/**
 @brief Delegate function to be called when the SDK has successfully captured the Signature.
 
 @param signatureImage The image of the Signature captured.
 @param signatureImageRef The reference ID of the Signature image.
 @param error Any error during the Signature capture will be passed in this variable.
 */
-(void) SignatureResult:(UIImage * _Nullable)signatureImage signatureImageRef:(NSString * _Nonnull)signatureImageRef error:(NSString * _Nonnull)error;

@end

@protocol XenchainKBACallback

-(void) IDVerificationStatus:(int)status profileObject:(NSDictionary * _Nullable)profileObject errorMessage:(NSString * _Nonnull)errorMessage;
-(void) KBARequestStatus:(NSDictionary * _Nullable)questionObjects errorMessage:(NSString * _Nonnull)errorMessage;
-(void) KBAVerificationStatus:(int)totalCorrect isKBACheckPass:(bool)isKBACheckPass errorMessage:(NSString * _Nonnull)errorMessage;

@end

#endif /* XenchainProtocol_h */
