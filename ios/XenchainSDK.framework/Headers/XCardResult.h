//
//  XCardResult.h
//  XenchainSDK
//
//  Created by Jovial  on 11/4/2019.
//  Copyright © 2019 Xenchain. All rights reserved.
//

#ifndef XCardResult_h
#define XCardResult_h

#import <Foundation/Foundation.h>

/**
 @brief Class used for storing the results of ID Card Scanning.
 @note Depending on the type of ID card and/or MRZ, some attributes of this class might be empty due to lack of information contained in it.
 */
@interface XCardResult : NSObject

@property (nonatomic, strong) NSString * _Nonnull mDocumentType;
@property (nonatomic, strong) NSString * _Nonnull mDocumentNumber;
@property (nonatomic, strong) NSString * _Nonnull mCardNumber;
@property (nonatomic, strong) NSString * _Nonnull mArmyNumber;
@property (nonatomic, strong) NSString * _Nonnull mBackDocumentNumber;
@property (nonatomic, strong) NSString * _Nonnull mBackDocumentNumberOld;

@property (nonatomic, strong) NSString * _Nonnull mFrontName;
@property (nonatomic, strong) NSString * _Nonnull mFrontNameOneLine;
@property (nonatomic, strong) NSString * _Nonnull mBackName;

@property (nonatomic, strong) NSString * _Nonnull mBloodType;

@property (nonatomic, strong) NSString * _Nonnull mAddress;
@property (nonatomic, strong) NSString * _Nonnull mVillage;
@property (nonatomic, strong) NSString * _Nonnull mRTRW;
@property (nonatomic, strong) NSString * _Nonnull mDistrict;
@property (nonatomic, strong) NSString * _Nonnull mCity;
@property (nonatomic, strong) NSString * _Nonnull mProvince;
@property (nonatomic, strong) NSString * _Nonnull mState;
@property (nonatomic, strong) NSString * _Nonnull mPostCode;

@property (nonatomic, strong) NSString * _Nonnull mReligion;
@property (nonatomic, strong) NSString * _Nonnull mStatus;
@property (nonatomic, strong) NSString * _Nonnull mJob;
@property (nonatomic, strong) NSString * _Nonnull mGender;

@property (nonatomic, strong) NSString * _Nonnull mCitizenship;
@property (nonatomic, strong) NSString * _Nonnull mNationality;
@property (nonatomic, strong) NSString * _Nonnull mIssuingCountry;
@property (nonatomic, strong) NSString * _Nonnull mRace;
@property (nonatomic, strong) NSString * _Nonnull mDrivingLicenseClass;

@property (nonatomic, strong) NSString * _Nonnull mPlaceOfBirth;
@property (nonatomic, strong) NSString * _Nonnull mDateOfBirth;
@property (nonatomic, strong) NSString * _Nonnull mCountryOfBirth;
@property (nonatomic, strong) NSString * _Nonnull mFrontDateIssued;
@property (nonatomic, strong) NSString * _Nonnull mBackDateIssued;
@property (nonatomic, strong) NSString * _Nonnull mDateUpdated;
@property (nonatomic, strong) NSString * _Nonnull mFrontExpiry;
@property (nonatomic, strong) NSString * _Nonnull mBackExpiry;

@property (nonatomic, strong) NSString * _Nonnull mChipNumber;
@property (nonatomic, strong) NSString * _Nonnull mSerialNumber;

@property (nonatomic, strong) NSString * _Nonnull mEmployerInfo;
@property (nonatomic, strong) NSString * _Nonnull mSector;
@property (nonatomic, strong) NSString * _Nonnull mEmploymentOccupation;
@property (nonatomic, strong) NSString * _Nonnull mDateOfApplication;
@property (nonatomic, strong) NSString * _Nonnull mForeignIdentificationNo;

@property (nonatomic, strong) NSString * _Nonnull mFacultyInfo;
@property (nonatomic, strong) NSString * _Nonnull mFacultyCityZipcodeState;
@property (nonatomic, strong) NSString * _Nonnull mFacultyAddress;

-(instancetype _Nullable) initWithTempValue;
-(instancetype _Nullable) initWithDictionary:(NSDictionary * _Nonnull)inputDictionary;
-(void) filledWithTempValues;
-(NSString * _Nonnull) toString;
-(NSDictionary * _Nonnull) toDictionary;


@end

#endif /* XCardResult_h */
