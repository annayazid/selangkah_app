package com.example.selangkah_new.xenchain.dialog;

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.widget.ImageView
import com.example.selangkah_new.R
import com.xenchain.reader.Dialog.XCustomDialog

class LoadingScreen : XCustomDialog() {
    private var mLoadingDialog: Dialog? = null
    private fun createDialog(mContext: Context): Dialog {

        mLoadingDialog = Dialog(mContext)
        mLoadingDialog!!.setCancelable(false)
        if (mLoadingDialog!!.window != null) mLoadingDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val viewDensity = mContext.resources.displayMetrics.density

        var inflater: LayoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var loadingLayout=inflater.inflate(R.layout.dialog_layout, null)
        var imageView: ImageView? =loadingLayout?.findViewById(R.id.imageView)
        mLoadingDialog!!.setContentView(loadingLayout!!)
        val animationDrawable: AnimationDrawable = imageView?.getBackground() as AnimationDrawable
        if (!animationDrawable.isRunning()) {
            animationDrawable.start()
        }


        return mLoadingDialog!!
    }

    override fun showDialog(activity: Activity) {
        try {
            if (mLoadingDialog != null && mLoadingDialog!!.isShowing) dismissDialog()
            mLoadingDialog = createDialog(activity)
            if (!activity.isFinishing) mLoadingDialog!!.show()
        } catch (ex: Exception) {
            ex.printStackTrace()
            mLoadingDialog!!.dismiss()
            mLoadingDialog = null
        }
    }

    override fun dismissDialog() {
        try {
            if (mLoadingDialog != null) {
                mLoadingDialog!!.dismiss()
                mLoadingDialog = null
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            mLoadingDialog = null
        }
    }
}