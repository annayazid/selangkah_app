package com.example.selangkah_new.xenchain.modle;

import java.util.*

data class ChannelResult<T>(
        val status:String?=null,
        val data:T?=null,
        val success:Boolean?=null,
        val message:String?=null
)
