package com.example.selangkah_new.xenchain.utils

import android.app.Activity
import java.util.*


class MyActivityManager {
    fun add(activity: Activity?) {
        if (activity != null) {
            activityStack.add(activity)
        }
    }

    fun remove(activity: Activity?) {
        if (activity != null) {
            for (i in activityStack.size - 1 downTo 0) {
                val currentActivity: Activity = activityStack.get(i)
                if (currentActivity.javaClass.equals(activity.javaClass)) {
                    activityStack.removeAt(i)
                    currentActivity.finish()
                    break
                }
            }
        }
    }
    fun findActivity(cls: Class<*>):Activity? {
        if (cls != null) {
            for (i in activityStack.size - 1 downTo 0) {
                val currentActivity: Activity = activityStack.get(i)
                if (currentActivity.javaClass.equals(cls)) {
                    return currentActivity
                }
            }
        }
        return null
    }
    

    fun removeAll(activity: Activity) {
        for (i in activityStack.size - 1 downTo 0) {
            activity.finish()
            activityStack.removeAt(i)
        }
    }
    fun findTopActivity():Activity {
        return activityStack[activityStack.size - 1]
    }


    companion object {
        val activityManager = MyActivityManager()
        private val activityStack: Stack<Activity> = Stack()
    }
}
