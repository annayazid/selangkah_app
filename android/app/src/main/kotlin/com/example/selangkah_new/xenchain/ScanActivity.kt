package com.example.selangkah_new


import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.InsetDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Html
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.selangkah_new.xenchain.utils.DisplayUtil
import com.example.selangkah_new.xenchain.utils.MyActivityManager
import com.xenchain.reader.Activity.XScannerActivity
import java.util.*

class ScanActivity: XScannerActivity(),View.OnClickListener{

    private  var scanLayout:android.view.View? = null

    private  var txtHologramProgress:android.widget.TextView? = null
    private  var txtScanTime:android.widget.TextView? = null
    private var txtScanContent:TextView?=null
    private  var frontImageView:android.widget.ImageView? = null
    private  var backImageView:android.widget.ImageView? = null

    private var progressBottom:TextView?=null
    private var progressTop:TextView?=null
    private var tvStatusWords:TextView?=null
    private var tvTopNotice:TextView?=null
    private var tvTopNotice1:TextView?=null
    private var tvImageFront:TextView?=null
    private var relativeProgress:RelativeLayout?=null

    private var passportImageLeft:View?=null
    private var passportImageRight:View?=null
    private var passportTVLeft:View?=null
    private var passportTVRight:View?=null

    private var tvImageBack:View?=null

    private var timer: CountDownTimer? = null
    private var isHologram = false
    private val canBypass = false
//    private var mHologramPlayer: MediaPlayer? = null

    private var mMillisUntilFinished: Long = 0
    private var mHologramProgress = 0.0

    private val TAG = "CameraOCRActivity"
    private val HOLOGRAM_SCAN_TIME: Long = 120000
    private var hologramView: VideoView? = null


    private var view_start_1:View?=null
    private var view_start_2:View?=null
    private var view_scan_1:View?=null
    private var view_scan_2:View?=null
    private var tv_need_help:TextView?=null
    private var linear_help:LinearLayout?=null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                    or WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.setStatusBarColor(Color.TRANSPARENT)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val window: Window = window
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }

        MyActivityManager.activityManager.add(this)

        val vi = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v = vi.inflate(R.layout.activity_scan, null)
        addChildView(v)

        findViewById<View>(R.id.view_statusbar_height).layoutParams=LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, DisplayUtil.getStatusBarHeight(this))
        findViewById<View>(R.id.view_bottombar_height).layoutParams=LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, DisplayUtil.getBottomBarHeight(this))

        findViewById<View>(R.id.ocr_scanner_back_button).setOnClickListener(this)
        scanLayout = findViewById(R.id.ocr_scanner_scan_layout)
        txtHologramProgress = findViewById(R.id.ocr_scanner_hologram_progress_text)
        txtScanTime = findViewById(R.id.ocr_scanner_scan_time_text)
        frontImageView = findViewById(R.id.ocr_scanner_img_front)
        backImageView = findViewById(R.id.ocr_scanner_img_back)
        progressTop=findViewById(R.id.tv_progress_top)
        progressBottom=findViewById(R.id.tv_progress_bottom)
        tvStatusWords=findViewById(R.id.tv_status_words)
        tvTopNotice=findViewById(R.id.tv_top_notice)
        tvTopNotice1=findViewById(R.id.tv_top_notice1)
        relativeProgress=findViewById(R.id.relative_progress)

        passportImageLeft=findViewById(R.id.image_passport_left)
        passportImageRight=findViewById(R.id.image_passport_right)
        passportTVLeft=findViewById(R.id.tv_passport_left)
        passportTVRight=findViewById(R.id.tv_passport_right)
        tv_need_help=findViewById(R.id.tv_need_help)
        linear_help=findViewById(R.id.linear_help)

        tvImageBack=findViewById(R.id.tv_image_back)
        tvImageFront=findViewById(R.id.tv_image_front)
        txtScanContent=findViewById(R.id.ocr_scanner_scan_content_text)
        hologramView = findViewById<VideoView>(R.id.ocr_scanner_hologram_video_view)

        view_start_1=findViewById(R.id.view_start_1)
        view_start_2=findViewById(R.id.view_start_1)
        view_scan_1=findViewById(R.id.view_scan_1)
        view_scan_2=findViewById(R.id.view_scan_2)
        
        refreshTimer(HOLOGRAM_SCAN_TIME)

//        tv_need_help?.text= Html.fromHtml(getString(R.string.need_help_string))
        tv_need_help?.getPaint()?.setFlags(Paint.UNDERLINE_TEXT_FLAG);
        tv_need_help?.getPaint()?.setAntiAlias(true)

        tvTopNotice?.viewTreeObserver?.addOnGlobalLayoutListener(mOnGlobalLayoutListener);


    }
    private var hasDraw=false;
    var mOnGlobalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        if(!hasDraw){
            hasDraw=true;
            var parmars:RelativeLayout.LayoutParams=relativeProgress?.layoutParams as RelativeLayout.LayoutParams
            parmars.leftMargin=tvTopNotice?.left as Int
            relativeProgress?.layoutParams=parmars;
        }
    }

    override fun onDestroy() {
        stopTimer()
        super.onDestroy()
        MyActivityManager.activityManager.remove(this)
    }


    override fun startScanHologram() {
        isHologram = true
        setupHologramVideo()
    }

    fun setupHologramVideo() {
        view_scan_1!!.visibility = View.GONE
        view_scan_2!!.visibility = View.GONE
        view_start_1!!.visibility = View.VISIBLE
        view_start_2!!.visibility = View.VISIBLE
        hologramView?.visibility=View.VISIBLE
        scanLayout!!.visibility=View.GONE
        relativeProgress!!.visibility=View.VISIBLE

        hologramView!!.setZOrderOnTop(true)
        hologramView!!.setOnPreparedListener { mp -> mp.isLooping = true }

        val path = "android.resource://$packageName/raw/hologram_animation"
        hologramView!!.setVideoURI(Uri.parse(path))
        hologramView!!.start()

        txtHologramProgress!!.text = String.format(getString(R.string.percent_complete), "0%")
    }

    private var bottomWidth:Int?=0;
    private var bottomHeight:Int=0;
    private var topLayoutParm:ViewGroup.LayoutParams?=null
    override fun setHologramProgress(hologramProgress: Double) {
        if(bottomWidth==0){
            bottomWidth=progressBottom?.width;
            bottomHeight=progressBottom?.height as Int;
        }
        if (hologramProgress > mHologramProgress) {
            mHologramProgress = hologramProgress
        }
        topLayoutParm=RelativeLayout.LayoutParams((bottomWidth!!.toInt() * hologramProgress).toInt(), bottomHeight)
        progressTop?.layoutParams=topLayoutParm
        txtHologramProgress!!.text = String.format(getString(R.string.percent_complete), "${(hologramProgress * 100).toInt()}%")
    }

    override fun passHologramICCheck() {
//        hologramICView!!.setImageDrawable(resources.getDrawable(R.drawable.result_correct_icon))
    }

    override fun passHologramNameCheck() {
//        hologramNameView!!.setImageDrawable(resources.getDrawable(R.drawable.result_correct_icon))
    }

    override fun passHologramGhostCheck() {
//        hologramGhostView!!.setImageDrawable(resources.getDrawable(R.drawable.result_correct_icon))
    }

    override fun hasScanHologram() {
        isHologram = false
        setupFrontBackScanning()
    }

    override fun startScanFontSize() {
        setupFrontBackScanning()
//        txtInstruction?.setText(R.string.verifying_font)
    }


    override fun hasScanFontSize() {
    }
    override fun startScanFront() {
        setupFrontBackScanning()
        tvTopNotice?.setText(R.string.come_face_stage_front)
        tvTopNotice1?.setText(R.string.come_face_stage_front)
        tvStatusWords?.setText(R.string.notice_step_ic_2)
//        txtInstruction?.visibility=View.INVISIBLE
//        txtInstruction?.setText(R.string.scan_front)
//        linear_help?.visibility=View.VISIBLE
        if(MainActivity.currentScanType=="ekycScanPassport"){
//            linear_help?.visibility=View.GONE
            tvTopNotice?.setText(R.string.come_face_passport)
            tvTopNotice1?.setText(R.string.come_face_passport)
            tvTopNotice?.visibility=View.GONE
            tvTopNotice1?.visibility=View.VISIBLE
            tvStatusWords?.setText(R.string.scan_passport_stage)
            passportImageLeft?.visibility=View.VISIBLE
            passportImageRight?.visibility=View.VISIBLE
            passportTVLeft?.visibility=View.VISIBLE
            passportTVRight?.visibility=View.VISIBLE
            backImageView?.visibility=View.GONE
            tvImageBack?.visibility=View.GONE
            tvImageFront?.setText(R.string.first_page)
        }
    }


    fun setupFrontBackScanning() {
        hologramView!!.visibility = View.GONE
        scanLayout!!.visibility=View.VISIBLE
        relativeProgress!!.visibility=View.GONE

        view_scan_1!!.visibility = View.VISIBLE
        view_scan_2!!.visibility = View.VISIBLE
        view_start_1!!.visibility = View.GONE
        view_start_2!!.visibility = View.GONE

    }

    override fun hasScanFront(frontBitmap: Bitmap?) {
        MainActivity.frontBitmap=frontBitmap
        frontImageView!!.setImageBitmap(frontBitmap)
    }

    override fun startScanBack() {
        tvTopNotice?.setText(R.string.come_face_stage_back)
        tvTopNotice1?.setText(R.string.come_face_stage_back)
//        txtInstruction?.setText(R.string.scan_back)
    }

    override fun hasScanBack(backBitmap: Bitmap?) {
        backImageView!!.setImageBitmap(backBitmap)
        MainActivity.backBitmap=backBitmap
//        txtInstruction!!.setText(R.string.ScanCompleted)
        stopTimer()
    }

    override fun onClick(v: View?) {
        stopTimer()
        val activity=MyActivityManager?.activityManager?.findActivity(MainActivity::class.java)
        if(activity!=null&&activity is MainActivity){
            activity.callBackFlutter("close_page")
        }
        finish()
    }


    fun pauseTimer(){
        timer?.cancel()
    }

    fun startTimer(){
        timer?.start()
    }

    override fun onPause() {
        super.onPause()
        stopTimer();
    }

    override fun onResume() {
        super.onResume()
        if(mMillisUntilFinished>0)
            refreshTimer(mMillisUntilFinished)
    }

    fun refreshTimer(timeValue: Long) {
        if (timer != null) timer?.cancel()
        timer = object : CountDownTimer(timeValue, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                mMillisUntilFinished = millisUntilFinished
                Log.d(TAG, "Scan Time Remaining: $mMillisUntilFinished")
                txtScanTime!!.text = String.format(getString(R.string.time_remaining), (mMillisUntilFinished / 1000).toInt())
            }

            override fun onFinish() {
                timer?.cancel()
                if (isHologram && canBypass) {
                    isHologram = false
                    this@ScanActivity.bypassHologramScan()
                    return
                }
                val activityMain=MyActivityManager?.activityManager?.findActivity(MainActivity::class.java)
                if(activityMain is MainActivity){
                    if(activityMain?.dialogShowing){
                        return ;
                    }else{
                        activityMain?.showAlertDialog(MyActivityManager.activityManager.findTopActivity(), activityMain.EKYC_ALERT_DIALOG_OVERTIME, false)
                    }
                }
            }
        }.start()
    }

    fun stopTimer() {
        if (timer != null) timer?.cancel()
    }

    var dialogShowing:Boolean=false;
    private fun showAlertDialog(activity: Activity) {
        val activityMain=MyActivityManager?.activityManager?.findActivity(MainActivity::class.java)
        if(activityMain is MainActivity&&activityMain?.dialogShowing){
            return
        }
        val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout = inflater.inflate(R.layout.dialog_notice, findViewById(R.id.custom))
        val tv_content=layout.findViewById<TextView>(R.id.tv_content)
        val tv_button_01=layout.findViewById<TextView>(R.id.tv_button_01)
        val tv_button_02=layout.findViewById<TextView>(R.id.tv_button_02)
        

        tv_content.text=activity.resources.getString(R.string.time_limit_str)
        tv_button_01.visibility= View.VISIBLE
        tv_button_01.text=activity.resources.getString(R.string.cancel_str)
        tv_button_02.visibility= View.VISIBLE
        tv_button_02.text=activity.resources.getString(R.string.try_again_str)


        val customAlert = AlertDialog.Builder(activity)
        customAlert.setView(layout)
        customAlert.setCancelable(false)
        val alertDialog = customAlert.create()
        alertDialog.show()
        dialogShowing=true;
        alertDialog.setOnDismissListener {
            dialogShowing=false;
        }
        if(activity is ScanActivity){
            activity.pauseTimer()
        }else if(activity is FaceRecordActivity){
            activity.pauseTimer()
        }

        val background = Objects.requireNonNull<Window>(alertDialog.window).decorView.background as InsetDrawable
        background.alpha = 0

        tv_button_01.setOnClickListener {
            alertDialog.dismiss()
            activity.onBackPressed()
        }
        tv_button_02.setOnClickListener {
            alertDialog.dismiss()
            startTimer()
        }
    }
}