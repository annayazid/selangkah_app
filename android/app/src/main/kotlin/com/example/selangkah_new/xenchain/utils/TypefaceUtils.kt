package com.example.selangkah_new.xenchain.utils

import android.content.Context
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

object TypefaceUtils {
    fun updateKipleIcon(root: ViewGroup, context: Context) {
        val typeface: Typeface = Typeface.createFromAsset(context.resources.assets, "fonts/KipleIcon.ttf")
        changeTypeFace(typeface,root)
    }

    fun updateSofiaProBold(root: View, context: Context) {
        val typeface: Typeface = Typeface.createFromAsset(context.resources.assets, "fonts/SofiaProBold.otf")
        changeTypeFace(typeface,root)
    }
    fun updateSofiaProLight(root: View, context: Context) {
        val typeface: Typeface = Typeface.createFromAsset(context.resources.assets, "fonts/SofiaProLight.otf")
        changeTypeFace(typeface,root)
    }
    fun updateSofiaProMedium(root: View, context: Context) {
        val typeface: Typeface = Typeface.createFromAsset(context.resources.assets, "fonts/SofiaProMedium.otf")
        changeTypeFace(typeface,root)
    }
    fun updateSofiaProRegular(root: View, context: Context) {
        val typeface: Typeface = Typeface.createFromAsset(context.resources.assets, "fonts/SofiaProRegular.otf")
        changeTypeFace(typeface,root)
    }
    fun updateSofiaProSemiBold(root: View, context: Context) {
        val typeface: Typeface = Typeface.createFromAsset(context.resources.assets, "fonts/SofiaProSemiBold.otf")
        changeTypeFace(typeface,root)
    }

    fun changeTypeFace(typeface: Typeface,child:View){
        if(typeface==null){
            return
        }
        if (child is TextView) {
            (child as TextView).setTypeface(typeface)
        } else if (child is Button) {
            (child as Button).setTypeface(typeface)
        } else if (child is EditText) {
            (child as EditText).setTypeface(typeface)
        }


    }
}