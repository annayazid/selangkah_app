package com.example.selangkah_new.xenchain.utils

import com.xenchain.reader.Model.ScanConfig.MyKadConfig

import com.xenchain.reader.Model.ScanConfig.PassportConfig

object XendityUtils {

    fun getPassportConfig(): PassportConfig? {
        var passportConfig=PassportConfig()
        return passportConfig
    }

    fun getMyKadConfig(): MyKadConfig? {
        val kadConfig = MyKadConfig()
        kadConfig.isScanDefaultConfig = true
        kadConfig.isCheckLandmark = true
        kadConfig.isCheckHologram = true
        kadConfig.isCheckHoloSwap = false
        kadConfig.isCheckFontSize = false
        kadConfig.isScanFrontBack = true
        kadConfig.isScanCitizen = false
        kadConfig.isScanReligion = false
        kadConfig.isScanGender = false
        kadConfig.isScanOldICNumber = false
        kadConfig.isRejectOldIC = false
        kadConfig.stableFrame = 1
        kadConfig.isCheckHoloMaterial=false
        return kadConfig
    }
    
    
}