package com.example.selangkah_new.xenchain.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.example.selangkah_new.R;


public final class ViewCircleView extends View {


    private int ScreenRate;

    //手机的屏幕密度
    private static float density;


    private Paint paint;

    private final int maskColor;
    private final int resultColor;
    
    boolean isFirst;
    int borderLeft=0;
    int borderTop=0;
    private Context context;

    public ViewCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        density = context.getResources().getDisplayMetrics().density;
        ScreenRate = (int) (20 * density);
        borderLeft=(int) (45 * density);
        borderTop=(int) (25 * density);

        paint = new Paint();
        Resources resources = getResources();
        maskColor = resources.getColor(R.color.color_translucent_50);
        resultColor = resources.getColor(R.color.colorPrimary);


    }


    int scanBorderHeight=700;
    @Override
    protected void onDraw(Canvas canvas) {
        //center scan rect
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        Rect frame = new Rect(ScreenRate, (height-scanBorderHeight)/2, width-ScreenRate, height-(height-scanBorderHeight)/2);
        if (frame == null) {
            return;
        }

        //init top and bottom
        if (!isFirst) {
            isFirst = true;
        }

        int layerID = canvas.saveLayer(0,0,width,height,paint,Canvas.ALL_SAVE_FLAG);
        //draw sha
        paint.setColor(maskColor);
        canvas.drawRect(0, 0, width, height, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT));
        RectF oval = new RectF(borderLeft, borderTop, width-borderLeft, height-borderTop);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        canvas.drawOval(oval, paint);

        paint.setXfermode(null);
        paint.setColor(resultColor);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        canvas.drawOval(oval, paint);

        canvas.restoreToCount(layerID);

    }


    private Bitmap getBitmapByBitmap(int drawableId, float width, float height) {
        int simpleSize = 1 ;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), drawableId,options);
        while ((options.outWidth / simpleSize > width) || (options.outHeight / simpleSize > height)) {
            simpleSize *= 2;
        }
        options.inJustDecodeBounds = false;
        options.inSampleSize = simpleSize;
        return BitmapFactory.decodeResource(context.getResources(), drawableId);
    }
}