package com.example.selangkah_new

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.InsetDrawable
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.selangkah_new.xenchain.utils.DisplayUtil
import com.example.selangkah_new.xenchain.utils.MyActivityManager
import com.google.gson.Gson
import com.xenchain.reader.Activity.XFaceRecordActivity
import java.util.*

class FaceRecordActivity: XFaceRecordActivity() {

    private var timer: CountDownTimer? = null
    private var mMillisUntilFinished: Long = 0
    private var tvTimerNumber:TextView?=null
    private val HOLOGRAM_SCAN_TIME: Long = 60000
    private var tv_button:TextView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                    or WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.setStatusBarColor(Color.TRANSPARENT)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val window: Window = window
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }

        MyActivityManager.activityManager.add(this)

        val vi = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v: View = vi.inflate(R.layout.activity_face_record, null)

        v.findViewById<View>(R.id.view_statusbar_height).layoutParams=LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, DisplayUtil.getStatusBarHeight(this))
        v.findViewById<View>(R.id.view_bottombar_height).layoutParams=LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,DisplayUtil.getBottomBarHeight(this))
        v.findViewById<View>(R.id.liveness_record_1_back_button).setOnClickListener(this)
        v.findViewById<View>(R.id.tv_button).setOnClickListener(this)
        tv_button=v.findViewById<TextView>(R.id.tv_button)
        tv_button?.setOnClickListener(this)
        tvTimerNumber=v.findViewById(R.id.tv_timer_number)
        setupCustomScanner(v)
    }

    override fun onDestroy() {
        if(alertDialog!=null&&alertDialog?.isShowing!!){
            alertDialog?.dismiss()
        }
        if(timer!=null){
            timer?.cancel()
        }
        super.onDestroy()
        MyActivityManager.activityManager.remove(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.liveness_record_1_back_button) {
            dismissVideoRecord()
            return
        }
        if (v.id == R.id.tv_button) {
            startVideoRecord()
        }
    }

    override fun preBlinkDetection() {}

    override fun blinkDetected() {}

    override fun smileDetected() {}

    override fun startRecording() {
        tv_button?.visibility=View.INVISIBLE
        refreshTimer(HOLOGRAM_SCAN_TIME)
//        faceInstructionText?.setText(R.string.recording)
    }

    override fun processingRecording() {
//        faceInstructionText?.setText(R.string.process_record)
    }

    override fun completingProcess() {
//        faceInstructionText?.setText(R.string.complete_process_record)
    }



    fun refreshTimer(timeValue: Long) {
        if (timer != null) timer?.cancel()
        timer = object : CountDownTimer(timeValue, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                mMillisUntilFinished = millisUntilFinished
                tvTimerNumber!!.text = String.format(getString(R.string.time_remaining), (mMillisUntilFinished / 1000).toInt())
            }

            override fun onFinish() {
                timer?.cancel()
                val activityMain=MyActivityManager?.activityManager?.findActivity(MainActivity::class.java)
                if(activityMain is MainActivity){
                    if(activityMain?.dialogShowing){
                        return ;
                    }else{
                        activityMain?.showAlertDialog(MyActivityManager.activityManager.findTopActivity(),activityMain.EKYC_ALERT_DIALOG_OVERTIME,false)
                    }
                }
//                showAlertDialog(this@FaceRecordActivity)
            }
        }.start()
    }

    fun stopTimer() {
        if (timer != null) timer?.cancel()
    }

    fun pauseTimer(){
        timer?.cancel()
    }

    fun startTimer(){
        timer?.start()
    }

    var alertDialog:AlertDialog?=null
    var dialogShowing:Boolean=false;
    private fun showAlertDialog(activity: Activity) {
        val activityMain=MyActivityManager?.activityManager?.findActivity(MainActivity::class.java)
        if(activityMain is ScanActivity&&activityMain?.dialogShowing){
            return
        }
        val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout = inflater.inflate(R.layout.dialog_notice, findViewById(R.id.custom))
        val tv_content=layout.findViewById<TextView>(R.id.tv_content)
        val tv_button_01=layout.findViewById<TextView>(R.id.tv_button_01)
        val tv_button_02=layout.findViewById<TextView>(R.id.tv_button_02)
        

        tv_content.text=activity.resources.getString(R.string.time_limit_str)
        tv_button_01.visibility= View.VISIBLE
        tv_button_01.text=activity.resources.getString(R.string.cancel_str)
        tv_button_02.visibility= View.VISIBLE
        tv_button_02.text=activity.resources.getString(R.string.try_again_str)


        val customAlert = AlertDialog.Builder(activity)
        customAlert.setView(layout)
        customAlert.setCancelable(false)
        alertDialog = customAlert.create()
        if(this!=null){
            alertDialog?.show()
            dialogShowing=true;
            customAlert.setOnDismissListener {
                dialogShowing=false;
            }
            if(activity is ScanActivity){
                activity.pauseTimer()
            }else if(activity is FaceRecordActivity){
                activity.pauseTimer()
            }

            val background = Objects.requireNonNull<Window>(alertDialog?.window).decorView.background as InsetDrawable
            background.alpha = 0

            tv_button_01.setOnClickListener {
                alertDialog?.dismiss()
                activity.onBackPressed()
            }
            tv_button_02.setOnClickListener {
                alertDialog?.dismiss()
                startTimer()
            }
        }
    }
}