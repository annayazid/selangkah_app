package com.example.selangkah_new.xenchain.utils;

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics
import android.util.TypedValue

object DisplayUtil {
    var screenWidthPx = 0
    var screenhightPx = 0
    var density = 0f
    var densityDPI = 0
    var screenWidthDip = 0f
    var screenHightDip = 0f

    /**
     * @param context
     * @return
     */
    fun getStatusBarHeight(context: Context): Int {
        val resources: Resources = context.resources
        val resourceId: Int = resources.getIdentifier("status_bar_height", "dimen", "android")
        var height:Int=resources.getDimensionPixelSize(resourceId)
        var statusBarHeight1 = -1
        if (resourceId > 0) {
            //根据资源ID获取响应的尺寸值
            statusBarHeight1 = resources.getDimensionPixelSize(resourceId)
        }else{
            statusBarHeight1= dp2px(context, 10)
        }
        return statusBarHeight1
    }

    /**
     * @param context
     * @return
     */
    fun getBottomBarHeight(context: Context): Int {
        var resourceId = 0
        val rid = context.resources.getIdentifier("config_showNavigationBar", "bool", "android")
        return if (rid != 0) {
            resourceId = context.resources.getIdentifier("navigation_bar_height", "dimen", "android")
            context.resources.getDimensionPixelSize(resourceId)
        } else dp2px(context, 15)
    }


    /** get screen height  */
    fun getWindowsHeight(activity: Activity): Int {
        val dm = DisplayMetrics()
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm)
        return dm.heightPixels
    }

    /** get screen width  */
    fun getWindowsWidth(activity: Activity): Int {
        val dm = DisplayMetrics()
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm)
        return dm.widthPixels
    }

    /**
     * dp to px
     */
    fun dip2px(context: Context, dpValue: Int): Int {
        val scale: Float = context.getResources().getDisplayMetrics().density
        return (dpValue * scale + 0.5f).toInt()
    }

    /**
     * px to dp
     */
    fun px2dip(context: Context, pxValue: Float): Int {
        val scale: Float = context.getResources().getDisplayMetrics().density
        return (pxValue / scale + 0.5f).toInt()
    }

    // px to sp
    fun px2sp(context: Context, pxValue: Float): Int {
        val fontScale: Float = context.getResources().getDisplayMetrics().scaledDensity
        return (pxValue / fontScale + 0.5f).toInt()
    }

    fun dip2px2(context: Context, dpValue: Float): Int {
        val scale: Float = context.getResources().getDisplayMetrics().density
        return (dpValue * scale + 0.5f).toInt()
    }

    fun dp2px(context: Context, dp: Int): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(),
                context.getResources().getDisplayMetrics()).toInt()
    }

    /**
     * sp to px
     */
    fun sp2px(context: Context, spValue: Float): Int {
        val fontScale: Float = context.getResources().getDisplayMetrics().scaledDensity
        return (spValue * fontScale + 0.5f).toInt()
    }
}