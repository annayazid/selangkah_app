package com.example.selangkah_new

import io.flutter.embedding.android.FlutterActivity

import android.Manifest
import android.app.Activity
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.drawable.InsetDrawable
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.selangkah_new.xenchain.dialog.LoadingScreen
import com.example.selangkah_new.xenchain.modle.ChannelResult
import com.example.selangkah_new.xenchain.utils.FileUtils
import com.example.selangkah_new.xenchain.utils.MyActivityManager
import com.example.selangkah_new.xenchain.utils.XendityUtils
import com.google.gson.Gson
import com.xenchain.reader.Callback.XenchainFaceCallback
import com.xenchain.reader.Callback.XenchainSDKCallback
import com.xenchain.reader.Callback.XenchainScannerCallback
import com.xenchain.reader.Model.ScanConfig.MyKadConfig
import com.xenchain.reader.Model.ScanConfig.PassportConfig
import com.xenchain.reader.Model.XCardResult
import com.xenchain.reader.XenchainSDK
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import io.flutter.embedding.android.FlutterFragmentActivity
import com.google.firebase.FirebaseApp

class MainActivity : FlutterFragmentActivity(), XenchainSDKCallback, XenchainScannerCallback,
    XenchainFaceCallback {

    private val CHANNEL = "flutter.native/chatHelper"
    private val EKYC_SCAN_ID_CARD = "kiplePay.flutter.ekyc";
    private val PLATFROM_CHANNEL = "kiplePay.flutter.platform";

    //staging
    //  private var apiKey = "LEa-J5KjWZsUus4b8UzJm8hLQh3vyUkv"
    //  private var apiURL = "https://uat-kiple-ekyca.xendity.com"

    //release
    private var apiKey = "LQSzGAbY9l86xwdAtqIBtnL46Ta1lVMa"
    private var apiURL = "https://kiple-ekyca.xendity.com"

    private val mOCRPreview = false

    private val PERMISSION_REQUEST_ID = 100
    private var currentResult: MethodChannel.Result? = null
    private var currentMethodCall: MethodCall? = null
    private var sdkInitResult: Boolean = false
    private var canStartScanning: Boolean = false

    private var mReferenceID = ""
    private var mCardResults: XCardResult? = null
    private var mOnBoardingID = ""

    private var kadConfig: MyKadConfig? = null
    private var passportConfig: PassportConfig? = null

    private var mFaceImageRef = ""
    private var mMetaCardResults: JSONObject? = null

    companion object {
        var currentScanType: String? = null
        var frontBitmap: Bitmap? = null
        var backBitmap: Bitmap? = null
    }

    val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MyActivityManager.activityManager.add(this)
        initNotificationChannel();

        try {
            FirebaseApp.initializeApp(this)
        } catch (e: Exception) {

        }
    }

    fun initNotificationChannel() {
        var notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager;
        var channel: NotificationChannel;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            channel = NotificationChannel(
                "sel.main.selangkah",
                "sel.main.selangkah",
                NotificationManager.IMPORTANCE_HIGH
            );
            notificationManager.createNotificationChannel(channel);
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        MyActivityManager.activityManager.remove(this)
    }

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);
        MethodChannel(
            flutterEngine.dartExecutor.binaryMessenger,
            EKYC_SCAN_ID_CARD
        ).setMethodCallHandler { call, result ->
            currentResult = result
            currentMethodCall = call
            dealMethodChannel(result, call)
        }

    }


    var language: String? = null;
    fun dealMethodChannel(result: MethodChannel.Result?, call: MethodCall?) {
        if (call?.method == "initEkyc") {
            language = call?.argument<String>("language")
            runOnUiThread {
                initSdk()
                if (language != null && !"".equals(language)) {
                    changeAppConfig(language!!);
                }
            }
        } else if (call?.method == "ekycScanId") {
            currentScanType = call?.method
            runOnUiThread {
                if (sdkInitResult) {
                    if (canStartScanning) {
                        if (kadConfig == null) {
                            kadConfig = XendityUtils.getMyKadConfig()
                        }
                        XenchainSDK.deployScanner(
                            kadConfig,
                            this@MainActivity,
                            ScanActivity::class.java,
                            this@MainActivity
                        )
                    } else {
                        checkAndRequestPermission(this@MainActivity, PERMISSION_REQUEST_ID)
                    }
                }
            }
        } else if (call?.method == "ekycScanPassport") {
            currentScanType = call?.method
            runOnUiThread {
                if (sdkInitResult) {
                    if (canStartScanning) {
                        if (passportConfig == null) {
                            passportConfig = XendityUtils.getPassportConfig()
                        }
                        XenchainSDK.deployScanner(
                            passportConfig,
                            this@MainActivity,
                            ScanActivity::class.java,
                            this@MainActivity
                        )
                    } else {
                        checkAndRequestPermission(this@MainActivity, PERMISSION_REQUEST_ID)
                    }
                }
            }
        } else if (call?.method == "ekycFaceRecord") {
            currentScanType = call?.method
            runOnUiThread {
                if (sdkInitResult) {
                    if (canStartScanning) {
                        XenchainSDK.deployFaceRecord(
                            mOnBoardingID,
                            this,
                            FaceRecordActivity::class.java,
                            this
                        )
                    } else {
                        checkAndRequestPermission(this@MainActivity, PERMISSION_REQUEST_ID)
                    }
                }
            }
        } else if (call?.method == "ekycAlert") {
            val type: String? = call?.argument<String>("type")
            showAlertDialog(MyActivityManager.activityManager.findTopActivity(), type, true)
        } else if (call?.method == "ekycTryAgain") {
            val activity = MyActivityManager.activityManager.findTopActivity()
            if (activity is ScanActivity) {
                activity.startTimer()
            } else if (activity is FaceRecordActivity) {
                activity.startTimer()
            }
        } else if (call?.method == "ekycGetScanImages") {
            if (currentResult != null && imageUrlList?.size!! > 0) {
                val gson: Gson = Gson()
                val resultStr: String =
                    gson.toJson(ChannelResult<List<String>>(IMAGE_LIST, imageUrlList, true, ""))
                currentResult?.success(resultStr)
            }
        } else if (call?.method == "ekycGetFaceImages") {
            if (currentResult != null && faceImageList?.size!! > 0) {
                val gson: Gson = Gson()
                val resultStr: String =
                    gson.toJson(ChannelResult<List<String>>(IMAGE_FACE, faceImageList, true, ""))
                currentResult?.success(resultStr)
            }
        } else {
            result?.notImplemented()
        }
    }

    fun initSdk() {
        XenchainSDK.SDKLoadingDialog = LoadingScreen()
        XenchainSDK.initSDK(apiKey, apiURL, this, true, this)
    }

    private val INIT_SDK = "init_sdk";
    private val IC_RESULT = "ic";
    private val PASSPORT_RESULT = "passport";
    private val FACE_RECORD = "face_record";
    private val TRY_AGAIN = "try_again";
    private val TRY_CANCEL = "try_cancel";
    private val IMAGE_LIST = "image_list"
    private val IMAGE_FACE = "image_face"
    private val CLOSE_PAGE = "close_page"

    override fun InitSDKStatus(status: Boolean, message: String?) {
        sdkInitResult = status
        if (currentResult != null) {
            val gson: Gson = Gson()
            val resultStr: String =
                gson.toJson(ChannelResult<Boolean>(INIT_SDK, status, status, message))
            currentResult?.success(resultStr)
        }
        if (status) {
            XenchainSDK.setPreviewOCR(mOCRPreview)
            return
        }
    }

    fun checkAndRequestPermission(inputActivity: Activity?, requestID: Int): Boolean {
        val listPermissions = ArrayList<String>()
        listPermissions.add(Manifest.permission.CAMERA)
        if (Build.VERSION.SDK_INT < 33) {
            listPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            listPermissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        listPermissions.add(Manifest.permission.ACCESS_WIFI_STATE)
        listPermissions.add(Manifest.permission.INTERNET)
        val requestedPermissions = ArrayList<String>()
        for (permission in listPermissions) {
            if (ContextCompat.checkSelfPermission(
                    inputActivity!!,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) requestedPermissions.add(permission)
        }
        if (!requestedPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(
                inputActivity!!,
                requestedPermissions.toTypedArray(),
                requestID
            )
            return false
        }
        canStartScanning = true
        dealMethodChannel(currentResult, currentMethodCall)
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_ID && grantResults.size > 0) {
            for (grantedItem in grantResults) {
                if (grantedItem != PackageManager.PERMISSION_GRANTED) {
                    checkAndRequestPermission(this, PERMISSION_REQUEST_ID)
                    return
                }
            }
            canStartScanning = true
            dealMethodChannel(currentResult, currentMethodCall)
        }
    }

    override fun scanResults(
        cardResult: XCardResult,
        referenceID: String,
        onBoardingID: String,
        errorMessage: String
    ) {
        if (errorMessage.isEmpty()) {
            val fields = cardResult.javaClass.declaredFields
            val resultBuilder = StringBuilder()
            var jsonMap = HashMap<String, Any>()
            jsonMap.put("mReferenceID", referenceID)
            jsonMap.put("mOnBoardingID", onBoardingID)
            for (item in fields) {
                try {
                    if (item[cardResult] != null && !item[cardResult].toString().isEmpty()) {
                        resultBuilder.append(
                            String.format(
                                "%s => %s\n",
                                item.name,
                                item[cardResult]
                            )
                        )
                        jsonMap.put(item.name, item[cardResult])
                    }
                } catch (e: IllegalAccessException) {
                    e.printStackTrace()
                }
            }
            var gson: Gson = Gson();
            val resultString: String = gson.toJson(jsonMap)
            mCardResults = cardResult
            mReferenceID = referenceID
            mOnBoardingID = onBoardingID
            XenchainSDK.setReferenceID(mReferenceID)
            if (currentResult != null) {
                if (currentMethodCall?.method == "ekycScanPassport") {
                    currentResult?.success(
                        gson.toJson(
                            ChannelResult<HashMap<String, Any>>(
                                PASSPORT_RESULT,
                                jsonMap,
                                true,
                                errorMessage
                            )
                        )
                    )
                } else if (currentMethodCall?.method == "ekycScanId") {
                    currentResult?.success(
                        gson.toJson(
                            ChannelResult<HashMap<String, Any>>(
                                IC_RESULT,
                                jsonMap,
                                true,
                                errorMessage
                            )
                        )
                    )
                }
            }
            imageUrlList?.clear()
            saveBitMap(frontBitmap, backBitmap, this, "images");
            if (currentMethodCall?.method == "ekycScanId") {
                runOnUiThread {
                    if (sdkInitResult) {
                        if (canStartScanning) {
                            if (!mReferenceID.isEmpty()) XenchainSDK.setReferenceID(mReferenceID)
                            if (mOCRPreview)
                                XenchainSDK.completeScanDeployment(
                                    mOnBoardingID,
                                    mMetaCardResults,
                                    mCardResults,
                                    kadConfig,
                                    this,
                                    this
                                )
                            else
                                XenchainSDK.completeScanDeployment(
                                    mOnBoardingID,
                                    mMetaCardResults!!.optString("refFrontID"),
                                    mMetaCardResults!!.optString("refFace"),
                                    mMetaCardResults!!.optString("refBackID"),
                                    mCardResults,
                                    this,
                                    this
                                )
                        } else {
                            checkAndRequestPermission(this@MainActivity, PERMISSION_REQUEST_ID)
                        }
                    }
                }
            } else if (currentMethodCall?.method == "ekycScanPassport") {
                runOnUiThread {
                    if (sdkInitResult) {
                        if (canStartScanning) {
                            if (!mReferenceID.isEmpty()) XenchainSDK.setReferenceID(mReferenceID)
                            if (mOCRPreview)
                                XenchainSDK.completeScanDeployment(
                                    mOnBoardingID,
                                    mMetaCardResults,
                                    mCardResults,
                                    passportConfig,
                                    this,
                                    this
                                )
                            else
                                XenchainSDK.completeScanDeployment(
                                    mOnBoardingID,
                                    mMetaCardResults!!.optString("refFrontID"),
                                    mMetaCardResults!!.optString("refFace"),
                                    mMetaCardResults!!.optString("refBackID"),
                                    mCardResults,
                                    this,
                                    this
                                )
                        } else {
                            checkAndRequestPermission(this@MainActivity, PERMISSION_REQUEST_ID)
                        }
                    }
                }
            }
        } else {
            var gson: Gson = Gson();
            if (currentResult != null) {
                if (currentMethodCall?.method == "ekycScanPassport") {
                    currentResult?.success(
                        gson.toJson(
                            ChannelResult<String>(
                                PASSPORT_RESULT,
                                "",
                                false,
                                errorMessage
                            )
                        )
                    )
                } else if (currentMethodCall?.method == "ekycScanId") {
                    currentResult?.success(
                        gson.toJson(
                            ChannelResult<String>(
                                IC_RESULT,
                                "",
                                false,
                                errorMessage
                            )
                        )
                    )
                }
            }
        }
    }

    override fun hologramScanResults(
        referenceID: String,
        onBoardingID: String,
        errorMessage: String
    ) {
        if (errorMessage.isEmpty()) {
            mReferenceID = referenceID
            mOnBoardingID = onBoardingID
            XenchainSDK.setReferenceID(mReferenceID)
            if (currentMethodCall?.method == "ekycScanId") XenchainSDK.deployScanner(
                kadConfig,
                this@MainActivity,
                ScanActivity::class.java,
                this@MainActivity
            )
            else XenchainSDK.deployScanner(
                passportConfig,
                this@MainActivity,
                ScanActivity::class.java,
                this@MainActivity
            )

        } else {
//            resultText.setText(errorMessage)
        }
    }

    private var imageUrlList: ArrayList<String>? = ArrayList<String>()
    private var faceImageList: ArrayList<String>? = ArrayList<String>()
    override fun scanMetaResults(
        idFaceBitmap: Bitmap?,
        refFace: String,
        idFrontBitmap: Bitmap?,
        refFrontID: String?,
        idBackBitmap: Bitmap?,
        refBackID: String?,
        errorMessage: String?
    ) {
        mFaceImageRef = refFace
        if (mMetaCardResults == null) mMetaCardResults = JSONObject()
        try {
            mMetaCardResults?.put("idFrontImage", idFrontBitmap)
            mMetaCardResults?.put("refFrontID", refFrontID)
            mMetaCardResults?.put("idBackImage", idBackBitmap)
            mMetaCardResults?.put("refBackID", refBackID)
            mMetaCardResults?.put("idFaceImage", idFaceBitmap)
            mMetaCardResults?.put("refFace", refFace)
        } catch (ignore: JSONException) {
        }
    }

    fun saveBitMap(idFrontBitmap: Bitmap?, idBackBitmap: Bitmap?, context: Context, type: String) {

        var thread: Thread = Thread(Runnable {
            var imageUrlfront: String? = "";
            var imageUrlBack: String? = "";
            if (idFrontBitmap != null) {
                imageUrlfront = FileUtils.savePhotoToSD(idFrontBitmap, context)
            }
            if (idBackBitmap != null) {
                imageUrlBack = FileUtils.savePhotoToSD(idBackBitmap, context)
            }
            Thread.sleep(200)
            if (type == "images") {
                if (imageUrlfront != null && "" != imageUrlfront) imageUrlList?.add(imageUrlfront)
                if (imageUrlBack != null && "" != imageUrlBack) imageUrlList?.add(imageUrlBack)
                if (currentMethodCall?.method == "ekycGetScanImages") {
                    if (currentResult != null && imageUrlList?.size!! > 0) {
                        runOnUiThread {
                            val gson: Gson = Gson()
                            val resultStr: String = gson.toJson(
                                ChannelResult<List<String>>(
                                    IMAGE_LIST,
                                    imageUrlList,
                                    true,
                                    ""
                                )
                            )
                            currentResult?.success(resultStr)
                        }
                    }
                }
            }
            if (type == "face") {
                if (imageUrlfront != null && "" != imageUrlfront) faceImageList?.add(imageUrlfront)
                if (currentMethodCall?.method == "ekycGetFaceImages") {
                    if (currentResult != null && faceImageList?.size!! > 0) {
                        runOnUiThread {
                            val gson: Gson = Gson()
                            val resultStr: String = gson.toJson(
                                ChannelResult<List<String>>(
                                    IMAGE_FACE,
                                    faceImageList,
                                    true,
                                    ""
                                )
                            )
                            currentResult?.success(resultStr)
                        }
                    }
                }
            }

        })
        thread.start()
    }

    override fun scanImageResults(imageObject: JSONObject) {
        mMetaCardResults = imageObject
//        if (mMetaCardResults!!.opt("idFaceImage") != null) resultImage.setImageBitmap(mMetaCardResults!!.opt("idFaceImage") as Bitmap)
    }

    override fun scanMetaResults(metaObject: JSONObject) {
        mMetaCardResults = metaObject
        mFaceImageRef = metaObject.optString("refFace")
    }

    override fun scanLandmarkInformation(landmarkScores: JSONObject) {
        Log.d(TAG, "scanLandmarkInformation: $landmarkScores")
    }

    override fun scanCompleteDeployment(status: Boolean, errorMessage: String) {
        Log.d(TAG, "scanCompleteDeployment: $errorMessage")
        if (status) {
            mCardResults = null
            mMetaCardResults = null
        }
    }

    override fun faceMatchResult(isMatched: Boolean, percentMatched: Double, error: String) {
        val builder = """
             IsMatched =>${if (isMatched) "True" else "False"}
             PercentMatched =>$percentMatched
             Error =>$error
             
             """.trimIndent()
        var jsonMap = HashMap<String, Any>()
        jsonMap.put("isMatched", isMatched)
        jsonMap.put("percentMatched", percentMatched)
        jsonMap.put("error", error)
        var gson: Gson = Gson();
        val resultString: String = gson.toJson(jsonMap)
        XenchainSDK.setReferenceID(mReferenceID)
        if (currentResult != null) {
            val resultStr = ChannelResult<HashMap<String, Any>>(FACE_RECORD, jsonMap, isMatched, "")
            currentResult?.success(gson.toJson(resultStr))
        }
//        resultText.setText(builder)
    }

    override fun faceMatchMetaResult(outputBitmap: Bitmap?, outputRef: String?) {
//        resultImage.setImageBitmap(outputBitmap)
        faceImageList?.clear()
        saveBitMap(outputBitmap, null, this, "face")
    }


    val EKYC_ALERT_DIALOG_OVERTIME: String = "ekycOvertime";
    val EKYC_ALERT_DIALOG_TIMEMAX: String = "ekycTimeMax";
    var dialogShowing: Boolean = false;
    fun showAlertDialog(activity: Activity, type: String?, backFlutter: Boolean) {
        val activityOther = MyActivityManager.activityManager.findTopActivity();
        if (activityOther is ScanActivity && activityOther?.dialogShowing) {
            return
        }
        if (activityOther is FaceRecordActivity && activityOther?.dialogShowing) {
            return
        }
        val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout = inflater.inflate(R.layout.dialog_notice, findViewById(R.id.custom))
        val tv_content = layout.findViewById<TextView>(R.id.tv_content)
        val tv_button_01 = layout.findViewById<TextView>(R.id.tv_button_01)
        val tv_button_02 = layout.findViewById<TextView>(R.id.tv_button_02)


        if (EKYC_ALERT_DIALOG_OVERTIME == type) {
            tv_content.text = activity.resources.getString(R.string.time_limit_str)
            tv_button_01.visibility = View.VISIBLE
            tv_button_01.text = activity.resources.getString(R.string.cancel_str)
            tv_button_02.visibility = View.VISIBLE
            tv_button_02.text = activity.resources.getString(R.string.try_again_str)
        } else if (EKYC_ALERT_DIALOG_TIMEMAX == type) {
            tv_content.text = activity.resources.getString(R.string.count_limit_str)
            tv_button_01.visibility = View.GONE
            tv_button_02.visibility = View.VISIBLE
            tv_button_02.text = activity.resources.getString(R.string.ok_str)
        }


        val customAlert = AlertDialog.Builder(activity)
        customAlert.setView(layout)
        customAlert.setCancelable(false)
        val alertDialog = customAlert.create()
        try {
            alertDialog.show()
            dialogShowing = true;
        } catch (e: Exception) {
            dialogShowing = false;
        }

        if (activity is ScanActivity) {
            activity.pauseTimer()
        } else if (activity is FaceRecordActivity) {
            activity.pauseTimer()
        }

        val background =
            Objects.requireNonNull<Window>(alertDialog.window).decorView.background as InsetDrawable
        background.alpha = 0
        alertDialog.setOnDismissListener {
            dialogShowing = false;
        }
        if (EKYC_ALERT_DIALOG_OVERTIME == type) {
            tv_button_01.setOnClickListener {
                alertDialog.dismiss()
                activity.onBackPressed()
                if (backFlutter && currentResult != null) {
                    val gson: Gson = Gson()
                    currentResult?.success(
                        gson.toJson(
                            ChannelResult<String>(
                                TRY_CANCEL,
                                "",
                                true,
                                ""
                            )
                        )
                    )
                }
            }
            tv_button_02.setOnClickListener {
                alertDialog.dismiss()
                if (activity is ScanActivity) {
                    activity.finish()
                } else if (activity is FaceRecordActivity) {
                    activity.finish()
                }
                if (backFlutter && currentResult != null) {
                    val gson: Gson = Gson()
                    currentResult?.success(
                        gson.toJson(
                            ChannelResult<String>(
                                TRY_AGAIN,
                                "",
                                true,
                                ""
                            )
                        )
                    )
                }
            }
        } else if (EKYC_ALERT_DIALOG_TIMEMAX == type) {
            tv_button_02.setOnClickListener {
                alertDialog.dismiss()
                activity.onBackPressed()
            }
        }
    }

    fun callBackFlutter(order: String) {
        if (currentResult != null) {
            val gson: Gson = Gson()
            currentResult?.success(gson.toJson(ChannelResult<String>(order, "", true, "")))
        }
    }

    fun changeAppConfig(newLanguage: String) {
        val configuration: Configuration = resources.configuration;
        val locale: Locale = Locale(newLanguage);
        configuration.locale = locale;
        val dm: DisplayMetrics = resources.displayMetrics;
        resources.updateConfiguration(configuration, dm);
    }


}
